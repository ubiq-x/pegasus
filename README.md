# Pegasus: Storage, Processing, and Analysis of Eye-Movement, Behavioral, and Arbitrary Data
A Web application supporting eye tracking research and analysis of arbitrary data.


## Contents
- [Features](#features)
- [Details](#details)
   * [System's Components](#systems-components)
      - [Pegasus Main](#pegasus-main)
      - [Pegasus Gaze](#pegasus-gaze)
      - [Pegasus Data](#pegasus-data)
   * [Technical Aspects](#technical-aspects)
      - [Server-Side](#server-side)
      - [Client-Side](#client-side)
   * [The Name](#the-name)
- [Dependencies](#dependencies)
- [Research Supported](#research-supported)
- [Acknowledgments](#acknowledgments)
- [Citing](#citing)


## Features
- Store and process eye-movements from recorded eye-tracking sessions
- Store and process arbitrary data sets
- Support for any eye-movement data, but especially psycholinguistic ones
- Subset eye-movement data using a query builder
- Generate data sets amenable to statistical analysis and predictive modeling
- R integration for built-in statistical analysis support
- Export data to plain text, HTML, and spreadsheet
- Supports EyeLink and Tobii eye trackers


## Details
### System's Components
Pegasus consists of two main applications: Pegasus Gaze and Pegasus Data.  While they serve different purposes and are as such largely independent, they are integrated in that data sets generated with Pegasus Gaze are automatically available for processing in Pegasus Data.  The third and implicit application, Pegasus Main, serves as an organizing layer on top of the two other applications.

#### Pegasus Main
The purpose of Pegasus Main (Figure 1) is to organize Pegasus Gaze experiments and data sets and Pegasus Data data set into projects.  Each projects can have an arbitrary number of experiments and datasets.  For example, a several-years long research project could accumulate multiple eye-tracking experiments, each of which contributes behavioral and eye-movement data and data sets derived from that data (i.e., generated using Pegasus Gaze) as well as arbitrary data sets related to each of the project (i.e., contributed through Pegasus Data).

![Pegasus Main](media/01-pegasus-main.png)
<p align="center"><b>Figure 1.</b> Pegasus Main.</p>

#### Pegasus Gaze
The purpose of Pegasus Gaze is to store eye-movement data and enable their easy processing and subsetting to generate data sets amenable to subsequent statistical analysis and predictive modeling.  Pegasus Gaze is the most substantial part of the Pegasus system and therefore I devote significant amount of space below to describing it.  While there is a lot of ground to cover, I elect to keep the description fairly high-level.

As shown in Figure 1, the user interface has been organized to suggest a workflow.  To that effect, the first-level organization is given by the following three tabs: Data Structures, Analysis, and Results.  This order resonates with the logic of first making sure data structures are in order, then moving on to analyzing the data, which eventually leads to obtaining results.  The second-level organization is given by a set of subordinate tabs nested inside of the first-level tabs.

![Pegasus Gaze.  List of experiments.](media/02-pegasus-gaze-exp.png)
<p align="center"><b>Figure 2.</b> Pegasus Gaze.  List of experiments.</p>

##### The "Data Structures" Tab
The highest level organizational unit in Pegasus Gaze is an `experiment` (Figure 2).  Further down, each experiment consists of a set of `stimuli` (Figure 3), `subjects` (Figure 4), and `trials` (Figure 4).  To accommodate the scenario of different subjects being exposed to different stimuli (or the same stimuli but in a different order, including multiple exposures), each trial a subject goes through is associated with a `stimuli sequence` (Figure 4).  Finally, each item in a `stimuli sequence` is associated with a set of eye-movements (i.e., fixations, saccades, and blinks) and events (e.g., on-screen prompts shown during the stimulus presentation as well as responses to those prompts).

![Pegasus Gaze, Data Structures, Stimuli.](media/03-pegasus-gaze-ds-stim.png)
<p align="center"><b>Figure 3.</b> Pegasus Gaze, Data Structures, Stimuli.</p>

As shown in Figure 3, each stimulus (here, textual stimulus) is described by a set of attributes like name, height, width, text, and font metrics.  A stimulus can be excluded from all analyses (and included again later if needed).  Furthermore, the Stimuli tab allows the experimenter to define _regions of interest_ (_ROIs_).  Figure 4 shows ROIs for the currently highlighted page of text (here, the first page of the _Sense and Sensibility_ novel by Jane Austen).  Viewing ROIs interface makes it easy to navigate between all the stimuli and it is encouraged that the experimenter views every single stimulus to make sure everything is in order.

![Pegasus Gaze, Data Structures, Stimuli.  ROIs: Saved.](media/04-pegasus-gaze-ds-stim-roi-saved.png)
<p align="center"><b>Figure 4.</b> Pegasus Gaze, Data Structures, Stimuli.  Displaying saved ROIs.</p>

![Pegasus Gaze, Data Structures, Stimuli.  ROIs: Define.](media/05-pegasus-gaze-ds-stim-roi-def-ui.png)
<p align="center"><b>Figure 5.</b> Pegasus Gaze, Data Structures, Stimuli.  Defining ROIs.</p>

Naturally, ROIs need to be defined first.  Figure 5 shows ROIs at the time of being defined.  Clause-initial, clause-terminal, sentence-initial, and sentence-terminal ROIs are recognized automatically, but the experimenter has the option to intervene in this auto-marking.  Those markings end up being represented as ROI-level variables (see Figure 8).  Apart from automatic variables, the experimenter has the option to define custom ROI variables.  Those variables can be used to, e.g., identify the target word in a sentence or a set of target words on a page of text.  Furthermore, any ROI can be disabled which will exclude it from all analyses.  This functionality may be useful when, e.g., hyphenated words are not of interest.  As shown on Figure 6, all ROI coordinates and variables can be exported from Pegasus Gaze for the purpose of subsequent import or other purposes.

![Pegasus Gaze, Data Structures, Stimuli.  ROIs list](media/06-pegasus-gaze-ds-stim-roi-lst.png)
<p align="center"><b>Figure 6.</b> Pegasus Gaze, Data Structures, Subjects and Trials.</p>

![Pegasus Gaze, Data Structures, Subjects and Trials](media/07-pegasus-gaze-ds-sub.png)
<p align="center"><b>Figure 7.</b> Pegasus Gaze, Data Structures, Subjects and Trials.</p>

Figure 7 shows the information on subjects, trials, stimuli sequences, and events.  Additionally to displaying plethora of useful times and aggregates (e.g., the number of blinks), that interface allows the experimenter to display calibration and validation results (Figure 8) as well as eye-movement data for each stimulus presentation.  As shown in Figure 9, those eye movements are presented in a new window where the interface allows the experimenter to perform global eye-movement adjustment (i.e., translation, scaling, and rotation) and to replay the eye movements.

![Pegasus Gaze, Data Structures, Subjects and Trials.  Calibration-validation.](media/08-pegasus-gaze-ds-sub-cal.png)
<p align="center"><b>Figure 8.</b> Pegasus Gaze, Data Structures, Subjects and Trials.  Calibration and validation results.</p>

![Pegasus Gaze, Data Structures, Subjects and Trials.  Eye movements.](media/09-pegasus-gaze-ds-sub-em.png)
<p align="center"><b>Figure 9.</b> Pegasus Gaze, Data Structures, Subjects and Trials.  Eye movement adjustment and replay interface.</p>

As shown in Figures 10 and 11, the application allows variables to be defined for `subjects`, `trials`, `stimuli`, and, as I have mentioned earlier, `regions of interest` (`ROIs`).  This way, demographic, behavioral, and other data can be fused with eye-movement data to eventually become a part of the generated and to-be-analyzed datasets.

![Pegasus Gaze, Data Structures, Variables](media/10-pegasus-gaze-ds-var.png)
<p align="center"><b>Figure 10.</b> Pegasus Gaze, Data Structures, Variables.</p>

![Pegasus Gaze, Data Structures, Variables.  Values of subject variables](media/11-pegasus-gaze-ds-var-values.png)
<p align="center"><b>Figure 11.</b> Pegasus Gaze, Data Structures, Variables.  Values of subject variables.</p>

Pegasus Gaze infers the structure of an experiment as described above based on the contents of an import ZIP archive.  That archive has to contain data recorded by either an EyeLink or a Tobii eye tracker, and can also contain optional information on ROIs, variables, excluded objects (i.e., stimuli, trials, subjects, ROIs, and stimulus-sequence items), memos (which serve informational purpose only), etc.  Adding support for a different eye tracker boils down to implementing an appropriate import routine for that eye tracker.  In other words, the purpose of an import routine is to translate between data structures used by a given eye tracking system and the internal data structures of Pegasus.

##### The "Analysis" Tab
Figure 12 shows the analysis interface.  Every analysis generates a dataset.  Once generated, a dataset can be subjected to statistical analysis or be the basis of predictive modeling.

![Pegasus Gaze, Analysis](media/12-pegasus-gaze-anal-clear.png)
<p align="center"><b>Figure 12.</b> Pegasus Gaze, Analysis.  Without any constraints.  Running this analysis would result in a dataset that includes all objects (stimuli, subject, etc.) unless they were excluded elsewhere in the application.</p>

Pegasus Gaze allows for data aggregation on the five following levels:

1. Stimulus sequence (or event, if defined)
2. Region of interest (ROI; current word only)
3. Region of interest (ROI; previous, current, and next words: word-order adjacent)
4. Region of interest (ROI; previous, current, and next words: fixation-order adjacent)
5. Fixation

The software allows a dataset be generated from the original (i.e., unadjusted) or adjusted eye-movement data.  To promote the use of only highest quality eye-movement data, the experimenter can choose the quality of calibration and validation.  Moreover, for previous-current-next analyses, the size of the perceptual span can be defined as well.  Furthermore, the interface allows to request word expansion to:

- The nearest clause
- The nearest sentence

thus ensuring that full clauses or sentences will make it to the resulting dataset.

Finally, a set of constraints regarding subjects, trials, stimuli, ROIs, events, and fixations can be defined to limit the scope of analysis.  Figures 13 and 14 show an example of subject name and subject variables being constrained while Figure 15 shows an example of an event constraint.

![Pegasus Gaze, Analysis](media/13-pegasus-gaze-anal-sub-name.png)
<p align="center"><b>Figure 13.</b> Pegasus Gaze, Analysis.  Constraining subject by name.</p>

![Pegasus Gaze, Analysis](media/14-pegasus-gaze-anal-sub-var.png)
<p align="center"><b>Figure 14.</b> Pegasus Gaze, Analysis.  Constraining subject by variable.</p>

![Pegasus Gaze, Analysis](media/15-pegasus-gaze-anal-evt.png)
<p align="center"><b>Figure 15.</b> Pegasus Gaze, Analysis.  Constraining events.</p>

Figure 16 shows how the analysis interface looks like with several constraints defined.  The "Calculate" button starts the analysis while the "+" button just next to it can be used to schedule the current analysis as part of a script.  A script is a sequence of analyses that are executed in a strictly sequential manner.  Using a script, the experimenter can prepare all the necessary analyses first and then run them in a batch mode.  This is especially useful for analyses that take a long time to complete (e.g., several hours).

![Pegasus Gaze, Analysis](media/16-pegasus-gaze-anal-filled.png)
<p align="center"><b>Figure 16.</b> Pegasus Gaze, Analysis.  With multiple constraints.  Running this analysis would result in a dataset that only considers the specified subjects, stimuli, ROIs, and events.</p>

##### The "Results" Tab
As shown in Figure 17, the Results tab lists all datasets generated with the analysis interface.  From there, datasets can be exported to text (tab-delimited) or previewed in a new window (only the first 200 rows are displayed; Figure 18).  Moreover, the analysis associated with a dataset can be re-run.

![Pegasus Gaze, Results](media/17-pegasus-gaze-res.png)
<p align="center"><b>Figure 17.</b> Pegasus Gaze, Results.</p>

![Pegasus Gaze, Results](media/18-pegasus-gaze-res-html.png)
<p align="center"><b>Figure 18.</b> Pegasus Gaze, Results.  HTML preview of a dataset.</p>

This is where Pegasus Gaze workflow ends.  However, the experimenter may wish to process the generated datasets further.  If that is the case, they can do so using Pegasus Data.

##### Psycholinguistic Focus
As a digression, because of my involvement in reading research, Pegasus Gaze is biased towards eye movements indigenous to reading and mindless reading.  For example, ROI-based aggregation is used irrespective of the research area.  However, as I have delineated above, when used on reading data, Pegasus Gaze automatically declares each word a ROI.  Moreover, if ROI is used as the unit of analysis (i.e., a word is the aggregating instance), a number of psycholinguistic variables are added to the resulting data set automatically.  These variables include:

- Word frequency (SUBTLEX),
- Word frequency (Kucera-Francis),
- Word age of acquisution,
- Word familiarity,

and many more.  Moreover, eye-movement variables typically used in reading research are added as well, e.g.:

- Gaze duration (i.e., the sum of all fixation durations in first-pass reading),
- Number of first-pass fixations,
- Number of regressions,
- Regression path duration,
- Probability of skipping,
- Probability of exactly one fixation,
- Probability of two-or-more fixations,

and many others.  Another way in which reading bias manifests itself is simply on the level of stimuli.  While "generic" visual stimuli (e.g., photographs of human faces, arbitrary scenes, or even videos) can be created, Pegasus' data structures are capable of attaching extra and reading-only attributes to textual stimuli (e.g., font metrics; Figure 3).

This psycholinguistic focus doesn't demarcate the application's capabilities.  On the level of data structures, Pegasus Gaze is a flexible software capable of dealing with any eye-movement data.

#### Pegasus Data
Pegasus Data allows the experimenter to manipulate datasets generated by Pegasus Gaze as well as other arbitrary datasets.  In fact, all Pegasus Gaze datasets are automatically visible in Pegasus Data.  The opposite is not true, however.  That is, datasets imported into or otherwise created by Pegasus Data are not displayed in Pegasus Gaze.  This is consistent with Pegasus Gaze being more specific and Pegasus Data being more general.

##### Datasets
Pegasus Data (Figures 19-22) conceptualizes a data set as a two-dimensional table of variables as columns and observations as rows.  The application is capable of importing, exporting (plain text, HTML, and spreadsheet), duplicating, renaming, merging, and splitting data sets.  It can also modify names, types, and values of variables, create new variables, discretize continuous variables, automatically perform routine statistical tests on numerical variables (e.g., normality tests), and generate reports.

![Pegasus Data.  List of datasets.](media/19-pegasus-data-ds.png)
<p align="center"><b>Figure 19.</b> Pegasus Data.  List of datasets.</p>

![Pegasus Data.  Dataset duplication.](media/20-pegasus-data-ds-dup.png)
<p align="center"><b>Figure 20.</b> Pegasus Data.  Dataset duplication.</p>

![Pegasus Data.  List of dataset variables.](media/21-pegasus-data-var.png)
<p align="center"><b>Figure 21.</b> Pegasus Data.  List of dataset variables.</p>

![Pegasus Data.  Variable transformation.](media/22-pegasus-data-var-xform.png)
<p align="center"><b>Figure 22.</b> Pegasus Data.  Variable transformation.</p>

##### R Console
Pegasus Data features a convenient console (Figure 23) that allows the experimenter to control an instance of the R statistical environment which resides on the server.  All console-issued commands are saved in a purgeable per-dataset history and a convenient cursor-up-and-down history navigation (think Unix shell) is available.  While not a substitute for full-blown scripts and R Markdown computational notebooks, this capability can come handy when performing quick sanity checks or getting an understanding of datasets prior to exporting them to text files for further analysis.

![Pegasus Data.  R console.](media/23-pegasus-data-r.png)
<p align="center"><b>Figure 23.</b> Pegasus Data.  R console.</p>

##### Bayesian Networks
I have started to integrate the [SMILE](https://www.bayesfusion.com/smile/) Bayesian network library into Pegasus Data (Figure 24).  However, due to my switch of focus, that feature remains unfinished.  When completed, it would allow the user to perform operations like parameter or structural learning and inference.  Of course, exported datasets can still be used in SMILE or other Bayesian network and other machine learning libraries, just without tight integration.

![Pegasus Data.  Sample Bayesian net.](media/24-pegasus-data-bn.png)
<p align="center"><b>Figure 24.</b> Pegasus Data.  Sample Bayesian net.</p>

### Technical Aspects
Pegasus is a Web application implemented within a classic three-tier RESTful architecture.  I glide over some details below.

#### Server-Side
The system has been optimized in terms of bandwidth usage; data is requested from the server on a need-basis and is cached locally for the duration of a session.  For example, eye movements are requested in the smallest stimulus presentation units and are downloaded only right prior to being displayed.

On the server side, Pegasus is a collection of Java Servlets which interface a PostgreSQL database the size of which can quickly reach hundreds of gigabytes if multiple large corpus experiments are present.  Tables are normalized, but small amount of redundancy has been introduced in order to cut down on the number of a certain costly joins.  All integrity constraints have been implemented in the data layer itself (through, e.g., `unique` and `check` constraints) and are not enforced by the logic layer.  While some checks may still be done by the servlets for performance reasons, data integrity does not depend on them.  Ubiquitous transactions ensure data integrity is not violated during, sometimes long, chains of `insert`/`update` queries or DDL statements.

Pegasus has been designed with a multi-user access in mind and as such supports user accounts.  Moreover, the database has also been designed to accommodate future geographical localization or distribution.

#### Client-Side
Pegasus is a single-page Web application with a desktop application look-and-feel.  Displaying stimuli, defining and displaying ROIs, and displaying eye movements (including replaying) is done by the canvas element (or more precisely, a stack of canvas elements).  Because the amounts of data required for most tasks is substantial, majority of tasks are delegated to the server.


### The Name
In Greek mythology, Pegasus was a winged divine horse, son of Poseidon.  It was ridden by the hero Bellerophon.  I chose the name Pegasus because this software supports eye-tracking research just like Pegasus supported Bellerophon on their many adventures.  And besides, I like the name.


## Dependencies
- Servlet container (e.g., [Apache Tomcat](https://tomcat.apache.org))
- [PostgreSQL](https://www.postgresql.org)
- [R](https://www.r-project.org)
- [SR Research](https://www.sr-research.com) EyeLink or [Tobii](https://www.tobii.com) eye-tracker


## Research Supported
Pegasus has been used in the following research:

Loboda, T.D. (2014).  Study and detection of mindless reading.  _PhD Dissertation_.

Loboda, T.D., Brusilovsky, P., & Brunstein, J. (2011).  Inferring relevance from eye-movements of readers.  _Intelligent User Interfaces (IUI)_, 175-184. ACM Press.

Loboda, T.D. & Reichle, E.D. (2012).  Lag and successor effects during mindless reading.  Talk given at the 16th _European Conference on Eye Movements_, Marseilles, France.

Loboda, T.D. & Reichle, E.D. (2012).  The effects of mind-wandering and working memory capacity on text comprehension.  Poster presented at the 16th _European Conference on Eye Movements_, Marseilles, France.

Loboda, T.D. & Reichle, E.D. (2014).  Detecting mindless reading from eye movements.  Poster presented at the _Psychonomic Society's 55th Annual Meeting_, Long Beach, CA, USA.

Loboda, T.D. & Reichle, E.D. (2015).  An eye-movement analysis of visual and cognitive processes during mindful versus mindless reading.  Talk given at the _Symposium on Visual Aspects of Reading_, Grasmere, UK.

Loboda, T.D. & Reichle, E.D. (2018).  Detecting mindless reading from eye movements. _Manuscript in preparation_.

Loboda, T.D. & Reichle, E.D. (2018).  Normal vs mindless reading: A comprehensive eye-movement study. _Manuscript in preparation_.

Reichle, E.D., Reineberg, A.E., & Schooler, J.W. (2010).  Eye movements during mindless reading.  _Psychological Science, 21(9)_, 1300-10.


## Acknowledgments
This software has benefited from discussions I had with [Professor Erik Reichle](http://www.erikdreichle.com), who at the time was my doctoral advisor.


## Citing
Loboda, T.D. (2009).  Pegasus: Storage, Processing, and Analysis of Eye-Movement, Behavioral, and Arbitrary Data [computer software].  Available at https://gitlab.com/ubiq-x/pegasus
