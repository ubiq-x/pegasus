/**
 * Pegasus: Main
 * -------------
 * 
 * DEPENDS ON:
 *   $.js
 */

/*
TODO:
- http://developer.yahoo.com/yui/examples/datatable/dt_tabview.html
*/


if (!Pegasus) var Pegasus = {};

Pegasus.Main = function() {
  
  // ===================================================================================================================
  // ==[ PRIVATE ]======================================================================================================
  // ===================================================================================================================
  
  var CONST = {
    appId       : "main",
    cookie      : {
      appName : "Pegasus-",
      days: 30
    },
    err         : {
      unexpected: "An unexpected error has occurred. This might be due to loss of connection or server side error. Please try again. If you are able to connect to the server but the error persists please notify the system administrator (Tomek Loboda, the.snake@gmail.com)."
    },
    panelW      : "900px",
    serverURI_S : "do",
    serverURI_L : location.protocol + "//" + location.host + "/pegasus/do",
    time        : { cmdResHide: 10000 }
  };
  
  var apps = {
    data: {
      id    : "data",
      btnStart : null,
      info     : null,
      name     : "Data",
      obj      : null,
      win      : null
    },
    gaze: {
      id    : "gaze",
      btnStart : null,
      info     : null,
      name     : "Gaze",
      obj      : null,
      win      : null
    }
  };
  
  var data = {};
  
  var state = {
    app     : { startUp: true },
    currPrj : { idx: -1, ui: { rec: null, radio: null } },
    timer   : { cmdRes : null }
  };
  
  var ui = {
    app: { mask: null },
    cmdRes: null,
    
    panel: {
      login   : { p: null },
      proc    : { p: null },
      profile : { p: null, username: null, email: null, lastLogin: null, apps: null },
      txt      : { p: null, edit: null, msg: null, view: null, fnNoteHide: null }
    },
    
    prj: { btnNew: null, btnDel: null },
    region: { app: { body: null } },
    sbar: { inf: null },
    
    tbl: {
      prj : { src: null, tbl: null }
    }
  };
  
  var valid = new Pegasus.util.Validation();
  
  
  // -------------------------------------------------------------------------------------------------------------------
  /**
   * Calculates sizes of critical elements and, if necessary, resizes some of them.
   */
  function afterWinResize() {
    var a = ui.region.app;
    a.body = YAHOO.util.Dom.getRegion($("app-bd"));
    if (!a.body.height) a.body.height = a.body.bottom - a.body.top;
    
    uiResizeDataTbl(ui.tbl.prj.tbl, false, true);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  /**
   * Checks if credentials have been stored in the cookie and if yes attempt an auto-login bypassing the login form.
   */
  function autoLogin() {
    var u = Pegasus.util.Cookie.get(CONST.cookie.appName + "username");
    var p = Pegasus.util.Cookie.get(CONST.cookie.appName + "passwd");
    
    if (!u || !p) {
      ui.panel.login.p.show();
      ui.panel.proc.p.hide();
      return;
    }
    
    procShow("Attempting auto-login...");
    $call({
      method  : "POST",
      uri     : CONST.serverURI_S,
      params  : "app=" + CONST.appId + "&cmd=login&username=" + u + "&passwd=" + p + "&pseudo=1",
      eval    : true,
      minHead : false,
      
      fnOk: cbLoginSuccess,
      fnErr: function (o) {
        ui.panel.login.p.show();
        ui.panel.proc.p.hide();
      }
    });
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  /**
   * This function is used by both manual and automatic login.
   */
  function cbLoginSuccess(o) {
    procShow("Loading...");
    if (o.outcome) {
      // Store the data received:
      $setObj(data, o.data);
      
      var currPrjId = parseInt(Pegasus.util.Cookie.get(CONST.cookie.appName + "curr-prj-id"));
      var currPrjIdx = -1;
      
      // Process projects:
      var R = [];  // rows
      for (var i=0, ni=data.prj.length; i < ni; i++) {
        var p = data.prj[i];
        if (p.id === currPrjId) currPrjIdx = i;
        R.push(prjGetTblRow(p, i, (p.id === currPrjId)));
      }
      
      var t = ui.tbl.prj.tbl;
      t.addRows(R);
      
      if (currPrjIdx != -1) {
        var row = t.getRow(currPrjIdx);
        var radio = t.getFirstTdEl(t.getLastTrEl()).firstChild.firstChild;
        t.selectRow(radio);
        
        state.currPrj.ui.radio = t.getRecord(radio);
        state.currPrj.ui.rec   = t.getRecord(radio);
        state.currPrj.idx      = currPrjIdx;
        
        prjSetBtnsEnabled(true);
        prjSetCurr();
      }
      
      Pegasus.Main.tblResort(t);
      
      // Persist the credentials used:
      Pegasus.Main.setCookie(CONST.cookie.appName, "username", data.profile.username);
      Pegasus.Main.setCookie(CONST.cookie.appName, "passwd", data.profile.passwd);
      
      initApp();
    }
    else {
      ui.panel.login.p.show();
      ui.panel.proc.p.hide();
      alert(o.msg);
    }
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function cmdResShow(msg) {
    if (state.timer.cmdRes) window.clearTimeout(state.timer.cmdRes);
    ui.cmdRes.innerHTML = msg;
    $show(ui.cmdRes);
    state.timer.cmdRes = window.setTimeout(function () { state.timer.cmdRes = null; $hide(ui.cmdRes); }, CONST.time.cmdResHide);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function construct() {}
  
  
  // -------------------------------------------------------------------------------------------------------------------
  /**
   * Executes command and handles a part of the response. To wit, server error or default actions for true or false 
   * outcome if the corresponding handler functions have not been specified. 'params' is a hash of key-value pairs.
   */
  function execCmd(get, params, fnOkTrue, fnOkFalse, fnErr, minHead) {
    var cmd = pub.getCmd(CONST.appId, params);
    
    $call({
      method  : (get ? "GET" : "POST"),
      uri     : (get ? CONST.serverURI_S + "?" + cmd : CONST.serverURI_S),
      params  : (get ? null : cmd),
      eval    : true,
      minHead : minHead,
      
      fnOk: function (res) {
        if (res.outcome) {
          execCmd_procRes(res);
          if (fnOkTrue) fnOkTrue(res);
          else ui.panel.proc.p.hide();
        }
        else {
          if (fnOkFalse) fnOkFalse(res)
          else {
            alert(res.msg);
            ui.panel.proc.p.hide();
          };
        }
      },
      
      fnErr: function (res) {
        if (fnErr) fnErr(res);
        else {
          alert(CONST.err.unexpected);
          ui.panel.proc.p.hide();
        }
      }
    });
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function execCmd_procRes(res) {
  }
  
  
  // -----------------------------------------------------------------------------------------------------------------
  /**
   * Generates the YUI data source and table.
   * 
   * ARGS:
   *   ui       : the appropriate 'ui' object's property
   *   cont     : the container
   *   fields   : array of fields
   *   cols     : array of columns
   *   events   : array of events
   *   settings : array of settings
   *   panel    : provide for data tables that should be editable while displayed in a modal panel
   *   scroll   : flag
   */
  function genTbl(appId, ui, cont, fields, cols, events, settings, panel, scroll) {
    // Source:
    var src = new YAHOO.util.DataSource();
    src.responseType   = YAHOO.util.DataSource.TYPE_JSARRAY;
    src.responseSchema = { fields: fields };
    ui.src = src;
    
    // Table:
    var tbl = new YAHOO.widget.ScrollingDataTable(cont, cols, src, $setObj({ scrollable: scroll }, settings));
    if (!events.cellClickEvent) tbl.on("cellClickEvent", tbl.onEventShowCellEditor);
    for (var e in events) tbl.on(e, events[e]);
    
    tbl.render();
    
    if (panel) {
      tbl.doBeforeShowCellEditor = function (cellEditor) {
        var el = cellEditor.getContainerEl();
        if (el.parentNode === document.body) {
          panel.body.appendChild(el);
          
          cellEditor.subscribe("showEvent", 
            function () {
              var xy = YAHOO.util.Dom.getXY(this.getTdEl());
              YAHOO.util.Dom.setXY(this.getContainerEl(), xy);
            }
          );
        }
        
        return true;
      };
    }
    
    ui.tbl = tbl;
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function getCurrPrj() { return data.prj[state.currPrj.idx]; };
  
  
  // -------------------------------------------------------------------------------------------------------------------
  /**
   * Parses the server response anticipating an object literal, performs operations common to many requests, and returns 
   * the parsed out object.
   */
  function getRes(o) {
    var res = {};
    try {
      eval("res = " + o.responseText);
    }
    catch (ex) {
      ui.panel.proc.p.hide();
      alert(ex);
    }
    
    return res;
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  /**
   * Returns a function that can be used as an async submitter for a DataTable inline editor. That function will take
   * care of requesting a single attribute updated.
   * 
   * IN:
   *   aFnValid: an array of validator functions; each takes one argument and returns a boolean
   *   ...
   */
  function getSetAttrFn(aFnValid, fnAfterSuccess, fnGetCmd) {
    return function (cb, newVal) {
      if ((this.value === null && newVal === "") || newVal == this.value) { cb(false,newVal); return; }
      
      // Validate:
      if (aFnValid.length > 0) {
        var isValid = $lfold(
          function (a,b) { return a && b; },
          $map(function (x) { return x(newVal); }, aFnValid),
          true
        );
        if (!isValid) { cb(false,newVal); return; }
      }
      
      // Persist server-side:
      procShow("Saving...");
      var that = this;
      $call({
        method  : "GET",
        uri     : CONST.serverURI_S + "?" + fnGetCmd(this, newVal),
        params  : null,
        eval    : true,
        minHead : true,
        
        fnOk: function(res) {
          if (res.outcome) {
            fnAfterSuccess(that, newVal, res);
            cb(true, newVal);
            ui.panel.proc.p.hide();
          }
          else {
            alert(res.msg);
            cb(false, newVal);
            ui.panel.proc.p.hide();
          }
        },
        
        fnErr: function(res) {
          alert(CONST.err.unexpected);
          cb(false, newVal);
          ui.panel.proc.p.hide();
        }
      });
    };
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initApp() {
    ui.panel.login.p.hide();
    procShow("Loading (populating UI)...");
    
    // Profile:
    var u = ui.panel.profile;
    var dp = data.profile;
    u.username.innerHTML  = dp.username;
    u.email.innerHTML     = dp.email;
    u.lastLogin.innerHTML = (dp.lastLogin ? dp.lastLogin.toDateString() : "---");
    u.apps.innerHTML      = "Data: " + dp.apps.data.dsCnt + (dp.apps.data.dsCnt === 1 ? " data set (" : " data sets (") + $size2str(dp.apps.data.dbSize) + ")<br />Gaze: " + dp.apps.gaze.expCnt + (dp.apps.gaze.expCnt === 1 ? " experiment (" : " experiments (") + $size2str(dp.apps.gaze.dbSize) + ")";
    
    $hide(ui.app.mask);
    ui.panel.proc.p.hide();
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initLogin() {
    var fnLogin = function(e) {
      var d = this.getData();
      if (d.username === "" || d.passwd === "") {
        alert("Please provide both username and password.");
        return;
      }
      procShow("Authenticating...");
      Pegasus.util.Cookie.remove(CONST.cookie.appName + "username");
      Pegasus.util.Cookie.remove(CONST.cookie.appName + "passwd");
      this.submit();
    };
    
    var panel = new YAHOO.widget.Dialog("panel-login",
      {
        buttons         : [ { text: "Log in", handler: fnLogin, isDefault: true } ],
        close           : false,
        draggable       : false,
        fixedcenter     : true,
        dragOnly        : true,
        hideaftersubmit : false,
        modal           : true,
        visible         : false,
        underlay        : "none"
      }
    );
    
    panel.callback = {
      success: function (req) { cbLoginSuccess($getRes(req)); },
      failure: function (o) {
        alert("Server unreachable. Please check your internet connection and try again.");
        ui.panel.login.p.show();
        ui.panel.proc.p.hide();
      }
    };
    panel.render();
    ui.panel.login.p = panel;
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initMain() {
    ui.app.mask = $("app-mask");
    
    ui.region.app.body = $("app-bd");
    
    // (1) Layout:
    ui.main = new YAHOO.widget.Layout({
      units: [
        { position: "top",    body: "app-hd-outer", height: 36 },
        { position: "bottom", body: "app-ft-outer", height: 23 },
        { position: "center", body: "app-bd" }
      ]
    });
    ui.main.render();
    
    // (2) Toolbar:
    new YAHOO.widget.Button("tbar-btn-profile", { onclick: { fn: function (e) { ui.panel.profile.p.show(); } } });
    new YAHOO.widget.Button("tbar-btn-logout", { onclick: { fn: logOut } });
    
    // (4) Status bar:
    ui.sbar.inf = $("inf");
    ui.sbar.inf.onclick = function(e) { alert("This is the main window of Pegasus. It is safe to close this window only if no Pegasus applications are running."); }
    sbarSet();
    
    // (5) Other:
    ui.cmdRes = $("cmd-res");
    ui.cmdRes.onclick = function (e) { window.clearTimeout(state.timer.cmdRes); state.timer.cmdRes = null; $hide(ui.cmdRes); };
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initProc() {
    ui.panel.proc.p = new YAHOO.widget.Panel("panel-proc", {
      width       : "240px",
      fixedcenter : true,
      close       : false,
      draggable   : false,
      modal       : true,
      visible     : false
    });
    ui.panel.proc.p.render();
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initPrj() {
    // Buttons:
    ui.prj.btnNew  = new YAHOO.widget.Button("prj-btn-new", { onclick: { fn: prjNew }                 });
    ui.prj.btnDel  = new YAHOO.widget.Button("prj-btn-del", { onclick: { fn: prjDel }, disabled: true });
    ui.prj.btnNote = new YAHOO.widget.Button("prj-btn-note", { onclick: { fn: function (e) { txtShow("Note", null, getCurrPrj().note, true, true); } } });
    
    for (var a in apps) {
      apps[a].btnStart = new YAHOO.widget.Button({ label: apps[a].name, container: $("prj-btn-bar"), onclick: { fn: function (appId) { return function () { Pegasus.Main.appStart(appId, this); } }(apps[a].id) }, disabled: true });
    }
    
    // Data table -- submitters:
    var fnAfterSuccess = function (prop) {
      return function (o,newVal,res) {
        data.prj[o.getRecord().getData("idx")][prop] = newVal;
      };
    };
    
    var fnGetCmd = function (attr) {
      return function (o,newVal) {
        return pub.getCmd(CONST.appId, { cmd: "attr-set", "obj-name": "prj", "obj-human-name": "project", "obj-id": o.getRecord().getData("id"), "attr-name": attr, "attr-val": $enc(newVal) });
      };
    };
    
    var fnSetName = getSetAttrFn([valid.strNonEmpty], fnAfterSuccess("name"), fnGetCmd("name"));
    var fnSetMemo = getSetAttrFn([],                  fnAfterSuccess("memo"), fnGetCmd("memo"));
    
    // Data table -- columns:
    var cols = [
      { key: "curr", label: "", formatter: "radio" },
      { key: "name", label: "Name", sortable: true, editor: new YAHOO.widget.TextboxCellEditor({ asyncSubmitter: fnSetName }), width: 200, maxAutoWidth: 320, className: "name" },
      { key: "created", label: "Created", sortable: true, formatter: Pegasus.Main.UI.Tbl.Formatter.date, sortOptions: { sortFunction: Pegasus.Main.UI.Tbl.Sorter.genDate(function () { return data.prj; }, "created") } },
      { key: "noteLen", label: "Note", sortable: true },
      { key: "memo", label: "Memo", sortable: true, editor: new YAHOO.widget.TextareaCellEditor({ asyncSubmitter: fnSetMemo }), width: 400, maxAutoWidth: 600 }
    ];
    
    // Data table -- event handlers:
    var onRadioClickEvent = function (oArgs) {
      var radio = oArgs.target;
      var rec = this.getRecord(radio);
      var newIdx = parseInt(rec.getData("idx"));
      
      if (newIdx === state.currPrj.idx) return;
      
      if (state.currPrj.ui.rec) state.currPrj.ui.rec.setData("curr", false);
      rec.setData("curr", true);
      
      state.currPrj.idx      = newIdx;
      state.currPrj.ui.rec   = rec;
      state.currPrj.ui.radio = radio;
      
      ui.tbl.prj.tbl.unselectAllRows();
      ui.tbl.prj.tbl.selectRow(radio);
      
      prjSetBtnsEnabled(true);
      prjSetCurr();
    };
    
    // Data table:
    genTbl(CONST.appId, ui.tbl.prj, $("prj-dt"), ["idx","curr","name","created","noteLen","memo"], cols, { radioClickEvent: onRadioClickEvent }, { sortedBy: { key:"name", dir:"asc" } }, null, true);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initProfile() {
    var panel = new YAHOO.widget.Dialog("panel-profile", {
      buttons             : [
        { text: "Change username", handler: function (e) { alert("Not implemented yet."); } },
        { text: "Change password", handler: function (e) { alert("Not implemented yet."); } },
        { text: "Close", handler: function (e) { this.hide(); }, isDefault: true }
      ],
      constraintoviewport : true,
      draggable           : true,
      dragOnly            : true,
      fixedcenter         : true,
      hideaftersubmit     : false,
      modal               : true,
      visible             : false
    });
    panel.render();
    ui.panel.profile.p = panel;
    
    ui.panel.profile.username  = $("profile-username");
    ui.panel.profile.email     = $("profile-email");
    ui.panel.profile.lastLogin = $("profile-last-login")
    ui.panel.profile.apps      = $("profile-apps");
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initTxtPanel() {
    // Panel -- button event handlers:
    var fnSelTxt = function (e) {
      if (ui.panel.txt.edit.style.display === "block") {
        ui.panel.txt.edit.select();
      }
      else {
        window.getSelection().removeAllRanges();
        var range = document.createRange();
        range.selectNode(ui.panel.txt.view);
        window.getSelection().addRange(range);
      }
    };
    
    // Panel -- event handlers:
    ui.panel.txt.fnNoteHide = function () {
      var u = ui.panel.txt;
      
      u.p.hideEvent.unsubscribe(u.fnNoteHide);
      
      var prj = getCurrPrj();
      if (prj.note === u.edit.value) return;
      
      prj.note = u.edit.value;
      execCmd(false,
        { cmd: "attr-set", "obj-name": "prj", "obj-human-name": "project", "obj-id": prj.id, "attr-name": "note", "attr-val": $enc(prj.note) },
        function (res) {
          cmdResShow(res.outcome ? "Note saved" : "Note NOT saved!");
        },
        null, null, true
      );
    };
    
    // Panel:
    var panel = new YAHOO.widget.Dialog("panel-txt",
      {
        buttons             : [
          { text: "Select text", handler: fnSelTxt },
          { text: "Close",       handler: function (e) { this.hide(); } }
        ],
        constraintoviewport : true,
        dragOnly            : true,
        fixedcenter         : true,
        modal               : true,
        visible             : false,
        width               : CONST.panelW
      }
    );
    panel.render();
    ui.panel.txt.p = panel;
    
    ui.panel.txt.msg  = $("panel-txt-msg");
    ui.panel.txt.view = $("panel-txt-view");
    ui.panel.txt.edit = $("panel-txt-edit");
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function logOut() {
    if (!confirm("Log out?")) return;
    
    // Main:
    Pegasus.util.Cookie.remove("Pegasus-" + "username");
    Pegasus.util.Cookie.remove("Pegasus-" + "passwd");
    
    // Application-specific:
    /*
    Pegasus.util.Cookie.remove("Pegasus-Gaze-" + "curr-exp-id");
    Pegasus.util.Cookie.remove("Pegasus-Data-" + "curr-ds-id");
    */
    
    window.location.reload();
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function prjDel() {
    var prj = getCurrPrj();
    if (!confirm("You are about to delete the active project ('" + prj.name + "'). This operation is irreversible.")) return;
    
    procShow("Deleting the project...");
    execCmd(true,
      { cmd: "prj-del", "prj-id": prj.id },
      function (res) {
        data.prj[state.currPrj.idx] = null;
        ui.tbl.prj.tbl.deleteRow(state.currPrj.ui.rec);
        
        resetCurrState("currPrj");
        prjSetBtnsEnabled(false);
        
        ui.panel.proc.p.hide();
      },
      null, null, true
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function resetCurrState(names) {
    for (var i=0; i < names.length; i++) {
      state[names[i]] = { idx: -1, ui: { rec: null, radio: null } };
    }
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function prjGetTblRow(o, idx, curr) {
    return $setProp($setObj({}, o), { idx: idx, curr: curr, size: $size2str(o.size, true), noteLen: $size2str(o.noteLen, true) });
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function prjNew() {
    var name = prompt("Name of the new project:");
    if (!name || (name && name.length === 0)) return;
    
    procShow("Creating the project...");
    execCmd(true,
      { cmd: "prj-new", name: encodeURIComponent(name) },
      function (res) {
        data.prj.push(res.prj);
        var t = ui.tbl.prj.tbl;
        t.addRow(prjGetTblRow(res.prj, data.prj.length-1, false));
        Pegasus.Main.tblResort(t);
        
        ui.panel.proc.p.hide();
      },
      null, null, true
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function prjSetBtnsEnabled(e) {
    ui.prj.btnDel.set("disabled", false);
    ui.prj.btnNote.set("disabled", false);
    
    for (var a in apps) {
      apps[a].btnStart.set("disabled", false);
    }
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function prjSetCurr() {
    Pegasus.Main.setCookie(CONST.cookie.appName, "curr-prj-id", getCurrPrj().id);
    afterWinResize();
    ui.panel.proc.p.hide();
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function procShow(msg) {
    ui.panel.proc.p.setHeader(msg);
    ui.panel.proc.p.show();
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function sbarSet() {
    var appCnt = 0;  // number of running apps
    for (var a in apps) {
      if (apps[a].win) appCnt++;
    }
    
    if (appCnt === 0) ui.sbar.inf.innerHTML = "Safe to close this window (0 applications running)";
    else ui.sbar.inf.innerHTML = "Do no close this window (" + appCnt + " " + (appCnt === 1 ? "application" : "applications") + " running)";
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function txtShow(title, msg, txt, edit, note) {
    var u = ui.panel.txt;
    
    if (edit) {
      u.edit.value = txt;
      $hide(u.view);
      $show(u.edit);
    }
    else {
      u.view.innerHTML = txt;
      $hide(u.edit);
      $show(u.view);
    }
    
    if (note) u.p.hideEvent.subscribe(u.fnNoteHide);
    
    u.msg.innerHTML = (msg && msg.length > 0 ? msg + "<br /><br />" : "");
    u.p.setHeader(title);
    
    u.p.show();
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function uiResizeDataTbl(dt, x, y) {
    var regB = ui.region.app.body;
    var regDT = YAHOO.util.Dom.getRegion(dt.getTableEl());
    if (x) {
      var adj = regDT.right > (regB.right - 40);
      if (adj) dt.set("width", (regB.width - regDT.left) + "px");
    }
    if (y) {
      var adj = regDT.bottom > (regB.bottom - 40);
      if (adj) dt.set("height", (regB.height - regDT.top) + "px");
    }
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function updateDBSize(size) {
    data.profile.dbSize = size;
    ui.panel.profile.dbSize.innerHTML = (size / (1024*1024)).toFixed(0) + " MB";
  }
  
  
  // ===================================================================================================================
  // ==[ PUBLIC ]=======================================================================================================
  // ===================================================================================================================
  
  var pub = {
    
    // -----------------------------------------------------------------------------------------------------------------
    UI: {
      Paginator: {
        template: ""
      },
      Tbl: {
        Formatter: {
          date: function (elCell, oRec, oCol, oData) {
            var d = oData;
            elCell.innerHTML = (d.toDateString() + ", " + d.toLocaleTimeString()).replace(/ /g, "&nbsp;");
          }
        },
        
        Sorter: {
          genDate: function (fnGetObj, col) {
            return function(a, b, desc) {
              var da = fnGetObj()[parseInt(a.getData("idx"))][col];
              var db = fnGetObj()[parseInt(b.getData("idx"))][col];
              return YAHOO.util.Sort.compare(da, db, desc);
            };
          }
        }
      }
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    /*
     * Registeres an application. This function should be called right after an application main object has been 
     * instantiated to ensure its proper connectivity with the Pegasus Main application.
     */
    appReg: function (id, win, obj) {
      var a = apps[id];
      a.win = win;
      a.obj = obj;
      //a.btnStart.disabled = true;
      //a.info.innerHTML = "running <a href=\"javascript:Pegasus.Main.appShow('" + id + "');\">show</a> <a href=\"javascript:Pegasus.Main.appStop('" + id + "');\">terminate</a>";
      
      sbarSet();
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    appShow: function (id) {
      var a = apps[id];
      if (a.win) a.win.focus();
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    appStart: function (id) {
      var a = apps[id];
      if (a.win) {
        a.win.focus();
        return;
      }
      
      //a.btnStart.disabled = true;
      //a.info.innerHTML = "starting...";
      window.open(id + ".html?prj-id=" + getCurrPrj().id, "", "fullscreen=1,location=0,menubar=0,titlebar=0,toolbar=0,status=0");
      
      sbarSet();
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    appStop: function (id) {
      var a = apps[id];
      
      if (!a.win) return;
      if (!confirm("Terminate the '" + a.name + "' application?")) return;
      
      a.win.close();
      a.win = null;
      a.obj = null;
      //a.btnStart.disabled = false;
      //a.info.innerHTML = "";
      
      sbarSet();
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    appStopAll: function (msg) {
      for (var appId in apps) {
        var a = apps[appId];
        if (a.win) {
          a.obj.stop(msg);
          a.win = null;
          a.obj = null;
          //a.info.innerHTML = "";
          //a.btnStart.disabled = false;
          
          sbarSet();
        }
      }
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    /*
     * Unregisteres an application. This function should be called when an application is about to be closed.
     */
    appUnreg: function (id) {
      var a = apps[id];
      if (!a.win) return;
      
      a.win = null;
      a.obj = null;
      //a.info.innerHTML = "";
      //a.btnStart.disabled = false;
      
      sbarSet();
    },
    
    
    // -------------------------------------------------------------------------------------------------------------------
    dataTblAddCellMouseInfoEvts: function (tbl, fnOver, info) {
      tbl.on("cellMouseoverEvent", function (oArgs) {
        var tgt = oArgs.target;
        var col = this.getColumn(tgt);
        fnOver(this.getRecord(tgt), col, this.getRecord(tgt).getData(col.key));
      });
      
      tbl.on("cellMouseoutEvent", function (oArgs) {
        info.innerHTML = "";
      });
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    /**
     * Returns the command given by 'spec' by enriching it with the username and the app id. 'spec' is expected to be 
     * an hash table of the command parameters.
     */
    getCmd: function (appId, spec) {
      var P = [];  // key-value pairs
      for (var k in spec) P.push(k + "=" + spec[k]);
      return "username=" + data.profile.username + "&app=" + appId + "&" + P.join("&");
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    getPrjName: function (id) {
      for (var i=0, ni=data.prj.length; i < ni; i++) {
        var p = data.prj[i];
        if (p.id === id) return p.name;
      }
      return null;
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    getServerURI: function () { return CONST.serverURI_L; },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    setCookie: function (app, name, value) {
      Pegasus.util.Cookie.set(app + name, value, CONST.cookie.days);
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    start: function () {
      initProc();
      procShow("Loading (initializing UI)...");
      
      initLogin();
      initMain();
      initProfile();
      initPrj();
      
      initTxtPanel();
      
      autoLogin();
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    tblResort: function (t) {
      t.sortColumn(t.getColumn(t.getState().sortedBy.key), t.getState().sortedBy.dir);
    }
  };
  
  
  // ===================================================================================================================
  // ==[ CONSTRUCT ]====================================================================================================
  // ===================================================================================================================
  
  construct();
  return pub;
}();
