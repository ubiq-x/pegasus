/**
 * Pegasus: Data
 * -------------
 * 
 * DEPENDS ON:
 *   $.js
 *   Validation.js
 */


if (!Pegasus) var Pegasus = {};

Pegasus.Data = function() {
  
  // ===================================================================================================================
  // ==[ PRIVATE ]======================================================================================================
  // ===================================================================================================================
  
  var CONST = {
    appId       : "data",
    cookie      : {
      appName: "Pegasus-Data-"
    },
    err         : {
      unexpected: "An unexpected error has occurred. This might be due to loss of connection or server side error. Please try again. If you are able to connect to the server but the error persists please notify the system administrator (Tomek Loboda, the.snake@gmail.com)."
    },
    panelW      : "900px",
    rCmd        : {
      loadDS: "x <- pegasus.loadDataSet()"
    },
    serverURI_S : "do",
    serverURI_L : location.protocol + "//" + location.host + "/pegasus/do",
    stat        : { alpha: 0.05 },
    time        : { cmdResHide: 10000 },
    var         : {
      type      : { bin: 1, cat: 2, ord: 3, dis: 4, con: 5, str: 6 },
      typeDispS : { 1: "bin", 2: "cat", 3: "ord", 4: "dis", 5: "con", 6: "str" },
      typeDispL : { 1: "Binary", 2: "Categorical", 3: "Ordinal", 4: "Discrete", 5: "Continuous", 6: "String" }
    }
  };
  
  var data = {};
  
  var state = {
    app      : { startUp: true },
    currDS   : { idx: -1, ui: { rec: null, radio: null } },
    currRep  : { idx: -1, ui: { rec: null, radio: null } },
    currVar  : { idx: -1, ui: { rec: null, radio: null } },
    prjId    : -1,
    rInt     : { currIdx: -1, lastCmd: "", ready: true },
    script      : {
      active : false,
      idx    : 0,
      lst    : []
    },
    timer    : { cmdRes : null },
    ui       : {
      tabViewIdx : { prep: 0, stat: 0, bnet: 0 },
      tbarIdx    : 0
    }
  };
  
  var ui = {
    app: { mask: null },
    cmdRes: null,
    main: null,
    
    panel: {
      proc       : { p: null },
      ds         : { p: null, btnDelVar: null, btnDiscret: null, btnDup: null, btnExport: null, btnImport: null, btnMerge: null, btnRepVal: null, btnShowHtml: null, btnShowTxt: null, btnSplit: null, btnSpreadsheet: null, btnStats: null, btnTSAddVar: null, btnTSWide: null },
      dsDup      : { p: null, inpDsName: null, inpCond: null },
      dsDiscret  : { p: null, inpDsName: null, inpSpec: null },
      dsMerge    : { p: null, inpDsName: null, inpIdxVarName: null, tblDSLst: null, spanSize: null, spec: [] },
      dsRepVal   : { p: null, inpSpec: null },
      dsStats    : { p: null, inpDsName: null, spec: null },
      txt        : { p: null, edit: null, msg: null, view: null, fnNoteHide: null },
      varReord   : { p: null },
      varTrans   : { p: null, inpName: null, inpVal: null, lstType: null },
      varType    : { p: null, inpNewName: null, lstNewType: null, txtCurrName: null, txtCurrType: null, txtInfo: null }
    },
    
    region: { app: { body: null } },
    sbar: { currDS: null, currPrj: null, info: null },
    
    tab: {
      prep : {
        data : { populated: false, resized: false, btnShowHtml: null, btnShowTxt: null },
        scr  : { txt: null },
        vars : { populated: false, resized: false, btnBasRep: null, btnDel: null, btnMarkTS: null, btnNew: null, btnReord: null, btnTrans: null, btnType: null }
      },
      stat : {
        rInt : { populated: false, in: null, out: null, btnClearCons: null, btnDelHist: null, btnFullHist: null, btnLoadDS: null, btnResetEng: null, btnSubmit: null },
        rScr : { in: null, btnClear: null, btnExec: null },
        rep  : { populated: false, btnDel: null, btnDelAll: null, btnPDF: null, btnCode: null, btnView: null }
      },
      bnet : {
        tmp : { btnLearnStruct: null }
      }
    },
    
    tabView: {
      prep: null,
      stat: null,
      bnet: null
    },
    
    tbar: { btnGrp: null },
    
    tbl: {
      data : { src: null, tbl: null },
      ds   : { src: null, tbl: null },
      rep  : { src: null, tbl: null },
      vars : { src: null, tbl: null }
    }
  };
  
  var valid = new Pegasus.util.Validation();
  
  
  // -------------------------------------------------------------------------------------------------------------------
  /**
   * Calculates sizes of critical elements and, if necessary, resizes some of them.
   */
  function afterWinResize() {
    var a = ui.region.app;
    a.body = YAHOO.util.Dom.getRegion($("app-bd"));
    if (!a.body.height) a.body.height = a.body.bottom - a.body.top;
    
    $("prep-tabview").childNodes[3].style.height = (a.body.height - 72) + "px";
    $("stat-tabview").childNodes[3].style.height = (a.body.height - 72) + "px";
    $("bnet-tabview").childNodes[3].style.height = (a.body.height - 72) + "px";
    
    // Tabs:
    ui.tab.prep.data.resized = false;
    ui.tab.prep.vars.resized = false;
    refreshTab();
    //uiResizeDataTbl(ui.tbl.ds, false, true);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function bnLearnStruct() {
    var uri = "../pegasus/popups/data/bn.html" + "?mode=learn-struct&ds-id=" + getCurrDS().id;
    window.open(uri, "", "toolbar=0,status=0,scrollbars=1");
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function cmdResShow(msg) {
    if (state.timer.cmdRes) window.clearTimeout(state.timer.cmdRes);
    ui.cmdRes.innerHTML = msg;
    $show(ui.cmdRes);
    state.timer.cmdRes = window.setTimeout(function () { state.timer.cmdRes = null; $hide(ui.cmdRes); }, CONST.time.cmdResHide);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function construct() {}
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function dsBasRep() {
    procShow("Generating basic report...");
    execCmd(true,
      { cmd: "ds-gen-basic-rep", "ds-id": getCurrDS().id },
      function (res) {
        repAdd2Tbl(res.rep);
        ui.panel.proc.p.hide();
      },
      null, null, true
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function dsDel01_curr() {
    var ds = getCurrDS();
    if (!confirm("You are about to delete the active data set ('" + ds.name + "'). This operation is irreversible.")) return;
    procShow("Deleting the current data set...");
    dsDel02(false, { cmd: "ds-del", "ds-ids": ds.id }, "Data set deletion complete: The previously current data set has been deleted");
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function dsDel01_name(fromScr, dsName) {
    if (!fromScr) {
      var dsName = prompt("Data set name:");
    }
    procShow("Deleting the current data set...");
    dsDel02(true, { cmd: "ds-del-name", "prj-id": state.prjId, "ds-name": dsName }, "Data set deletion complete: Data set '" + dsName + "' has been deleted");
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function dsDel01_sel() {
    var SIds = [];  // ids of selected data sets
    $map(function (x) { if (x && x.sel) SIds.push(x.id); }, data.ds);
    var dsCnt = SIds.length;
    
    if (dsCnt < 1) return alert("Please checkmark at least one dataset.");
    
    var ds = getCurrDS();
    if (!confirm("You are about to delete " + (dsCnt == 1 ? " 1 data set" : dsCnt + " data sets") + ". This operation is irreversible.")) return;
    
    procShow((dsCnt == 1 ? "Deleting the checkmarked data set..." : "Deleting checkmarked data sets..."));
    dsDel02(false, { cmd: "ds-del", "ds-ids": SIds.join(",") }, "Data set deletion complete: " + (SIds.length === 1 ? "1 data set has" : SIds.length + " data sets have") + " been deleted");
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function dsDel02(fromScr, cmd, resOkMsg) {
    execCmd(true, cmd,
      function (res) {
        data.ds[state.currDS.idx] = null;
        ui.tbl.ds.tbl.deleteRow(state.currDS.ui.rec);
        
        ui.tbar.btnGrp.check(0);
        ui.tabView.prep.set("activeIndex", 1);
        var t = ui.tbl.vars.tbl;
        if (t.getRecordSet().getLength() > 0) t.deleteRows(0, t.getRecordSet().getLength());
        
        ui.tabView.stat.set("activeIndex", 2);
        t = ui.tbl.rep.tbl;
        if (t.getRecordSet().getLength() > 0) t.deleteRows(0, t.getRecordSet().getLength());
        
        // Tabs:
        ui.tab.prep.vars.populated   = false;
        ui.tab.stat.rInt.populated = false;
        ui.tab.stat.rep.populated = false;
        refreshTab(); 
        
        //notifCheck();
        
        resetCurrState("currDS");
        dsSetBtnsEnabled(false);
        ui.sbar.currDS.innerHTML = "---";
        
        if (resOkMsg) cmdResShow(resOkMsg);
        
        if (fromScr && state.script.active) scriptStep(false);
        else ui.panel.proc.p.hide();
      },
      null, null, true
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function dsDiscret(fromScr, dsNameSrc, dsNameDst, spec) {
    var u = ui.panel.dsDiscret;
    var ds = getCurrDS();
    
    if (!fromScr) {
      if (!u.inpDSName.value) return alert("Data set name cannot be empty.");
      if (!u.inpSpec.value) return alert("Specification cannot be empty.");
      
      var dsNameSrc = ds.name;
      var dsNameDst = u.inpDSName.value;
      var spec      = u.inpSpec.value;
      
      procShow("Discretizing the data set...");
    }
    
    execCmd(false,
      { cmd: "ds-discret", "prj-id": state.prjId, "ds-name-src": dsNameSrc, "ds-name-dst": dsNameDst, spec: spec },
      function (res) {
        data.ds.push(res.ds);
        var t = ui.tbl.ds.tbl;
        t.addRow(tblRowGet_ds(res.ds, data.ds.length-1, false));
        Pegasus.Main.tblResort(t);
        
        cmdResShow("Data set discretization complete: Data set '" + dsNameSrc + "' has been discretized and saved as '" + dsNameDst + "')");
        
        if (state.script.active) scriptStep(false);
        else {
          u.p.hide();
          ui.panel.proc.p.hide();
        }
      },
      null, null, true
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function dsDup(fromScr, dsNameSrc, dsNameDst, cond) {
    var u = ui.panel.dsDup;
    var ds = getCurrDS();
    
    if (!fromScr) {
      if (!u.inpDSName.value) return alert("Data set name cannot be empty.");
      
      var dsNameSrc = ds.name;
      var dsNameDst = u.inpDSName.value;
      var cond      = u.inpCond.value;
      
      procShow("Duplicating the data set...");
    }
    
    execCmd(false,
      { cmd: "ds-dup", "prj-id": state.prjId, "ds-name-src": dsNameSrc, "ds-name-dst": dsNameDst, cond: cond },
      function (res) {
        data.ds.push(res.ds);
        var t = ui.tbl.ds.tbl;
        t.addRow(tblRowGet_ds(res.ds, data.ds.length-1, false));
        Pegasus.Main.tblResort(t);
        
        cmdResShow("Data set duplication complete: Data set '" + dsNameSrc + "' has been saved as '" + dsNameDst + "')");
        
        if (state.script.active) scriptStep(false);
        else {
          u.p.hide();
          ui.panel.proc.p.hide();
        }
      },
      null, null, true
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function dsExport(fromScr, dsName, filename, format, delimVar, delimObs, missVal, zip) {
    if (!fromScr) {
      var dsName   = getCurrDS().name;
      var format   = "txt";
      var delimVar = "tab";
      var delimObs = "new-line";
      var missVal  = ".";
      var zip      = 0;
      
      var filename = prompt("Filename:");
      if (!filename || (filename && filename.length === 0)) return;
      
      procShow("Exporting the data set...");
    }
    
    execCmd(true,
      { cmd: "ds-exp", "prj-id": state.prjId, "ds-name": dsName, filename: filename, format: format, "delim-var": delimVar, "delim-obs": delimObs, "miss-val": missVal, zip: zip },
      function (res) {
        cmdResShow("Data set export complete: Data set has been saved in the file '" + filename + "'");
        
        if (state.script.active) scriptStep(false);
        else ui.panel.proc.p.hide();
      },
      null, null, true
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function dsDelVar(fromScr, dsName, spec) {
    var ds = getCurrDS();
    
    if (!fromScr) {
      var spec = prompt("Specification:");
      if (!spec) return;
      
      var dsName = ds.name;
      
      procShow("Deleting variables of the data set...");
    }
    
    execCmd(false,
      { cmd: "ds-del-var", "prj-id": state.prjId, "ds-name": dsName, spec: spec },
      function (res) {
        cmdResShow("Variables deletion complete: Provided " + res.inf.varCnt + (res.inf.varCnt === 1 ? " variable" : " variables") + ", " + res.inf.varDelCnt + " deleted");
        
        if (state.script.active) scriptStep(false);
        else ui.panel.proc.p.hide();
      },
      null, null, true
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function dsImport() {
    //if (!confirm("You are about to request an execution of the import routine. If this operation succeeds, the content of the active data set ('" + getCurrDS().name + "') will be substitued with those content of the import file. All existing reports will be deleted as well. This operation is irreversible.\n\nNOTE: The history of R commands will not be affected.") || prompt("Type 'IMPORT' below to confirm the import of the active experiment:") !== "IMPORT") return;
    
    var file = prompt("Path to the text file:", "/tmp/ds.txt");
    if (!file || file.length === 0) return;
    
    procShow("Importing data...");
    var idx = state.currDS.idx;
    execCmd(true,
      { cmd: "ds-imp", "ds-id": getCurrDS().id, file: file, "miss-val": "." },
      function (res) {
        data.ds[idx] = res.ds;
        dsUpdatePanel(res.ds);
        dsSetCurr(false);
        cmdResProc(res);
        
        ui.panel.proc.p.hide();
      },
      null, null, true
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  /**
   * Initialize and show the panel.
   */
  function dsMerge01() {
    var S = $filter(function (x) { return (x && x.sel); }, data.ds);  // selected data sets
    var dsCnt = S.length;
    
    if (dsCnt < 2) return alert("Please checkmark at least two datasets.");
    
    var u = ui.panel.dsMerge;
    
    u.spec = [];
    $removeChildren(u.tblDSLst);
    u.inpDSName.value = "";
    u.inpIdxVarName.value = "";
    
    var dsIdx = 0;
    var size = 0;
    $map(
      function (x) {
        var tr = $$("tr", u.tblDSLst);
        var td01 = $$("td", tr, null, null, "<b>" + x.name + "</b>");
        var td02 = $$("td", tr);
        
        var comIdxVar = $$("select", td02);
        for (var i = 0; i < dsCnt; i++) {
          $setAttr($$("option", comIdxVar, null, null, "" + i), { value: i });
        }
        comIdxVar.selectedIndex = dsIdx;
        comIdxVar.onchange = function (dsIdx) {
          return function () {
            ui.panel.dsMerge.spec[dsIdx].idx = this.selectedIndex;
          }
        }(dsIdx);
        
        u.spec.push({ name: x.name, idx: dsIdx });
        
        dsIdx++;
        size += x.size;
      },
      S
    );
    
    u.spanSize.innerHTML = $size2str(size);
    
    u.p.show();
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  /**
   * Request the merge.
   */
  function dsMerge02(fromScr, dsResName, idxVarName, spec) {
    if (!fromScr) {
      var u = ui.panel.dsMerge;
      
      if (!u.inpDSName.value) return alert("Data set name cannot be empty.");
      if (!u.inpIdxVarName.value && !confirm("By leaving the 'Index variable name' field empty you are opting to skip adding that variable to the resulting data set.  Is this intentional?")) return;
      
      var dsResName  = u.inpDSName.value;
      var idxVarName = u.inpIdxVarName.value;
      var spec       = $map(function (x) { return $enc(x.name) + ":" + x.idx }, u.spec).join(",");
      
      procShow("Merging data sets...");
    }
    
    execCmd(true,
      { cmd: "ds-merge", "prj-id": state.prjId, "ds-name": dsResName, "idx-var-name": idxVarName, "spec": spec },
      function (res) {
        data.ds.push(res.ds);
        var t = ui.tbl.ds.tbl;
        t.addRow(tblRowGet_ds(res.ds, data.ds.length-1, false));
        Pegasus.Main.tblResort(t);
        
        if (state.script.active) scriptStep(false);
        else {
          ui.panel.dsMerge.p.hide();
          ui.panel.proc.p.hide();
        }
      },
      null, null, true
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function dsNew() {
    var name = prompt("Name of the new data set:");
    if (!name || (name && name.length === 0)) return;
    
    procShow("Creating the data set...");
    execCmd(true,
      { cmd: "ds-new", "prj-id": state.prjId, name: encodeURIComponent(name) },
      function (res) {
        data.ds.push(res.ds);
        var t = ui.tbl.ds.tbl;
        t.addRow(tblRowGet_ds(res.ds, data.ds.length-1, false));
        Pegasus.Main.tblResort(t);
        
        ui.panel.proc.p.hide();
      },
      null, null, true
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function dsRepVal(fromScr, dsNameSrc, dsNameDst, spec) {
    var u = ui.panel.dsRepVal;
    var ds = getCurrDS();
    
    if (!fromScr) {
      if (!u.inpDSName.value) return alert("Data set name cannot be empty.");
      if (!u.inpSpec.value) return alert("Specification cannot be empty.");
      
      var dsNameSrc = ds.name;
      var dsNameDst = u.inpDSName.value;
      var spec      = u.inpSpec.value;
      
      procShow("Replacing values...");
    }
    
    execCmd(true,
      { cmd: "ds-rep-val", "prj-id": state.prjId, "ds-name-src": dsNameSrc, "ds-name-dst": dsNameDst, spec: spec },
      function (res) {
        data.ds.push(res.ds);
        var t = ui.tbl.ds.tbl;
        t.addRow(tblRowGet_ds(res.ds, data.ds.length-1, false));
        Pegasus.Main.tblResort(t);
        
        cmdResShow("Replacing values complete: Data set '" + dsNameSrc + "' has been saved as '" + dsNameDst + "')");
        
        if (state.script.active) scriptStep(false);
        else {
          u.p.hide();
          ui.panel.proc.p.hide();
        }
      },
      null, null, true
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function dsSetBtnsEnabled(e) {
    var u = ui.panel.ds;
    
    // Button bar:
    u.btnShowTxt.set  ("disabled", !e);
    u.btnShowHtml.set ("disabled", !e);
    u.btnExport.set   ("disabled", !e);
    u.btnImport.set   ("disabled", !e);
    u.btnDup.set      ("disabled", !e);
    u.btnDelVar.set   ("disabled", !e);
    u.btnRepVal.set   ("disabled", !e);
    u.btnDiscret.set  ("disabled", !e);
    u.btnSplit.set    ("disabled", !e);
    u.btnStats.set    ("disabled", !e);
    u.btnTSAddVar.set ("disabled", !e);
    u.btnTSWide.set   ("disabled", !e);
    
    // Panel buttons:
    u.p.getButtons()[1].set("disabled", !e);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  /**
   * 'persist' indicates if the data set from the last session is being loaded or the user has just changed the active 
   * data set or the import routine has finished and requested the newly imported data set be set active. Only the 
   * explicit user action should be persisted.
   * 
   * If 'fnAfter' is provided, that function should hide the processing panel by making the following call:
   * 
   *    ui.panel.proc.p.hide();
   * 
   */
  function dsSetCurr(persist, fnAfter) {
    procShow("Loading the data set...");
    
    // Persist action:
    if (persist) Pegasus.Main.setCookie(CONST.cookie.appName, "curr-ds-id", getCurrDS().id);
    
    // Tabs:
    ui.tab.prep.data.populated = false;
    ui.tab.prep.vars.populated = false;
    ui.tab.stat.rInt.populated = false;
    ui.tab.stat.rep.populated  = false;
    
    // Other:
    //notifCheck();
    dsSetBtnsEnabled(true);
    refreshTab();
    afterWinResize();
    resetCurrState(["currVar"]);
    
    state.rInt.currIdx = getCurrDS().rInt.recent.length;
    
    if (fnAfter) fnAfter();
    else ui.panel.proc.p.hide();
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function dsShow(format) {
    var ds = getCurrDS();
    var win = window.open(CONST.serverURI_L + "?" + getCmd({ cmd: "ds-exp", "prj-id": state.prjId, "ds-name": ds.name, format: format, "delim-var": "tab", "delim-obs": "new-line", "miss-val": ".", zip: 0 }), "", "toolbar=1,status=0,scrollbars=1");
    window.setTimeout(function () { win.document.title = "Data set '" + ds.name + "'" }, 500);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function dsSplit() {
    var spec = $trim(prompt("Split proportions in percents (e.g., '10,70,20'):"));
    if (!spec) return;
    
    procShow("Splitting the data set...");
    execCmd(true,
      { cmd: "ds-split", "ds-id": getCurrDS().id, spec: spec },
      function (res) {
        /*
        var DS = data.exp[state.currExp.idx].ds;
        var newDS = res.DS;
        for (var ds in newDS) {
          ds.idx = DS.length;
          DS.push(ds);
          ui.tbl.ds.tbl.addRow(ds);
        }
        */
        alert("Refresh the page to see the new data sets");
        
        ui.panel.proc.p.hide();
      },
      null, null, true
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function dsSpreadsheet() {
    return alert("Implementation update necessary.");
    
    var S = $filter(function (x) { return (x && x.sel); }, data.exp[state.currExp.idx].ds);  // selected data sets
    
    if (S.length < 2) return alert("Please checkmark at least two datasets.");
    if ($filter(function (x) { return x.name.length === 0; }, S).length > 0) return alert("All checkmarked datasets need to have a name.");
    if ($filter(function (x) { return x.name.toLowerCase() === "comp" || x.name.toLowerCase() === "stat"; }, S).length > 0) return alert("The names 'Comp' and 'Stat' are reserved and cannot be used for names of datasets.");
    
    // Check for name duplicates:
    var names = {};  // stores names to compare against while going through the results below
    for (var i=0, ni=S.length; i < ni; i++) {
      var s = S[i];
      if (names[s.name]) return alert("Names of datasets whithin a spreadsheet need to be unique.");
      names[s.name] = true;
    }
    
    // Execute command:
    var ids = $map(function (x) { return x.id; }, S).join(",");
    //$("file-dl-form").setAttribute("action", CONST.serverURI_S + "?" + getCmd({ cmd: "gen-spreadsheet", "an-res-ids": ids }));
    //$("file-dl-form").submit();
    window.open("/pegasus-gaze/servlet/main.Do?" + getCmd({ cmd: "gen-spreadsheet", "ds-ids": ids }), "", "toolbar=0,status=0");
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  // Sample specification: r,sub,trial,stim,nevt,em_wlen;em_sfd,em_gd;min,max,stddev
  function dsStats(fromScr, dsNameSrc, dsNameDst, spec, sep) {
    var u = ui.panel.dsStats;
    var ds = getCurrDS();
    
    if (!fromScr) {
      if (!u.inpDSName.value) return alert("Data set name cannot be empty.");
      if (!u.inpSpec.value) return alert("Specification cannot be empty.");
      
      var dsNameSrc = ds.name;
      var dsNameDst = u.inpDSName.value;
      var spec      = u.inpSpec.value;
      var sep       = "-";
      
      procShow("Generating statistics data set...");
    }
    
    execCmd(true,
      { cmd: "ds-gen-stats", "prj-id": state.prjId, "ds-name-src": dsNameSrc, "ds-name-dst": dsNameDst, spec: spec, sep: sep },
      function (res) {
        data.ds.push(res.ds);
        var t = ui.tbl.ds.tbl;
        t.addRow(tblRowGet_ds(res.ds, data.ds.length-1, false));
        Pegasus.Main.tblResort(t);
        
        cmdResShow("Generating statistics data set complete: Data set '" + dsNameDst + "' has been created");
        
        if (state.script.active) scriptStep(false);
        else {
          u.p.hide();
          ui.panel.proc.p.hide();
        }
      },
      null, null, true
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  // Sample specification: i;sub,trial,stim,em_nevt;em_nevt
  function dsTSAddVar(fromScr, dsName, spec) {
    var ds = getCurrDS();
    
    if (!fromScr) {
      if (ds.tsVar && !confirm("The selected data set already contains a time series variable. If you continuing, a new time series variable will be added and the old one will become a regular variable.")) return;
      
      var dsName = ds.name;
      
      var spec = prompt("Specification ('<ts-var>;<resetting vars>;<after var>', e.g., 't_i;sub,trial,stim;stim'; no single quotes):");
      if (!spec || spec.length === 0) return;
      
      procShow("Adding a new time series variable...");
    }
    
    execCmd(true,
      { cmd: "ds-ts-add-var", "prj-id": state.prjId, "ds-name": dsName, spec: spec },
      function (res) {
        /*
        data.ds.push(res.ds);
        var t = ui.tbl.ds.tbl;
        t.addRow(tblRowGet_ds(res.ds, data.ds.length-1, false));
        Pegasus.Main.tblResort(t);
        */
        
        cmdResShow("Adding time-series variable complete");
        
        if (state.script.active) scriptStep(false);
        else ui.panel.proc.p.hide();
      },
      null, null, true
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function dsTSWide(fromScr, dsNameSrc, dsNameDst) {
    var ds = getCurrDS();
    
    if (!fromScr) {
      if (!ds.tsVar) return alert("No time-series variable has been defined in the current data set.");
      
      var dsNameSrc = ds.name;
      var dsNameDst = prompt("New data set name:");
      
      if (!dsNameDst) return;
      
      procShow("Generating a new data set...");
    }
    
    execCmd(true,
      { cmd: "ds-ts-2-wide", "prj-id": state.prjId, "ds-name-src": dsNameSrc, "ds-name-dst": dsNameDst, "ts-len-var": "ts_len", "idx-start": 1, "idx-first": 0, "idx-inc": 0, "idx-pre": "_" },
      function (res) {
        data.ds.push(res.ds);
        var t = ui.tbl.ds.tbl;
        t.addRow(tblRowGet_ds(res.ds, data.ds.length-1, false));
        Pegasus.Main.tblResort(t);
        
        cmdResShow("Generating wide time-series format data set complete: Data set '" + dsNameSrc + "' has been reformatted into data set '" + dsNameDst + "'");
        
        if (state.script.active) scriptStep(false);
        else ui.panel.proc.p.hide();
      },
      null, null, true
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function dsVarReorder() {
    var spec = prompt("Specify the new order:", $map(function (x) { return x.name; }, getCurrDS().var));
    if (!spec || spec.length === 0) return;
    
    procShow("Reordering variables...");
    execCmd(true,
      { cmd: "ds-var-reorder", "ds-id": getCurrDS().id, spec: spec },
      function (res) {
        ui.panel.proc.p.hide();
      },
      null, null, true
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function dsUpdatePanel(ds) {
    ds.size = $size2str(ds.size, true);
    ds.idx = state.currDS.idx;
    ds.curr = true;
    ui.tbl.ds.tbl.updateRow(state.currDS.idx, ds);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  /**
   * Executes command and handles a part of the response. To wit, server error or default actions for true or false 
   * outcome if the corresponding handler functions have not been specified. 'params' is a hash of key-value pairs.
   */
  function execCmd(get, params, fnOkTrue, fnOkFalse, fnErr, minHead) {
    var cmd = getCmd(params);
    
    $call({
      method  : (get ? "GET" : "POST"),
      uri     : (get ? CONST.serverURI_S + "?" + cmd : CONST.serverURI_S),
      params  : (get ? null : cmd),
      eval    : true,
      minHead : minHead,
      
      fnOk: function (res) {
        if (res.outcome) {
          execCmd_procRes(res);
          if (fnOkTrue) fnOkTrue(res);
          else ui.panel.proc.p.hide();
        }
        else {
          if (fnOkFalse) fnOkFalse(res)
          else {
            alert(res.msg);
            ui.panel.proc.p.hide();
          };
        }
      },
      
      fnErr: function (res) {
        if (fnErr) fnErr(res);
        else {
          alert(CONST.err.unexpected);
          ui.panel.proc.p.hide();
        }
      }
    });
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function execCmd_procRes(res) {
    if (res && res.size && res.size.ds) {
      var ds = getCurrDS();
      ds.size = res.size.ds;
      
      var o = { idx: state.currDS.idx, curr: true };
      $setObj(o, ds);
      o.size = $size2str(ds.size, true);
      
      ui.tbl.ds.tbl.updateRow(state.currDS.idx, o);
    }
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  /**
   * Generates the YUI data source and table.
   * 
   * ARGS:
   *   ui       : the appropriate 'ui' object's property
   *   cont     : the container
   *   fields   : array of fields
   *   cols     : array of columns
   *   events   : array of events
   *   settings : array of settings
   *   panel    : provide for data tables that should be editable while displayed in a modal panel
   *   scroll   : flag
   */
  function genTbl(appId, ui, cont, fields, cols, events, settings, panel, scroll) {
    // Source:
    var src = new YAHOO.util.DataSource();
    src.responseType   = YAHOO.util.DataSource.TYPE_JSARRAY;
    src.responseSchema = { fields: fields };
    ui.src = src;
    
    // Table:
    var tbl = new YAHOO.widget.ScrollingDataTable(cont, cols, src, $setObj({ scrollable: scroll }, settings));
    if (!events.cellClickEvent) tbl.on("cellClickEvent", tbl.onEventShowCellEditor);
    for (var e in events) tbl.on(e, events[e]);
    
    tbl.render();
    
    if (panel) {
      tbl.doBeforeShowCellEditor = function (cellEditor) {
        var el = cellEditor.getContainerEl();
        if (el.parentNode === document.body) {
          panel.body.appendChild(el);
          
          cellEditor.subscribe("showEvent", 
            function () {
              var xy = YAHOO.util.Dom.getXY(this.getTdEl());
              YAHOO.util.Dom.setXY(this.getContainerEl(), xy);
            }
          );
        }
        
        return true;
      };
    }
    
    ui.tbl = tbl;
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function getCurrDS()  { return data.ds[state.currDS.idx]; }
  function getCurrRep() { return getCurrDS().rep[state.currRep.idx]; }
  function getCurrVar() { return getCurrDS().var[state.currVar.idx]; }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function getCmd(spec) { return Pegasus.Main.getCmd(CONST.appId, spec); }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  /**
   * Returns a function that can be used as an async submitter for a DataTable inline editor. That function will take
   * care of requesting a single attribute updated.
   * 
   * IN:
   *   aFnValid: an array of validator functions; each takes one argument and returns a boolean
   *   ...
   */
  function getSetAttrFn(aFnValid, fnAfterSuccess, fnGetCmd) {
    return function (cb, newVal) {
      if ((this.value === null && newVal === "") || newVal == this.value) { cb(false,newVal); return; }
      
      // Validate:
      if (aFnValid.length > 0) {
        var isValid = $lfold(
          function (a,b) { return a && b; },
          $map(function (x) { return x(newVal); }, aFnValid),
          true
        );
        if (!isValid) { cb(false,newVal); return; }
      }
      
      // Persist server-side:
      procShow("Saving...");
      var that = this;
      execCmd(true, fnGetCmd(this, newVal),
        function (res) {
          fnAfterSuccess(that, newVal, res);
          cb(true, newVal);
          ui.panel.proc.p.hide();
        },
        function (res) {
          alert(res.msg);
          cb(false, newVal);
          ui.panel.proc.p.hide();
        },
        function (res) {
          alert(CONST.err.unexpected);
          cb(false, newVal);
          ui.panel.proc.p.hide();
        },
        true
      );
    };
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function getVal(v,n) { return (isNaN(v) ? "." : v.toFixed(n)); }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initApp() {
    procShow("Retrieving data sets...");
    
    var currDSId = parseInt(Pegasus.util.Cookie.get(CONST.cookie.appName + "curr-ds-id"));
    if (isNaN(currDSId)) currDSId = "";
    
    execCmd(true, { cmd: "ds-get-lst", "prj-id": state.prjId, "ds-id": currDSId }, initApp_cb, null, null, true);
  }
  
  
  // ----^----
  function initApp_cb(res) {
    if (!res.outcome) {
      alert(res.msg);
      ui.panel.proc.p.hide();
      return;
    }
    
    procShow("Processing data sets...");
    
    data.ds = [];
    $setObj(data.ds, res.ds);  // store the data received
    
    var currDSId = parseInt(Pegasus.util.Cookie.get(CONST.cookie.appName + "curr-ds-id"));
    var currDSIdx = -1;
    
    // Tabs:
    ui.tabView.prep.set("activeIndex", state.ui.tabViewIdx.prep);
    ui.tabView.stat.set("activeIndex", state.ui.tabViewIdx.stat);
    ui.tabView.bnet.set("activeIndex", state.ui.tabViewIdx.bnet);
    
    ui.tbar.btnGrp.check(state.ui.tbarIdx);
    ui.tbar.btnGrp.getButtons()[state.ui.tbarIdx].fireEvent("click");
    
    // Data sets:
    var R = [];  // rows
    for (var i=0, ni=data.ds.length; i < ni; i++) {
      var ds = data.ds[i];
      if (ds.id === currDSId) currDSIdx = i;
      R.push(tblRowGet_ds(ds, i, (ds.id === currDSId)));
    }
    
    var t = ui.tbl.ds.tbl;
    t.addRows(R);
    
    if (currDSIdx != -1) {
      var row = t.getRow(currDSIdx);
      var radio = t.getFirstTdEl(t.getLastTrEl()).firstChild.firstChild;
      t.selectRow(radio);
      
      state.currDS.ui.radio = t.getRecord(radio);
      state.currDS.ui.rec   = t.getRecord(radio);
      state.currDS.idx      = currDSIdx;
      
      ui.panel.ds.p.getButtons()[1].set("disabled", false);
      ui.panel.ds.p.getButtons()[2].set("disabled", false);
      ui.sbar.currDS.innerHTML = ds.name;
    }
    
    Pegasus.Main.tblResort(t);
    
    if (currDSIdx !== -1) {
      if (!data.ds[state.currDS.idx].loaded) {  // we should never be here -- I should remove this check
        state.currDS = { idx: -1, ui: { rec: null, radio: null } };
      }
      
      dsSetCurr(false, null);
      state.app.startUp = false;
      afterWinResize();
    }
    else {
      state.app.startUp = false;
      afterWinResize();
      ui.panel.ds.p.show();
    }
    
    $hide(ui.app.mask);
    ui.panel.proc.p.hide();
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initBNetTmp() {
    // Button bar:
    ui.tab.bnet.tmp.btnShow = new YAHOO.widget.Button("bnet-tmp-btn-learn", { onclick: { fn: bnLearnStruct } });
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initDSPanel() {
    // Panel -- button event handlers:
    var fnClose = function (e) {
      if (state.currDS.idx !== -1) this.hide();
      else alert("Please activate a data set first. If neccessary please create one.");
      
      if (!getCurrDS().loaded) {
        procShow("Retrieving the data set...");
        $call({
          method  : "GET",
          uri     : CONST.serverURI_S + "?" + getCmd({ cmd: "ds-get", id: getCurrDS().id}),
          params  : null,
          eval    : true,
          minHead : true,
          
          fnOk: function (res) {
            if (res.outcome) {
              data.ds[state.currDS.idx] = res.ds;
              dsSetCurr(true, null);
            }
            else {
              alert(res.msg);
              ui.panel.proc.p.hide();
            }
          },
          
          fnErr: function (res) {
            ui.panel.login.p.show();
            ui.panel.proc.p.hide();
          }
        });
      }
      else dsSetCurr(true, null);
    };
    
    // Panel:
    var panel = new YAHOO.widget.Dialog("panel-ds", {
      buttons             : [
        { text: "New", handler: dsNew },
        { text: "Delete", handler: dsDel01_curr },
        { text: "Delete checkmarked", handler: dsDel01_sel },
        { text: "Close", handler: fnClose, isDefault: true }
      ],
      close               : false,
      constraintoviewport : true,
      draggable           : true,
      dragOnly            : true,
      fixedcenter         : true,
      modal               : true,
      visible             : false
    });
    panel.render();
    ui.panel.ds.p = panel;
    
    // Button bar:
    ui.panel.ds.btnShowTxt     = new YAHOO.widget.Button("panel-ds-btn-show-txt",    { onclick: { fn: function (e) { dsShow("txt");  } },              disabled: true  });
    ui.panel.ds.btnShowHtml    = new YAHOO.widget.Button("panel-ds-btn-show-html",   { onclick: { fn: function (e) { dsShow("html"); } },              disabled: true  });
    ui.panel.ds.btnExport      = new YAHOO.widget.Button("panel-ds-btn-export",      { onclick: { fn: function (e) { dsExport(false); } },             disabled: true  });
    ui.panel.ds.btnImport      = new YAHOO.widget.Button("panel-ds-btn-import",      { onclick: { fn: dsImport },                                      disabled: true  });
    ui.panel.ds.btnDup         = new YAHOO.widget.Button("panel-ds-btn-dup",         { onclick: { fn: function (e) { ui.panel.dsDup.p.show(); } },     disabled: true  });
    ui.panel.ds.btnDelVar      = new YAHOO.widget.Button("panel-ds-btn-del-var",     { onclick: { fn: function (e) { dsDelVar(false); } },             disabled: true  });
    ui.panel.ds.btnRepVal      = new YAHOO.widget.Button("panel-ds-btn-rep-val",     { onclick: { fn: function (e) { ui.panel.dsRepVal.p.show(); } },  disabled: true  });
    ui.panel.ds.btnDiscret     = new YAHOO.widget.Button("panel-ds-btn-discret",     { onclick: { fn: function (e) { ui.panel.dsDiscret.p.show(); } }, disabled: true  });
    ui.panel.ds.btnMerge       = new YAHOO.widget.Button("panel-ds-btn-merge",       { onclick: { fn: dsMerge01 },                                     disabled: false });
    ui.panel.ds.btnSplit       = new YAHOO.widget.Button("panel-ds-btn-split",       { onclick: { fn: dsSplit },                                       disabled: true  });
    ui.panel.ds.btnStats       = new YAHOO.widget.Button("panel-ds-btn-stats",       { onclick: { fn: function (e) { ui.panel.dsStats.p.show(); } },   disabled: true  });
    ui.panel.ds.btnTSAddVar    = new YAHOO.widget.Button("panel-ds-btn-ts-add-var",  { onclick: { fn: function (e) { dsTSAddVar(false); } },           disabled: true  });
    ui.panel.ds.btnTSWide      = new YAHOO.widget.Button("panel-ds-btn-ts-wide",     { onclick: { fn: function (e) { dsTSWide(false); } },             disabled: true  });
    ui.panel.ds.btnSpreadsheet = new YAHOO.widget.Button("panel-ds-btn-spreadsheet", { onclick: { fn: dsSpreadsheet },                                 disabled: false });
    
    dsSetBtnsEnabled(false);
    
    // Data table -- submitters:
    var fnGetCmd = function (attr) {
      return function (o,newVal) {
        return { cmd: "attr-set", "obj-name": "ds", "obj-human-name": "data set", "obj-id": o.getRecord().getData("id"), "attr-name": attr, "attr-val": $enc(newVal) };
      };
    };
    
    var fnSetName = getSetAttrFn([valid.strNonEmpty], function (o,newVal,res) { data.ds[o.getRecord().getData("idx")].name = newVal; if (o.getRecord().getData("idx") === state.currDS.idx) { ui.sbar.currDS.innerHTML = newVal; } }, fnGetCmd("name"));
    var fnSetMemo = getSetAttrFn([],                  function (o,newVal,res) { data.ds[o.getRecord().getData("idx")].memo = newVal; }, fnGetCmd("memo"));
    
    // Data table -- columns:
    var cols = [
      { key: "curr", label: "", formatter: "radio" },
      { key: "sel", label: "", formatter: "checkbox" },
      { key: "app", label: "App", sortable: true },
      { key: "name", label: "Name", sortable: true, editor: new YAHOO.widget.TextboxCellEditor({ asyncSubmitter: fnSetName }), width: 180, className: "name" },
      { key: "created", label: "Created", formatter: Pegasus.Main.UI.Tbl.Formatter.date, sortable: true, sortOptions: { sortFunction: Pegasus.Main.UI.Tbl.Sorter.genDate(function () { return data.ds; }, "created") } },
      { key: "varCnt", label: "Var", formatter: YAHOO.widget.DataTable.formatNumber, sortable: true },
      { key: "obsCnt", label: "Obs", formatter: YAHOO.widget.DataTable.formatNumber, sortable: true },
      { key: "ts", label: "TS", formatter: YAHOO.widget.DataTable.formatNumber, sortable: true },
      { key: "size", label: "Size", sortable: true },
      { key: "calcTime", label: "Calc", sortable: true },
      { key: "noteLen", label: "Note", sortable: true },
      { key: "memo", label: "Memo", sortable: true, editor: new YAHOO.widget.TextareaCellEditor({ asyncSubmitter: fnSetMemo }), width: 260 }
    ];
    
    // Data table -- event handlers:
    var onRadioClickEvent = function (oArgs) {
      var radio = oArgs.target;
      var rec = this.getRecord(radio);
      var newIdx = parseInt(rec.getData("idx"));
      
      if (newIdx === state.currDS.idx) return;
      
      state.currDS.ui.rec = rec;
      state.currDS.idx = newIdx;
      
      if (state.currDS.ui.radio) {
        state.currDS.ui.radio.setData("curr", false);
      }
      state.currDS.ui.radio = rec;
      rec.setData("curr", true);
      
      ui.tbl.ds.tbl.unselectAllRows();
      ui.tbl.ds.tbl.selectRow(radio);
      
      dsSetBtnsEnabled(true);
      ui.sbar.currDS.innerHTML = rec.getData("name");
      
      /*
      if (!getCurrDS().loaded) {
        procShow("Retrieving the data set...");
        $call({
          method  : "GET",
          uri     : CONST.serverURI_S + "?" + getCmd({ cmd: "ds-get", id: getCurrDS().id}),
          params  : null,
          eval    : true,
          minHead : true,
          
          fnOk: function (res) {
            if (res.outcome) {
              data.ds[state.currDS.idx] = res.ds;
              dsSetCurr(true, null);
            }
            else {
              alert(res.msg);
              ui.panel.proc.p.hide();
            }
          },
          
          fnErr: function (res) {
            ui.panel.login.p.show();
            ui.panel.proc.p.hide();
          }
        });
      }
      else dsSetCurr(true, null);
      */
    };
    
    var onCheckboxClickEvent = function (oArgs) {
      var cbox = oArgs.target;
      var rec = this.getRecord(cbox);
      var idx = parseInt(rec.getData("idx"));
      
      rec.setData("sel", cbox.checked);
      data.ds[idx].sel = cbox.checked;
    };
    
    // Data table:
    genTbl(CONST.appId, ui.tbl.ds, $("panel-ds-dt"), ["idx","curr","sel","app","name","created","varCnt","obsCnt","ts","size","calcTime","noteLen","memo"], cols, { radioClickEvent: onRadioClickEvent, checkboxClickEvent: onCheckboxClickEvent }, { sortedBy: { key:"created", dir:"desc" }, height: "500px" }, panel, true);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initDSDupPanel() {
    var u = ui.panel.dsDup;
    
    // Panel -- controls:
    u.inpDSName = $("panel-ds-dup-ds-name");
    u.inpCond = $("panel-ds-dup-cond");
    
    // Panel:
    var panel = new YAHOO.widget.Dialog("panel-ds-dup", {
      buttons             : [
        { text: "Dup",   handler: function (e) { dsDup(false); } },
        { text: "Close", handler: function (e) { u.p.hide(); }, isDefault: true }
      ],
      close               : false,
      constraintoviewport : true,
      draggable           : true,
      dragOnly            : true,
      fixedcenter         : true,
      modal               : true,
      visible             : false
    });
    
    panel.render();
    u.p = panel;
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initDSDiscretPanel() {
    var u = ui.panel.dsDiscret;
    
    // Panel -- controls:
    u.inpDSName = $("panel-ds-discret-ds-name");
    u.inpSpec = $("panel-ds-discret-spec");
    
    // Panel:
    var panel = new YAHOO.widget.Dialog("panel-ds-discret", {
      buttons             : [
        { text: "Discretize", handler: function (e) { dsDiscret(false); } },
        { text: "Close",      handler: function (e) { u.p.hide(); }, isDefault: true }
      ],
      close               : false,
      constraintoviewport : true,
      draggable           : true,
      dragOnly            : true,
      fixedcenter         : true,
      modal               : true,
      visible             : false
    });
    
    panel.render();
    u.p = panel;
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initDSMergePanel() {
    var u = ui.panel.dsMerge;
    
    // Panel -- controls:
    u.inpDSName = $("panel-ds-merge-ds-name");
    u.inpIdxVarName = $("panel-ds-merge-idx-var-name");
    u.tblDSLst = $("panel-ds-merge-ds-lst-tbl");
    u.spanSize = $("panel-ds-merge-size");
    
    // Panel:
    var panel = new YAHOO.widget.Dialog("panel-ds-merge", {
      buttons             : [
        { text: "Merge", handler: function (e) { dsMerge02(false); } },
        { text: "Close", handler: function (e) { ui.panel.dsMerge.p.hide(); }, isDefault: true }
      ],
      close               : false,
      constraintoviewport : true,
      draggable           : true,
      dragOnly            : true,
      fixedcenter         : true,
      modal               : true,
      visible             : false
    });
    
    panel.showEvent.subscribe(
      function () {
      }
    );
    
    panel.render();
    u.p = panel;
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initDSRepValPanel() {
    var u = ui.panel.dsRepVal;
    
    // Panel -- controls:
    u.inpDSName = $("panel-ds-rep-val-ds-name");
    u.inpSpec = $("panel-ds-rep-val-spec");
    
    // Panel:
    var panel = new YAHOO.widget.Dialog("panel-ds-rep-val", {
      buttons             : [
        { text: "Replace", handler: function (e) { dsRepVal(false); } },
        { text: "Close",   handler: function (e) { u.p.hide(); }, isDefault: true }
      ],
      close               : false,
      constraintoviewport : true,
      draggable           : true,
      dragOnly            : true,
      fixedcenter         : true,
      modal               : true,
      visible             : false
    });
    
    panel.render();
    u.p = panel;
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initDSStatsPanel() {
    var u = ui.panel.dsStats;
    
    // Panel -- controls:
    u.inpDSName = $("panel-ds-stats-ds-name");
    u.inpSpec = $("panel-ds-stats-spec");
    
    // Panel:
    var panel = new YAHOO.widget.Dialog("panel-ds-stats", {
      buttons             : [
        { text: "Generate", handler: function (e) { dsStats(false); } },
        { text: "Close",      handler: function (e) { u.p.hide(); }, isDefault: true }
      ],
      close               : false,
      constraintoviewport : true,
      draggable           : true,
      dragOnly            : true,
      fixedcenter         : true,
      modal               : true,
      visible             : false
    });
    
    panel.render();
    u.p = panel;
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initMain() {
    ui.app.mask = $("app-mask");
    ui.region.app.body = $("app-bd");
    
    // (1) Layout:
    ui.main = new YAHOO.widget.Layout({
      units: [
        { position: "top",    body: "app-hd-outer", height: 36 },
        { position: "bottom", body: "app-ft-outer", height: 23 },
        { position: "center", body: "app-bd" }
      ]
    });
    ui.main.render();
    
    // (2) Toolbar:
    // (2a) Radio buttons:
    state.ui.tbarIdx = parseInt(YAHOO.util.Cookie.get(CONST.cookie.appName + "ui-tbar-idx")) || 0;
    ui.tbar.btnGrp = new YAHOO.widget.ButtonGroup("btn-grp-tbar");
    var B = ui.tbar.btnGrp.getButtons();
    B[0].on("click", function (e) { $show("prep-tabview"); $hide("stat-tabview"); $hide("bnet-tabview"); state.ui.tbarIdx = 0; Pegasus.Main.setCookie(CONST.cookie.appName, "ui-tbar-idx", 0); refreshTab(); });
    B[1].on("click", function (e) { $hide("prep-tabview"); $show("stat-tabview"); $hide("bnet-tabview"); state.ui.tbarIdx = 1; Pegasus.Main.setCookie(CONST.cookie.appName, "ui-tbar-idx", 1); refreshTab(); });
    B[2].on("click", function (e) { $hide("prep-tabview"); $hide("stat-tabview"); $show("bnet-tabview"); state.ui.tbarIdx = 2; Pegasus.Main.setCookie(CONST.cookie.appName, "ui-tbar-idx", 2); refreshTab(); });
    
    // (2b) Other buttons:
    new YAHOO.widget.Button("tbar-btn-note", { onclick: { fn: function (e) { txtShow("Note", null, getCurrDS().note, true, true); } } });
    new YAHOO.widget.Button("tbar-btn-ds", { onclick: { fn: function (e) { ui.panel.ds.p.show(); } } });
    
    // (3) Tab view:
    state.ui.tabViewIdx.prep = parseInt(YAHOO.util.Cookie.get(CONST.cookie.appName + "ui-tabview-idx-prep")) || 0;
    ui.tabView.prep = new YAHOO.widget.TabView("prep-tabview", { orientation: "top" });
    ui.tabView.prep.set("activeIndex", state.ui.tabViewIdx.prep);
    ui.tabView.prep.on("activeIndexChange", function (e) { var idx = ui.tabView.prep.get("activeIndex"); state.ui.tabViewIdx.prep = idx; Pegasus.Main.setCookie(CONST.cookie.appName, "ui-tabview-idx-prep", idx); refreshTab(); } );
    
    state.ui.tabViewIdx.stat = parseInt(YAHOO.util.Cookie.get(CONST.cookie.appName + "ui-tabview-idx-stat")) || 0;
    ui.tabView.stat = new YAHOO.widget.TabView("stat-tabview", { orientation: "top" });
    ui.tabView.stat.set("activeIndex", state.ui.tabViewIdx.stat);
    ui.tabView.stat.on("activeIndexChange", function (e) { var idx = ui.tabView.stat.get("activeIndex"); state.ui.tabViewIdx.stat = idx; Pegasus.Main.setCookie(CONST.cookie.appName, "ui-tabview-idx-stat", idx); refreshTab(); } );
    
    state.ui.tabViewIdx.bnet = parseInt(YAHOO.util.Cookie.get(CONST.cookie.appName + "ui-tabview-idx-bnet")) || 0;
    ui.tabView.bnet = new YAHOO.widget.TabView("bnet-tabview", { orientation: "top" });
    ui.tabView.bnet.set("activeIndex", state.ui.tabViewIdx.bnet);
    ui.tabView.bnet.on("activeIndexChange", function (e) { var idx = ui.tabView.bnet.get("activeIndex"); state.ui.tabViewIdx.bnet = idx; Pegasus.Main.setCookie(CONST.cookie.appName, "ui-tabview-idx-bnet", idx); refreshTab(); } );
    
    // (4) Status bar:
    ui.sbar.currPrj = $("sbar-curr-prj");
    ui.sbar.currPrj.innerHTML = Pegasus.Main.getPrjName(state.prjId);
    
    ui.sbar.currDS = $("sbar-curr-ds");
    ui.sbar.currDS.onclick = function (e) { ui.panel.ds.p.show(); };
    
    ui.sbar.info = $("sbar-info");
    
    // (5) Other:
    ui.cmdRes = $("cmd-res");
    ui.cmdRes.onclick = function (e) { window.clearTimeout(state.timer.cmdRes); state.timer.cmdRes = null; $hide(ui.cmdRes); };
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initProcPanel() {
    ui.panel.proc.p = new YAHOO.widget.Panel("panel-proc", {
      width       : "240px",
      fixedcenter : true,
      close       : false,
      draggable   : false,
      modal       : true,
      visible     : false
    });
    ui.panel.proc.p.render();
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initPrepData() {
    // Buttons:
    ui.tab.prep.data.btnShowTxt  = new YAHOO.widget.Button("prep-data-btn-show-txt",  { onclick: { fn: function (e) { dsShow("txt");  } } });
    ui.tab.prep.data.btnShowHtml = new YAHOO.widget.Button("prep-data-btn-show-html", { onclick: { fn: function (e) { dsShow("html"); } } });
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initPrepScript() {
    var u = ui.tab.prep.scr;
    
    u.txt = $("prep-scr-txt");
    
    (new YAHOO.widget.Button("prep-scr-btn-clear")).on("click", function (e) { if (u.txt.value.length === 0) return; if (!confirm("Clear the content of the script?")) return; u.txt.value = ""; });
    (new YAHOO.widget.Button("prep-scr-btn-exec")). on("click", function (e) { if (u.txt.value.length === 0) return; scriptExec(); });
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initPrepVars() {
    var u = ui.tab.prep.vars;
    
    // Button bar:
    u.btnBasRep = new YAHOO.widget.Button("prep-vars-btn-bas-rep", { onclick: { fn: dsBasRep }                                                    });
    u.btnNew    = new YAHOO.widget.Button("prep-vars-btn-new",     { onclick: { fn: varNew }                                                      });
    u.btnDel    = new YAHOO.widget.Button("prep-vars-btn-del",     { onclick: { fn: varDel },                                      disabled: true });
    u.btnMarkTS = new YAHOO.widget.Button("prep-vars-btn-mark-ts", { onclick: { fn: varMarkTS },                                   disabled: true });
    //u.btnReord  = new YAHOO.widget.Button("prep-vars-btn-reord",   { onclick: { fn: function (e) { ui.panel.varReord.p.show(); } }                });
    u.btnReord  = new YAHOO.widget.Button("prep-vars-btn-reord",   { onclick: { fn: dsVarReorder }                                                });
    u.btnType   = new YAHOO.widget.Button("prep-vars-btn-type",    { onclick: { fn: function (e) { ui.panel.varType.p.show(); } }, disabled: true });
    u.btnTrans  = new YAHOO.widget.Button("prep-vars-btn-trans",   { onclick: { fn: function (e) { ui.panel.varTrans.p.show(); } }                });
    
    // Data table -- submitters:
    var fnAfterSuccess = function (prop) {
      return function (o,newVal,res) {
        getCurrDS().var[o.getRecord().getData("idx")][prop] = newVal;
      };
    };
    
    var fnGetCmd = function (attr) {
      return function (o,newVal) {
        return { cmd: "attr-set", "obj-name": "var", "obj-human-name": "variable", "obj-id": o.getRecord().getData("id"), "attr-name": attr, "attr-val": $enc(newVal) };
      };
    };
    
    //var fnSetName = getSetAttrFn([valid.strNonEmpty], fnAfterSuccess("name"), fnGetCmd("name"));
    var fnSetName = function () { alert("Server implementation needed."); };
    var fnSetMemo = getSetAttrFn([], fnAfterSuccess("memo"), fnGetCmd("memo"));
    
    // Data table -- columns:
    var cols = [
      { key: "curr", label: "", formatter: "radio" },
      //{ key: "name", label: "Name", editor: new YAHOO.widget.TextboxCellEditor({ asyncSubmitter: fnSetName }), maxAutoWidth: 120, className: "name" },
      { key: "name", label: "Name", maxAutoWidth: 160, className: "name" },
      { key: "type", label: "Type" },
      { key: "missValCnt", label: "Miss (%)" },
      {
        label: "Descriptives",
        children: [
          { key: "stat.desc.min", label: "Min" },
          { key: "stat.desc.max", label: "Max" },
          { key: "stat.desc.mean", label: "Mean" },
          { key: "stat.desc.med", label: "Med" },
          { key: "stat.desc.sd", label: "SD" }
        ]
      },
      {
        label: "Normality<sup><b>1</b></sup>",
        children: [
          { key: "stat.norm.ad", label: "AD" },
          { key: "stat.norm.cvm", label: "CVM" },
          { key: "stat.norm.ks", label: "KS" },
          { key: "stat.norm.sw", label: "SW" }
        ]
      },
      { key: "memo", label: "Memo", editor: new YAHOO.widget.TextareaCellEditor({ asyncSubmitter: fnSetMemo }), maxAutoWidth: 200 }
    ];
    
    // Data table -- event handlers:
    var onRadioClickEvent = function (oArgs) {
      var radio = oArgs.target;
      var rec = this.getRecord(radio);
      var newIdx = parseInt(rec.getData("idx"));
      
      if (newIdx === state.currVar.idx) return;
      
      if (state.currVar.ui.rec) state.currVar.ui.rec.setData("curr", false);
      rec.setData("curr", true);
      
      state.currVar.idx      = newIdx;
      state.currVar.ui.rec   = rec;
      state.currVar.ui.radio = radio;
      
      ui.tbl.vars.tbl.unselectAllRows();
      ui.tbl.vars.tbl.selectRow(radio);
      
      varSetBtnsEnabled(true);
    };
    
    // Data table:
    genTbl(CONST.appId, ui.tbl.vars, $("prep-vars-dt"), ["idx","curr","name","type","missValCnt","stat.desc.min","stat.desc.max","stat.desc.mean","stat.desc.med","stat.desc.sd","stat.norm.ks","stat.norm.ad","stat.norm.sw","memo"], cols, { radioClickEvent: onRadioClickEvent }, { sortedBy: { key:"name", dir:"asc" } }, null, true);
    
    // Mouse over and out:
    Pegasus.Main.dataTblAddCellMouseInfoEvts(
      ui.tbl.vars.tbl,
      function (rec, col, data) {
        if (col.key === "stat.norm.ad" || col.key === "stat.norm.cvm" || col.key === "stat.norm.ks" || col.key === "stat.norm.sw") {
          var v = getCurrDS().var[rec.getData("idx")];
          if (!v.stat.norm) return;
          var testAbbr = col.key.substr(col.key.lastIndexOf(".") + 1);
          ui.sbar.info.innerHTML = "Statistic: <b>" + v.stat.norm[testAbbr].s + "</b>, p-value: <b>" + v.stat.norm[testAbbr].p + "</b>";
        }
      },
      ui.sbar.info
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initStatRInt() {
    var u = ui.tab.stat.rInt;
    
    // Button bar:
    u.btnClearCons = new YAHOO.widget.Button("stat-r-int-btn-clear-cons", { onclick: { fn: rIntClearCons }                 });
    u.btnFullHist  = new YAHOO.widget.Button("stat-r-int-btn-full-hist",  { onclick: { fn: rIntFullHist }                  });
    u.btnDelHist   = new YAHOO.widget.Button("stat-r-int-btn-del-hist",   { onclick: { fn: rIntDelHist }                   });
    u.btnResetEng  = new YAHOO.widget.Button("stat-r-int-btn-reset-eng",  { onclick: { fn: rIntResetEng },  disabled: true });
    u.btnLoadDS    = new YAHOO.widget.Button("stat-r-int-btn-load-ds",    { onclick: { fn: rIntLoadDS },    disabled: true });
    
    // Other controls:
    u.in        = $("stat-r-int-in");
    u.out       = $("stat-r-int-out");
    u.btnSubmit = $("stat-r-int-btn-submit");
    u.btnSubmit.onclick = rIntSubmit;
    
    // Events:
    new YAHOO.util.KeyListener(u.in, { keys: 13 }, { fn: rIntSubmit,    scope: this, correctScope:true }).enable();
    new YAHOO.util.KeyListener(u.in, { keys: 27 }, { fn: rIntHistReset, scope: this, correctScope:true }).enable();
    new YAHOO.util.KeyListener(u.in, { keys: 40 }, { fn: rIntHistNext,  scope: this, correctScope:true }).enable();
    new YAHOO.util.KeyListener(u.in, { keys: 38 }, { fn: rIntHistPrev,  scope: this, correctScope:true }).enable();
    
    // Check for R connectivity:
    rExec(
      "R.version.string",
      function (res) {
        u.btnResetEng.set("disabled", false);
        u.btnLoadDS.set("disabled", false);
        u.btnSubmit.disabled = false;
        u.in.disabled = false;
      },
      function (res) {
        rIntOutAddRes(null, true, "An error has occured while connecting to the R engine. Because of that, the 'R: Interactive' feature of the application will be unavilable.", -1, 0);
      }
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initStatRScr() {
    var u = ui.tab.stat.rScr;
    
    // Button bar:
    u.btnClear = new YAHOO.widget.Button("stat-r-scr-btn-clear", { onclick: { fn: rScrClear }                 });
    u.btnExec  = new YAHOO.widget.Button("stat-r-scr-btn-exec",  { onclick: { fn: rScrExec },  disabled: true });
    
    // Other controls:
    u.in = $("stat-r-scr-in");
    
    // Check for R connectivity:
    rExec(
      "R.version.string",
      function (res) {
        u.btnClear.set("disabled", false);
        u.btnExec.set("disabled", false);
        u.in.disabled = false;
      },
      function (res) {
        u.in.value = "An error has occured while connecting to the R engine. Because of that, the 'R: Script' feature of the application will be unavilable.";
      }
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initStatRep() {
    var u = ui.tab.stat.rep;
    
    // Button bar:
    u.btnView   = new YAHOO.widget.Button("stat-rep-btn-view",    { onclick: { fn: repView  },                        disabled: true });
    u.btnPDF    = new YAHOO.widget.Button("stat-rep-btn-pdf",     { onclick: { fn: repPDF   },                        disabled: true });
    u.btnDel    = new YAHOO.widget.Button("stat-rep-btn-del",     { onclick: { fn: function (e) { repDel(false); } }, disabled: true });
    u.btnDelAll = new YAHOO.widget.Button("stat-rep-btn-del-all", { onclick: { fn: function (e) { repDel(true); }  }                 });
    u.btnCode   = new YAHOO.widget.Button("stat-rep-btn-code",    { onclick: { fn: repCode },                         disabled: true });
    
    // Data table -- submitters:
    var fnAfterSuccess = function (prop) {
      return function (o,newVal,res) {
        getCurrDS().rep[o.getRecord().getData("idx")][prop] = newVal;
      };
    };
    
    var fnGetCmd = function (attr) {
      return function (o,newVal) {
        return { cmd: "attr-set", "obj-name": "rep", "obj-human-name": "report", "obj-id": o.getRecord().getData("id"), "attr-name": attr, "attr-val": $enc(newVal) };
      };
    };
    
    var fnSetName = getSetAttrFn([valid.strNonEmpty], fnAfterSuccess("name"), fnGetCmd("name"));
    var fnSetMemo = getSetAttrFn([],                  fnAfterSuccess("memo"), fnGetCmd("memo"));
    
    // Data table -- columns:
    var cols = [
      { key: "curr", label: "", formatter: "radio" },
      { key: "name", label: "Name", sortabke: true, editor: new YAHOO.widget.TextboxCellEditor({ asyncSubmitter: fnSetName }), maxAutoWidth: 120, className: "name" },
      { key: "created", label: "Created", sortable: true, formatter: Pegasus.Main.UI.Tbl.Formatter.date, sortable: true, sortOptions: { sortFunction: Pegasus.Main.UI.Tbl.Sorter.genDate(function () { return getCurrDS().rep; }, "created") } },
      { key: "size", label: "Size", sortable: true },
      { key: "calcTime", label: "Calc", sortable: true },
      { key: "memo", label: "Memo", sortable: true, editor: new YAHOO.widget.TextareaCellEditor({ asyncSubmitter: fnSetMemo }), maxAutoWidth: 200 }
    ];
    
    // Data table -- event handlers:
    var onRadioClickEvent = function (oArgs) {
      var radio = oArgs.target;
      var rec = this.getRecord(radio);
      var newIdx = parseInt(rec.getData("idx"));
      
      if (newIdx === state.currRep.idx) return;
      
      if (state.currRep.ui.rec) state.currRep.ui.rec.setData("curr", false);
      rec.setData("curr", true);
      
      state.currRep.idx      = newIdx;
      state.currRep.ui.rec   = rec;
      state.currRep.ui.radio = radio;
      
      var t = ui.tbl.rep.tbl;
      t.unselectAllRows();
      t.selectRow(radio);
      
      var u = ui.tab.stat.rep;
      u.btnView.set("disabled", false);
      u.btnPDF.set("disabled", false);
      u.btnDel.set("disabled", false);
      u.btnCode.set("disabled", false);
    };
    
    // Data table:
    genTbl(CONST.appId, ui.tbl.rep, $("stat-rep-dt"), ["idx","curr","name","created","size","calcTime","memo"], cols, { radioClickEvent: onRadioClickEvent }, { sortedBy: { key:"created", dir:"desc" } }, null, true);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initTxtPanel() {
    // Panel -- button event handlers:
    var fnSelTxt = function (e) {
      if (ui.panel.txt.edit.style.display === "block") {
        ui.panel.txt.edit.select();
      }
      else {
        window.getSelection().removeAllRanges();
        var range = document.createRange();
        range.selectNode(ui.panel.txt.view);
        window.getSelection().addRange(range);
      }
    };
    
    // Panel -- event handlers:
    ui.panel.txt.fnNoteHide = function () {
      var u = ui.panel.txt;
      
      u.p.hideEvent.unsubscribe(u.fnNoteHide);
      
      var ds = getCurrDS();
      if (ds.note === u.edit.value) return;
      
      ds.note = u.edit.value;
      execCmd(false,
        { cmd: "attr-set", "obj-name": "ds", "obj-human-name": "data set", "obj-id": ds.id, "attr-name": "note", "attr-val": $enc(ds.note) },
        function (res) {
          cmdResShow(res.outcome ? "Note saved" : "Not NOT saved");
        },
        null, null, true
      );
    };
    
    // Panel:
    var panel = new YAHOO.widget.Dialog("panel-txt",
      {
        buttons             : [
          { text: "Select text", handler: fnSelTxt },
          { text: "Close",       handler: function (e) { this.hide(); } }
        ],
        constraintoviewport : true,
        dragOnly            : true,
        fixedcenter         : true,
        modal               : true,
        visible             : false,
        width               : CONST.panelW
      }
    );
    panel.render();
    ui.panel.txt.p = panel;
    
    ui.panel.txt.msg  = $("panel-txt-msg");
    ui.panel.txt.view = $("panel-txt-view");
    ui.panel.txt.edit = $("panel-txt-edit");
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initVarReordPanel() {
    var u = ui.panel.varReord;
    
    // Panel -- controls:
    //u.inpVal  = $("panel-var-trans-inp-val");
    
    // Panel -- button event handlers:
    var fnApply = function (e) {
      alert("Not implemented yet.");
    };
    
    // Panel:
    var panel = new YAHOO.widget.Dialog("panel-var-reord", {
      buttons             : [
        { text: "Reorder", handler: fnApply },
        { text: "Close", handler: function (e) { ui.panel.varReord.p.hide(); }, isDefault: true }
      ],
      close               : false,
      constraintoviewport : true,
      draggable           : true,
      dragOnly            : true,
      fixedcenter         : true,
      modal               : true,
      visible             : false,
      width               : "400px"
    });
    panel.render();
    u.p = panel;
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initVarTransPanel() {
    var u = ui.panel.varTrans;
    
    // Panel -- controls:
    u.inpVal  = $("panel-var-trans-inp-val");
    u.inpName = $("panel-var-trans-inp-name");
    
    // Panel -- type drop-down list:
    var fnLstTypeClick = function (p_sType, p_aArgs, p_oItem) {
      ui.panel.varTrans.lstType.set("label", p_oItem.cfg.getProperty("text"));
      ui.panel.varTrans.lstType.set("value", p_oItem.value);
    };
    
    var mnuLstType = [];
    for (var i = 1; i <= 6; i++) {
      mnuLstType.push({ text: CONST.var.typeDispL[i] + " (" + CONST.var.typeDispS[i] + ")", value: i, onclick: { fn: fnLstTypeClick } });
    }
    
    u.lstType = new YAHOO.widget.Button({
      container: "panel-var-trans-lst-type",
      label: CONST.var.typeDispL[CONST.var.type.con] + " (" + CONST.var.typeDispS[CONST.var.type.con] + ")",
      value: CONST.var.type.con,
      menu: mnuLstType,
      type: "menu"
    });
    
    // Panel:
    var panel = new YAHOO.widget.Dialog("panel-var-trans", {
      buttons             : [
        { text: "Transform", handler: function (e) { varTrans(false); } },
        { text: "Close", handler: function (e) { ui.panel.varTrans.p.hide(); }, isDefault: true }
      ],
      close               : false,
      constraintoviewport : true,
      draggable           : true,
      dragOnly            : true,
      fixedcenter         : true,
      modal               : true,
      visible             : false,
      width               : "400px"
    });
    
    // Panel -- events:
    panel.showEvent.subscribe(function (e) {
      var u = ui.panel.varTrans;
      u.inpVal.value = "";
      u.inpName.value = "";
      u.p.show();
    });
    
    // Finish up:
    panel.render();
    u.p = panel;
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initVarTypePanel() {
    var u = ui.panel.varType;
    
    // Panel -- controls:
    u.txtCurrName = $("panel-var-type-txt-curr-name");
    u.txtCurrType = $("panel-var-type-txt-curr-type");
    u.inpNewName  = $("panel-var-type-inp-new-name");
    u.lstNewType  = $("panel-var-type-lst-new-type");
    u.txtInfo     = $("panel-var-type-txt-info");
    
    // Panel -- type drop-down list:
    var fnLstNewTypeClick = function (p_sType, p_aArgs, p_oItem) {
      var u = ui.panel.varType;
      
      u.lstNewType.set("label", p_oItem.cfg.getProperty("text"));
      u.lstNewType.set("value", p_oItem.value);
      
      var currType = getCurrVar().type;
      var newType = parseInt(u.lstNewType.get("value"));
      
      if (currType === newType) u.txtInfo.innerHTML = "Variable already is of this type.";
      else if (currType === CONST.var.type.str && newType !== CONST.var.type.bin) u.txtInfo.innerHTML = "Conversion from the string to any numeric type will not succeed if at least one value cannot be casted as a number. This does not include missing values.";
      else if (currType === CONST.var.type.str && newType === CONST.var.type.bin) u.txtInfo.innerHTML = "Conversion from the string to any numeric type will not succeed if at least one value cannot be casted as a number. This does not include missing values.<br /><br />Conversion to the binary type will produce <i>0</i> for all zeros and <i>1</i> for all other numbers.";
      else if (currType !== CONST.var.type.str && newType === CONST.var.type.bin) u.txtInfo.innerHTML = "Conversion to the binary type will produce <i>0</i> for all zeros and <i>1</i> for all other numbers.";
      else if (currType === CONST.var.type.con && newType !== CONST.var.type.str) u.txtInfo.innerHTML = "Conversion from the continuous to any of the other numeric types will result in truncation of the decimal part of all numbers.<br /><br />If you need a different result, please transform the value of the variable with the <i>round()</i> or <i>ceil()</i> functions prior to changing its type.";
      else u.txtInfo.innerHTML = "";
    };
    
    var mnuLstNewTypeClick = [];
    for (var i = 1; i <= 6; i++) {
      mnuLstNewTypeClick.push({ text: CONST.var.typeDispL[i] + " (" + CONST.var.typeDispS[i] + ")", value: i, onclick: { fn: fnLstNewTypeClick } });
    }
    
    u.lstNewType = new YAHOO.widget.Button({
      container: "panel-var-type-lst-new-type",
      label: "---",
      value: "",
      menu: mnuLstNewTypeClick,
      type: "menu"
    });
    
    // Panel -- button event handlers:
    var fnChangeType = function (e) {
      var u = ui.panel.varType;
      
      var nameExists = false;
      $map(function (v) { if (!nameExists) nameExists = (nameExists || v.name === u.inpNewName.value); }, getCurrDS().var);
      if (nameExists) return alert("A variable with the specified name already exists in the active data set. The only existing variable the type of which you can change using this window is the active variable. In order to do so, please leave the new variable name field empty.");
      
      if (!u.lstNewType.get("value")) return alert("Please select the type.");
      
      if (u.inpNewName.value.length === 0 && parseInt(u.lstNewType.get("value")) === getCurrVar().type) return alert("The active variable is already of this type.");
      
      if (u.inpNewName.value.length === 0 && !confirm("You are about to change an existing variable. This operation is irreversible. Please remember, that you can create a new variable and remove the old one after you have made sure the new variable meets your requirements.")) return;
      
      procShow("Changing variable type...");
      execCmd(true,
        { cmd: "var-set-type", "var-id": getCurrVar().id, "new-name": encodeURIComponent(u.inpNewName.value), type: u.lstNewType.get("value") },
        function (res) {
          // Data sets data table:
          var ds = getCurrDS();
          var o = { idx: state.currDS.idx, curr: true };
          $setObj(o, ds);
          o.size = $size2str(ds.size, true);
          o.varCnt++;
          ui.tbl.ds.tbl.updateRow(state.currDS.idx, o);
          
          ui.panel.proc.p.hide();
        },
        null, null, true
      );
    };
    
    // Panel:
    var panel = new YAHOO.widget.Dialog("panel-var-type", {
      buttons             : [
        { text: "Change type", handler: fnChangeType },
        { text: "Close", handler: function (e) { ui.panel.varType.p.hide(); }, isDefault: true }
      ],
      close               : false,
      constraintoviewport : true,
      draggable           : true,
      dragOnly            : true,
      fixedcenter         : true,
      modal               : true,
      visible             : false,
      width               : "400px"
    });
    
    // Panel -- events:
    panel.showEvent.subscribe(function (e) {
      var u = ui.panel.varType;
      
      u.txtCurrName.innerHTML = getCurrVar().name;
      u.txtCurrType.innerHTML = CONST.var.typeDispL[getCurrVar().type] + " (" + CONST.var.typeDispS[getCurrVar().type] + ")";
      u.inpNewName.value = "";
      u.lstNewType.set("label", "---");
      u.lstNewType.set("value", "");
      
      u.p.show();
    });
    
    // Finish up:
    panel.render();
    u.p = panel;
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function noteShow(title, msg, txt, edit) {
    var u = ui.panel.txt;
    
    u.msg.innerHTML = (msg && msg.length > 0 ? msg + "<br /><br />" : "");
    if (edit) {
      u.edit.value = txt;
      $hide(u.view);
      $show(u.edit);
    }
    else {
      u.view.innerHTML = txt;
      $hide(u.edit);
      $show(u.view);
    }
    u.p.setHeader(title);
    u.p.show();
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function procShow(msg) {
    ui.panel.proc.p.setHeader(msg);
    ui.panel.proc.p.show();
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  /*
   * Populates the current tab if it contains at least one DataTable object.
   */
  function refreshTab() {
    if (state.app.startUp || state.currDS.idx == -1) return;
    
    switch (state.ui.tbarIdx) {
      case 0:  // prep
        switch (state.ui.tabViewIdx.prep) {
          case 0: refreshTab_PrepData(); break;
          case 1: refreshTab_PrepVars(); break;
        }
        break;
      
      case 1:  // stat
        switch (state.ui.tabViewIdx.stat) {
          case 0: refreshTab_StatRInt(); break;
          case 2: refreshTab_StatRep(); break;
        }
        break;
    }
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function refreshTab_PrepData() {
    // ...
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function refreshTab_PrepVars() {
    var tab = ui.tab.prep.vars;
    var tbl = ui.tbl.vars.tbl;
    
    // Populate:
    if (!tab.populated) {
      tab.populated = true;
      
      if (!state.script.active) procShow("Populating the tab...");
      
      var fnFinish = function (e) {
        tbl.unsubscribe("postRenderEvent");
        if (!state.script.active) ui.panel.proc.p.hide();
      }
      
      var fnVars = function (e) {
        tab.btnDel.set("disabled", true);
        tab.btnType.set("disabled", true);
        
        tbl.onShow();
        if (tbl.getRecordSet().getLength() > 0) tbl.deleteRows(0, tbl.getRecordSet().getLength());
        
        var rows = [];
        if (state.currDS.idx >= 0) {
          var ds = getCurrDS();
          var V = (ds ? ds.var : []);
          for (var i=0, ni=V.length; i < ni; i++) {
            /*
            var v = V[i];
            
            var vsd = v.stat.desc;
            var vsn = v.stat.norm;
            var vsm = v.stat.misc;
            
            var o = {
              idx              : i,
              id               : v.id,
              type             : CONST.var.typeDispS[v.type],
              name             : v.name,
              memo             : v.memo,
              "missValCnt"     : vsm.missValCnt + "&nbsp;(" + (getCurrDS().obsCnt === 0 ? "-" : Math.round((vsm.missValCnt / getCurrDS().obsCnt) * 100)) + ")",
              "stat.desc.min"  : (vsd && vsd.min  && !isNaN(vsd.min)   ? getVal(vsd.min,  2) : ""),
              "stat.desc.max"  : (vsd && vsd.max  && !isNaN(vsd.max)   ? getVal(vsd.max,  2) : ""),
              "stat.desc.mean" : (vsd && vsd.mean && !isNaN(vsd.mean)  ? getVal(vsd.mean, 2) : ""),
              "stat.desc.med"  : (vsd && vsd.med  && !isNaN(vsd.med)   ? getVal(vsd.med,  2) : ""),
              "stat.desc.sd"   : (vsd && vsd.sd   && !isNaN(vsd.sd)    ? getVal(vsd.sd,   2) : ""),
              "stat.norm.ad"   : (vsn && !isNaN(vsn.ad.s)  && vsn.ad.s  !== "Infinity" ? (vsn.ad.p  < CONST.stat.alpha ? "" : "+" ) : ""),
              "stat.norm.cvm"  : (vsn && !isNaN(vsn.cvm.s) && vsn.cvm.s !== "Infinity" ? (vsn.cvm.p < CONST.stat.alpha ? "" : "+" ) : ""),
              "stat.norm.ks"   : (vsn && !isNaN(vsn.ks.s)  && vsn.ks.s  !== "Infinity" ? (vsn.ks.p  < CONST.stat.alpha ? "" : "+" ) : ""),
              "stat.norm.sw"   : (vsn && !isNaN(vsn.sw.s)  && vsn.sw.s  !== "Infinity" ? (vsn.sw.p  < CONST.stat.alpha ? "" : "+" ) : "")
            };
            
            rows.push(o);
            */
            rows.push(tblRowGet_var(V[i], V.length));
          }
        }
        tbl.on("postRenderEvent", fnFinish);
        if (rows.length > 0) tbl.addRows(rows);
        else tbl.render();
      }
      
      fnVars();
    }
    
    // Resize:
    if (!tab.resized) {
      uiResizeDataTbl(tbl, false, true);
      tab.resized = true;
    }
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function refreshTab_StatRInt() {
    var tab = ui.tab.stat.rInt;
    
    // Populate:
    if (!tab.populated) {
      tab.populated = true;
      
      if (!state.script.active) procShow("Populating the tab...");
      
      rIntClearCons();
      var ri = getCurrDS().rInt;
      var i = 0;
      $map(
        function (x) {
          rIntOutAddCmd(x.cmd);
          rIntOutAddRes(x.id, x.full, x.res, i++, x.resLnCnt);
        },
        ri.recent
      );
      
      switch (ri.cmdCnt) {
        case 0: rIntOutAddInfo("INFO: the commands history is empty"); break;
        case 1: rIntOutAddInfo("INFO: 1 command in the history; it is shown above"); break;
        case ri.recent.length: rIntOutAddInfo("INFO: " + ri.cmdCnt + " commands in the history; all of them shown above"); break;
        default: rIntOutAddInfo("INFO: " + ri.cmdCnt + " commands in the history; " + ri.recent.length + " most recent ones shown above");
      }
      
      if (!state.script.active) ui.panel.proc.p.hide();
    }
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function refreshTab_StatRep() {
    var tab = ui.tab.stat.rep;
    var tbl = ui.tbl.rep.tbl;
    
    // Populate:
    if (!tab.populated) {
      tab.populated = true;
      
      if (!state.script.active) procShow("Populating the tab...");
      
      var fnFinish = function (e) {
        tbl.unsubscribe("postRenderEvent");
        Pegasus.Main.tblResort(tbl);
        ui.tab.stat.rep.btnDelAll.set("disabled", getCurrDS().rep.length === 0);
        Pegasus.Main.tblResort(tbl);
        if (!state.script.active) ui.panel.proc.p.hide();
      }
      
      var fnRep = function (e) {
        tab.btnView.set("disabled", true);
        tab.btnPDF.set("disabled", true);
        tab.btnDel.set("disabled", true);
        
        tbl.onShow();
        if (tbl.getRecordSet().getLength() > 0) tbl.deleteRows(0, tbl.getRecordSet().getLength());
        
        var rows = [];
        if (state.currDS.idx >= 0) {
          var ds = getCurrDS();
          if (ds) {
            var R = ds.rep;
            for (var i=0, ni=R.length; i < ni; i++) {
              var r = R[i];
              rows.push({ idx: i, id: r.id, name: r.name, created: r.created, size: $size2str(r.size, true), calcTime: r.calcTime, memo: r.memo });
            }
          }
        }
        tbl.on("postRenderEvent", fnFinish);
        if (rows.length > 0) tbl.addRows(rows);
        else tbl.render();
      }
      
      fnRep();
    }
    
    // Resize:
    if (!tab.resized) {
      uiResizeDataTbl(tbl, false, true);
      tab.resized = true;
    }
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function repAdd2Tbl(r) {
    X = r;
    var R = getCurrDS().rep;
    R.push(r);
    var o = { idx: R.length-1 };
    $setObj(o, r);
    
    ui.tab.stat.rep.populated = false;
    ui.tab.stat.rep.resized   = false;
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function repDel(all) {
    if (!confirm((all ? "Delete all reports? This operation is irreversible." : "Delete the active report ('" + getCurrRep().name + "')? This operation is irreversible."))) return;
    
    procShow(all ? "Deleting all reports..." : "Deleting the active report...");
    execCmd(true,
      (all
        ? { cmd: "rep-del-all", "ds-id": getCurrDS().id }
        : { cmd: "rep-del", "rep-id": getCurrRep().id }
      ),
      function (res) {
        var t = ui.tbl.rep.tbl;
        
        if (all) {
          getCurrDS().rep = [];
          t.deleteRows(0, t.getRecordSet().getLength());
        }
        else {
          getCurrDS().rep[state.currRep.idx] = null;
          t.deleteRow(state.currRep.ui.rec);
        }
        
        ui.tab.stat.rep.btnView.set("disabled", true);
        ui.tab.stat.rep.btnPDF.set("disabled", true);
        ui.tab.stat.rep.btnDel.set("disabled", true);
        ui.tab.stat.rep.btnDelAll.set("disabled", t.getRecordSet().getLength() === 0);
        ui.tab.stat.rep.btnCode.set("disabled", true);
        
        state.currRep = { idx: -1, ui: { rec: null, radio: null } };
        
        ui.panel.proc.p.hide();
      },
      null, null, true
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function repPDF() {
    window.open(CONST.serverURI_L + "?" + getCmd({ cmd: "rep-get-pdf", "rep-id": getCurrRep().id }), "", "toolbar=1,status=0,scrollbars=1");
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function repCode() {
    window.open(CONST.serverURI_L + "?" + getCmd({ cmd: "rep-get-code", "rep-id": getCurrRep().id }), "", "toolbar=1,status=0,scrollbars=1");
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function repView() {
    window.open("popups/data/rep-view.html?rep-id=" + getCurrRep().id, "", "toolbar=1,status=0,scrollbars=1");
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function resetCurrState(names) {
    for (var i=0; i < names.length; i++) {
      state[names[i]] = { idx: -1, ui: { rec: null, radio: null } };
    }
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function rExec(cmd, fnOk, fnErr) {
    var dsId = (state.currDS.idx !== -1 ? getCurrDS().id : "");
    execCmd(true, { cmd: "r-exec-cmd", rcmd: encodeURIComponent(cmd.replace(/\+/g, "%2B")), "ds-id": dsId }, fnOk, fnOk, fnErr, true);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function rIntClearCons() {
    $removeChildren(ui.tab.stat.rInt.out);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function rIntDelHist() {
    if (!confirm("You are about to delete the command history. If you continue, you will not be able to access the commands exectured so far. This operation is irreversible.")) return;
    
    var fnOk = function (res) {
      state.rInt.currIdx = 0;
      state.rInt.ready = true;
      
      var ri = getCurrDS().rInt;
      ri.cmdCnt = 0;
      ri.recent = [];
      
      rIntClearCons();
      
      ui.panel.proc.p.hide();
    };
    
    procShow("Deleting the command history...");
    execCmd(true,
      { cmd: "r-int-del-hist", "ds-id": getCurrDS().id },
      fnOk,
      fnOk,
      function (res) {
        state.rInt.ready = true;
        ui.panel.proc.p.hide();
      },
      true
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function rIntFullHist() {
    var ds = getCurrDS();
    var win = window.open(CONST.serverURI_L + "?" + getCmd({ cmd: "r-int-get-hist", "ds-id": ds.id }), "", "toolbar=1,status=0,scrollbars=1");
    window.setTimeout(function () { win.document.title = "R command history for data set '" + ds.name + "'" }, 500);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function rIntHistNext() {
    var s = state.rInt;
    var ri = getCurrDS().rInt;
    
    if (s.currIdx === ri.recent.length) return;
    
    if (s.currIdx === ri.recent.length - 1) {
      s.currIdx++;
      ui.tab.stat.rInt.in.value = s.lastCmd;
    }
    else {
      ui.tab.stat.rInt.in.value = ri.recent[++s.currIdx].cmd;
    }
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function rIntHistPrev() {
    var s = state.rInt;
    var ri = getCurrDS().rInt;
    
    if (s.currIdx === 0) return;
    
    if (s.currIdx === ri.recent.length) s.lastCmd = ui.tab.stat.rInt.in.value;
    ui.tab.stat.rInt.in.value = ri.recent[--s.currIdx].cmd;
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function rIntHistReset() {
    var s = state.rInt;
    s.currIdx = getCurrDS().rInt.recent.length;
    s.lastCmd = "";
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function rIntLoadDS() {
    ui.tab.stat.rInt.in.value = CONST.rCmd.loadDS;
    rIntSubmit();
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function rIntOutAddCmd(s) {
    var out = ui.tab.stat.rInt.out;
    $$("pre", out, null, "cmd", "&gt; " + s);
    out.scrollTop = out.scrollHeight;
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function rIntOutAddInfo(s) {
    var out = ui.tab.stat.rInt.out;
    var pre = $$("pre", out, null, "info", s);
    out.scrollTop = out.scrollHeight;
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function rIntOutAddRes(id, full, res, idx, resLnCnt) {
    var out = ui.tab.stat.rInt.out;
    var pre = $$("pre", out, null, "res", res + (!full ? "  " : ""));
    if (!full) {
      var spanLoad = $setAttr($$("span", pre, null, "load", "... (" + resLnCnt + ")"));
      spanLoad.onclick = function (e) {
        spanLoad.onclick = undefined;
        spanLoad.innerHTML = "loading...";
        rIntOutLoadFullRes(id, pre, idx);
      };
    }
    out.scrollTop = out.scrollHeight;
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function rIntOutLoadFullRes(id, pre, idx) {
    execCmd(true,
      { cmd: "r-int-get-res", "r-int-id": id },
      function (res) {
        var r = getCurrDS().rInt.recent[idx];
        r.res = res.res;
        r.full = true;
        
        pre.innerHTML = res.res;
      },
      null, null, true
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function rIntResetEng() {
    alert("Not implemented yet");
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function rIntSubmit() {
    if (!state.rInt.ready) return;
    
    state.rInt.ready = false;
    
    var cmd = $trim(ui.tab.stat.rInt.in.value);
    if (cmd.length === 0) return;
    
    ui.tab.stat.rInt.btnSubmit.disabled = true;
    
    ui.tab.stat.rInt.in.value = "";
    rIntOutAddCmd(cmd);
    
    var ri = getCurrDS().rInt;
    
    fnOk = function (res) {
      ri.cmdCnt++;
      ri.recent.push({ id: res.id, full: true, cmd: cmd, res: res.res });
      state.rInt.currIdx = ri.recent.length;
      
      rIntOutAddRes(null, true, res.res, -1, 0);
      
      ui.tab.stat.rInt.btnSubmit.disabled = false;
      state.rInt.ready = true;
    };
    
    execCmd(true,
      { cmd: "r-int-exec", code: encodeURIComponent(cmd.replace(/\+/g, "%2B")), "ds-id": getCurrDS().id },
      fnOk,
      fnOk,
      function (res) {
        state.rInt.currIdx = ri.recent.length;
        ui.tab.stat.rInt.btnSubmit.disabled = false;
        state.rInt.ready = true;
      },
      true
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function rScrClear() {
    if (!confirm("Clear the R script?")) return;
    ui.tab.stat.rScr.in.value = "";
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function rScrExec() {
    var s = $trim(ui.tab.stat.rScr.in.value);
    if (s.length === 0) return;
    
    var repName = prompt("Report name:");
    if (!repName) return;
    
    procShow("Executing the R script...");
    execCmd(false,
      { cmd: "r-scr-exec", "ds-id": getCurrDS().id, "rep-name": repName, code: encodeURIComponent(s.replace(/\+/g, "%2B")) },
      function (res) {
        repAdd2Tbl(res.rep);
        ui.panel.proc.p.hide();
      },
      null, null, true
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function scriptExec() {
    state.script.lst = $filter(function (x) { return $trim(x).length > 0; }, ui.tab.prep.scr.txt.value.split("\n"));
    var cmdCnt = state.script.lst.length;
    if (cmdCnt === 0) return;
    if (!confirm("Execute the script (" + cmdCnt + (cmdCnt === 1 ? " command" : " commands") + ")?")) return;
    
    state.script.active = true;
    state.script.idx = 0;
    scriptStep(false);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function scriptStep(wasLastCmdErr) {
    var fnFinish = function (msg) {
      state.script.active = false;
      alert(msg);
      ui.panel.proc.p.hide();
    };
    
    if (state.script.idx === state.script.lst.length) return fnFinish("All commands (" + state.script.idx + ") in the script have been executed.");
    
    if (wasLastCmdErr && !confirm("The last command did not execute succesfully. Continue the script execusion?")) {
      alert("Script execution stopped (" + state.script.idx + " command" + (state.script.idx === 1 ? " has" : "s have") + " been executed).");
      state.script.active = false;
      ui.panel.proc.p.hide();
      return;
    }
    
    try {
      procShow("Executing script (step " + (state.script.idx+1) + " of " + (state.script.lst.length) + ")")
      var fn = null;
      try {
        eval("fn = function () { script." + state.script.lst[state.script.idx] + "; }");
      }
      catch (ex) {}
      
      state.script.idx++;
      if (YAHOO.lang.isFunction(fn)) fn();
      else return fnFinish("Error executing the current command. Script execution will now be terminated.");
    }
    catch (ex) {}
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function tblRowGet_ds(o, idx, curr) { return $setProp($setObj({}, o), { idx: idx, curr: curr, ts: (!!o.tsVar), size: $size2str(o.size, true), noteLen: $size2str(o.noteLen, true) }); }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function tblRowGet_var(v, idx, curr) {
    var vsd = v.stat.desc;
    var vsn = v.stat.norm;
    var vsm = v.stat.misc;
    
    return {
      idx              : idx,
      curr             : curr,
      id               : v.id,
      type             : CONST.var.typeDispS[v.type],
      name             : v.name,
      memo             : v.memo,
      "missValCnt"     : vsm.missValCnt + "&nbsp;(" + (getCurrDS().obsCnt === 0 ? "-" : Math.round((vsm.missValCnt / getCurrDS().obsCnt) * 100)) + ")",
      "stat.desc.min"  : (vsd && vsd.min !== null  && !isNaN(vsd.min)  ? getVal(vsd.min,  2) : ""),
      "stat.desc.max"  : (vsd && vsd.max !== null  && !isNaN(vsd.max)  ? getVal(vsd.max,  2) : ""),
      "stat.desc.mean" : (vsd && vsd.mean !== null && !isNaN(vsd.mean) ? getVal(vsd.mean, 2) : ""),
      "stat.desc.med"  : (vsd && vsd.med !== null  && !isNaN(vsd.med)  ? getVal(vsd.med,  2) : ""),
      "stat.desc.sd"   : (vsd && vsd.sd !== null   && !isNaN(vsd.sd)   ? getVal(vsd.sd,   2) : ""),
      "stat.norm.ad"   : (vsn && !isNaN(vsn.ad.s)  && vsn.ad.s  !== "Infinity" ? (vsn.ad.p  < CONST.stat.alpha ? "" : "+" ) : ""),
      "stat.norm.cvm"  : (vsn && !isNaN(vsn.cvm.s) && vsn.cvm.s !== "Infinity" ? (vsn.cvm.p < CONST.stat.alpha ? "" : "+" ) : ""),
      "stat.norm.ks"   : (vsn && !isNaN(vsn.ks.s)  && vsn.ks.s  !== "Infinity" ? (vsn.ks.p  < CONST.stat.alpha ? "" : "+" ) : ""),
      "stat.norm.sw"   : (vsn && !isNaN(vsn.sw.s)  && vsn.sw.s  !== "Infinity" ? (vsn.sw.p  < CONST.stat.alpha ? "" : "+" ) : "")
    };
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function txtShow(title, msg, txt, edit, note) {
    var u = ui.panel.txt;
    
    if (edit) {
      u.edit.value = txt;
      $hide(u.view);
      $show(u.edit);
    }
    else {
      u.view.innerHTML = txt;
      $hide(u.edit);
      $show(u.view);
    }
    
    if (note) u.p.hideEvent.subscribe(u.fnNoteHide);
    
    u.msg.innerHTML = (msg && msg.length > 0 ? msg + "<br /><br />" : "");
    u.p.setHeader(title);
    
    u.p.show();
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function uiResizeDataTbl(dt, x, y) {
    var regB = ui.region.app.body;
    var regDT = YAHOO.util.Dom.getRegion(dt.getTableEl());
    if (x) {
      var adj = regDT.right > (regB.right - 40);
      if (adj) dt.set("width", (regB.width - regDT.left) + "px");
    }
    if (y) {
      var adj = regDT.bottom > (regB.bottom - 40);
      if (adj) dt.set("height", (regB.height - regDT.top) + "px");
    }
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function varDel() {
    var name = getCurrVar().name;
    if (!confirm("You are about to delete the active variable ('" + name + "'). This operation is irreversible.")) return;
    
    procShow("Deleting a variable...");
    execCmd(true,
      { cmd: "var-del", "var-id": getCurrVar().id },
      function (res) {
        // Variables data table:
        data.ds[state.currDS.idx].var[state.currVar.idx] = null;
        ui.tbl.vars.tbl.deleteRow(state.currVar.ui.rec);
        state.currVar = { idx: -1, ui: { rec: null, radio: null } };
        
        resetCurrState("currDS");
        varSetBtnsEnabled(false);
        
        // Data sets data table:
        var ds = getCurrDS();
        var o = { idx: state.currDS.idx, curr: true };
        $setObj(o, ds);
        o.size = $size2str(ds.size, true);
        o.varCnt--;
        ui.tbl.ds.tbl.updateRow(state.currDS.idx, o);
        
        ui.panel.proc.p.hide();
      },
      null, null, true
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function varNew() {
    alert("Not implemented yet");
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function varMarkTS() {
    var v = getCurrVar();
    if (v.type !== CONST.var.type.dis) return alert("Only a discrete variable can be marked as a time series indicator.");
    
    procShow("Marking as time series...");
    execCmd(true,
      { cmd: "var-mark-ts", "var-id": v.id },
      function (res) {
        /*
        v.ts = !!!v.ts;
        ui.tbl.ds.tbl.updateRow(state.currDS.idx, o);
        */
        
        ui.panel.proc.p.hide();
      },
      null, null, true
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function varSetBtnsEnabled(e) {
    var u = ui.tab.prep.vars;
    u.btnDel.set("disabled", !e);
    u.btnMarkTS.set("disabled", !e);
    u.btnType.set("disabled", !e);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function varRename(fromScr, dsName, varNameSrc, varNameDst) {
    if (!fromScr) {
      // ...
      
      procShow("Renaming variables...");
    }
    
    execCmd(true,
      { cmd: "ds-var-rename", "prj-id": state.prjId, "ds-name": dsName, "var-name-src": varNameSrc, "var-name-dst": varNameDst },
      function (res) {
        if (state.script.active) scriptStep(false);
        else ui.panel.proc.p.hide();
      },
      null, null, true
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function varTrans(fromScr, dsName, name, type, val) {
    var u = ui.panel.varTrans;
    
    if (!fromScr) {
      if (!u.inpVal.value) return alert("Value cannot be empty.");
      if (!u.inpName.value) return alert("Name cannot be empty.");
      
      var nameExists = false;
      $map(function (v) { if (!nameExists) nameExists = (nameExists || v.name === u.inpName.value); }, getCurrDS().var);
      if (nameExists) return alert("A variable with the specified name already exists in the current data set.");
      
      var dsName = getCurrDS().name;
      var name   = u.inpName.value;
      var type   = u.lstType.get("value");
      var val    = u.inpVal.value;
      
      procShow("Transforming variable...");
    }
    
    execCmd(true,
      { cmd: "ds-var-trans", "prj-id": state.prjId, "ds-name": dsName, name: encodeURIComponent(name), type: type, val: encodeURIComponent(encodeURIComponent(val)) },
      function (res) {
        var ds = getCurrDS();
        if (ds && ds.name === dsName) {
          ds.var.push(res.var);
          ds.varCnt++;
        
          //refreshTab_PrepVars();
          var t = ui.tbl.vars.tbl;
          t.addRow(tblRowGet_var(res.var, ds.varCnt));
          t.render();
          //Pegasus.Main.tblResort(t);
        }
        
        cmdResShow("Variable transformation complete: Variable '" + name + "' has been created");
        
        if (state.script.active) scriptStep(false);
        else {
          u.p.hide();
          ui.panel.proc.p.hide();
        }
      },
      null, null, true
    );
  }
  
  
  // ===================================================================================================================
  // ==[ SCRIPT ]=======================================================================================================
  // ===================================================================================================================
  
  var script = {
    
    // -----------------------------------------------------------------------------------------------------------------
    // TODO: arguments validation
    dsDel:       function (dsName) { dsDel01_name(true, dsName); },
    dsDelVar:    function (dsName, spec) { dsDelVar(true, dsName, spec); },
    dsDiscret:   function (dsNameSrc, dsNameDst, spec) { dsDiscret(true, dsNameSrc, dsNameDst, spec); },
    dsDup:       function (dsNameSrc, dsNameDst, cond) { dsDup(true, dsNameSrc, dsNameDst, cond); },
    dsRepVal:    function (dsNameSrc, dsNameDst, spec) { dsRepVal(true, dsNameSrc, dsNameDst, spec); },
    dsStats:     function (dsNameSrc, dsNameDst, spec, sep) { dsStats(true, dsNameSrc, dsNameDst, spec, sep); },
    dsExport:    function (dsName, filename, format, delimVar, delimObs, missVal, zip) { dsExport(true, dsName, filename, format, delimVar, delimObs, missVal, zip); },
    dsTSAddVar:  function (dsName, spec) { dsTSAddVar(true, dsName, spec); },
    dsTSWide:    function (dsNameSrc, dsNameDst) { dsTSWide(true, dsNameSrc, dsNameDst); },
    
    dsMerge: function (dsResName, idxVarName, spec) {
      var spec2 = $map(function (x) { return $enc(x.name) + ":" + x.idx }, spec).join(",");
      dsMerge02(true, dsResName, idxVarName, spec2);
    },
    
    varRename: function (dsName, varNameSrc, varNameDst) { varRename(true, dsName, varNameSrc, varNameDst); },
    varTrans:  function (dsName, varName, type, val) { varTrans(true, dsName, varName, type, val); }
  };
  
  
  // ===================================================================================================================
  // ==[ PUBLIC ]=======================================================================================================
  // ===================================================================================================================
  
  var pub = {
    
    // -----------------------------------------------------------------------------------------------------------------
    getAppId: function () { return CONST.appId; },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    getCmdURI: function (spec) { return CONST. serverURI_L + "?" + getCmd(spec); },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    start: function (prjId) {
      state.prjId = prjId;
      Pegasus.Main.appReg(CONST.appId, window, Pegasus.Data);
      
      initProcPanel();
      procShow("Loading (initializing UI)...");
      
      initMain();
      initDSPanel();
      initDSDupPanel();
      initDSDiscretPanel();
      initDSMergePanel();
      initDSRepValPanel();
      initDSStatsPanel();
      initVarReordPanel();
      initVarTypePanel();
      initVarTransPanel()
      initPrepData();
      initPrepScript();
      initPrepVars();
      initStatRInt();
      initStatRScr();
      initStatRep();
      initBNetTmp();
      
      initTxtPanel();
      
      initApp();
      
      YAHOO.util.Event.on(window, "resize", afterWinResize);
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    stop: function (msg) {
      if (msg) {
        $show(ui.app.mask);
        alert(msg);
      }
      window.close();
    }
  };
  
  
  // ===================================================================================================================
  // ==[ CONSTRUCT ]====================================================================================================
  // ===================================================================================================================
  
  construct();
  return pub;
}();


/*
TODO:
- make use of getCurrDS() and the other methods everywhere
*/
