/**
 * Data set manager
 * -----------------------
 * 
 * DEPENDS ON:
 *   $.js
 */


var DSMan = function() {
  
  // ===================================================================================================================
  // ==[ PRIVATE ]======================================================================================================
  // ===================================================================================================================
  
  var CONST = {
    serverURL : location.protocol + "//" + location.host + "/pegasus/servlet/main.Do"
  };
  
  var data = {
    colOrd : [],
    cols   : {},
    vars   : {}
  };
  
  var state = {
    curr : {
      ds : { name: "..." }
    }
  };
  
  var ui = {
    main : {
      cont   : null,
      dsLst  : { cont: null },
      varLst : { cont: null, tbl: null }
    },
    
    panel : {
      proc     : { p: null },
      plotDist : { p: null, lstX1: null, lstX2: null, lstX3: null, lstPlot: null, lstType: null, img: null },
      plotScat : { p: null }
    },
    
    sbar  : null,
    tbar  : { cont : null, lstDS: null, btnOpen: null, btnDist: null }
  };
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function construct() {
    initProc();
    procShow("Loading...");
    
    initTBar();
    initSBar();
    initMain();
    initDist();
    initDS();
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function distPlot() {
    procShow("Plotting...");
    
    var u = ui.panel.plotDist;
    var url = CONST.serverURL + "?username=tomek&app=data&cmd=ds-plot-dist&ds=" + state.curr.ds.name + "&plot=" + u.lstPlot.value + "&x1=" + u.lstX1.value + "&x2=" + u.lstX2.value + "&x3=" + u.lstX3.value + "&type=" + u.lstType.value + "&layout=" + u.lstLayout01.value + "," +  + u.lstLayout02.value;
    var cb = function (res) {
      if (res.outcome) {
        ui.panel.plotDist.img.src = CONST.serverURL + "/../../ds/" + state.curr.ds.name + "/work/dist.png?" + Math.random();
        ui.panel.proc.p.hide();
      }
      else {
        ui.panel.proc.p.hide();
        alert(res.msg);
      }
    };
    $call("get", url, null, cb, true);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function dsOpen() {
    procShow("Retrieving data...");
    
    var url = CONST.serverURL + "?username=tomek&app=data&cmd=ds-open&ds=" + state.curr.ds.name;
    var cb = function (res) {
      if (res.outcome) {
        digestVars(res);
        
        var lst01 = ui.panel.plotDist.lstX1;
        var lst02 = ui.panel.plotDist.lstX2;
        var lst03 = ui.panel.plotDist.lstX3;
        $removeChildren(lst01);
        $removeChildren(lst02);
        $removeChildren(lst03);
        
        $setAttr($$("option", lst01, null, null, "--- A ---"), { selected: "selected"}).value = "";
        $setAttr($$("option", lst02, null, null, "--- B ---"), { selected: "selected"}).value = "";
        $setAttr($$("option", lst03, null, null, "--- C ---"), { selected: "selected"}).value = "";
        
        for (var v in data.vars) {
          $$("option", lst01, null, null, v).value = v;
          $$("option", lst02, null, null, v).value = v;
          $$("option", lst03, null, null, v).value = v;
        }
        
        ui.tbar.btnDist.disabled = false;
      }
      else {
        ui.panel.proc.p.hide();
        alert(res.msg);
      }
    };
    $call("get", url, null, cb, true);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initDist() {
    var panel = new YAHOO.widget.Dialog("panel-plot-dist",
      {
        buttons             : [
          { text: "Plot",  handler: distPlot, isDefault: true },
          { text: "Close", handler: function (e) { ui.panel.plotDist.p.hide(); } }
        ],
        close               : true,
        constraintoviewport : true,
        draggable           : true,
        fixedcenter         : true,
        modal               : true,
        visible             : false
      }
    );
    panel.render();
    ui.panel.plotDist.p = panel;
    
    ui.panel.plotDist.lstX1 = $("panel-plot-dist-tbar-x1");
    ui.panel.plotDist.lstX2 = $("panel-plot-dist-tbar-x2");
    ui.panel.plotDist.lstX3 = $("panel-plot-dist-tbar-x3");
    
    ui.panel.plotDist.lstPlot = $("panel-plot-dist-tbar-plot");
    ui.panel.plotDist.lstType = $("panel-plot-dist-tbar-type");
    
    ui.panel.plotDist.lstLayout01 = $("panel-plot-dist-tbar-layout-01");
    ui.panel.plotDist.lstLayout02 = $("panel-plot-dist-tbar-layout-02");
    
    ui.panel.plotDist.img = $("panel-plot-dist-main-img");
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initDS() {
    var url = CONST.serverURL + "?username=tomek&app=data&cmd=ds-lst";
    var cb = function (res) {
      if (res.outcome) {
        if (res.lst.length === 0) return;
        
        var lst = ui.tbar.lstDS;
        $removeChildren(lst);
        $setAttr($$("option", lst, null, null, "--- dataset ---"), { selected: "selected"}).value = "";
        for (var i=0, ni=res.lst.length; i < ni; i++) {
          $$("option", lst, null, null, res.lst[i]).value = res.lst[i];
        }
        
        ui.panel.proc.p.hide();
      }
      else {
        alert(res.msg);
        procShow("Unrecoverable error has occured. This application will not terminate.");
      }
    };
    $call("get", url, null, cb, true);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initMain() {
    ui.main.cont = $("main");
    
    ui.main.dsLst.cont = $("main-ds-lst");
    
    ui.main.varLst.cont = $("main-var-lst");
    ui.main.varLst.tbl  = $$("table", ui.main.varLst.cont);
    ui.main.varLst.tbl.setAttribute("cellpadding", "0");
    ui.main.varLst.tbl.setAttribute("cellspacing", "0");
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initProc() {
    ui.panel.proc.p = new YAHOO.widget.Panel("panel-proc", 
      {
        width       : "240px",
        fixedcenter : true,
        close       : false,
        draggable   : false,
        zindex      : 200,
        modal       : true,
        visible     : false
      }
    );
    ui.panel.proc.p.render();
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initSBar() {
    ui.sbar = $("sbar");
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initTBar() {
    ui.tbar.cont = $("tbar");
    
    ui.tbar.lstDS = $$("select", ui.tbar.cont);
    $$("option", ui.tbar.lstDS, null, null, "--- dataset ---").value = "";
    
    ui.tbar.lstDS.onchange = function (e) {
      state.curr.ds.name = ui.tbar.lstDS.value;
      ui.tbar.btnOpen.disabled = (state.curr.ds.name.length === 0);
    }
    
    ui.tbar.btnOpen = $$("input", ui.tbar.cont);
    $setAttr(ui.tbar.btnOpen, { type: "button", value: "Open", disabled: true });
    $setProp(ui.tbar.btnOpen, { onclick: function (e) { dsOpen(); } });
    
    ui.tbar.btnDist = $$("input", ui.tbar.cont);
    $setAttr(ui.tbar.btnDist, { type: "button", value: "Distribution", disabled: true });
    $setProp(ui.tbar.btnDist, { onclick: function (e) { ui.panel.plotDist.p.show(); } });
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function procShow(msg) {
    ui.panel.proc.p.setHeader(msg);
    ui.panel.proc.p.show();
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function digestVars(o) {
    // (1) Init:
    $removeChildren(ui.main.varLst.tbl);
    
    data.vars   = {};
    data.cols   = {};
    data.colOrd = [];
    
    //for (var c in o.colOrd) data.colOrd.push(c);
    
    var tr = $$("tr", ui.main.varLst.tbl);
    
    // (2) Header:
    $$("th", tr);
    $$("th", tr);
    for (var j=0, nj=o.vars[0].cols.length; j < nj; j++) {
      var c = o.vars[0].cols[j];
      if (c.type !== "sep") $$("th", tr, null, null, c.name);
    }
    
    // (3) Data:
    for (var i=0, ni=o.vars.length; i < ni; i++) {
      var v = o.vars[i];
      
      tr = $$("tr", ui.main.varLst.tbl, null, (i % 2 ? "even" : null));
      
      var chbox = $$("input", $$("td", tr));
      $setAttr(chbox, { type: "checkbox" });
      
      data.vars[v.name]      = $getSubObj(v, ["name", "type"]);
      data.vars[v.name].cols = {}
      data.vars[v.name].ui   = { tr: tr };
      
      var lastTD = $$("td", tr, null, null, "<b>" + v.name + "</b>");
      
      for (var j=0, nj=v.cols.length; j < nj; j++) {
        var c = v.cols[j];
        
        data.vars[v.name].cols[c.name] = {};
        var vc = data.vars[v.name].cols[c.name];
        
        vc = c;
        
        if (c.type === "num") {
          vc.ui  = { td: $$("td", tr, null, null, (!isNaN(c.val) && c.val.toFixed ? $trunc(c.val, 4) : c.val)) };
          lastTD = function (x) { return x; }(vc.ui.td);
        }
        else if (c.type === "str") {
          vc.ui  = { td: $$("td", tr, null, null, c.val) };
          lastTD = function (x) { return x; }(vc.ui.td);
        }
        else if (c.type === "img") {
          vc.ui  = { td: $$("td", tr) };
          
          var img = $$("img", vc.ui.td);
          $setAttr(img, { src: "../ds/" + state.curr.ds.name + "/img-sys/" + c.val, name: v.name });
          img.style.height = "12px";
          img.style.border = "1px solid #000000";
          img.onclick = function (_img) {
            return function (e) {
              if (_img.style.height === "12px") _img.style.height = "240px";
              else if (_img.style.height === "240px") _img.style.height = "";
              else _img.style.height = "12px";
            }
          }(img);
          
          lastTD = function (x) { return x; }(vc.ui.td);
        }
        else if (c.type === "sep") {
          lastTD.style.borderRight = "1px dotted #bbbbbb";
        }
      }
    }
    
    // (4) Finish:
    ui.panel.proc.p.hide();
  }
  
  
  // ===================================================================================================================
  // ==[ PUBLIC ]=======================================================================================================
  // ===================================================================================================================
  
  var pub = {
  };
  
  
  // ===================================================================================================================
  // ==[ CONSTRUCT ]====================================================================================================
  // ===================================================================================================================
  
  construct();
  return pub;
};
