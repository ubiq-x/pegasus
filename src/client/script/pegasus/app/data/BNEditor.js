/**
 * Bayesian network editor
 * -----------------------
 * 
 * DEPENDS ON:
 *   $.js
 */


var BNEditor = function(_cont, _dsId, _dsVars) {
  
  // ===================================================================================================================
  // ==[ PRIVATE ]======================================================================================================
  // ===================================================================================================================
  
  var CONST = {
    serverURL : location.protocol + "//" + location.host + "/pegasus/servlet/main.Do"
  };
  
  var bn = null;
  
  var state = {
    curr  : { tbar: null },
    learn : {
      alg  : "pc",
      ds   : { id: _dsId, vars: {}, varCnt: 0, selCnt: 0 }
    },
  };
  
  var ui = {
    cont    : _cont,
    img     : null,
    sbar    : null,
    
    learn   : {
      varLst : null
    },
    
    panel   : {
      proc : { p: null }
    },
    
    tbar    : {
      cont        : null,
      build       : {
        cont             : null,
        btnLearn         : null,
        btnNetNew        : null,
        btnNewArc        : null,
        btnNewVar        : null,
        btnUpdateBeliefs : null
      },
      learnBg     : {
        cont         : null,
        btnArcForce  : null,
        btnArcForbid : null
      },
      learnParams : {
        cont : null
      },
      learnStruct : {
        cont      : null,
        btnRandom : null
      },
      learnVars   : {
        cont   : null,
        btnAlg : null,
        lst    : null
      },
      reason      : {
        cont : null
      },
      zoom : {
        cont             : null,
        btnZoomIn        : null,
        btnZoomOut       : null,
        btnZoomReset     : null
      }
    },
    
    wizard  : {
      btnCancel : null,
      btnPrev   : null,
      btnNext   : null,
      cont      : null
    }
  };
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function buildNetNew() {
    if (bn.vars.length === 0) return sbarSet("New network: Done");
    if (confirm("Are you sure you want to discard the current network?")) {
      bn.reset();
      sbarSet("New network: Done");
    }
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function buildNewArc() {
    bn.modeSetNewArc01();
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function buildNewVar() {
    bn.modeSetNewVar();
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function construct() {
    initProc();
    procShow("Loading...");
    
    initImg();
    initUI();
    
    tbarSet("build");
    sbarSet("Ready");
    
    ui.panel.proc.p.hide();
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initImg() {
    ui.img = $$("embed", ui.cont, "img");
    ui.img.setAttribute("type", "image/svg+xml");
    ui.img.setAttribute("src", "../../svg/bn.svg");
    
    window.setTimeout(
      function () {
        bn = ui.img.getSVGDocument().defaultView;
        bn.setEditor(pub);
      },
      1000
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initProc() {
    ui.panel.proc.p = new YAHOO.widget.Panel("panel-proc", 
      {
        width       : "240px",
        fixedcenter : true,
        close       : false,
        draggable   : false,
        zindex      : 200,
        modal       : true,
        visible     : false
      }
    );
    ui.panel.proc.p.render();
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initUI() {
    // (1) Toolbar:
    ui.tbar.build.cont = $("tbar-build");
    ui.tbar.build.btnNetNew        = new YAHOO.widget.Button({ container: ui.tbar.build.cont, label: "New network", onclick: { fn: function (e) { buildNetNew(); }}});
    ui.tbar.build.btnNewVar        = new YAHOO.widget.Button({ container: ui.tbar.build.cont, label: "New var", onclick: { fn: function (e) { buildNewVar(); }}});
    ui.tbar.build.btnNewArc        = new YAHOO.widget.Button({ container: ui.tbar.build.cont, label: "New arc", onclick: { fn: function (e) { buildNewArc(); }}});
    ui.tbar.build.btnLearn         = new YAHOO.widget.Button({ container: ui.tbar.build.cont, label: "Learn", onclick: { fn: function (e) { learnVars(); }}});
    ui.tbar.build.btnLearnParams   = new YAHOO.widget.Button({ container: ui.tbar.build.cont, label: "Learn parameters", onclick: { fn: function (e) { learnParams2(); }}});
    ui.tbar.build.btnUpdateBeliefs = new YAHOO.widget.Button({ container: ui.tbar.build.cont, label: "Update beliefs", onclick: { fn: function (e) { reasonUpdateBeliefs(); } }});
                                     new YAHOO.widget.Button({ container: ui.tbar.build.cont, label: "Sample network", onclick: { fn: function (e) { sampleNet(); } } });
    
    ui.tbar.reason.cont = $("tbar-reason");
    
    ui.tbar.learnVars.cont = $("tbar-learn-vars");
    
    function fnBtnAlgClick(p_sType, p_aArgs, p_oItem) {
      ui.tbar.learnVars.btnAlg.set("label", p_oItem.cfg.getProperty("text"));
      ui.tbar.learnVars.btnAlg.set("value", p_oItem.value);
      state.learn.alg = p_oItem.value;
    }
    
    ui.tbar.learnVars.btnAlg = new YAHOO.widget.Button({
      container : ui.tbar.learnVars.cont,
      type      : "menu",
      label     : "PC",
      value     : "pc",
      menu      : [
        { text: "PC",   value: "pc",   onclick: { fn: fnBtnAlgClick } },
        { text: "DBCL", value: "dbcl", onclick: { fn: fnBtnAlgClick } }
      ]
    });
    
    ui.tbar.learnBg.cont = $("tbar-learn-bg");
    ui.tbar.learnBg.btnArcForce  = new YAHOO.widget.Button({ container: ui.tbar.learnBg.cont, label: "Force arc", onclick: { fn: function (e) { learnBgArcForce(); }}});
    ui.tbar.learnBg.btnArcForbid = new YAHOO.widget.Button({ container: ui.tbar.learnBg.cont, label: "Forbid arc", onclick: { fn: function (e) { learnBgArcForbid(); }}});
    
    ui.tbar.learnStruct.cont = $("tbar-learn-struct");
    ui.tbar.learnStruct.btnRandom = new YAHOO.widget.Button({ container: ui.tbar.learnStruct.cont, label: "Orient randomly", onclick: { fn: function (e) { learnStructRandom(); }}});
    
    ui.tbar.learnParams.cont = $("tbar-learn-params");
    
    ui.tbar.zoom.cont = $("tbar-zoom");
    ui.tbar.zoom.btnZoomIn    = new YAHOO.widget.Button({ container: ui.tbar.zoom.cont, label: "+", onclick: { fn: function (e) { zoomIn(); } }});
    ui.tbar.zoom.btnZoomOut   = new YAHOO.widget.Button({ container: ui.tbar.zoom.cont, label: "-", onclick: { fn: function (e) { zoomOut(); } }});
    ui.tbar.zoom.btnZoomReset = new YAHOO.widget.Button({ container: ui.tbar.zoom.cont, label: "0", onclick: { fn: function (e) { zoomReset(); } }});
    
    // (2) Status bar:
    ui.sbar = $("sbar");
    
    // (3) Wizard:
    ui.wizard.cont = $("wizard-nav");
    ui.wizard.btnPrev   = new YAHOO.widget.Button({container: ui.wizard.cont, label: "Prev" });
    ui.wizard.btnNext   = new YAHOO.widget.Button({container: ui.wizard.cont, label: "Next" });
    ui.wizard.btnCancel = new YAHOO.widget.Button({container: ui.wizard.cont, label: "Cancel" });
    
    // (4) Other:
    ui.learn.varLst = $("learn-vars-lst");
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function learnBg() {
    if (state.learn.ds.selCnt === 0) return alert("No variables have been selected.");
    
    ui.wizard.btnPrev.set("disabled", true);
    ui.wizard.btnNext.set("disabled", true);
    
    tbarSet("learnBg");
    ui.tbar.zoom.cont.style.display = "inline";
    sbarSet("Learning: Background knowledge (step 2 of 3)");
    
    $hide(ui.learn.varLst);
    bn.reset();
    bn.modeSetLearnBg();
    $show(ui.img);
    for (var v in state.learn.ds.vars) {
      if (state.learn.ds.vars[v].sel) bn.varAdd(0,0, state.learn.ds.vars[v].name);
    }
    bn.layoutCircle();
    
    ui.wizard.btnPrev.set("onclick", { fn: function (e) { learnVars(); } });
    ui.wizard.btnNext.set("onclick", { fn: function (e) { learnStruct(); } });
    
    ui.wizard.btnPrev.set("disabled", false);
    ui.wizard.btnNext.set("disabled", false);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function learnBgArcForbid() {
    alert("Forbid an arc: not implemented yet");
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function learnBgArcForce() {
    alert("Force an arc: not implemented yet");
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function learnParams() {
    if (!bn.arcsAllDir()) return alert("All arcs need to be directed.");
    
    procShow("Learning parameters...");
    sbarSet("Performing parameter learning...");
    
    var url = CONST.serverURL + "?username=tomek&app=data&cmd=bn-learn-params-pattern&res-id-pattern=" + bn.resIds.pattern + "&res-id-dataset=" + bn.resIds.dataset + "&arcs=" + bn.arcsGet();
    var cb = function (res) {
      if (res.outcome) {
        tbarSet("build");
        sbarSet("Learning: Done");
        
        bn.arcsFixAll();
        bn.digestEqns(res.bn);
        bn.modeSetBuild();
        
        ui.wizard.btnPrev.set("onclick", undefined);
        ui.wizard.btnNext.set("onclick", undefined);
        $hide(ui.wizard.cont);
        
        ui.panel.proc.p.hide();
      }
      else {
        ui.panel.proc.p.hide();
        alert(res.msg);
        sbarSet("Learning: Arcs orientation (step 1 of 2)");
      }
    };
    $call("get", url, null, cb, true);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function learnParams2() {
    procShow("Learning parameters...");
    sbarSet("Performing parameter learning...");
    
    var url = CONST.serverURL + "?username=tomek&app=data&cmd=bn-learn-params-dag";
    var cb = function (res) {
      if (res.outcome) {
        tbarSet("build");
        sbarSet("Learning: Done");
        
        bn.arcsFixAll();
        bn.digestEqns(res.bn);
        bn.modeSetBuild();
        
        ui.wizard.btnPrev.set("onclick", undefined);
        ui.wizard.btnNext.set("onclick", undefined);
        $hide(ui.wizard.cont);
        
        ui.panel.proc.p.hide();
      }
      else {
        ui.panel.proc.p.hide();
        alert(res.msg);
        sbarSet("Learning: Arcs orientation (step 1 of 2)");
      }
    };
    $call("get", url, null, cb, true);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function learnStruct() {
    procShow("Learning structure and laying out...");
    sbarSet("Performing structural learning and graph layout...");
    
    var vars = "";
    for (var v in state.learn.ds.vars) {
      if (state.learn.ds.vars[v].sel) vars += state.learn.ds.vars[v].name + ",";
    }
    vars = vars.substr(0, vars.length-1);  // remove the trailing comma
    
    var url = CONST.serverURL + "?username=tomek&app=data&cmd=bn-learn-struct&alg=" + state.learn.alg + "&ds-id=" + state.learn.ds.id + "&vars=" + vars;
    var cb = function (res) {
      if (res.outcome) {
        tbarSet("learnStruct");
        sbarSet("Learning: Arcs orientation (step 1 of 2)");
        
        bn.reset();
        bn.digestNet(res.bn, res.resIds);
        bn.modeSetLearnStruct();
        
        ui.wizard.btnPrev.set("onclick", { fn: function (e) { learnBg(); } });
        ui.wizard.btnNext.set("onclick", { fn: function (e) { learnParams(); } });
        
        ui.panel.proc.p.hide();
      }
      else {
        ui.panel.proc.p.hide();
        alert(res.msg);
        sbarSet("Learning: Background knowledge (step 2 of 3)");
      }
    };
    $call("get", url, null, cb, true);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function learnVars(vars) {
    $hide(ui.tbar.zoom.cont);
    $hide(ui.img);
    procShow("Retrieving dataset information...");
    sbarSet("Learning: Retrieving dataset information...");
    
    var fnUI = function () {
      tbarSet("learnVars");
      sbarSet("Learning: Variables (step 1 of 3)");
      
      ui.wizard.btnPrev.set("disabled", true);
      ui.wizard.btnNext.set("disabled", false);
      
      ui.wizard.btnCancel.set("onclick", { fn: function (e) { if (!confirm("Cancel learning?")) return; state.learn.ds.vars = []; $hide(ui.wizard.cont); tbarSet("build"); sbarSet("Learning: Canceled"); bn.reset(); $show(ui.img); } });
      ui.wizard.btnNext.set("onclick", { fn: function (e) { learnBg(); } });
      $show(ui.wizard.cont);
      
      $show(ui.learn.varLst);
      
      ui.panel.proc.p.hide();
    };
    
    if (state.learn.ds.varCnt === 0) {
      var url = CONST.serverURL + "?username=tomek&app=data&cmd=ds-get-vars&ds-id=" + state.learn.ds.id;
      var cb = function (res) {
        if (res.outcome) {
          $removeChildren(ui.learn.varLst);
          state.learn.ds.vars = {};
          $map(
            function (v) {
              var div = $$("div", ui.learn.varLst, null, "off", v);
              div.onclick = function (div, v) {
                return function (e) {
                  div.className = (div.className === "off" ? "on" :  "off");
                  state.learn.ds.vars[v].sel = !state.learn.ds.vars[v].sel;
                  state.learn.ds.selCnt += (state.learn.ds.vars[v].sel ? 1 : -1);
                }
              }(div, v);
              
              state.learn.ds.vars[v] = { div: div, name: v, sel: false };
              state.learn.ds.varCnt++;
            },
            res.vars
          );
          
          if (vars) {
            $map(
              function (v) {
                if (state.learn.ds.vars[v]) state.learn.ds.vars[v].div.onclick();
              },
              vars.split(",")
            );
          }
          
          fnUI();
        }
        else {
          ui.panel.proc.p.hide();
          alert(res.msg);
          sbarSet("Learning: Unsuccessful");
          $hide(ui.learn.varLst);
          $show(ui.img);
        }
      };
      $call("get", url, null, cb, true);
    }
    else fnUI();
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function learnStructRandom() {
    alert("Orient randomly: not implemented yet");
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function procShow(msg) {
    ui.panel.proc.p.setHeader(msg);
    ui.panel.proc.p.show();
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function sampleNet() {
    bn.reset();
    
    bn.varAdd(275,75, "A", "This is some cool variable");
    
    bn.varAdd(275,175, "B", "B");
    
    bn.varAdd(50,300, "C", "C");
    bn.varAdd(100,300, "D", "D");
    bn.varAdd(150,300, "E", "E");
    bn.varAdd(200,300, "F", "F");
    bn.varAdd(250,300, "G", "G");
    bn.varAdd(300,300, "H", "H");
    bn.varAdd(350,300, "I", "I");
    
    bn.varAdd(500,325, "J", "J");
    
    bn.varAdd(400,380, "K", "K");
    bn.varAdd(450,420, "L", "L");
    bn.varAdd(500,450, "M", "M");
    bn.varAdd(550,430, "N", "N");
    bn.varAdd(600,450, "O", "O");
    bn.varAdd(650,400, "1234", "1234");
    bn.varAdd(700,350, "12345", "12345");
    
    bn.varAdd(460,200, "P", "P");
    bn.varAdd(550,225, "Q", "Q");
    
    bn.varAdd(550,550, "R", "R");
    bn.varAdd(650,550, "S", "S");
    
    bn.arcAdd(0,1, true, true);
    
    bn.arcAdd(1,2, true, true);
    bn.arcAdd(1,3, true, true);
    bn.arcAdd(1,4, true, true);
    bn.arcAdd(1,5, true, true);
    bn.arcAdd(1,6, true, true);
    bn.arcAdd(1,7, true, true);
    bn.arcAdd(1,8, true, true);
    bn.arcAdd(1,9, true, true);
    
    bn.arcAdd(9,10, true, true);
    bn.arcAdd(9,11, true, true);
    bn.arcAdd(9,12, true, true);
    bn.arcAdd(9,13, true, true);
    bn.arcAdd(9,14, true, true);
    bn.arcAdd(9,15, true, true);
    bn.arcAdd(9,16, true, true);
    
    bn.arcAdd(17,9, true, true);
    bn.arcAdd(18,9, true, true);
    
    bn.arcAdd(14,19, true, true);
    bn.arcAdd(14,20, true, true);
    
    sbarSet("Sample network: Done");
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function sbarSet(txt) {
    ui.sbar.innerHTML = txt;
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function tbarSet(t) {
    if (state.curr.tbar) $hide(ui.tbar[state.curr.tbar].cont);
    ui.tbar[t].cont.style.display = "inline";
    state.curr.tbar = t;
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function reasonUpdateBeliefs() {
    if (bn.vars.length === 0) return sbarSet("Update beliefs: The network contains no nodes");
    
    sbarSet("Updating beliefs...");
    procShow("Updating beliefs...");
    
    var url = CONST.serverURL + "?username=tomek&app=data&cmd=bn-update-beliefs";
    var cb = function (res) {
      if (res.outcome) {
        bn.digestValues(res.bn);
        sbarSet("Update beliefs: Done");
        ui.panel.proc.p.hide();
      }
      else {
        sbarSet("Update beliefs: Unsuccessful");
        ui.panel.proc.p.hide();
        alert(res.msg);
      }
    };
    $call("get", url, null, cb, true);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function zoomIn() {
    bn.zoomIn();
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function zoomOut() {
    bn.zoomOut();
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function zoomReset() {
    bn.zoomReset();
  }
  
  
  // ===================================================================================================================
  // ==[ PUBLIC ]=======================================================================================================
  // ===================================================================================================================
  
  var pub = {
    
    // -----------------------------------------------------------------------------------------------------------------
    ehKeyPress: function (e) {
      bn.digestKeyPress($evtChar(e), $evtCode(e));
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    getNet: function () { return bn; },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    learnStruct: function (dsId, vars) {
      state.learn.ds.id = dsId;
      learnVars(vars);
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    setStatusBar: function (txt) {
      sbarSet(txt);
    }
  };
  
  
  // ===================================================================================================================
  // ==[ CONSTRUCT ]====================================================================================================
  // ===================================================================================================================
  
  construct();
  return pub;
};
