/**
 * Pegasus: Gaze
 * -------------
 * 
 * DEPENDS ON:
 *   $.js
 *   Validation.js
 */


if (!Pegasus) var Pegasus = {};

Pegasus.Gaze = function() {
  
  // ===================================================================================================================
  // ==[ PRIVATE ]======================================================================================================
  // ===================================================================================================================
  
  var CONST = {
    appId       : "gaze",
    cal         : {
      map: {w: 400, h: 300 }
    },
    cookie      : { appName : "Pegasus-Gaze-" },
    tbl: { exc: "&bull;" },
    err         : {
      unexpected: "An unexpected error has occurred. This might be due to loss of connection or server side error. Please try again. If you are able to connect to the server but the error persists please notify the system administrator (Tomek Loboda, the.snake@gmail.com)."
    },
    exp         : {
      fixDur : { min: 0, max: 10000 }
    },
    panelW      : "900px",
    serverURI_S : "do",
    serverURI_L : location.protocol + "//" + location.host + "/pegasus/do",
    stim        : {
      ftFamily   : { def: "Courier New" },
      ftSize     : { min:     2, max:  100 },
      letterSpac : { min:    -5, max:    5 },
      lineH      : { min:     1, max: 1000 },
      margin     : { min: -1000, max: 1000 },
      offset     : { min: -1000, max: 1000 },
      padding    : { min: -1000, max: 1000 },
      roiH       : { min:    10, max:  300 },
      res        : {
        h: { min: 100, max: 2000 },
        v: { min: 100, max: 2000 }
      },
      txtMaxLen : 16  // [chars]
    },
    time        : { cmdResHide: 10000 },
    trial       : {
      tBuff: { min: 0, max: 60000 }
    }
  };
  
  var data = {};
  
  var state = {
    an           : {
      aggr: { evt: [], roiId: "", roiVar: [], stimId: "", stimVar: [], subId: "", subVar: [], trialId: "", trialVar: [] }
    },
    app         : { startUp: true },  
    constr      : { evt: null, rois: null, stim: null, sub: null, trial: null },
    currCal     : { idx: -1, ui: { rec: null, radio: null } },
    currDS      : { idx: -1, ui: { rec: null, radio: null } },
    currEvt     : { idx: -1, ui: { rec: null, radio: null } },
    currExp     : { idx: -1, ui: { rec: null, radio: null } },
    currStim    : { idx: -1, ui: { rec: null, radio: null } },
    currStimSeq : { idx: -1, ui: { rec: null, radio: null } },
    currSub     : { idx: -1, ui: { rec: null, radio: null } },
    currTrial   : { idx: -1, ui: { rec: null, radio: null } },
    currVar     : {
      roi   : { idx: -1, ui: { rec: null, radio: null } },
      stim  : { idx: -1, ui: { rec: null, radio: null } },
      sub   : { idx: -1, ui: { rec: null, radio: null } },
      trial : { idx: -1, ui: { rec: null, radio: null } }
    },
    prjId       : -1,
    script      : {
      active : false,
      idx    : 0,
      lst    : []
    },
    timer       : { cmdRes : null },
    ui          : {
      tabViewIdx : { ds: 0, an: 0, res: 0 },
      tbarIdx    : 0,
    }
  };
  
  var ui = {
    app: { mask: null },
    cmdRes: null,
    main: null,
    notif: { lnk: null },
    
    panel: {
      cal          : { p: null, map: null },
      constr       : {
        elName : null,
        elVar  : null,
        roi    : { p: null, lstNames: null, lstVar: null, taVar: null },
        stim   : { p: null, lstNames: null, lstVar: null, taVar: null },
        sub    : { p: null, lstNames: null, lstVar: null, taVar: null },
        trial  : { p: null, lstNames: null, lstVar: null, taVar: null }
      },
      evt          : { p: null, btnEvt: null, btnTime: null, fnBtnEvtClick: null, btnFixT01: null, btnFixT02: null, btnFixT03: null, btnFixT04: null },
      exp          : { p: null, btnAdjEMs: null, btnExcRep: null, btnExport: null, btnLnk: null, btnImpRep: null, btnStats: null },
      proc         : { p: null },
      res          : { p: null },
      txt          : { p: null, edit: null, msg: null, view: null, fnNoteHide: null },
      txtIn        : { p: null, btnAct: null, txt: null, msg: null },
      vars         : { p: null, names: null },
      varGen       : { p: null, name: null, btnVar: null },
      varROIMutate : { p: null, names: null, btnMethod: null }
    },
    
    region: { app: { body: null } },
    sbar: { currExp: null, currPrj: null, info: null },
    
    tab: {
      ds: {
        imp     : { btnType: null },
        st      : { populated: false, resized: false, btnEvtExc: null, btnSubExc: null, btnSubVar: null, btnTrialCal: null, btnTrialExc: null, btnTrialVar: null, btnStimSeqExc: null, btnStimSeqEM: null, btnStimSeqXForm: null, winEM: null },
        stim    : { populated: false, resized: false, btnFileInf: null, btnFileROIs: null, btnROIsDef: null, btnROIsDef: null, btnWordPredSet: null, btnWordPredShow: null, winROIs: null },
        vars    : {
          populated : false,
          resized   : false,
          roi       : { btnDel: null, btnMutate: null, btnWordPredSet: null },
          stim      : { btnDel: null },
          sub       : { btnDel: null },
          trial     : { btnDel: null }
        }
      },
      an: {
        aggr  : { constr: { evt: null, roiN: null, roiV: null, stimN: null, stimV: null, subN: null, subV: null, trialN: null, trialV: null }, btnEvt: null, btnGD: null, btnTrialCal: null, btnTrialVal: null, btnROIExp: null, btnUnit: null, inpDSName: null, inpExpT0: null, inpExpT1: null, inpTrialT0: null, inpTrialT1: null, inpRecT0: null, inpRecT1: null, inpFD0: null, inpFD1: null, inpROIMaxDist: null, inpVars: null },
        trans : { constr: { evt: null, roiN: null, roiV: null, stimN: null, stimV: null, subN: null, subV: null, trialN: null, trialV: null } },
        ts    : { constr: { evt: null, roiN: null, roiV: null, stimN: null, stimV: null, subN: null, subV: null, trialN: null, trialV: null } },
        scr   : { txt: null }
      },
      res: {
        ds   : { populated: false, resized: false, btnDel: null, btnDelAll: null, btnRerun: null, btnShowTxt: null, btmShowHtml: null, btnSpreadsheet: null },
        hist : { lst: null }
      }
    },
    
    tabView: {
      ds  : null,
      an  : null,
      res : null
    },
    
    tbar: { btnGrp  : null },
    
    tbl: {
      cal     : { src: null, tbl: null },
      ds      : { src: null, tbl: null },
      evt     : { src: null, tbl: null },
      exp     : { src: null, tbl: null },
      stim    : { src: null, tbl: null },
      stimSeq : { src: null, tbl: null },
      sub     : { src: null, tbl: null },
      res     : { src: null, tbl: null },
      trial   : { src: null, tbl: null },
      vars    : {
        roi   : { src: null, tbl: null },
        stim  : { src: null, tbl: null },
        sub   : { src: null, tbl: null },
        trial : { src: null, tbl: null }
      },
      varP    : { src: null, tbl: null }
    }
  };
  
  var valid = new Pegasus.util.Validation();
  
  
  // -------------------------------------------------------------------------------------------------------------------
  /**
   * Calculates sizes of critical elements and, if necessary, resizes some of them.
   */
  function afterWinResize() {
    var a = ui.region.app;
    a.body = YAHOO.util.Dom.getRegion($("app-bd"));
    if (!a.body.height) a.body.height = a.body.bottom - a.body.top;
    
    $("ds-tabview").childNodes[3].style.height = (a.body.height - 72) + "px";
    $("an-tabview").childNodes[3].style.height = (a.body.height - 72) + "px";
    $("res-tabview").childNodes[3].style.height = (a.body.height - 72) + "px";
    
    // Tabs:
    ui.tab.ds.st.resized   = false;
    ui.tab.ds.stim.resized = false;
    ui.tab.ds.vars.resized = false;
    refreshTab();
  }
  
  
  // -----------------------------------------------------------------------------------------------------------------
  function attrSet(obj, attr, val, id, fnOkTrue, fnOkFalse, fnErr) {
    execCmd(true, { cmd:"attr-set-quick", obj: obj, attr: attr, val: val, id: id }, fnOkTrue, fnOkFalse, fnErr, true);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function calSetBtnsEnabled(e) {
    // Panel buttons:
    ui.panel.cal.p.getButtons()[0].set("disabled", !e);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function calSetCurr() {
    calSetBtnsEnabled(true);
    
    // Map:
    var map = ui.panel.cal.map;
    var cal = getCurrCal();
    var r = { x: CONST.cal.map.w / cal.scrW, y: CONST.cal.map.h / cal.scrH };  // screen-size to map-size ratio
    
    map.clear();
    var P = cal.points;
    for (var i=0, ni=P.length; i < ni; i++) {
      var p = P[i];
      var pC = { x: p.x * r.x, y: p.y * r.y };
      var pV = { x: (p.x + p.xErr) * r.x, y: (p.y + p.yErr) * r.y };
      
      map.drawLine(pC, pV, 1, "#ff0000");
      map.drawCircle(pC, 3, 1, "#000000", "#ffffff");
      map.drawCircle(pV, 3, 0, "#000000", "#000000");
    }
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function calShowPoints() {
    var res = "Angle error (in deg.; points listed left-to-right and top-to-bottom):\n\n";
    var P = getCurrCal().points;
    for (var i=0, ni=P.length; i < ni; i++) {
      var p = P[i];
      res += i + ": " + p.angErr + "\n";
    }
    alert(res);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  /**
   * INPUT:
   *   qry - a query to be executed; if empty query is created from the form
   *   run - flag: run now or add to script?
   */
  function calcAggr(qry, run) {
    var s = state.an.aggr;
    var u = ui.tab.an.aggr;
    
    // (1) Validate input:
    if (!qry) {
      if (u.inpDSName.value.length === 0) return alert("Please provide the name of the resulting data set.");
      
      if ((u.inpExpT0   .value !== "" && isNaN(u.inpExpT0   .value)) || (u.inpExpT1   .value !== "" && isNaN(u.inpExpT1   .value))) return alert("'Experiment time' values have to be either empty or they have to be numbers denoting a positive range.");
      if ((u.inpTrialT0 .value !== "" && isNaN(u.inpTrialT0 .value)) || (u.inpTrialT1 .value !== "" && isNaN(u.inpTrialT1 .value))) return alert("'Trial time' values have to be either empty or they have to be numbers denoting an a positive range.");
      if ((u.inpRecT0   .value !== "" && isNaN(u.inpRecT0   .value)) || (u.inpRecT1   .value !== "" && isNaN(u.inpRecT1   .value))) return alert("'Recording time' values have to be either empty or they have to be numbers denoting an a positive range.");
      if ((u.inpFD0     .value !== "" && isNaN(u.inpFD0     .value)) || (u.inpFD1     .value !== "" && isNaN(u.inpFD1     .value))) return alert("'Fixation duration' values have to be either empty or they have to be numbers denoting an a positive range.");
      
      if (isNaN(u.inpROIMaxDist.value)) return alert("'ROI max distance' value cannot be empty and has to be a number.");
      
      if (u.inpExpT0   .value !== "") u.inpExpT0   .value = parseInt(u.inpExpT0   .value);
      if (u.inpExpT1   .value !== "") u.inpExpT1   .value = parseInt(u.inpExpT1   .value);
      if (u.inpTrialT0 .value !== "") u.inpTrialT0 .value = parseInt(u.inpTrialT0 .value);
      if (u.inpTrialT1 .value !== "") u.inpTrialT1 .value = parseInt(u.inpTrialT1 .value);
      if (u.inpRecT0   .value !== "") u.inpRecT0   .value = parseInt(u.inpRecT0   .value);
      if (u.inpRecT1   .value !== "") u.inpRecT1   .value = parseInt(u.inpRecT1   .value);
      if (u.inpFD0     .value !== "") u.inpFD0     .value = parseInt(u.inpFD0     .value);
      if (u.inpFD1     .value !== "") u.inpFD1     .value = parseInt(u.inpFD1     .value);
      
      qry = "";
    }
    
    // (2) Request the calculation:
    if (qry.length === 0) {
      var adj = (u.btnGD.get("value") === "a" ? 1 : 0);
      var e = $filter(function (e) { return e.length > 0; }, s.evt);
      if (e.length === 0) s.evt = [];
      
      if (run) {
        qry = { cmd: "an-calc-aggr", "prj-id": state.prjId, "ds-name": encodeURIComponent(u.inpDSName.value), unit: u.btnUnit.get("value"), adj: adj, "exp-id": getCurrExp().id, "ids-sub": s.subId, "ids-trial": s.trialId, "ids-stim": s.stimId, "vars-sub": encodeURIComponent($join(s.subVar, ";")), "vars-trial": encodeURIComponent($join(s.trialVar, ";")), "trial-cal": u.btnTrialCal.get("value"), "trial-val": u.btnTrialVal.get("value"), "vars-stim": encodeURIComponent($join(s.stimVar, ";")), "vars-roi": encodeURIComponent($join(s.roiVar, ";")), "roi-max-dist": u.inpROIMaxDist.value, "roi-exp": u.btnROIExp.get("value"), evts: e.join(";"), "t-exp-0": u.inpExpT0.value, "t-exp-1": u.inpExpT1.value, "t-trial-0": u.inpTrialT0.value, "t-trial-1": u.inpTrialT1.value, "t-rec-0": u.inpRecT0.value, "t-rec-1": u.inpRecT1.value, "fix-dur-0": u.inpFD0.value, "fix-dur-1": u.inpFD1.value, vars: encodeURIComponent(u.inpVars.value) };
      }
      else {
        var qryStr = "{cmd:\"an-calc-aggr\",\"prj-id\":\"" + state.prjId + "\",\"ds-name\":\"" + encodeURIComponent(u.inpDSName.value) + "\",unit:\"" + u.btnUnit.get("value") + "\",adj:\"" + adj + "\",\"exp-id\":\"" + getCurrExp().id + "\",\"ids-sub\":\"" + s.subId + "\",\"ids-trial\":\"" + s.trialId + "\",\"ids-stim\":\"" + s.stimId + "\",\"vars-sub\":\"" + encodeURIComponent($join(s.subVar, ";")) + "\",\"vars-trial\":\"" + encodeURIComponent($join(s.trialVar, ";")) + "\",\"trial-cal\":\"" + u.btnTrialCal.get("value") + "\",\"trial-val\":\"" + u.btnTrialVal.get("value") + "\",\"vars-stim\":\"" + encodeURIComponent($join(s.stimVar, ";")) + "\",\"vars-roi\":\"" + encodeURIComponent($join(s.roiVar, ";")) + "\",\"roi-max-dist\":\"" + u.inpROIMaxDist.value + "\",\"roi-exp\":\"" + u.btnROIExp.get("value") + "\",evts:\"" + e.join(";") + "\",\"t-exp-0\":\"" + u.inpExpT0.value + "\",\"t-exp-1\":\"" + u.inpExpT1.value + "\",\"t-trial-0\":\"" + u.inpTrialT0.value + "\",\"t-trial-1\":\"" + u.inpTrialT1.value + "\",\"t-rec-0\":\"" + u.inpRecT0.value + "\",\"t-rec-1\":\"" + u.inpRecT1.value + "\",\"fix-dur-0\":\"" + u.inpFD0.value + "\",\"fix-dur-1\":\"" + u.inpFD1.value + "\",vars:\"" + encodeURIComponent(u.inpVars.value) + "\"}".replace(/\\"/g, "\\\\\"");
        ui.tab.an.scr.txt.value += "calcAggr(" + qryStr + ", true)\n";
        return;
      }
    }
    
    if (!state.script.active) procShow("Calculating...");
    
    execCmd(true, qry,
      function (res) {
        var DS = getCurrExp().ds;
        var ds = res.ds;
        ds.idx = DS.length-1;
        DS.push(ds);
        
        ui.tbl.ds.tbl.addRow(ds);
        ui.tab.res.ds.populated = false;
        ui.tab.res.ds.resized   = false;
        
        if (state.script.active) scriptStep(false);
        else ui.panel.proc.p.hide();
      },
      function (res) {
        alert(res.msg);
        if (state.script.active) scriptStep(true);
        else ui.panel.proc.p.hide();
      },
      function (res) {
        alert(CONST.err.unexpected);
        if (state.script.active) scriptStep(true);
        else ui.panel.proc.p.hide();
      },
      true
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function cmdResShow(msg) {
    if (state.timer.cmdRes) window.clearTimeout(state.timer.cmdRes);
    ui.cmdRes.innerHTML = msg;
    $show(ui.cmdRes);
    state.timer.cmdRes = window.setTimeout(function () { state.timer.cmdRes = null; $hide(ui.cmdRes); }, CONST.time.cmdResHide);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function constrReset(tab) {
    // State:
    state.an[tab] = { evt: [], roiId: "", roiVar: [], stimId: "", stimVar: [], subId: "", subVar: [], trialId: "", trialVar: [] };
    
    // UI:
    var c = ui.tab.an[tab].constr;
    for (var x in c) {
      if (!c[x]) continue;
      c[x].className = "constr-lst constr-lst-none";
      c[x].innerHTML = "<span>no constraints</span>";
    }
    
    var u = ui.tab.an.aggr;
    u.inpExpT0.value      = "";
    u.inpExpT1.value      = "";
    u.inpTrialT0.value    = "";
    u.inpTrialT1.value    = "";
    u.inpRecT0.value      = "";
    u.inpRecT1.value      = "";
    u.inpFD0.value        = "";
    u.inpFD1.value        = "";
    u.inpROIMaxDist.value = "0";
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function constrShowPanel(objName, elName, elVar) {
    ui.panel.constr.elName = elName;
    ui.panel.constr.elVar  = elVar;
    ui.panel.constr[objName].p.show();
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function construct() {}
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function dsDel(all) {
    if (!confirm((all ? "Delete all data sets? This operation is irreversible." : "Delete the active data set ('" + getCurrDS().name + "')? This operation is irreversible."))) return;
    
    procShow(all ? "Deleting all data sets..." : "Deleting the active data set...");
    execCmd(true,
      (all
        ? { cmd: "ds-del-all", "exp-id": getCurrExp().id }
        : { cmd: "ds-del", "ds-id": getCurrDS().id }
      ),
      function (res) {
        var t = ui.tbl.ds.tbl;
        
        if (all) {
          getCurrExp().ds = [];
          t.deleteRows(0, t.getRecordSet().getLength());
        }
        else {
          getCurrExp().ds[state.currDS.idx] = null;
          t.deleteRow(state.currDS.ui.rec);
        }
        
        ui.tab.res.ds.btnShowTxt.set("disabled", true);
        ui.tab.res.ds.btnShowHtml.set("disabled", true);
        ui.tab.res.ds.btnRerun.set("disabled", true);
        ui.tab.res.ds.btnDel.set("disabled", true);
        ui.tab.res.ds.btnDelAll.set("disabled", t.getRecordSet().getLength() === 0);
        
        state.currDS = { idx: -1, ui: { rec: null, radio: null } };
        
        ui.panel.proc.p.hide();
      },
      null, null, true
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function dsGetTblRow(o, idx, curr) {
    return $setProp($setObj({}, o), { idx: idx, curr: curr, ts: (!!o.tsVar), size: $size2str(o.size, true), noteLen: $size2str(o.noteLen, true) });
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function dsRerun() {
    if (!confirm("Recreate the selected dataset?")) return;
    calcAggr("username=" + data.profile.username + "&" + getCurrDS().qry);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function dsShow(format) {
    var ds = getCurrDS();
    var win = window.open(CONST.serverURI_L + "?" + getCmd({ cmd: "ds-exp", "prj-id": state.prjId, "ds-id": ds.id, "ds-name": ds.name, format: format, "delim-var": "tab", "delim-obs": "new-line", "miss-val": ".", zip: 0 }), "", "toolbar=1,status=0,scrollbars=1");
    window.setTimeout(function () { win.document.title = "Data set '" + ds.name + "'" }, 500);
    /*
    var ds = getCurrDS();
    var win = window.open(CONST.serverURI_L + "?" + getCmd({ cmd: "ds-exp", "ds-id": ds.id, format: "txt", "delim-var": "tab", "delim-obs": "new-line", "miss-val": ".", zip: 0 }), "", "toolbar=1,status=0,scrollbars=1");
    window.setTimeout(function () { win.document.title = "Data set: " + ds.name; }, 250);
    */
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function dsSpreadsheet() {
    var S = $filter(function (x) { return (x && x.sel); }, getCurrExp().ds);  // selected data sets
    
    if (S.length < 2) return alert("Please select at least two datasets.");
    if ($filter(function (x) { return x.name.length === 0; }, S).length > 0) return alert("All selected datasets need to have a name.");
    if ($filter(function (x) { return x.name.toLowerCase() === "comp" || x.name.toLowerCase() === "stat"; }, S).length > 0) return alert("The names 'Comp' and 'Stat' are reserved and cannot be used for names of datasets.");
    
    // Check for name duplicates:
    var names = {};  // stores names to compare against while going through the results below
    for (var i=0, ni=S.length; i < ni; i++) {
      var s = S[i];
      if (names[s.name]) return alert("Names of datasets whithin a spreadsheet need to be unique.");
      names[s.name] = true;
    }
    
    // Execute command:
    var ids = $map(function (x) { return x.id; }, S).join(",");
    //$("file-dl-form").setAttribute("action", "/et/servlet/main.Do?" + getCmd({ cmd: "ds-gen-spreadsheet", "an-res-ids": ids }));
    //$("file-dl-form").submit();
    window.open("/pegasus-gaze/servlet/main.Do?" + getCmd({ cmd: "ds-gen-spreadsheet", "ds-ids": ids }), "Data sets spreadsheet", "toolbar=0,status=0");
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function evtBtnsSetEnabled(e) {
    ui.tab.ds.st.btnEvtExc.set("disabled", !e);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function evtGetTblRow(o, idx, curr) {
    return $setProp($setObj({}, o), { idx: idx, curr: curr, exc: (o.exc ? CONST.tbl.exc : "") });
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function evtSetCurr() {
    // Buttons:
    evtBtnsSetEnabled(true);
    ui.tab.ds.st.btnEvtExc.set("label", (getCurrEvt().exc ? "Inc" : "Exc"));
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function excObjToggle(humanName, name, obj, tbl, idx, btn, fnGetTblRow) {
    if (obj.exc && !confirm("You are about to mark the the selected " + humanName + " as to be INCLUDED in analyses run from now on. This operation is non-destructive and can always be reversed.")) return;
    
    var info = "";
    if (!obj.exc) {
      info = prompt("You are about to mark the selected " + humanName + " as to be EXCLUDED from all analyses run from now on. This operation is non-destructive and can always be reversed.\n\nPlease provide exclusion info (can be empty):");
      if (info === null) return;
    }
    
    procShow("Setting object " + (obj.exc ? "included" : "excluded") + "...");
    execCmd(true,
      { cmd: "exc-obj-toggle", "obj-name": name, "obj-id": obj.id, "exc-info": info },
      function (res) {
        obj.exc = !obj.exc;
        obj.excInfo = info;
        
        tbl.updateRow(idx, fnGetTblRow(obj, idx, true));
        
        btn.set("label", (obj.exc ? "Inc" : "Exc"));
        ui.panel.proc.p.hide();
      },
      function (res) { ui.panel.proc.p.hide(); },
      function (res) { ui.panel.proc.p.hide(); },
      true
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  /**
   * Executes command and handles a part of the response. To wit, server error or default actions for true or false 
   * outcome if the corresponding handler functions have not been specified. 'params' is a hash of key-value pairs.
   */
  function execCmd(get, params, fnOkTrue, fnOkFalse, fnErr, minHead) {
    var cmd = getCmd(params);
    
    $call({
      method  : (get ? "GET" : "POST"),
      uri     : (get ? CONST.serverURI_S + "?" + cmd : CONST.serverURI_S),
      params  : (get ? null : cmd),
      eval    : true,
      minHead : minHead,
      
      fnOk: function (res) {
        if (res.outcome) {
          execCmd_procRes(res);
          if (fnOkTrue) fnOkTrue(res);
          else ui.panel.proc.p.hide();
        }
        else {
          if (fnOkFalse) fnOkFalse(res)
          else {
            alert(res.msg);
            ui.panel.proc.p.hide();
          };
        }
      },
      
      fnErr: function (res) {
        if (fnErr) fnErr(res);
        else {
          alert(CONST.err.unexpected);
          ui.panel.proc.p.hide();
        }
      }
    });
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function execCmd_procRes(res) {}
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function expAdjEMs() {
    if (!confirm("Adjusting EMs is a potentially lengthy operation. Please note, that the original EMs will be left intact. The result of the adjustment process is a new set of coordinates that does not interfere with the original EMs. Do you wish to continue?")) return;
    
    procShow("Adjusting EMs...");
    execCmd(true,
      { cmd: "exp-adj-ems", "exp-id": getCurrExp().id },
      function (res) {
        txtShow("EM adjustment report", null, res.rep, false, false);
        for (var i=0, ni=getCurrExp().sub.length; i < ni; i++) {
          var sub = getCurrExp().sub[i];
          for (var j=0, nj=sub.trials.length; j < nj; j++) {
            var trial = sub.trials[j];
            for (var k=0, nk=trial.stimSeq.length; k < nk; k++) {
              var stimSeq = trial.stimSeq[k];
              stimSeq.fixCntROIAdj = -1;
              stimSeq.fix = null;
              stimSeq.blinks = null;
            }
          }
        }
        expSetCurr(false);
      },
      null, null, true
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function expGetTblRow(o, idx, curr) {
    return $setProp($setObj({}, o), { idx: idx, curr: curr, id: o.id, name: o.name, created: o.created, subCnt: o.subCnt, trialCnt: o.trialCnt, stimCnt: o.stimCnt, roiCnt: o.roiCnt, linked: (o.linked ? 1 : 0), noteLen: $size2str(o.noteLen, true), memo: o.memo });
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function expDel(e) {
    var exp = getCurrExp();
    if (!confirm("You are about to delete the active experiment ('" + exp.name + "'). This operation is irreversible.") || prompt("Type 'DELETE' below to confirm the deletion of the active experiment:") !== "DELETE") return;
    
    procShow("Deleting the experiment...");
    var idx = state.currExp.idx;
    execCmd(true,
      { cmd: "exp-del", "exp-id": exp.id },
      function (res) {
        data.exp[state.currExp.idx] = null;
        ui.tbl.exp.tbl.deleteRow(state.currExp.ui.rec);
        
        ui.tbar.btnGrp.check(0);
        ui.tabView.ds.set("activeIndex", 2);
        
        tblClear(ui.tbl.stim.tbl);
        ui.tabView.ds.set("activeIndex", 3);
        
        tblClear(ui.tbl.sub.tbl);
        tblClear(ui.tbl.trial.tbl);
        tblClear(ui.tbl.stimSeq.tbl);
        
        // Tabs:
        ui.tab.ds.st.populated   = false;
        ui.tab.ds.stim.populated = false;
        ui.tab.ds.vars.populated = false;
        refreshTab(); 
        
        notifCheck();
        
        resetCurrState("currExp");
        expSetBtnsEnabled(false);
        ui.sbar.currExp.innerHTML = "---";
        
        alert("Experiment has been deleted.");
        ui.panel.proc.p.hide();
      },
      null, null, true
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function expExcRep() {
    procShow("Generating...");
    execCmd(true,
      { cmd: "exp-get-exc-rep", "exp-id": getCurrExp().id },
      function (res) {
        txtShow("Experiment exclusion report", null, res.rep, false, false);
        ui.panel.proc.p.hide();
      },
      null, null, true
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function expExport() {
    if (!confirm("This will export the experiment and allow you to download it as a ZIP archive. That archive can later be used to import the experiment. Please note, that not all features of an experiment are exported at this point.")) return;
    
    $("file-dl-form").setAttribute("action", CONST.serverURI_L + "?" + getCmd({ cmd: "exp-export", "exp-id": getCurrExp().id }));
    $("file-dl-form").submit();
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function expImport() {
    if (state.currExp.idx === -1) { alert("Please select an experiment first."); return; }
    if ($("ds-imp-filename").value.length === 0) { alert("Please provide the the file information."); return; }
    if (!confirm("You are about to request an execution of the import routine. If this operation succeeds, all the subject, trial, stimuli, and gaze information associated with the active experiment ('"+ getCurrExp().name +"') will be wiped out and substitued with those from the import file. This operation is irreversible.\n\nNOTE: None of the variables that have been defined so far will be removed.") || prompt("Type 'IMPORT' below to confirm the import of the active experiment:") !== "IMPORT") return;
    
    procShow("Importing data...");
    var filename = $("ds-imp-filename").value;
    var idx = state.currExp.idx;
    execCmd(true,
      { cmd: "exp-import", "exp-id": data.exp[idx].id, filename: encodeURIComponent(filename) },
      function (res) {
        data.exp[idx] = res.exp;
        expUpdatePanel(res.exp);
        expSetCurr(false, function () { expImpRep(data.exp[idx]); });
        ui.panel.proc.p.hide();
      },
      null, null, true
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function expImpRep(exp) {
    var fnShow = function () {
      if (!exp.impRep) {
        alert("No import report is associated with the active experiment.");
        ui.panel.proc.p.hide();
        return;
      }
      
      txtShow("Experiment import report", "Import performed on: <b>" + exp.impDate.toDateString() + "</b>", exp.impRep, false, false);
      ui.panel.proc.p.hide();
    };
    
    if (exp.impRep === null) {
      procShow("Retrieving data...");
      execCmd(true,
        { cmd: "exp-get-imp-rep", "exp-id": exp.id },
        function (res) {
          exp.impRep = res.impRep;
          fnShow();
        },
        null, null, true
      );
    }
    else fnShow();
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function expLnk() {
    var e = getCurrExp();
    if (!confirm("Linking of EMs to ROIs is a potentially lengthy operation. It should be requested only after ROIs for all relevant stimuli have been defined. Please also note, that all current fixation-ROI links will be replaced with the new ones. This operation is irreversible. Do you wish to continue?")) return;
    
    procShow("Linking EMs to ROIs...");
    execCmd(true,
      { cmd: "exp-lnk", "exp-id": e.id },
      function (res) {
        txtShow("EMs to ROIs linking report", null, res.rep, false, false);
        expUpdatePanel(res.exp);
        data.exp[state.currExp.idx] = res.exp;
        expSetCurr(false);
      },
      null, null, true
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function expNew(e) {
    var name = prompt("Name of the new experiment:");
    if (!name || (name && name.length === 0)) return;
    
    procShow("Creating the experiment...");
    execCmd(true,
      { cmd: "exp-new", "prj-id": state.prjId, name: encodeURIComponent(name) },
      function (res) {
        data.exp.push(res.exp);
        var t = ui.tbl.exp.tbl;
        t.addRow(dsGetTblRow(res.exp, data.exp.length-1, false));
        Pegasus.Main.tblResort(t);
        
        ui.panel.proc.p.hide();
      },
      null, null, true
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function expSetBtnsEnabled(e) {
    // Button bar:
    ui.panel.exp.btnStats.set("disabled", !e);
    ui.panel.exp.btnExcRep.set("disabled", !e);
    ui.panel.exp.btnImpRep.set("disabled", !e);
    ui.panel.exp.btnAdjEMs.set("disabled", !e);
    ui.panel.exp.btnLnk.set("disabled", !e);
    ui.panel.exp.btnExport.set("disabled", !e);
    
    // Panel buttons:
    ui.panel.exp.p.getButtons()[1].set("disabled", !e);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  /**
   * 'persist' indicates if the experiment from the last session is being loaded or the user has just changed the active 
   * experiment or the import routine has finished and requested the newly imported experiment be set active. Only the 
   * explicit user action should be persisted.
   * 
   * If 'fnAfter' is provided, that function should hide the processing panel by calling the following method:
   * 
   *    ui.panel.proc.p.hide();
   * 
   */
  function expSetCurr(persist, fnAfter) {
    var exp = getCurrExp();
    procShow("Loading the experiment...");
    
    // Process data:
    $map(
      function (x) {
        if (!x.img.uri && x.img.len > 0) x.img.uri = getCmd({ cmd: "img-get", obj: "stim", "obj-id": x.id, attr: "img" });
        if (!x.imgROI.uri) x.imgROI.uri = getCmd({ cmd: "img-get", obj: "stim", "obj-id": x.id, attr: "img_roi" });
      },
      exp.stim
    );
    
    // Tabs:
    ui.tab.ds.st.populated   = false;
    ui.tab.ds.stim.populated = false;
    ui.tab.ds.vars.populated = false;
    ui.tab.res.ds.populated  = false;
    
    // Reset constriants:
    state.constr.evt   = null;
    state.constr.roi   = null;
    state.constr.stim  = null;
    state.constr.sub   = null;
    state.constr.trial = null;
    
    constrReset("aggr");
    
    // Reset events:
    var items = [];
    var evts = exp.evt;
    for (var i=0, ni=evts.length; i < ni; i++) {
      var evt = evts[i];
      items.push({ text: evt.name, value: evt, onclick: { fn: ui.panel.evt.fnBtnEvtClick } });
    }
    var menu = ui.panel.evt.btnEvt.getMenu();
    menu.clearContent();
    menu.addItems(items);
    menu.render();
    
    // Other:
    notifCheck();
    expSetBtnsEnabled(true);
    refreshTab();
    afterWinResize();
    resetCurrState(["currDS","currEvt","currStim","currStimSeq","currSub","currTrial","currVar.sub","currVar.trial","currVar.stim","currVar.roi"]);
    
    if (fnAfter) fnAfter();
    else ui.panel.proc.p.hide();
    
    // Persist the new UI state (last step so that we know everything above worked):
    if (persist) Pegasus.Main.setCookie(CONST.cookie.appName, "curr-exp-id", getCurrExp().id);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function expStats() {
    procShow("Calculating...");
    execCmd(true,
      { cmd: "exp-get-stats", "exp-id": getCurrExp().id },
      function (res) {
        txtShow("Experiment statistics", null, res.stats, false, false);
        ui.panel.proc.p.hide();
      },
      null, null, true
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function expUpdatePanel(exp) {
    ui.tbl.exp.tbl.updateRow(expGetTblRow(exp, state.currExp.idx, true));
  }
  
   
  // -----------------------------------------------------------------------------------------------------------------
  /**
   * Generates the YUI data source and table.
   * 
   * ARGS:
   *   ui       : the appropriate 'ui' object's property
   *   cont     : the container
   *   fields   : array of fields
   *   cols     : array of columns
   *   events   : array of events
   *   settings : array of settings
   *   panel    : provide for data tables that should be editable while displayed in a modal panel
   *   scroll   : flag
   */
  function genTbl(appId, ui, cont, fields, cols, events, settings, panel, scroll) {
    // Source:
    var src = new YAHOO.util.DataSource();
    src.responseType   = YAHOO.util.DataSource.TYPE_JSARRAY;
    src.responseSchema = { fields: fields };
    ui.src = src;
    
    // Table:
    var tbl = new YAHOO.widget.ScrollingDataTable(cont, cols, src, $setObj({ scrollable: scroll }, settings));
    if (!events.cellClickEvent) tbl.on("cellClickEvent", tbl.onEventShowCellEditor);
    for (var e in events) tbl.on(e, events[e]);
    
    tbl.render();
    
    if (panel) {
      tbl.doBeforeShowCellEditor = function (cellEditor) {
        var el = cellEditor.getContainerEl();
        if (el.parentNode === document.body) {
          panel.body.appendChild(el);
          
          cellEditor.subscribe("showEvent", 
            function () {
              var xy = YAHOO.util.Dom.getXY(this.getTdEl());
              YAHOO.util.Dom.setXY(this.getContainerEl(), xy);
            }
          );
        }
        
        return true;
      };
    }
    
    ui.tbl = tbl;
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function getCurrExp()     { return data.exp[state.currExp.idx]; }
  function getCurrStim()    { return data.exp[state.currExp.idx].stim[state.currStim.idx]; }
  function getCurrSub()     { return data.exp[state.currExp.idx].sub[state.currSub.idx]; }
  function getCurrTrial()   { return data.exp[state.currExp.idx].sub[state.currSub.idx].trials[state.currTrial.idx]; }
  function getCurrCal()     { return data.exp[state.currExp.idx].sub[state.currSub.idx].trials[state.currTrial.idx].cal[state.currCal.idx]; }
  function getCurrStimSeq() { return data.exp[state.currExp.idx].sub[state.currSub.idx].trials[state.currTrial.idx].stimSeq[state.currStimSeq.idx]; }
  function getCurrEvt()     { return data.exp[state.currExp.idx].sub[state.currSub.idx].trials[state.currTrial.idx].stimSeq[state.currStimSeq.idx].evt[state.currEvt.idx]; }
  function getCurrDS()      { return data.exp[state.currExp.idx].ds[state.currDS.idx]; }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function getCmd(spec) { return Pegasus.Main.getCmd(CONST.appId, spec); }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  /**
   * Returns a function that can be used as an async submitter for a DataTable inline editor. That function will take
   * care of requesting a single attribute updated.
   * 
   * IN:
   *   aFnValid: an array of validator functions; each takes one argument and returns a boolean
   *   ...
   */
  function getSetAttrFn(aFnValid, fnAfterSuccess, fnGetCmd) {
    return function (cb, newVal) {
      if ((this.value === null && newVal === "") || newVal == this.value) { cb(false,newVal); return; }
      
      // Validate:
      if (aFnValid.length > 0) {
        var isValid = $lfold(
          function (a,b) { return a && b; },
          $map(function (x) { return x(newVal); }, aFnValid),
          true
        );
        if (!isValid) { cb(false,newVal); return; }
      }
      
      // Persist server-side:
      procShow("Saving...");
      var that = this;
      execCmd(true, fnGetCmd(this, newVal),
        function (res) {
          fnAfterSuccess(that, newVal, res);
          cb(true, newVal);
          ui.panel.proc.p.hide();
        },
        function (res) {
          cb(false, newVal);
          alert(res.msg);
          ui.panel.proc.p.hide();
        },
        function(res) {
          alert(CONST.err.unexpected);
          cb(false, newVal);
          ui.panel.proc.p.hide();
        },
        true
      );
    };
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  // e.g.: histAdd(qry, res.t, "Calculate aggregate statistics (results saved and available from Results --> Data sets)");
  function histAdd(cmd, t, txt) {
    var div = $$("div", null, null, "item");
    div.oncontextmenu = function (e) { return true };
    
    var btnRemove = $$("input", div);
    btnRemove.setAttribute("type", "button");
    btnRemove.value = "X";
    btnRemove.onclick = function (e) { ui.tab.res.lst.removeChild(this.parentNode); };
    
    $$("span", div, null, "cmd", cmd + (t !== undefined ? " [" + t + " ms]" : ""));
    $$("br", div);
    $$("div", div, null, "txt", txt.replace(/\n/g, "<br />"));
    
    var l = ui.tab.res.lst;
    (l.hasChildNodes ? l.insertBefore(div, l.firstChild) : l.appendChild(div));
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function histShowTbl(txt) {
    var tbl = ui.tbl.res.tbl;
    tblClear(tbl);
        
    var rows = [];
    var R = txt.split("\n");
    for (var i=1, ni=R.length; i < ni; i++) {
      var r = R[i].split("\t");
      if (i > 10) continue;
      rows.push(
        {
          stim:r[0], roi:r[1],
          "fc-min":  r[2],  "fc-max":  r[3],  "fc-m":  r[4],  "fc-med":  r[5],  "fc-sd":  r[6],
          "afd-min": r[7],  "afd-max": r[8],  "afd-m": r[9],  "afd-med": r[10], "afd-sd": r[11],
          "ffd-min": r[12], "ffd-max": r[13], "ffd-m": r[14], "ffd-med": r[15], "ffd-sd": r[16],
          "sfd-min": r[17], "sfd-max": r[18], "sfd-m": r[19], "sfd-med": r[20], "sfd-sd": r[21],
          "tt-min":  r[22], "tt-max":  r[23], "tt-m":  r[24], "tt-med":  r[25], "tt-sd":  r[26]
        }
      );
    }
    
    if (rows.length > 0) tbl.addRows(rows);
    tbl.render();
    ui.panel.res.p.show();
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initAggr() {
    var u = ui.tab.an.aggr;
    
    // Init events:
    var btnEvt = new YAHOO.widget.Button("an-aggr-btn-evt", { onclick: { fn: function (e) { ui.panel.evt.p.show() } } });
    ui.tab.an.aggr.btnEvt = btnEvt;
    
    // Constraint info fields:
    u.constr.stimN  = $("an-aggr-stim-n");
    u.constr.stimV  = $("an-aggr-stim-v");
    u.constr.subN   = $("an-aggr-sub-n");
    u.constr.subV   = $("an-aggr-sub-v");
    u.constr.trialN = $("an-aggr-trial-n");
    u.constr.trialV = $("an-aggr-trial-v");
    u.constr.roiN   = $("an-aggr-roi-n");
    u.constr.roiV   = $("an-aggr-roi-v");
    u.constr.evt    = $("an-aggr-evt");
    
    // Controls:
    u.inpDSName     = $("an-aggr-ds-name");
    u.inpROIMaxDist = $("an-aggr-roi-max-dist");
    u.inpExpT0      = $("an-aggr-exp-t0");
    u.inpExpT1      = $("an-aggr-exp-t1");
    u.inpTrialT0    = $("an-aggr-trial-t0");
    u.inpTrialT1    = $("an-aggr-trial-t1");
    u.inpRecT0      = $("an-aggr-rec-t0");
    u.inpRecT1      = $("an-aggr-rec-t1");
    u.inpFD0        = $("an-aggr-fd0");
    u.inpFD1        = $("an-aggr-fd1");
    u.inpVars       = $("an-aggr-other-vars");
    
    // "Results" drop-down list:
    function fnBtnUnitClick(p_sType, p_aArgs, p_oItem) {
      u.btnUnit.set("label", p_oItem.cfg.getProperty("text"));
      u.btnUnit.set("value", p_oItem.value);
    }
    
    u.btnUnit = new YAHOO.widget.Button({
      type: "menu",
      label: "Stim-seq (event)",
      value: "s-seq",
      menu: [
        { text: "Stim-seq (or event if defined)",          value: "s-seq",                 onclick: { fn: fnBtnUnitClick } },
        { text: "ROI (curr)",                              value: "roi",                   onclick: { fn: fnBtnUnitClick } },
        { text: "ROI (curr-prev-next: order-adjacent)",    value: "roi-prev-next-adj-ord", onclick: { fn: fnBtnUnitClick } },
        { text: "ROI (curr-prev-next: fixation-adjacent)", value: "roi-prev-next-adj-fix", onclick: { fn: fnBtnUnitClick } },
        { text: "Fixation",                                value: "fix",                   onclick: { fn: fnBtnUnitClick } }
      ],
      container: "an-aggr-unit"
    });
    
    // Gaze data drop-down list:
    function fnBtnGDClick(p_sType, p_aArgs, p_oItem) {
      ui.tab.an.aggr.btnGD.set("label", p_oItem.cfg.getProperty("text"));
      ui.tab.an.aggr.btnGD.set("value", p_oItem.value);
    }
    
    u.btnGD = new YAHOO.widget.Button({
      type: "menu",
      label: "Original (blue)",
      value: "o",
      menu: [
        { text: "Original (blue)", value: "o", onclick: { fn: fnBtnGDClick } },
        { text: "Adjusted (red)",  value: "a", onclick: { fn: fnBtnGDClick } }
      ],
      container: "an-aggr-gd"
    });
    
    // ROIs "expand to" drop-down list:
    function fnBtnROIExpClick(p_sType, p_aArgs, p_oItem) {
      ui.tab.an.aggr.btnROIExp.set("label", p_oItem.cfg.getProperty("text"));
      ui.tab.an.aggr.btnROIExp.set("value", p_oItem.value);
    }
    
    u.btnROIExp = new YAHOO.widget.Button({
      type: "menu",
      label: "---",
      value: "-",
      menu: [
        { text: "---", value: "-", onclick: { fn: fnBtnROIExpClick } },
        { text: "The nearest clause", value: "c", onclick: { fn: fnBtnROIExpClick } },
        { text: "The nearest sentence", value: "s", onclick: { fn: fnBtnROIExpClick } }
      ],
      container: "an-aggr-roi-exp"
    });
    
    /*
    u.btnGrp = new YAHOO.widget.Button({
      type: "menu",
      label: "Word: As is (displays variables)",
      value: "w0",
      menu: [
        { text: "None", value: "-", onclick: { fn: fnBtnGrpClick } },
        { text: "Word: As is (displays variables)", value: "w0", onclick: { fn: fnBtnGrpClick } },
        { text: "Word: Ignore spaces", value: "w1", onclick: { fn: fnBtnGrpClick } },
        { text: "Word: Ignore punctuation", value: "w2", onclick: { fn: fnBtnGrpClick } },
        { text: "Word: Ignore both", value: "w3", onclick: { fn: fnBtnGrpClick } }
      ],
      container: "an-aggr-grp"
    });
    */
    
    // Trial calibration and validation drop-down lists:
    function fnBtnTrialCalClick(p_sType, p_aArgs, p_oItem) {
      ui.tab.an.aggr.btnTrialCal.set("label", p_oItem.cfg.getProperty("text"));
      ui.tab.an.aggr.btnTrialCal.set("value", p_oItem.value);
    }
    
    function fnBtnTrialValClick(p_sType, p_aArgs, p_oItem) {
      ui.tab.an.aggr.btnTrialVal.set("label", p_oItem.cfg.getProperty("text"));
      ui.tab.an.aggr.btnTrialVal.set("value", p_oItem.value);
    }
    
    u.btnTrialCal = new YAHOO.widget.Button({
      type: "menu",
      label: "Any",
      value: "0",
      menu: [
        { text: "Any"        , value: "0", onclick: { fn: fnBtnTrialCalClick } },
        { text: "Good"       , value: "1", onclick: { fn: fnBtnTrialCalClick } },
        { text: "Fair"       , value: "2", onclick: { fn: fnBtnTrialCalClick } },
        { text: "Poor"       , value: "3", onclick: { fn: fnBtnTrialCalClick } },
        { text: "Fair + good", value: "4", onclick: { fn: fnBtnTrialCalClick } },
        { text: "Fair + poor", value: "5", onclick: { fn: fnBtnTrialCalClick } }
      ],
      container: "an-aggr-trial-cal"
    });
    
    u.btnTrialVal = new YAHOO.widget.Button({
      type: "menu",
      label: "Any",
      value: "0",
      menu: [
        { text: "Any"        , value: "0", onclick: { fn: fnBtnTrialValClick } },
        { text: "Good"       , value: "1", onclick: { fn: fnBtnTrialValClick } },
        { text: "Fair"       , value: "2", onclick: { fn: fnBtnTrialValClick } },
        { text: "Poor"       , value: "3", onclick: { fn: fnBtnTrialValClick } },
        { text: "Fair + good", value: "4", onclick: { fn: fnBtnTrialValClick } },
        { text: "Fair + poor", value: "5", onclick: { fn: fnBtnTrialValClick } }
      ],
      container: "an-aggr-trial-val"
    });
    
    // Button event handlers:
    var fnReset = function (e) {
      if (!confirm("Reset all the above constraints?")) return;
      constrReset("aggr");
    };
    
    // Buttons:
    (new YAHOO.widget.Button("an-aggr-btn-sub")).on("click", function (e) { constrShowPanel("sub", u.constr.subN, u.constr.subV); });
    (new YAHOO.widget.Button("an-aggr-btn-trial")).on("click", function (e) { constrShowPanel("trial", u.constr.trialN, u.constr.trialV); });
    (new YAHOO.widget.Button("an-aggr-btn-stim")).on("click", function (e) { constrShowPanel("stim", u.constr.stimN, u.constr.stimV); });
    (new YAHOO.widget.Button("an-aggr-btn-roi")).on("click", function (e) { constrShowPanel("roi", u.constr.roiN, u.constr.roiV); });
    (new YAHOO.widget.Button("an-aggr-btn-reset")).on("click", fnReset);
    (new YAHOO.widget.Button("an-aggr-btn-calc")).on("click", function (e) { calcAggr(null, true); });
    (new YAHOO.widget.Button("an-aggr-btn-scr")).on("click", function (e) { calcAggr(null, false); });
  }
  
   
  // -------------------------------------------------------------------------------------------------------------------
  function initApp() {
    procShow("Retrieving experiments...");
    
    var currExpId = parseInt(Pegasus.util.Cookie.get(CONST.cookie.appName + "curr-exp-id"));
    if (isNaN(currExpId)) currExpId = "";
    
    execCmd(true, { cmd: "exp-get-lst", "prj-id": state.prjId, "exp-id": currExpId }, initApp_cb, null, null, true);
  }
  
  
  // ----^----
  function initApp_cb(res) {
    procShow("Processing experiments...");
    
    data.exp = [];
    $setObj(data.exp, res.exp);  // store the data received
    
    var currExpId = parseInt(Pegasus.util.Cookie.get(CONST.cookie.appName + "curr-exp-id"));
    var currExpIdx = -1;
    
    // Tabs:
    ui.tabView.ds.set("activeIndex", state.ui.tabViewIdx.ds);
    ui.tabView.an.set("activeIndex", state.ui.tabViewIdx.an);
    ui.tabView.res.set("activeIndex", state.ui.tabViewIdx.res);
    
    ui.tbar.btnGrp.check(state.ui.tbarIdx);
    ui.tbar.btnGrp.getButtons()[state.ui.tbarIdx].fireEvent("click");
    
    // Experiments:
    var R = [];  // rows
    for (var i=0, ni=data.exp.length; i < ni; i++) {
      var e = data.exp[i];
      if (e.id === currExpId) currExpIdx = i;
      R.push(expGetTblRow(e, i, (e.id === currExpId)));
    }
    
    var t = ui.tbl.exp.tbl;
    t.addRows(R);
    
    if (currExpIdx != -1) {
      var row = t.getRow(currExpIdx);
      var radio = t.getFirstTdEl(t.getLastTrEl()).firstChild.firstChild;
      t.selectRow(radio);
      
      state.currExp.ui.radio = t.getRecord(radio);
      state.currExp.ui.rec   = t.getRecord(radio);
      state.currExp.idx      = currExpIdx;
      
      expSetBtnsEnabled(true);
      ui.sbar.currExp.innerHTML = e.name;
    }
    
    Pegasus.Main.tblResort(t);
        
    if (currExpIdx !== -1) {
      if (!getCurrExp().loaded) {
        execCmd(true,
          { cmd: "exp-get", id: currExpId},
          function (res) {
            data.exp[currExpIdx] = res.exp;
            expSetCurr(false);
            state.app.startUp = false;
            afterWinResize();
            
            $hide(ui.app.mask);
            ui.panel.proc.p.hide();
          },
          null,
          function (res) {
            alert("Communcation error has occured. This application will now terminate.");
            ui.panel.proc.p.hide();
          },
          true
        );
      }
      else {
        expSetCurr(false);
        state.app.startUp = false;
        afterWinResize();
        
        $hide(ui.app.mask);
        ui.panel.proc.p.hide();
      }
    }
    else {
      state.app.startUp = false;
      afterWinResize();
      ui.panel.exp.p.show();
      
      $hide(ui.app.mask);
      ui.panel.proc.p.hide();
    }
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initCalPanel() {
    // Map:
    var mapCont = $("panel-cal-map");
    mapCont.style.width  = CONST.cal.map.w + "px";
    mapCont.style.height = CONST.cal.map.h + "px";
    
    ui.panel.cal.map = Canvas(null, null, mapCont);
    ui.panel.cal.map.setDim(CONST.cal.map.w, CONST.cal.map.h);
    
    // Panel:
    var panel = new YAHOO.widget.Dialog("panel-cal", {
      buttons             : [
        { text: "Show points", handler: function (e) { calShowPoints(); } },
        { text: "Close", handler: function (e) { ui.panel.cal.p.hide(); }, isDefault: true }
      ],
      close               : true,
      constraintoviewport : true,
      dragOnly            : true,
      fixedcenter         : true,
      modal               : true,
      visible             : false
    });
    
    panel.showEvent.subscribe(  // select the last calibration if none has been selected so far
      function () {
        if (state.currCal.idx !== -1) return;
        
        var tbl = ui.tbl.cal.tbl;
        var tr = tbl.getLastTrEl();
        if (!tr) return;
        
        var radio = tbl.getFirstTdEl(tr).firstChild.firstChild;
        $setAttr(radio, { checked: true });
        tbl.selectRow(radio);
        
        state.currCal.ui.radio = tbl.getRecord(radio);
        state.currCal.ui.rec   = tbl.getRecord(radio);
        state.currCal.idx      = tbl.getRecordSet().getLength() - 1;
        
        calSetCurr();
      }
    );
    
    panel.render();
    ui.panel.cal.p = panel;
    
    // Data table -- columns:
    var cols = [
      { key: "curr", label: "", className: "curr", formatter: "radio" },
      { key: "t", label: "Time [ms]" },
      { key: "eye", label: "Eye" },
      { key: "pointCnt", label: "Points" },
      { label: "Result", children: [
          { key: "calRes", label: "Cal" },
          { key: "valRes", label: "Val" }
        ]
      },
      { label: "Val err [deg]", children: [
          { key: "valErrAvg", label: "Avg" },
          { key: "valErrMax", label: "Max" }
        ]
      },
      { key: "stimCnt", label: "Stim" },
      { key: "dur", label: "Dur [ms]" }
    ];
    
    // Data table -- event handlers:
    var onRadioClickEvent = function (oArgs) {
      var radio = oArgs.target;
      var rec = this.getRecord(radio);
      var newIdx = parseInt(rec.getData("idx"));
      
      if (newIdx === state.currCal.idx) return;
      
      state.currCal.ui.rec = rec;
      state.currCal.idx = newIdx;
      
      if (state.currCal.ui.radio) {
        state.currCal.ui.radio.setData("curr", false);
      }
      state.currCal.ui.radio = rec;
      rec.setData("curr", true);
      
      ui.tbl.cal.tbl.unselectAllRows();
      ui.tbl.cal.tbl.selectRow(radio);
      
      calSetCurr();
    };
    
    // Data table:
    genTbl(CONST.appId, ui.tbl.cal, $("panel-cal-dt"), ["idx","curr","t","eye","pointCnt","calRes","valRes","valErrAvg","valErrMax","stimDur","dur"], cols, { radioClickEvent: onRadioClickEvent }, { sortedBy: { key:"t", dir:"asc" } }, panel, true);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initConstr() {
    initConstr_genConstrPanel("Subjects", "sub",   function () { return getCurrExp().sub; });
    initConstr_genConstrPanel("Trials",   "trial", function () { return (getCurrExp().sub[0] ? getCurrExp().sub[0].trials : []); });
    initConstr_genConstrPanel("Stimuli",  "stim",  function () { return getCurrExp().stim; });
    initConstr_genConstrPanel("ROIs",     "roi",   function () { return []; });
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  /**
   * Generates a constraint panel.
   */
  function initConstr_genConstrPanel(title, objName, fnGetSrcData, fnSetIds) {
    // Button event handlers:
    var fnApply = function (e) {
      var S = $filter(function (x) { return x.selected; }, state.constr[objName]);  // selected items
      switch (S.length) {
        case 0:
          alert("At least one name needs to be selected.");
          return;
        
        case fnGetSrcData().length:
          ui.panel.constr.elName.className = "constr-lst constr-lst-none";
          ui.panel.constr.elName.innerHTML = "<span>no constraints</span>";
          state.an.aggr[objName + "Id"] = "";
          break;
        
        default:
          var html = "";
          var ids = ""
          ui.panel.constr.elName.className = "constr-lst constr-lst-some";
          for (var i=0, ni=S.length; i < ni; i++) {
            html += "<span>" + S[i].o.name + "</span>";
            ids += S[i].o.id + (i < ni-1 ? "," : "");
          }
          ui.panel.constr.elName.innerHTML = html;
          state.an.aggr[objName + "Id"] = ids;
      }
    };
    
    var fnAdd = function (e) {
      var val = ui.panel.constr[objName].taVar.value;
      if (val.length === 0) return;
      
      var V = state.an.aggr[objName + "Var"];
      var parent = ui.panel.constr.elVar;
      if (parent.childNodes.length === 1) $removeChildren(parent);
      
      parent.className = "constr-lst constr-lst-some";
      var span01 = $$("span", parent, null, "var", val);
      var span02 = $$("span", parent, null, "var-x", "X");
      span02.onclick = function (idx, parent, el) {
        return function (e) {
          V[idx] = null;
          parent.removeChild(el.nextSibling);
          parent.removeChild(el);
          
          if (!parent.hasChildNodes()) {
            parent.className = "constr-lst constr-lst-none";
            parent.innerHTML = "<span>no constraints</span>";
          }
        }
      }(V.length, parent, span01);
      
      V.push(val);
    };
    
    // HTML -- panel:
    var divPanel = $$("div", document.body);
    divPanel.style.width = "360px";
    var divTabView = $$("div", document.body);
    var hd = $$("div", divPanel, null, "hd", title);
    var bd = $$("div", divPanel, null, "bd");
    
    // HTML -- names:
    var divTabN = $$("div");
    var divNBtns = $$("div", divTabN);
    divNBtns.className = "btn-bar";
    divNBtns.style.marginBottom = "8px";
    var divNLst = $$("div", divTabN, null, "constr-panel-lst");
    
    new YAHOO.widget.Button({ type: "button", label: "All",     container: divNBtns, onclick: { fn: function (e) { $map(function (x) { if (!x.selected) x.div.onclick(); }, state.constr[objName]); } } });
    new YAHOO.widget.Button({ type: "button", label: "None",    container: divNBtns, onclick: { fn: function (e) { $map(function (x) { if (x.selected) x.div.onclick(); }, state.constr[objName]); } } });
    new YAHOO.widget.Button({ type: "button", label: "Inverse", container: divNBtns, onclick: { fn: function (e) { $map(function (x) { x.div.onclick(); }, state.constr[objName]); } } });
    var btn = new YAHOO.widget.Button({ type: "button", label: "Apply", container: divNBtns, onclick: { fn: fnApply } });
    btn.setStyle("margin-left", "30px");
    btn.setStyle("font-weight", "bold");
    
    // HTML -- variables:
    var divTabV = $$("div");
    var divVBtns = $$("div", divTabV);
    divVBtns.className = "btn-bar";
    divVBtns.style.marginBottom = "8px";
    var divVLst = $$("div", divTabV);
    var ta = $$("textarea", divTabV, null, "constr-var");
    ta.style.width  = "312px";
    ta.style.height = "120px";
    ui.panel.constr[objName].taVar = ta;
    
    var btn = new YAHOO.widget.Button({ type: "button", label: "Add", container: divVBtns, onclick: { fn: fnAdd } });
    btn.setStyle("font-weight", "bold");
    
    $$("span", divTabV, null, "note", "<b>EXAMPLES:</b><br />&nbsp;&nbsp;&bull;&nbsp;gender:`gender` = 1<br />&nbsp;&nbsp;&bull;&nbsp;reading-speed:log10(`reading-speed`) > 2");
    
    // HTML -- tab view:
    var tv = new YAHOO.widget.TabView();
    tv.addTab(new YAHOO.widget.Tab({ label: "Name", contentEl: divTabN, active: true }));
    tv.addTab(new YAHOO.widget.Tab({ label: "Variables", contentEl: divTabV }));
    tv.appendTo(bd);
    
    // Panel:
    var panel = new YAHOO.widget.Dialog(divPanel, {
      buttons             : [{ text: "Close", handler: function (e) { this.hide(); } }],
      constraintoviewport : true,
      dragOnly            : true,
      fixedcenter         : true,
      modal               : true,
      visible             : false
    });
    
    panel.showEvent.subscribe(
      function () {
        var D = fnGetSrcData();
        if (!state.constr[objName]) {
          // Names:
          $removeChildren(ui.panel.constr[objName].lstNames);
          state.constr[objName] = [];
          
          for (var i=0, ni=D.length; i < ni; i++) {
            var d = D[i];
            var div = $$("div", ui.panel.constr[objName].lstNames, null, "sel sel-1", d.name);
            div.onclick = function (x) {
              return function (e) {
                var d = state.constr[objName][x];
                d.selected = !d.selected;
                d.div.className = "sel " + (d.selected ? "sel-1" : "sel-0");
              };
            }(i);
            state.constr[objName].push({ o:d, div:div, selected:true });
          }
          
          // Variables:
          $removeChildren(ui.panel.constr[objName].lstVar);
          for (var i=0, ni=getCurrExp().vars[objName].length; i < ni; i++) {
            var v = getCurrExp().vars[objName][i];
            var div = $$("div", ui.panel.constr[objName].lstVar, null, "sel sel-2", v.name + " (" + v.type + ")");
          }
          
          panel.render();
        }
      }
    );
    
    panel.render();
    ui.panel.constr[objName].p = panel;
    ui.panel.constr[objName].lstNames = divNLst;
    ui.panel.constr[objName].lstVar   = divVLst;
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initConstrEvt() {
    // Event drop-down list:
    ui.panel.evt.fnBtnEvtClick = function (p_sType, p_aArgs, p_oItem) {
      ui.panel.evt.btnEvt.set("label", p_oItem.cfg.getProperty("text"));
      ui.panel.evt.btnEvt.set("value", p_oItem.cfg.getProperty("text"));
    }
    
    ui.panel.evt.btnEvt = new YAHOO.widget.Button({ container: "panel-evt-evt", menu: [], type: "menu" });
    ui.panel.evt.btnEvt.getMenu().render("panel-evt-evt");
    
    // Time drop-down list:
    var fnBtnTimeClick = function (p_sType, p_aArgs, p_oItem) {
      ui.panel.evt.btnTime.set("label", p_oItem.cfg.getProperty("text"));
      ui.panel.evt.btnTime.set("value", p_oItem.value);
    };
    
    ui.panel.evt.btnTime = new YAHOO.widget.Button({
      container: "panel-evt-t",
      label: "Response",
      value: "r",
      menu: [
        { text: "Response", value: "r", onclick: { fn: fnBtnTimeClick } },
        { text: "Prompt",   value: "p", onclick: { fn: fnBtnTimeClick } }
      ],
      type: "menu"
    });
    
    // Fixation drop-down lists:
    initConstrEvt_genFixCombo("panel-evt-fix-t01", "btnFixT01");
    initConstrEvt_genFixCombo("panel-evt-fix-t02", "btnFixT02");
    initConstrEvt_genFixCombo("panel-evt-fix-t03", "btnFixT03");
    initConstrEvt_genFixCombo("panel-evt-fix-t04", "btnFixT04");
    
    // Panel -- button event handlers:
    var fnAdd = function(e) {
      var u = ui.panel.evt;
      
      // Validate input:
      if (!u.btnEvt.get("value")) return alert("Please select an event.");
      
      var d = this.getData();
      var t01 = parseInt(d.t01);
      var t02 = parseInt(d.t02);
      var t03 = parseInt(d.t03);
      var t04 = parseInt(d.t04);
      
      if (isNaN(t01) || isNaN(t02) || isNaN(t03) || isNaN(t04) || t01 < 0 || t02 < 0 || t03 < 0 || t04 < 0) return alert("Please provide all times as non-negative integers.");
      if (t01 < t02) return alert("On the 'before onset' side of an event, the first time cannot be smaller than the second time.");
      if (t03 > t04) return alert("On the 'after onset' side of an event, the first time cannot be bigger than the second time.");
      
      var name = u.btnEvt.get("value");
      var E = state.an.aggr.evt;
      
      var fixT01 = u.btnFixT01.get("value");
      var fixT02 = u.btnFixT02.get("value");
      var fixT03 = u.btnFixT03.get("value");
      var fixT04 = u.btnFixT04.get("value");
      
      // Add (DS):
      E.push($enc(name) + ":" + u.btnTime.get("value") + "," + t01 + "," + t02 + "," + t03 + "," + t04 + "," + fixT01 + fixT02 + fixT03 + fixT04);
      
      // Add (UI):
      var txt = name + ": t=" + u.btnTime.get("label").toLowerCase() + "; b=(" + fixT01 + ", " + t01 + "-" + t02 + ", " + fixT02 + "); a=(" + fixT03 + ", " + t03 + "-" + t04 + ", " + fixT04 + ")";
      
      var parent = ui.tab.an.aggr.constr.evt;
      if (parent.childNodes.length === 1) $removeChildren(parent);
      parent.className = "constr-lst constr-lst-some";
      
      var span01 = $$("span", parent, null, "var", txt);
      var span02 = $$("span", parent, null, "var-x", "X");
      span02.onclick = function (idx, parent, el) {
        return function (e) {
          state.an.aggr.evt[idx] = "";
          parent.removeChild(el.nextSibling);
          parent.removeChild(el);
          
          if (!parent.hasChildNodes()) {
            parent.className = "constr-lst constr-lst-none";
            parent.innerHTML = "<span>no constraints</span>";
          }
        }
      }(E.length-1, parent, span01);
    };
    
    // Panel:
    var panel = new YAHOO.widget.Dialog("panel-evt", {
      buttons             : [
        { text: "Add", handler: fnAdd },
        { text: "Close", handler: function (e) { ui.panel.evt.p.hide(); }, isDefault: true }
      ],
      constraintoviewport : true,
      dragOnly            : true,
      fixedcenter         : true,
      modal               : true,
      visible             : false
    });
    
    // Panel -- events:
    panel.showEvent.subscribe(function (e) {
      ui.panel.evt.btnEvt.set("label", "---");
      ui.panel.evt.btnEvt.set("value", "");
    });
    
    // Finish up:
    panel.render();
    ui.panel.evt.p = panel;
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initConstrEvt_genFixCombo(parent, btn) {
    var fn = function (p_sType, p_aArgs, p_oItem) {
      ui.panel.evt[btn].set("label", p_oItem.cfg.getProperty("text"));
      ui.panel.evt[btn].set("value", p_oItem.value);
    }
    
    ui.panel.evt[btn] = new YAHOO.widget.Button({
      container: parent,
      label: "exc",
      type: "menu",
      menu: [
        { text: "exc", value: "e", onclick: { fn: fn } },
        { text: "inc", value: "i", onclick: { fn: fn } },
      ],
      value: "e"
    });
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initDS() {
    // Buttons:
    //ui.tab.res.ds.btnShow        = new YAHOO.widget.Button("res-ds-btn-show",        { onclick: { fn: dsShow },                         disabled: true });
    
    ui.tab.res.ds.btnShowTxt     = new YAHOO.widget.Button("res-ds-btn-show-txt",    { onclick: { fn: function (e) { dsShow("txt");  } }, disabled: true  });
    ui.tab.res.ds.btnShowHtml    = new YAHOO.widget.Button("res-ds-btn-show-html",   { onclick: { fn: function (e) { dsShow("html"); } }, disabled: true  });
    ui.tab.res.ds.btnRerun       = new YAHOO.widget.Button("res-ds-btn-rerun",       { onclick: { fn: function (e) { dsRerun(); } },      disabled: true });
    ui.tab.res.ds.btnSpreadsheet = new YAHOO.widget.Button("res-ds-btn-spreadsheet", { onclick: { fn: dsSpreadsheet }                                  });
    ui.tab.res.ds.btnDel         = new YAHOO.widget.Button("res-ds-btn-del",         { onclick: { fn: function (e) { dsDel(false); } },   disabled: true });
    ui.tab.res.ds.btnDelAll      = new YAHOO.widget.Button("res-ds-btn-del-all",     { onclick: { fn: function (e) { dsDel(true); } },    disabled: true });
    
    // Data table -- submitters:
    var fnAfterSuccess = function (prop) {
      return function (o,newVal,res) {
        getCurrExp().ds[o.getRecord().getData("idx")][prop] = newVal;
      };
    };
    
    var fnGetCmd = function (attr) {
      return function (o,newVal) {
        return { cmd: "ds-attr-set", "ds-id": o.getRecord().getData("id"), "attr-name": attr, "attr-val": $enc(newVal) };
      };
    };
    
    var fnSetName = getSetAttrFn([], fnAfterSuccess("name"), fnGetCmd("name"));
    var fnSetMemo = getSetAttrFn([], fnAfterSuccess("memo"), fnGetCmd("memo"));
    
    // Data table -- columns:
    var cols = [
      { key: "curr", label: "", className: "curr", formatter: "radio" },
      { key: "sel", label: "Sel", formatter: "checkbox"},
      { key: "name", label: "Name", sortable: true, editor: new YAHOO.widget.TextboxCellEditor({ asyncSubmitter: fnSetName }), width: 200, maxAutoWidth: 300, className: "name" },
      { key: "created", label: "Created", sortable: true, formatter: Pegasus.Main.UI.Tbl.Formatter.date, sortOptions: { sortFunction: Pegasus.Main.UI.Tbl.Sorter.genDate(function () { return getCurrExp().ds; }, "created") } },
      { key: "varCnt", label: "Var", sortable: true },
      { key: "obsCnt", label: "Obs", sortable: true },
      { key: "ts", label: "TS", formatter: YAHOO.widget.DataTable.formatNumber, sortable: true },
      { key: "size", label: "Size", sortable: true, width: 40 },
      { key: "calcTime", label: "Calc Time", sortable: true },
      { key: "noteLen", label: "Note", sortable: true },
      { key: "memo", label: "Memo", sortable: true, editor: new YAHOO.widget.TextboxCellEditor({ asyncSubmitter: fnSetMemo }), maxAutoWidth: 400 },
    ];
    
    // Data table -- event handlers:
    var onRadioClickEvent = function (oArgs) {
      var radio = oArgs.target;
      var rec = this.getRecord(radio);
      var newIdx = parseInt(rec.getData("idx"));
      
      if (newIdx === state.currDS.idx) return;
      
      state.currDS.ui.rec = rec;
      state.currDS.idx = newIdx;
      
      if (state.currDS.ui.radio) {
        state.currDS.ui.radio.setData("curr", false);
      }
      state.currDS.ui.radio = rec;
      rec.setData("curr", true);
      
      ui.tbl.ds.tbl.unselectAllRows();
      ui.tbl.ds.tbl.selectRow(radio);
      
      ui.tab.res.ds.btnShowTxt.set("disabled", false);
      ui.tab.res.ds.btnShowHtml.set("disabled", false);
      ui.tab.res.ds.btnDel.set("disabled", false);
      ui.tab.res.ds.btnRerun.set("disabled", false);
    };
    
    var onCheckboxClickEvent = function (oArgs) {
      var chbox = oArgs.target;
      var rec = this.getRecord(chbox);
      var val = chbox.checked;
      rec.setData("sel", val);
      getCurrExp().ds[parseInt(rec.getData("idx"))].sel = val;
    };
    
    // Data table:
    genTbl(CONST.appId, ui.tbl.ds, $("res-ds-dt"), ["idx","curr","sel","name","created","varCnt","obsCnt","ts","size","calcTime","noteLen","memo"], cols, { radioClickEvent: onRadioClickEvent, checkboxClickEvent: onCheckboxClickEvent }, { sortedBy: { key:"created", dir:"desc" } }, null, true);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initEvt() {
    // Buttons:
    ui.tab.ds.st.btnEvtExc = new YAHOO.widget.Button("ds-st-evt-btn-exc", { onclick: { fn: function (e) { excObjToggle("event", "evt", getCurrEvt(), ui.tbl.evt.tbl, state.currEvt.idx, ui.tab.ds.st.btnEvtExc, evtGetTblRow); } }, disabled: true });
    
    // Data table -- submitters:
    var fnGetCmd = function (attr) {
      return function (o,newVal) {
        return { cmd: "attr-set", "obj-name": "evt", "obj-human-name": "event", "obj-id": o.getRecord().getData("id"), "attr-name": attr, "attr-val": $enc(newVal) };
      };
    };
    
    var fnSetName = getSetAttrFn([valid.strNonEmpty], function (o,newVal,res) { data.exp[o.getRecord().getData("idx")].name = newVal; }, fnGetCmd("name"));
    var fnSetMemo = getSetAttrFn([],                  function (o,newVal,res) { data.exp[o.getRecord().getData("idx")].memo = newVal; }, fnGetCmd("memo"));
    
    // Data table -- columns:
    var cols = [
      { key: "curr", label: "", className: "curr", formatter: "radio" },
      { key: "exc", label: "x", className: "exc" },
      { key: "name", label: "Name", editor: new YAHOO.widget.TextboxCellEditor({ asyncSubmitter: fnSetName }), maxAutoWidth: 120, className: "name" },
      {
        label: "Time [ms]",
        children: [
          { key: "tPrompt", label: "Prompt" },
          { key: "tResp", label: "Resp" }
        ]
      },
      { key: "memo", label: "Memo", editor: new YAHOO.widget.TextareaCellEditor({ asyncSubmitter: fnSetMemo }), maxAutoWidth: 160 }
    ];
    
    // Data table -- event handlers:
    var onRadioClickEvent = function (oArgs) {
      var radio = oArgs.target;
      var rec = this.getRecord(radio);
      var newIdx = parseInt(rec.getData("idx"));
      
      if (newIdx === state.currEvt.idx) return;
      
      if (state.currEvt.ui.rec) state.currEvt.ui.rec.setData("curr", false);
      rec.setData("curr", true);
      
      state.currEvt.idx      = newIdx;
      state.currEvt.ui.rec   = rec;
      state.currEvt.ui.radio = radio;
      
      ui.tbl.evt.tbl.unselectAllRows();
      ui.tbl.evt.tbl.selectRow(radio);
      
      evtSetCurr();
    };
    
    // Data table:
    genTbl(CONST.appId, ui.tbl.evt, $("ds-st-evt-dt"), ["idx","curr","exc","name","tPrompt","tResp","memo"], cols, { radioClickEvent: onRadioClickEvent }, {}, null, true);
    
    // Mouse over and out:
    Pegasus.Main.dataTblAddCellMouseInfoEvts(
      ui.tbl.evt.tbl,
      function (rec, col, data) {
        if (col.key === "exc") {
          var e = getCurrStimSeq().evt[rec.getData("idx")];
          if (e.exc) ui.sbar.info.innerHTML = (e.excInfo && e.excInfo.length > 0 ? "<b>Exclusion info:</b> " + e.excInfo : "No exclusion info has been provided");
        }
        else if (col.key === "tPrompt" || col.key === "tResp") {
          ui.sbar.info.innerHTML = Pegasus.util.Util.ms2time(data);
        }
      },
      ui.sbar.info
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initExpPanel() {
    // Panel -- button event handlers:
    var fnClose = function (e) {
      if (state.currExp.idx !== -1) this.hide();
      else alert("Please select an expriment first. If neccessary please create one.");
    };
    
    // Panel:
    var panel = new YAHOO.widget.Dialog("panel-exp", {
      buttons             : [
        { text: "New", handler: expNew },
        { text: "Delete", handler: expDel },
        { text: "Close", handler: fnClose, isDefault: true }
      ],
      close               : false,
      constraintoviewport : true,
      dragOnly            : true,
      fixedcenter         : true,
      modal               : true,
      visible             : false
    });
    panel.render();
    ui.panel.exp.p = panel;
    
    // Button bar:
    ui.panel.exp.btnStats  = new YAHOO.widget.Button("panel-exp-btn-stats",   { onclick: { fn: expStats },                                  disabled: true });
    ui.panel.exp.btnExcRep = new YAHOO.widget.Button("panel-exp-btn-exc-rep", { onclick: { fn: expExcRep },                                 disabled: true });
    ui.panel.exp.btnImpRep = new YAHOO.widget.Button("panel-exp-btn-imp-rep", { onclick: { fn: function (e) { expImpRep(getCurrExp()); } }, disabled: true });
    ui.panel.exp.btnAdjEMs = new YAHOO.widget.Button("panel-exp-btn-adj-ems", { onclick: { fn: expAdjEMs },                                 disabled: true });
    ui.panel.exp.btnLnk    = new YAHOO.widget.Button("panel-exp-btn-lnk",     { onclick: { fn: expLnk },                                    disabled: true });
    ui.panel.exp.btnExport = new YAHOO.widget.Button("panel-exp-btn-export",  { onclick: { fn: expExport },                                 disabled: true });
    
    expSetBtnsEnabled(false);
    
    // Data table -- submitters:
    var fnGetCmd = function (attr) {
      return function (o,newVal) {
        return { cmd: "attr-set", "obj-name": "exp", "obj-human-name": "experiment", "obj-id": o.getRecord().getData("id"), "attr-name": attr, "attr-val": $enc(newVal) };
      };
    };
    
    var fnSetName = getSetAttrFn([valid.strNonEmpty], function (o,newVal,res) { data.exp[o.getRecord().getData("idx")].name = newVal; if (o.getRecord().getData("idx") === state.currExp.idx) { ui.sbar.currExp.innerHTML = newVal; } }, fnGetCmd("name"));
    var fnSetMemo = getSetAttrFn([],                  function (o,newVal,res) { data.exp[o.getRecord().getData("idx")].memo = newVal; }, fnGetCmd("memo"));
    
    // Data table -- columns:
    var cols = [
      { key: "curr", label: "", className: "curr", formatter: "radio" },
      { key: "name", label: "Name", sortable: true, editor: new YAHOO.widget.TextboxCellEditor({ asyncSubmitter: fnSetName }), width: 200, className: "name" },
      { key: "created", label: "Created", sortable: true, formatter: Pegasus.Main.UI.Tbl.Formatter.date, sortOptions: { sortFunction: Pegasus.Main.UI.Tbl.Sorter.genDate(function () { return data.exp; }, "created") } },
      { key: "subCnt", label: "Sub", sortable: true, formatter: YAHOO.widget.DataTable.formatNumber },
      { key: "trialCnt", label: "Trial", sortable: true, formatter: YAHOO.widget.DataTable.formatNumber },
      { key: "stimCnt", label: "Stim", sortable: true, formatter: YAHOO.widget.DataTable.formatNumber },
      { key: "roiCnt", label: "ROI", sortable: true, formatter: YAHOO.widget.DataTable.formatNumber },
      { key: "linked", label: "Linked", sortable: true },
      { key: "noteLen", label: "Note", sortable: true },
      { key: "memo", label: "Memo", sortable: true, editor: new YAHOO.widget.TextareaCellEditor({ asyncSubmitter: fnSetMemo }), width: 300 }
    ];
    
    // Data table -- event handlers:
    var onRadioClickEvent = function (oArgs) {
      var radio = oArgs.target;
      var rec = this.getRecord(radio);
      var newIdx = parseInt(rec.getData("idx"));
      
      if (newIdx === state.currExp.idx) return;
      
      state.currExp.ui.rec = rec;
      state.currExp.idx = newIdx;
      
      if (state.currExp.ui.radio) {
        state.currExp.ui.radio.setData("curr", false);
      }
      state.currExp.ui.radio = rec;
      rec.setData("curr", true);
      
      ui.tbl.exp.tbl.unselectAllRows();
      ui.tbl.exp.tbl.selectRow(radio);
      
      expSetBtnsEnabled(true);
      ui.sbar.currExp.innerHTML = rec.getData("name");
      
      if (!getCurrExp().loaded) {
        procShow("Retrieving the experiment...");
        execCmd(true,
          { cmd: "exp-get", id: getCurrExp().id},
          function (res) {
            data.exp[state.currExp.idx] = res.exp;
            expSetCurr(true);
          },
          null, null, true
        );
      }
      else expSetCurr(true);
    };
    
    // Data table:
    genTbl(CONST.appId, ui.tbl.exp, $("panel-exp-dt"), ["idx","curr","name","created","subCnt","trialCnt","stimCnt","roiCnt","linked","noteLen","memo"], cols, { radioClickEvent: onRadioClickEvent }, { sortedBy: { key:"name", dir:"asc" }, height: "500px" }, panel, true);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initHist() {
    // Tab:
    ui.tab.res.lst = $("res-hist-lst");
    
    var btnClear = new YAHOO.widget.Button("res-hist-btn-clear");
    btnClear.on("click", function (e) { $removeChildren(ui.tab.res.lst); });
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initImp() {
    // Type:
    function fnBtnTypeClick (p_sType, p_aArgs, p_oItem) {
      if (p_oItem.value === "a") {
        alert("Functionality not implemented yet.");
        return;
      }
      ui.tab.ds.imp.btnType.set("label", p_oItem.cfg.getProperty("text"));
    }
    
    ui.tab.ds.imp.btnType = new YAHOO.widget.Button({
      container: "ds-imp-type",
      label: "Overwrite",
      menu: [
        { text: "Overwrite", value: "o", onclick: { fn: fnBtnTypeClick } },
        { text: "Append",    value: "a", onclick: { fn: fnBtnTypeClick } }
      ],
      type: "menu",
      value: "o",
    });
    
    // Button:
    var btnStart = new YAHOO.widget.Button("ds-imp-btn-start");
    btnStart.on("click", function (e) { expImport(); });
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initMain() {
    ui.app.mask = $("app-mask");
    ui.region.app.body = $("app-bd");
    
    // (1) Layout:
    ui.main = new YAHOO.widget.Layout({
      units: [
        { position: "top",    body: "app-hd-outer", height: 36 },
        { position: "bottom", body: "app-ft-outer", height: 23 },
        { position: "center", body: "app-bd" }
      ]
    });
    ui.main.render();
    
    // (2) Toolbar:
    // (2a) Radio buttons:
    state.ui.tbarIdx = parseInt(Pegasus.util.Cookie.get(CONST.cookie.appName + "ui-tbar-idx")) || 0;
    ui.tbar.btnGrp = new YAHOO.widget.ButtonGroup("btn-grp-tbar");
    var B = ui.tbar.btnGrp.getButtons();
    B[0].on("click", function (e) { $hide("an-tabview"); $hide("res-tabview"); $show("ds-tabview");  state.ui.tbarIdx = 0; Pegasus.Main.setCookie(CONST.cookie.appName, "ui-tbar-idx", 0); refreshTab(); });
    B[1].on("click", function (e) { $hide("ds-tabview"); $hide("res-tabview"); $show("an-tabview");  state.ui.tbarIdx = 1; Pegasus.Main.setCookie(CONST.cookie.appName, "ui-tbar-idx", 1); refreshTab(); });
    B[2].on("click", function (e) { $hide("ds-tabview"); $hide("an-tabview");  $show("res-tabview"); state.ui.tbarIdx = 2; Pegasus.Main.setCookie(CONST.cookie.appName, "ui-tbar-idx", 2); refreshTab(); });
    
    // (2b) Other buttons:
    new YAHOO.widget.Button("tbar-btn-note", { onclick: { fn: function (e) { txtShow("Note", null, getCurrExp().note, true, true); } } });
    new YAHOO.widget.Button("tbar-btn-exp", { onclick: {fn: function (e) { ui.panel.exp.p.show(); } } });
    
    // (3) Tab view:
    state.ui.tabViewIdx.ds = parseInt(Pegasus.util.Cookie.get(CONST.cookie.appName + "ui-tabview-idx-ds")) || 0;
    ui.tabView.ds = new YAHOO.widget.TabView("ds-tabview", { orientation: "top" });
    ui.tabView.ds.set("activeIndex", state.ui.tabViewIdx.ds);
    ui.tabView.ds.on("activeIndexChange", function (e) { var idx = ui.tabView.ds.get("activeIndex"); state.ui.tabViewIdx.ds = idx; Pegasus.Main.setCookie(CONST.cookie.appName, "ui-tabview-idx-ds", idx); refreshTab(); } );
    
    state.ui.tabViewIdx.an = parseInt(Pegasus.util.Cookie.get(CONST.cookie.appName + "ui-tabview-idx-an")) || 0;
    ui.tabView.an = new YAHOO.widget.TabView("an-tabview", { orientation: "top" });
    ui.tabView.an.set("activeIndex", state.ui.tabViewIdx.an);
    ui.tabView.an.on("activeIndexChange", function (e) { var idx = ui.tabView.an.get("activeIndex"); state.ui.tabViewIdx.an = idx; Pegasus.Main.setCookie(CONST.cookie.appName, "ui-tabview-idx-an", idx); refreshTab(); } );
    
    state.ui.tabViewIdx.res = parseInt(Pegasus.util.Cookie.get(CONST.cookie.appName + "ui-tabview-idx-res")) || 0;
    ui.tabView.res = new YAHOO.widget.TabView("res-tabview", { orientation: "top" });
    ui.tabView.res.set("activeIndex", state.ui.tabViewIdx.res);
    ui.tabView.res.on("activeIndexChange", function (e) { var idx = ui.tabView.res.get("activeIndex"); state.ui.tabViewIdx.res = idx; Pegasus.Main.setCookie(CONST.cookie.appName, "ui-tabview-idx-res", idx); refreshTab(); } );
    
    // (4) Status bar:
    ui.sbar.currPrj = $("sbar-curr-prj");
    ui.sbar.currPrj.innerHTML = Pegasus.Main.getPrjName(state.prjId);
    
    ui.sbar.currExp = $("sbar-curr-exp");
    ui.sbar.currExp.onclick = function (e) { ui.panel.exp.p.show(); };
    
    ui.sbar.info = $("sbar-info");
    
    // (5) Other:
    ui.cmdRes = $("cmd-res");
    ui.cmdRes.onclick = function (e) { window.clearTimeout(state.timer.cmdRes); state.timer.cmdRes = null; $hide(ui.cmdRes); };
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initNotif() {
    ui.notif.lnk = $("notif-lnk");
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initProc() {
    ui.panel.proc.p = new YAHOO.widget.Panel("panel-proc", {
      width       : "240px",
      fixedcenter : true,
      close       : false,
      draggable   : false,
      modal       : true,
      visible     : false
    });
    ui.panel.proc.p.render();
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initScript() {
    var u = ui.tab.an.scr;
    
    u.txt = $("an-scr-txt");
    
    (new YAHOO.widget.Button("an-scr-btn-clear")).on("click", function (e) { if (u.txt.value.length === 0) return; if (!confirm("Clear the content of the script?")) return; u.txt.value = ""; });
    (new YAHOO.widget.Button("an-scr-btn-exec")).on("click", function (e) { if (u.txt.value.length === 0) return; scriptExec(); });
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initStim() {
    // Buttons:
    ui.tab.ds.stim.btnExc          = new YAHOO.widget.Button("ds-stim-btn-exc",            { onclick: { fn: function (e) { excObjToggle("stimulus", "stim", getCurrStim(), ui.tbl.stim.tbl, state.currStim.idx, ui.tab.ds.stim.btnExc, stimGetTblRow); } }, disabled: true });
    ui.tab.ds.stim.btnVar          = new YAHOO.widget.Button("ds-stim-btn-var",            { onclick: { fn: function (e) { varOpenValues(getCurrExp().stim, "stim", "Stimulus"); } } });
    ui.tab.ds.stim.btnROIsSav      = new YAHOO.widget.Button("ds-stim-btn-rois-sav",       { onclick: { fn: stimROIsSav },      disabled: true });
    ui.tab.ds.stim.btnROIsDef      = new YAHOO.widget.Button("ds-stim-btn-rois-def",       { onclick: { fn: stimROIsDef },      disabled: true });
    ui.tab.ds.stim.btnFileInf      = new YAHOO.widget.Button("ds-stim-btn-file-inf",       { onclick: { fn: stimGenFileInf },   disabled: true });
    ui.tab.ds.stim.btnFileROIs     = new YAHOO.widget.Button("ds-stim-btn-file-rois",      { onclick: { fn: stimGenFileROIs },  disabled: true });
    ui.tab.ds.stim.btnWordPredSet  = new YAHOO.widget.Button("ds-stim-btn-word-pred-set",  { onclick: { fn: stimWordPredSet },  disabled: true });
    ui.tab.ds.stim.btnWordPredShow = new YAHOO.widget.Button("ds-stim-btn-word-pred-show", { onclick: { fn: stimWordPredShow }, disabled: true });
    
    // Data table -- submitters:
    var fnAfterSuccess = function (prop) {
      return function (o,newVal,res) {
        getCurrExp().stim[o.getRecord().getData("idx")][prop] = newVal;
      };
    };
    
    var fnGetCmd = function (attr) {
      return function (o,newVal) {
        return { cmd: "attr-set", "obj-name": "stim", "obj-human-name": "stimulus", "obj-id": o.getRecord().getData("id"), "attr-name": attr, "attr-val": $enc(newVal) };
      };
    };
    
    var fnSetName       = getSetAttrFn([valid.strNonEmpty],                                                                      fnAfterSuccess("name"),       fnGetCmd("name"));
    var fnSetResH       = getSetAttrFn([valid.isNum, valid.getNumInRange(CONST.stim.res.h.min, CONST.stim.res.h.max)],           fnAfterSuccess("resH"),       fnGetCmd("res_h"));
    var fnSetResV       = getSetAttrFn([valid.isNum, valid.getNumInRange(CONST.stim.res.v.min, CONST.stim.res.v.max)],           fnAfterSuccess("resV"),       fnGetCmd("res_v"));
    var fnSetFtSize     = getSetAttrFn([valid.isNum, valid.getNumInRange(CONST.stim.ftSize.min, CONST.stim.ftSize.max)],         fnAfterSuccess("fontSize"),   fnGetCmd("font_size"));
    var fnSetFtFam      = getSetAttrFn([valid.strNonEmpty],                                                                      fnAfterSuccess("fontFamily"), fnGetCmd("font_fam"));
    var fnSetLetterSpac = getSetAttrFn([valid.isNum, valid.getNumInRange(CONST.stim.letterSpac.min, CONST.stim.letterSpac.max)], fnAfterSuccess("letterSpac"), fnGetCmd("letter_spac"));
    var fnSetLineH      = getSetAttrFn([valid.isNum, valid.getNumInRange(CONST.stim.lineH.min, CONST.stim.lineH.max)],           fnAfterSuccess("lineH"),      fnGetCmd("line_h"));
    var fnSetOffsetX    = getSetAttrFn([valid.isNum, valid.getNumInRange(CONST.stim.offset.min, CONST.stim.offset.max)],         fnAfterSuccess("offsetX"),    fnGetCmd("offset_x"));
    var fnSetOffsetY    = getSetAttrFn([valid.isNum, valid.getNumInRange(CONST.stim.offset.min, CONST.stim.offset.max)],         fnAfterSuccess("offsetY"),    fnGetCmd("offset_y"));
    var fnSetAlignH     = getSetAttrFn([],                                                                                       fnAfterSuccess("alignH"),     fnGetCmd("align_h"));
    var fnSetAlignV     = getSetAttrFn([],                                                                                       fnAfterSuccess("alignV"),     fnGetCmd("align_v"));
    var fnSetMarginT    = getSetAttrFn([valid.isNum, valid.getNumInRange(CONST.stim.margin.min, CONST.stim.margin.max)],         fnAfterSuccess("marginT"),    fnGetCmd("margin_t"));
    var fnSetMarginL    = getSetAttrFn([valid.isNum, valid.getNumInRange(CONST.stim.margin.min, CONST.stim.margin.max)],         fnAfterSuccess("marginL"),    fnGetCmd("margin_l"));
    var fnSetMarginR    = getSetAttrFn([valid.isNum, valid.getNumInRange(CONST.stim.margin.min, CONST.stim.margin.max)],         fnAfterSuccess("marginR"),    fnGetCmd("margin_r"));
    var fnSetPaddingT   = getSetAttrFn([valid.isNum, valid.getNumInRange(CONST.stim.padding.min, CONST.stim.padding.max)],       fnAfterSuccess("paddingT"),   fnGetCmd("padding_t"));
    var fnSetPaddingB   = getSetAttrFn([valid.isNum, valid.getNumInRange(CONST.stim.padding.min, CONST.stim.padding.max)],       fnAfterSuccess("paddingB"),   fnGetCmd("padding_b"));
    var fnSetROIH       = getSetAttrFn([valid.isNum, valid.getNumInRange(CONST.stim.roiH.min, CONST.stim.roiH.max)],             fnAfterSuccess("roiH"),       fnGetCmd("roi_h"));
    var fnSetROIExp     = getSetAttrFn([],                                                                                       fnAfterSuccess("roiExp"),     fnGetCmd("roi_exp"));
    var fnSetMemo       = getSetAttrFn([],                                                                                       fnAfterSuccess("memo"),       fnGetCmd("memo"));
    
    // Data table -- columns:
    var cols = [
      { key: "curr", label: "", className: "curr", formatter: "radio" },
      { key: "exc", label: "x", className: "exc" },
      { key: "name", label: "Name", editor: new YAHOO.widget.TextboxCellEditor({ asyncSubmitter: fnSetName }), maxAutoWidth: 80, className: "name" },
      {
        label: "Resolution<sup><b>1</b></sup>",
        children: [
          { key: "resH", label: "h", formatter: YAHOO.widget.DataTable.formatNumber, editor: new YAHOO.widget.TextboxCellEditor({ asyncSubmitter: fnSetResH }) },
          { key: "resV", label: "v", formatter: YAHOO.widget.DataTable.formatNumber, editor: new YAHOO.widget.TextboxCellEditor({ asyncSubmitter: fnSetResV }) }
        ]
      },
      {
        label: "Textual only",
        children: [
          { key: "txt", label: "Text", minWidth: 140 },
          {
            label: "Font",
            children: [
              { key: "fontFamily", label: "Family", editor: new YAHOO.widget.TextboxCellEditor({ asyncSubmitter: fnSetFtFam }), minWidth: 80 },
              { key: "fontSize", label: "Size<sup><b>2</b></sup>", formatter: YAHOO.widget.DataTable.formatNumber, editor: new YAHOO.widget.TextboxCellEditor({ asyncSubmitter: fnSetFtSize }) }
            ]
          },
          {
            label: "Letter",
            children: [
              { key: "letterSpac", label: "Space<sup><b>1</b></sup>", editor: new YAHOO.widget.TextboxCellEditor({ asyncSubmitter: fnSetLetterSpac }) }
            ]
          },
          {
            label: "Line",
            children: [
              { key: "lineH", label: "H<sup><b>3</b></sup>", formatter: YAHOO.widget.DataTable.formatNumber, editor: new YAHOO.widget.TextboxCellEditor({ asyncSubmitter: fnSetLineH }) }
            ]
          },
          {
            label: "Align",
            children: [
              { key: "alignH", label: "h", editor: new YAHOO.widget.RadioCellEditor({ radioOptions: [{ label: "Left", value: "l" },{ label: "Center", value: "c" },{ label: "Right", value: "r" }], asyncSubmitter: fnSetAlignH }) },
              { key: "alignV", label: "v", editor: new YAHOO.widget.RadioCellEditor({ radioOptions: [{ label: "Top", value: "t" },{ label: "Center", value: "c" },{ label: "Bottom", value: "b" }], asyncSubmitter: fnSetAlignV }) }
            ]
          },
          {
            label: "Margin<sup><b>1</b></sup>",
            children: [
              { key: "marginT", label: "t", formatter: YAHOO.widget.DataTable.formatNumber, editor: new YAHOO.widget.TextboxCellEditor({ asyncSubmitter: fnSetMarginT }) },
              { key: "marginL", label: "l", formatter: YAHOO.widget.DataTable.formatNumber, editor: new YAHOO.widget.TextboxCellEditor({ asyncSubmitter: fnSetMarginL }) },
              { key: "marginR", label: "r", formatter: YAHOO.widget.DataTable.formatNumber, editor: new YAHOO.widget.TextboxCellEditor({ asyncSubmitter: fnSetMarginR }) }
            ]
          },
          {
            label: "Padding<sup><b>1</b></sup>",
            children: [
              { key: "paddingT", label: "t", formatter: YAHOO.widget.DataTable.formatNumber, editor: new YAHOO.widget.TextboxCellEditor({ asyncSubmitter: fnSetPaddingT }) },
              { key: "paddingB", label: "b", formatter: YAHOO.widget.DataTable.formatNumber, editor: new YAHOO.widget.TextboxCellEditor({ asyncSubmitter: fnSetPaddingB }) }
            ]
          },
          {
            label: "ROI",
            children: [
              { key: "roiH", label: "H<sup><b>1</b></sup>", formatter: YAHOO.widget.DataTable.formatNumber, editor: new YAHOO.widget.TextboxCellEditor({ asyncSubmitter: fnSetROIH }) },
              { key: "roiExp", label: "Exp", editor: new YAHOO.widget.RadioCellEditor({ radioOptions: [{ label: "Yes", value: "1" },{ label: "No", value: "0" }], asyncSubmitter: fnSetROIExp }) }
            ]
          }
        ]
      },
      {
        label: "Offset<sup><b>1</b></sup>",
        children: [
          { key: "offsetX", label: "x", formatter: YAHOO.widget.DataTable.formatNumber, editor: new YAHOO.widget.TextboxCellEditor({ asyncSubmitter: fnSetOffsetX }) },
          { key: "offsetY", label: "y", formatter: YAHOO.widget.DataTable.formatNumber, editor: new YAHOO.widget.TextboxCellEditor({ asyncSubmitter: fnSetOffsetY }) },
        ]
      },
      { key: "memo", label: "Memo", editor: new YAHOO.widget.TextareaCellEditor({ asyncSubmitter: fnSetMemo }), maxAutoWidth: 80 },
      {
        label: "Status",
        children: [
          {
            label: "ROIs",
            children: [
              { key: "roiCnt", label: "Cnt", formatter: YAHOO.widget.DataTable.formatNumber }
            ]
          },
        ]
      }
    ];
    
    // Data table -- event handlers:
    var onCellClickEvent = function (oArgs) {
      var rec = this.getRecord(oArgs.target);
      var col = this.getColumn(oArgs.target);
      
      if (col.key !== "txt") this.onEventShowCellEditor(oArgs);
      else {
        var stim = getCurrExp().stim[rec.getData("idx")];
        var fnAfterSucess = function (o,newVal) {
          stim.txt = newVal;
          ui.tbl.stim.tbl.updateCell(rec, col, (newVal.length > CONST.stim.txtMaxLen ? newVal.substr(0, CONST.stim.txtMaxLen) + "..." : newVal));
        }
        var fnSetTxt = getSetAttrFn([], fnAfterSucess, function (o,newVal,res) { return { cmd: "attr-set", "obj-name": "stim", "obj-human-name": "stimulus", "obj-id": stim.id, "attr-name": "txt", "attr-val": $enc(newVal) }; });
        txtInShow("Stimulus text (" + stim.name + ")", null, stim.txt, "Save", function (e) { fnSetTxt(function (res, newVal) { if (res) { ui.panel.txtIn.p.hide(); }; }, txt = ui.panel.txtIn.txt.value) });
      }
    };
    
    var onRadioClickEvent = function (oArgs) {
      var radio = oArgs.target;
      var rec = this.getRecord(radio);
      var newIdx = parseInt(rec.getData("idx"));
      
      if (newIdx === state.currStim.idx) return;
      
      if (state.currStim.ui.rec) state.currStim.ui.rec.setData("curr", false);
      rec.setData("curr", true);
      
      state.currStim.idx      = newIdx;
      state.currStim.ui.rec   = rec;
      state.currStim.ui.radio = radio;
      
      ui.tbl.stim.tbl.unselectAllRows();
      ui.tbl.stim.tbl.selectRow(radio);
      
      stimSetBtnsEnabled(true);
    };
    
    // Data table:
    genTbl(CONST.appId, ui.tbl.stim, $("ds-stim-dt"), ["idx","curr","exc","name","resH","resV","txt","fontFamily","fontSize","letterSpac","lineH","alignH","alignV","roiH","roiExp","marginT","marginL","marginR","paddingT","paddingB","offsetX","offsetY","memo","roiCnt"], cols, { cellClickEvent: onCellClickEvent, radioClickEvent: onRadioClickEvent }, { sortedBy: { key:"name", dir:"asc" } }, null, true);
    
    // Mouse over and out:
    Pegasus.Main.dataTblAddCellMouseInfoEvts(
      ui.tbl.stim.tbl,
      function (rec, col, data) {
        if (col.key === "exc") {
          var s = getCurrExp().stim[rec.getData("idx")];
          if (s.exc) ui.sbar.info.innerHTML = (s.excInfo && s.excInfo.length > 0 ? "<b>Exclusion info:</b> " + s.excInfo : "No exclusion info has been provided");
        }
      },
      ui.sbar.info
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function stimSetBtnsEnabled(e) {
    var s = getCurrStim();
    ui.tab.ds.stim.btnExc          .set("label", (s.exc ? "Inc" : "Exc"));
    ui.tab.ds.stim.btnExc          .set("disabled", false);
    ui.tab.ds.stim.btnROIsSav      .set("disabled", $trim(s.txt).length === 0);
    ui.tab.ds.stim.btnROIsDef      .set("disabled", $trim(s.txt).length === 0);
    ui.tab.ds.stim.btnFileInf      .set("disabled", false);
    ui.tab.ds.stim.btnFileROIs     .set("disabled", s.roiCnt === 0);
    ui.tab.ds.stim.btnWordPredSet  .set("disabled", false);
    ui.tab.ds.stim.btnWordPredShow .set("disabled", false);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initStimSeq() {
    // Buttons:
    ui.tab.ds.st.btnStimSeqExc = new YAHOO.widget.Button("ds-st-stim-seq-btn-exc", { onclick: { fn: function (e) { excObjToggle("stimuli sequence entry", "stim_seq", getCurrStimSeq(), ui.tbl.stimSeq.tbl, state.currStimSeq.idx, ui.tab.ds.st.btnStimSeqExc, stimSeqGetTblRow); } }, disabled: true });
    ui.tab.ds.st.btnStimSeqEM = new YAHOO.widget.Button("ds-st-stim-seq-btn-em-data", { onclick: { fn: stimSeqShowEM }, disabled: true });
    ui.tab.ds.st.btnStimSeqXForm = new YAHOO.widget.Button("ds-st-stim-seq-btn-xform", { onclick: { fn: stimSeqShowXForm }, disabled: true });
    
    // Data table -- columns:
    var cols = [
      { key: "curr", label: "", className: "curr", formatter: "radio" },
      { key: "exc", label: "x", className: "exc" },
      { key: "name", label: "Name", className: "name" },
      {
        label: "Time [ms]",
        children: [
          { key: "t0", label: "Start" },
          { key: "t1", label: "End" }
        ]
      },
      {
        label: "Fixations",
        children: [
          { key: "fixCnt", label: "All" },
          { key: "fixCntROI", label: "ROI" },
          { key: "fixCntROIAdj", label: "ROI adj" },
          { key: "fixCntExc", label: "Exc" }
        ]
      },
      {
        label: "Blinks",
        children: [
          { key: "blinkCnt", label: "All" }
        ]
      },
      {
        label: "XForm",
        children: [
          { key: "xformCnt", label: "Cnt" }
        ]
      }
    ];
    
    // Data table -- event handlers:
    var onRadioClickEvent = function (oArgs) {
      var radio = oArgs.target;
      var rec = this.getRecord(radio);
      var newIdx = parseInt(rec.getData("idx"));
      
      if (newIdx === state.currStimSeq.idx) return;
      
      if (state.currStimSeq.ui.rec) state.currStimSeq.ui.rec.setData("curr", false);
      rec.setData("curr", true);
      
      state.currStimSeq.idx      = newIdx;
      state.currStimSeq.ui.rec   = rec;
      state.currStimSeq.ui.radio = radio;
      
      ui.tbl.stimSeq.tbl.unselectAllRows();
      ui.tbl.stimSeq.tbl.selectRow(radio);
      
      ui.tab.ds.st.btnStimSeqEM.set("disabled", rec.getData("fixCnt") === 0);
      ui.tab.ds.st.btnStimSeqXForm.set("disabled", rec.getData("xformCnt") === 0);
      
      resetCurrState(["currEvt"]);
      stimSeqSetCurr();
    };
    
    // Data table:
    genTbl(CONST.appId, ui.tbl.stimSeq, $("ds-st-stim-seq-dt"), ["idx","curr","exc","name","t0","t1","fixCnt","fixCntROI","fixCntROIAdj","fixCntExc","blinkCnt","xformCnt","stimId"], cols, { radioClickEvent: onRadioClickEvent }, {}, null, true);
    
    // Mouse over and out:
    Pegasus.Main.dataTblAddCellMouseInfoEvts(
      ui.tbl.stimSeq.tbl,
      function (rec, col, data) {
        if (col.key === "exc") {
          var ss = getCurrTrial().stimSeq[rec.getData("idx")];
          if (ss.exc) ui.sbar.info.innerHTML = (ss.excInfo && ss.excInfo.length > 0 ? "<b>Exclusion info:</b> " + ss.excInfo : "No exclusion info has been provided");
        }
        else if (col.key === "t0" || col.key === "t1") {
          ui.sbar.info.innerHTML = Pegasus.util.Util.ms2time(data);
        }
      },
      ui.sbar.info
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initSub() {
    // Buttons:
    ui.tab.ds.st.btnSubExc = new YAHOO.widget.Button("ds-st-sub-btn-exc", { onclick: { fn: function (e) { excObjToggle("subject", "sub", getCurrSub(), ui.tbl.sub.tbl, state.currSub.idx, ui.tab.ds.st.btnSubExc, subGetTblRow); } }, disabled: true });
    ui.tab.ds.st.btnSubVar = new YAHOO.widget.Button("ds-st-sub-btn-var", { onclick: { fn: function (e) { varOpenValues(getCurrExp().sub, "sub", "Subject"); } } });
    
    // Data table -- submitters:
    var fnAfterSuccess = function (prop) {
      return function (o,newVal,res) {
        getCurrExp().sub[o.getRecord().getData("idx")][prop] = newVal;
      };
    };
    
    var fnGetCmd = function (attr) {
      return function (o, newVal) {
        return { cmd: "attr-set", "obj-name": "sub", "obj-human-name": "subject", "obj-id": o.getRecord().getData("id"), "attr-name": attr, "attr-val": $enc(newVal) };
      };
    };
    
    var fnSetName = getSetAttrFn([valid.strNonEmpty], fnAfterSuccess("name"), fnGetCmd("name"));
    var fnSetSex  = getSetAttrFn([],                  fnAfterSuccess("sex"),  fnGetCmd("sex"));
    var fnSetMemo = getSetAttrFn([],                  fnAfterSuccess("memo"), fnGetCmd("memo"));
    
    // Data table -- columns:
    var cols = [
      { key: "curr", label: "", className: "curr", formatter: "radio" },
      { key: "exc", label: "x", className: "exc" },
      { key: "name", label: "Name", sortable: true, editor: new YAHOO.widget.TextboxCellEditor({ asyncSubmitter: fnSetName }), maxAutoWidth: 160, className: "name", className: "name" },
      { key: "memo", label: "Memo", sortable: true, editor: new YAHOO.widget.TextboxCellEditor({ asyncSubmitter: fnSetMemo }), maxAutoWidth: 160 }
    ];
    
    // Data table -- event handlers:
    var onRadioClickEvent = function (oArgs) {
      var radio = oArgs.target;
      var rec = this.getRecord(radio);
      var newIdx = parseInt(rec.getData("idx"));
      
      if (newIdx === state.currSub.idx) return;
      
      if (state.currSub.ui.rec) state.currSub.ui.rec.setData("curr", false);
      rec.setData("curr", true);
      
      state.currSub.idx      = newIdx;
      state.currSub.ui.rec   = rec;
      state.currSub.ui.radio = radio;
      
      ui.tbl.sub.tbl.unselectAllRows();
      ui.tbl.sub.tbl.selectRow(radio);
      
      ui.tab.ds.st.btnTrialCal.set("disabled", true);
      
      resetCurrState(["currTrial", "currStimSeq", "currEvt"]);
      subSetCurr();
    };
    
    // Data table:
    genTbl(CONST.appId, ui.tbl.sub, $("ds-st-sub-dt"), ["idx","curr","exc","name","memo"], cols, { radioClickEvent: onRadioClickEvent }, { sortedBy: { key:"name", dir:"asc" } }, null, true);
    
    // Mouse over and out:
    Pegasus.Main.dataTblAddCellMouseInfoEvts(
      ui.tbl.sub.tbl,
      function (rec, col, data) {
        if (col.key === "exc") {
          var s = getCurrExp().sub[rec.getData("idx")];
          if (s.exc) ui.sbar.info.innerHTML = (s.excInfo && s.excInfo.length > 0 ? "<b>Exclusion info:</b> " + s.excInfo : "No exclusion info has been provided");
        }
      },
      ui.sbar.info
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initTrials() {
    // Buttons:
    ui.tab.ds.st.btnTrialExc = new YAHOO.widget.Button("ds-st-trial-btn-exc", { onclick: { fn: function (e) { excObjToggle("trial", "trial", getCurrTrial(), ui.tbl.trial.tbl, state.currTrial.idx, ui.tab.ds.st.btnTrialExc, trialGetTblRow); } }, disabled: true });
    ui.tab.ds.st.btnTrialVar = new YAHOO.widget.Button("ds-st-trial-btn-var", { onclick: { fn: function (e) { varOpenValues(getCurrExp().trials, "trial", "Trial"); } } });
    ui.tab.ds.st.btnTrialCal = new YAHOO.widget.Button("ds-st-trial-btn-cal", { onclick: { fn: function (e) { ui.panel.cal.p.show(); } }, disabled: true });
    
    // Data table -- submitters:
    var fnAfterSuccess = function (prop) {
      return function (o,newVal,res) {
        getCurrSub().trials[o.getRecord().getData("idx")][prop] = newVal;
      };
    };
    
    var fnGetCmd = function (attr) {
      return function (o,newVal) {
        return { cmd: "attr-set", "obj-name": "trial", "obj-human-name": "trial", "obj-id": o.getRecord().getData("id"), "attr-name": attr, "attr-val": $enc(newVal) };
      };
    };
    
    var fnAfterSuccessName = function (o,newVal) {
      var idx = o.getRecord().getData("idx");
      $map(function (s) { s.trials[idx].name = newVal; }, getCurrExp().sub);
    };
    
    var fnSetName  = getSetAttrFn([valid.strNonEmpty],                                                 fnAfterSuccessName,      fnGetCmd("name"));
    var fnSetTBuff = getSetAttrFn([valid.getNumInRange(CONST.trial.tBuff.min, CONST.trial.tBuff.max)], fnAfterSuccess("tBuff"), fnGetCmd("t_buff"));
    var fnSetMemo  = getSetAttrFn([],                                                                  fnAfterSuccess("memo"),  fnGetCmd("memo"));
    
    // Data table -- columns:
    var cols = [
      { key: "curr", label: "", className: "curr", formatter: "radio" },
      { key: "exc", label: "x", className: "exc" },
      { key: "name", label: "Name", editor: new YAHOO.widget.TextboxCellEditor({ asyncSubmitter: fnSetName }), className: "name" },
      {
        label: "Time [ms]",
        children: [
          { key: "t0", label: "Exp t0" },
          { key: "t1", label: "Exp t1" },
          { key: "t0rec", label: "Rec t0" },
          { key: "t1rec", label: "Rec t1" },
          { key: "dur", label: "Dur" },
          { key: "tBuff", label: "Exc", formatter: YAHOO.widget.DataTable.formatNumber, editor: new YAHOO.widget.TextboxCellEditor({ asyncSubmitter: fnSetTBuff }) }
        ]
      },
      { key: "cal", label: "Calibrations" },
      { key: "memo", label: "Memo", editor: new YAHOO.widget.TextboxCellEditor({ asyncSubmitter: fnSetMemo }), maxAutoWidth: 160 }
    ];
    
    // Data table -- event handlers:
    var onRadioClickEvent = function (oArgs) {
      var radio = oArgs.target;
      var rec = this.getRecord(radio);
      var newIdx = parseInt(rec.getData("idx"));
      
      if (newIdx === state.currTrial.idx) return;
      
      if (state.currTrial.ui.rec) state.currTrial.ui.rec.setData("curr", false);
      rec.setData("curr", true);
      
      state.currTrial.idx      = newIdx;
      state.currTrial.ui.rec   = rec;
      state.currTrial.ui.radio = radio;
      
      ui.tbl.trial.tbl.unselectAllRows();
      ui.tbl.trial.tbl.selectRow(radio);
      
      ui.tab.ds.st.btnTrialCal.set("disabled", false);
      
      resetCurrState(["currStimSeq", "currEvt"]);
      trialSetCurr();
    };
    
    // Data table:
    genTbl(CONST.appId, ui.tbl.trial, $("ds-st-trial-dt"), ["idx","curr","exc","name","dur","tBuff","cal","memo"], cols, { radioClickEvent: onRadioClickEvent }, {}, null, true);
    
    // Mouse over and out:
    Pegasus.Main.dataTblAddCellMouseInfoEvts(
      ui.tbl.trial.tbl,
      function (rec, col, data) {
        if (col.key === "exc") {
          var t = getCurrSub().trials[rec.getData("idx")];
          if (t.exc) ui.sbar.info.innerHTML = (t.excInfo && t.excInfo.length > 0 ? "<b>Exclusion info:</b> " + t.excInfo : "No exclusion info has been provided");
        }
        else if (col.key === "dur" || col.key === "t0" || col.key === "t1" || col.key === "t0rec" || col.key === "t1rec") {
          ui.sbar.info.innerHTML = Pegasus.util.Util.ms2time(data);
        }
      },
      ui.sbar.info
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initTxtPanel() {
    // Panel -- button event handlers:
    var fnSelTxt = function (e) {
      if (ui.panel.txt.edit.style.display === "block") {
        ui.panel.txt.edit.select();
      }
      else {
        window.getSelection().removeAllRanges();
        var range = document.createRange();
        range.selectNode(ui.panel.txt.view);
        window.getSelection().addRange(range);
      }
    };
    
    // Panel -- event handlers:
    ui.panel.txt.fnNoteHide = function () {
      var u = ui.panel.txt;
      
      u.p.hideEvent.unsubscribe(u.fnNoteHide);
      
      var exp = getCurrExp();
      if (exp.note === u.edit.value) return;
      
      exp.note = u.edit.value;
      execCmd(false,
        { cmd: "attr-set", "obj-name": "exp", "obj-human-name": "experiment", "obj-id": exp.id, "attr-name": "note", "attr-val": $enc(exp.note) },
        function (res) {
          cmdResShow(res.outcome ? "Note saved" : "Not NOT saved");
        },
        null, null, true
      );
    };
    
    // Panel:
    var panel = new YAHOO.widget.Dialog("panel-txt",
      {
        buttons             : [
          { text: "Select text", handler: fnSelTxt },
          { text: "Close",       handler: function (e) { this.hide(); } }
        ],
        constraintoviewport : true,
        dragOnly            : true,
        fixedcenter         : true,
        modal               : true,
        visible             : false,
        width               : CONST.panelW
      }
    );
    panel.render();
    ui.panel.txt.p = panel;
    
    ui.panel.txt.msg  = $("panel-txt-msg");
    ui.panel.txt.view = $("panel-txt-view");
    ui.panel.txt.edit = $("panel-txt-edit");
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initTxtInPanel() {
    // Panel:
    var panel = new YAHOO.widget.Dialog("panel-txt-in",
      {
        buttons             : [
          { text: "Action", handler: function (e) { ui.panel.txt.txt.select(); } },
          { text: "Cancel", handler: function (e) { this.hide(); } }
        ],
        constraintoviewport : true,
        dragOnly            : true,
        fixedcenter         : true,
        modal               : true,
        visible             : false,
        width               : CONST.panelW
      }
    );
    panel.render();
    ui.panel.txtIn.p = panel;
    
    ui.panel.txtIn.msg    = $("panel-txt-in-msg");
    ui.panel.txtIn.txt    = $("panel-txt-in-txt");
    ui.panel.txtIn.btnAct = panel.getButtons()[0];
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initVar_genGenPanel() {
    // Panel:
    var fnGen = function (e) {
      var name = this.getData().name;
      
      // Validate input:
      if (ui.panel.varGen.btnVar.get("value") == "-") return alert("Please select the variable.");
      if (name === "") return alert("Please provide a new variable's name.");
      
      // Submit command:
      procShow("Generating variable...");
      execCmd(true,
        { cmd: "var-gen", "exp-id": getCurrExp().id, "var": ui.panel.varGen.btnVar.get("value"), name: name },
        function (res) {
          var v = res.v;
          ui.tbl.vars.sub.tbl.addRow({ idx: getCurrExp().vars.sub.length, id: v.id, type: v.type, name: v.name, memo: v.memo, sys: v.sys });
          getCurrExp().vars.sub.push(v);
          ui.panel.proc.p.hide();
          ui.panel.varGen.p.hide();
        },
        null, null, true
      );
    };
    
    var panel = new YAHOO.widget.Dialog("panel-ds-var-gen",
      {
        buttons             : [
          { text: "Generate", handler: fnGen },
          { text: "Close", handler: function (e) { this.hide(); }, isDefault: true }
        ],
        constraintoviewport : true,
        dragOnly            : true,
        fixedcenter         : true,
        modal               : true,
        visible             : false
      }
    );
    panel.render();
    ui.panel.varGen.p = panel;
    
    // "Variable" drop-down list:
    function fnBtnVarClick(p_sType, p_aArgs, p_oItem) {
      ui.panel.varGen.btnVar.set("label", p_oItem.cfg.getProperty("text"));
      ui.panel.varGen.btnVar.set("value", p_oItem.value);
      if (p_oItem.value !== "-") ui.panel.varGen.name.value = p_oItem.value;
    }
    
    ui.panel.varGen.btnVar = new YAHOO.widget.Button({
      type: "menu",
      label: "---",
      value: "-",
      menu: [
        { text: "---"                                                                   , value: "-"     , onclick: { fn: fnBtnVarClick } },
        { text: "Subject - Reading speed A (words-per-stimulus / total-stimulus-presentation-time)", value: "read-spd-a", onclick: { fn: fnBtnVarClick } }
      ],
      container: "panel-ds-var-gen-var"
    });
    
    ui.panel.varGen.name = $("panel-ds-var-gen-name");
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initVar_genROIMutatePanel() {
    // Panel:
    var fnMutate = function (e) {
      var name = this.getData().name;
      
      // Validate input:
      if (name === "") return alert("Please provide a new variable's name.");
      if (ui.panel.varROIMutate.btnMethod.get("value") == "-") return alert("Please select the method.");
      
      procShow("Mutating variable...");
      execCmd(true,
        { cmd: "varmutate-roi", id: getCurrExp().vars.roi[state.currVar.roi.idx].id, name: name, method: ui.panel.varROIMutate.btnMethod.get("value") },
        function (res) {
          var v = res.v;
          ui.tbl.vars.roi.tbl.addRow({ idx: getCurrExp().vars.roi.length, id: v.id, type: v.type, name: v.name, memo: v.memo, sys: v.sys });
          getCurrExp().vars.roi.push(v);
          ui.panel.proc.p.hide();
        },
        null, null, true
      );
    };
    
    var panel = new YAHOO.widget.Dialog("panel-var-roi-mutate",
      {
        buttons             : [
          { text: "Mutate variable", handler: fnMutate },
          { text: "Close", handler: function (e) { this.hide(); }, isDefault: true }
        ],
        constraintoviewport : true,
        dragOnly            : true,
        fixedcenter         : true,
        modal               : true,
        visible             : false
      }
    );
    panel.render();
    ui.panel.varROIMutate.p = panel;
    
    // "Method" drop-down list:
    function fnBtnMethodClick(p_sType, p_aArgs, p_oItem) {
      ui.panel.varROIMutate.btnMethod.set("label", p_oItem.cfg.getProperty("text"));
      ui.panel.varROIMutate.btnMethod.set("value", p_oItem.value);
    }
    
    ui.panel.varROIMutate.btnMethod = new YAHOO.widget.Button({
      type: "menu",
      label: "---",
      value: "-",
      menu: [
        { text: "Expand to the nearest clause",   value: "exp-c", onclick: { fn: fnBtnMethodClick } },
        { text: "Expand to the nearest sentence", value: "exp-s", onclick: { fn: fnBtnMethodClick } }
      ],
      container: "panel-var-roi-mutate-method"
    });
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initVar() {
    initVar_genROIMutatePanel();
    initVar_genGenPanel();
    
    // ROI-specific code:
    ui.tab.ds.vars.roi.btnMutate      = new YAHOO.widget.Button("ds-var-roi-btn-mutate",        { onclick: { fn: function (e) { ui.panel.varROIMutate.p.show(); } }, disabled: true  });
    ui.tab.ds.vars.roi.btnWordPredSet = new YAHOO.widget.Button("ds-var-roi-btn-word-pred-set", { onclick: { fn: function (e) { varROISetWordPred();            } }, disabled: false });
    
    // Generate data tables for all objects:
    initVar_genVar("sub", "Subject");
    initVar_genVar("trial", "Trial");
    initVar_genVar("stim", "Stimulus");
    initVar_genVar("roi", "ROI");
    
    // Panel:
    var panel = new YAHOO.widget.Dialog("panel-var", {
      buttons             : [
        { text: "Close", handler: function (e) { this.hide(); }, isDefault: true }
      ],
      constraintoviewport : true,
      dragOnly            : true,
      fixedcenter         : true,
      modal               : true,
      visible             : false
    });
    panel.render();
    ui.panel.vars.p = panel;
    
    // Data table:
    genTbl(CONST.appId, ui.tbl.varP, $("panel-var-dt"), ["name"], [], {}, { paginator: new YAHOO.widget.Paginator({ rowsPerPage: 20, alwaysVisible: false, containers: "panel-var-dt-paginator" }) }, panel, true);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  /**
   * Generates a variable list for a given object, e.g., 'sub'.
   */
  function initVar_genVar(objName, objNameH) {
    // Button event handlers:
    var fnAdd = function (e) {
      var name = prompt("Name of the new " + objNameH + " variable:");
      if (!name || (name && name.length === 0)) return;
      
      procShow("Adding variable...");
      execCmd(true,
        { cmd: "var-add", "exp-id": getCurrExp().id, "obj": objName, name: name },
        function (res) {
          var v = res.v;
          ui.tbl.vars[objName].tbl.addRow({ idx: getCurrExp().vars[objName].length, id: v.id, type: v.type, name: v.name, memo: v.memo, sys: v.sys });
          getCurrExp().vars[objName].push(v);
          ui.panel.proc.p.hide();
        },
        null, null, true
      );
    };
    
    var fnDel = function (e) {
      if (!confirm("Do you want to delete the selected " + objNameH + " variable?")) return;
      
      procShow("Deleting variables...");
      var idx = state.currVar[objName].idx;
      execCmd(true,
        { cmd: "var-del", id: getCurrExp().vars[objName][state.currVar[objName].idx].id },
        function (res) {
          getCurrExp().vars[objName][state.currVar[objName].idx] = null;
          ui.tbl.vars[objName].tbl.deleteRow(state.currVar[objName].ui.rec);
          ui.tab.ds.vars[objName].btnDel.set("disabled", true);
          
          ui.panel.proc.p.hide();
        },
        null, null, true
      );
    };
    
    // Buttons:
    var btnVal = new YAHOO.widget.Button("ds-var-" + objName + "-btn-val", { onclick: { fn: function (e) { varOpenValues(getCurrExp()[objName], objName, objNameH); } } });
    var btnAdd = new YAHOO.widget.Button("ds-var-" + objName + "-btn-add", { onclick: { fn: fnAdd } });
    var btnDel = new YAHOO.widget.Button("ds-var-" + objName + "-btn-del", { onclick: { fn: fnDel }, disabled: true });
    
    if (objName === "sub") new YAHOO.widget.Button("ds-var-" + objName + "-btn-gen", { onclick: { fn: varOpenGen } });
    
    ui.tab.ds.vars[objName].btnDel = btnDel;
    
    // Data table -- submitters:
    var fnGetCmd = function (attr) {
      return function (o,newVal) {
        return { cmd: "attr-set", "obj-name": "var", "obj-human-name": "Variable", "obj-id": o.getRecord().getData("id"), "attr-name": attr, "attr-val": $enc(newVal) };
      };
    };
    
    var fnSetName = getSetAttrFn([valid.strNonEmpty], function (o,newVal,res) { getCurrExp().vars[objName][o.getRecord().getData("idx")].name = newVal; }, fnGetCmd("name"));
    //var fnSetType = getSetAttrFn([]                 , function (o,newVal,res) { getCurrExp().vars[objName][o.getRecord().getData("idx")].type = newVal; }, fnGetCmd("type"));
    var fnSetType = getSetAttrFn([]                 , function (o,newVal,res) { getCurrExp().vars[objName] = res.vars; }, fnGetCmd("type"));
    var fnSetMemo = getSetAttrFn([],                  function (o,newVal,res) { getCurrExp().vars[objName][o.getRecord().getData("idx")].memo = newVal; }, fnGetCmd("memo"));
    
    // Data table -- columns:
    var cols = [
      { key: "curr", label: "", className: "curr", formatter: "radio" },
      { key: "name", label: "Name", editor: new YAHOO.widget.TextboxCellEditor({ asyncSubmitter: fnSetName }), width: 120, className: "name" },
      { key: "type", label: "T", editor: new YAHOO.widget.RadioCellEditor({ radioOptions: [{ label: "Bi-valued", value: "b" }, { label: "Numeric", value: "n" }, { label: "String", value: "s" }], asyncSubmitter: fnSetType }) },
      { key: "memo", label: "Memo", editor: new YAHOO.widget.TextareaCellEditor({ asyncSubmitter: fnSetMemo }), width: 160 },
      { key: "sys", label: "S" }
    ];
    
    // Data table -- event handlers:
    var onCellClickEvent = function (oArgs) {
      if (this.getRecord(oArgs.target).getData("sys") === 1) return;
      this.onEventShowCellEditor(oArgs);
    };
    
    var onRadioClickEvent = function (oArgs) {
      var radio = oArgs.target;
      var rec = this.getRecord(radio);
      var newIdx = parseInt(rec.getData("idx"));
      
      if (newIdx === state.currVar[objName].idx) return;
      
      if (state.currVar[objName].ui.rec) state.currVar[objName].ui.rec.setData("curr", false);
      rec.setData("curr", true);
      
      state.currVar[objName].idx      = newIdx;
      state.currVar[objName].ui.rec   = rec;
      state.currVar[objName].ui.radio = radio;
      
      ui.tbl.vars[objName].tbl.unselectAllRows();
      ui.tbl.vars[objName].tbl.selectRow(radio);
      
      ui.tab.ds.vars[objName].btnDel.set("disabled", (rec.getData("sys") === 1));
      if (objName === "roi") ui.tab.ds.vars.roi.btnMutate.set("disabled", (rec.getData("sys") === 1));
    };
    
    // Data table:
    genTbl(CONST.appId, ui.tbl.vars[objName], $("ds-var-" + objName + "-dt"), ["idx","curr","name","type","memo","sys"], cols, { cellClickEvent: onCellClickEvent, radioClickEvent: onRadioClickEvent }, { sortedBy: { key:"name", dir:"asc" } }, null, true);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function logOut() {
    if (!confirm("Log out?")) return;
    Pegasus.util.Cookie.remove(CONST.cookie.appName + "username");
    Pegasus.util.Cookie.remove(CONST.cookie.appName + "passwd");
    Pegasus.util.Cookie.remove(CONST.cookie.appName + "curr-exp-id");
    window.location.reload();
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function notifCheck() {
    ui.notif.lnk.style.display = (!getCurrExp() || (state.currExp.idx > -1 && getCurrExp().linked) ? "none" : "inline");
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function procShow(msg) {
    ui.panel.proc.p.setHeader(msg);
    ui.panel.proc.p.show();
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  /*
   * Populates the current tab if it contains at least one DataTable object.
   */
  function refreshTab() {
    if (state.app.startUp || state.currExp.idx == -1) return;
    
    switch (state.ui.tbarIdx) {
      case 0:  // DS
        switch (state.ui.tabViewIdx.ds) {
          case 1: refreshTab_DSVar();  break;
          case 2: refreshTab_DSStim(); break;
          case 3: refreshTab_DSST();   break;
        }
        break;
      
      case 2:  // RES
        switch (state.ui.tabViewIdx.res) {
          case 0: refreshTab_DS(); break;
        }
        break;
    }
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function refreshTab_DSStim() {
    var u = ui.tab.ds.stim;
    var tbl = ui.tbl.stim.tbl;
    
    // Populate:
    if (!u.populated) {
      u.populated = true;
      
      procShow("Populating the tab...");
      
      var fnFinish = function (e) {
        tbl.unsubscribe("postRenderEvent");
        Pegasus.Main.tblResort(tbl);
        
        ui.panel.proc.p.hide();
      }
      
      var fnStim = function (e) {
        u.btnROIsSav      .set("disabled", true);
        u.btnROIsDef      .set("disabled", true);
        u.btnFileInf      .set("disabled", true);
        u.btnFileROIs     .set("disabled", true);
        u.btnWordPredSet  .set("disabled", true);
        u.btnWordPredShow .set("disabled", true);
        
        tbl.onShow();
        tblClear(tbl);
        
        var rows = [];
        if (state.currExp.idx >= 0) {
          var S = getCurrExp().stim;
          for (var i=0, ni=S.length; i < ni; i++) {
            rows.push(stimGetTblRow(S[i], i, false));
          }
        }
        tbl.on("postRenderEvent", fnFinish);
        if (rows.length > 0) tbl.addRows(rows);
        else tbl.render();
      }
      
      fnStim();
    }
    
    // Resize:
    if (!u.resized) {
      uiResizeDataTbl(tbl, false, true);
      u.resized = true;
    }
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  /*
   * Data structure: Stimuli and trials
   */
  function refreshTab_DSST() {
    if (!getCurrExp()) return;
    
    var u = ui.tab.ds.st;
    
    // Populate:
    if (!u.populated) {
      u.populated = true;
      
      procShow("Populating the tab...");
      
      // Buttons:
      u.btnTrialCal.set("disabled", true);
      u.btnStimSeqEM.set("disabled", true);
      u.btnStimSeqXForm.set("disabled", true);
      
      var fnFinish = function (e) {
        ui.tbl.evt.tbl.unsubscribe("postRenderEvent");
        
        ui.panel.proc.p.hide();
      }
      
      // Events:
      var fnEvt = function (e) {
        ui.tbl.stimSeq.tbl.unsubscribe("postRenderEvent");
        
        var tbl = ui.tbl.evt.tbl;
        tbl.onShow();
        tbl.on("postRenderEvent", fnFinish);
        if (!tblClear(tbl)) tbl.render();
      };
      
      // Stimuli sequence:
      var fnStimSeq = function (e) {
        ui.tbl.trial.tbl.unsubscribe("postRenderEvent");
        
        var tbl = ui.tbl.stimSeq.tbl;
        tbl.onShow();
        tbl.on("postRenderEvent", fnEvt);
        if (!tblClear(tbl)) tbl.render();
      };
      
      // Trials:
      var fnTrials = function (e) {
        ui.tbl.sub.tbl.unsubscribe("postRenderEvent");
        Pegasus.Main.tblResort(ui.tbl.sub.tbl);
        
        var tbl = ui.tbl.trial.tbl;
        tbl.onShow();
        tbl.on("postRenderEvent", fnStimSeq);
        if (!tblClear(tbl)) tbl.render();
      };
      
      // Subjects:
      var fnSub = function (e) {
        ui.tbl.stim.tbl.unsubscribe("postRenderEvent");
        
        var tbl = ui.tbl.sub.tbl;
        tbl.onShow();
        tblClear(tbl);
        
        var rows = [];
        if (state.currExp.idx >= 0) {
          var S = getCurrExp().sub;
          for (var i=0, ni=S.length; i < ni; i++) {
            rows.push(subGetTblRow(S[i], i, false));
          }
        }
        tbl.on("postRenderEvent", fnTrials);
        if (rows.length > 0) tbl.addRows(rows);
        else tbl.render();
      };
      
      fnSub();
    }
    
    // Resize:
    if (!u.resized) {
      uiResizeDataTbl(ui.tbl.sub.tbl, false, true);
      uiResizeDataTbl(ui.tbl.trial.tbl, false, true);
      uiResizeDataTbl(ui.tbl.stimSeq.tbl, false, true);
      uiResizeDataTbl(ui.tbl.evt.tbl, false, true);
      u.resized = true;
    }
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function refreshTab_DSVar() {
    var u = ui.tab.ds.vars;
    
    // Populate:
    if (!u.populated) {
      u.populated = true;
      
      procShow("Populating the tab...");
      
      var fnFinish = function (e) {
        var tbl = ui.tbl.vars.roi.tbl;
        tbl.unsubscribe("postRenderEvent");
        Pegasus.Main.tblResort(tbl);
        
        ui.panel.proc.p.hide();
      }
      
      var varNames = ["sub","trial","stim","roi"];
      var iVN = 0;
      var vn = null;
      
      var fnVar = function (e) {
        if (vn) ui.tbl.vars[vn].tbl.unsubscribe("postRenderEvent");
        if (iVN > varNames.length-1) return fnFinish();
        
        vn = varNames[iVN++];
        ui.tab.ds.vars[vn].btnDel.set("disabled", true);
        
        var tbl = ui.tbl.vars[vn].tbl;
        tbl.onShow();
        tblClear(tbl);
        
        var rows = [];
        if (state.currExp.idx >= 0) {
          var V = getCurrExp().vars[vn];
          for (var i=0, ni=V.length; i < ni; i++) {
            var v = V[i];
            rows.push({ idx: i, id: v.id, type: v.type, name: v.name, memo: v.memo, sys: v.sys });
          }
        }
        tbl.on("postRenderEvent", fnVar);
        if (rows.length > 0) tbl.addRows(rows);
        else tbl.render();
      };
      
      fnVar();
    }
    
    // Resize:
    if (!u.resized) {
      uiResizeDataTbl(ui.tbl.vars.sub.tbl,   false, true);
      uiResizeDataTbl(ui.tbl.vars.trial.tbl, false, true);
      uiResizeDataTbl(ui.tbl.vars.stim.tbl,  false, true);
      uiResizeDataTbl(ui.tbl.vars.roi.tbl,   false, true);
      u.resized = true;
    }
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function refreshTab_DS() {
    var u = ui.tab.res.ds;
    
    // Populate:
    if (!u.populated) {
      u.populated = true;
      
      procShow("Populating the tab...");
      
      var fnFinish = function (e) {
        var tbl = ui.tbl.ds.tbl;
        tbl.unsubscribe("postRenderEvent");
        Pegasus.Main.tblResort(tbl);
        ui.tab.res.ds.btnDelAll.set("disabled", getCurrExp().ds.length === 0);
        
        ui.panel.proc.p.hide();
      }
      
      var fnDS = function (e) {
        var tbl = ui.tbl.ds.tbl;
        tbl.onShow();
        tblClear(tbl);
        
        var rows = [];
        if (state.currExp.idx >= 0) {
          var DS = getCurrExp().ds;
          for (var i=0, ni=DS.length; i < ni; i++) {
            rows.push(dsGetTblRow(DS[i], i, false));
          }
        }
        tbl.on("postRenderEvent", fnFinish);
        if (rows.length > 0) tbl.addRows(rows);
        else tbl.render();
      }
      
      fnDS();
    }
    
    // Resize:
    if (!u.resized) {
      uiResizeDataTbl(ui.tbl.ds.tbl, false, true);
      u.resized = true;
    }
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function resetCurrState(names) {
    for (var i=0; i < names.length; i++) {
      state[names[i]] = { idx: -1, ui: { rec: null, radio: null } };
    }
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function scriptExec() {
    state.script.lst = $filter(function (x) { return $trim(x).length > 0; }, ui.tab.an.scr.txt.value.split("\n"));
    var cmdCnt = state.script.lst.length;
    if (cmdCnt === 0) return;
    if (!confirm("Execute the script (" + cmdCnt + (cmdCnt === 1 ? " command" : " commands") + ")?")) return;
    
    state.script.active = true;
    state.script.idx = 0;
    scriptStep(false);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function scriptStep(wasLastCmdErr) {
    var fnFinish = function (msg) {
      state.script.active = false;
      alert(msg);
      ui.panel.proc.p.hide();
    };
    
    if (state.script.idx === state.script.lst.length) return fnFinish("Script execution ended. Commands executed: " + state.script.idx + ".");
    
    if (wasLastCmdErr && !confirm("The last command did not execute succesfully. Continue the script execusion?")) {
      alert("Script execution stopped. Commands executed: " + state.script.idx + ".");
      ui.panel.proc.p.hide();
      return;
    }
    
    try {
      procShow("Executing script (command " + (state.script.idx+1) + " of " + (state.script.lst.length) + ")")
      var fn = null;
      try {
        eval("fn = function () { script." + state.script.lst[state.script.idx] + "; }");
      }
      catch (ex) {}
      
      state.script.idx++;
      if (YAHOO.lang.isFunction(fn)) fn();
      else return fnFinish("Error executing the current command. Script execution will now be terminated.");
    }
    catch (ex) {}
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function stimGenFileInf() {
    var s = getCurrStim();
    var txt = 
      "res-h:"       + s.resH       + "\n" +
      "res-v:"       + s.resV       + "\n" +
      "font-fam:'"   + s.fontFamily.replace(/\'/g, "\\\'") + "'\n" +
      "font-size:"   + s.fontSize   + "\n" +
      "letter-spac:" + s.letterSpac + "\n" +
      "line-h:"      + s.lineH      + "\n" +
      "align-h:'"    + s.alignH     + "'\n" +
      "align-v:'"    + s.alignV     + "'\n" +
      "margin-t:"    + s.marginT    + "\n" +
      "margin-l:"    + s.marginL    + "\n" +
      "margin-r:"    + s.marginR    + "\n" +
      "padding-t:"   + s.paddingT   + "\n" +
      "padding-b:"   + s.paddingB   + "\n" +
      "roi-h:"       + s.roiH       + "\n" +
      "offset-x:"    + s.offsetX    + "\n" +
      "offset-y:"    + s.offsetY;
    txtShow("The 'inf' file for the '" + s.name + "' stimulus", "The text below can help during subsequent data imports. Copy and paste it into a file named <b>inf</b> (only filename, no extension) that should be placed in the directory of a stimulus (e.g., <b>stim/stimulus-x</b>). Alternatively, you can place it right in the <b>stim</b> directory; in that case it will be applied to all stimuli. If, however, a given stimulus has its own inf <b>file</b> (located in its directory), setting in that file will take precedence to the all-stimuli settings.", txt, false, false);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function stimGenFileROIs() {
    var s = getCurrStim();
    stimGetROIs(s,
      function () {
        var L = [];  // lines
        $map(
          function (r) {
            var l = r.name + "\t" + r.x1 + "," + r.y1 + "," + r.x2 + "," + r.y2 + "\t";
            var V = r.vars;
            for (var v in V) l += v + ":" + V[v] + ",";
            l = l.substring(0,l.length-1);  // get rid of the trailing comma
            L.push(l);
          },
          s.rois
        );
        
        ui.panel.proc.p.hide();
        txtShow("The 'rois' file for the '" + s.name + "' stimulus", "The text below can help during subsequent data imports. Copy and paste it into a file named <b>rois</b> (only filename, no extension) that should be placed in the directory of a stimulus (e.g., <b>stim/stimulus-x/</b>).", L.join("\n"), false, false);
      }
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function stimGetROIs(s, cb) {
    if (!s.rois) {
      procShow("Retrieving data...");
      execCmd(true,
        { cmd: "stim-get-rois", "stim-id": s.id },
        function (res) {
          s.rois = res.rois;
          cb();
        },
        null, null, true
      );
    }
    else cb();
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function stimWordPredSet() {
    var s = getCurrStim();
    if (prompt("You are about to initiate the retrieval of the Google and Microsoft ngram-based word predictability for the text in the selected stimulus.  This process will add word predictability based on 3-, 4-, and 5-grams and involves a repeated querying of an external Web service and may therefore be time-consuming. To continue, type 'SET-PRED' below:").toLowerCase() != "set-pred") return;
    
    procShow("Retrieving word predictability...");
    
    execCmd(true, { cmd: "stim-set-word-pred", "stim-id": s.id },
      function (res) {
        alert(res.rep);
        ui.panel.proc.p.hide();
      },
      null, null, true
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function stimWordPredShow() {
    var s = getCurrStim();
    stimWordPredShow_get(s,
      function () {
        ui.panel.proc.p.hide();
        txtShow("Word predictability for stimulus '" + s.name + "'", null, s.wordPred, false, false);
      }
    );
  }
  
  
  // ----^----
  function stimWordPredShow_get(s, cb) {
    if (!s.wordPred) {
      procShow("Retrieving data...");
      execCmd(true,
        { cmd: "stim-get-word-pred", "stim-id": s.id },
        function (res) {
          s.wordPred = res.wordPred;
          cb();
        },
        null, null, true
      );
    }
    else cb();
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function stimGetTblRow(o, idx, curr) {
    return $setProp($setObj({}, o), { idx: idx, curr: curr, txt: (o.txt.length > CONST.stim.txtMaxLen ? o.txt.substr(0, CONST.stim.txtMaxLen) + "..." : o.txt), exc: (o.exc ? CONST.tbl.exc : "") });
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function stimSeqBtnsSetEnabled(e) {
    ui.tab.ds.st.btnStimSeqExc.set("disabled", !e);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function stimSeqSetCurr() {
    // Buttons:
    stimSeqBtnsSetEnabled(true);
    evtBtnsSetEnabled(false);
    ui.tab.ds.st.btnStimSeqExc.set("label", (getCurrStimSeq().exc ? "Inc" : "Exc"));
    
    // Events:
    var tblEvt = ui.tbl.evt.tbl;
    tblClear(tblEvt);
    var E = getCurrStimSeq().evt;
    var rows = [];
    for (var i=0, ni=E.length; i < ni; i++) {
      rows.push(evtGetTblRow(E[i], i, false));
    }
    tblEvt.addRows(rows);
    uiResizeDataTbl(tblEvt, false, true);
    
    resetCurrState(["currEvt"]);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function stimSeqGetTblRow(o, idx, curr) {
    return $setProp($setObj({}, o), { idx: idx, curr: curr, exc: (o.exc ? CONST.tbl.exc : "") });
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function stimSeqShowEM() {
    var ss = getCurrStimSeq();
    
    var S = getCurrExp().stim;
    var s = null;
    var idx = 0;
    for (; idx < S.length; idx++) {
      if (S[idx].id === ss.stimId) break;
    }
    s = S[idx];
    
    var uri =
      "popups/gaze/stim-seq-em.html" +
      "?exp-idx="      + state.currExp.idx +
      "&sub-idx="      + state.currSub.idx +
      "&trial-idx="    + state.currTrial.idx +
      "&stim-seq-idx=" + state.currStimSeq.idx +
      "&stim-idx="     + idx;
    
    // Finish:
    var fnFinish = function () {
      procShow("\"Eye movements\" window is open...");
      ui.tab.ds.st.winEM = window.open(uri, "", "toolbar=0,status=0");
    }
    
    // ROIs:
    var fnROIs = function () {
      stimGetROIs(s, function () { fnFinish(); });
    }
    
    // Get EM data:
    stimSeqRetrieveEMData(ss, fnROIs);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function stimSeqShowXForm() {
    var ss = getCurrStimSeq();
    stimSeqRetrieveEMData(
      ss,
      function () {
        txtShow("Transformations", "Number of transformations applied: <b>" + ss.xform.length + "</b>", ss.xform.join("\n"), false, false);
      }
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  /**
   * Retrieves EM data for the specified stimulus sequence entry. The 'fnAfter()' function is called after the
   * retrieval has finished. The data is retrieved only if it hasn't been retrieved before.
   */
  function stimSeqRetrieveEMData(ss, fnAfter) {
    // it is enough to check the 'ss.fix', since all EM data are retrieved with a single call,
    // so if 'ss.fix' is null that means no EM data have been requested yet
    if (ss.fix === null) {
      procShow("Retrieving data...");
      execCmd(true,
        { cmd: "stim-seq-get-em-data", "stim-seq-id": ss.id },
        function (res) {
          ss.fix      = res.fix;
          ss.blinks   = res.blinks;
          ss.xform    = res.xform;
          ss.xformCnt = res.xformCnt;
          
          ui.panel.proc.p.hide();
          fnAfter();
        },
        null, null, true
      );
    }
    else fnAfter();
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function stimROIsDef() {
    var s = getCurrStim();
    
    procShow("\"ROIs\" window is open...");
    var uri =
      "../pegasus/popups/gaze/stim-rois-def-txt.html" +
      "?exp-idx="     + state.currExp.idx +
      "&stim-idx="    + state.currStim.idx +
      "&txt="         + encodeURIComponent(s.txt) +
      "&res-h="       + s.resH +
      "&res-v="       + s.resV +
      "&font-family=" + s.fontFamily +
      "&font-size="   + s.fontSize +
      "&letter-spac=" + s.letterSpac +
      "&line-h="      + s.lineH +
      "&offset-x="    + s.offsetX +
      "&offset-y="    + s.offsetY +
      "&align-h="     + s.alignH +
      "&align-v="     + s.alignV +
      "&margin-t="    + s.marginT +
      "&margin-l="    + s.marginL +
      "&margin-r="    + s.marginR +
      "&padding-t="   + s.paddingT +
      "&padding-b="   + s.paddingB +
      "&roi-h="       + s.roiH +
      "&roi-exp="     + s.roiExp +
      "&img-uri="     + encodeURIComponent("../../" + CONST.serverURI_S + "?" + s.img.uri);
    ui.tab.ds.stim.winROIs = window.open(uri, "", "toolbar=0,status=0");
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function stimROIsSav() {
    var uri =
      "../pegasus/popups/gaze/stim-rois-sav.html" +
      "?exp-idx="  + state.currExp.idx +
      "&stim-cnt=" + getCurrExp().stim.length +
      "&curr-stim-idx=" + state.currStim.idx;
    ui.tab.ds.stim.winROIs = window.open(uri, "", "toolbar=0,status=0");
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function subBtnsSetEnabled(e) {
    ui.tab.ds.st.btnSubExc.set("disabled", !e);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function subGetTblRow(o, idx, curr) {
    return $setProp($setObj({}, o), { idx: idx, curr: curr, exc: (o.exc ? CONST.tbl.exc : "") });
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function subSetCurr() {
    // Buttons:
    subBtnsSetEnabled(true);
    trialBtnsSetEnabled(false);
    stimSeqBtnsSetEnabled(false);
    evtBtnsSetEnabled(false);
    ui.tab.ds.st.btnSubExc.set("label", (getCurrSub().exc ? "Inc" : "Exc"));
    
    // Trials:
    var tblTrials = ui.tbl.trial.tbl;
    tblClear(tblTrials);
    var T = getCurrSub().trials;
    var rows = [];
    for (var i=0, ni=T.length; i < ni; i++) {
      rows.push(trialGetTblRow(T[i], i, false));
    }
    tblTrials.addRows(rows);
    uiResizeDataTbl(tblTrials, false, true);
    
    // Stimuli sequence:
    var tblStimSeq = ui.tbl.stimSeq.tbl;
    tblClear(tblStimSeq);
    
    // Events:
    var tblEvt = ui.tbl.evt.tbl;
    tblClear(tblEvt);
    
    resetCurrState(["currTrial","currStimSeq","currCal","currEvt"]);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  /**
   * Remove all rows from a table. Returns `false` if the table is already empty.
   */
  function tblClear(t) {
    if (t.getRecordSet().getLength() > 0) {
      t.deleteRows(0, t.getRecordSet().getLength());
      return true;
    }
    else return false;
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function trialBtnsSetEnabled(e) {
    ui.tab.ds.st.btnTrialExc.set("disabled", !e);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function trialGetTblRow(o, idx, curr) {
    var calCnt = o.cal.length;
    var c = (calCnt === 0 ? null : o.cal[calCnt-1]);
    
    return $setProp($setObj({}, o), { idx: idx, curr: curr, exc: (o.exc ? CONST.tbl.exc : ""), cal: (c === null ? "0" : calCnt + ";" + c.pointCnt + "," + c.calRes.charAt(0) + "," + c.valRes.charAt(0) + "," + c.valErrMax) });
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function trialSetCurr() {
    // Buttons:
    trialBtnsSetEnabled(true);
    stimSeqBtnsSetEnabled(false);
    evtBtnsSetEnabled(false);
    ui.tab.ds.st.btnTrialExc.set("label", (getCurrTrial().exc ? "Inc" : "Exc"));
    
    // Stimuli sequence:
    var tblStimSeq = ui.tbl.stimSeq.tbl;
    tblClear(tblStimSeq);
    var SS = getCurrTrial().stimSeq;
    var rows = [];
    for (var i=0, ni=SS.length; i < ni; i++) {
      rows.push(stimSeqGetTblRow(SS[i], i, false));
    }
    tblStimSeq.addRows(rows);
    uiResizeDataTbl(tblStimSeq, false, true);
    ui.tab.ds.st.btnStimSeqEM.set("disabled", true);
    ui.tab.ds.st.btnStimSeqXForm.set("disabled", true);
    
    // Events:
    var tblEvt = ui.tbl.evt.tbl;
    tblClear(tblEvt);
    
    // Calibrations:
    var tblCal = ui.tbl.cal.tbl;
    tblClear(tblCal);
    var C = getCurrTrial().cal;
    var rows = [];
    for (var i=0, ni=C.length; i < ni; i++) {
      var o = $setProp(null, C[i]);
      o.idx = i;
      rows.push(o);
    }
    tblCal.addRows(rows);
    uiResizeDataTbl(tblCal, false, true);
    calSetBtnsEnabled(false);
    ui.panel.cal.map.clear();
    
    resetCurrState(["currStimSeq","currCal","currEvt"]);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function txtShow(title, msg, txt, edit, note) {
    var u = ui.panel.txt;
    
    if (edit) {
      u.edit.value = txt;
      $hide(u.view);
      $show(u.edit);
    }
    else {
      u.view.innerHTML = txt;
      $hide(u.edit);
      $show(u.view);
    }
    
    if (note) u.p.hideEvent.subscribe(u.fnNoteHide);
    
    u.msg.innerHTML = (msg && msg.length > 0 ? msg + "<br /><br />" : "");
    u.p.setHeader(title);
    
    u.p.show();
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function txtInShow(title, msg, txt, actBtnLbl, fnActBtn) {
    var u = ui.panel.txtIn;
    
    u.msg.innerHTML = (msg && msg.length > 0 ? msg + "<br /><br />" : "");
    u.txt.value = txt;
    u.p.setHeader(title);
    u.btnAct.set("label", actBtnLbl);
    u.btnAct.removeListener("click");
    u.btnAct.on("click", fnActBtn);
    u.p.show();
    u.txt.focus();
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function uiResizeDataTbl(dt, x, y) {
    var regB = ui.region.app.body;
    var regDT = YAHOO.util.Dom.getRegion(dt.getTableEl());
    if (x) {
      var adj = regDT.right > (regB.right - 40);
      dt.set("width", (adj ? (regB.width - regDT.left) + "px" : ""));
    }
    if (y) {
      var adj = regDT.bottom > (regB.bottom - 40);
      dt.set("height", (adj ? (regB.height - regDT.top) + "px" : ""));
    }
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function varROISetWordPred() {
    if (prompt("You are about to initiate the retrieval of the Google and Microsoft ngram-based word predictability for the text in all the stimuli defined in the current experiment.  This process will add word predictability based on 3-, 4-, and 5-grams and involves a repeated querying of an external Web service and may therefore be time-consuming. To continue, type 'SET-PRED' below:").toLowerCase() != "set-pred") return;
    
    procShow("Retrieving word predictability...");
    
    execCmd(true, { cmd: "exp-set-word-pred", "exp-id": data.exp[state.currExp.idx].id },
      function (res) {
        alert(res.rep);
        ui.panel.proc.p.hide();
      },
      null, null, true
    );
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function varOpenGen() {
    ui.panel.varGen.p.show();
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function varOpenValues(O, objName, objNameH) {
    if (objName === "trial" || objName === "roi") return alert("Not implemented yet.");
    
    procShow("Please wait...");
    var V = getCurrExp().vars[objName];
    
    // Data table -- submitters:
    var fnSet = function (V, varIdx) {
      return getSetAttrFn(
        V, 
        function (o, newVal, res) {
          var oId = o.getRecord().getData("oid");
          var val = getCurrExp().vars[objName][varIdx].val;
          val[oId] = {};
          val[oId].id = res.id;
          val[oId][objName + "Id"] = oId;
          val[oId].val = newVal
        },
        function (o, newVal) {
          return { cmd: "var-set", "obj-name": objName, "obj-id": o.getRecord().getData("oid"), "var-id": getCurrExp().vars[objName][varIdx].id, "val": $enc(newVal) };
        }
      );
    };
    
    var t = ui.tbl.varP.tbl;
    
    // Data table -- remove rows and columns:
    tblClear(t);
    for (var i=0, ni=t.getColumnSet().getDefinitions().length; i < ni; i++) t.removeColumn(0);
    
    // Data table -- insert columns:
    t.insertColumn({ key: "oname", label: objNameH, sortable: true, width: 120 });
    for (var i=0, ni=V.length; i < ni; i++) {
      var v = V[i];
      
      var idx = $$$(i);
      var formatter = undefined;
      var editor = undefined;
      
      switch (v.type) {
        case "b":
          editor = new YAHOO.widget.RadioCellEditor({ radioOptions: [{ label: "Missing", value: ""},"0","1"], asyncSubmitter: fnSet([], idx) });
          break;
        case "n":
          editor = new YAHOO.widget.TextboxCellEditor({ asyncSubmitter: fnSet([valid.isNum], idx) })
          break;
        case "s":
          editor = new YAHOO.widget.TextboxCellEditor({ asyncSubmitter: fnSet([], idx) })
          break;
      }
      
      t.insertColumn({ key: "vname_" + v.name, label: v.name, formatter: formatter, editor: editor });
    }
    
    // Data table -- add rows:
    var rows = [];
    for (var i=0, ni=O.length; i < ni; i++) {
      var o = O[i];
      var r = { oidx: i, oid: o.id, oname: o.name };
      
      for (var j=0, nj=V.length; j < nj; j++) {
        var v = V[j];
        if (!v.val[o.id]) continue;
        
        r.vidx = $$$(j);
        r["vname_" + v.name] = v.val[o.id].val;
      }
      
      rows.push(r);
    }
    
    // Data table -- finish up:
    t.on("postRenderEvent",
      function (e) {
        ui.panel.vars.p.show();
        t.onShow();
        ui.panel.proc.p.hide();
      }
    );
    if (rows.length > 0) t.addRows(rows);
    else t.render();
  }
  
  
  // ===================================================================================================================
  // ==[ SCRIPT ]=======================================================================================================
  // ===================================================================================================================
  
  var script = {
    calcAggr: function (qry, run) { calcAggr(qry, run); }
  };
  
  
  // ===================================================================================================================
  // ==[ PUBLIC ]=======================================================================================================
  // ===================================================================================================================
  
  var pub = {
    
    // -----------------------------------------------------------------------------------------------------------------
    excFirstNFix: function (expIdx, subIdx, trialIdx, stimIdx, stimSeqIdx, n) {
      var trialId    = data.exp[expIdx].sub[subIdx].trials[trialIdx].id;
      var stimSeqId  = data.exp[expIdx].sub[subIdx].trials[trialIdx].stimSeq[stimSeqIdx].id;
      var t0         = data.exp[expIdx].sub[subIdx].trials[trialIdx].stimSeq[stimSeqIdx].t0;
      var t1         = data.exp[expIdx].sub[subIdx].trials[trialIdx].stimSeq[stimSeqIdx].t1;
      var stimId     = data.exp[expIdx].stim[stimIdx].id;
      
      execCmd(true,
        { cmd: "stim-seq-exc-first-n-fix", "trial-id": trialId, "stim-id": stimId, "stim-seq-id": stimSeqId, t0: t0, t1: t1, n: n },
        function (res) {
          var ss = data.exp[expIdx].sub[subIdx].trials[trialIdx].stimSeq[stimSeqIdx];
          ss.fixCntExc = n;
          
          var set = { idx: state.currStimSeq.idx, curr: true };
          $setObj(set, ss);
          
          ui.tbl.stimSeq.tbl.updateRow(state.currStimSeq.idx, set);
          
          data.exp[expIdx].linked = false;
          $show(ui.notif.lnk);
        },
        function (res) {
          var win = (ui.tab.ds.st.winEM || window);
          if (win.excFirstNFix_resp) win.excFirstNFix_resp();
          win.alert((res.outcome ? "Excluding fixations done." : res.msg));
        },
        function (res) {
          var win = (ui.tab.ds.st.winEM || window);
          if (win.excFirstNFix_resp) win.excFirstNFix_resp();
          win.alert(res.msg || CONST.err.unexpected)
        },
        true
      );
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    getAppId: function () { return CONST.appId; },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    getCookieAppName: function () { return CONST.cookie.appName; },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    getData: function () { return data; },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    getEMWinTitle: function (expIdx, stimIdx, subIdx, trialIdx, stimSeqIdx) {
      var exp = data.exp[expIdx];
      var sub = exp.sub[subIdx];
      var trial = sub.trials[trialIdx];
      var stim = exp.stim[stimIdx];
      return "Eye movements: " + sub.name + ", " + trial.name + ", " + stim.name + " (" + (stimSeqIdx+1) + ")";
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    getNextStimSeq : function (expIdx, subIdx, trialIdx, stimSeqIdx, fnAfter) {
      // TODO:
      /*
      var e = data.exp[expIdx];
      var s = e.sub[subIdx];
      var t = s.trials[trialIdx];
      var ss = t.stimSeq[stimSeqIdx];
      
      var found = false;
      do {
        do {
          do {
            if (stimSeqIdx++ < ss.length-1);
          } while (!found && stimSeqIdx < ss.length-1);
        } while (!found && trialIdx < t.length-1);
      } while (!found && subIdx < e.sub.length-1);
      */
      
      stimSeqIdx++;
      var SS = data.exp[expIdx].sub[subIdx].trials[trialIdx].stimSeq;
      if (stimSeqIdx == SS.length) return fnAfter(null, "There is no next stimulus sequence entry. Gaza data window will now be close.");
      var ss = SS[stimSeqIdx];
      
      var S = data.exp[expIdx].stim;
      var s = null;
      var stimIdx = 0;
      for (; stimIdx < S.length; stimIdx++) {
        if (S[stimIdx].id === ss.stimId) break;
      }
      s = S[stimIdx];
      
      // ROIs:
      var fnROIs = function () {
        stimGetROIs(s, function () { fnAfter({ exp: expIdx, sub: subIdx, trial: trialIdx, stimSeq: stimSeqIdx, stim: stimIdx }); }, null);
      }
      
      // Get EM data:
      stimSeqRetrieveEMData(ss, fnROIs);
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    getStimSeqBlinks: function (expIdx, subIdx, trialIdx, stimSeqIdx) {
      return data.exp[expIdx].sub[subIdx].trials[trialIdx].stimSeq[stimSeqIdx].blinks;
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    /**
     * Returns the most recent calibration object for the specified stimuli sequence entry. Returns `null` if no 
     * calibration objects exist for that trial.
     */
    getStimSeqCal: function (expIdx, subIdx, trialIdx, stimSeqIdx) {
      var ss = data.exp[expIdx].sub[subIdx].trials[trialIdx].stimSeq[stimSeqIdx];
      var C = data.exp[expIdx].sub[subIdx].trials[trialIdx].cal;
      
      if (C.length === 0) return null;
      
      for (var i = C.length - 1; i >= 0; i--) {
        var c = C[i];
        if (c.t < ss.t0) return c;
      }
      
      return null;
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    getStimSeqFix: function (expIdx, subIdx, trialIdx, stimSeqIdx) {
      return data.exp[expIdx].sub[subIdx].trials[trialIdx].stimSeq[stimSeqIdx].fix;
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    getStimSeqStimInfo: function (expIdx, stimIdx) {
      var s = data.exp[expIdx].stim[stimIdx];
      return {
        imgURI : "../../" + CONST.serverURI_S + "?" + s.img.uri,
        res    : { h: s.resH, v: s.resV }
      };
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    getStimROIs: function (expIdx, stimIdx) {
      var R = data.exp[expIdx].stim[stimIdx].rois;
      return (R || []);
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    getStimTxt: function (expIdx, stimIdx) {
      return data.exp[expIdx].stim[stimIdx].txt;
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    id: {
      getExp   : function (expIdx)                   { return data.exp[expIdx].id; },
      getSub   : function (expIdx, subIdx)           { return data.exp[expIdx].sub[subIdx].id; },
      getStim  : function (expIdx, stimIdx)          { return data.exp[expIdx].stim[stimIdx].id; },
      getTrial : function (expIdx, subIdx, trialIdx) { return data.exp[expIdx].sub[subIdx].trials[trialIdx].id; }
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    getSettingsExp: function () { return getCurrExp().settings; },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    /*
     * Returns the list of user-defined ROI variables.
     */
    getUserROIVars: function (expIdx) {
      return $filter(function (v) { return !v.sys; }, data.exp[expIdx].vars.roi);
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    obj: {
      getExp   : function (expIdx)                   { return data.exp[expIdx]; },
      getSub   : function (expIdx, subIdx)           { return data.exp[expIdx].sub[subIdx]; },
      getStim  : function (expIdx, stimIdx)          { return data.exp[expIdx].stim[stimIdx]; },
      getTrial : function (expIdx, subIdx, trialIdx) { return data.exp[expIdx].sub[subIdx].trials[trialIdx]; }
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    start: function (prjId) {
      state.prjId = prjId;
      Pegasus.Main.appReg(CONST.appId, window, Pegasus.Gaze);
      
      initProc();
      procShow("Loading (initializing UI)...");
      
      initCalPanel();
      initExpPanel();
      
      initMain();
      initNotif();
      initImp();
      
      initVar();
      initSub();
      initTrials();
      initStimSeq();
      initEvt();
      initStim();
      
      initConstrEvt();
      initConstr();
      initAggr();
      initScript();
      
      initHist();
      initDS();
      
      initTxtPanel();
      initTxtInPanel();
      
      initApp();
      
      YAHOO.util.Event.on(window, "resize", afterWinResize);
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    stimSetZoom: function (val) {
      var e = getCurrExp();
      attrSet("exp", "stim_zoom", val, e.id, function (res) { e.settings.stim.zoom = val; });
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    stimUpd: function (set, roiCnt, roisEnc, expIdx, stimIdx) {
      if (!set) return ui.panel.proc.p.hide();
      
      procShow("Saving ROIs...");
      var s = data.exp[expIdx].stim[stimIdx];
      
      execCmd(false,
        { cmd:"stim-upd", id:s.id, "font-size":set.fontSize, "letter-spac":set.letterSpac, "line-h":set.lineH, "align-h":set.alignH, "align-v":set.alignV, "margin-t":set.marginT, "margin-l":set.marginL, "margin-r":set.marginR, "padding-t":set.paddingT, "padding-b":set.paddingB, "roi-h":set.roiH, "roi-exp":set.roiExp, "offset-x":set.offsetX, "offset-y":set.offsetY, rois:roisEnc },
        function (res) {
          $setObj(s,set);
          s.roiCnt = roiCnt;
          
          s.imgROI.uri = (roiCnt > 0 ? getCmd({ cmd: "img-get", obj: "stim", "obj-id": s.id, attr: "img_roi" }) : null);
          
          stimGetTblRow(s, s.idx, s.curr);
          
          ui.tab.ds.stim.btnFileROIs.set("disabled", s.roiCnt === 0);
          
          data.exp[expIdx].linked = false;
          expUpdatePanel(data.exp[expIdx]);
          $show(ui.notif.lnk);
          
          ui.panel.proc.p.hide();
        },
        null, null, true
      );
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    stop: function (msg) {
      if (msg) {
        $show(ui.app.mask);
        alert(msg);
      }
      window.close();
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    xformEMData: function (tx, expIdx, subIdx, trialIdx, stimSeqIdx, fnAfter) {
      if (!tx) return ui.panel.proc.p.hide();
      
      procShow("Applying transformations...");
      var ss = data.exp[expIdx].sub[subIdx].trials[trialIdx].stimSeq[stimSeqIdx];
      
      execCmd(true,
        { cmd:"stim-seq-xform", "stim-seq-id":ss.id, "tx": tx.tr.x, "ty": tx.tr.y, "sx": tx.scale.x, "sy": tx.scale.y, a: tx.rot.a },
        function (res) {
          ss.fix      = res.fix;
          ss.blinks   = res.blinks;
          ss.xform    = res.xform;
          ss.xformCnt = res.xform.length;
          
          var set = { idx: stimSeqIdx, curr: true };
          $setObj(set, ss);
          
          ui.tbl.stimSeq.tbl.updateRow(stimSeqIdx, set);
          
          ui.tab.ds.st.btnStimSeqXForm.set("disabled", false);
          
          data.exp[expIdx].linked = false;
          expUpdatePanel(data.exp[expIdx]);
          $show(ui.notif.lnk);
          
          if (fnAfter) fnAfter();
          else ui.panel.proc.p.hide();
        },
        null, null, true
      );
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    winOpenFix: function () {
      procShow("\"Eye movements\" window is open...");
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    winOpenROIs: function () {
      procShow("\"ROIs\" window is open...");
    }
    
  };
  
  
  // ===================================================================================================================
  // ==[ CONSTRUCT ]====================================================================================================
  // ===================================================================================================================
  
  construct();
  return pub;
}();


/*
TODO:
- make use of getCurrExp() and the other methods everywhere
- YUI menu button values: http://blog.davglass.com/files/yui/button9/
*/
