/**
 * FixTransformer
 * --------------
 * 
 * Whenever a point unit is used a DTP or PostScript point is assumed.
 * 
 * IN:
 * 
 * DEPENDS ON:
 *   $.js
 */


var FixTransformer = function (_res, _parentBlink, _parentBlinkH, _parentFix, _parentFixH, _parentFixAdj, _parentFixHAdj, _parentSaccS, _parentSaccR, _parentSaccU, _parentSaccSAdj, _parentSaccRAdj, _parentSaccUAdj, _parentWire, _parentCal, _inf, _fixLst, _blinkLst, _fix, _blink) {
  
  // ===================================================================================================================
  // ==[ PRIVATE ]======================================================================================================
  // ===================================================================================================================
  
  var CONST = {
    blink       : { a: 7, color: "#000000", colorH: "#FFDD00" },  // 'a' is the side of the box
    cal         : { radCal: 6, radVal: 5, colorCal: "#ffffff", colorVal: "#000000", colorErr: "#000000", widthErr: 3 },
    fix         : { rad: 2, radH: 4, color: "#0000ff", colorH: "#0000ff" },
    fixAdj      : { rad: 2, radH: 6, color: "#ff0000", colorH: "#ff0000" },
    sacc        : { colorR: "#C545DD", colorS: "#00ff00", colorU: "#cccccc" },
    tx          : {
      scale : { min: 0.5, max: 1.5 },
      tr    : { min: -500, max: 500 },
      rot   : { min: -15, max: 15 }
    },
    wire        : { color: "#000000" }
  };
  
  var B = null;   // blinks
  var F = null;   // fixations
  
  var B2  = null;  // blinks (transformed coords only)
  var F2  = null;  // fixations (transformed coords only)
  var FA2 = null;  // fixations (adjusted; transformed coords only)
  
  var cal = null;  // calibration
  
  var scr = {           // SCREEN:
    res : { h:0, v:0 }  // resolution [px]
  };
  
  var state = {
    idxH     : { blink: -1, fix: -1 },
    timer    : null,
    updated  : { rot: true, scale: true, tr: true },
    zoom     : 1.00
  };
  
  var tx = {  // TRANSFORMATIONS
    rot   : { a: 0.0 },
    scale : { x: 1.00, y: 1.00 },
    tr    : { x:0, y:0 }
  };
  
  var ui = {
    canvases : [ "blink", "blinkH", "cal", "fix", "fixH", "fixAdj", "fixHAdj", "saccR", "saccS", "saccU", "saccRAdj", "saccSAdj", "saccUAdj", "wire" ],  // names of the hash keys below; for iteration purposes
    
    blink    : { parent: null, canvas: null, img: null, imgH: null, lst: null },
    blinkH   : { parent: null, canvas: null },
    cal      : { parent: null, canvas: null },
    fix      : { parent: null, canvas: null, img: null, imgFirst: null, imgH: null, lst: null },
    fixH     : { parent: null, canvas: null },
    fixAdj   : { parent: null, canvas: null, img: null, imgFirst: null, imgH: null, lst: null },
    fixHAdj  : { parent: null, canvas: null },
    inf      : null,
    saccR    : { parent: null, canvas: null },
    saccS    : { parent: null, canvas: null },
    saccU    : { parent: null, canvas: null },
    saccRAdj : { parent: null, canvas: null },
    saccSAdj : { parent: null, canvas: null },
    saccUAdj : { parent: null, canvas: null },
    wire     : { parent: null, canvas: null }
  };
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function construct() {
    scr.res.h = _res.h;
    scr.res.v = _res.v;
    
    if (_inf) ui.inf = _inf;
    if (_fixLst) ui.fix.lst = _fixLst;
    
    // (1) Parents:
    ui.blink.parent    = _parentBlink;
    ui.blinkH.parent   = _parentBlinkH;
    ui.cal.parent      = _parentCal;
    ui.fix.parent      = _parentFix;
    ui.fixH.parent     = _parentFixH;
    ui.fixAdj.parent   = _parentFixAdj;
    ui.fixHAdj.parent  = _parentFixHAdj;
    ui.saccS.parent    = _parentSaccS;
    ui.saccR.parent    = _parentSaccR;
    ui.saccU.parent    = _parentSaccU;
    ui.saccSAdj.parent = _parentSaccSAdj;
    ui.saccRAdj.parent = _parentSaccRAdj;
    ui.saccUAdj.parent = _parentSaccUAdj;
    ui.wire.parent     = _parentWire;
    
    // (2) Canvases:
    var fnInit01 = function (o) {
      ui[o].canvas = Canvas(null, null, ui[o].parent);
      ui[o].canvas.setDim(scr.res.h, scr.res.v);
    };
    
    fnInit01("blink");
    fnInit01("blinkH");
    fnInit01("cal");
    fnInit01("fix");
    fnInit01("fixH");
    fnInit01("fixAdj");
    fnInit01("fixHAdj");
    fnInit01("wire");
    
    var fnInit02 = function (o, col) {  // saccade canvas
      ui[o].canvas = Canvas(null, null, ui[o].parent);
      ui[o].canvas.setDim(scr.res.h, scr.res.v);
      ui[o].canvas.getCtx().lineWidth = 1;
      ui[o].canvas.getCtx().strokeStyle = col;
    };
    
    fnInit02("saccS", CONST.sacc.colorS);
    fnInit02("saccR", CONST.sacc.colorR);
    fnInit02("saccU", CONST.sacc.colorU);
    
    fnInit02("saccSAdj", CONST.sacc.colorS);
    fnInit02("saccRAdj", CONST.sacc.colorR);
    fnInit02("saccUAdj", CONST.sacc.colorU);
    
    // (3) Other:
    F = _fix;
    B = _blink;
    
    F2  = [];
    FA2 = [];
    B2  = [];
    for (var i=0, ni=F.length; i < ni; i++) { F2 [i] = { x: F[i].p.x, y: F[i].p.y }; }
    for (var i=0, ni=F.length; i < ni; i++) { FA2[i] = { x: F[i].pAdj.x, y: F[i].pAdj.y }; }
    for (var i=0, ni=B.length; i < ni; i++) { B2 [i] = { p1: { x: B[i].p1.x, y: B[i].p1.y }, p2: { x: B[i].p2.x, y: B[i].p2.y } }; }
    
    initCanvases();
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function initCanvases() {
    // Blinks:
    var a = CONST.blink.a + 1;
    
    ui.blink.img = new Canvas();
    ui.blink.img.setDim(a+1, a+1);
    ui.blink.img.drawLine({ x:1, y:1 }, { x:a, y:a }, 1, CONST.blink.color);
    ui.blink.img.drawLine({ x:a, y:1 }, { x:1, y:a }, 1, CONST.blink.color);
    
    ui.blink.imgH = new Canvas();
    ui.blink.imgH.setDim(a+2,a+2);
    ui.blink.imgH.drawRect({ x:2, y:2 }, a-1, a-1, 0, 2, "#000000", CONST.blink.colorH);
    
    // Fixations:
    var fnFix = function (o) {
      var r = CONST[o].rad + 1;
      
      ui[o].img = new Canvas();
      ui[o].img.setDim(r*2, r*2);
      ui[o].img.drawCircle({ x:r, y:r }, r-1, 1, CONST[o].color);
      
      ui[o].imgFirst = new Canvas();
      ui[o].imgFirst.setDim(r*2, r*2);
      ui[o].imgFirst.drawCircle({ x:r, y:r }, r-1, 1, "#000000", "#000000");
      
      //r += 2;
      r = CONST[o].radH;
      ui[o].imgH = new Canvas();
      ui[o].imgH.setDim(r*2+2, r*2+2);
      ui[o].imgH.drawCircle({ x:r+1, y:r+1 }, r-1, 2, "#000000", CONST[o].colorH);
    };
    
    fnFix("fix");
    fnFix("fixAdj");
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  /**
   * If `force` is true, plotting will be performed whether or not there has been any update.
   */
  function plot(force) {
    if (!force && !state.updated.tr && !state.updated.scale && !state.updated.rot) return;
    
    plotBlinks();
    plotFix("", F2);
    plotFix("Adj", FA2);
    plotWireframe();
    
    state.updated.tr    = false;
    state.updated.scale = false;
    state.updated.rot   = false;
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function plotBlinks() {
    var a = CONST.fix.rad;
    var img = ui.blink.img.get();
    
    ui.blink.canvas.clear();
    var c = ui.blink.canvas.getCtx();
    
    for (var i=0, ni=B2.length; i < ni; i++) {
      var p1 = B2[i].p1;
      c.drawImage(img, p1.x-a-1, p1.y-a-1);
    }
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  /**
   * Because calibration image is updated only when zoom is changed, this method should be called only after the  
   * calibration object is updated and when zoom changes.
   */
  function plotCal() {
    if (!cal) return;
    
    var img = ui.cal.canvas;
    img.clear();
    var P = cal.points;
    for (var i=0, ni=P.length; i < ni; i++) {
      var p = P[i];
      var pC = { x: p.x, y: p.y };
      var pV = { x: p.x + p.xErr, y: p.y + p.yErr };
      
      img.drawLine(pC, pV, CONST.cal.widthErr, CONST.cal.colorErr);
      img.drawCircle(pC, CONST.cal.radCal, 2, "#000000", CONST.cal.colorCal);
      img.drawCircle(pV, CONST.cal.radVal, 1, "#000000", CONST.cal.colorVal);
    }
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function plotFix(suff, arrFix) {
    var r = CONST["fix" + suff].rad;
    var img = ui["fix" + suff].img.get();
    
    ui["fix" + suff].canvas.clear();
    ui["saccS" + suff].canvas.clear();
    ui["saccR" + suff].canvas.clear();
    ui["saccU" + suff].canvas.clear();
    
    var cf  = ui["fix" + suff].canvas.getCtx();
    var css = ui["saccS" + suff].canvas.getCtx();
    var csr = ui["saccR" + suff].canvas.getCtx();
    var csu = ui["saccU" + suff].canvas.getCtx();
    
    var p0 = arrFix[0];
    
    css.beginPath();
    csr.beginPath();
    csu.beginPath();
    
    css.moveTo(p0.x, p0.y);
    csr.moveTo(p0.x, p0.y);
    csu.moveTo(p0.x, p0.y);
    
    var avgA = null;
    
    for (var i=1, ni=arrFix.length; i < ni; i++) {
      var p = arrFix[i];
      var s = F[i].saccInROI;
      
      if (!s) {  // unknown saccade
        csu.lineTo(p.x, p.y);
        css.moveTo(p.x, p.y);
        csr.moveTo(p.x, p.y);
      }
      else if (s > 0) {  // right saccade
        css.lineTo(p.x, p.y);
        csr.moveTo(p.x, p.y);
        csu.moveTo(p.x, p.y);
      }
      else {  // left saccade
        csr.lineTo(p.x, p.y);
        css.moveTo(p.x, p.y);
        csu.moveTo(p.x, p.y);
      }
      
      cf.drawImage(img, p.x-r-2, p.y-r-2);  // fixation
    }
    
    css.stroke();
    csr.stroke();
    csu.stroke();
    
    cf.drawImage(ui.fix.imgFirst.get(), p0.x-r-2, p0.y-r-2);  // the first fixation
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function plotWireframe() {
    var c = ui.wire.canvas;
    c.clear();
    
    c.drawRect({ x:100, y:100 }, scr.res.h-200, scr.res.v-200, 0, 1, CONST.wire.color, undefined);
    c.drawRect({ x:150, y:150 }, scr.res.h-300, scr.res.v-300, 0, 1, CONST.wire.color, undefined);
    c.drawRect({ x:200, y:200 }, scr.res.h-400, scr.res.v-400, 0, 1, CONST.wire.color, undefined);
    
    c.drawLine({ x:scr.res.h/2, y:100 }, { x:scr.res.h/2, y:scr.res.v-100 }, 1, CONST.wire.color);
    c.drawLine({ x:100, y:scr.res.v/2 }, { x:scr.res.h-100, y:scr.res.v/2 }, 1, CONST.wire.color);
    
    c.drawCircle({ x:scr.res.h/2, y:scr.res.v/2 }, 25, 1, CONST.wire.color, undefined);
    c.drawCircle({ x:scr.res.h/2, y:scr.res.v/2 }, 50, 1, CONST.wire.color, undefined);
    c.drawCircle({ x:scr.res.h/2, y:scr.res.v/2 }, 75, 1, CONST.wire.color, undefined);
    c.drawCircle({ x:scr.res.h/2, y:scr.res.v/2 }, 100, 1, CONST.wire.color, undefined);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function step() {
    if (state.idxH.fix === F.length-1) return pub.stop();
    pub.setFix(++(state.idxH.fix));
    state.timer = window.setTimeout(step, F[state.idxH.fix].dur);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  /**
   * If `force` is true, plotting will be performed whether or not there has been any update.
   */
  function transform(force) {
    if (!force && !state.updated.tr && !state.updated.scale && !state.updated.rot) return;
    
    // Translation and scale:
    for (var i=0, ni=F.length; i < ni; i++) {
      var f = F[i];
      F2 [i] = { x: (f.p.x + tx.tr.x) * tx.scale.x, y: (f.p.y + tx.tr.y) * tx.scale.y };
      FA2[i] = { x: (f.pAdj.x + tx.tr.x) * tx.scale.x, y: (f.pAdj.y + tx.tr.y) * tx.scale.y };
    }
    for (var i=0, ni=B.length; i < ni; i++) {
      var b = B[i];
      B2[i] = {
        p1: { x: (b.p1.x + tx.tr.x) * tx.scale.x, y: (b.p1.y + tx.tr.y) * tx.scale.y },
        p2: { x: (b.p2.x + tx.tr.x) * tx.scale.x, y: (b.p2.y + tx.tr.y) * tx.scale.y }
      };
    }
    
    // Rotation:
    var a = (Math.PI * tx.rot.a) / 180;
    if (a !== 0) {
      var sin = Math.sin(a);
      var cos = Math.cos(a);
      
      for (var i=0, ni=F2.length; i < ni; i++) {
        var f = F2[i];
        var fa = FA2[i];
        F2 [i] = { x: cos*f.x - sin*f.y, y: sin*f.x + cos*f.y };
        FA2[i] = { x: cos*fa.x - sin*fa.y, y: sin*fa.x + cos*fa.y };
      }
      for (var i=0, ni=B2.length; i < ni; i++) {
        var b = B2[i];
        B2[i] = {
          p1: { x: cos * b.p1.x - sin * b.p1.y, y: sin * b.p1.x + cos * b.p1.y },
          p2: { x: cos * b.p2.x - sin * b.p2.y, y: sin * b.p2.x + cos * b.p2.y }
        };
      }
    }
    
    // Transform wireframe:
    var c = ui.wire.canvas.getCtx();
    c.restore();
    ui.wire.canvas.clear();
    c.save();
    c.rotate(a);
    c.scale(tx.scale.x, tx.scale.y);
    
    var p = ui.wire.parent;
    p.style.left = tx.tr.x + "px";
    p.style.top  = tx.tr.y + "px";
  }
  
  
  // ===================================================================================================================
  // ==[ PUBLIC ]=======================================================================================================
  // ===================================================================================================================
  
  var pub = {
    
    // -----------------------------------------------------------------------------------------------------------------
    /**
     * Called when done with the object to clean everything. Most 
     * notably, when the table HTML element that is used in the process 
     * is to be reused.
     */
    done: function() {},
    
    
    // -----------------------------------------------------------------------------------------------------------------
    getTx      : function () { return tx; },
    getTxScale : function () { return tx.scale; },
    getTxRot   : function () { return tx.rot; },
    getTxTr    : function () { return tx.tr; },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    isUpdated      : function () { return state.updated.rot || state.updated.scale || state.updated.tr; },
    isUpdatedScale : function () { return state.updated.scale; },
    isUpdatedRot   : function () { return state.updated.rot; },
    isUpdatedTr    : function () { return state.updated.tr; },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    incAngle: function (by) { if (tx.rot.a + by > CONST.tx.rot.max) return;  tx.rot.a = Math.round((tx.rot.a+by)*10)/10; state.updated.rot = true; },
    decAngle: function (by) { if (tx.rot.a - by < CONST.tx.rot.min) return;  tx.rot.a = Math.round((tx.rot.a-by)*10)/10; state.updated.rot = true; },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    incScale: function (axis, by) { if (tx.scale[axis] + by > CONST.tx.scale.max) return;  tx.scale[axis] = Math.round((tx.scale[axis]+by)*100)/100; state.updated.scale = true; },
    decScale: function (axis, by) { if (tx.scale[axis] - by < CONST.tx.scale.min) return;  tx.scale[axis] = Math.round((tx.scale[axis]-by)*100)/100; state.updated.scale = true; },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    incTr: function (axis, by) { if (tx.tr[axis] + by > CONST.tx.tr.max) return;  tx.tr[axis] += by; state.updated.tr = true; },
    decTr: function (axis, by) { if (tx.tr[axis] - by < CONST.tx.tr.min) return;  tx.tr[axis] -= by; state.updated.tr = true; },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    play: function () {
      if (state.timer) return;
      if (ui.fix.lst) ui.fix.lst.disabled = true;
      step();
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    setBlink: function (idx) {
      var p1 = B2[idx].p1;
      var c = ui.blinkH.canvas;
      
      c.clear();
      c.drawImg(ui.blink.imgH.get(), p1.x-CONST.blink.a+3, p1.y-CONST.blink.a+3);
      state.idxH.blink = idx;
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    setCal: function (c) {
      cal = c;
      plotCal();
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    setFix: function (idx) {
      var f    = F[idx];
      var p    = F2[idx];
      var pAdj = FA2[idx];
      
      ui.fixH.canvas.clear();
      ui.fixH.canvas.drawImg(ui.fix.imgH.get(), p.x-CONST.fix.radH-2, p.y-CONST.fix.radH-2);
      
      ui.fixHAdj.canvas.clear();
      ui.fixHAdj.canvas.drawImg(ui.fixAdj.imgH.get(), pAdj.x-CONST.fixAdj.radH-2, pAdj.y-CONST.fixAdj.radH-2);
      
      state.idxH.fix = idx;
      
      if (ui.inf && ui.inf.style.display !== "none") {
        ui.inf.innerHTML = 
          "Dur: " + f.dur + " ms; SaccLen: " +
          (f.saccInROI  ? " &nbsp;i-roi: "  + f.saccInROI  : "") +
          (f.saccOutROI ? " &nbsp;o-roi: " + f.saccOutROI : "") +
          (f.saccInLet  ? " &nbsp;i-let: "  + f.saccInLet  : "") +
          (f.saccOutLet ? " &nbsp;o-let: " + f.saccOutLet : "");
      }
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    stop: function() {
      window.clearTimeout(state.timer);
      state.timer = null;
      if (ui.fix.lst) {
        ui.fix.lst.disabled = false;
        ui.fix.lst.selectedIndex = state.idxH.fix;
      }
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    update: function () {
      transform(false);
      plot(false);
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    // `z` should be a factor (e.g., 0.75), not a percentage (e.g., 75%).
    zoom: function (z) {
      state.zoom = z;
      
      $map(
        function (x) {
          ui[x].canvas.clear();
          ui[x].canvas.getCtx().restore();
          ui[x].canvas.getCtx().save();
          ui[x].canvas.setDim(scr.res.h * z, scr.res.v * z);
          ui[x].canvas.scale(z,z);
        },
        ui.canvases
      );
      
      transform(true);
      plot(true);
      plotCal();
    }
  };
  
  
  // ===================================================================================================================
  // ==[ CONSTRUCT ]====================================================================================================
  // ===================================================================================================================
  
  construct();
  return pub;
};
