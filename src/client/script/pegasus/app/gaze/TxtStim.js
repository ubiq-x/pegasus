/**
 * Text Stimulus
 * -------------
 * 
 * Whenever a point unit is used a DTP (or PostScript) point is assumed.
 * 
 * IN:
 *   txt
 *     e.g. "Text that has been presented as the stimulus"
 *
 *   res
 *     screen resolution, e.g. { h:800, v:600 }
 *
 *   fontSize
 *     in points (PostScript or DTP), e.g. 20
 *
 * EXAMPLE:
 *   Big fat cat chased a mouse. The cat sucked big time. The mouse was laughing her ass off. The cat died within an 
 *   hour from exhaustion.
 * 
 * DEPENDS ON:
 *   $.js
 */


var TxtStim = function (_txt, _res, _fontFamily, _fontSize, _letterSpac, _lineH, _roiH, _roiExp, _align, _margin, _padding, _offset, _tbl, _rois, _userROIVars) {
  
  // ===================================================================================================================
  // ==[ PRIVATE ]======================================================================================================
  // ===================================================================================================================
  
  var CONST = {
    abbrev     : ["Mr.","Mrs.","Dr."],
    align      : {  // possible values for stim.align and the corresponding CSS values
      h: { l: "left", c: "center", r: "right" },
      v: { t: "top", c: "middle", b: "bottom" }
    },
    char       : { nonword: ".,;-=+~`!@#$%^&*()<>?/\\" },
    clickMode  : { normal:0, custom:1, disable:2, sentEnd:3, sentStart:4, clauseEnd:5, clauseStart:6 },
    color      : {
      bg     : { disabled: "#ffffff", normal: "transparent", sentEnd: "#FFC6C6", sentStart: "#C8FFC6", clauseEnd: "#BFEDF9", clauseStart: "#FBDDB3" },
      border : { disabled: "#bbbbbb", normal: "#5E9AFA",     sentEnd: "#EE0505", sentStart: "#32A91A", clauseEnd: "#568B99", clauseStart: "#C6A06E" }
    },
    ftSize     : { min:     2, max:  100 },
    letterSpac : { min:    -5, max:    5 },
    lineH      : { min:     1, max: 1000 },
    margin     : { min: -1000, max: 1000 },
    offset     : { min: -1000, max: 1000 },
    padding    : { min: -1000, max: 1000 },
    roiH       : { min:    10, max:  300 },
    tags       : { firstInLine: "ln-s", lastInLine: "ln-e", firstOnPage: "pg-s", lastOnPage: "pg-e", sentEnd: "sent-e", sentStart: "sent-s", clauseEnd: "cl-e", clauseStart: "cl-s" }
  };
  
  var scr = {               // SCREEN:
    res    : { h:0, v:0 },  // resolution [px]
    dim    : { w:0, h:0 },  // dimension in inches
    pxSize : 0              // pixel size in inches
  };
  
  var state = {
    clickMode   : CONST.clickMode.none,
    updated     : true,                  // flag: has it been updated?
    firstUpdate : true,
    zoom        : 1.00
  };
  
  var stim = {                        // STIMULUS:
    align      : { h: "c", v: "c" },  // 
    expROIs    : true,
    fontFamily : "Courier New",
    fontSize   : 12,                  // [pt]
    letterSpac : 0.0,                 // [px]
    lineH      : 100,                 // [%]
    lines      : [],                  // for line-by-line processing
    margin     : { t:0, l:0, r:0 },   // margins around the stimulus [px]
    offset     : { x:0, y:0 },        // a systematic error correction [px]
    roiH       : 200,                 // height of ROIs in a single-line stimulus mode [px]
    roiExp     : true,                // flag: should first and last lines' ROIs be expanded
    rois       : [],                  // ROIs and their coords
    singleLine : true,                // flag: is the stimulus a single line?
    txt        : "",                  // textual content
    padding    : { t:0, b:0 },        // vertical padding for each ROI [px]
    parLnIdx01 : [],                  // indices of lines that start a paragraph
    parLnIdx02 : [],                  // indices of lines that end a paragraph
    words      : []                   // words along with their dimensions
  };
  
  var ui = {
    cell   : null,
    rois   : null,
    tbl    : null,
    varLst : { lst: null, parent: null, title: null }
  };
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function createWordSpan(html) {
    var span = document.createElement("span");
    var ss = span.style;
    ss.fontFamily = stim.fontFamily;
    ss.border = "1px solid transparent";
    ss.marginLeft = "-1px";
    ss.marginRight = "-1px";
    ss.fontWeight = "normal";
    span.innerHTML = " <nobr>" + html + "</nobr>";
    
    return span;
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function addWord(ln, w, lnIdx) {
    if ($trim(w).length === 0) return;
    
    // (1) Add leading spaces:
    if (ln.words.length === 0 && ln.leadSpaceCnt > 0) {
      var html = "";
      for (var i=0, ni=ln.leadSpaceCnt; i < ni; i++) html += "&nbsp;";
      var span = createWordSpan(html);
      ui.cell.appendChild(span);
    }
    
    // (2) Add the word:
    var span = createWordSpan(w);
    ui.cell.appendChild(span);
    
    if ($trim(w) === "<br>") return;
    
    // (3) Create the ROI object:
    var r = {
      idx         : stim.rois.length,
      enabled     : true,
      sentStart   : false,
      sentEnd     : false,
      clauseStart : false,
      clauseEnd   : false,
      style       : "normal",  // style indicator, e.g., "disabled", "sentEnd", etc.
      div         : null
    };
    stim.rois.push(r);
    
    // (4) Create the word object:
    var w = {
      dim    : { w:0, h:0 },
      pos    : { x:0, y:0 },
      roi    : r,
      str    : w,
      spaces : (w.length - $trim(w).length),  // amount of leading spaces
      span   : span,                          // HTML element holding the word
      tags   : []                             // e.g., the first word in the line
    };
    stim.words.push(w);
    ln.words.push(w);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  /*
   * Slices the stimulus 's' into lines and words and adds initializes the 'stim' object.
   */
  function addWords(s) {
    var L = s.split("\n");
    var lnCnt = -1;
    $map(
      function (l) {
        lnCnt++;
        var ln = { leadSpaceCnt: -1, words: [] };
        var idx01=0, idx02=0;
        while (idx02 < l.length) {
          idx01 = idx02;
          while (l[idx02] === ' ' && idx02 < l.length) idx02++;  // include leading spaces
          if (ln.leadSpaceCnt === -1) {
            if (idx02 === 0) ln.leadSpaceCnt = 0;
            else {
              ln.leadSpaceCnt = idx02;
              idx01 = idx02;
            }
          }
          while (l[idx02] !== ' ' && idx02 < l.length) idx02++;  // include the word and any trailing punctuation
          var word = l.substring(idx01, idx02);
          addWord(ln, word, lnCnt);
        }
        addWord(ln, "<br>", lnCnt);
        
        if (l.length === 0 && lnCnt < L.length-1) {
          stim.parLnIdx01.push(lnCnt+1 - stim.parLnIdx01.length-1);
          stim.parLnIdx02.push(lnCnt-1 - stim.parLnIdx02.length);
        }
      },
      L
    );
    stim.parLnIdx01.push(0);
    stim.parLnIdx02.push(lnCnt - stim.parLnIdx01.length + 1);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function construct() {
    // (1) Screen:
    scr.res.h = _res.h;
    scr.res.v = _res.v;
    
    // (2) Parent:
    ui.rois = _rois;
    if (_tbl) {
      ui.tbl = _tbl;
      ui.cell = _tbl.rows[0].cells[0];
      ui.tbl.style.width = (scr.res.h - 500) + "px";
      ui.tbl.style.height = scr.res.v + "px";
    }
    else {
      // TODO: create the table as a child of the _parent element
    }
    
    // (3) User-defined variables list:
    ui.varLst.parent = $$("div", document.body, null, "var-lst");
    ui.varLst.title  = $$("div", ui.varLst.parent, null, "title");
    ui.varLst.cont   = $$("div", ui.varLst.parent, null, "lst");
    for (var i=0; i < _userROIVars.length; i++) {
      var v = _userROIVars[i];
      var divItem = $$("div", ui.varLst.cont, null, "item", v.name);
    }
    
    var divBtn = $$("div", ui.varLst.parent, null, "btns");
    var btn = $$("input", divBtn, null, "act");
    btn.setAttribute("type", "button");
    btn.value = "Close";
    btn.onclick = function (e) { $hide(ui.varLst.parent); };
    
    // (4) Stimulus:
    stim.fontFamily = _fontFamily;
    stim.fontSize = _fontSize;
    stim.letterSpac = _letterSpac;
    stim.lineH = _lineH;
    stim.roiH = _roiH;
    stim.roiExp = _roiExp;
    stim.offset = { x: _offset.x, y: _offset.y };
    stim.align = _align;
    stim.margin = { t: _margin.t, l: _margin.l, r: _margin.r };
    stim.padding = { t: _padding.t, b: _padding.b };
    stim.txt = _txt;
    
    addWords(stim.txt);
    suggestTags();
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  /*
   * Returns coordinates assuming (0,0) is the upper left corner of the screen.
   */
  function getPos(o) {
    var pos = { x:-1, y:-1 };
    if (o.offsetParent) {
      do {
        pos.x += o.offsetLeft;
        pos.y += o.offsetTop;
      } while (o = o.offsetParent);
    }
    return pos;
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function showMsg(msg) {
    alert(msg);
    return false;
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function isWordAbbrev(w) {
    w = $trim(w);
    for (var i=0, ni=CONST.abbrev.length; i < ni; i++) {
      if (CONST.abbrev[i] === w) return true;
    }
    return false;
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function isWordCapitalized(w) {
    w = $trim(w);
    var c = w.charCodeAt(0);
    if (c === 34 || c === 39 || c === 96) {  // starts with: ' " `
      c = (w.length > 1 ? w.charCodeAt(1) : null);
    }
    
    return (c ? (c >= 65 && c <= 90) : false);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function isWordNonword(w) {
    w = $trim(w);
    for (var i=0, ni=w.length; i < ni; i++) {
      if (CONST.char.nonword.indexOf(w[i]) === -1) return false;
    }
    return true;
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function isWordSentEnd(w) {
    w = $trim(w);
    var c = w.charCodeAt(w.length-1);
    if (c === 34 || c === 39 || c === 96) {  // ends with: ' " `
      c = (w.length > 2 ? w.charCodeAt(w.length-2) : null);
    }
    
    return (c ? (c === 46 || c === 33 || c === 63) : false);  // . ! ?
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function isWordClauseEnd(w) {
    w = $trim(w);
    var c = w.charCodeAt(w.length-1);
    if (c === 34 || c === 39 || c === 96) {  // ends with: ' " `
      c = (w.length > 2 ? w.charCodeAt(w.length-2) : null);
    }
    
    return (c ? (c === 44 || c === 59 || c === 45) : false);  // , ; -
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function roiSetTags(roi, T,F) {
    $map(function (x) { roi[x] = true; }, T);
    $map(function (x) { roi[x] = false; }, F);
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function suggestTags() {
    var pw = null;  // previous word
    var i = 0, ni = stim.words.length;
    
    // (1) Find and process the first word (i.e., skip non-words):
    while (pw === null && i < ni) {
      if (!isWordNonword($trim(stim.words[i].str))) {
        pw = stim.words[0];
      }
      i++;
    }
    
    if (isWordCapitalized(pw.str)) {
      pw.roi.sentStart = true;
      pw.roi.style     = "sentStart";
    }
    
    // (2) Process all remaining words:
    for (; i < ni; i++) {
      var w = stim.words[i];
      var s = $trim(w.str);
      
      if (isWordAbbrev(s)) continue;
      
      // (2.1) Non-word:
      if (isWordNonword(s)) {
        roiSetTags(w.roi, [], ["sentStart","sentEnd","clauseStart","clauseEnd"]);
        w.roi.enabled = false;
        w.roi.style = "disabled";
        continue;
      }
      
      // (2.2) Sentence start/end:
      else if (pw.roi.sentEnd) {
        roiSetTags(w.roi, ["sentStart"], ["sentEnd","clauseStart","clauseEnd"]);
        w.roi.style = "sentStart";
      }
      else if (isWordSentEnd(s)) {
        roiSetTags(w.roi, ["sentEnd"], ["sentStart","clauseStart","clauseEnd"]);
        w.roi.style = "sentEnd";
        /*
        if (i < stim.words.length-2) {
          w = stim.words[++i];
          roiSetTags(w.roi, ["sentStart"], ["sentEnd","clauseStart","clauseEnd"]);
          w.roi.style = "sentStart";
        }
        */
      }
      
      // (2.3) Clause start/end:
      else if (pw.roi.clauseEnd && !w.roi.sentStart && !w.roi.sentEnd) {
        roiSetTags(w.roi, ["clauseStart"], ["sentStart","sentEnd","clauseEnd"]);
        w.roi.style = "clauseStart";
      }
      else if (isWordClauseEnd(s) && !w.roi.sentStart && !w.roi.sentEnd) {
        roiSetTags(w.roi, ["clauseEnd"], ["sentStart","sentEnd","clauseStart"]);
        w.roi.style = "clauseEnd";
        /*
        if (i < stim.words.length-2) {
          w = stim.words[++i];
          roiSetTags(w.roi, ["clauseStart"], ["sentStart","sentEnd","clauseEnd"]);
          w.roi.style = "clauseStart";
        }
        */
      }
      
      pw = w;
    }
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function updateROIs() {
    while (ui.rois.hasChildNodes()) ui.rois.removeChild(ui.rois.firstChild);
    
    var dy = (!stim.singleLine ? 0 : (stim.roiH - stim.words[0].dim.h) / 2);
    var pw = null;  // preceding word
    for (var i=0, ni=stim.words.length; i < ni; i++) {
      var w = stim.words[i];
      
      // (1) Reinit the ROI:
      r = w.roi;
      r.x1  = w.pos.x + stim.offset.x;
      r.y1  = w.pos.y + stim.offset.y - dy;
      r.x2  = w.pos.x + w.dim.w + stim.offset.x;
      r.y2  = w.pos.y + w.dim.h + stim.offset.y + dy;
      r.div = null;
      
      // (2) Fixes:
      if (pw) {
        var pr = pw.roi;
        
        // (2a) ROIs overlap -- contract the preceding one by 1:
        if (pr.x2 === r.x1) {
          pr.x2 -= 1;
          pr.div.style.width = (pr.x2 - pr.x1 - 1) + "px";
        }
        
        // (2b) ROIs don't touch -- expand the preceding one by 1:
        else if (r.x1 - pr.x2 > 1) {
          pr.x2 += 1;
          pr.div.style.width = (pr.x2 - pr.x1 - 1) + "px";
        }
      }
      
      // (3) Create a corresponsing DIV:
      var div = document.createElement("div");
      var ds = div.style;
      ds.position = "absolute";
      ds.left     = (r.x1) + "px";
      ds.top      = (r.y1) + "px";
      ds.width    = (r.x2 - r.x1) + "px";
      ds.height   = (r.y2 - r.y1) + "px";
      ds.opacity  = "0.75";
      ds.border   = "1px solid " + CONST.color.border[r.style];
      ds.backgroundColor = CONST.color.bg[r.style];
      
      div.onclick = function (w) {
        var x = w.roi;
        return function (e) {
          switch (state.clickMode) {
            case CONST.clickMode.normal:
              x.enabled     = true;
              x.sentStart   = false;
              x.sentEnd     = false;
              x.clauseStart = false;
              x.clauseEnd   = false;
              x.style       = "normal";
              break;
              
            case CONST.clickMode.disable:
              x.enabled = !x.enabled;
              if (x.enabled) {
                x.style       = "normal";
                x.sentStart   = false;
                x.sentEnd     = false;
                x.clauseStart = false;
                x.clauseEnd   = false;
              }
              else x.style = "disabled";
              break;
              
            case CONST.clickMode.sentEnd:
              if (!x.enabled) x.enabled = true;
              x.sentStart   = false;
              x.clauseStart = false;
              x.clauseEnd   = false;
              x.sentEnd     = !x.sentEnd;
              x.style       = (x.sentEnd ? "sentEnd" : "normal");
              
              if (x.idx < stim.rois.length-1) {
                var xx = stim.rois[x.idx+1];  // following ROI
                if (!xx.enabled) xx.enabled = true;
                xx.sentStart   = false;
                xx.sentEnd     = false;
                xx.clauseStart = false;
                xx.clauseEnd   = false;
                
                if (x.sentEnd) {
                  xx.sentStart   = true;
                  xx.clauseStart = false;
                  xx.clauseEnd   = false;
                  xx.style       = "sentStart";
                }
                else {
                  xx.sentStart   = false;
                  xx.clauseStart = false;
                  xx.clauseEnd   = false;
                  xx.style       = "normal";
                }
                xx.div.style.borderColor     = CONST.color.border[xx.style];
                xx.div.style.backgroundColor = CONST.color.bg[xx.style];
              }
              
              break;
            
            case CONST.clickMode.sentStart:
              if (!x.enabled) x.enabled = true;
              x.sentEnd     = false;
              x.clauseStart = false;
              x.clauseEnd   = false;
              x.sentStart   = !x.sentStart;
              x.style       = (x.sentStart ? "sentStart" : "normal");
              
              if (x.idx > 0) {
                var xx = stim.rois[x.idx-1];  // preceding ROI
                if (!xx.enabled) xx.enabled = true;
                xx.sentStart   = false;
                xx.sentEnd     = false;
                xx.clauseStart = false;
                xx.clauseEnd   = false;
                
                if (x.sentStart) {
                  xx.sentEnd     = true;
                  xx.clauseStart = false;
                  xx.clauseEnd   = false;
                  xx.style       = "sentEnd";
                }
                else {
                  xx.sentEnd     = false;
                  xx.clauseStart = false;
                  xx.clauseEnd   = false;
                  xx.style       = "normal";
                }
                xx.div.style.borderColor     = CONST.color.border[xx.style];
                xx.div.style.backgroundColor = CONST.color.bg[xx.style];
              }
              
              break;
              
            case CONST.clickMode.clauseEnd:
              break;
              
            case CONST.clickMode.clauseStart:
              break;
              
            case CONST.clickMode.custom:
              var mpos = Mouse.getPos(e);
              ui.varLst.title.innerHTML = w.str;
              ui.varLst.parent.style.left = mpos.x + "px";
              ui.varLst.parent.style.top = mpos.y + "px";
              $show(ui.varLst.parent);
              break;
          }
          x.div.style.borderColor     = CONST.color.border[x.style];
          x.div.style.backgroundColor = CONST.color.bg[x.style];
        }
      }(w);
      
      ui.rois.appendChild(div);
      r.div = div;
      
      // (4) Finalize:
      pw = w;
    }
    
    state.firstUpdate = false;
  }
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function updateStimulus() {
    var z = state.zoom;
    
    // (1) Init:
    var ts = ui.tbl.style;
    ts.marginTop     = stim.margin.t * z + "px";
    ts.marginLeft    = stim.margin.l * z + "px";
    ts.width         = (scr.res.h - stim.margin.l - stim.margin.r) * z + "px";
    ts.height        = (scr.res.v - stim.margin.t) * z + "px";
    ts.marginRight   = stim.margin.r * z + "px";
    
    var cs = ui.cell.style;
    cs.fontSize      = stim.fontSize * z + "pt";
    cs.textAlign     = CONST.align.h[stim.align.h];
    cs.verticalAlign = CONST.align.v[stim.align.v];
    cs.lineHeight    = stim.lineH + "%";
    cs.letterSpacing = (stim.letterSpac * z) + "px";
    
    // (2) Calculate word dimensions:
    var lines = [];    // groups ROIs into lines for line-by-line processing
    lines.push([]);
    
    var first = true;  // flag: is it the first word in the line?
    var pw = null;     // preceding word
    var lnCnt = 1;     // number of lines
    $map(
      function (w) {
        w.tags = [];
        
        w.span.style.paddingTop    = stim.padding.t * z + "px";
        w.span.style.paddingBottom = stim.padding.b * z + "px";
        
        w.str = w.str.replace(/ |&nbsp;/g, "");
        for (var i=0; i < w.spaces; i++) w.str = (i % 2 ? "&nbsp;" : " ") + w.str;
        w.span.innerHTML = "<nobr>" + w.str + "</nobr>";
        
        var pos = getPos(w.span);
        var ppos = (!pw ? null : getPos(pw.span));
        if (pw && (pos.x < ppos.x || (pos.x === ppos.x && pos.y > ppos.y))) {
          w.span.innerHTML = w.str.replace(/ |&nbsp;/g, "");
          pos = getPos(w.span);
          lnCnt++;
          lines.push([]);
        }
        first = (!pw || pw.pos.y !== pos.y+2);
        
        w.pos = { x: pos.x + (first ? 2 : 3), y: pos.y + 2 };
        w.dim = { w: w.span.offsetWidth - 3, h: w.span.offsetHeight - 3 };
        
        if (!pw) {
          w.tags.push(CONST.tags.firstOnPage);
        }
        if (first) {
          w.tags.push(CONST.tags.firstInLine);
          if (pw) pw.tags.push(CONST.tags.lastInLine);
        }
        
        lines[lnCnt-1].push(w);
        
        pw = w;
      },
      stim.words
    );
    pw.tags.push(CONST.tags.lastInLine);
    pw.tags.push(CONST.tags.lastOnPage);
    
    var dy = Math.floor(lines[0][0].dim.h * (stim.lineH/100) / 2);  // preserve for (4) and (5) below
    
    // (3) Ensure ROIs touch vertically:
    var isOddLine = true;
    for (var i=0; i < lnCnt-1; i++) {
      var gap = lines[i+1][0].pos.y - (lines[i][0].pos.y + lines[i][0].dim.h) - 1;
      var adjAbove = (isOddLine ? Math.floor(gap/2) : Math.ceil(gap/2));
      var adjBelow = gap - adjAbove;
      
      $map(function (w) { w.dim.h += adjAbove; }, lines[i]);
      $map(function (w) { w.pos.y -= adjBelow; w.dim.h += adjBelow; }, lines[i+1]);
      
      /*
      if (!$arrContains(stim.parLnIdx01, i) && !$arrContains(stim.parLnIdx02, i)) {
        $map(function (w) { w.dim.h += adjAbove; }, lines[i]);
        $map(function (w) { w.pos.y -= adjBelow; w.dim.h += adjBelow; }, lines[i+1]);
      }
      else if ($arrContains(stim.parLnIdx01, i) && !$arrContains(stim.parLnIdx02, i)) {
        $map(function (w) { w.dim.h += adjAbove; }, lines[i]);
        $map(function (w) { w.pos.y -= adjBelow; w.dim.h += adjBelow; }, lines[i+1]);
      }
      else if ($arrContains(stim.parLnIdx02, i)) {}
      */
      
      isOddLine = !isOddLine;
    }
    
    // (4) Expand the first and the last of each paragraph:
    /*
    if (stim.roiExp) {
      for (var i=0, ni=stim.parLnIdx01.length; i < ni; i++) {
        $map(function (w) { w.pos.y -= dy; w.dim.h += dy; }, lines[stim.parLnIdx01[i]]);
      }
      
      for (var i=0, ni=stim.parLnIdx02.length; i < ni; i++) {
        $map(function (w) { w.dim.h += dy; }, lines[stim.parLnIdx02[i]]);
      }
    }
    */
    
    // (5) Expand the first and the last line:
    $map(function (w) { w.pos.y -= dy; w.dim.h += dy; }, lines[0]);
    $map(function (w) { w.dim.h += dy; }, lines[lnCnt-1]);
    
    // (5) Other:
    stim.singleLine = (stim.words[0].pos.y === stim.words[stim.words.length-1].pos.y);
  }
  
  
  // ===================================================================================================================
  // ==[ PUBLIC ]=======================================================================================================
  // ===================================================================================================================
  
  var pub = {
    
    // -----------------------------------------------------------------------------------------------------------------
    /**
     * Called when done with the object to clean everything. Most notably, when the table HTML element that is used in 
     * the process is to be reused.
     */
    done: function() {
      $removeChildren(ui.cell);
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    getAlign      : function () { return stim.align; },
    getFontSize   : function () { return stim.fontSize; },
    getLetterSpac : function () { return stim.letterSpac; },
    getLineH      : function () { return stim.lineH; },
    getMargin     : function () { return stim.margin; },
    getOffset     : function () { return stim.offset; },
    getPadding    : function () { return stim.padding; },
    getROIH       : function () { return stim.roiH; },
    getROIExp     : function () { return stim.roiExp; },
    getROIs       : function () { return stim.rois; },
    getText       : function () { return stim.txt; },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    /*
    getROIsArr: function () {
      var A = [];
      for (var i=0, ni=stim.rois.length; i < ni; i++) {
        var r = stim.rois[i];
        var w = stim.words[i];
        
        var vars = {};
        $map(function (x) { vars[x] = 1; }, w.tags);
        A.push({ name: w.str, x1: r.x1, y1: r.y1, x2: r.x2, y2: r.y2, vars: vars });
      }
      return A;
    },
    */
    
    
    // -----------------------------------------------------------------------------------------------------------------
    getROICnt: function () { return stim.rois.length; },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    getROIsTxt: function (delim) {
      var res = $map(
        function (A) {
          var w = A[0];
          var r = A[1];
          
          var T = [];  // tags
          T = T.concat(w.tags);
          if (r.sentEnd) T.push(CONST.tags.sentEnd);
          if (r.sentStart) T.push(CONST.tags.sentStart);
          if (r.clauseEnd) T.push(CONST.tags.clauseEnd);
          if (r.clauseStart) T.push(CONST.tags.clauseStart);
          
          return w.str.replace(/\\/g, "\\\\").replace(/"/g, "\\\"").replace(/&nbsp;/g, " ") + "\t" + r.x1 + "," + r.y1 + "," + r.x2 + "," + r.y2 + (T.length > 0 ? "\t" + T.join(",") : "");
        },
        $filter(
          function (A) { return A[1].enabled; },
          stim.words, stim.rois
        )
      );
      return res.join(delim);
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    isSingleLine : function () { return stim.singleLine; },
    isUpdated    : function () { return state.updated; },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    incFontSize: function () { if (stim.fontSize + 1 > CONST.ftSize.max) return; stim.fontSize += 1; state.updated = true; },
    decFontSize: function () { if (stim.fontSize - 1 < CONST.ftSize.min) return; stim.fontSize -= 1; state.updated = true; },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    incLetterSpac: function () { if (stim.letterSpac + 0.01 > CONST.letterSpac.max) return; stim.letterSpac += 0.01; stim.letterSpac = Math.round(stim.letterSpac*100)/100; state.updated = true; },
    decLetterSpac: function () { if (stim.letterSpac - 0.01 < CONST.letterSpac.min) return; stim.letterSpac -= 0.01; stim.letterSpac = Math.round(stim.letterSpac*100)/100; state.updated = true; },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    incLineH: function () { if (stim.lineH + 1 > CONST.lineH.max) return; stim.lineH += 1; state.updated = true; },
    decLineH: function () { if (stim.lineH - 1 < CONST.lineH.min) return; stim.lineH -= 1; state.updated = true; },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    incMargin: function (side) { if (stim.margin[side] === undefined) return showMsg("Wrong side."); if (stim.margin[side] + 1 > CONST.margin.max) return; stim.margin[side] += 1; state.updated = true; },
    decMargin: function (side) { if (stim.margin[side] === undefined) return showMsg("Wrong side."); if (stim.margin[side] - 1 < CONST.margin.min) return; stim.margin[side] -= 1; state.updated = true; },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    incOffset: function (axis) { if (stim.offset[axis] === undefined) return showMsg("Wrong axis."); if (stim.offset[axis] + 1 > CONST.offset.max) return; stim.offset[axis] += 1; state.updated = true; },
    decOffset: function (axis) { if (stim.offset[axis] === undefined) return showMsg("Wrong axis."); if (stim.offset[axis] - 1 < CONST.offset.min) return; stim.offset[axis] -= 1; state.updated = true; },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    incPadding: function (side) { if (stim.padding[side] === undefined) return showMsg("Wrong side."); if (stim.padding[side] + 1 > CONST.padding.max) return; stim.padding[side] += 1; state.updated = true; },
    decPadding: function (side) { if (stim.padding[side] === undefined) return showMsg("Wrong side."); if (stim.padding[side] - 1 < CONST.padding.min) return; stim.padding[side] -= 1; state.updated = true; },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    incROIH: function () { if (stim.roiH + 1 > CONST.roiH.max) return; stim.roiH += 1; state.updated = true; },
    decROIH: function () { if (stim.roiH + 1 > CONST.roiH.max) return; stim.roiH -= 1; state.updated = true; },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    setAlign: function (dir, a) {
      if (!CONST.align[dir]) return showMsg("Wrong alignment direction.")
      if (!CONST.align[dir][a]) return showMsg("Wrong alignment value for the specified direction.");
      stim.align[dir] = a;
      state.updated = true;
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    setClickModeNormal      : function () { state.clickMode = CONST.clickMode.normal; },
    setClickModeCustom      : function () { state.clickMode = CONST.clickMode.custom; },
    setClickModeDisable     : function () { state.clickMode = CONST.clickMode.disable; },
    setClickModeSentEnd     : function () { state.clickMode = CONST.clickMode.sentEnd; },
    setClickModeSentStart   : function () { state.clickMode = CONST.clickMode.sentStart; },
    setClickModeClauseEnd   : function () { state.clickMode = CONST.clickMode.clauseEnd; },
    setClickModeClauseStart : function () { state.clickMode = CONST.clickMode.clauseStart; },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    setROIExp: function (b) { stim.roiExp = b; state.updated = true; },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    // `z` should be a factor (e.g., 0.75), not a percentage (e.g., 75%).
    setZoom: function (z) {
      state.zoom = z;
      
      updateStimulus();
      updateROIs();
      
      state.updated = false;
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    update: function () {
      if (!state.updated) return;
      
      updateStimulus();
      updateROIs();
      
      state.updated = false;
    }
  };
  
  
  // ===================================================================================================================
  // ==[ CONSTRUCT ]====================================================================================================
  // ===================================================================================================================
  
  construct();
  return pub;
};
