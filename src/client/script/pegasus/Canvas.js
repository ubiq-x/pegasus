var Canvas = function(_canvas, _class, _parent) {
  
  // ===================================================================================================================
  // == PRIVATE ========================================================================================================
  // ===================================================================================================================
  
  var canvas = null;
  var ctx = null;
  
  var canvasScaleX = 1.0;
  var canvasScaleY = 1.0;
  
  
  // -------------------------------------------------------------------------------------------------------------------
  function construct() {
    if (_canvas && typeof _canvas === "string") {
      canvas = document.getElementById(_canvas);
    }
    else {
      canvas = document.createElement("canvas");
      if (_parent) _parent.appendChild(canvas);
    }
    
    if (_class) canvas.className = _class;
    ctx = canvas.getContext("2d");
    
    ctx.save();
  }
  
  
  // ===================================================================================================================
  // == PUBLIC =========================================================================================================
  // ===================================================================================================================
  
  var pub = {
    
    ANGLE_360 : (Math.PI * 360) / 180,
    ANGLE_180 : (Math.PI * 180) / 180,
    ANGLE_90  : (Math.PI * 90)  / 180,
    ANGLE_45  : (Math.PI * 45)  / 180,
    ANGLE_30  : (Math.PI * 30)  / 180,
    ANGLE_0   : (Math.PI * 30)  / 180,
    
    
    // -----------------------------------------------------------------------------------------------------------------
    addEvList: function(e, fn, capture) {
      canvas.addEventListener(e, fn, capture);
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    clear: function() {
      ctx.save();
      ctx.scale(1/canvasScaleX, 1/canvasScaleY);
      ctx.clearRect(0, 0, canvas.width, canvas.height);
      ctx.restore();
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    clearRect: function(x, y , width, height) {
      ctx.clearRect(x, y , width, height);
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    deg2rad: function(angle) { return (Math.PI * angle) / 180; },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    drawCircle: function(center, radius, width, fgColor, bgColor) {
      ctx.save();
      ctx.beginPath();
      ctx.arc(center.x, center.y, radius, 0, Math.PI*2, true);
      ctx.closePath();
      if (bgColor) {
        ctx.fillStyle = bgColor;
        ctx.fill();
      }
      if (width > 0) {
        ctx.lineWidth = width;
        ctx.strokeStyle = fgColor;
        ctx.stroke();
      }
      ctx.restore();
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    drawImg: function(img, x, y) {
      ctx.drawImage(img, x, y);
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    drawLine: function(from, to, width, color) {
      ctx.save();
      ctx.lineWidth = width;
      ctx.strokeStyle = color;
      ctx.beginPath();
      ctx.moveTo(from.x, from.y);
      ctx.lineTo(to.x, to.y);
      ctx.stroke();
      ctx.restore();
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    drawDottedLine: function(from, to, width, dotDist, color) {
      var tmp = document.createElement("canvas");
      tmp.width = width;
      tmp.height = width;
      tmp.getContext("2d").fillStyle = color;
      tmp.getContext("2d").fillRect(0,0,width,width);
      
      var distX = Math.abs(from.x - to.x);
      var distY = Math.abs(from.y - to.y);
      var steps = Math.sqrt(distX*distX + distY*distY);
      var dx = distX / steps;
      var dy = distY / steps;
      var currX = from.x;
      var currY = from.y;
      
      ctx.save();
      for (var i=0, n=steps; i < n; i+=2) {
        ctx.drawImage(tmp, parseInt(currX), parseInt(currY));
        currX += dx+dx;
        currY += dy+dy;
      }
      ctx.restore();
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    drawPolyline: function(/*type, */points, width, color) {
      ctx.save();
      ctx.lineWidth = width;
      ctx.strokeStyle = color;
      ctx.beginPath();
      ctx.moveTo(points[0].x, points[0].y);
      for (var i=1, l=points.length; i < l; i++) {
        ctx.lineTo(points[i].x, points[i].y);
      }
      ctx.stroke();
      ctx.restore();
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    drawRect: function(p, width, height, angle, lineWidth, fgColor, bgColor) {
      ctx.save();
      ctx.translate(p.x, p.y);
      ctx.rotate(pub.deg2rad(angle));
      if (fgColor) {
        ctx.lineWidth = lineWidth;
        ctx.strokeStyle = fgColor;
        ctx.strokeRect(0, 0, width, height);
      }
      if (bgColor) {
        ctx.fillStyle = bgColor;
        ctx.fillRect(0, 0, width, height);
      }
      ctx.restore();
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    drawRectCenter: function(center, width, height, angle, lineWidth, fgColor, bgColor) {
      ctx.save();
      ctx.translate(center.x, center.y);
      ctx.rotate(pub.deg2rad(angle));
      if (fgColor) {
        ctx.lineWidth = lineWidth;
        ctx.strokeStyle = fgColor;
        ctx.strokeRect(-width/2, -height/2, width, height);
      }
      if (bgColor) {
        ctx.fillStyle = bgColor;
        ctx.fillRect(-width/2, -height/2, width, height);
      }
      ctx.restore();
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    drawText: function(text, position, fgColor, bgColor, border) {
      var div = document.createElement("div");
      div.style.position = "absolute";
      div.style.left = position.x;
      div.style.top = position.y;
      div.style.border = border;
      div.innerHTML = text;
      canvas.appendChild(div);
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    drawTriangle: function(center, side, angle, width, fgColor, bgColor) {
      var m = Math.sqrt(side*side - (side/2)*(side/2));  // the median
      
      ctx.save();
      ctx.lineWidth = width;
      ctx.strokeStyle = fgColor;
      ctx.translate(center.x, center.y);
      ctx.rotate(pub.deg2rad(angle));
      ctx.beginPath();
      ctx.moveTo(-side/2, m/3);
      ctx.lineTo(side/2, m/3);
      ctx.lineTo(0, -2/3*m);
      ctx.closePath();
      ctx.stroke();
      if (bgColor) {
        ctx.fillStyle = bgColor;
        ctx.fill();
      }
      ctx.restore();
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    drawString: function(text, color, size, x, y) {
      var fw = size*0.666666;
      var lw = size*0.125;
      var ls = lw/2;
      var xp = 0;
      var cr = lw;
      ctx.lineCap = "round";
      ctx.lineJoin = "round"
      ctx.lineWidth = lw;
      ctx.strokeStyle = color;
      for (var i = 0; i < text.length; i++) {
        this.drawSymbol(text[i], ls, x+xp, y, fw, size);
        xp += (text[i]!="."?fw+cr:(fw/2)+cr);
      }
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    drawSymbol: function(symbol, fc, cx, cy, cw, ch) {
      ctx.beginPath();
      switch (symbol) {
        case "0":
          ctx.moveTo(cx+fc,cy+(ch*0.333333));
          ctx.arc(cx+(cw/2),cy+(cw/2),(cw/2)-fc,this.ANGLE_180,0, false);
          ctx.arc(cx+(cw/2),(cy+ch)-(cw/2),(cw/2)-fc,0,this.ANGLE_180, false);
          ctx.closePath();
          break;
        case "1":
          ctx.moveTo(cx+(cw*0.1)+fc,cy+ch-fc);
          ctx.lineTo(cx+cw-fc,cy+ch-fc);
          ctx.moveTo(cx+(cw*0.666666),cy+ch-fc);
          ctx.lineTo(cx+(cw*0.666666),cy+fc);
          ctx.lineTo(cx+(cw*0.25),cy+(ch*0.25));
          break;
        case "2":
          ctx.moveTo(cx+cw-fc,cy+(ch*0.8));
          ctx.lineTo(cx+cw-fc,cy+ch-fc);
          ctx.lineTo(cx+fc,cy+ch-fc);
          ctx.arc(cx+(cw/2),cy+(cw*0.425),(cw*0.425)-fc,this.ANGLE_45,-this.ANGLE_180, true);
          break;
        case "3":
          ctx.moveTo(cx+(cw*0.1)+fc,cy+fc);
          ctx.lineTo(cx+(cw*0.9)-fc,cy+fc);
          ctx.arc(cx+(cw/2),cy+ch-(cw*0.5),(cw*0.5)-fc,-this.ANGLE_90,this.ANGLE_180, false);
          break;
        case "4":
          ctx.moveTo(cx+(cw*0.75),cy+ch-fc);
          ctx.lineTo(cx+(cw*0.75),cy+fc);
          ctx.moveTo(cx+cw-fc,cy+(ch*0.666666));
          ctx.lineTo(cx+fc,cy+(ch*0.666666));
          ctx.lineTo(cx+(cw*0.75),cy+fc);
          ctx.moveTo(cx+cw-fc,cy+ch-fc);
          ctx.lineTo(cx+(cw*0.5),cy+ch-fc);
          break;
        case "5":
          ctx.moveTo(cx+(cw*0.9)-fc,cy+fc);
          ctx.lineTo(cx+(cw*0.1)+fc,cy+fc);
          ctx.lineTo(cx+(cw*0.1)+fc,cy+(ch*0.333333));
          ctx.arc(cx+(cw/2),cy+ch-(cw*0.5),(cw*0.5)-fc,pub.deg2rad(-80),this.ANGLE_180, false);
          break;
        case "6":
          ctx.moveTo(cx+fc,cy+ch-(cw*0.5)-fc);
          ctx.arc(cx+(cw/2),cy+ch-(cw*0.5),(cw*0.5)-fc,-this.ANGLE_180,this.ANGLE_180, false);
          ctx.bezierCurveTo(cx+fc,cy+fc,cx+fc,cy+fc,cx+(cw*0.9)-fc,cy+fc);
          ctx.moveTo(cx+(cw*0.9)-fc,cy+fc);
          break;
        case "7":
          ctx.moveTo(cx+(cw*0.5),cy+ch-fc);
          ctx.lineTo(cx+cw-fc,cy+fc);
          ctx.lineTo(cx+(cw*0.1)+fc,cy+fc);
          ctx.lineTo(cx+(cw*0.1)+fc,cy+(ch*0.25)-fc);
          break;
        case "8":
          ctx.moveTo(cx+(cw*0.92)-fc,cy+(cw*0.59));
          ctx.arc(cx+(cw/2),cy+(cw*0.45),(cw*0.45)-fc,pub.deg2rad(25),pub.deg2rad(-205), true);
          ctx.arc(cx+(cw/2),cy+ch-(cw*0.5),(cw*0.5)-fc,pub.deg2rad(-135),-this.ANGLE_45, true);
          ctx.closePath();
          ctx.moveTo(cx+(cw*0.79),cy+(ch*0.47));
          ctx.lineTo(cx+(cw*0.21),cy+(ch*0.47));
          break;
        case "9":
          ctx.moveTo(cx+cw-fc,cy+(cw*0.5));
          ctx.arc(cx+(cw/2),cy+(cw*0.5),(cw*0.5)-fc,this.ANGLE_0,this.ANGLE_360, false);
          ctx.bezierCurveTo(cx+cw-fc,cy+ch-fc,cx+cw-fc,cy+ch-fc,cx+(cw*0.1)+fc,cy+ch-fc);
          break;
        case "%":
          ctx.moveTo(cx+fc,cy+(ch*0.75));
          ctx.lineTo(cx+cw-fc,cy+(ch*0.25));
          ctx.moveTo(cx+(cw*0.505),cy+(cw*0.3));
          ctx.arc(cx+(cw*0.3),cy+(cw*0.3),(cw*0.3)-fc,this.ANGLE_0,this.ANGLE_360, false);
          ctx.moveTo(cx+(cw*0.905),cy+ch-(cw*0.3));
          ctx.arc(cx+(cw*0.7),cy+ch-(cw*0.3),(cw*0.3)-fc,this.ANGLE_0,this.ANGLE_360, false);
          break;
        case ".":
          ctx.moveTo(cx+(cw*0.25),cy+ch-fc-fc);
          ctx.arc(cx+(cw*0.25),cy+ch-fc-fc,fc,this.ANGLE_0,this.ANGLE_360, false);
          ctx.closePath();
          break;
        case ":":
          ctx.moveTo(cx+(cw*0.25),cy+ch-fc-fc);
          ctx.arc(cx+(cw*0.25),cy+ch-fc-fc,fc,this.ANGLE_0,this.ANGLE_360, false);
          ctx.closePath();
          break;
      }	
      ctx.stroke();
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    get    : function() { return canvas; },
    getCtx : function() { return ctx; },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    getImg: function() {
      var i = new Image();
      i.src = canvas.toDataURL();
      return i;
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    getH: function() { return canvas.height; },
    getW: function() { return canvas.width; },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    hide: function() { canvas.style.display = "none"; },
    show: function() { canvas.style.display = "block"; },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    scale     : function(x,y)   { ctx.scale(x,y);  canvasScaleX = x;  canvasScaleY = y; },
    rotate    : function(angle) { ctx.rotate(pub.deg2rad(angle)); },
    translate : function(x,y)   { ctx.translate(x,y); },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    setDim: function(w, h) {
      canvas.width = w;
      canvas.height = h;
    },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    setH: function(h) { canvas.height = h; },
    setW: function(w) { canvas.width = w; },
    
    
    // -----------------------------------------------------------------------------------------------------------------
    toDataURL: function() { return canvas.toDataURL(); },
  };
  
  
  // ===================================================================================================================
  // == CONSTRUCT ======================================================================================================
  // ===================================================================================================================
  
  construct();
  return pub;
};
