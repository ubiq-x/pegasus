// JSON Performance comparison of eval, new Function and JSON
// http://weblogs.asp.net/yuanjian/archive/2009/03/22/json-performance-comparison-of-eval-new-function-and-json.aspx

function $(id) { return document.getElementById(id); }
function $trim(s) { return (!s ? s : s.replace(/^\s+|\s+$/g, "")); }
function $enc(s) { return encodeURIComponent("'" + s.replace(/\\/g, "\\\\").replace(/'/g, "\\'") + "'"); }
function $dec(s) { return decodeURIComponent(s).replace(/\+/g, " "); };


// ---------------------------------------------------------------------------------------------------------------------
function $join(A, delim) {
  return $filter(function (x) { return !!x; }, A).join(delim);
}


// ---------------------------------------------------------------------------------------------------------------------
function $hide() {
  for (var i=0, ni=arguments.length; i < ni; i++) {
    var x = arguments[i];
    (typeof x === "string" ? $(x).style.display = "none" : x.style.display = "none");
  }
}


// ---------------------------------------------------------------------------------------------------------------------
function $show() {
  for (var i=0, ni=arguments.length; i < ni; i++) {
    var x = arguments[i];
    (typeof x === "string" ? $(x).style.display = "block" : x.style.display = "block");
  }
}


// ---------------------------------------------------------------------------------------------------------------------
function $$(name, parent, id, clazz, html) {
  var el = document.createElement(name);
  if (id) el.id = id;
  if (clazz) el.className = clazz;
  if (html) el.innerHTML = html;
  if (parent) parent.appendChild(el);
  return el;
}


// ---------------------------------------------------------------------------------------------------------------------
/**
 * This function will not create an image if the `src` attribute is not provided. The reason for that is that some 
 * browsers do not handle that well and may generate unnecessary and useless requests to the server.
 */
function $$img(parent, id, clazz, src) {
  if (!src) return;
  
  var el = document.createElement("img");
  if (id) el.id = id;
  if (clazz) el.className = clazz;
  if (src) el.setAttribute("src", src);
  if (parent) parent.appendChild(el);
  return el;
}


// ---------------------------------------------------------------------------------------------------------------------
function $$txt(parent, txt) {
  var el = document.createTextNode(txt);
  if (parent) parent.appendChild(el);
  return el;
}


// ---------------------------------------------------------------------------------------------------------------------
function $$$(x) { return x; }


// ---------------------------------------------------------------------------------------------------------------------
function $setAttr(o, A) {
  for (var a in A) o.setAttribute(a, A[a]);
  return o;
}


// ---------------------------------------------------------------------------------------------------------------------
function $setProp(o, A) {
  if (!o) var o = {};
  for (var a in A) o[a] = A[a];
  return o;
}


// ---------------------------------------------------------------------------------
function $size2str(bytes, html) {
	if (bytes < 1024) return bytes + (html ? "&nbsp;" : " ") + "B";
	if (bytes < 1024*1024) return Math.round(bytes/1024) + (html ? "&nbsp;" : " ") + "kB";
	if (bytes < 1024*1024*1024) return Math.round((bytes/(1024*1024))*100)/100 + (html ? "&nbsp;" : " ") + "MB";
	if (bytes < 1024*1024*1024*1024) return Math.round((bytes/(1024*1024*1024))*100)/100 + (html ? "&nbsp;" : " ") + "GB";
	return (html ? ">&nbsp;1&nbsp;TB" : "> 1 TB");
}


// ---------------------------------------------------------------------------------------------------------------------
function $arrDup(A, x, optional) {
  var n = A.length;
  for (var i=0; i < n; i++) {
    for (var j=i+1; j < n; j++) {
      if (optional) {
        if (A[i][x] !== undefined && A[j][x] !== undefined && A[i][x] === A[j][x]) return true;
      }
      else if (A[i][x] === A[j][x]) return true;
    }
  }
  return false;
}


// ---------------------------------------------------------------------------------------------------------------------
function $arrRange(A, x, min, max, optional) {
  for (var i=0, n=A.length; i < n; i++) {
    if (optional) {
      if (A[i][x] && (A[i][x] < min || A[i][x] > max)) return false;
    }
    else if (A[i][x] < min || A[i][x] > max) return false;
  }
  return true;
}


// ---------------------------------------------------------------------------------------------------------------------
/**
 * Removes the item 'x' from the array 'A' and returns a new array.
 */
function $arrRemItem(A,x) {
  var res = [];
  $map(function (y) { if (x !== y) res.push(y); }, A);
  return res;
}


// ---------------------------------------------------------------------------------------------------------------------
function $arrContains(A,x) {
  for (var i=0, ni=A.length; i < ni; i++) {
    if (A[i] === x) return true;
  }
  return false;
}


// ---------------------------------------------------------------------------------------------------------------------
/**
 * Converts a hashtable into an array. The resulting elements are computed as follows:
 * 
 *   <key> + connStr + <value>
 * 
 * If `keys` is a nonempty array of keys, then only those elements of the hashtable are put in the resulting array.
 */
function $hash2arr(H, connStr, keys) {
  var A = [];
  if (keys) {  // only specific keys
    for (var i=0, ni=keys.length; i < ni; i++) {
      var k = keys[i];
      A.push(k + connStr + H[k]);
    }
  }
  else {  // all keys
    for (var k in H) {
      A.push(k + connStr + H[k]);
    }
  }
  return A;
}


// ---------------------------------------------------------------------------------------------------------------------
/**
 * The object 'o' is expected to contain the following properties:
 *   method  : "POST" | "GET"
 *   uri     : ""
 *   params  : ""  (used for POST requests)
 *   eval    : true|false  (evaluate the 'requestText' property of the reponse?)
 *   minHead : true|false  (minimize header of the request?)
 *   fnOk    : function (HTTP ok)
 *   fnErr   : function (HTTP error)
 */
function $call(o) {
  // Init:
  var fnSetHead = function(req,k,v) {
    try { req.setRequestHeader(k,v); } catch(e) {}
  };
  
  var req = new XMLHttpRequest();
  req.open(o.method, o.uri, true);
  
  // Header:
  if (o.minHead) {
    //fnSetHead(req, "User-Agent", null);
    fnSetHead(req, "Accept", null);
    fnSetHead(req, "Accept-Language", null);
    fnSetHead(req, "Content-Type", "M");
    //fnSetHead(req, "Connection", "keep-alive");
    //fnSetHead(req, "Keep-Alive", null);
  }
  
  // Response handling:
  if (o.fnOk || o.fnErr) {
    req.onreadystatechange = function (e) {
      if (req.readyState === 4) {
        // ok:
        if (req.status === 200) {
          var res = $getRes(req);
          if (o.fnOk) o.fnOk(res);
        }
        // error:
        else {
          if (o.fnErr) o.fnErr(req);
        }
      }
    };
  };
  
  // Send:
  if (o.params) {
    req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    //req.setRequestHeader("Content-length", o.params.length);
    //req.setRequestHeader("Connection", "close");
    req.send(o.params);
  }
  else req.send(null);
}


// ---------------------------------------------------------------------------------------------------------------------
function $getRes(req) {
  var res = null;
  try {
    eval("res = " + req.responseText);
  }
  catch (ex) {
    res = { outcome: false, msg: "Server's response parsing error:\n\n" + ex };
  }
  return res;
};


// ---------------------------------------------------------------------------------------------------------------------
function $evtGet(e) {
  if (typeof e == 'undefined') e = window.event;
  if (typeof e.layerX == 'undefined') e.layerX = e.offsetX;
  if (typeof e.layerY == 'undefined') e.layerY = e.offsetY;
  return e;
}


// ---------------------------------------------------------------------------------------------------------------------
function $evtChar(e) {
  e = $evtGet(e);
  return e.charCode;
}


// ---------------------------------------------------------------------------------------------------------------------
function $evtCode(e) {
  e = $evtGet(e);
  return e.keyCode;
}


// ---------------------------------------------------------------------------------------------------------------------
function $evtKey(e) {
  e = $evtGet(e);
  return (window.event ? event.keyCode : e.which);
}


// ---------------------------------------------------------------------------------------------------------------------
function $evtTgt(e) {
  e = $evtGet(e);
  return e.target || e.srcElement;
}


// ---------------------------------------------------------------------------------------------------------------------
/**
 * Returns only elements for which 'fn' returns 'true' (or anything that evaluates to 'true').
 * 
 * Pass a single object, an array, or multiple arrays after the 'fn' argument. In the case of multiple arrays they are 
 * anticipated to be of the same size.
 *
 * 'fn' should accept a single argument. In the case of multiple arrays that element will be an array.
 */
function $filter(fn) {
  var lstCnt = arguments.length-1;
  var res = [];
  
  if (lstCnt === 1) {  // one extra argument
    if (!(arguments[1] instanceof Array)) {  // and this argument ain't an array
      var ok = fn(arguments[1])
      return (ok ? arguments[1] : null);
    }
    
    for (var i=0, l=arguments[1].length; i < l; i++) {  // it is an array
      var ok = fn(arguments[1][i])
      if (ok) res.push(arguments[1][i]);
    }
  }
  else {  // multiple extra arguments
    for (var i=0, l=arguments[1].length; i < l; i++) {
      var lst = [];
      for (var j=1; j <= lstCnt; j++) {
        lst.push(arguments[j][i]);
      }
      var ok = fn(lst)
      if (ok) res.push(lst);
    }
  }
  
  return res;
}


// ---------------------------------------------------------------------------------------------------------------------
function $lfold(fn, A, init) {
  var res = init;
  for (var i=0,ni=A.length; i < ni; i++) {
    res = fn(res, A[i]);
  }
  return res;
}


// ---------------------------------------------------------------------------------------------------------------------
/**
 * Pass a single object, an array, or multiple arrays after the 'fn' argument. In the case of multiple arrays they are 
 * anticipated to be of the same size.
 *
 * 'fn' should accept a single argument. In the case of multiple arrays that element will be an array.
 */
function $map(fn) {
  var lstCnt = arguments.length-1;
  var res = [];
  
  if (lstCnt === 1) {  // one extra argument
    if (!(arguments[1] instanceof Array)) {  // and this argument ain't an array
      return fn(arguments[1]);
    }
    
    for (var i=0, l=arguments[1].length; i < l; i++) {  // it is an array
      res[i] = fn(arguments[1][i]);
    }
  }
  else {  // multiple extra arguments
    for (var i=0, l=arguments[1].length; i < l; i++) {
      var lst = [];
      for (var j=1; j <= lstCnt; j++) {
        lst.push(arguments[j][i]);
      }
      res[i] = fn(lst);
    }
  }
  
  return res;
}


// ---------------------------------------------------------------------------------------------------------------------
function $removeChildren(el) {
  if (typeof x === "string") el = $(id);
  while (el.hasChildNodes()) el.removeChild(el.childNodes[0] || el.children[0]);
}


// ---------------------------------------------------------------------------------------------------------------------
function $setObj(to, from, prop) {
  if (!to) var to = {};
  if (prop) $map(function (x) { to[x] = from[x]; }, prop);
  else for (var x in from) to[x] = from[x];
  return to;
}


// ---------------------------------------------------------------------------------------------------------------------
function $getSubObj(o, prop) {
  var o2 = {};
  $setObj(o2, o, prop);
  return o2;
}


// ---------------------------------------------------------------------------------------------------------------------
function $trunc(num, ndec) {
    if (num == 0) return 0;
		var n = Math.pow(10, ndec);
		return Math.round(num*n)/n;
}
