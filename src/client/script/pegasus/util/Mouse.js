if (!Pegasus) var Pegasus = {};
if (!Pegasus.util) Pegasus.util = {};


Pegasus.util.Mouse = {
  
  // -------------------------------------------------------------------------------------------------------------------
  // Src: http://www.quirksmode.org/js/events_properties.html
  getPos: function (e) {
    var pos = { x:0, y:0 };
    if (!e) var e = window.event;
    if (e.pageX || e.pageY) {
      pos.x = e.pageX;
      pos.y = e.pageY;
    }
    else if (e.clientX || e.clientY) {
      pos.x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
      pos.y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
    }
    return pos;
  }
};
