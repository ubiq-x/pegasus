if (!Pegasus) var Pegasus = {};
if (!Pegasus.util) Pegasus.util = {};


Pegasus.util.Comm = {
  
  // -----------------------------------------------------------------------------------------------------------------
  getQS: function() {
    var qs = {};
    
    $map(
      function (x) {
        var tmp = x.split("=");
        if (tmp[0]) qs[tmp[0]] = decodeURIComponent(tmp[1]);
      },
      window.location.search.substr(1).split("&")
    );
    
    return qs;
  }
};
