if (!Pegasus) var Pegasus = {};
if (!Pegasus.util) Pegasus.util = {};


/*
 * Src: http://www.quirksmode.org/js/cookies.html
 */
Pegasus.util.Cookie = {
  
  // -----------------------------------------------------------------------------------------------------------------
  get: function(name) {
    var nameEq = name + "=";
    var C = document.cookie.split(';');
    for(var i = 0; i < C.length; i++) {
      var c = C[i];
      while (c.charAt(0)==' ') c = c.substring(1, c.length);
      if (c.indexOf(nameEq) == 0) return c.substring(nameEq.length, c.length);
    }
    return null;
  },
  
  
  // -----------------------------------------------------------------------------------------------------------------
  set: function(name, value, days) {
    var expires = "";
    if (days) {
      var date = new Date();
      date.setTime(date.getTime() + (days*24*60*60*1000));
      expires = "; expires=" + date.toGMTString();
    }
    document.cookie = name + "=" + value + expires + "; path=/";
  },
  
  
  // -----------------------------------------------------------------------------------------------------------------
  remove: function(name) {
    this.set(name, "", -1);
  }
};
