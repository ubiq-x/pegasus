if (!Pegasus) var Pegasus = {};
if (!Pegasus.util) Pegasus.util = {};


Pegasus.util.Util = {
  
  // -------------------------------------------------------------------------------------------------------------------
  ms2time: function (t) {
    var tSec = t / 1000;
    var s = Math.floor(tSec % 60);
    var m = Math.floor((tSec % 3600) / 60);
    var h = Math.floor(tSec / 3600);
    
    return (h < 10 ? "0" : "") + h + ":" + (m < 10 ? "0" : "") + m + ":" + (s < 10 ? "0" : "") + s;
  }
}
