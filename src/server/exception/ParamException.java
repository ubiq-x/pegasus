package exception;


/**
 * @author Tomek D. Loboda
 */
@SuppressWarnings("serial")
public class ParamException extends Exception {
	public final String msg;
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * C'tor.
	 */
	public ParamException(String msg) {
		this.msg = msg;
	}
	
	
	// ---------------------------------------------------------------------------------
	public String getMessage() {
		return msg;
	}
}
