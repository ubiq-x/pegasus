package exception;


/**
 * @author Tomek D. Loboda
 */
@SuppressWarnings("serial")
public class PegasusException extends Exception {
	public final String msg;
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * C'tor.
	 */
	public PegasusException(String msg) {
		this.msg = msg;
	}
	
	
	// ---------------------------------------------------------------------------------
	public String getMessage() {
		return msg;
	}
}
