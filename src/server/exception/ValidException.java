package exception;


/**
 * @author Tomek D. Loboda
 */
@SuppressWarnings("serial")
public class ValidException extends Exception {
	public final String msg;
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * C'tor.
	 */
	public ValidException(String msg) {
		this.msg = msg;
	}
	
	
	// ---------------------------------------------------------------------------------
	public String getMessage() {
		return msg;
	}
}
