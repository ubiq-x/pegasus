package util;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;


/**
 * @author Tomek D. Loboda
 */
public class DataSet {
	public static final int TYPE_I = 0;
	public static final int TYPE_F = 1;
	
	public List<List<String>> data = new ArrayList<List<String>>(); 
	public List<Stats> stats = new ArrayList<Stats>(); 
	public List<Integer> types = new ArrayList<Integer>(); 
	public Hashtable<String,Integer> names = new Hashtable<String,Integer>();
	
	public int colCnt = 0;
	public int rowCnt = 0;
	
	
	// ---------------------------------------------------------------------------------
	public DataSet(String data, String names, String delimRow, String delimCol) {
		// (1) Parse:
		String[] R = data.split(delimRow);
		colCnt = R[0].split(delimCol).length;
		
		String[] N = names.split(delimCol);
		for (int i = 0; i < N.length; i++) {
			this.names.put(N[i], i);
			stats.add(new Stats());
		}
		
		for (int i = 0; i < R.length; i++) {
			List<String> r = new ArrayList<String>();
			String[] C = R[i].split(delimCol);
			for (int j = 0; j < C.length; j++) {
				String val = C[j];
				r.add(val);
				
				try {
					float valF = new Float(val);
					stats.get(j).addVal(valF);
				}
				catch (NumberFormatException e) {}
			}
			this.data.add(r);
		}
		rowCnt = this.data.size();
		
		// (2) Determine the type:
		for (int j = 0; j < colCnt; j++) {
			types.add(TYPE_I);
			for (int i = 0; i < rowCnt; i++) {
				if (val(i,j).contains(".")) {
					types.set(types.size()-1, TYPE_F);
					break;
				}
			}
		}
	}
	
	
	// ---------------------------------------------------------------------------------
	public Stats getVarStats(String name) { return stats.get(names.get(name)); }
	public int getVarType(String name) { return types.get(names.get(name)); }
	
	
	// ---------------------------------------------------------------------------------
	public String val(int r, int c) { return data.get(r).get(c); }
	public String val(int r, String name) { return (data.get(r).get(names.get(name)) == "." ? "" + stats.get(r).getMean() : data.get(r).get(names.get(name))); }
	
	
	// ---------------------------------------------------------------------------------
	public void save(String path)  throws IOException {
		BufferedWriter out = new BufferedWriter(new FileWriter(path));
		Iterator<String> it = names.keySet().iterator();
		while (it.hasNext()) out.write(it.next() + (it.hasNext() ? "\t" : ""));
		out.write("\n");
		
		for (int i = 0; i < rowCnt; i++) {
			for (int j = 0; j < colCnt; j++) {
				out.write(val(i,j) + (j < colCnt-1 ? "\t" : ""));
			}
			out.write("\n");
		}
		
		out.flush();
		out.close();
	}
}
