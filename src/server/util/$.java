package util;

import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * @author Tomek D. Loboda
 */
abstract public class $ {
	
	// -----------------------------------------------------------------------------------------------------------------
	public static int elemCnt(String s, char delim)         { return s.replaceAll("[^" + delim + "]", "").length(); }
	public static int elemCnt(StringBuilder sb, char delim) { return elemCnt(sb.toString(), delim); }
	public static int elemCnt(StringBuffer sb, char delim)  { return elemCnt(sb.toString(), delim); }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static String ifNull(String s01, String s02) { return (s01 == null ? s02 : s01); }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Returns the index of the N-th occurance of the `lookFor` string in the `lookIn` string, or -1 if that index 
	 * doesn't exist.
	 */
	public static int idxOfNth(String lookIn, String lookFor, int n) {
		int idx = 0;
		for (int i = 0; i < n; i++) {
			idx += lookIn.indexOf(lookFor, idx);
			if (idx == -1) return -1;
		}
		return idx;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static StringBuffer join(String[] A, String prefix, String suffix, String delim, boolean trailDelim, boolean excEmpty) {
		if (A == null) return new StringBuffer();
		
		StringBuffer res = new StringBuffer();
		for (int i = 0; i < A.length; i++) {
			String a = A[i];
			if (excEmpty && (a == null || a.length() == 0)) continue;
			res.append(prefix + a + suffix + delim);
		}
		if (!trailDelim && res.length() > 0) res.delete(res.length() - delim.length(), res.length());
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static StringBuffer join(List<String> L, String prefix, String suffix, String delim, boolean trailDelim, boolean excEmpty) {
		if (L == null) return new StringBuffer();
		
		StringBuffer res = new StringBuffer();
		for (int i = 0; i < L.size(); i++) {
			String l = L.get(i);
			if (excEmpty && (l == null || l.length() == 0)) continue;
			res.append(prefix + L.get(i) + suffix + delim);
		}
		if (!trailDelim && res.length() > 0) res.delete(res.length() - delim.length(), res.length());
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static StringBuffer join2(List<Integer> L, String prefix, String suffix, String delim, boolean trailDelim, boolean excEmpty) {
		if (L == null) return new StringBuffer();
		
		StringBuffer res = new StringBuffer();
		for (int i = 0; i < L.size(); i++) {
			String l = "" + L.get(i);
			if (excEmpty && (l == null || l.length() == 0)) continue;
			res.append(prefix + L.get(i) + suffix + delim);
		}
		if (!trailDelim && res.length() > 0) res.delete(res.length() - delim.length(), res.length());
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static StringBuffer join(Set<String> S, String prefix, String suffix, String delim, boolean trailDelim, boolean excEmpty) {
		if (S == null) return new StringBuffer();
		
		StringBuffer res = new StringBuffer();
		Iterator<String> it = S.iterator();
		while (it.hasNext()) {
			res.append(prefix + it.next() + suffix + delim);
		}
		if (!trailDelim && res.length() > 0) res.delete(res.length() - delim.length(), res.length());
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static void log(String s) { System.out.println(s); }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static String[] regExpr2arr(String s, String regExpr) {
		Pattern p = Pattern.compile(regExpr);
		Matcher m = p.matcher(s);
		if (!m.find()) return null;
		
		String[] res = new String[m.groupCount()];
		for (int i = 0; i < m.groupCount(); i++) {
			res[i] = m.group(i+1);  // +1 because the group 0 is actually the input string itself
		}
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static String repStr(String s, int n) {
		String res = "";
		for (int i = 0; i < n; i++) res += s;
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static boolean strInArr(String s, String[] A) {
		for (String a: A) if (a.equals(s)) return true;
		return false;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static double trunc(double d, int ndec) {
		double n = Math.pow(10, ndec);
		return ((double) Math.round(d*n)/n);
	}
}
