package util;

import java.sql.Time;


public class Convert {
	
	// ---------------------------------------------------------------------------------
	public static String size2str(long bytes) {
		if (bytes < 1024) return bytes + " B";
		if (bytes < 1024*1024) return Math.round((bytes/1024)*100)/100 + " kB";
		if (bytes < 1024*1024*1024) return Math.round((bytes/(1024*1024))*100)/100 + " MB";
		if (bytes < 1024*1024*1024*1024) return Math.round((bytes/(1024*1024*1024))*100)/100 + " GB";
		return "> 1 TB";
	}
	
	
	// ---------------------------------------------------------------------------------
	public static String time2str(long ms) {
		return (new Time(ms - 68400000)).toString();  // need to subtract 68400000 ms to make it 00:00:00
	}
}
