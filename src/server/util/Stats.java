package util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;

import app.gaze.aggr.Aggr;


/**
 * Calculates 5-point statistics.
 * 
 * @author Tomek D. Loboda
 */
public class Stats {
	private final List<Double> V = new ArrayList<Double>();  // values
	
	private double min = 0;
	private double max = 0;
	private double mean = 0;
	private double median = 0;
	private double stdev = 0;
	private double sterr = 0;
	
	private boolean isUpdated = false;
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Stats() {}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public void addVal(double v) {
		V.add(new Double(v));
		isUpdated = false;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public void addVals(double[] vals) {
		for (int j = 0; j < vals.length; j++) V.add(new Double(vals[j]));
		isUpdated = false;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public void addVals(double[] vals, int step) {
		for (int j = 0; j < vals.length; j += step) V.add(new Double(vals[j]));
		isUpdated = false;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	// Some formulas: http://img135.imageshack.us/img135/9779/calculationslz5.jpg
	public void calc() {
		int N = V.size();
		
		if (N == 0) return;
		
		Collections.sort(V);
		
		min = Double.MAX_VALUE;
		max = Double.MIN_VALUE;
		
		Iterator<Double> it = V.iterator();
		while (it.hasNext()) {
			double v = it.next();
			mean += v;
			if (v < min) min = v;
			if (v > max) max = v;
		}
		mean /= N;
		
		int idxMid = V.size() / 2;
		if (idxMid == 0 || idxMid % 2 == 1) median = V.get(idxMid);
		else median = (V.get(idxMid-1).floatValue() + V.get(idxMid) / 2.0);

		double ss = 0;  // sum of squares
		it = V.iterator();
		while (it.hasNext()) {
			double v = ((Double) it.next()) - mean;
			ss += v*v;
		}
		stdev = (double) Math.sqrt(ss / (N-1));
		
		sterr = stdev / (double) Math.sqrt(N);
		
		isUpdated = true;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public String get() {
		if (V.size() == 0) return "null,null,null,null,null,null";
		
		if (!isUpdated) calc();
		return $.trunc(min, 2) + "," + $.trunc(max, 2) + "," + $.trunc(mean, 2) + "," + $.trunc(median, 2) + "," + $.trunc(stdev, 2) + "," + $.trunc(sterr, 2);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public List<String> get(Set<String> vars, String varName) {
		List<String> res = new ArrayList<String>();
		
		res.add(Aggr.varGetVal(vars, varName + "_min", $.trunc(min, 2)));
		res.add(Aggr.varGetVal(vars, varName + "_max", $.trunc(max, 2)));
		res.add(Aggr.varGetVal(vars, varName + "_mean", $.trunc(mean, 2)));
		res.add(Aggr.varGetVal(vars, varName + "_med", $.trunc(median, 2)));
		res.add(Aggr.varGetVal(vars, varName + "_sd", $.trunc(stdev, 2)));
		res.add(Aggr.varGetVal(vars, varName + "_se", $.trunc(sterr, 2)));
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Without automatic truncation.
	 */
	public Double getMin    () { return (!isUpdated || V.size() == 0 ? null : min    ); }
	public Double getMax    () { return (!isUpdated || V.size() == 0 ? null : max    ); }
	public Double getMean   () { return (!isUpdated || V.size() == 0 ? null : mean   ); }
	public Double getMedian () { return (!isUpdated || V.size() == 0 ? null : median ); }
	public Double getStdev  () { return (!isUpdated || V.size() == 0 ? null : stdev  ); }
	public Double getSterr  () { return (!isUpdated || V.size() == 0 ? null : sterr  ); }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * With automatic truncation.
	 */
	public Double getMin    (int ndec) { return (!isUpdated || V.size() == 0 ? null : $.trunc(min,    ndec)); }
	public Double getMax    (int ndec) { return (!isUpdated || V.size() == 0 ? null : $.trunc(max,    ndec)); }
	public Double getMean   (int ndec) { return (!isUpdated || V.size() == 0 ? null : $.trunc(mean,   ndec)); }
	public Double getMedian (int ndec) { return (!isUpdated || V.size() == 0 ? null : $.trunc(median, ndec)); }
	public Double getStdev  (int ndec) { return (!isUpdated || V.size() == 0 ? null : $.trunc(stdev,  ndec)); }
	public Double getSterr  (int ndec) { return (!isUpdated || V.size() == 0 ? null : $.trunc(sterr,  ndec)); }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Used to retrieve one-values statistics simulating the regular statistics format.
	 */
	public static String getOneVal(String val) { return val + "\t" + val + "\t" + val + "\t" + val + "\t" + val + "\t" + val; }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public void setSpreadsheet(HSSFSheet s, int idxR, int idxC, HSSFCellStyle cs) {
		if (V.size() > 0 && !isUpdated) calc();
		
		for (int i = 1; i <= 6; i++) {
			HSSFRow r = s.getRow(idxR + i-1);
			HSSFCell c = null;
			c = r.createCell(idxC, HSSFCell.CELL_TYPE_NUMERIC);
			switch (i) {
				case 1: c.setCellValue(mean); break;
				case 2: c.setCellValue(median); break;
				case 3: c.setCellValue(stdev); break;
				case 4: c.setCellValue(sterr); break;
				case 5: c.setCellValue(min); break;
				case 6: c.setCellValue(max); break;
			}
			c.setCellStyle(cs);
		}
	}
	
	
	// ---------------------------------------------------------------------------------
	public int size() { return V.size(); }
}
