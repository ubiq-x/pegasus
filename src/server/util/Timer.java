package util;


public class Timer {
	private long t0;
	
	
	// ---------------------------------------------------------------------------------
	public Timer() { t0 = System.currentTimeMillis(); }
	
	
	// ---------------------------------------------------------------------------------
	public long elapsed() { return System.currentTimeMillis() - t0; }
	
	
	// ---------------------------------------------------------------------------------
	public String elapsedHuman() {
		long t = System.currentTimeMillis() - t0;
		return Convert.time2str(t);
	}
}
