package util;

import inter.DBI;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;



/**
 * @author Tomek D. Loboda
 */
public class SQLConn {
	private Connection conn = null;
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public SQLConn()  throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		//Class.forName("com.mysql.jdbc.Driver").newInstance();
		Class.forName("org.postgresql.Driver").newInstance();
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Connection get()  throws SQLException {
		if (conn == null || conn.isClosed()) conn = DriverManager.getConnection("jdbc:postgresql:" + DBI.DB_NAME, DBI.DB_USERNAME, DBI.DB_PASSWD);
		return conn;
	}
}
