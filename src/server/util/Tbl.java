package util;

import java.util.ArrayList;
import java.util.List;


/**
 * An auto-extensible table.
 * 
 * @author Tomek D. Loboda
 */
public class Tbl {
	private String pre = "";
	private String post = "";
	private final List<String> header = new ArrayList<String>();
	private final List<List<String>> rows = new ArrayList<List<String>>();
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * C'tor.
	 */
	public Tbl() {}
	
	
	// ---------------------------------------------------------------------------------
	public void addCol(String name, String[] values) {
		header.add(name);
		if (values == null) {
			for (List<String> r: rows) {
				r.add("");
			}
		}
		else {
			int i = 0;
			for (String v: values) {
				if (i == rows.size()) addRow(null);  // auto-extending
				rows.get(i).add(v);
				i++;
			}
		}
	}
	
	
	// ---------------------------------------------------------------------------------
	public void addRow(String[] elems) {
		List<String> r = new ArrayList<String>();
		if (elems == null) {
			for (int i = 0; i < header.size(); i++) {
				r.add("");
			}
		}
		else {
			for (String e: elems) {
				r.add(e);
			}
		}
		rows.add(r);
	}
	
	
	// ---------------------------------------------------------------------------------
	public void addRows(List<List<String>> R) {
		for (List<String> r: R) {
			this.addRow(r.toArray(new String[0]));
		}
	}
	
	
	// ---------------------------------------------------------------------------------
	public int colCnt() {
		return header.size();
	}
	
	
	// ---------------------------------------------------------------------------------
	public StringBuffer get(String delimCol, String delimRow) {
		StringBuffer res = new StringBuffer();
		
		res.append(pre + delimRow + delimRow);
		res.append($.join(header.toArray(new String[0]), "", "", delimCol, false, false) + delimRow);
		
		for (List<String> r: rows) {
			res.append($.join(r.toArray(new String[0]), "", "", delimCol, false, false) + delimRow);
		}
		res.append(delimRow + post);
		
		return res;
	}
	
	
	// ---------------------------------------------------------------------------------
	public List<List<String>> getRows() {
		return rows;
	}
	
	
	// ---------------------------------------------------------------------------------
	public void setPre(String p) {
		pre = p;
	}
	
	
	// ---------------------------------------------------------------------------------
	public void setPost(String p) {
		post = p;
	}
}
