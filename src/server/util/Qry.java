package util;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


/**
 * This class implements a query issuer that return the results in a textual form. 
 * Only non-update queries are allowed (SELECT). It's a lazy evaluation 
 * implementation -- the query is issued only when necessary.
 * 
 * @author Tomek D. Loboda
 */
public class Qry {
	private final String db;
	private final String qry;
	private String[] colNames;
	private List<String[]> rows = new ArrayList<String[]>();
	private boolean issued = false;
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * C'tor.
	 */
	public Qry(String db, String qry) {
		this.db = db;
		this.qry = qry;
	}
	
	
	// ---------------------------------------------------------------------------------
	public StringBuffer getText(Statement st)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		
		if (!issued) {
			if (st == null) {
				Statement st02 = (new SQLConn()).get().createStatement();
				issue(st02);
				st02.close();
			}
			else {
				issue(st);
			}
		}
		
		StringBuffer res = new StringBuffer();
		
		res.append(rows.size() + " row(s) returned:\n\n");
		res.append($.join(colNames, "", "", "\t", false, false) + "\n");
		for (String[] r: rows) {
			res.append($.join(r, "", "", "\t", false, false) + "\n");
		}
		
		return res;
	}
	
	
	// ---------------------------------------------------------------------------------
	public Tbl getTbl(Statement st)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		
		if (!issued) {
			if (st == null) {
				Statement st02 = (new SQLConn()).get().createStatement();
				issue(st02);
				st02.close();
			}
			else {
				issue(st);
			}
		}
		
		Tbl t = new Tbl();
		t.setPre(rows.size() + " row(s) returned:");
		for (String c: colNames) {
			t.addCol(c, null);
		}
		for (String[] r: rows) {
			t.addRow(r);
		}
		
		return t;
	}
	
	
	// ---------------------------------------------------------------------------------
	private void issue(Statement st)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		
		st.executeQuery("USE " + db);
		
		ResultSet rs = st.executeQuery(qry);
		ResultSetMetaData meta = rs.getMetaData();
		
		int colCnt = meta.getColumnCount();
		colNames = new String[colCnt];
		for (int i = 0; i < colCnt; i++) {
			colNames[i] = meta.getColumnName(i+1);
		}
		
		while (rs.next()) {
			String[] row = new String[colCnt];
			for (int i = 0; i < colCnt; i++) {
				row[i] = rs.getString(i+1);
			}
			rows.add(row);
		}
		
		rs.close();
		
		issued = true;
	}
}
