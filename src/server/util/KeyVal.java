package util;

import java.util.Hashtable;
import java.util.Iterator;


/**
 * The hashtable passed as an argument during the object construction can be 'null'. In 
 * such case, 'defVal' will be returned for all the 'get()' requests.
 * 
 * @author Tomek D. Loboda
 */
public class KeyVal {
	private final Hashtable<String, String> kv = new Hashtable<String, String>();
	private final String defVal;
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * C'tor.
	 */
	public KeyVal(Hashtable<String, String> kv, String defVal) {
		digest(kv);
		this.defVal = defVal;
	}
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * Digests a new set of key-value pairs. Key name conflicts are resolved through an 
	 * overwrite.
	 */
	public void digest(Hashtable<String, String> kv) {
		if (kv == null) return;
		Iterator<String> it = kv.keySet().iterator();
		while (it.hasNext()) {
			String k = it.next();
			this.kv.put(k, kv.get(k));
		}
	}
	
	
	// ---------------------------------------------------------------------------------
	public Hashtable<String, String> get() {
		return kv;
	}
	
	
	// ---------------------------------------------------------------------------------
	public String get(String key) {
		if (kv.contains(key)) return kv.get(key);
		else return defVal;
	}
	
	
	// ---------------------------------------------------------------------------------
	public String getKeys(String around, String delim) {
		StringBuffer res = new StringBuffer();
		Iterator<String> it = kv.keySet().iterator();
		while (it.hasNext()) {
			res.append(around + it.next() + around);
			if (it.hasNext()) res.append(delim);
		}
		return res.toString();
	}
	
	
	// ---------------------------------------------------------------------------------
	public String getValues(String around, String delim) {
		StringBuffer res = new StringBuffer();
		Iterator<String> it = kv.keySet().iterator();
		while (it.hasNext()) {
			res.append(around + kv.get(it.next()) + around);
			if (it.hasNext()) res.append(delim);
		}
		return res.toString();
	}
}
