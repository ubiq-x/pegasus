package util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Wraps around the rediculously verbose three-lines-should-be-one way of matching 
 * regular expressions in Java.
 */
public class RegExpr {

	// ---------------------------------------------------------------------------------
	public static Matcher match(String pattern, String txt) {
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(txt);
		m.find();
		return m;
	}
}
