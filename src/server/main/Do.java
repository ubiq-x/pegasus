
package main;

import inter.DBI;
import inter.DSI;
import inter.FSI;
import inter.HTTPI;
import inter.JSI;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.sql.SQLException;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import research.Research01;
import smile.Network;
import util.SQLConn;
import app.adm.Adm;
import app.data.DataSet;
import app.data.R;
import app.data.Rep;
import app.data.bn.BN;
import app.gaze.Exp;
import app.gaze.Stim;
import app.gaze.StimSeq;
import app.gaze.Var;
import app.gaze.aggr.Aggr;
import app.gaze.imp.Import;
import app.gaze.oper.Export;
import app.main.Prj;
import exception.ParamException;
import exception.PegasusException;
import exception.ValidException;


/**
 * @author Tomek D. Loboda
 */
@SuppressWarnings("serial")
@WebServlet(name="Pegasus", urlPatterns={"/do"})
public class Do extends HttpServlet {
	public static final String APP_ID_STR_ADM  = "adm";
	public static final String APP_ID_STR_DATA = "data";
	public static final String APP_ID_STR_GAZE = "gaze";
	public static final String APP_ID_STR_MAIN = "main";
	
	public static final String DB_MAIN    = "m";
	public static final String DB_DATA    = "d";
	public static final String DB_GAZE    = "g";
	public static final String DB_DATA_DS = "dds";
	
	public static final String URL_NGRAM_GOOGLE    = "http://localhost:10000";
	//public static final String URL_NGRAM_GOOGLE    = "http://lrdc-195.lrdc.pitt.edu:10000";
	public static final String URL_NGRAM_MICROSOFT = "http://weblm.research.microsoft.com/rest.svc/bing-body/2013-12";
	
	public static final int NGRAM_APP_TIMEOUT = 5;  // [seconds]
	
	public static final String MISS_VAL = ".";
	
	private static int appIdData = -1;
	private static int appIdGaze = -1;
	
	private static String pathApp;
	private static String pathBN;
	private static String pathDS;
	private static String pathRes;
	private static String pathWork;
	
	public static Hashtable<String, String> delim = new Hashtable<String, String>();
	
	public static DBCache dbCache = new DBCache();
	public static ResMan resMan = new ResMan();
	public static R r = null;
	
	public static SQLConn sqlConn = null;
	
	private final Hashtable<String, Integer> acc = new Hashtable<String, Integer>();  // general purpose accumulators
	
	
	// ---------------------------------------------------------------------------------
	public void    accSet(String name, int val) { acc.put(name, val); }
	public void    accRem(String name)          { if (acc.get(name) != null) acc.remove(name); }
	public Integer accGet(String name)          { return acc.get(name); }
	
	
	// ---------------------------------------------------------------------------------
	private void dispatch(HttpServletRequest req, HttpServletResponse res)  throws Exception {
		/*
		System.out.println("req (get)");
		for (Iterator<String> it = req.getParameterMap().keySet().iterator(); it.hasNext(); ) {
			String n = it.next();
			String v = req.getParameterMap().get(n)[0];
			System.out.println("  " + n + ":" + v);
		}
		System.out.println("req (post)");
		for (Enumeration<String> e = req.getParameterNames(); e.hasMoreElements(); ) {
			String n = e.nextElement();
			String v = req.getParameter(n);
			System.out.println("  " + n + ":" + v);
		}
		System.out.println();
		*/
		
		ReqCtx rc = new ReqCtx(req);
		
		String app = rc.getStr("app", true);
		if (app == null || app.length() == 0) {
			HTTPI.retTxt(res, "{outcome:false,msg:\"No application specified.\"}");
			return;
		}
		
		String cmd = rc.getStr("cmd", true);
		if (cmd == null || cmd.length() == 0) {
			HTTPI.retTxt(res, "{outcome:false,msg:\"No command specified.\"}");
			return;
		}
		
		String username = rc.getStr("username", true);
		DBI dbi = new DBI();
		
		if (app.equals(APP_ID_STR_DATA)) execCmdData(req, res, rc, dbi, username, cmd);
		else if (app.equals(APP_ID_STR_GAZE)) execCmdGaze(req, res, rc, dbi, username, cmd);
		else if (app.equals(APP_ID_STR_MAIN)) execCmdMain(req, res, rc, dbi, username, cmd);
		else if (app.equals(APP_ID_STR_ADM)) execCmdAdm(req, res, rc, dbi, username, cmd);
		else HTTPI.retTxt(res, "{outcome:false,msg:\"Unknown application '" + app + "'.\"}");
		
		dbi.destroy();
	}
	
	
	// ---------------------------------------------------------------------------------
	public void doGet(HttpServletRequest req, HttpServletResponse resp)  throws ServletException, IOException {
		try { dispatch(req, resp); }
		catch (Exception e) {
			if (e instanceof ParamException || e instanceof ValidException || e instanceof PegasusException) {
				HTTPI.retTxt(resp, "{outcome:false,msg:" + JSI.str2js(e.getMessage()) + "}");
			}
			else HTTPI.retStr(resp, e);
		}
		catch (java.lang.OutOfMemoryError e) {
			HTTPI.retTxt(resp, "{outcome:false,msg:" + JSI.str2js(e.getMessage()) + "}");
		}
	}
	
	
	// ---------------------------------------------------------------------------------
	public void doPost(HttpServletRequest req, HttpServletResponse resp)  throws IOException, ServletException { doGet(req, resp); }
	
	
	// ---------------------------------------------------------------------------------
	public static int getAppIdData() { return appIdData; }
	public static int getAppIdGaze() { return appIdGaze; }
	
	
	// ---------------------------------------------------------------------------------
	public static String getPathApp()  { return pathApp;  }
	public static String getPathBN()   { return pathBN;   }
	public static String getPathDS()   { return pathDS;   }
	public static String getPathRes()  { return pathRes; }
	public static String getPathWork() { return pathWork; }
	
	
	// ---------------------------------------------------------------------------------
	public void init() {
		DBI dbi;
		try {
			// Database:
			sqlConn = new SQLConn();
			dbi = new DBI();
			
			//Thread.currentThread().setPriority(Thread.NORM_PRIORITY - 2);
			
			// App IDs:
			appIdData = (int) DBI.getLong(dbi, "SELECT id FROM \"" + DB_MAIN + "\".app WHERE str_id = '" + APP_ID_STR_DATA + "'", "id");
			appIdGaze = (int) DBI.getLong(dbi, "SELECT id FROM \"" + DB_MAIN + "\".app WHERE str_id = '" + APP_ID_STR_GAZE + "'", "id");
			
			// Delimiters:
			delim.put("tab", "\t");
			delim.put("new-line", "\n");
			
			// Paths:
			pathApp  = this.getServletContext().getRealPath("") + File.separatorChar;
			pathBN   = pathApp + "bn"   + File.separatorChar;
			pathDS   = pathApp + "ds"   + File.separatorChar;
			pathRes  = pathApp + "res"  + File.separatorChar;
			pathWork = pathApp + "work" + File.separatorChar;
			
			// Clear the work dir:
			File dir = new File(pathWork);
			String[] N = dir.list();
			for (String n: N) {
				File f = new File(pathWork + File.separatorChar + n);
				if (f.isFile()) f.delete();
				else FSI.delDir(f, true);
			}
			
			// R:
			r = new R(dbi);
			
			// Spit out startup diagnostics:
			System.out.print("* PEGASUS init: mem-max: " + Runtime.getRuntime().maxMemory() / (1024*1024) + "MB, mem-free: " + Runtime.getRuntime().freeMemory() / (1024*1024) + "MB\n");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	// ---------------------------------------------------------------------------------
	private void execCmdAdm(HttpServletRequest req, HttpServletResponse res, ReqCtx rc, DBI dbi, String username, String cmd)  throws IOException, SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException, ParamException, ValidException  {
		     if (cmd.equals("ping"))                 { HTTPI.retTxt(res, "pong"); }
		
		else if (cmd.equals("tmp"))                  { Adm.tmp(dbi, username); }
		else if (cmd.equals("calc-trial-times"))     { HTTPI.retTxt(res, Adm.calcTrialTimes(dbi, username).getMsg(true)); }
		else if (cmd.equals("copy-trial-times"))     { HTTPI.retTxt(res, Adm.copyTrialTimes(dbi, username, rc.getInt("exp-id-src", true), rc.getInt("exp-id-dst", true)).getMsg(true)); }
		else if (cmd.equals("clear-trial-rec-time")) { HTTPI.retTxt(res, Adm.clearTrialRecTime(dbi, username, rc.getInt("exp-id", true)).getMsg(true)); }
		else if (cmd.equals("gen-roi-imgs"))         { HTTPI.retTxt(res, Adm.genROIImgs(dbi, username).getMsg(true)); }
		else if (cmd.equals("pos-tag-stim-txt"))     { HTTPI.retTxt(res, Adm.posTagStimTxt(dbi, username).getMsg(true)); }
		else if (cmd.equals("set-trial-rec-time"))   { HTTPI.retTxt(res, Adm.setTrialRecTime(dbi, username, rc.getInt("exp-id", true)).getMsg(true)); }
		
		// Unknown command:
		else { HTTPI.retTxt(res, "{outcome:false,msg:\"Unknown command '" + cmd + "'.\"}"); }
	}
	
	
	// ---------------------------------------------------------------------------------
	private void execCmdData(HttpServletRequest req, HttpServletResponse res, ReqCtx rc, DBI dbi, String username, String cmd)  throws Exception {
		// Misc:
		     if (cmd.equals("attr-set")) { HTTPI.retRes(res, DBI.attrSet(dbi, username, APP_ID_STR_DATA, rc.getStr("obj-name", true), rc.getStr("obj-human-name", true), rc.getInt("obj-id", true), rc.getStr("attr-name", true), rc.getStr("attr-val", true))); }
		else if (cmd.equals("img-get"))  { app.data.DB.getImg(res, dbi, username, rc.getStr("obj", true), rc.getInt("obj-id", true), rc.getStr("attr", true)); }
		
		// Bayesian network:
		else if (cmd.equals("bn-learn-struct")) {
			BN bn = new BN();
			HTTPI.retTxt(res, bn.learnStruct(username, dbi, rc.getStr("alg", true), rc.getInt("ds-id", true), rc.getStr("vars", true)));
		}
		
		else if (cmd.equals("bn-learn-params-dag")) {
			BN bn = new BN();
			
			Network net = new Network();
			net.readFile("D://net.xdsl");
			
			smile.learning.DataSet ds = new smile.learning.DataSet();
			StringBuilder sb = FSI.getStr("D:\\ds");
			int idxEndLn = sb.indexOf("\n");
			util.DataSet dsA = new util.DataSet(sb.substring(idxEndLn+1), sb.substring(0,idxEndLn), "\n", "\t");
			
			String[] V = {"R", "sacca", "saccd", "fd", "wlen", "wfrq"};
			for (int i = 0; i < V.length; i++) {
				String v = V[i];
				ds.addFloatVariable(v);
			}
			
			// (2.2) Add observations:
			for (int i = 0; i < dsA.rowCnt; i++) {
				ds.addEmptyRecord();
				for (int j = 0; j < V.length; j++) {
					String v = V[j];
					float val = new Float(dsA.val(i,v));
					ds.setFloat(j,i, val);
				}
			}
			
			HTTPI.retTxt(res, bn.learnParamsDAG(username, dbi, net, ds));
			
			net.writeFile("D://net2.xdsl");
		}
		
		else if (cmd.equals("bn-learn-params-pattern")) {
			BN bn = new BN();
			HTTPI.retTxt(res, bn.learnParamsPattern(username, dbi, rc.getInt("res-id-pattern", true), rc.getInt("res-id-dataset", true), rc.getStr("arcs", true)));
		}
		
		else if (cmd.equals("bn-update-beliefs")) {
			BN bn = new BN();
			HTTPI.retTxt(res, bn.updateBeliefs());
		}
		
		// Data set:
		else if (cmd.equals("ds-del"))      { HTTPI.retRes(res, DataSet.del(dbi, username, rc.getStr("ds-ids", true))); }
		else if (cmd.equals("ds-del-name")) { HTTPI.retRes(res, DataSet.delName(dbi, username, rc.getInt("prj-id", true), rc.getStr("ds-name", true))); }
		else if (cmd.equals("ds-del-var"))  { HTTPI.retRes(res, DataSet.delVar(dbi, username, rc.getInt("prj-id", true), rc.getStr("ds-name", true), rc.getStr("spec", true))); }
		else if (cmd.equals("ds-dup"))      { HTTPI.retRes(res, DataSet.dup(dbi, username, rc.getInt("prj-id", true), rc.getStr("ds-name-src", true), rc.getStr("ds-name-dst", true), rc.getStr("cond", false))); }
		else if (cmd.equals("ds-discret"))  { HTTPI.retRes(res, DataSet.discretize(dbi, username, rc.getInt("prj-id", true), rc.getStr("ds-name-src", true), rc.getStr("ds-name-dst", true), rc.getStr("ts-suffix", false), rc.getStr("spec", true))); }
		else if (cmd.equals("ds-exp"))      {
			String format   = rc.getStr("format", true);
			String filename = rc.getStr("filename", false);
			
			if (format.equalsIgnoreCase("txt") && filename == null) {
				Writer out = HTTPI.getWriter(res, "text/plain");
				DataSet.exp(dbi, username, rc.getInt("prj-id", true), rc.getStr("ds-name", true), filename, format, rc.getStr("delim-var", true), rc.getStr("delim-obs", true), rc.getStr("miss-val", true), rc.getBool("zip", true), out);
				out.close();
				res.flushBuffer();
			}
			else if (format.equalsIgnoreCase("html")) {
				Writer out = HTTPI.getWriter(res, "text/html");
				DataSet.exp(dbi, username, rc.getInt("prj-id", true), rc.getStr("ds-name", true), filename, format, rc.getStr("delim-var", true), rc.getStr("delim-obs", true), rc.getStr("miss-val", true), rc.getBool("zip", true), out);
				out.close();
				res.flushBuffer();
			}
			else if (format.equalsIgnoreCase("txt") && filename != null) {
				if ((new File(Do.getPathDS() + filename)).exists()) {
					HTTPI.retRes(res, new Result(false, "msg:\"A file with that name already exists. Please provide a different name.\""));
				}
				else {
					BufferedWriter out = new BufferedWriter(new FileWriter(pathDS + rc.getStr("filename", false)), FSI.BUF_SIZE);
					//FileWriter out = new FileWriter(pathDS + rc.getStr("filename", false));
					DataSet.exp(dbi, username, rc.getInt("prj-id", true), rc.getStr("ds-name", true), filename, format, rc.getStr("delim-var", true), rc.getStr("delim-obs", true), rc.getStr("miss-val", true), rc.getBool("zip", true), out);
					out.close();
					
					HTTPI.retRes(res, new Result(true, ""));
				}
			}
			else HTTPI.retRes(res, new Result(false, "Incorrect arguments"));
			
			/*
			StringBuffer cont = DataSet.exp(dbi, username, rc.getInt("ds-id", true), format, rc.getStr("delim-var", true), rc.getStr("delim-obs", true), rc.getStr("miss-val", true), rc.getStr("filename", false), rc.getBool("zip", true));
			
			if (format.equalsIgnoreCase("html")) HTTPI.retHTML(res, cont);
			else HTTPI.retTxt(res, cont);
			*/
		}
		else if (cmd.equals("ds-gen-basic-rep")) { HTTPI.retRes(res, Rep.genBasic(dbi, username, rc.getInt("ds-id", true))); }
		else if (cmd.equals("ds-get"))           { HTTPI.retTxt(res, "{outcome:true,ds:" + DataSet.get(dbi, username, rc.getInt("id", true)) + "}"); }
		else if (cmd.equals("ds-get-lst"))       { HTTPI.retTxt(res, "{outcome:true,ds:" + DataSet.getLst(dbi, username, rc.getInt("prj-id", true), null, rc.getInt("ds-id", false)) + "}"); }
		else if (cmd.equals("ds-imp"))           { HTTPI.retRes(res, DataSet.impTxt(dbi, username, rc.getInt("ds-id", true), rc.getStr("file", true), "\\t", rc.getStr("miss-val", true))); }
		else if (cmd.equals("ds-merge"))         { HTTPI.retRes(res, DataSet.merge(dbi, username, rc.getInt("prj-id", true), rc.getStr("ds-name", true), rc.getStr("idx-var-name", false), rc.getStr("spec", true))); }
		else if (cmd.equals("ds-new"))           { HTTPI.retRes(res, DataSet.addNew(dbi, username, rc.getInt("prj-id", true), appIdData, rc.getStr("name", true), null, null, 0, null)); }
		else if (cmd.equals("ds-rep-val"))       { HTTPI.retRes(res, DataSet.repVal(dbi, username, rc.getInt("prj-id", true), rc.getStr("ds-name-src", true), rc.getStr("ds-name-dst", true), rc.getStr("spec", true))); }
		else if (cmd.equals("ds-split"))         { HTTPI.retRes(res, DataSet.split(dbi, username, rc.getInt("ds-id", true), rc.getStr("spec", true))); }
		else if (cmd.equals("ds-gen-stats"))     { HTTPI.retRes(res, DataSet.genStats(dbi, username, rc.getInt("prj-id", true), rc.getStr("ds-name-src", true), rc.getStr("ds-name-dst", true), rc.getStr("spec", true), rc.getStr("sep", true))); }
		else if (cmd.equals("ds-ts-add-var"))    { HTTPI.retRes(res, DataSet.tsAddVar(dbi, username, rc.getInt("prj-id", true), rc.getStr("ds-name", true), rc.getStr("spec", true))); }
		else if (cmd.equals("ds-ts-2-wide"))     { HTTPI.retRes(res, DataSet.tsLong2Wide(dbi, username, rc.getInt("prj-id", true), rc.getStr("ds-name-src", true), rc.getStr("ds-name-dst", true), rc.getStr("ts-len-var", false), rc.getInt("idx-start", true), rc.getBool("idx-first", true), rc.getBool("idx-inc", true), rc.getStr("idx-pre", true))); }
		else if (cmd.equals("ds-var-reorder"))   { HTTPI.retRes(res, DataSet.varReorder(dbi, username, rc.getInt("ds-id", true), rc.getStr("spec", true))); }
		else if (cmd.equals("ds-var-trans"))     { HTTPI.retRes(res, DataSet.varTrans(dbi, username, rc.getInt("prj-id", true), rc.getStr("ds-name", true), rc.getStr("name", true), rc.getInt("type", true), rc.getStr("val", true))); }
		else if (cmd.equals("ds-var-rename"))    { HTTPI.retRes(res, DataSet.varRename(dbi, username, rc.getInt("prj-id", true), rc.getStr("ds-name", true), rc.getStr("var-name-src", true), rc.getStr("var-name-dst", true))); }
		     
		// R:
		else if (cmd.equals("r-int-del-hist")) { HTTPI.retRes(res, DataSet.rIntDel(dbi, username, rc.getInt("ds-id", true))); }
		else if (cmd.equals("r-int-exec"))     { HTTPI.retRes(res, r.exec(username, rc.getInt("ds-id", false), rc.getStr("code", true), true, true)); }
		else if (cmd.equals("r-int-get-hist")) { HTTPI.retTxt(res, DataSet.rIntGetHist(dbi, username, rc.getInt("ds-id", true))); }
		else if (cmd.equals("r-int-get-res"))  { HTTPI.retRes(res, DataSet.rIntGetRes(dbi, username, rc.getInt("r-int-id", true))); }
		else if (cmd.equals("r-scr-exec"))     { HTTPI.retRes(res, Rep.rScrExec(dbi, username, rc.getInt("ds-id", true), rc.getStr("rep-name", true), rc.getStr("code", true))); }
		
		// Report:
		else if (cmd.equals("rep-del"))      { HTTPI.retRes(res, Rep.del(dbi, username, rc.getInt("rep-id", true))); }
		else if (cmd.equals("rep-del-all"))  { HTTPI.retRes(res, Rep.delAll(dbi, username, rc.getInt("ds-id", true))); }
		else if (cmd.equals("rep-get-code")) { HTTPI.retTxt(res, Rep.getCode(dbi, username, rc.getInt("rep-id", true))); }
		else if (cmd.equals("rep-get-json")) { HTTPI.retRes(res, Rep.expJSON(dbi, username, rc.getInt("rep-id", true))); }
		
		else if (cmd.equals("rep-get-pdf")) {
			// TODO: change file to a db-retrieved object
			HTTPI.retFile(res, "", Rep.expPDF(dbi, username, rc.getInt("rep-id", true)), true);
		}
		
		else if (cmd.equals("rep-get-img")) {
			int imgId = rc.getInt("img-id", true);
			HTTPI.retFileDB(res, dbi, "SELECT content AS s FROM \"" + DB_DATA + "-" + username + "\".rep_item WHERE id = " + imgId, "image/png", "rep-img-" + imgId + ".png");
		}
		
		// Variable:
		else if (cmd.equals("var-del"))      { HTTPI.retRes(res, app.data.Var.del(dbi, username, rc.getInt("var-id", true))); }
		else if (cmd.equals("var-mark-ts"))  { HTTPI.retRes(res, app.data.Var.markTS(dbi, username, rc.getInt("var-id", true))); }
		else if (cmd.equals("var-set-type")) { HTTPI.retRes(res, app.data.Var.setType(dbi, username, rc.getInt("var-id", true), rc.getStr("new-name", false), rc.getInt("type", true))); }
		
		// Unknown command:
		else { HTTPI.retTxt(res, "{outcome:false,msg:\"Unknown command '" + cmd + "'.\"}"); }		
		
		/*
		else if (cmd.equals("ds-get-vars")) {
			//HTTPInterface.retStr(res, dbi.dsGetVars(db, rc.getInt("ds-id", true)));
			//HTTPInterface.retStr(res, "{outcome:true,vars:[\"R\",\"sub\",\"nrois\",\"t\",\"wpms\",\"nf\",\"nfpf\",\"nr\",\"nfpr\",\"nwf\",\"notf\",\"nosf\",\"afd\",\"ffd\",\"sfd\",\"gd\",\"tt\",\"p0s\",\"p1s\",\"p2s\",\"nlet\",\"nokffrq\",\"fam\",\"aoa\"]}");
			HTTPI.retStr(res, "{outcome:true,vars:[\"R\",\"sub\",\"t\",\"saccl\",\"saccd\",\"ot\",\"os\",\"wlen\",\"fd\",\"nlet\",\"kffrq\",\"fam\",\"aoa\"]}");
		}
		else if (cmd.equals("ds-open")) {
			DSI dsi = new DSI();
			HTTPI.retStr(res, dsi.dsOpen(rc.getStr("ds", true)));
		}
		else if (cmd.equals("ds-plot-dist")) {
			DSI dsi = new DSI();
			dsi.plotDist(rc.getStr("ds", true), rc.getStr("plot", true), rc.getStr("x1", ""), rc.getStr("x2", ""), rc.getStr("x3", ""), rc.getStr("type", true), rc.getStr("layout", true));
			HTTPI.retStr(res, "{outcome:true}");
		}
		else if (cmd.equals("ds-ren-val")) { HTTPI.retStr(res, dbi.renDSVal(username, rc.getInt("id", true), rc.getStr("new-name", true), rc.getBool("ts", true), rc.getStr("ts-suffix", false), rc.getStr("info", true))); }
		else if (cmd.equals("tmp")) {
			StringBuffer sb = new StringBuffer();
			ResultSet rs = dbi.execQry("SELECT COALESCE(to_char(\"wm-pu\", 'FM999999999990D999999999999'), '.') AS x FROM \"pegasus-data-ds-tomek\".ds_10;");
			while (rs.next()) sb.append(rs.getString("x") + "\n");
			rs.close();
			HTTPI.retStr(res, sb);
		}
		*/
	}
	
	
	// ---------------------------------------------------------------------------------
	private void execCmdGaze(HttpServletRequest req, HttpServletResponse res, ReqCtx rc, DBI dbi, String username, String cmd)  throws Exception {
		// Misc:
		     if (cmd.equals("attr-set"))       { HTTPI.retRes(res, DBI.attrSet(dbi, username, APP_ID_STR_GAZE, rc.getStr("obj-name", true), rc.getStr("obj-human-name", true), rc.getInt("obj-id", true), rc.getStr("attr-name", true), rc.getStr("attr-val", true))); }
		else if (cmd.equals("attr-set-quick")) { HTTPI.retRes(res, app.gaze.DB.attrSet(dbi, username, rc.getStr("obj", true), rc.getStr("attr", true), rc.getStr("val", true), rc.getInt("id", true))); }
		else if (cmd.equals("exc-obj-toggle")) { HTTPI.retRes(res, dbi.excObjToggle(username, rc.getStr("obj-name", true), rc.getInt("obj-id", true), rc.getStr("exc-info", false))); }
		else if (cmd.equals("img-get"))        { app.gaze.DB.getImg(res, dbi, username, rc.getStr("obj", true), rc.getInt("obj-id", true), rc.getStr("attr", true)); }
		else if (cmd.equals("ping"))           { HTTPI.retTxt(res, "{outcome:true,msg:\"pong\"}"); }
		
		// Analysis:
		else if (cmd.equals("an-calc-aggr")) {
			Aggr aggr = new Aggr(dbi);
			HTTPI.retTxt(res, aggr.calc(username, rc.getInt("prj-id", true), rc.getStr("unit", true), rc.getBool("adj", true), rc.getInt("exp-id", true), rc.getStr("ids-sub", false), rc.getStr("ids-trial", false), rc.getStr("ids-stim", false), rc.getStr("ids-roi", false), rc.getStr("vars-sub", false), rc.getStr("vars-trial", false), rc.getStr("vars-stim", false), rc.getStr("vars-roi", false), rc.getInt("trial-cal", true), rc.getInt("trial-val", true), rc.getInt("roi-max-dist", true), rc.getStr("roi-exp", true), rc.getStr("evts", false), rc.getLong("t-exp-0", false), rc.getLong("t-exp-1", false), rc.getLong("t-trial-0", false), rc.getLong("t-trial-1", false), rc.getLong("t-rec-0", false), rc.getLong("t-rec-1", false), rc.getInt("fix-dur-0", false), rc.getInt("fix-dur-1", false), rc.getStr("ds-name", true), rc.getStr("vars", false), rc.getStr("cons", false), rc.getQry()).getMsg(true));
		}
		
		// Data set:
		else if (cmd.equals("ds-del"))     { HTTPI.retRes(res, DataSet.del(dbi, username, rc.getStr("ds-id", true))); }
		else if (cmd.equals("ds-del-all")) { HTTPI.retRes(res, dbi.delAllDS(username, rc.getInt("exp-id", true))); }
		else if (cmd.equals("ds-exp"))     {
			String format   = rc.getStr("format", true);
			String filename = rc.getStr("filename", false);
			
			if (format.equalsIgnoreCase("txt") && filename == null) {
				Writer out = HTTPI.getWriter(res, "text/plain");
				DataSet.exp(dbi, username, rc.getInt("prj-id", true), rc.getStr("ds-name", true), filename, format, rc.getStr("delim-var", true), rc.getStr("delim-obs", true), rc.getStr("miss-val", true), rc.getBool("zip", true), out);
				out.close();
				res.flushBuffer();
			}
			else if (format.equalsIgnoreCase("html")) {
				Writer out = HTTPI.getWriter(res, "text/html");
				DataSet.exp(dbi, username, rc.getInt("prj-id", true), rc.getStr("ds-name", true), filename, format, rc.getStr("delim-var", true), rc.getStr("delim-obs", true), rc.getStr("miss-val", true), rc.getBool("zip", true), out);
				out.close();
				res.flushBuffer();
			}
			else if (format.equalsIgnoreCase("txt") && filename != null) {
				if ((new File(Do.getPathDS() + filename)).exists()) {
					HTTPI.retRes(res, new Result(false, "msg:\"A file with that name already exists. Please provide a different name.\""));
				}
				else {
					BufferedWriter out = new BufferedWriter(new FileWriter(pathDS + rc.getStr("filename", false)), FSI.BUF_SIZE);
					DataSet.exp(dbi, username, rc.getInt("prj-id", true), rc.getStr("ds-name", true), filename, format, rc.getStr("delim-var", true), rc.getStr("delim-obs", true), rc.getStr("miss-val", true), rc.getBool("zip", true), out);
					out.close();
					
					HTTPI.retRes(res, new Result(true, ""));
				}
			}
			else HTTPI.retRes(res, new Result(false, "Incorrect arguments"));
			
			/*
			StringBuffer cont = DataSet.exp(dbi, username, rc.getInt("ds-id", true), format, rc.getStr("delim-var", true), rc.getStr("delim-obs", true), rc.getStr("miss-val", true), rc.getStr("filename", false), rc.getBool("zip", true));
			
			if (format.equalsIgnoreCase("html")) HTTPI.retHTML(res, cont);
			else HTTPI.retTxt(res, cont);
			*/
		}
		else if (cmd.equals("ds-attr-set"))        { HTTPI.retRes(res, Exp.dsAttrSet(dbi, username, rc.getInt("ds-id", true), rc.getStr("attr-name", true), rc.getStr("attr-val", true))); }
		else if (cmd.equals("ds-gen-spreadsheet")) { HTTPI.retFile(res, "application/vnd.ms-excel", dbi.genSpreadsheet(username, rc.getStr("ds-ids", true)), false); }
		
		else if (cmd.equals("ds-xform-to-ts")) {
			DSI dsi = new DSI();
			HTTPI.retTxt(res, dsi.xform2TS());
		}
		
		// Experiment:
		else if (cmd.equals("exp-adj-ems"))            { HTTPI.retRes(res, Exp.adjEMs(dbi, username, rc.getInt("exp-id", true), rc.getStr("ss-ids", false))); }
		else if (cmd.equals("exp-del"))                { HTTPI.retRes(res, Exp.del(dbi, username, rc.getInt("exp-id", true))); }
		else if (cmd.equals("exp-get"))                { HTTPI.retTxt(res, "{outcome:true,exp:" + Exp.get(dbi, username, rc.getInt("id", true)) + "}"); }
		else if (cmd.equals("exp-get-exc-rep"))        { HTTPI.retTxt(res, "{outcome:true,rep:" + JSI.str2js(Exp.getExcRep(dbi, username, rc.getInt("exp-id", true))) + "}"); }
		else if (cmd.equals("exp-get-lst"))            { HTTPI.retTxt(res, Exp.getLst(dbi, username, rc.getInt("prj-id", true), rc.getInt("exp-id", false)).getMsg(true)); }
		else if (cmd.equals("exp-get-imp-rep"))        { HTTPI.retTxt(res, "{outcome:true,impRep:" + JSI.str2js(DBI.getStr(dbi, "SELECT imp_rep FROM \"" + DB_GAZE + "-" + username + "\".exp WHERE id = " + rc.getInt("exp-id", true), "imp_rep")) + "}"); }
		else if (cmd.equals("exp-gen-ngram-in-files")) { HTTPI.retRes(res, Exp.genNgramInFiles(dbi, username, rc.getInt("exp-id", true))); }
		else if (cmd.equals("exp-get-stats"))          { HTTPI.retRes(res, Exp.getStats(dbi, username, rc.getInt("exp-id", true))); }
		else if (cmd.equals("exp-set-word-pred"))      { HTTPI.retRes(res, Exp.setWordPred(dbi, username, rc.getInt("exp-id", true))); }
		else if (cmd.equals("exp-lnk"))                { HTTPI.retRes(res, Exp.lnk(dbi, username, rc.getInt("exp-id", true))); }
		else if (cmd.equals("exp-new"))                { HTTPI.retRes(res, Exp.addNew(dbi, username, rc.getInt("prj-id", true), rc.getStr("name", true))); }
		
		else if (cmd.equals("exp-export")) {
			Export e = new Export(username, rc.getInt("exp-id", true));
			HTTPI.retZIP(res, e.getDir(), e.getFilename(), false);
		}
		
		else if (cmd.equals("exp-import")) {
			Import i = new Import(username, rc.getInt("exp-id", true), rc.getStr("filename", true));
			HTTPI.retTxt(res, i.run());
		}
		
		// Stimulus:
		else if (cmd.equals("stim-upd"))           { HTTPI.retRes(res, Stim.updateStim(dbi, username, rc.getInt("id", true), rc.getInt("font-size", true), rc.getFloat("letter-spac", true), rc.getInt("line-h", true), rc.getStr("align-h", true), rc.getStr("align-v", true), rc.getInt("margin-t", true), rc.getInt("margin-l", true), rc.getInt("margin-r", true), rc.getInt("padding-t", true), rc.getInt("padding-b", true), rc.getInt("roi-h", true), rc.getStr("roi-exp", true), rc.getInt("offset-x", true), rc.getInt("offset-y", true), rc.getStr("rois", false))); }
		else if (cmd.equals("stim-get-rois"))      { HTTPI.retRes(res, Stim.getROIs(dbi, username, rc.getInt("stim-id", true))); }
		else if (cmd.equals("stim-get-word-pred")) { HTTPI.retRes(res, Stim.getWordPred(dbi, username, rc.getInt("stim-id", true))); }
		else if (cmd.equals("stim-set-word-pred")) { HTTPI.retRes(res, Stim.setWordPred(dbi, username, rc.getInt("stim-id", true))); }
		
		// Stimulus sequence:
		else if (cmd.equals("stim-seq-exc-first-n-fix")) { HTTPI.retTxt(res, dbi.excFirstNFix(username, rc.getInt("trial-id", true), rc.getInt("stim-id", true), rc.getInt("stim-seq-id", true), rc.getInt("t0", true), rc.getInt("t1", true), rc.getInt("n", true))); }
		else if (cmd.equals("stim-seq-get-em-data"))     { HTTPI.retTxt(res, DBI.getEMData(dbi, username, rc.getInt("stim-seq-id", true))); }
		else if (cmd.equals("stim-seq-xform"))           { HTTPI.retTxt(res, StimSeq.xform(dbi, username, rc.getInt("stim-seq-id", true), rc.getInt("tx", true), rc.getInt("ty", true), rc.getFloat("sx", true), rc.getFloat("sy", true), rc.getFloat("a", true))); }
		
		// Variable:
		else if (cmd.equals("var-add"))        { HTTPI.retRes(res, Var.add(dbi, username, rc.getInt("exp-id", true), rc.getStr("obj", true), rc.getStr("type", false), rc.getStr("name", true))); }
		else if (cmd.equals("var-del"))        { HTTPI.retRes(res, Var.del(dbi, username, rc.getInt("id", true))); }
		else if (cmd.equals("var-gen"))        { HTTPI.retRes(res, Var.gen(dbi, username, rc.getInt("exp-id", true), rc.getStr("var", true), rc.getStr("name", true))); }
		else if (cmd.equals("var-mutate-roi")) { HTTPI.retRes(res, Var.mutateROIVar(dbi, username, rc.getInt("id", true), rc.getStr("name", true), rc.getStr("method", true))); }
		else if (cmd.equals("var-set"))        { HTTPI.retTxt(res, dbi.setVar(username, rc.getStr("obj-name", true), rc.getInt("obj-id", true), rc.getInt("var-id", true), rc.getStr("val", false))); }
		
		// Research:
		/*
		else if (cmd.equals("research-01")) {
			Research01 exp = new Research01();
			HTTPI.retTxt(res, exp.run(rc.getStr("net-name", true), rc.getStr("ds-name", true), rc.getStr("ev-vars", true), rc.getInt("t-max", true)));
		}
		*/
		
		/*
		// Test:
		else if (cmd.equals("test")) {
			
		}
		*/
		
		// Unknown command:
		else { HTTPI.retTxt(res, "{outcome:false,msg:\"Unknown command '" + cmd + "'.\"}"); }
		
		
		/*
		else if (cmd.equals("app-complement-vars")) { HTTPI.retStr(res, dbi.appComplementVars(username, rc.getInt("exp-id", true))); }
		else if (cmd.equals("tmp")) {
			String[] A = {"Yo ", "snake", "!"};
			tmp(A);
			HTTPInterface.retStr(res, util.Util.join(A, "").toString());
		}
		*/
	}
	
	
	// ---------------------------------------------------------------------------------
	private void execCmdMain(HttpServletRequest req, HttpServletResponse res, ReqCtx rc, DBI dbi, String username, String cmd)  throws Exception {
		// Misc:
		     if (cmd.equals("attr-set"))    { HTTPI.retRes(res, DBI.attrSet(dbi, username, APP_ID_STR_MAIN, rc.getStr("obj-name", true), rc.getStr("obj-human-name", true), rc.getInt("obj-id", true), rc.getStr("attr-name", true), rc.getStr("attr-val", true))); }
		else if (cmd.equals("login"))       { HTTPI.retTxt(res, dbi.login(username, rc.getStr("passwd", true), rc.getBool("pseudo", false), rc.getInt("exp-id", false))); }
		else if (cmd.equals("profile-get")) { HTTPI.retTxt(res, dbi.getProfile(username)); }
		
		// Project:
		else if (cmd.equals("prj-del")) { HTTPI.retRes(res, Prj.del(dbi, username, rc.getInt("prj-id", true))); }
		else if (cmd.equals("prj-new")) { HTTPI.retRes(res, Prj.addNew(dbi, username, rc.getStr("name", true))); }
		
		// Unknown command:
		else { HTTPI.retTxt(res, "{outcome:false,msg:\"Unknown command '" + cmd + "'.\"}"); }
	}
	
	
	// ---------------------------------------------------------------------------------
	// unit:
	//sqlConn.get().close();
}
