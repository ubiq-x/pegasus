package main;

import inter.HTTPI;

import java.io.UnsupportedEncodingException;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import exception.ParamException;


/**
 * @author Tomek D. Loboda
 */
public class ReqCtx {
	private final HttpServletRequest req;
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * C'tor.
	 */
	public ReqCtx(HttpServletRequest req) {
		this.req = req;
	}
	
	
	// ---------------------------------------------------------------------------------
	public boolean getBool(String name, boolean required)  throws UnsupportedEncodingException, ParamException {
		String s = HTTPI.getParamStr(req, name);
		if (required && (s == null || s.length() == 0)) throw new ParamException("'" + name + "' parameter cannot be empty");
		return (s == null || !s.equals("1") ? false : true);
	}
	
	
	// ---------------------------------------------------------------------------------
	public Float getFloat(String name, boolean required)  throws UnsupportedEncodingException, ParamException {
		String s = HTTPI.getParamStr(req, name);
		if (required && (s == null || s.length() == 0)) throw new ParamException("'" + name + "' parameter cannot be empty");
		return (s == null || s.length() == 0 ? null : new Float(s));
	}
	
	
	// ---------------------------------------------------------------------------------
	public Integer getInt(String name, boolean required)  throws UnsupportedEncodingException, ParamException {
		String s = HTTPI.getParamStr(req, name);
		if (required && (s == null || s.length() == 0)) throw new ParamException("'" + name + "' parameter cannot be empty");
		return (s == null || s.length() == 0 ? null : new Integer(s));
	}
	
	
	// ---------------------------------------------------------------------------------
	public Long getLong(String name, boolean required)  throws UnsupportedEncodingException, ParamException {
		String s = HTTPI.getParamStr(req, name);
		if (required && (s == null || s.length() == 0)) throw new ParamException("'" + name + "' parameter cannot be empty");
		return (s == null || s.length() == 0 ? null : new Long(s));
	}
	
	
	// ---------------------------------------------------------------------------------
	public String getStr(String name, boolean required)  throws UnsupportedEncodingException, ParamException {
		String s = HTTPI.getParamStr(req, name);
		if (required && (s == null || s.length() == 0)) throw new ParamException("'" + name + "' parameter cannot be empty");
		return s;
	}
	
	
	// ---------------------------------------------------------------------------------
	public String getStr(String name, String defVal)  throws UnsupportedEncodingException, ParamException {
		String s = HTTPI.getParamStr(req, name, defVal);
		return s;
	}
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * Reconstructs the query received by the servlet.
	 */
	@SuppressWarnings("unchecked")
	public String getQry() {
		StringBuilder sb = new StringBuilder();
		Enumeration<String> en = req.getParameterNames();
		while (en.hasMoreElements()) {
			String p = en.nextElement();
			sb.append(p + "=" + req.getParameter(p) + (en.hasMoreElements() ? "&" : ""));
		}
		return sb.toString();
	}
}
