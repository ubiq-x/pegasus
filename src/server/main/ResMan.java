package main;

import java.util.Vector;


/**
 * @author Tomek D. Loboda
 */
public class ResMan {
	private Vector<Object> res = new Vector<Object>();
	
	
	// ---------------------------------------------------------------------------------
	public ResMan() {}
	
	
	// ---------------------------------------------------------------------------------
	public Object get(int id, boolean remove) {
		Object o = res.get(id);
		if (remove) remove(id);
		return o;
	}
	
	
	// ---------------------------------------------------------------------------------
	public int put(Object o) {
		res.add(o);
		return res.size() - 1;
	}
	
	
	// ---------------------------------------------------------------------------------
	public void remove(int id) { res.set(id, null); }
}
