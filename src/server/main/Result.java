package main;


/**
 * @author Tomek D. Loboda
 */
public class Result {
	public final boolean ok;
	public final StringBuffer msg;
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Result(boolean ok, StringBuffer msg) {
		this.ok = ok;
		this.msg = msg;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Result(boolean ok, String msg) {
		this.ok = ok;
		this.msg = new StringBuffer(msg);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public StringBuffer getMsg(boolean js) {
		return 
			(js
				? new StringBuffer("{outcome:" + ok + (msg.length() > 0 ? "," + msg : "") + "}")
				: new StringBuffer(msg)
			);
	}
}
