package main;

import inter.DBI;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import app.gaze.dstruct.Word;



/**
 * Loads the performance-critical DB stuff into the memory and stores it in a ready-to-be-used 
 * format.
 * 
 * @author Tomek D. Loboda
 */
public class DBCache {
	private final boolean debug = false;
	private Map<String,Word> mapWord = null;
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * C'tor.
	 */
	public DBCache() {}
	
	
	// ---------------------------------------------------------------------------------
	public Map<String,Word> getWords()  throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		if (mapWord == null) {
			if (!debug) {
				DBI dbi = new DBI();
				mapWord = dbi.getWords();
			}
			else mapWord = new HashMap<String,Word>();
		}
		return mapWord;
	}
}
