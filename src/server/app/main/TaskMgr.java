package app.main;

import inter.DBI;

import java.sql.SQLException;
import java.util.HashMap;

import main.Result;


/**
 * A manager for tasks scheduled for execution.
 * 
 * This class provides a way of keeping track of tasks at any point of their life cycle (scheduled, in progress, 
 * finished).
 * 
 * @author Tomek D. Loboda
 */
public class TaskMgr {
	private final DBI dbi;
	private HashMap<Integer, Task> T = new HashMap<Integer, Task>();
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public TaskMgr()  throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		dbi = new DBI();
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Result getTaskLst() {
		StringBuffer sb = new StringBuffer();
		
		return new Result(true, sb);
	}
}
