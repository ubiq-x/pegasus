package app.main;

import inter.DBI;
import inter.JSI;

import java.io.UnsupportedEncodingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import main.Do;
import main.Result;


/**
 * @author Tomek D. Loboda
 */
public class Prj {
	
	// -----------------------------------------------------------------------------------------------------------------
	public static Result addNew(DBI dbi, String username, String name) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException, UnsupportedEncodingException {
		String dbM = "\"" + Do.DB_MAIN + "\"";
		int prjId = DBI.ID_NONE;
		
		Statement stX = (new DBI()).transBegin();
		try {
			prjId = DBI.insertGetId(stX, null, "INSERT INTO " + dbM + ".prj (user_id, name) VALUES ((SELECT id FROM " + dbM + ".user WHERE username = " + DBI.esc(username) + "), " + DBI.esc(name) + ")");
			DBI.transCommit(stX, true);
		}
		catch (SQLException e) {
			DBI.transRollback(stX, true);
			
			if (e.getErrorCode() == DBI.SQL_ERR_DUP_ENTRY) return new Result(false, "msg:\"A peoject with that name already exists. Project names must be unique.\"");
			else throw e;
		}
		return new Result(true, "prj:" + get(dbi, username, prjId));
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static Result del(DBI dbi, String username, int prjId)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		dbi.execUpd("DELETE FROM \"" + Do.DB_MAIN + "\".prj WHERE id = " + prjId);
		return new Result(true, "");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	@SuppressWarnings("deprecation")
	public static StringBuffer get(DBI dbi, String username, int prjId)  throws SQLException, UnsupportedEncodingException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		String dbM = "\"" + Do.DB_MAIN + "\"";
		StringBuffer res = new StringBuffer();
		
		// (1) Data set:
		ResultSet rs = dbi.execQry("SELECT p.id, p.name, p.memo, p.created, p.note, LENGTH(p.note) as note_len FROM " + dbM + ".prj p WHERE id = " + prjId);
		
		rs.next();
		res.append(
			"{" +
			"id:" + rs.getInt("id") + "," +
			"name:" + JSI.str2js(rs.getString("name")) + "," +
			"memo:" + JSI.str2js(rs.getString("memo")) + "," +
			"created:" + (rs.getTimestamp("created").getYear() == 0 ? "null" : "new Date(" + rs.getTimestamp("created").getTime() + ")") + "," +
			"note:" + JSI.str2js(rs.getString("note")) + "," +
			"noteLen:" + rs.getInt("note_len") +
			"}" + (!rs.isLast() ? "," : "")
		);
		rs.close();
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	@SuppressWarnings("deprecation")
	public static StringBuffer getLst(DBI dbi, String username)  throws SQLException, UnsupportedEncodingException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		String dbM = "\"" + Do.DB_MAIN + "\"";
		StringBuffer res = new StringBuffer("[");
		
		ResultSet rs = dbi.execQry(
			"SELECT p.id, p.name, p.memo, p.created, p.note, LENGTH(p.note) as note_len FROM " + dbM + ".prj p " +
			"INNER JOIN " + dbM + ".user u ON p.user_id = u.id " +
			"WHERE u.username = " + DBI.esc(username) + " " +
			"ORDER BY p.name"
		);
		
		while (rs.next()) {
			res.append(
				"{" +
				"id:" + rs.getInt("id") + "," +
				"name:" + JSI.str2js(rs.getString("name")) + "," +
				"memo:" + JSI.str2js(rs.getString("memo")) + "," +
				"created:" + (rs.getTimestamp("created").getYear() == 0 ? "null" : "new Date(" + rs.getTimestamp("created").getTime() + ")") + "," +
				"note:" + JSI.str2js(rs.getString("note")) + "," +
				"noteLen:" + rs.getInt("note_len") +
				"}" + (!rs.isLast() ? "," : "")
			);
		}
		rs.close();
		res.append("]");
		
		return res;
	}
}
