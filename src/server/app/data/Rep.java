package app.data;

import inter.DBI;
import inter.FSI;
import inter.JSI;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import main.Do;
import main.Result;

import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;

import util.$;
import util.Convert;


/**
 * @author Tomek D. Loboda
 */
public class Rep {
	public static final int ITEM_TYPE_TXT   = 1;
	public static final int ITEM_TYPE_IMG   = 2;
	public static final int ITEM_TYPE_SEC   = 3;
	public static final int ITEM_TYPE_SSEC  = 4;
	public static final int ITEM_TYPE_SSSEC = 5;
	
	public static final int STAT_DESC_PROB_MAX_VAL_CNT = 15;
	
	private static final String MIN_P_VAL = "0.0001";
	
	private static final String R_SCR_SEC = "REPORT SEC:";
	private static final String R_SCR_SSEC = "REPORT SUB-SEC:";
	private static final String R_SCR_SSSEC = "REPORT SUB-SUB-SEC:";
	private static final String R_SCR_TXT = "REPORT TXT:";
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Rep() {}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static Result del(DBI dbi, String username, int repId)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		String dbData = "\"" + Do.DB_DATA + "-" + username + "\"";
		
		int repSize = (int) DBI.getLong(dbi, "SELECT size FROM " + dbData + ".rep WHERE id = " + repId, "size");
		int dsId = (int) DBI.getLong(dbi, "SELECT ds_id FROM " + dbData + ".rep WHERE id = " + repId, "ds_id");
		
		Statement stX = (new DBI()).transBegin();
		try {
			stX.executeUpdate("DELETE FROM " + dbData + ".rep WHERE id = " + repId);
			stX.executeUpdate("UPDATE " + dbData + ".ds SET size = (size - " + repSize + ") WHERE id = " + dsId);
			DBI.transCommit(stX, true);
		}
		catch (SQLException e) {
			DBI.transRollback(stX, true);
			throw e;
		}
		
		return new Result(true, "size:{ds:" + DataSet.getSize(dbi, username, dsId) + "}");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static Result delAll(DBI dbi, String username, int dsId)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		String dbData = "\"" + Do.DB_DATA + "-" + username + "\"";
		
		int repSize = (int) DBI.getLong(dbi, "SELECT SUM(size) AS size FROM " + dbData + ".rep WHERE ds_id = " + dsId, "size");
		
		Statement stX = (new DBI()).transBegin();
		try {
			stX.executeUpdate("DELETE FROM \"" + Do.DB_DATA + "-" + username + "\".rep WHERE ds_id = " + dsId);
			stX.executeUpdate("UPDATE " + dbData + ".ds SET size = (size - " + repSize + ") WHERE id = " + dsId);
			DBI.transCommit(stX, true);
		}
		catch (SQLException e) {
			DBI.transRollback(stX, true);
			throw e;
		}
		
		return new Result(true, "size:{ds:" + DataSet.getSize(dbi, username, dsId) + "}");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static Result expJSON(DBI dbi, String username, int repId)  throws SQLException {
		String db = "\"" + Do.DB_DATA + "-" + username + "\"";
		
		StringBuffer res = new StringBuffer("rep:{info:" + get(dbi, username, repId) + ",items:[");
		
		ResultSet rs = dbi.execQry("SELECT id, type_id, content FROM " + db + ".rep_item WHERE rep_id = " + repId + " ORDER BY id");
		while (rs.next()) {
			int typeId = rs.getInt("type_id");
			res.append("{type:" + typeId + ",");
			switch (typeId) {
				case ITEM_TYPE_SEC:
				case ITEM_TYPE_SSEC:
				case ITEM_TYPE_SSSEC:
				case ITEM_TYPE_TXT:
					res.append("txt:" + JSI.str2js(new String(rs.getBytes("content"))));
					break;
				case ITEM_TYPE_IMG:
					res.append("id:" + rs.getInt("id"));
					break;
			}
			res.append("}" + (!rs.isLast() ? "," : ""));
		}
		rs.close();
		res.append("]}");
		
		return new Result(true, res);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static String expPDF(DBI dbi, String username, int repId)  throws SQLException {
		return "";
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Generates a basic report for the data set specified.
	 * 
	 * - http://rserve.sourcearchive.com/documentation/0.5-1/clients_2java-new_2Rserve_2test_2PlotDemo_8java-source.html
	 */
	public static Result genBasic(DBI dbi, String username, int dsId)  throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException, RserveException, REXPMismatchException, IOException {
		util.Timer timer = new util.Timer();
		
		String dbData   = "\"" + Do.DB_DATA + "-" + username + "\"";
		String dbDataDS = "\"" + Do.DB_DATA_DS + "-" + username + "\"";
		
		int obsCnt = (int) DBI.getLong(dbi, "SELECT COUNT(*) AS cnt FROM " + dbDataDS + ".ds_" + dsId, "cnt");
		int repId = DBI.ID_NONE;
		int dbSize = DBI.REC_SIZE_REP;
		
		File tmpDir = FSI.getTmpWorkDir(username + "-rep-basic-", "");
		RConnection rconn = Do.r.getConn();
		
		DBI dbiX = new DBI();
		Statement stX = dbiX.transBegin();
		try {
			repId = DBI.insertGetId(stX, null, "INSERT INTO " + dbData + ".rep (ds_id, name) VALUES (" + dsId + ",'Basic')");
			
			PreparedStatement psInsItem = dbiX.getConn().get().prepareStatement("INSERT INTO " + dbData + ".rep_item (rep_id, type_id, content, meta, size) VALUES (" + repId + ", ?, ?, ?, ?)");
			
			ResultSet rsVars = dbi.execQry("SELECT id, type_id, name FROM " + dbData + ".var WHERE ds_id = " + dsId + " ORDER BY ord");
			while (rsVars.next()) {
				String varName = rsVars.getString("name");
				int varId = rsVars.getInt("id");
				int varType = rsVars.getInt("type_id");
				
				dbSize += itemAddTxt(psInsItem, ITEM_TYPE_SEC, varName);
				
				Var.calcStats(dbi, username, dsId, varId, varName, false);
				Do.r.loadVar(username, "pegasus.tmp.var", dsId, varName);
				
				ResultSet rsStats = dbi.execQry(
					"SELECT " +
					"COALESCE(CASE WHEN stat_desc_min  = 0 THEN '0' ELSE trim(trailing '.0' from stat_desc_min::text)  END, '-') AS stat_desc_min, " +
					"COALESCE(CASE WHEN stat_desc_max  = 0 THEN '0' ELSE trim(trailing '.0' from stat_desc_max::text)  END, '-') AS stat_desc_max, " +
					"COALESCE(CASE WHEN stat_desc_mean = 0 THEN '0' ELSE trim(trailing '.0' from stat_desc_mean::text) END, '-') AS stat_desc_mean, " +
					"COALESCE(CASE WHEN stat_desc_med  = 0 THEN '0' ELSE trim(trailing '.0' from stat_desc_med::text)  END, '-') AS stat_desc_med, " +
					"COALESCE(CASE WHEN stat_desc_sd   = 0 THEN '0' ELSE trim(trailing '.0' from stat_desc_sd::text)   END, '-') AS stat_desc_sd, " +
					"stat_norm_ad_s, stat_norm_ad_p, " +
					"stat_norm_cvm_s, stat_norm_cvm_p, " +
					"stat_norm_ks_s, stat_norm_ks_p, " +
					"stat_norm_sw_s, stat_norm_sw_p, " +
					"stat_misc_miss_val_cnt " +
					"FROM " + dbData + ".var WHERE id = " + varId
				);
				rsStats.next();
				
				// (1) Descriptives:
				int missValCnt = rsStats.getInt("stat_misc_miss_val_cnt");
				
				dbSize += itemAddTxt(psInsItem, ITEM_TYPE_SSEC, "Descriptives");
				if (missValCnt == obsCnt) {
					dbSize += itemAddTxt(psInsItem, ITEM_TYPE_TXT,
						"Missing : " + missValCnt + " (" + (missValCnt / obsCnt * 100) + "%)"
					);
				}
				else {
					switch (varType) {
						case Var.TYPE_BIN:
						case Var.TYPE_CAT:
						case Var.TYPE_ORD:
							dbSize += itemAddTxt(psInsItem, ITEM_TYPE_TXT,
								"Mean    : " + $.ifNull(rsStats.getString("stat_desc_mean"), ".") + "\n" + 
								"Missing : " + missValCnt + " (" + (missValCnt / obsCnt * 100) + "%)"
							);
							
							// Probabilities:
							StringBuilder prob = new StringBuilder();
							ResultSet rsValCnt = dbi.execQry(ResultSet.TYPE_SCROLL_INSENSITIVE, "SELECT DISTINCT \"" + varName + "\" AS val, COUNT(*) AS cnt FROM " + dbDataDS + ".ds_" + dsId + " GROUP BY val ORDER BY val");
							if (DBI.getRecCnt(rsValCnt) <= STAT_DESC_PROB_MAX_VAL_CNT) {
								dbSize += itemAddTxt(psInsItem, ITEM_TYPE_SSSEC, "Counts (probabilities)");
								while (rsValCnt.next()) {
									int cnt = rsValCnt.getInt("cnt");
									prob.append(rsValCnt.getString("val") + ": " + cnt + " (" + ((double) Math.round(((double) cnt / (double) obsCnt) * 100) / 100) + ")\n");
								}
							}
							dbSize += itemAddTxt(psInsItem, ITEM_TYPE_TXT, prob.toString());
							
							break;
						
						case Var.TYPE_STR:
							dbSize += itemAddTxt(psInsItem, ITEM_TYPE_TXT,
								"Missing : " + missValCnt + " (" + (missValCnt / obsCnt * 100) + "%)"
							);
							break;
						
						default:
							dbSize += itemAddTxt(psInsItem, ITEM_TYPE_TXT,
								//"Min     : " + $.ifNull(rsStats.getString("stat_desc_min"),  ".") + "\n" + 
								"Min     : " + rsStats.getString("stat_desc_min") + "\n" + 
								"Max     : " + $.ifNull(rsStats.getString("stat_desc_max"),  ".") + "\n" + 
								"Mean    : " + $.ifNull(rsStats.getString("stat_desc_mean"), ".") + "\n" + 
								"Median  : " + $.ifNull(rsStats.getString("stat_desc_med"),  ".") + "\n" + 
								"SD      : " + $.ifNull(rsStats.getString("stat_desc_sd"),   ".") + "\n" +
								"Missing : " + missValCnt + " (" + (missValCnt / obsCnt * 100) + "%)"
							);
					}
				}
				
				// (2) Normality:
				if (missValCnt < obsCnt) {
					switch (varType) {
						case Var.TYPE_BIN:
							break;
						
						case Var.TYPE_STR:
							break;
						
						default:
							dbSize += itemAddTxt(psInsItem, ITEM_TYPE_SSEC, "Normality");
							
							dbSize += itemAddTxt(psInsItem, ITEM_TYPE_SSSEC, "Anderson-Darling");
							dbSize += itemAddTxt(psInsItem, ITEM_TYPE_TXT,
								"Statistic : " + rsStats.getString("stat_norm_ad_s") + "\n" +
								"p         : " + (rsStats.getFloat("stat_norm_ad_p") == 0 ? "< " + MIN_P_VAL : rsStats.getString("stat_norm_ad_p")) + "\n" +
								"Normal    : " + (rsStats.getFloat("stat_norm_ad_p") < 0.05 ? "no" : "yes") 
							);
							
							dbSize += itemAddTxt(psInsItem, ITEM_TYPE_SSSEC, "Cramer-von-Mises");
							dbSize += itemAddTxt(psInsItem, ITEM_TYPE_TXT,
								"Statistic : " + rsStats.getString("stat_norm_cvm_s") + "\n" +
								"p         : " + (rsStats.getFloat("stat_norm_cvm_p") == 0 ? "< " + MIN_P_VAL : rsStats.getString("stat_norm_cvm_p")) + "\n" +
								"Normal    : " + (rsStats.getFloat("stat_norm_cvm_p") < 0.05 ? "no" : "yes") 
							);
							
							dbSize += itemAddTxt(psInsItem, ITEM_TYPE_SSSEC, "Kolmogorov-Smirnoff (Lillieford)");
							dbSize += itemAddTxt(psInsItem, ITEM_TYPE_TXT,
								"Statistic : " + rsStats.getString("stat_norm_ks_s") + "\n" +
								"p         : " + (rsStats.getFloat("stat_norm_ks_p") == 0 ? "< " + MIN_P_VAL : rsStats.getString("stat_norm_ks_p")) + "\n" +
								"Normal    : " + (rsStats.getFloat("stat_norm_ks_p") < 0.05 ? "no" : "yes") 
							);
							
							dbSize += itemAddTxt(psInsItem, ITEM_TYPE_SSSEC, "Shapiro-Wilk");
							dbSize += itemAddTxt(psInsItem, ITEM_TYPE_TXT,
								"Statistic : " + rsStats.getString("stat_norm_sw_s") + "\n" +
								"p         : " + (rsStats.getFloat("stat_norm_sw_p") == 0 ? "< " + MIN_P_VAL : rsStats.getString("stat_norm_sw_p")) + "\n" +
								"Normal    : " + (rsStats.getFloat("stat_norm_sw_p") < 0.05 ? "no" : "yes") 
							);
					}
				}
				
				// (3) Shape:
				if (missValCnt < obsCnt) {
					switch (varType) {
						case Var.TYPE_BIN:
						case Var.TYPE_STR:
							break;
						
						default:
							dbSize += itemAddTxt(psInsItem, ITEM_TYPE_SSEC, "Shape");
							dbSize += itemAddRCodeImg(tmpDir.getAbsolutePath() + File.separatorChar + "hist-" + varName + ".png", psInsItem, rconn, "hist(pegasus.tmp.var$x, main=\"\", xlab=\"" + varName + "\");", 480, 240, 4.5f, 4.5f, 0.75f, 0.75f);
							dbSize += itemAddRCodeImg(tmpDir.getAbsolutePath() + File.separatorChar + "dens-" + varName + ".png", psInsItem, rconn, "hist(pegasus.tmp.var$x, main=\"\", xlab=\"" + varName + "\", freq=F);lines(density(pegasus.tmp.var$x), col=\"red\");", 480, 240, 4.5f, 4.5f, 0.75f, 0.75f);
							dbSize += itemAddRCodeImg(tmpDir.getAbsolutePath() + File.separatorChar + "boxplot-" + varName + ".png", psInsItem, rconn, "boxplot(pegasus.tmp.var$x, horizontal=T, main=\"\", xlab=\"" + varName + "\");", 480, 120, 4.5f, 0.75f, 0.75f, 0.75f);
					}
				}
				
				rsStats.close();
			}
			rsVars.close();
			
			// (4) Update size:
			stX.executeUpdate("UPDATE " + dbData + ".rep SET size = " + dbSize + ", calc_time = " + timer.elapsed() + " WHERE id = " + repId);
			stX.executeUpdate("UPDATE " + dbData + ".ds SET size = (size + " + dbSize + ") WHERE id = " + dsId);
			
			DBI.transCommit(stX, true);
		}
		catch (SQLException e) {
			DBI.transRollback(stX, true);
			
			if (e.getErrorCode() == DBI.SQL_ERR_DUP_ENTRY) return new Result(false, "msg:\"A data set with the specified name already exists.\n\nIf you cannot see such data set in the list, it is possible that someone else has added it. Please remember that this is a multiuser system and more than one person can be working on the same project and/or data set at the same time. Consider refreshing the page.\"");
			else throw e;
		}
		
		rconn.eval("remove(pegasus.tmp.var)");
		
		return new Result(true, "rep:" + get(dbi, username, repId) + ",size:{ds:" + DataSet.getSize(dbi, username, dsId) + "}");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static StringBuffer get(DBI dbi, String username, int repId)  throws SQLException {
		StringBuffer res = new StringBuffer();
		
		ResultSet rs = dbi.execQry("SELECT id, name, memo, created, size, calc_time FROM \"" + Do.DB_DATA + "-" + username + "\".rep WHERE id = " + repId);
		rs.next();
		
		String memo = rs.getString("memo");
		res.append(
			"{" +
			"id:" + rs.getString("id") + "," +
			"name:" + JSI.str2js(rs.getString("name")) + "," +
			(memo != null && memo.length() > 0 ? "memo:" + JSI.str2js(memo) + "," : "") +
			"created:" + "new Date(" + rs.getTimestamp("created").getTime() + ")," +
			"size:" + rs.getInt("size") + "," +
			"calcTime:\"" + Convert.time2str(rs.getLong("calc_time")) + "\"" +
			"}"
		);
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static StringBuffer getLst(DBI dbi, String username, int dsId)  throws SQLException {
		StringBuffer res = new StringBuffer("[");
		
		ResultSet rs = dbi.execQry("SELECT id, name, memo, created, size, calc_time FROM \"" + Do.DB_DATA + "-" + username + "\".rep WHERE ds_id = " + dsId);
		while (rs.next()) {
			String memo = rs.getString("memo");
			res.append(
				"{" +
				"id:" + rs.getString("id") + "," +
				"name:" + JSI.str2js(rs.getString("name")) + "," +
				(memo != null && memo.length() > 0 ? "memo:" + JSI.str2js(memo) + "," : "") +
				"created:" + "new Date(" + rs.getTimestamp("created").getTime() + ")," +
				"size:" + rs.getInt("size") + "," +
				"calcTime:\"" + Convert.time2str(rs.getLong("calc_time")) + "\"" +
				"}" + (!rs.isLast() ? "," : "")
			);
		}
		rs.close();
		res.append("]");
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static String getCode(DBI dbi, String username, int repId)  throws SQLException {
		return DBI.getStr(dbi, "SELECT code FROM \"" + Do.DB_DATA + "-" + username + "\".rep WHERE id = " + repId, "code");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	private static int itemAddFile(PreparedStatement ps, int typeId, String path, String meta, boolean del)  throws SQLException, FileNotFoundException {
		File f = new File(path);
		
		int dbSize = DBI.REC_SIZE_REP_ITEM + (int) f.length() + meta.length();
		
		ps.setInt(1, typeId);
		FileInputStream fisImg = new FileInputStream(f);
		ps.setBinaryStream(2, fisImg, (int) f.length());
		ps.setString(3, meta);
		ps.setInt(4, DBI.REC_SIZE_REP_ITEM + (int) f.length() + meta.length());
		
		ps.executeUpdate();
		if (del) f.delete();
		
		return dbSize;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	private static int itemAddTxt(PreparedStatement ps, int typeId, String content)  throws SQLException {
		int dbSize = DBI.REC_SIZE_REP_ITEM + content.length();
		
		ps.setInt(1, typeId);
		ps.setBytes(2, content.getBytes());
		ps.setString(3, null);
		ps.setInt(4, DBI.REC_SIZE_REP_ITEM + content.length());
		ps.executeUpdate();
		
		return dbSize;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * `rcode` is assumed to have a semicolon at the end.
	 * 
	 * - marings: bottom, left, top, right
	 */
	private static int itemAddRCodeImg(String path, PreparedStatement ps, RConnection rconn, String rcode, int w, int h, float marB, float marL, float marT, float marR)  throws RserveException, FileNotFoundException, SQLException, REXPMismatchException {
		rconn.eval("png(\"" + path + "\", bg=\"transparent\", width=" + w + ", height=" + h + ");par(mar=c(" + marB + "," + marL + "," + marT + "," + marR + "));" + rcode + "dev.off();");
		String meta = "w:" + w + ",h:" + h;
		int dbSize = DBI.REC_SIZE_REP_ITEM + (int) (new File(path)).length() + meta.length();
		itemAddFile(ps, ITEM_TYPE_IMG, path, meta, true);
		
		return dbSize;
		
		/*
		REXP rx = rconn.eval("pegasus.tmp.img = readBin('" + path + "', 'raw', 1024*1024); unlink('" + path + "'); r;");
		
		ps.setInt(1, ITEM_TYPE_IMG);
		byte[] img = rx.asBytes();
		ByteArrayInputStream isImg = new ByteArrayInputStream(img);
		ps.setBinaryStream(2, isImg, img.length);
		ps.executeUpdate();
		*/
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/*
	private static void itemAddRCodeTxt() {
	}
	*/
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static Result rScrExec(DBI dbi, String username, int dsId, String repName, String code)  throws SQLException, REXPMismatchException, IOException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		util.Timer timer = new util.Timer();
		
		String db   = "\"" + Do.DB_DATA + "-" + username + "\"";
		
		int repId = DBI.ID_NONE;
		int dbSize = DBI.REC_SIZE_REP;
		
		DBI dbiX = new DBI();
		Statement stX = dbiX.transBegin();
		try {
			repId = DBI.insertGetId(stX, null, "INSERT INTO " + db + ".rep (ds_id, name, code) VALUES (" + dsId + "," + DBI.esc(repName) + "," + DBI.esc(code) + ")");
			PreparedStatement psInsItem = dbiX.getConn().get().prepareStatement("INSERT INTO " + db + ".rep_item (rep_id, type_id, content, meta, size) VALUES (" + repId + ", ?, ?, ?, ?)");
			
			for (String c: code.split("\n")) {
				c = c.trim();
				if (c.length() == 0) continue;
				
				// Report item:
				if (c.startsWith(R_SCR_SEC)) dbSize += itemAddTxt(psInsItem, ITEM_TYPE_SEC, c.substring(R_SCR_SEC.length()).trim());
				else if (c.startsWith(R_SCR_SSEC)) dbSize += itemAddTxt(psInsItem, ITEM_TYPE_SSEC, c.substring(R_SCR_SSEC.length()).trim());
				else if (c.startsWith(R_SCR_SSSEC)) dbSize += itemAddTxt(psInsItem, ITEM_TYPE_SSSEC, c.substring(R_SCR_SSSEC.length()).trim());
				
				else if (c.startsWith(R_SCR_TXT)) dbSize += itemAddTxt(psInsItem, ITEM_TYPE_TXT, c.substring(R_SCR_TXT.length()).trim());
				
				// A command:
				else {
					dbSize += itemAddTxt(psInsItem, ITEM_TYPE_TXT,
						"> " + c + "\n" +
						Do.r.exec(username, dsId, c, false, false).getMsg(false)
					);
				}
			}
			
			// (4) Update size:
			stX.executeUpdate("UPDATE " + db + ".rep SET size = " + dbSize + ", calc_time = " + timer.elapsed() + " WHERE id = " + repId);
			stX.executeUpdate("UPDATE " + db + ".ds SET size = (size + " + dbSize + ") WHERE id = " + dsId);
			
			DBI.transCommit(stX, true);
		}
		catch (SQLException e) {
			DBI.transRollback(stX, true);
			
			if (e.getErrorCode() == DBI.SQL_ERR_DUP_ENTRY) return new Result(false, "msg:\"A data set with the specified name already exists.\n\nIf you cannot see such data set in the list, it is possible that someone else has added it. Please remember that this is a multiuser system and more than one person can be working on the same project and/or data set at the same time. Consider refreshing the page.\"");
			else throw e;
		}
		
		return new Result(true, "rep:" + get(dbi, username, repId) + ",size:{ds:" + DataSet.getSize(dbi, username, dsId) + "}");
	}
}
