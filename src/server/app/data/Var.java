package app.data;

import inter.DBI;
import inter.JSI;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import main.Do;
import main.Result;

import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;

import util.$;


/**
 * @author Tomek D. Loboda
 */
public class Var {
	public static final int TYPE_UND = 0;  // undefined (must be defined later)
	public static final int TYPE_BIN = 1;  // binary
	public static final int TYPE_CAT = 2;  // categorical (nominal)
	public static final int TYPE_ORD = 3;  // ordinal
	public static final int TYPE_DIS = 4;  // discrete
	public static final int TYPE_CON = 5;  // continuous
	public static final int TYPE_STR = 6;  // string
	
	public static final int FORMAT_NUMERIC_PRECISION = 24;
	public static final int FORMAT_NUMERIC_SCALE = 12;
	
	private static final double NORM_S_MAX = 9999.9999d;
	
	private int id = DBI.ID_NONE;
	private int type = TYPE_UND;
	private String name = null;
	private String defVal = null;
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Var(int id, int type, String name, String defVal) {
		this.id = id;
		this.type = type;
		this.name = name;
		this.defVal = defVal;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Calculates statistics of a variable calculating them first, but only if necessary. It may or may not return the 
	 * resuls (`ret`). If it does, the format is JSON.
	 */
	public static String calcStats(DBI dbi, String username, int dsId, int varId, String varName, boolean ret)  throws SQLException, RserveException, REXPMismatchException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		String dbD = "\"" + Do.DB_DATA + "-" + username + "\"";
		
		int statObsCnt = (int) DBI.getLong(dbi, "SELECT COUNT(*) AS cnt FROM \"" + Do.DB_DATA_DS + "-" + username + "\".ds_" + dsId, "cnt");
		int statMissValCnt = (int) DBI.getLong(dbi, "SELECT COUNT(*) - COUNT(\"" + varName + "\") AS cnt FROM \"" + Do.DB_DATA_DS + "-" + username + "\".ds_" + dsId, "cnt");
		int type = (int) DBI.getLong(dbi, "SELECT type_id FROM " + dbD + ".var WHERE id = " + varId, "type_id");
		
		// (1) All-missing-values or string variable:
		if (statMissValCnt == statObsCnt || type == TYPE_STR) {
			dbi.execUpd("UPDATE " + dbD + ".var SET stat_misc_miss_val_cnt = " + statMissValCnt + ", stat_ok = 1 WHERE id = " + varId);
			return "{desc:null,norm:null,misc:{missValCnt:" + statMissValCnt + "}}"; 
		}
		
		// (2) Numeric variable:
		// (2.1) Statistics up-to-date:
		int ok = (int) DBI.getLong(dbi, "SELECT stat_ok FROM " + dbD + ".var WHERE id = " + varId, "stat_ok");
		if (ok == 1) {
			return (ret
				? DBI.getStr(
					dbi,
					"SELECT " +
					"'{desc:{" +
					"min:' ||  COALESCE(stat_desc_min::text,  'null') || '," +
					"max:' ||  COALESCE(stat_desc_max::text,  'null') || '," +
					"mean:' || COALESCE(stat_desc_mean::text, 'null') || '," +
					"med:' ||  COALESCE(stat_desc_med::text,  'null') || '," +
					"sd:' ||   COALESCE(stat_desc_sd::text,   'null') || '" +
					"},norm:{" +
					"ad:{s:' ||  COALESCE(stat_norm_ad_s::text,  '\"Infinity\"') || ',p:' || COALESCE(stat_norm_ad_p::text,  '\"1\"') || '}," +
					"cvm:{s:' || COALESCE(stat_norm_cvm_s::text, '\"Infinity\"') || ',p:' || COALESCE(stat_norm_cvm_p::text, '\"1\"') || '}," +
					"ks:{s:' ||  COALESCE(stat_norm_ks_s::text,  '\"Infinity\"') || ',p:' || COALESCE(stat_norm_ks_p::text,  '\"1\"') || '}," +
					"sw:{s:' ||  COALESCE(stat_norm_sw_s::text,  '\"Infinity\"') || ',p:' || COALESCE(stat_norm_sw_p::text,  '\"1\"') || '}" +
					"}," +
					"misc:{missValCnt:' || stat_misc_miss_val_cnt || '}}' " +
					"AS stat FROM " + dbD + ".var WHERE id = " + varId, "stat"
				)
				: null
			);
		}
		
		// (2.2) Statistics outdated:
		String statDescMin  = "null";
		String statDescMax  = "null";
		String statDescMean = "null";
		String statDescMed  = "null";
		String statDescSD   = "null";
		
		String statNormAD_s  = "null";
		String statNormAD_p  = "null";
		String statNormCVM_s = "null";
		String statNormCVM_p = "null";
		String statNormKS_s  = "null";
		String statNormKS_p  = "null";
		String statNormSW_s  = "null";
		String statNormSW_p  = "null";
		
		// (2.2.1) Load variable:
		RConnection rconn = Do.r.getConn();
		try {
			Do.r.loadVar(username, "pegasus.tmp.var", dsId, varName);
			
			// (2.2.2) Descriptives:
			statDescMin  = rconn.eval("min(pegasus.tmp.var, na.rm = T)").asString();
			statDescMax  = rconn.eval("max(pegasus.tmp.var, na.rm = T)").asString();
			//statDescMean = rconn.eval("mean(pegasus.tmp.var, na.rm = T)").asString();
			statDescMean = rconn.eval("colMeans(pegasus.tmp.var, na.rm = T)[1]").asString();
			statDescMed  = (type == TYPE_BIN ? "null" : rconn.eval("median(pegasus.tmp.var, na.rm = T)").asString());
			statDescSD   = (type == TYPE_BIN ? "null" : rconn.eval("sd(pegasus.tmp.var, na.rm = T)").asString());
			
			// (2.2.3) Normality:
			statNormAD_s = "null";
			statNormAD_p = "null";
			if (type != TYPE_BIN) {
				try {
					rconn.eval("pegasus.res <- ad.test(pegasus.tmp.var$x)");
					statNormAD_s = rconn.eval("if (is.infinite(pegasus.res$statistic) || is.nan(pegasus.res$statistic)) { 'null' } else { min(pegasus.res$statistic, " + NORM_S_MAX + ") }").asString();
					statNormAD_p = rconn.eval("if (is.infinite(pegasus.res$p.value) || is.nan(pegasus.res$p.value)) { 'null' } else { if (pegasus.res$p.value < 0) { 0 } else { if (pegasus.res$p.value > 1) { 1 } else { pegasus.res$p.value } } }").asString();
				}
				catch (RserveException e) {}
			}
			
			statNormCVM_s = "null";
			statNormCVM_p = "null";
			if (type != TYPE_BIN) {
				try {
					rconn.eval("pegasus.res <- cvm.test(pegasus.tmp.var$x)");
					statNormCVM_s = rconn.eval("if (is.infinite(pegasus.res$statistic) || is.nan(pegasus.res$statistic)) { 'null' } else { min(pegasus.res$statistic, " + NORM_S_MAX + ") }").asString();
					statNormCVM_p = rconn.eval("if (is.infinite(pegasus.res$p.value) || is.nan(pegasus.res$p.value)) { 'null' } else { if (pegasus.res$p.value < 0) { 0 } else { if (pegasus.res$p.value > 1) { 1 } else { pegasus.res$p.value } } }").asString();
				}
				catch (RserveException e) {}
			}
			
			statNormKS_s = "null";
			statNormKS_p = "null";
			if (type != TYPE_BIN) {
				try {
					rconn.eval("pegasus.res <- lillie.test(pegasus.tmp.var$x)");
					statNormKS_s = rconn.eval("if (is.infinite(pegasus.res$statistic) || is.nan(pegasus.res$statistic)) { 'null' } else { min(pegasus.res$statistic, " + NORM_S_MAX + ") }").asString();
					statNormKS_p = rconn.eval("if (is.infinite(pegasus.res$p.value) || is.nan(pegasus.res$p.value)) { 'null' } else { if (pegasus.res$p.value < 0) { 0 } else { if (pegasus.res$p.value > 1) { 1 } else { pegasus.res$p.value } } }").asString();
				}
				catch (RserveException e) {}
			}
			
			statNormSW_s = "null";
			statNormSW_p = "null";
			if (type != TYPE_BIN) {
				try {
					rconn.eval("pegasus.res <- shapiro.test(pegasus.tmp.var$x)");
					statNormSW_s = rconn.eval("if (is.infinite(pegasus.res$statistic) || is.nan(pegasus.res$statistic)) { 'null' } else { min(pegasus.res$statistic, " + NORM_S_MAX + ") }").asString();
					statNormSW_p = rconn.eval("if (is.infinite(pegasus.res$p.value) || is.nan(pegasus.res$p.value)) { 'null' } else { if (pegasus.res$p.value < 0) { 0 } else { if (pegasus.res$p.value > 1) { 1 } else { pegasus.res$p.value } } }").asString();
				}
				catch (RserveException e) {}
			}
			
			rconn.eval("remove(pegasus.tmp.var)");
			
			// (2.2.4) Persist and return:
			dbi.execUpd(
				"UPDATE " + dbD + ".var SET " +
				
				"stat_desc_min = "  + (statDescMin.equals("NaN")  ? "null" : "ROUND(" + statDescMin  + ",12)") + ", " +
				"stat_desc_max = "  + (statDescMax.equals("NaN")  ? "null" : "ROUND(" + statDescMax  + ",12)") + ", " +
				"stat_desc_mean = " + (statDescMean.equals("NaN") ? "null" : "ROUND(" + statDescMean + ",12)") + ", " +
				"stat_desc_med = "  + (statDescMed.equals("NaN")  ? "null" : "ROUND(" + statDescMed  + ",12)") + ", " +
				"stat_desc_sd = "   + (statDescSD.equals("NaN")   ? "null" : "ROUND(" + statDescSD   + ",12)") + ", " +
				
				"stat_norm_ad_s = ROUND("  + statNormAD_s  + ",4), " +
				"stat_norm_ad_p = ROUND("  + statNormAD_p  + ",4), " +
				"stat_norm_cvm_s = ROUND(" + statNormCVM_s + ",4), " +
				"stat_norm_cvm_p = ROUND(" + statNormCVM_p + ",4), " +
				"stat_norm_ks_s = ROUND("  + statNormKS_s  + ",4), " +
				"stat_norm_ks_p = ROUND("  + statNormKS_p  + ",4), " +
				"stat_norm_sw_s = ROUND("  + statNormSW_s  + ",4), " +
				"stat_norm_sw_p = ROUND("  + statNormSW_p  + ",4), " +
				
				"stat_misc_miss_val_cnt = " + statMissValCnt + ", " +
				"stat_ok = 1 " +
				
				"WHERE id = " + varId
			);
		}
		catch (RserveException e) {
		}
		
		// TODO: space-optimize return:
		return (ret
			?
				"{" +
				"desc:{" +
				(statDescMin.equals("null") ? "" : "min:" + statDescMin + ",") +
				(statDescMin.equals("null") ? "" : "max:" + statDescMax + ",") +
				(statDescMin.equals("null") ? "" : "mean:" +statDescMean + ",") +
				(statDescMin.equals("null") ? "" : "med:" +statDescMed + ",") +
				(statDescMin.equals("null") ? "" : "sd:" + statDescSD + "") +
				"}," +
				"norm:{" +
				"ad:{" +
				"s:" + $.ifNull(statNormAD_s, "\"Infinity\"") + "," +
				"p:" + $.ifNull(statNormAD_p, "\"1\"") +
				"}," +
				"cvm:{" +
				"s:" + $.ifNull(statNormCVM_s, "\"Infinity\"") + "," +
				"p:" + $.ifNull(statNormCVM_p, "\"1\"") +
				"}," +
				"ks:{" +
				"s:" + $.ifNull(statNormKS_s, "\"Infinity\"") + "," +
				"p:" + $.ifNull(statNormKS_p, "\"1\"") +
				"}," +
				"sw:{" +
				"s:" + $.ifNull(statNormSW_s, "\"Infinity\"") + "," +
				"p:" + $.ifNull(statNormSW_p, "\"1\"") +
				"}" +
				"}," +
				"misc:{" +
				"missValCnt:" + statMissValCnt + 
				"}" +
				"}"
			: null
		);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Returns a `val` of the `typeIn` casted as `typeOut`. If null is returned, then a conversion error has 
	 * occured, e.g., a non-numeric string could not be converted to an integer.
	 */
	public static String convValType(String val, int typeIn, int typeOut) {
		if (typeIn == typeOut) return val;
		if (val == null || val.equals(Do.MISS_VAL)) return "null";
		
		String res = null;
		
		try {
			switch (typeOut) {
				// To binary:
				case TYPE_BIN:
					res = (new Double(val) == 0.0f ? "0" : "1");
					break;
				
				// To integer:
				case TYPE_CAT:
				case TYPE_ORD:
				case TYPE_DIS:
					res = "" + Math.floor(new Double(val));
					break;
				
				// To real:
				case TYPE_CON:
					res = "" + (new Double(val)).doubleValue();
					break;
				
				// To string:
				case TYPE_STR:
					res = DBI.esc(val); 
					break;
			}
		}
		catch (NumberFormatException e02) {
			res = null;
		}
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static Result del(DBI dbi, String username, int varId)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		String dbD = "\"" + Do.DB_DATA + "-" + username + "\"";
		String dbDS = "\"" + Do.DB_DATA_DS + "-" + username + "\"";
		
		// (1) Get info:
		int dsId = (int) DBI.getLong(dbi, "SELECT ds_id FROM " + dbD + ".var WHERE id = " + varId, "ds_id");
		
		ResultSet rs = dbi.execQry("SELECT ord, ds_id, name, (SELECT COUNT(*) FROM " + dbD + ".var WHERE ds_id = " + dsId + ") AS var_cnt FROM " + dbD + ".var WHERE id = " + varId);
		rs.next();
		int ord = rs.getInt("ord");
		int varCnt = rs.getInt("var_cnt") - 1;
		String varName = rs.getString("name");
		rs.close();
		
		// (2) Delete variable:
		Statement stX = (new DBI()).transBegin();
		try {
			stX.executeUpdate("DELETE FROM " + dbD + ".var WHERE id = " + varId);
			stX.executeUpdate("UPDATE " + dbD + ".var SET ord = ord-1 WHERE id IN (SELECT id FROM " + dbD + ".var WHERE ds_id = " + dsId + " AND ord > " + ord + " ORDER BY ord ASC)");
			stX.executeUpdate("ALTER TABLE " + dbDS + ".ds_" + dsId + " DROP COLUMN \"" + varName + "\"");
			stX.executeUpdate(DataSet.getSQLUpdateMetaInfo(dbi, username, dsId, null));
			DBI.transCommit(stX, true);
		}
		catch (SQLException e) {
			DBI.transRollback(stX, true);
			throw e;
		}
		
		return new Result(true, "size:{ds:" + DataSet.getSize(dbi, username, dsId) + "}");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static StringBuffer getLst(DBI dbi, String username, int dsId)  throws SQLException, REXPMismatchException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		String dbD = "\"" + Do.DB_DATA + "-" + username + "\"";
		
		StringBuffer res = new StringBuffer("[");
		
		R r = Do.r;
		try {
			r.connStart();
				// Quick fix to make it work for GitLab description (2018.09.20)
			ResultSet rs = dbi.execQry("SELECT * FROM " + dbD + ".var WHERE ds_id = " + dsId + " ORDER BY ord");
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String memo = rs.getString("memo");
				
				res.append(
					"{" +
					"id:" + id + "," +
					"type:" + rs.getInt("type_id") + "," +
					"name:" + JSI.str2js(name) + "," +
					"stat:" + calcStats(dbi, username, dsId, id, name, true) + "," +
					(memo != null && memo.length() > 0 ? "memo:" + JSI.str2js(memo) : "") +
					"}" + (!rs.isLast() ? "," : "")
				);
			}
			rs.close();
			r.connEnd();
				// Quick fix to make it work for GitLab description (2018.09.20)
		}
		catch (RserveException e) {}
		
		res.append("]");
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Returns the SQL type corresponding to the specified type of the variable.
	 */
	public static String getSQLColType(int type) {
		String res = "";
		switch (type) {
			case TYPE_BIN: res = "smallint"; break;  // MySQL: TINYINT(1)
			case TYPE_CAT: res = "integer"; break;  // MySQL: INT
			case TYPE_ORD: res = "integer"; break;  // MySQL: INT
			case TYPE_DIS: res = "integer"; break;  // MySQL: INT
			case TYPE_CON: res = "numeric(" + FORMAT_NUMERIC_PRECISION + "," + FORMAT_NUMERIC_SCALE + ")"; break;  // MySQL: FLOAT
			case TYPE_STR: res = "varchar(64)"; break;
		}
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static String getSQLVal(int type, String val) {
		if (type == TYPE_STR) return DBI.esc(val);
		else return val;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static int getType(DBI dbi, String db, int varId)  throws SQLException {
		return (int) DBI.getLong(dbi, "SELECT type_id FROM " + db + ".var WHERE id = " + varId, "type_id");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	// TODO: change into `toggleTS`
	public static Result markTS(DBI dbi, String username, int varId)  throws SQLException {
		String dbD = "\"" + Do.DB_DATA + "-" + username + "\"";
		
		if (getType(dbi, dbD, varId) != Var.TYPE_DIS) return new Result(false, "msg:\"Only a discrete variable can be marked as a time series indicator.\"");
		
		int dsId = (int) DBI.getLong(dbi, "SELECT ds_id FROM " + dbD + ".var WHERE id = " + varId, "ds_id");
		String name = DBI.getStr(dbi, "SELECT name FROM " + dbD + ".var WHERE id = " + varId, "name");
		dbi.execUpd("UPDATE " + dbD + ".ds SET ts_var = " + DBI.esc(name) + " WHERE id = " + dsId);
		
		return new Result(true, "");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static Result setType(DBI dbi, String username, int varIdCurr, String nameNew, int typeNew)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		String dbD = "\"" + Do.DB_DATA + "-" + username + "\"";
		String dbDS = "\"" + Do.DB_DATA_DS + "-" + username + "\"";
		
		int typeCurr = (int) DBI.getLong(dbi, "SELECT type_id FROM " + dbD + ".var WHERE id = " + varIdCurr, "type_id");
		if (typeCurr == typeNew) return new Result(false, "msg:\"The active variable is already of this type.\"");
		
		int dsId = (int) DBI.getLong(dbi, "SELECT ds_id FROM " + dbD + ".var WHERE id = " + varIdCurr, "ds_id");
		int varIdNew = DBI.ID_NONE;
		
		int varCnt = (int) DBI.getLong(dbi, "SELECT COUNT(*) AS cnt FROM " + dbD + ".var WHERE ds_id = " + dsId, "cnt");
		
		if (nameNew != null && nameNew.length() > 0) {
			if (nameNew.equals(DataSet.ID_COL_NAME)) return new Result(false, "msg:\"The name '" + DataSet.ID_COL_NAME + "' is reserved and cannot be used as a name of a variable. Please provide a different name.\"");;
			if (DBI.getLong(dbi, "SELECT id FROM " + dbD + ".var WHERE ds_id = " + dsId + " AND name = " + DBI.esc(nameNew), "id") != DBI.ID_NONE) return new Result(false, "msg:\"A variable with the specified name already exists in the active data set. The only existing variable the type of which you can change using this window is the active variable. In order to do so, please leave the new variable name field empty.\"");
		}
		
		DBI dbiX = new DBI();
		Statement stX = dbiX.transBegin();
		try {
			String nameCurr = DBI.getStr(dbi, "SELECT name FROM " + dbD + ".var WHERE id = " + varIdCurr, "name");
			
			// (1) Retrieve and convert all values of the source variable:
			List<Val> lstVal = new ArrayList<Val>();
			ResultSet rs = dbi.execQry("SELECT id, \"" + nameCurr + "\" AS val FROM " + dbDS + ".ds_" + dsId + " ORDER BY id");
			while (rs.next()) {
				String v = convValType(rs.getString("val"), typeCurr, typeNew);
				if (v != null) lstVal.add(new Val(rs.getInt("id"), v));
			}
			rs.close();
			
			// (2) Change the source variable's type of add a new variable:
			if (nameNew != null && nameNew.length() > 0) {
				int ordCurr = (int) DBI.getLong(dbi, "SELECT ord FROM " + dbD + ".var WHERE id = " + varIdCurr, "ord");
				stX.executeUpdate("UPDATE " + dbD + ".var SET ord = ord+1 WHERE ds_id = " + dsId + " AND ord > " + ordCurr + " ORDER BY ord DESC");
				varIdNew = DBI.insertGetId(stX, null, "INSERT INTO " + dbD + ".var (ds_id, type_id, name, ord) VALUES (" + dsId + "," + typeNew + "," + DBI.esc(nameNew) + "," + (ordCurr+1) + ")");
				stX.executeUpdate("ALTER TABLE " + dbDS + ".ds_" + dsId + " ADD COLUMN \"" + nameNew + "` " + getSQLColType(typeNew) + " AFTER \"" + nameCurr + "\"");
				varCnt++;
			}
			else {
				nameNew = nameCurr;
				varIdNew = varIdCurr;
				stX.executeUpdate("UPDATE " + dbD + ".var SET type_id = " + typeNew + " WHERE id = " + varIdNew);
				stX.executeUpdate("ALTER TABLE " + dbDS + ".ds_" + dsId + " MODIFY COLUMN \"" + nameNew + "\" " + getSQLColType(typeNew));
			}
			
			// (3) Committ the converted values to the destination variable:
			Iterator<Val> it = lstVal.iterator();
			while (it.hasNext()) {
				Val v = it.next();
				stX.executeUpdate("UPDATE " + dbDS + ".ds_" + dsId + " SET \"" + nameNew + "\" = " + v.val + " WHERE id = " + v.id);
			}
			
			stX.executeUpdate(DataSet.getSQLUpdateMetaInfo(dbi, username, dsId, null));
			stX.executeUpdate("UPDATE " + dbD + ".var SET stat_ok = 0 WHERE id = " + varIdNew);
			
			DBI.transCommit(stX, true);
		}
		catch (SQLException e) {
			DBI.transRollback(stX, true);
			throw e;
		}
		
		return new Result(true, "size:{ds:" + DataSet.getSize(dbi, username, dsId) + "}");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public String getDefVal() { return (defVal == null ? "null" : "'" + defVal + "'"); }
	public int    getId()     { return id; }
	public String getName()   { return name; }
	public int    getType()   { return type; }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public void setName(String n) { name = n; }
	public void setType(int t)    { type = t; }
	
	
	// =================================================================================================================
	private static class Val {
		public final int id;
		public final String val;
		
		public Val(int id, String val) {
			this.id = id;
			this.val = val;
		}
	}
}
