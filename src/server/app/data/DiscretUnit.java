package app.data;

import inter.DBI;


/**
 * Discretization unit
 * 
 * @author Tomek D. Loboda
 */
public class DiscretUnit {
	private final String name;
	private final int type;
	
	private final String[] states;
	private final double[][] ranges;
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Automatically determines if the variable should be of the DIS or STR type.
	 * 
	 * spec: "s1_below_1,.,1;s2_1_2,1,2;s3_2_up,2,."
	 */
	public DiscretUnit(String name, String spec) {
		String[] A = spec.split(";");
		
		this.states = new String[A.length];
		this.ranges = new double[A.length][2];
		
		boolean isInt = true;
		for (int i = 0; i < A.length; i++) {
			String a = A[i];
			String[] B = a.split(",");
			
			this.states[i] = B[0];
			ranges[i][0] = (B[1].equals(".") ? -Double.MAX_VALUE : new Double(B[1]));
			ranges[i][1] = (B[2].equals(".") ?  Double.MAX_VALUE : new Double(B[2]));
			
			if (isInt) {
				try { new Integer(B[0]); }
				catch (NumberFormatException e) { isInt = false; }
			}
		}
		
		this.name = name;
		type = (isInt ? Var.TYPE_DIS : Var.TYPE_STR);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public int getIdx(double val) {
		for (int i = 0; i < states.length; i++) {
			if (val >= ranges[i][0] && val < ranges[i][1]) return i;
		}
		//return -1;
		return states.length - 1;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Returns a state usable directly in a SQL query.
	 */
	public String getValSQL(Double val) {
		//System.out.println((val == null ? "null" : type + "." + name + ": " + val + " --> " + getIdx(val)));
		if (val == null) return "null";  // a missing value has been provided; return the ball
		
		if (type == Var.TYPE_DIS) return "" + getIdx(val);
		else return DBI.esc(states[getIdx(val)]);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public String getName() { return name; }
	public int    getType() { return type; }
}
