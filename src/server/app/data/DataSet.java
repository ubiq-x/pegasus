package app.data;

import inter.DBI;
import inter.FSI;
import inter.JSI;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import main.Do;
import main.Result;

import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.Rserve.RserveException;

import util.$;
import util.Convert;


/**
 * @author Tomek D. Loboda
 */
public class DataSet {
	public static final String ID_COL_NAME = "__id__";
	
	public static final int RINT_MAX_CMD_CNT = 20;
	public static final int RINT_MAX_RES_LN_CNT = 2;
	
	public static final int EXP_FORMAT_TXT  = 1;
	public static final int EXP_FORMAT_HTML = 2;
	public static final int EXP_PAGE_LIMIT = 200;
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public DataSet() {}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static Result addNew(DBI dbi, String username, int prjId, int appId, String name, String memo, String tsVar, int calcTime, List<Var> vars)  throws SQLException, RserveException, REXPMismatchException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		int dsId = DBI.ID_NONE;
		
		Statement stX = (new DBI()).transBegin();
		try {
			dsId = createDSTbl(dbi, stX, username, prjId, appId, name, memo, tsVar, calcTime, vars);
			DBI.transCommit(stX, true);
		}
		catch (SQLException e) {
			DBI.transRollback(stX, true);
			
			if (e.getErrorCode() == DBI.SQL_ERR_DUP_ENTRY) return new Result(false, "msg:\"A data set with the specified name already exists.\n\nIf you cannot see such data set in the list, it is possible that someone else has added it. Please remember that this is a multiuser system and more than one person can be working on the same project and/or data set at the same time. Consider refreshing the page.\"");
			else throw e;
		}
		
		return new Result(true, "ds:" + get(dbi, username, dsId));
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	private static int createDSTbl(DBI dbi, Statement stX, String username, int prjId, int appId, String name, String memo, String tsVar, int calcTime, List<Var> vars)  throws SQLException, RserveException, REXPMismatchException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		String dbD = "\"" + Do.DB_DATA + "-" + username + "\"";
		String dbDS = "\"" + Do.DB_DATA_DS + "-" + username + "\"";
		
		int dsId = DBI.insertGetId(stX, null, "INSERT INTO " + dbD + ".ds (prj_id, app_id, name, memo, ts_var, calc_time, size) VALUES (" + prjId + "," + appId + "," + DBI.esc(name) + "," + DBI.esc(memo) + "," + DBI.esc(tsVar) + "," + calcTime + "," + (DBI.REC_SIZE_DATA_SET + name.length()) + ")");
		
		StringBuilder qryTbl = new StringBuilder(
			"CREATE TABLE " + dbDS + ".\"ds_" + dsId + "\" (" +
			"\"" + ID_COL_NAME + "\" serial,"  // MySQL: "\"" + ID_COL_NAME + "\" INT UNSIGNED NOT NULL AUTO_INCREMENT,"
		);
		
		if (vars != null) {
			int i = 0;
			Iterator<Var> it = vars.iterator();
			while (it.hasNext()) {
				Var v = it.next();
				
				qryTbl.append("\"" + v.getName() + "\" " + Var.getSQLColType(v.getType()) + " NULL,");
				stX.executeUpdate("INSERT INTO " + dbD + ".var (ds_id, type_id, name, ord) VALUES (" + dsId + "," + v.getType() + "," + DBI.esc(v.getName()) + "," + (i++) + ")");
			}
		}
		
		qryTbl.append("PRIMARY KEY (\"" + ID_COL_NAME + "\"))");
		stX.executeUpdate(qryTbl.toString());
		
		return dsId;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Delegates the deletion to the data set's native application.
	 * 
	 * dsIds - a list of comma-delimited IDs of to-be-deleted data sets.
	 */
	public static Result del(DBI dbi, String username, String dsIds)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		DBI dbiX = new DBI();
		Statement stX = dbiX.transBegin();
		try {
			del(dbiX, stX, username, dsIds);
			DBI.transCommit(stX, true);
			//dbiX.vacuum();
		}
		catch (SQLException e) {
			DBI.transRollback(stX, true);
			throw e;
		}
		
		return new Result(true, "");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * dsIds - a list of comma-delimited IDs of to-be-deleted data sets.
	 */
	public static void del(DBI dbiX, Statement stX, String username, String dsIds)  throws SQLException {
		for (String dsId: dsIds.split(",")) {
			// Data set:
			stX.executeUpdate("DROP TABLE \"" + Do.DB_DATA_DS + "-" + username + "\".ds_" + dsId);
			
			// Native application:
			long appId = DBI.getLong(dbiX, "SELECT app_id FROM \"" + Do.DB_DATA + "-" + username + "\".ds WHERE id = " + dsId, "app_id");
			if (appId == Do.getAppIdGaze()) {
				stX.executeUpdate("DELETE FROM \"" + Do.DB_GAZE + "-" + username + "\".ds WHERE id = " + dsId);
			}
		}
		stX.executeUpdate("DELETE FROM \"" + Do.DB_DATA + "-" + username + "\".ds WHERE id IN (" + dsIds + ")");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static Result delName(DBI dbi, String username, int prjId, String dsName)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		return del(dbi, username, "" + getId(dbi, username, prjId, dsName));
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static Result delVar(DBI dbi, String username, int prjId, String dsName, String spec) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		return delVar(dbi, username, prjId, getId(dbi, username, prjId, dsName), spec);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static Result delVar(DBI dbi, String username, int prjId, int dsId, String spec) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		String dbD = "\"" + Do.DB_DATA + "-" + username + "\"";
		String dbDS = "\"" + Do.DB_DATA_DS + "-" + username + "\"";
		
		int varCnt = 0;
		int varDelCnt = 0;
		
		Statement stX = (new DBI()).transBegin();
		try {
			String[] varNames01 = spec.split(",");  // all variable names
			varCnt = varNames01.length;
			
			// Collect names of columns that actually exist in the data set:
			List<String> varNames02 = new ArrayList<String>();  // only the existing variable names
			for (int i = 0; i < varNames01.length; i++) {
				String vn = varNames01[i];
				int cnt = (int) DBI.getLong(dbi, "SELECT COUNT(*) AS cnt FROM " + dbD + ".var WHERE ds_id = " + dsId + " AND name = '" + vn + "'", "cnt");
				if (cnt == 1) {
					varNames02.add(vn);
					varDelCnt++;
				}
			}
			
			// Perform filtering:
			if (varDelCnt > 0) {
				stX.executeUpdate("DELETE FROM " + dbD + ".var WHERE ds_id = " + dsId + " AND name IN(" + $.join(varNames02, "'", "'", ",", false, true) + ")");
				stX.executeUpdate("UPDATE " + dbD + ".ds SET size = " + DBI.getTblSize(dbi, dbDS, "ds_" + dsId) + " WHERE id = " + dsId);
				if (varNames02.contains(DBI.getStr(dbi, "SELECT ts_var FROM " + dbD + ".ds WHERE id = " + dsId, "ts_var"))) {
					stX.executeUpdate("UPDATE " + dbD + ".ds SET ts_var = NULL WHERE id = " + dsId);
				}
					// TODO: I've added the above 'if' without checking it, so it could be throwing an exception [2011-12-08, 5:57 PM]
				stX.executeUpdate("ALTER TABLE " + dbDS + ".ds_" + dsId + " " + $.join(varNames02, "DROP COLUMN ", "", ",", false, true));
				
				DBI.transCommit(stX, true);
			}
		}
		catch (SQLException e) {
			DBI.transRollback(stX, true);
			throw e;
		}
		
		//return new Result(true, "msg:" + JSI.str2js("Number of variables provided: " + varCnt + "\nNumber of variables deleted: " + varDelCnt));
		return new Result(true, "inf:{varCnt:" + varCnt + ",varDelCnt:" + varDelCnt + "}");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * spec: "height:s1_0_170,0,170;s2_170_up,170,.|weight:s1_0_80,0,80;s2_80_150,70,150;s3_150_up,150,."
	 */
	public static Result discretize(DBI dbi, String username, int prjId, String dsNameSrc, String dsNameDst, String tsSuffix, String spec)  throws Exception {
		String dbD = "\"" + Do.DB_DATA + "-" + username + "\"";
		String dbDS = "\"" + Do.DB_DATA_DS + "-" + username + "\"";
		
		int dsIdSrc = getId(dbi, username, prjId, dsNameSrc);
		int dsIdDst = DBI.ID_NONE;
		
		// (1) Decode discretization specification:
		String diVarNames = "";
		Hashtable<String,DiscretUnit> DU = new Hashtable<String,DiscretUnit>();
		String[] A = spec.split("\\|");
		if (A.length == 0) return new Result(false, "msg:\"Incorrect specification.\"");
		for (String a: A) {
			String[] B = a.split("\\:");
			if (B.length < 2) return new Result(false, "msg:\"Incorrect specification.\"");
			
			try { DU.put(B[0], new DiscretUnit(B[0], B[1])); }
			catch (NumberFormatException e) { return new Result(false, "msg:\"Incorrect specification.\""); }
			catch (ArrayIndexOutOfBoundsException e) { return new Result(false, "msg:\"Incorrect specification.\""); }
			
			diVarNames += B[0] + ",";
		}
		if (DU.size() == 0) return new Result(false, "msg:\"Incorrect specification.\"");
		diVarNames = diVarNames.substring(0, diVarNames.length() - 1);
		
		// (2) Check if the TS variable will be affected:
		String tsVar = DBI.getStr(dbi, "SELECT ts_var FROM " + dbD + ".ds WHERE id = " + dsIdSrc, "ts_var");
		if (tsVar != null && DU.containsKey(tsVar)) tsVar = null;
		
		// (3) Generate the new variables list:
		List<Var> vars = new ArrayList<Var>();
		StringBuilder qryVars = new StringBuilder();
		
		// (3.1) Retrieve from the original data set:
		ResultSet rsVar = dbi.execQry("SELECT name, type_id FROM " + dbD + ".var WHERE ds_id = " + dsIdSrc + " ORDER BY ord");
		while (rsVar.next()) {
			String name = rsVar.getString("name");
			vars.add(new Var(DBI.ID_NONE, rsVar.getInt("type_id"), name, null));
			qryVars.append("\"" + name + "\"" + (!rsVar.isLast() ? "," : ""));
		}
		rsVar.close();
		
		// (3.2) Check and update types of affected variables:
		Iterator<Var> itVar = vars.iterator();
		while (itVar.hasNext()) {
			Var v = itVar.next();
			if (DU.containsKey(v.getName())) {
				if (v.getType() == Var.TYPE_STR) return new Result(false, "msg:\"Variable '" + v.getName() + "' is of type 'string' and therefore cannot be discretized.\"");
				v.setType(DU.get(v.getName()).getType());
			}
		}
		
		// (4) Perform discretization:
		// (4.1) Add the data set table:
		Statement stX = (new DBI()).transBegin();
		try {
			// Create the new data set table:
			String dsName = DBI.getStr(dbi, "SELECT name FROM " + dbD + ".ds WHERE id = " + dsIdSrc, "name");
			//int prjId = (int) DBI.getLong(dbi, "SELECT prj_id FROM " + dbD + ".ds WHERE id = " + dsIdSrc, "prj_id");
			dsIdDst = createDSTbl(dbi, stX, username, prjId, Do.getAppIdData(), dsNameDst, "Discret: '" + dsName + "' (" + diVarNames + ")", tsVar, 0, vars);
			
			// Insert data:
			StringBuilder qry01 = new StringBuilder("INSERT INTO " + dbDS + ".ds_" + dsIdDst + " (" + qryVars + ") VALUES ");
			ResultSet rsData = dbi.execQry("SELECT " + qryVars.toString() + " FROM " + dbDS + ".ds_" + dsIdSrc + " ORDER BY " + ID_COL_NAME);
			while (rsData.next()) {
				StringBuilder qry02 = new StringBuilder();
				itVar = vars.iterator();
				while (itVar.hasNext()) {
					Var v = itVar.next();
					
					if (DU.containsKey(v.getName())) {
						// check for missing value:
						String s = rsData.getString(v.getName());
						Double d = (s == null ? null : rsData.getDouble(v.getName()));
						
						// proceed:
						qry02.append(DU.get(v.getName()).getValSQL(d));
					}
					else qry02.append(Var.getSQLVal(v.getType(), rsData.getString(v.getName())));
					
					if (itVar.hasNext()) qry02.append(",");
				}
				
				stX.executeUpdate(qry01.toString() + "(" + qry02.toString() + ")");
			}
			rsData.close();
			
			DBI.transCommit(stX, true);
		}
		catch (SQLException e)           { DBI.transRollback(stX, true); throw e; }
		catch (RserveException e)        { DBI.transRollback(stX, true); throw e; }
		catch (REXPMismatchException e)  { DBI.transRollback(stX, true); throw e; }
		catch (InstantiationException e) { DBI.transRollback(stX, true); throw e; }
		catch (IllegalAccessException e) { DBI.transRollback(stX, true); throw e; }
		catch (ClassNotFoundException e) { DBI.transRollback(stX, true); throw e; }
		catch (Exception e)              { DBI.transRollback(stX, true); throw e; }
		
		return new Result(true, "ds:" + get(dbi, username, dsIdDst));
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Duplicates a data set. If `prjId` is equal to `DBI.ID_NONE,` the original data set's project is assumed.
	 * 
	 * OUT: the ID of the new data set.
	 */
	public static int dup(DBI dbi, String username, Statement stX, int dsId, int prjId, String name, String memo, String sqlWhere, int obsCnt, boolean withData, boolean consolidateTSVar, int tsVarMin)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		String dbD = "\"" + Do.DB_DATA + "-" + username + "\"";
		String dbDS = "\"" + Do.DB_DATA_DS + "-" + username + "\"";
		
		int dsIdNew = DBI.ID_NONE;
		
		if (prjId == DBI.ID_NONE) prjId = (int) DBI.getLong(dbi, "SELECT prj_id FROM " + dbD + ".ds WHERE id = " + dsId, "prj_id");
		String tsVar = DBI.getStr(dbi, "SELECT ts_var FROM " + dbD + ".ds WHERE id = " + dsId, "ts_var");
		
		// Data set:
		dsIdNew = DBI.insertGetId(stX, null, "INSERT INTO " + dbD + ".ds (prj_id, app_id, name, memo, ts_var, calc_time, size, obs_cnt) VALUES (" + prjId + "," + Do.getAppIdData() + "," + DBI.esc(name) + "," + DBI.esc(memo) + "," + DBI.esc(tsVar) + ",0," + (DBI.REC_SIZE_DATA_SET + name.length()) + "," + obsCnt + ")");
		stX.executeUpdate("CREATE TABLE " + dbDS + ".ds_" + dsIdNew + " AS SELECT * FROM " + dbDS + ".ds_" + dsId + (sqlWhere == null || sqlWhere.length() == 0 ? "" : " WHERE " + sqlWhere) + " WITH " + (withData ? "" : "NO ") + "DATA");
		stX.executeUpdate("ALTER TABLE " + dbDS + ".ds_" + dsIdNew + " ADD CONSTRAINT ds_" + dsIdNew + "_pkey PRIMARY KEY (" + ID_COL_NAME + ")");
		
		// Variables:
		int i = 0;
		StringBuilder qryVars = new StringBuilder("INSERT INTO " + dbD + ".var (ds_id, type_id, name, memo, ord) VALUES ");
		ResultSet rsVar = stX.executeQuery("SELECT type_id, name, memo FROM " + dbD + ".var WHERE ds_id = " + dsId + " ORDER BY ord");
		while (rsVar.next()) {
			qryVars.append("(" + dsIdNew + "," + rsVar.getInt("type_id") + "," + DBI.esc(rsVar.getString("name")) + "," + DBI.esc(rsVar.getString("memo")) + "," + (i++) + ")" + (!rsVar.isLast() ? "," : ""));
		}
		rsVar.close();
		stX.executeUpdate(qryVars.toString());
		
		if (consolidateTSVar) {
			// TODO
		}
		
		return dsIdNew;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Duplicates a data set. If `prjId` is equal to `DBI.ID_NONE,` the original data set's project is assumed.
	 */
	public static Result dup(DBI dbi, String username, int prjId, String dsNameSrc, String dsNameDst, String cond)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException, RserveException, REXPMismatchException {
		String dbD = "\"" + Do.DB_DATA + "-" + username + "\"";
		String dbDS = "\"" + Do.DB_DATA_DS + "-" + username + "\"";
		
		String sqlWhere = cond;  // TODO: decode condition into the actual SQL
		
		int dsIdSrc = getId(dbi, username, prjId, dsNameSrc);
		int dsIdDst = DBI.ID_NONE;
		
		DBI dbiX = new DBI();
		Statement stX = dbiX.transBegin();
		try {
			// Data set:
			dsIdDst = DBI.insertGetId(stX, null, "INSERT INTO " + dbD + ".ds (prj_id, app_id, name, memo, ts_var, calc_time, size, obs_cnt) SELECT " + prjId + "," + Do.getAppIdData() + "," + DBI.esc(dsNameDst) + ", memo, ts_var, 0, size, " + (sqlWhere == null || sqlWhere.length() == 0 ? "obs_cnt" : "(SELECT COUNT(*) FROM " + dbDS + ".ds_" + dsIdSrc + " WHERE " + sqlWhere + ")") + " FROM " + dbD + ".ds WHERE id = " + dsIdSrc);
			stX.executeUpdate("CREATE TABLE " + dbDS + ".ds_" + dsIdDst + " AS SELECT * FROM " + dbDS + ".ds_" + dsIdSrc + (sqlWhere == null || sqlWhere.length() == 0 ? "" : " WHERE " + sqlWhere) + " WITH DATA");
			stX.executeUpdate("ALTER TABLE " + dbDS + ".ds_" + dsIdDst + " ADD CONSTRAINT ds_" + dsIdDst + "_pkey PRIMARY KEY (" + ID_COL_NAME + ")");
			
			// Variables:
			int varCnt = 0;
			StringBuilder qryVars = new StringBuilder("INSERT INTO " + dbD + ".var (ds_id, type_id, name, memo, ord) VALUES ");
			ResultSet rsVar = stX.executeQuery("SELECT type_id, name, memo FROM " + dbD + ".var WHERE ds_id = " + dsIdSrc + " ORDER BY ord");
			while (rsVar.next()) {
				qryVars.append("(" + dsIdDst + "," + rsVar.getInt("type_id") + "," + DBI.esc(rsVar.getString("name")) + "," + DBI.esc(rsVar.getString("memo")) + "," + (varCnt++) + ")" + (!rsVar.isLast() ? "," : ""));
			}
			rsVar.close();
			if (varCnt > 0) stX.executeUpdate(qryVars.toString());
			
			// Commit:
			DBI.transCommit(stX, true);
		}
		catch (SQLException e) {
			DBI.transRollback(stX, true);
			
			if (e.getErrorCode() == DBI.SQL_ERR_DUP_ENTRY) return new Result(false, "msg:\"A data set with the specified name already exists.\n\nIf you cannot see such data set in the list, it is possible that someone else has added it. Please remember that this is a multiuser system and more than one person can be working on the same project and/or data set at the same time. Consider refreshing the page.\"");
			else throw e;
		}
		
		// (5) Finish up:
		return new Result(true, "ds:" + get(dbi, username, dsIdDst));
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static void exp(DBI dbi, String username, int prjId, String dsName, String filename, String formatStr, String delimVar, String delimObs, String missVal, boolean zip, Writer out)  throws SQLException, IOException {
		exp(dbi, username, getId(dbi, username, prjId, dsName), filename, formatStr, delimVar, delimObs, missVal, zip, out);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	// SELECT a.val AS val_a, a.i AS i_a, b.val AS val_b, b.i AS i_b FROM (SELECT @i:=@i+1 AS i, val FROM data, (SELECT @i:=0) tmp WHERE var_id=9) a, (SELECT @j:=@j+1 AS i, val FROM data, (SELECT @j:=0) tmp WHERE var_id=10) b WHERE a.i = b.i;
	/**
	 * I was getting the following PostgreSQL error with tables (data sets) with about 900 columns (variables):
	 * 'stack depth limit exceeded.' I was able to make them go away by increasing the 'max_stack_depth' parameter in 
	 * the '/usr/local/pgsql/data/postgresql.conf' file from '2MB' to '4MB'. 
	 */
	public static void exp(DBI dbi, String username, int dsId, String filename, String formatStr, String delimVar, String delimObs, String missVal, boolean zip, Writer out)  throws SQLException, IOException {
		int format = EXP_FORMAT_TXT;
		if (formatStr.equalsIgnoreCase("html")) format = EXP_FORMAT_HTML; 
		
		/*
		if (format == EXP_FORMAT_TXT) {
			// psql -U postgres -d pegasus -c "COPY \"dds-tomek\".ds_1 TO stdout (FORMAT csv, DELIMITER ' ', NULL '.', HEADER);" > /tmp/ds.txt
			dbi.execQry("COPY \"" + Do.DB_DATA_DS + "-" + username + "\".ds_" + dsId + " TO '" + filename + "");
			return;
		}
		*/
		
		String dbDS = "\"" + Do.DB_DATA_DS + "-" + username + "\"";
		
		delimVar = Do.delim.get(delimVar);
		delimObs = Do.delim.get(delimObs);
		
		switch (format) {
			case EXP_FORMAT_HTML : out.append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\"><html><head><title>(first " + EXP_PAGE_LIMIT + " observations)</title><link rel=\"stylesheet\" type=\"text/css\" href=\"style/ds-exp-html.css\"></head><body><table cellpadding=\"0\" cellspacing=\"0\"><tr>");  break;
		}
		//int varCnt = (int) DBI.getLong(dbi, "SELECT COUNT(*) AS cnt FROM " + dbD + " WHERE ds_id = " + dsId, "cnt");
		
		StringBuilder qryObs = new StringBuilder("SELECT ");
		ResultSet rsVars = dbi.execQry("SELECT name, type_id FROM \"" + Do.DB_DATA + "-" + username + "\".var WHERE ds_id = " + dsId + " ORDER BY ord");
		while (rsVars.next()) {
			String vname = rsVars.getString("name");
			switch (format) {
				case EXP_FORMAT_TXT  : out.append(vname + (!rsVars.isLast() ? delimVar : ""));  break;
				case EXP_FORMAT_HTML : out.append("<th>" + vname + "</th>");  break;
			}
			
			int type = rsVars.getInt("type_id");
			String val = "";
			switch (type) {
				case Var.TYPE_BIN:
				case Var.TYPE_CAT:
				case Var.TYPE_ORD:
				case Var.TYPE_DIS: val = "\"" + vname + "\"::text"; break;
				case Var.TYPE_CON: val = "CASE WHEN \"" + vname + "\" = 0 THEN '0' ELSE trim(trailing '.' from trim(trailing '0' from \"" + vname + "\"::text)) END"; break;
				case Var.TYPE_STR: val = "\"" + vname + "\""; break;
			}
			switch (format) {
				case EXP_FORMAT_TXT  : qryObs.append("COALESCE(" + val + ", '" + missVal + "')" + (!rsVars.isLast() ? " || '" + delimVar + "' || ": ""));  break;
				case EXP_FORMAT_HTML : qryObs.append("'<td>' || COALESCE(" + val + ", '" + missVal + "') || '</td>'" + (!rsVars.isLast() ? " || ": ""));  break;
			}
		}
		rsVars.close();
		
		switch (format) {
			case EXP_FORMAT_TXT  : out.append(delimObs);  break;
			case EXP_FORMAT_HTML : out.append("</tr>");  break;
		}
		
		qryObs.append(" AS obs FROM " + dbDS + ".ds_" + dsId + " ORDER BY " + ID_COL_NAME + (format == EXP_FORMAT_HTML ? " LIMIT " + EXP_PAGE_LIMIT : ""));
		
		//System.out.println(qryObs.toString());
		//int i = 0;
		ResultSet rsObs = dbi.execQry(qryObs.toString());
		while (rsObs.next()) {
			switch (format) {
				case EXP_FORMAT_TXT  : out.append(rsObs.getString("obs") + (!rsObs.isLast() ? delimObs : ""));  break;
				case EXP_FORMAT_HTML : out.append("<tr>" + rsObs.getString("obs") + "</tr>");  break;
			}
			//if (++i == 100) { out.flush(); i = 0; };
		}
		
		switch (format) {
			case EXP_FORMAT_HTML : out.append("</table></body></html>");  break;
		}
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static Result genSpreadsheet(DBI dbi, String username, String dsIds) {
		return new Result(false, "msg:\"Not implemented yet\"");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Generates a new data sets with the designated statistics of the designated variables given the specified 
	 * grouping variables. The specification format is as follows:
	 * 
	 *   <grouping variables>;<statistics variables>;<statistics>
	 * 
	 *  e.g.,
	 * 
	 *   r;em_gd;min
	 *   r,sub,trial,stim,nevt,em_wlen;em_sfd,em_gd;min,max,stddev
	 */
	public static Result genStats(DBI dbi, String username, int prjId, String dsNameSrc, String dsNameDst, String spec, String sep)  throws Exception {
		String dbD = "\"" + Do.DB_DATA + "-" + username + "\"";
		String dbDS = "\"" + Do.DB_DATA_DS + "-" + username + "\"";
		
		int dsIdSrc = getId(dbi, username, prjId, dsNameSrc);
		int dsIdDst = DBI.ID_NONE;
		
		// (1) Decode and validate the specification:
		spec = spec.toLowerCase();
		
		// (1.1) Split and check for format errors:
		String[] A = spec.split(";");
		if (A.length != 3) return new Result(false, "msg:\"Incorrect specification: All three componenet must be specified, (1) grouping variables, (2) statistics variables, and (3) statistics.\"");
		String[] arrVarsGrp  = A[0].split(",");
		String[] arrVarsStat = A[1].split(",");
		String[] arrStats    = A[2].split(",");
		
		// (1.2) Statistics:
		if (arrStats.length == 0) return new Result(false, "msg:\"Incorrect specification: At least one statistics has to be specified.\"");
		for (String s: arrStats) {
			if (!s.equals("avg") && !s.equals("bit_and") && !s.equals("bit_or") && !s.equals("bool_and") && !s.equals("bool_or") && !s.equals("count") && !s.equals("every") && !s.equals("max") && !s.equals("min") && !s.equals("stddev") && !s.equals("sum") && !s.equals("variance")) return new Result(false, "msg:\"Incorrect specification: Unknown statistics '" + s + "' specified.\"");
		}
		
		// (1.3) Grouping variables:
		List<Var> varsGrp = new ArrayList<Var>();
		for (String s: arrVarsGrp) {
			if (DBI.getLong(dbi, "SELECT COUNT(*) AS cnt FROM " + dbD + ".var WHERE ds_id = " + dsIdSrc + " AND name = '" + s + "'", "cnt") == 0) return new Result(false, "msg:\"Incorrect specification: All grouping variables must be extant.\"");
			
			ResultSet rs = dbi.execQry("SELECT id, type_id, name, ord FROM " + dbD + ".var WHERE ds_id = " + dsIdSrc + " AND name = '" + s + "' ORDER BY ord");
			rs.next();
			
			varsGrp.add(new Var(rs.getInt("id"), rs.getInt("type_id"), s, null));
		}
		
		// (1.4) Statistic variables:
		List<Var> varsStat = new ArrayList<Var>();
		for (String s: arrVarsStat) {
			if (DBI.getLong(dbi, "SELECT COUNT(*) AS cnt FROM " + dbD + ".var WHERE ds_id = " + dsIdSrc + " AND name = '" + s + "'", "cnt") == 0) return new Result(false, "msg:\"Incorrect specification: All statistic variables must be extant.\"");
			
			ResultSet rs = dbi.execQry("SELECT id, type_id, name, ord FROM " + dbD + ".var WHERE ds_id = " + dsIdSrc + " AND name = '" + s + "' ORDER BY ord");
			rs.next();
			
			int type = rs.getInt("type_id");
			if (type == Var.TYPE_STR || type == Var.TYPE_UND) return new Result(false, "msg:\"Incorrect specification: All statistics variables need to be numerical.\"");
			
			varsStat.add(new Var(rs.getInt("id"), type, s, null));
		}
		
		// (3) Generate the new variables list and the data query:
		StringBuilder qryData = new StringBuilder("(SELECT \"" + A[0].replaceAll(",", "\",\"") + "\", ");
		
		List<Var> varsNew = new ArrayList<Var>();
		varsNew.addAll(varsGrp);
		for (Var v: varsStat) {
			for (String stat: arrStats) {
				String name = v.getName();
				
				// Determine the type:
				int type = DBI.ID_NONE;
				if (stat.equals("avg") || stat.equals("stddev") || stat.equals("variance")) type = Var.TYPE_CON;
				else if (stat.equals("count")) type = Var.TYPE_DIS;
				else if (stat.equals("bool_and") || stat.equals("bool_or") || stat.equals("every")) type = Var.TYPE_BIN;
				else type = v.getType();
				
				qryData.append(stat + "(\"" + name + "\") AS \"" + name + sep + stat + "\", ");
				varsNew.add(new Var(DBI.ID_NONE, type, name + sep + stat, null));
			}
		}
		
		qryData.delete(qryData.length()-2, qryData.length()-1);
		qryData.append("FROM " + dbDS + ".ds_" + dsIdSrc + " GROUP BY \"" + A[0].replaceAll(",", "\",\"") + "\" ORDER BY \"" + A[0].replaceAll(",", "\",\"") + "\")");
		
		// (4) Generate statistics:
		Statement stX = (new DBI()).transBegin();
		try {
			// (4.1) Create the new data set:
			dsIdDst = createDSTbl(dbi, stX, username, prjId, Do.getAppIdData(), dsNameDst, "Statistics: (1) ds:'" + dsNameSrc + "', (2) spec:" + spec + ")", null, 0, varsNew);
			
			// (4.2) Populate the data set:
			StringBuffer qryTmp = new StringBuffer("INSERT INTO " + dbDS + ".ds_" + dsIdDst + " (");
			for (int i = 0; i < varsNew.size(); i++) {
				qryTmp.append("\"" + varsNew.get(i).getName() + "\"" + (i < varsNew.size()-1 ? ", " : ""));
			}
			qryTmp.append(") ");
			qryData.insert(0, qryTmp);
			System.out.println(qryData.toString());
			stX.executeUpdate(qryData.toString());
			
			DBI.transCommit(stX, true);
		}
		catch (SQLException e)           { DBI.transRollback(stX, true); throw e; }
		catch (RserveException e)        { DBI.transRollback(stX, true); throw e; }
		catch (REXPMismatchException e)  { DBI.transRollback(stX, true); throw e; }
		catch (InstantiationException e) { DBI.transRollback(stX, true); throw e; }
		catch (IllegalAccessException e) { DBI.transRollback(stX, true); throw e; }
		catch (ClassNotFoundException e) { DBI.transRollback(stX, true); throw e; }
		catch (Exception e)              { DBI.transRollback(stX, true); throw e; }
		
		return new Result(true, "ds:" + get(dbi, username, dsIdDst));
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	@SuppressWarnings("deprecation")
	public static StringBuffer get(DBI dbi, String username, int dsId)  throws SQLException, RserveException, REXPMismatchException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		String dbD = "\"" + Do.DB_DATA + "-" + username + "\"";
		StringBuffer res = new StringBuffer();
		
		// (1) Data set:
		ResultSet rs = dbi.execQry(
			"SELECT " +
			"d.id, a.name AS app, d.name, d.memo, d.created, d.ts_var, d.calc_time, d.obs_cnt, d.size, d.note, LENGTH(d.note) AS note_len," +
			"(SELECT COUNT(*) FROM " + dbD + ".var WHERE ds_id = " + dsId + ") AS var_cnt " +
			"FROM " + dbD + ".ds d " +
			"INNER JOIN \"" + Do.DB_MAIN + "\".app a ON d.app_id = a.id " +
			"WHERE d.id = " + dsId
		);
		
		rs.next();
		res.append(
			"{" +
			"loaded:true," +
			"id:" + rs.getString("id") + "," +
			"app:" + JSI.str2js(rs.getString("app")) + "," +
			"name:" + JSI.str2js(rs.getString("name")) + "," +
			"memo:" + JSI.str2js(rs.getString("memo")) + "," +
			"created:" + (rs.getTimestamp("created").getYear() == 0 ? "null" : "new Date(" + rs.getTimestamp("created").getTime() + ")") + "," +
			"tsVar:" + JSI.str2js(rs.getString("ts_var")) + "," +
			"calcTime:\"" + Convert.time2str(rs.getLong("calc_time")) + "\"," +
			"varCnt:" + rs.getInt("var_cnt") + "," +
			"obsCnt:" + rs.getInt("obs_cnt") + "," +
			"size:" + rs.getInt("size") + "," +
			"noteLen:" + rs.getInt("note_len") + "," +
			"var:" + Var.getLst(dbi, username, dsId) + "," +
			"obs:null," +
			"rep:" + Rep.getLst(dbi, username, dsId) + "," +
			"rInt:" + getRInt(dbi, username, dsId) + "," +
			"note:" + JSI.str2js(rs.getString("note")) +
			"}"
		);
		rs.close();
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static long getCnt(DBI dbi, String username)  throws SQLException {
		return DBI.getLong(dbi, "SELECT COUNT(id) AS cnt FROM \"" + Do.DB_DATA + "-" + username + "\".ds", "cnt");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	private static int getId(DBI dbi, String username, int prjId, String dsName)  throws SQLException {
		return (int) DBI.getLong(dbi, "SELECT id FROM \"" + Do.DB_DATA + "-" + username + "\".ds WHERE prj_id = " + prjId + " AND name = '" + dsName + "'", "id");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * If 'dsId' is not 'null', that data set will be returned in its entirety. 'ids' is a list of comma-delimited 
	 * IDs of data sets. No check is performed if it's 'null' or has a zero length.
	 */
	public static StringBuffer getLst(DBI dbi, String username, Integer prjId, String dsIds, Integer dsId)  throws SQLException, UnsupportedEncodingException, InstantiationException, IllegalAccessException, ClassNotFoundException, RserveException, REXPMismatchException {
		if (dsIds != null && dsIds.length() == 0) return new StringBuffer("[]");
		
		String dbD = "\"" + Do.DB_DATA + "-" + username + "\"";
		StringBuffer res = new StringBuffer("[");
		
		List<String> lstQryWhere = new ArrayList<String>();
		if (prjId != null) lstQryWhere.add("d.prj_id = " + prjId.intValue());
		if (dsIds != null && dsIds.length() > 0) lstQryWhere.add("d.id IN (" + dsIds + ")");
		
		String qryWhere = (lstQryWhere.size() == 0 ? "" : "WHERE " + $.join(lstQryWhere, "", "", " AND ", false, false) + " ");
		
		ResultSet rs = dbi.execQry(
			"SELECT d.id, a.name AS app, d.name, d.memo, d.created, d.ts_var, d.calc_time, tmp01.var_cnt, d.obs_cnt, d.size, LENGTH(d.note) AS note_len " +
			"FROM " + dbD + ".ds d " +
			"INNER JOIN \"" + Do.DB_MAIN + "\".app a ON d.app_id = a.id " +
			"LEFT JOIN (SELECT ds_id, COUNT(*) AS var_cnt FROM " + dbD + ".var GROUP BY ds_id) tmp01 ON tmp01.ds_id = d.id " +
			qryWhere +
			"ORDER BY created DESC, name DESC"
		);
		
		while (rs.next()) {
			int id = rs.getInt("id");
			String memo = rs.getString("memo");
			
			if (dsId != null && dsId.intValue() == id) res.append(get(dbi, username, id));
			else {
				res.append(
					"{" +
					"loaded:false," +
					"id:" + id + "," +
					"app:" + JSI.str2js(rs.getString("app")) + "," +
					"name:" + JSI.str2js(rs.getString("name")) + "," +
					(memo != null && memo.length() > 0 ? "memo:" + JSI.str2js(memo) + "," : "") +
					"created:" + "new Date(" + rs.getTimestamp("created").getTime() + ")," +
					"tsVar:" + JSI.str2js(rs.getString("ts_var")) + "," +
					"calcTime:\"" + Convert.time2str(rs.getLong("calc_time")) + "\"," +
					"varCnt:" + rs.getInt("var_cnt") + "," +
					"obsCnt:" + rs.getInt("obs_cnt") + "," +
					"size:" + rs.getInt("size") + "," +
					"noteLen:" + rs.getInt("note_len") + "," +
					"var:null," +
					"obs:null," +
					"rep:null," +
					"histR:null" +
					"}"
				);
			}
			if (!rs.isLast()) res.append(",");
		}
		rs.close();
		res.append("]");
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static StringBuffer getRInt(DBI dbi, String username, int dsId)  throws SQLException {
		String dbD = "\"" + Do.DB_DATA + "-" + username + "\"";
		StringBuffer res = new StringBuffer("{");
		
		int cnt = (int) DBI.getLong(dbi, "SELECT COUNT(*) AS cnt FROM " + dbD + ".r_int WHERE ds_id = " + dsId, "cnt");
		res.append("cmdCnt:" + cnt + ",");
		
		ResultSet rs = dbi.execQry("SELECT id, cmd, res, res_ln_cnt FROM " + dbD + ".r_int WHERE ds_id = " + dsId + " ORDER BY id " + (cnt > RINT_MAX_CMD_CNT ? "LIMIT " + RINT_MAX_CMD_CNT + " OFFSET " + (cnt - RINT_MAX_CMD_CNT) : ""));
		res.append("recent:[");
		while (rs.next()) {
			String sCmd = rs.getString("cmd");
			String sRes = rs.getString("res");
			int resLnCnt = rs.getInt("res_ln_cnt");
			boolean isTooLong = resLnCnt > RINT_MAX_RES_LN_CNT;
			
			res.append(
				"{" +
				"id:" + rs.getInt("id") + "," +
				"full:" + (!isTooLong) + "," +
				"cmd:" + JSI.str2js(sCmd) + "," +
				"res:" + JSI.str2js(!isTooLong ? sRes : sRes.substring(0, $.idxOfNth(sRes, "\n", RINT_MAX_RES_LN_CNT) - 1)) +
				(isTooLong ? ",resLnCnt:" + resLnCnt : "") +
				"}" + (!rs.isLast() ? "," : "")
			);
		}
		rs.close();
		res.append("]}");
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static int getSize(DBI dbi, String username, int dsId)  throws SQLException {
		return (int) DBI.getLong(dbi, "SELECT size FROM \"" + Do.DB_DATA + "-" + username + "\".ds WHERE id = " + dsId, "size");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * The value of `modifier` is added to the calculated size of the table. It can be negative or positive. It should
	 * be used when the size is to be updated from within a transaction. That is, in circumstances where the size of 
	 * the table has not changed yet, but it will once the changes are commited. That ovciously implies, that the 
	 * calculation of the magnitude of those changes need to happen elsewhere.
	 * 
	 * [B].
	 */
	public static String getSQLUpdateMetaInfo(DBI dbi, String username, int dsId, Long calcTime) throws SQLException {
		String dbD   = "\"" + Do.DB_DATA    + "-" + username + "\"";
		String dbDDS = "\"" + Do.DB_DATA_DS + "-" + username + "\"";
		
		long varCnt   = DBI.getLong(dbi, "SELECT COUNT(*) AS cnt FROM " + dbD + ".var WHERE ds_id = " + dsId, "cnt");
		long obsCnt   = DBI.getLong(dbi, "SELECT COUNT(*) AS cnt FROM " + dbDDS + ".ds_" + dsId, "cnt");
		
		long sizeName = DBI.getLong(dbi, "SELECT LENGTH(name) AS len FROM " + dbD + ".ds WHERE id = " + dsId, "len");
		long sizeMemo = DBI.getLong(dbi, "SELECT LENGTH(memo) AS len FROM " + dbD + ".ds WHERE id = " + dsId, "len");
		long sizeRep  = DBI.getLong(dbi, "SELECT COALESCE(SUM(size),0) AS size FROM " + dbD + ".rep WHERE ds_id = " + dsId, "size");
		long sizeTbl  = DBI.getTblSize(dbi, dbDDS, "ds_" + dsId);
		long sizeMisc = DBI.REC_SIZE_DATA_SET + DBI.REC_SIZE_VAR * varCnt;
		
		long size     = sizeName + sizeMemo + sizeRep + sizeTbl + sizeMisc;
		
		return "UPDATE " + dbD  + ".ds SET obs_cnt = " + obsCnt + ", size = " + size + (calcTime == null ? "" : ", calc_time = " + calcTime) + " WHERE id = " + dsId;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	private static boolean hasVar(DBI dbi, String username, int dsId, String varName)  throws SQLException {
		String dbD = "\"" + Do.DB_DATA + "-" + username + "\"";
		return (DBI.getLong(dbi, "SELECT COUNT(id) AS cnt FROM " + dbD + ".var WHERE ds_id = " + dsId + " AND name = " + DBI.esc(varName), "cnt") == 1);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Import a data set inferring the variable types.
	 */
	@SuppressWarnings("unchecked")
	public static Result impTxt(DBI dbi, String username, int dsId, String file, String delim, String missVal)  throws SQLException, IOException, RserveException, REXPMismatchException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		String dbD = "\"" + Do.DB_DATA + "-" + username + "\"";
		String dbDS = "\"" + Do.DB_DATA_DS + "-" + username + "\"";
		
		List<String[]> rows = FSI.file2lstSplit(file, delim, 0);
		if (rows.size() < 2) return new Result(false, "msg:\"A data set needs to contain a header row and at least one data row.\"");
		
		int varCnt = rows.get(0).length;
		
		// (1) Check the size of each row:
		Iterator<String[]> it = rows.iterator();
		while (it.hasNext()) {
			if (it.next().length != varCnt) return new Result(false, "msg:\"The data set contains rows of unequal lengths. Please make sure that the number of values in each row is equal to the number of variables in the data set.\"");
		}
		
		// (2) Check column names:
		String[] rowH = rows.get(0);
		for (String r: rowH) {
			if (r.equals(DataSet.ID_COL_NAME)) return new Result(false, "msg:\"The name '" + DataSet.ID_COL_NAME + "' is reserved and cannot be used as a name of a variable. Please provide a different name for that column of the data set and try again.\"");
		}
		
		// (3) Determine data types:
		Set<String>[] vals = new HashSet[varCnt];  // to establish the number of unique values of a variable
		int[] types = new int[varCnt];
		for (int i = 0; i < varCnt; i++) {
			types[i] = Var.TYPE_DIS;
			vals[i] = new HashSet<String>();
		}
		
		it = rows.iterator();
		it.next();  // skip header row
		while (it.hasNext()) {
			String[] row = it.next();
			for (int i = 0; i < varCnt; i++) {
				if (types[i] == Var.TYPE_STR) continue;  // if already established that it's a string then move on
				
				String val = row[i];
				if (val.equals(missVal)) continue;  // skip missing values
				
				vals[i].add(val);
				
				try {
					new Integer(val);
				}
				catch (NumberFormatException e01) {
					try {
						new Double(val);
						types[i] = Var.TYPE_CON;
					}
					catch (NumberFormatException e02) {
						types[i] = Var.TYPE_STR;
					}
				}
			}
		}
		
		for (int i = 0; i < varCnt; i++) {
			if (types[i] == Var.TYPE_DIS && vals[i].size() == 2) types[i] = Var.TYPE_BIN;
		}
		
		// (4) Perform the import:
		DBI dbiX = new DBI();
		Statement stX = dbiX.transBegin();
		try {
			// (4.1) Structure:
			stX.executeUpdate("DROP TABLE " + dbDS + ".ds_" + dsId);
			stX.executeUpdate("UPDATE " + dbD + ".ds SET ts_var = NULL, calc_time = 0, size = " + DBI.REC_SIZE_DATA_SET + " WHERE id = " + dsId);
			stX.executeUpdate("DELETE FROM " + dbD + ".var WHERE ds_id = " + dsId);
			stX.executeUpdate("DELETE FROM " + dbD + ".rep WHERE ds_id = " + dsId);
			
			StringBuilder qryTbl = new StringBuilder(
				"CREATE TABLE " + dbDS + ".\"ds_" + dsId + "\" (" +
				"\"" + ID_COL_NAME + "\" serial,"  // MySQL: "\"" + ID_COL_NAME + "\" INT UNSIGNED NOT NULL AUTO_INCREMENT,"
			);
			
			StringBuilder qryDataRowPre = new StringBuilder("INSERT INTO " + dbDS + ".ds_" + dsId + " (");
			
			for (int i = 0; i < varCnt; i++) {
				stX.executeUpdate("INSERT INTO " + dbD + ".var (ds_id, type_id, name, ord) VALUES (" + dsId + "," + types[i] + "," + DBI.esc(rowH[i]) + "," + i + ")");
				
				qryTbl.append("\"" + rowH[i] + "\" " + Var.getSQLColType(types[i]) + " NULL,");
				qryDataRowPre.append("\"" + rowH[i] + "\"" + (i < varCnt-1 ? "," : ""));
			}
			
			qryTbl.append("PRIMARY KEY (\"" + ID_COL_NAME + "\"))");
			qryDataRowPre.append(") VALUES (");
			
			stX.executeUpdate(qryTbl.toString());
			
			// (4.2) Data:
			it = rows.iterator();
			it.next();  // skip header row
			while (it.hasNext()) {
				String[] row = it.next();
				StringBuilder qryDataRow = new StringBuilder(qryDataRowPre);
				for (int i = 0; i < varCnt; i++) {
					String val = row[i];
					if (val.equals(missVal)) val = null;
					else if (types[i] == Var.TYPE_STR) val = DBI.esc(val);
					
					qryDataRow.append(val + (i < varCnt-1 ? "," : ""));
				}
				qryDataRow.append(")");
				stX.executeUpdate(qryDataRow.toString());
			}
			
			// (4.3) Meta info:
			stX.executeUpdate(getSQLUpdateMetaInfo(dbiX, username, dsId, 0l));
			
			DBI.transCommit(stX, true);
		}
		catch (SQLException e) {
			DBI.transRollback(stX, true);
			throw e;
		}
		
		return new Result(true, "ds:" + get(dbi, username, dsId));
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	// Excel import: http://epramono.blogspot.com/2004/12/poi-for-excel-parser.html
	public static Result impExcel(DBI dbi, String username, int dsId, String file, String delim, String missVal) {
		return new Result(false, "msg:\"Not implemented yet\"");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static Result merge(DBI dbi, String username, int prjId, String name, String idxVarName, String spec)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException, RserveException, REXPMismatchException {
		String dbD = "\"" + Do.DB_DATA + "-" + username + "\"";
		String dbDS = "\"" + Do.DB_DATA_DS + "-" + username + "\"";
		
		if (idxVarName != null && idxVarName.length() == 0) idxVarName = null;
		
		// (1) Init:
		String[] specSplit = spec.split(",");
		
		if (specSplit.length < 2) return new Result(false, "msg:\"Please specify at least two data sets to be merged.\"");
		
		int[] dsIdxs = new int[specSplit.length];
		int[] dsIds = new int[specSplit.length];
		String dsIdsStr = "";
		
		for (int i = 0; i < specSplit.length; i++) {
			String[] tmp = $.regExpr2arr(specSplit[i], "'([^']+)':(\\d+)");
			dsIdxs[i] = (new Integer(tmp[1])).intValue();
			dsIds[i] = (int) DBI.getLong(dbi, "SELECT id FROM " + dbD + ".ds WHERE prj_id = " + prjId + " AND name = " + DBI.esc(tmp[0]), "id");
			
			dsIdsStr += dsIds[i] + (i < specSplit.length - 1 ? "," : "");
		}
		
		// (2) Check variable names and types:
		List<Var> vars = new ArrayList<Var>();
		if (idxVarName != null) vars.add(new Var(DBI.ID_NONE, Var.TYPE_DIS, idxVarName, null));
		
		StringBuilder varsAll = new StringBuilder();
		StringBuilder varsFirst = null;
		for (int dsId: dsIds) {
			StringBuilder varsCurr = new StringBuilder();
			ResultSet rsTmp = dbi.execQry("SELECT name, type_id FROM " + dbD + ".var WHERE ds_id = " + dsId + " ORDER BY ord");
			while (rsTmp.next()) {
				int varType = rsTmp.getInt("type_id");
				String varName = rsTmp.getString("name");
				
				if (varsFirst == null) {
					vars.add(new Var(DBI.ID_NONE, varType, varName, null));
					varsAll.append(varName + (!rsTmp.isLast() ? "," : ""));
				}
				
				varsCurr.append(varName + "::" + varType + "---");
			}
			rsTmp.close();
			
			if (varsFirst == null) varsFirst = varsCurr;
			else if (!varsFirst.toString().equals(varsCurr.toString())) return new Result(false, "msg:\"Data sets to be merged need to have (1) the same amount of variables with (2) the same names, (3) the same types, and (4) appearing in the same order.  The selected data sets do not meet these criteria.\"");
		}
		
		// (3) Acquire additional info:
		ResultSet rsInf = dbi.execQry("SELECT prj_id, ts_var FROM " + dbD + ".ds WHERE id IN (" + dsIdsStr + ") LIMIT 1");
		rsInf.next();
		String tsVar = rsInf.getString("ts_var");
		rsInf.close();
		
		StringBuilder memo = new StringBuilder("Merged: ");
		for (int i = 0; i < dsIds.length; i++) {
			memo.append("'" + DBI.getStr(dbi,"SELECT name FROM " + dbD + ".ds WHERE id = " + dsIds[i], "name") + "' [" + dsIdxs[i] + "]" + (i < dsIds.length - 1 ? ", " : ""));
		}
		
		// (4) Create the new data set:
		DBI dbiX = new DBI();
		Statement stX = dbiX.transBegin();
		int dsIdNew = DBI.ID_NONE;
		
		try {
			// (4.1) Create the new table:
			dsIdNew = createDSTbl(dbi, stX, username, prjId, Do.getAppIdData(), name, memo.toString(), tsVar, 0, vars);
			
			// (4.2) Copy data:
			stX.execute("SET search_path TO " + dbDS);
			for (int i = 0; i < dsIds.length; i++) {
				stX.executeUpdate("INSERT INTO ds_" + dsIdNew + " (" + (idxVarName != null ? "\"" + idxVarName + "\"," : "") + varsAll + ") SELECT " + (idxVarName != null ? dsIdxs[i] + "," : "") + varsAll + " FROM ds_" + dsIds[i] + " ORDER BY " + ID_COL_NAME);
			}
			stX.executeUpdate(getSQLUpdateMetaInfo(dbiX, username, dsIdNew, 0l));
			
			DBI.transCommit(stX, true);
		}
		catch (SQLException e) {
			DBI.transRollback(stX, true);
			
			if (e.getErrorCode() == DBI.SQL_ERR_DUP_ENTRY) return new Result(false, "msg:\"A data set with the specified name already exists.\n\nIf you cannot see such data set in the list, it is possible that someone else has added it. Please remember that this is a multiuser system and more than one person can be working on the same project and/or data set at the same time. Consider refreshing the page.\"");
			else throw e;
		}
		
		// (5) Finish up:
		return new Result(true, "ds:" + get(dbi, username, dsIdNew));
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static Result rIntDel(DBI dbi, String username, int dsId)  throws SQLException {
		dbi.execUpd("DELETE FROM \"" + Do.DB_DATA + "-" + username + "\".r_int WHERE ds_id = " + dsId);
		return new Result(true, "");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static StringBuffer rIntGetHist(DBI dbi, String username, int dsId)  throws SQLException {
		StringBuffer res = new StringBuffer();
		ResultSet rs = dbi.execQry("SELECT cmd, res FROM \"" + Do.DB_DATA + "-" + username + "\".r_int WHERE ds_id = " + dsId + " ORDER BY id ");
		while (rs.next()) {
			res.append("> " + rs.getString("cmd") + "\n" + rs.getString("res") + "\n");
		}
		rs.close();
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static Result rIntGetRes(DBI dbi, String username, int rIntId)  throws SQLException {
		return new Result(true, "res:" + JSI.str2js(DBI.getStr(dbi, "SELECT res FROM \"" + Do.DB_DATA + "-" + username + "\".r_int WHERE id = " + rIntId, "res")));
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * TODO: add preserving distribution of values of a given variable
	 * TODO: add randomizing the order of observations in new data sets
	 */
	public static Result split(DBI dbi, String username, int dsId, String spec)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
	    String msgErr = "Please provide at least two positive integers that add up to a 100.";
		String dbD = "\"" + Do.DB_DATA + "-" + username + "\"";
		String dbDS = "\"" + Do.DB_DATA_DS + "-" + username + "\"";
		
	    // (1) Decode and validate proportions:
		String[] A = spec.split(",");
		int[] P = new int[A.length + 1];
		P[0] = 0;
		int sum = 0;
		
		if (A.length < 2) return new Result(false, "msg:\"" + msgErr + "\"");
		
		// TODO: uncommend below after fixing `ds.obsCnt`
		//if (ds.obsCnt < P.length) return alert("The active data set has " + ds.obsCnt + " observations and has been requested to be split into " + P.length + " data sets.\n\nA data set cannot be split unless it has an amount of observations not smaller than the requested number of new data sets.");
		
		for (int i = 0; i < A.length; i++) {
			try {
				int p = new Integer(A[i]);
				if (p <= 0) return new Result(false, "msg:\"" + msgErr + "\"");
				P[i+1] = p;
				sum += p;
			}
			catch (NumberFormatException e) {
				return new Result(false, "msg:\"" + msgErr + "\"");
			}
		}
		if (sum != 100) return new Result(false, "msg:\"" + msgErr + "\"");
		
		// (2) Initialize the discretization unit used for assigning observations to new data sets:
		List<List<Integer>> lstId = new ArrayList<List<Integer>>();
		String duInfo = "";
		int pCumul = 0;
		for (int i = 1; i < P.length; i++) {
			duInfo += i + "," + pCumul + "," + (pCumul + P[i]) + (i < P.length - 1 ? ";" : "");
			pCumul += P[i];
			lstId.add(new ArrayList<Integer>());
		}
		DiscretUnit du = new DiscretUnit(null, duInfo);
		
		// (3) Perform split:
		Statement stX = (new DBI()).transBegin();
		try {
			// (3.1) Assign observations to new data sets:
			ResultSet rs = dbi.execQry("SELECT " + ID_COL_NAME + " AS id FROM " + dbDS + ".ds_" + dsId);
			while (rs.next()) {
				int idx = du.getIdx(Math.random() * 100);
				lstId.get(idx).add(rs.getInt("id"));
			}
			rs.close();
			
			// (3.2) Create new data sets:
			String dsName = DBI.getStr(dbi, "SELECT name FROM " + dbD + ".ds WHERE id = " + dsId, "name");
			for (int i = 1; i < P.length; i++) {
				List<Integer> lid = lstId.get(i-1);
				dup(dbi, username, stX, dsId, DBI.ID_NONE, dsName + "-split-" + i, "Split: " + P[i] + "% of '" + dsName + "'", (lid.size() > 0 ? ID_COL_NAME + " IN (" + $.join2(lid, "", "", ",", false, true) + ")" : ""), lid.size(), (lid.size() > 0 ? true : false), false, 0);
			}
			
			DBI.transCommit(stX, true);
		}
		catch (SQLException e) {
			DBI.transRollback(stX, true);
			throw e;
		}
		
		//return new Result(true, "ds:" + get(dbi, username, dsIdNew));
		return new Result(true, "");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * spec: "gender:0->m,1->f|skill:0->low,1->medium,2->high"
	 */
	public static Result repVal(DBI dbi, String username, int prjId, String dsNameSrc, String dsNameDst, String spec)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException, RserveException, REXPMismatchException {
		String dbD = "\"" + Do.DB_DATA + "-" + username + "\"";
		String dbDS = "\"" + Do.DB_DATA_DS + "-" + username + "\"";
		
		int dsIdSrc = getId(dbi, username, prjId, dsNameSrc);
		int dsIdDst = DBI.ID_NONE;
		
		// (1) Decode specification:
		String diVarNames = "";
		Hashtable<String,ValSubstUnit> VSU = new Hashtable<String,ValSubstUnit>();
		String[] A = spec.split("\\|");
		if (A.length == 0) return new Result(false, "msg:\"Incorrect specification.\"");
		for (String a: A) {
			String[] B = a.split("\\:");
			if (B.length < 2) return new Result(false, "msg:\"Incorrect specification.\"");
			
			int type = (int) DBI.getLong(dbi, "SELECT type_id FROM " + dbD + ".var WHERE ds_id = " + dsIdSrc + " AND name = '" + B[0] + "'", "type_id");
			try { VSU.put(B[0], new ValSubstUnit(B[0], B[1], type)); }
			catch (NumberFormatException e) { return new Result(false, "msg:\"Incorrect specification.\""); }
			catch (ArrayIndexOutOfBoundsException e) { return new Result(false, "msg:\"Incorrect specification.\""); }
			
			diVarNames += B[0] + ",";
		}
		if (VSU.size() == 0) return new Result(false, "msg:\"Incorrect specification.\"");
		diVarNames = diVarNames.substring(0, diVarNames.length() - 1);
		
		// (2) Check if the TS variable will be affected:
		String tsVar = DBI.getStr(dbi, "SELECT ts_var FROM " + dbD + ".ds WHERE id = " + dsIdSrc, "ts_var");
		if (tsVar != null && VSU.containsKey(tsVar)) tsVar = null;
		
		// (2) Generate the new variables list:
		List<Var> vars = new ArrayList<Var>();
		StringBuilder qryVars = new StringBuilder();
		
		// (2.1) Retrieve from the original data set:
		ResultSet rsVar = dbi.execQry("SELECT name, type_id FROM " + dbD + ".var WHERE ds_id = " + dsIdSrc + " ORDER BY ord");
		while (rsVar.next()) {
			String name = rsVar.getString("name");
			vars.add(new Var(DBI.ID_NONE, rsVar.getInt("type_id"), name, null));
			qryVars.append("\"" + name + "\"" + (!rsVar.isLast() ? "," : ""));
		}
		rsVar.close();
		
		// (2.2) Check and update types of affected variables:
		Iterator<Var> itVar = vars.iterator();
		while (itVar.hasNext()) {
			Var v = itVar.next();
			if (VSU.containsKey(v.getName())) v.setType(VSU.get(v.getName()).getType());
		}
		
		// (2) Perform value substitution:
		Statement stX = (new DBI()).transBegin();
		try {
			// Create the new data set table:
			String dsName = DBI.getStr(dbi, "SELECT name FROM " + dbD + ".ds WHERE id = " + dsIdSrc, "name");
			//int prjId = (int) DBI.getLong(dbi, "SELECT prj_id FROM " + dbD + ".ds WHERE id = " + dsIdSrc, "prj_id");
			dsIdDst = createDSTbl(dbi, stX, username, prjId, Do.getAppIdData(), dsNameDst, "Rep-val: '" + dsName + "' (" + diVarNames + ")", tsVar, 0, vars);
			
			// Insert data:
			StringBuilder qry01 = new StringBuilder("INSERT INTO " + dbDS + ".ds_" + dsIdDst + " (" + qryVars + ") VALUES ");
			ResultSet rsData = dbi.execQry("SELECT " + qryVars.toString() + " FROM " + dbDS + ".ds_" + dsIdSrc + " ORDER BY " + ID_COL_NAME);
			while (rsData.next()) {
				StringBuilder qry02 = new StringBuilder();
				itVar = vars.iterator();
				while (itVar.hasNext()) {
					Var v = itVar.next();
					if (VSU.containsKey(v.getName())) {
						String valNew = VSU.get(v.getName()).getValSQL(rsData.getString(v.getName()));
						qry02.append((valNew != null ? valNew : Var.getSQLVal(v.getType(), rsData.getString(v.getName()))));
					}
					else qry02.append(Var.getSQLVal(v.getType(), rsData.getString(v.getName())));
					if (itVar.hasNext()) qry02.append(",");
				}
				
				//System.out.println(qry01.toString() + "(" + qry02.toString() + ")");
				stX.executeUpdate(qry01.toString() + "(" + qry02.toString() + ")");
			}
			rsData.close();
			
			
			DBI.transCommit(stX, true);
		}
		catch (SQLException e)           { DBI.transRollback(stX, true); throw e; }
		catch (RserveException e)        { DBI.transRollback(stX, true); throw e; }
		catch (REXPMismatchException e)  { DBI.transRollback(stX, true); throw e; }
		catch (InstantiationException e) { DBI.transRollback(stX, true); throw e; }
		catch (IllegalAccessException e) { DBI.transRollback(stX, true); throw e; }
		catch (ClassNotFoundException e) { DBI.transRollback(stX, true); throw e; }
		
		return new Result(true, "ds:" + get(dbi, username, dsIdDst));
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Adds a time series variable.
	 * 
	 * Features:
	 *   - ...
	 * 
	 * Algorithm:
	 *   1. ...
	 *   
	 * Sample specification: ts_i;r,sub,trial,stim,em_nevt;em_nevt
	 */
	// TODO: Figure out how to handle missing values. Currently, they are assumed to be a new value of the respective resetting variable.
	public static Result tsAddVar(DBI dbi, String username, int prjId, String dsName, String spec)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException, RserveException, REXPMismatchException {
		String dbD = "\"" + Do.DB_DATA + "-" + username + "\"";
		String dbDS = "\"" + Do.DB_DATA_DS + "-" + username + "\"";
		
		int dsId = getId(dbi, username, prjId, dsName);
		
		// (1) Decode and validate the specification:
		// (1.1) General structure:
		String[] A = spec.split(";");
		if (A.length != 3 || A[0].length() == 0 || A[1].length() == 0) return new Result(false, "Incorrect specification");
		
		// (1.2) Time series variable name:
		String varTS = A[0];
		if (hasVar(dbi, username, dsId, varTS)) return new Result(false, "msg:\"Variable '" + varTS + "' already exists.\"");
		
		// (1.3) Resetting variables:
		String[] varsReset = A[1].split(",");
		List<String> varsResetNotFound = new ArrayList<String>();
		for (String v: varsReset) {
			if (!hasVar(dbi, username, dsId, v)) varsResetNotFound.add(v);
		}
		if (varsResetNotFound.size() != 0) return new Result(false, "msg:\"The following resetting variable" + (varsResetNotFound.size() == 1 ? " has" : "s have") + " not been found: " + $.join(varsResetNotFound, "", "", ", ", false, true) + "\"");
		
		// (1.4) The "after" variable:
		String varAfter = A[2];
		if (varAfter.length() > 0 && !hasVar(dbi, username, dsId, varAfter)) return new Result(false, "msg:\"The \\\"after\\\" variable ('" + varAfter + "') not found.\"");
		
		// (2) Add the new variable:
		DBI dbiX = new DBI();
		Statement stX = dbiX.transBegin();
		try {
			// (2.1) Add the TS variable:
			// TODO: add the new TS variable after the specified variable
			int ord = (varAfter.length() == 0 
				? 0
				: (int) DBI.getLong(dbi, "SELECT (ord + 1) AS ord FROM " + dbD + ".var WHERE ds_id = " + dsId + " AND name = " + DBI.esc(varAfter), "ord")
			);
			
			//stX.executeUpdate("UPDATE " + dbD + ".var SET ord = ord + 1 WHERE id IN (SELECT id FROM " + dbD + ".var WHERE ds_id = " + dsId + " AND ord >= " + ord + " ORDER BY ord DESC)");
			  // TODO: The above query didn't work, so I went with the below loop. I'd prefer a one-statement solution.
			  //       - http://stackoverflow.com/questions/655010/how-to-update-and-order-by-using-ms-sql
			ResultSet rsOrd = dbiX.execQry("SELECT id FROM " + dbD + ".var WHERE ds_id = " + dsId + " AND ord >= " + ord + " ORDER BY ord DESC");
			while (rsOrd.next()) {
				stX.executeUpdate("UPDATE " + dbD + ".var SET ord = ord + 1 WHERE id = " + rsOrd.getInt("id"));
			}
			rsOrd.close();
			
			stX.executeUpdate("INSERT INTO " + dbD + ".var (ds_id, type_id, name, ord) VALUES (" + dsId + ", " + Var.TYPE_DIS + ", " + DBI.esc(varTS) + ", " + ord + ")");
			stX.executeUpdate("UPDATE " + dbD + ".ds SET ts_var = " + DBI.esc(varTS) + " WHERE id = " + dsId);
			stX.executeUpdate("ALTER TABLE " + dbDS + ".ds_" + dsId + " ADD COLUMN \"" + varTS + "\" " + Var.getSQLColType(Var.TYPE_DIS));
			  // TODO: validate the column name
			
			// (2.2) Update values of the TS variable:
			StringBuffer qry = new StringBuffer("SELECT " + ID_COL_NAME + " as id, '' || ");
			for (int i = 0; i < varsReset.length; i++) {
				String v = varsReset[i];
				int type = (int) DBI.getLong(dbi, "SELECT type_id FROM " + dbD + ".var WHERE ds_id = " + dsId + " AND name = '" + v + "'", "type_id");
				switch (type) {
					case Var.TYPE_UND:
					case Var.TYPE_STR:
						qry.append("COALESCE(" + v + ", '')");
						break;
					
					case Var.TYPE_CON:
						qry.append("COALESCE(CASE WHEN " + v + " = 0 THEN '0' ELSE trim(trailing '.0' from " + v + "::text) END, '')");
						break;
					
					default:
						qry.append("COALESCE(CASE WHEN " + v + " = 0 THEN '0' ELSE " + v + "::text END, '')");
				}
				if (i < varsReset.length - 1) qry.append(" || ");
			}
			qry.append(" AS comb FROM " + dbDS + ".ds_" + dsId + " ORDER BY " + $.join(varsReset, "", "", ",", false, true) + "," + ID_COL_NAME);
			
			String combPrev = null;  // previous combination of the resetting variable values
			int i = 0;
			//System.out.println("SELECT " + ID_COL_NAME + " as id, '' || " + $.join(varsReset, "COALESCE(", ",'')", " || ':' || ", false, true) + " AS comb FROM " + dbDS + ".ds_" + dsId + " ORDER BY " + $.join(varsReset, "", "", ",", false, true));
			//ResultSet rsTS = dbiX.execQry("SELECT " + ID_COL_NAME + " as id, '' || " + $.join(varsReset, "COALESCE(", ",'')", " || ':' || ", false, true) + " AS comb FROM " + dbDS + ".ds_" + dsId + " ORDER BY " + $.join(varsReset, "", "", ",", false, true));
			System.out.println(qry);
			ResultSet rsTS = dbiX.execQry(qry.toString());
			while (rsTS.next()) {
				String combCurr = rsTS.getString("comb");
				
				if (combPrev == null || !combCurr.equals(combPrev)) i = 0;
				else i++;
				
				combPrev = combCurr;
				
				stX.executeUpdate("UPDATE " + dbDS + ".ds_" + dsId + " SET \"" + varTS + "\" = " + i + " WHERE " + ID_COL_NAME + " = " + rsTS.getInt("id"));
			}
			rsTS.close();
			
			// (2.3) Enforce constraints on the TS variable:
			//stX.executeUpdate("ALTER TABLE " + dbDS + ".ds_" + dsId + " ALTER COLUMN " + varTS + " SET NOT NULL");  // not-null
			
			// (2.3) Execute the query:
			DBI.transCommit(stX, true);
		}
		catch (SQLException e) {
			DBI.transRollback(stX, true);
			throw e;
		}
		
		return new Result(true, "ds:" + get(dbi, username, dsId));
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Transform a time series data set in a long to a wide format.
	 * 
	 * Features:
	 *   - Automatically determines the lowest value of the TS column
	 *   - Does not require that the TS column has all values between the min and max; it adds those as missing values
	 * 
	 * Algorithm:
	 *   1. Determine the min and max of the TS column
	 *   2. Select names of variables from the `var` table and for each of them to get:
	 *       a) List of TS-invariant variables
	 *       b) List of TS variables
	 *   3. Create a new DS
	 *   4. Create an INSERT query and inject the data into the new data set
	 */
	public static Result tsLong2Wide(DBI dbi, String username, int prjId, String dsNameSrc, String dsNameDst, String tsLenVar, int idxStart, boolean idxFirst, boolean idxInc, String idxPre)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException, RserveException, REXPMismatchException {
		String dbD  = "\"" + Do.DB_DATA    + "-" + username + "\"";
		String dbDS = "\"" + Do.DB_DATA_DS + "-" + username + "\"";
		
		int dsIdSrc = getId(dbi, username, prjId, dsNameSrc);
		int dsIdDst = DBI.ID_NONE;
		
		// (1) Retrieve info:
		String tsVar = DBI.getStr(dbi, "SELECT ts_var FROM " + dbD + ".ds WHERE id = " + dsIdSrc, "ts_var");
		if (DBI.getLong(dbi, "SELECT COUNT(*) AS cnt FROM " + dbDS + ".ds_" + dsIdSrc + " WHERE \"" + tsVar + "\" = NULL", "cnt") > 0) return new Result(false, "msg:\"The time series indicator variable contains missing values. That is not permitable.\"");
		
		int tsMin = (int) DBI.getLong(dbi, "SELECT MIN(\"" + tsVar + "\") AS ts_min FROM " + dbDS + ".ds_" + dsIdSrc, "ts_min");
		int tsMax = (int) DBI.getLong(dbi, "SELECT MAX(\"" + tsVar + "\") AS ts_max FROM " + dbDS + ".ds_" + dsIdSrc, "ts_max");
		int tsLen = tsMax - tsMin + 1;
		
		if (DBI.getLong(dbi, "SELECT COUNT(*) AS cnt FROM " + dbD + ".ds WHERE prj_id = " + prjId + " AND name = " + DBI.esc(dsNameDst), "cnt") != 0) return new Result(false, "msg:\"A data set with the name specified already exists. Please provide a different name.\"");
		
		// (2) Separate the original DS's variables into TS and TS-invariant:
		boolean isPreTS = true;  // is the loop before or after the TS indicator column (the boundary between TS-invariant and TS vars) 
		List<Var> lstVarTSInv = new ArrayList<Var>();  // time series invariant variables
		List<Var> lstVarTS = new ArrayList<Var>();
		
		ResultSet rsVar = dbi.execQry("SELECT name, type_id FROM " + dbD + ".var WHERE ds_id = " + dsIdSrc + " ORDER BY ord");
		while (rsVar.next()) {
			String name = rsVar.getString("name");
			if (name.equals(tsVar)) {
				isPreTS = false;
				continue;
			}
			
			if (isPreTS) lstVarTSInv.add(new Var(DBI.ID_NONE, rsVar.getInt("type_id"), name, null));
			else lstVarTS.add(new Var(DBI.ID_NONE, rsVar.getInt("type_id"), name, null));
		}
		rsVar.close();
		
		if (lstVarTS.size() == 0) return new Result(false, "msg:\"No time series variables found in the specified data set. Those variables need to be placed after the time series indicator variable.\"");
		
		// (3) Create the new DS and fill it with data:
		Statement stX = (new DBI()).transBegin();
		try {
			// (3.1) Create the list of the new DS's variables and build the VALUES part of the INSERT query:
			List<Var> lstVarNewDS = new ArrayList<Var>();
			StringBuilder qry01 = new StringBuilder("");    // the first part of the INSERT query
			StringBuilder sbTSInv = new StringBuilder("");  // used to select existing data
			StringBuilder sbTS = new StringBuilder("");     // used to select existing data
			
			// TS-invariant variables:
			for (int i = 0; i < lstVarTSInv.size(); i++) {
				Var v = lstVarTSInv.get(i);
				lstVarNewDS.add(v);
				qry01.append("\"" + v.getName() + "\",");
				sbTSInv.append("\"" + v.getName() + "\"" + (i == lstVarTSInv.size() - 1 ? "" : ","));
			}
			if (tsLenVar != null) {
				lstVarNewDS.add(new Var(DBI.ID_NONE, Var.TYPE_DIS, tsLenVar, null));
				qry01.append("\"" + tsLenVar + "\",");
			}
			
			// TS variables:
			sbTS.append("\"" + tsVar + "\",");
			if (!idxFirst) idxStart--;
			for (int i = tsMin; i <= tsMax; i++) {
				String pre = null;  // current TS prefix
				if (i == tsMin && !idxFirst) pre = "";
				else pre = idxPre + (i + idxStart - tsMin - 1);
				
				if (idxInc) {
					String varName = tsVar + pre;
					lstVarNewDS.add(new Var(DBI.ID_NONE, Var.TYPE_DIS, varName, null));
					qry01.append("\"" + varName + "\",");
				}
				
				for (int j = 0; j < lstVarTS.size(); j++) {
					Var v = lstVarTS.get(j);
					String varName = v.getName() + pre;
					
					lstVarNewDS.add(new Var(DBI.ID_NONE, v.getType(), varName, null));
					qry01.append("\"" + varName + "\"" + (i == tsMax && j == lstVarTS.size() - 1 ? "" : ","));
				}
			}
			
			for (int i = 0; i < lstVarTS.size(); i++) {
				Var v = lstVarTS.get(i);
				sbTS.append("\"" + v.getName() + "\"" + (i == lstVarTS.size() - 1 ? "" : ","));
			}
			
			// (3.2) Create the new DS table:
			dsIdDst = createDSTbl(dbi, stX, username, prjId, Do.getAppIdData(), dsNameDst, "Wide TS: '" + dsNameSrc + "'", null, 0, lstVarNewDS);
			qry01.insert(0, "INSERT INTO " + dbDS + ".ds_" + dsIdDst + " (");
			qry01.append(") VALUES ");
			
			// (3.3) Add the actual variable values part to the INSERT query:
			List<Integer> lstTSStartId = new ArrayList<Integer>();
			ResultSet rsTSStart = dbi.execQry("SELECT " + ID_COL_NAME + " AS id FROM " + dbDS + ".ds_" + dsIdSrc + " WHERE \"" + tsVar + "\" = " + tsMin + " ORDER BY " + ID_COL_NAME);
			while (rsTSStart.next()) {
				lstTSStartId.add(rsTSStart.getInt("id"));
			}
			rsTSStart.close();
			
			String valMiss = $.repStr("null,", lstVarTS.size());
			valMiss = valMiss.substring(0, valMiss.length() - 1);
			
			for (int i = 0; i < lstTSStartId.size(); i++) {
				int id = lstTSStartId.get(i);
				
				// TS-invariant variables:
				StringBuilder qry02 = new StringBuilder();
				ResultSet rsVarTSInv = dbi.execQry("SELECT " + sbTSInv + " FROM " + dbDS + ".ds_" + dsIdSrc + " WHERE " + ID_COL_NAME + " = " + id);
				rsVarTSInv.next();
				
				Iterator<Var> it01 = lstVarTSInv.iterator();
				while (it01.hasNext()) {
					qry02.append(tsLong2Wide_getVal(rsVarTSInv, it01.next()) + ",");
				}
				
				// TS variables:
				StringBuilder qry03 = new StringBuilder();
				Hashtable<Integer,String> htValTS = new Hashtable<Integer,String>();
				ResultSet rsVarTS = dbi.execQry("SELECT " + sbTS + " FROM " + dbDS + ".ds_" + dsIdSrc + " WHERE " + ID_COL_NAME + " >= " + id + (i < lstTSStartId.size() - 1 ? " AND " + ID_COL_NAME + " < " + lstTSStartId.get(i+1) : ""));
				while (rsVarTS.next()) {
					int ts = rsVarTS.getInt(tsVar);
					
					StringBuilder sbTmp = new StringBuilder((idxInc ? ts + "," : ""));
					Iterator<Var> it02 = lstVarTS.iterator();
					while (it02.hasNext()) {
						sbTmp.append(tsLong2Wide_getVal(rsVarTS, it02.next()) + (it02.hasNext() ? "," : ""));
					}
					
					htValTS.put(ts, sbTmp.toString());
				}
				
				int currTSLen = tsLen;
				for (int j = tsMin; j <= tsMax; j++) {
					if (htValTS.containsKey(j)) qry03.append(htValTS.get(j));
					else {
						qry03.append((idxInc ? j + "," : "") + valMiss);
						currTSLen--;
					}
					qry03.append((j == tsMax ? "" : ","));
				}
				
				stX.executeUpdate(qry01.toString() + "(" + qry02 + (tsLenVar != null ? currTSLen + "," : "") + qry03.toString() + ")");
			}
			
			// (4.4) Execute the query:
			DBI.transCommit(stX, true);
			
		}
		catch (SQLException e) {
			DBI.transRollback(stX, true);
			throw e;
		}
		
		return new Result(true, "ds:" + get(dbi, username, dsIdDst));
	}
	
	
	// - - - -
	private static String tsLong2Wide_getVal(ResultSet rs, Var v)  throws SQLException {
		switch (v.getType()) {
			case Var.TYPE_BIN: return "" + rs.getInt(v.getName());
			case Var.TYPE_CAT: return "" + rs.getInt(v.getName());
			case Var.TYPE_ORD: return "" + rs.getInt(v.getName());
			case Var.TYPE_DIS: return "" + rs.getInt(v.getName());
			case Var.TYPE_CON: return "" + rs.getFloat(v.getName());
			case Var.TYPE_STR: return DBI.esc(rs.getString(v.getName()));
		}
		return null;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static Result varRename(DBI dbi, String username, int prjId, String dsName, String varNameSrc, String varNameDst)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		String dbD  = "\"" + Do.DB_DATA    + "-" + username + "\"";
		String dbDS = "\"" + Do.DB_DATA_DS + "-" + username + "\"";
		
		// (1) Check:
		int dsId = getId(dbi, username, prjId, dsName);
		if (!hasVar(dbi, username, dsId, varNameSrc)) return new Result(false, "msg:\"The specified variable ('" + varNameSrc + "') does not exist.\"");
		if ( hasVar(dbi, username, dsId, varNameDst)) return new Result(false, "msg:\"The data set already contains variable named '" + varNameDst + "'.\"");
		
		// (2) Rename:
		Statement stX = (new DBI()).transBegin();
		try {
			stX.executeUpdate("UPDATE " + dbD + ".var SET name = " + DBI.esc(varNameDst) + " WHERE ds_id = " + dsId + " AND name = " + DBI.esc(varNameSrc));
			stX.executeUpdate("ALTER TABLE " + dbDS + ".ds_" + dsId + " RENAME COLUMN \"" + varNameSrc + "\" TO \"" + varNameDst + "\"");
			
			DBI.transCommit(stX, true);
		}
		catch (SQLException e) {
			DBI.transRollback(stX, true);
			throw e;
		}
		
		return new Result(true, "");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Reorder variables in the specified data set according to the provided specification.
	 */
	// sub,trial,ts_i,r,em_dur,em_wlen,em_fp,em_sacc_i_roi,em_sacc_o_roi,em_sacc_i_let,em_sacc_o_let,subtl_frq_log10,roi_cl_e,roi_sent_e
	public static Result varReorder(DBI dbi, String username, int dsId, String spec)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException, RserveException, REXPMismatchException {
		String dbD = "\"" + Do.DB_DATA + "-" + username + "\"";
		
		// (1) Decode and validate the specs:
		String[] V = spec.split(",");
		boolean doVarMatch = (DBI.getLong(dbi, "SELECT (a.cnt - b.cnt) AS diff FROM (SELECT COUNT(*) AS cnt FROM " + dbD + ".var WHERE ds_id = " + dsId + ") a, (SELECT COUNT(*) AS cnt FROM " + dbD + ".var WHERE ds_id = " + dsId + " AND name IN ('" + $.join(V, "", "", "','", false, true) + "')) b", "diff") == 0 ? true : false);
		if (!doVarMatch) return new Result(false, "msg:\"Variables provided in the specification do not match the variables of the data set.\"");
		
		// (2) Reorder variables:
		Statement stX = (new DBI()).transBegin();
		try {
			for (int i = 0; i < V.length; i++) {
				stX.executeUpdate("UPDATE " + dbD + ".var SET ord = " + i + " WHERE ds_id = " + dsId + " AND name = '" + V[i] + "'");
			}
			
			DBI.transCommit(stX, true);
		}
		catch (SQLException e) {
			DBI.transRollback(stX, true);
			throw e;
		}
		
		return new Result(true, "ds:" + get(dbi, username, dsId));
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Transform a variable (and create a new one as a result).
	 */
	public static Result varTrans(DBI dbi, String username, int prjId, String dsName, String name, int type, String val)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException, RserveException, REXPMismatchException {
		String dbD  = "\"" + Do.DB_DATA    + "-" + username + "\"";
		String dbDS = "\"" + Do.DB_DATA_DS + "-" + username + "\"";
		
		int dsId = getId(dbi, username, prjId, dsName);
		
		if (name != null && name.length() > 0 && DBI.getLong(dbi, "SELECT id FROM " + dbD + ".var WHERE ds_id = " + dsId + " AND name = " + DBI.esc(name), "id") != DBI.ID_NONE) return new Result(false, "msg:\"A variable with the specified name already exists in the current data set.\"");
		String memo = "Transformed: " + val;
		
		int varId = DBI.ID_NONE;
		DBI dbiX = new DBI();
		Statement stX = dbiX.transBegin();
		try {
			int ordMax = (int) DBI.getLong(dbi, "SELECT MAX(ord) AS ord FROM " + dbD + ".var WHERE ds_id = " + dsId, "ord");
			varId = DBI.insertGetId(stX, null, "INSERT INTO " + dbD + ".var (ds_id, type_id, name, ord) VALUES (" + dsId + "," + type + "," + DBI.esc(name) + "," + (ordMax+1) + ")");
			
			stX.executeUpdate("ALTER TABLE " + dbDS + ".ds_" + dsId + " ADD COLUMN \"" + name + "\" " + Var.getSQLColType(type));
			stX.executeUpdate("UPDATE " + dbDS + ".ds_" + dsId + " SET \"" + name + "\" = (" + val + ")");
			stX.executeUpdate("UPDATE " + dbD + ".var SET memo = " + DBI.esc(memo) + " WHERE id = " + varId);
			
			DBI.transCommit(stX, true);
		}
		catch (SQLException e) {
			DBI.transRollback(stX, true);
			throw e;
		}
		
		return new Result(true, "var:{id:" + varId + ",type:" + type + ",name:" + JSI.str2js(name) + ",memo:" + JSI.str2js(memo) + ",stat:" + Var.calcStats(dbi, username, dsId, varId, name, true) + ",memo:\"\"}");
	}
}
