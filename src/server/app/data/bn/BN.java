package app.data.bn;

import inter.DBI;
import inter.FSI;
import inter.JSI;

import java.awt.Point;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import main.Do;
import smile.Network;
import smile.learning.DBCML;
import smile.learning.DataMatch;
import smile.learning.EM;
import smile.learning.PC;
import smile.learning.Pattern;
import util.$;


/**
 * @author Tomek D. Loboda
 */
public class BN {
	
	// ---------------------------------------------------------------------------------
	public BN() {}
	
	
	// ---------------------------------------------------------------------------------
	public String learnStruct(String db, DBI dbi, String alg, int dsId, String vars)  throws SQLException, IOException {
		// (1) Init:
		smile.learning.DataSet dsL = new smile.learning.DataSet();  // dataset: learning
		StringBuilder sb = FSI.getStr(Do.getPathDS() + "zo-id-em-f-10" + File.separatorChar + "ds");
		int idxEndLn = sb.indexOf("\n");
		util.DataSet dsA = new util.DataSet(sb.substring(idxEndLn+1), sb.substring(0,idxEndLn), "\n", "\t");
		String[] V = vars.split(",");
		
		// (2) Construct the learning dataset:
		// (2.1) Add variables:
		for (int i = 0; i < V.length; i++) {
			String v = V[i];
			dsL.addFloatVariable(v);
			/*
			switch (dsA.getVarType(v)) {
				case util.DataSet.TYPE_I: dsL.addIntVariable(v); break;
				case util.DataSet.TYPE_F: dsL.addFloatVariable(v); break;
			}
			*/
		}
		
		// (2.2) Add observations:
		for (int i = 0; i < dsA.rowCnt; i++) {
			dsL.addEmptyRecord();
			for (int j = 0; j < V.length; j++) {
				String v = V[j];
				float val;
				try {
					val = new Float(dsA.val(i,v));
				}
				catch (NumberFormatException e) {
					val = dsA.getVarStats(v).getMean().floatValue();
				}
				dsL.setFloat(j,i, val);
				/*
				switch (dsA.getVarType(v)) {
					case util.DataSet.TYPE_I: dsL.setInt(j, i, new Integer(dsA.val(i, v))); break;
					case util.DataSet.TYPE_F: dsL.setFloat(j, i, new Float(dsA.val(i, v))); break;
				}
				*/
			}
		}
		
		/*
		StringBuffer sb = new StringBuffer();
		sb.append(dsL.getRecordCount() + "\n");
		for (int i = 0; i < dsL.getRecordCount(); i++) {
			sb.append(dsL.getFloat(0,i) + "\t" + dsL.getFloat(1,i) + "\n");
		}
		System.out.println(sb + "\n");
		*/
		
		// (3) Learn and layout:
		if (alg.equals("pc")) return learnStruct_PC(dsL, V);
		else if (alg.equals("dbcl")) return learnStruct_DBCL(dsL, V);
		else return null;
	}
	
	
	// ---------------------------------------------------------------------------------
	public String learnStruct_PC(smile.learning.DataSet ds, String[] vars)  throws SQLException, IOException {
		StringBuilder res  = new StringBuilder();
		try {
			Pattern p = new PC().learn(ds);
			Graph graph = new Graph(p);
			graph.layout(new Point(40,40), new Point(600,600));
			
			int resIdDS = Do.resMan.put(ds);
			int resIdP  = Do.resMan.put(p);
			
			// (4) Construct response:
			// (4.1) Variables:
			res.append("{outcome:true,resIds:{dataset:" + resIdDS + ",pattern:" + resIdP + "},bn:{vars:[");
			for (int i = 0; i < vars.length; i++) {
				res.append("{name:" + JSI.str2js(vars[i]) + ",pos:" + graph.getPosJS(i) + "}" + (i < vars.length - 1 ? "," : ""));
			}
			res.append("],arcs:[");
			
			// (4.2) Arcs:
			Set<String> arcs = new HashSet<String>();
			for (int i = 0; i < p.getSize(); i++) {
				for (int j = 0; j < p.getSize(); j++) {
					if (i == j) continue;
					int e = p.getEdge(i,j);
					if (e != Pattern.EdgeType.None && !arcs.contains(i + "-" + j)) {
						res.append("{varAIdx:" + i + ",varBIdx:" + j + ",dir:" + (e == Pattern.EdgeType.Directed ? "true" : "false") + "}" + (i == p.getSize() - 1 && j == p.getSize() - 1 ? "" : ","));
						arcs.add(j + "-" + i);
					}
				}
			}
			res.append("]}}");
		}
		catch (smile.SMILEException ex) {
			String msg = ex.getMessage();
			if (msg.indexOf("pc: constant not allowed") != -1) msg = "The following variables are in fact constants (no variation in values they takes on):\n\n        " + $.join(msg.substring(msg.indexOf("pc: constant not allowed") + "pc: constant not allowed".length() + 2, msg.length() - 1).split(" "), "", "", "\n        ", false, false) + "\n\nPlease go back to the previous step, exclude those variables, and try again.";
			res.append("{outcome:false,msg:" + JSI.str2js(msg) + "}");
		}
		
		return res.toString();
	}
	
	
	// ---------------------------------------------------------------------------------
	public String learnStruct_DBCL(smile.learning.DataSet ds, String[] vars)  throws SQLException, IOException {
		StringBuilder res  = new StringBuilder();
		try {
			System.out.println();
			Pattern p = new Pattern();
			smile.learning.DataSet ds2 = new smile.learning.DataSet();  // a data set to be enriched with derivatives 
			Object[] O = new DBCML().learn(ds, p, ds2);
			
			Graph graph = new Graph(p);
			graph.layout(new Point(40,40), new Point(600,600));
			
			int resIdDS = Do.resMan.put(ds2);
			int resIdP  = Do.resMan.put(p);
			
			// (4) Construct response:
			// (4.1) Variables:
			int varCnt = 0;
			res.append("{outcome:true,resIds:{dataset:" + resIdDS + ",pattern:" + resIdP + "},bn:{vars:[");
			for (int i = 0; i < vars.length; i++) {
				varCnt++;
				res.append("{name:" + JSI.str2js(vars[i]) + ",pos:" + graph.getPosJS(varCnt-1) + "}" + (varCnt < p.getSize()-2 ? "," : ""));
				Integer[] D = (Integer[]) O[i];  // derivatives of the current variable
				for (int j = 1; j < D.length; j++) {
					res.append("{name:" + JSI.str2js($.repStr("d", j) + "_" + vars[i]) + ",pos:" + graph.getPosJS(varCnt-1) + "}" + (varCnt < p.getSize()-2 ? "," : ""));
				}
			}
			res.append("],arcs:[");
			
			// (4.2) Arcs:
			Set<String> arcs = new HashSet<String>();
			for (int i = 0; i < p.getSize(); i++) {
				for (int j = 0; j < p.getSize(); j++) {
					if (i == j) continue;
					int e = p.getEdge(i,j);
					if (e != Pattern.EdgeType.None && !arcs.contains(i + "-" + j)) {
						if (arcs.size() > 0) res.append(",");
						res.append("{varAIdx:" + i + ",varBIdx:" + j + ",dir:" + (e == Pattern.EdgeType.Directed ? "true" : "false") + "}");
						arcs.add(j + "-" + i);
					}
				}
			}
			res.append("]}}");
		}
		catch (smile.SMILEException ex) {
			String msg = ex.getMessage();
			if (msg.indexOf("pc: constant not allowed") != -1) msg = "The following variables are in fact constants (no variation in values they takes on):\n\n        " + $.join(msg.substring(msg.indexOf("pc: constant not allowed") + "pc: constant not allowed".length() + 2, msg.length() - 1).split(" "), "", "", "\n        ", false, false) + "\n\nPlease go back to the previous step, exclude those variables, and try again.";
			res.append("{outcome:false,msg:" + JSI.str2js(msg) + "}");
		}
		
		return res.toString();
	}
	
	
	// ---------------------------------------------------------------------------------
	public String learnParamsPattern(String db, DBI dbi, int resIdP, int resIdDS, String arcs) {
		// Make a DAG (by orienting edges):
		Pattern p = (Pattern) Do.resMan.get(resIdP, false);
		String[] A = arcs.split(",");
		for (int i = 0; i < A.length; i++) {
			String[] a = A[i].split("-");
			p.setEdge(new Integer(a[0]), new Integer(a[1]), Pattern.EdgeType.Directed);
			p.setEdge(new Integer(a[1]), new Integer(a[0]), Pattern.EdgeType.None);
		}
		if (p.hasCycle()) {
			return "{outcome:false,msg:\"A Bayesian network cannot have cycles. Please make sure there are none and try again.\"}";
		}
		
		// Learn parameters and save the network:
		smile.learning.DataSet ds = (smile.learning.DataSet) Do.resMan.get(resIdDS, false);
		Network net = p.makeNetwork(ds);
		
		String res = learnParamsDAG(db, dbi, net, ds);
		
		Do.resMan.remove(resIdP);
		Do.resMan.remove(resIdDS);
		
		return res;
	}
	
	
	// ---------------------------------------------------------------------------------
	public String learnParamsDAG(String db, DBI dbi, Network net, smile.learning.DataSet ds) {
		DataMatch[] DM = match(ds, net);
		(new EM()).learn(ds, net, DM);
		net.writeFile(Do.getPathBN() + "test.xdsl");
		
		// (3) Finish:
		StringBuilder res = new StringBuilder("{outcome:true,bn:{vars:[");
		for (int i = 0; i < ds.getVariableCount(); i++) {
			String nodeId = name2id(ds.getVariableId(i));
			res.append("{name:" + JSI.str2js(net.getNodeName(nodeId)) + ",eqn:" + JSI.str2js(net.getNodeEquation(nodeId)) + "}" + (i < ds.getVariableCount()-1 ? "," : ""));
		}
		res.append("]}}");
		
		return res.toString();
	}
	
	
	// ---------------------------------------------------------------------------------
	private DataMatch[] match(smile.learning.DataSet ds, Network net) {
		DataMatch[] DM = new DataMatch[ds.getVariableCount()];
		for (int i = 0; i < ds.getVariableCount(); i++) {
			DM[i] = new DataMatch(i, net.getNode(name2id(ds.getVariableId(i))), 0);
		}
		return DM;
	}
	
	
	// ---------------------------------------------------------------------------------
	private String name2id(String name) { return name.replaceAll(" ", "_").replaceAll("-", "_"); }
	
	
	// ---------------------------------------------------------------------------------
	public String updateBeliefs() {
		Network net = new Network();
		net.readFile(Do.getPathBN() + "test.xdsl");
		net.setSampleCount(1000);
		net.updateBeliefs();
		
		StringBuilder res = new StringBuilder("{outcome:true,bn:{vars:[");
		for (int i = 0; i < net.getNodeCount(); i++) {
			double[] V = net.getNodeValue(i);
			res.append("{name:" + JSI.str2js(net.getNodeName(i)) + ",val:\"" + $.trunc(V[0],2) + ", " + $.trunc(V[1],2) + "\"}" + (i < net.getNodeCount()-1 ? "," : ""));
		}
		res.append("]}}");
		
		return res.toString();
	}
}
