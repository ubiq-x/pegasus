package app.data.bn;


/**
 * @author Tomek D. Loboda
 */
public class GraphEdge {
	public GraphNode A;
	public GraphNode B;
	
	
	// ---------------------------------------------------------------------------------
	public GraphEdge(GraphNode A, GraphNode B) {
		this.A = A;
		this.B = B;
	}
}
