package app.data.bn;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import smile.learning.Pattern;


/**
 * @author Tomek D. Loboda
 */
public class Graph {
	private List<GraphNode> nodes = new ArrayList<GraphNode>();
	private List<GraphEdge> edges = new ArrayList<GraphEdge>();
	
	
	// ---------------------------------------------------------------------------------
	public Graph(Pattern p) {
		// Nodes:
		for (int i = 0; i < p.getSize(); i++) nodes.add(new GraphNode());
		
		// Edges:
		for (int i = 0; i < p.getSize(); i++) {
			for (int j = 0; j < p.getSize(); j++) {
				if (p.getEdge(i,j) != Pattern.EdgeType.None) {
					GraphNode A = nodes.get(i);
					GraphNode B = nodes.get(j);
					
					GraphEdge e = new GraphEdge(A,B);
					A.edges.add(e);
					B.edges.add(e);
					
					edges.add(e);
				}
			}
		}
	}
	
	
	// ---------------------------------------------------------------------------------
	public String getPosJS(int nodeIdx) {
		GraphNode N = nodes.get(nodeIdx);
		return "{x:" + N.pos.x + ",y:" + N.pos.y + "}";
	}
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * Inspiration: http://rubenlaguna.com/wp/2009/03/09/force-directed-layout-in-netbeans
	 * 
	 * Resources:
	 * - http://en.wikipedia.org/wiki/Force-based_algorithms
	 *   - http://en.wikipedia.org/wiki/Hooke's_Law
	 *   - http://en.wikipedia.org/wiki/Coulomb's_law
	 * - http://java.itags.org/java/1045345/
	 * - http://prefuse.org
	 */
	public void layout(Point min, Point max) {
		Random random = new Random(100);
		Iterator<GraphNode> itN01;
		Iterator<GraphNode> itN02;
		Iterator<GraphEdge> itE;
		
		// (1) Randomize initial positions of all nodes:
		itN01 = nodes.iterator();
		while (itN01.hasNext()) {
			itN01.next().pos = new Point(random.nextInt(max.x + 1), random.nextInt(max.y + 1));
		}
		
		for (int i = 0; i < nodes.size() / 2; i++) {
			GraphNode A = nodes.get(i);
			for (int j = i + 1; j < nodes.size(); j++) {
				GraphNode B = nodes.get(j);
				int dx = B.pos.x - A.pos.x;
				int dy = B.pos.y - A.pos.y;
				double d = Math.sqrt(dx*dx + dy*dy);
				if (d < 40) B.pos.translate(30,30);
			}
		}
		
		// (2) Layout nodes:
		int dMin = 100;  // minimum distance
		int step = 0;
		
		do {
            step++;
			
            itN01 = nodes.iterator();
			while (itN01.hasNext()) itN01.next().force = new Point(0,0);
			
            itN01 = nodes.iterator();
			while (itN01.hasNext()) {
				GraphNode A = itN01.next();
				
				// Repulsion forces (from all other nodes):
				itN02 = nodes.iterator();
				while (itN02.hasNext()) {
					GraphNode B = itN02.next();
					if (A == B) continue;
					
					int dx = A.pos.x - B.pos.x;
					int dy = A.pos.y - B.pos.y;
					double d = Math.sqrt(dx*dx + dy*dy);
					
					dx = (int) (dx * dMin / d);  // inversely proportional to the distance
					dy = (int) (dy * dMin / d);
					
					A.force.translate(dx, dy); 
				}
				
				// Attraction forces (from attached nodes):
				itE = A.edges.iterator();
				while (itE.hasNext()) {
					GraphEdge e = itE.next();
					GraphNode B = e.B;
					
					int dx = A.pos.x - B.pos.x;
					int dy = A.pos.y - B.pos.y;
					double d = Math.sqrt(dx*dx + dy*dy);
					
					dx = (int) -(dx * d / 30);
					dy = (int) -(dy * d / 30);
					
					A.force.translate( dx,  dy);  // directly proportional to the distance 
					B.force.translate(-dx, -dy);
				}
				
				// Use the force:
				int x = A.force.x / dMin;
				int y = A.force.y / dMin;
				x = (x > 0 ? Math.min(x, min.x) : Math.max(x, -min.x));
				y = (y > 0 ? Math.min(y, min.y) : Math.max(y, -min.y));
				
				A.pos.translate(x,y);
			}
		} while (step < 500);
        
		// (3) Bring the graph into view:
		Point p01 = new Point(Integer.MAX_VALUE, Integer.MAX_VALUE);  // upper left corner (of the graph bounding box)
		
		itN01 = nodes.iterator();
		while (itN01.hasNext()) {
			GraphNode A = itN01.next();
			if (A.pos.x < p01.x) p01.x = A.pos.x;
			if (A.pos.y < p01.y) p01.y = A.pos.y;
		}
		
		itN01 = nodes.iterator();
		while (itN01.hasNext()) {
			itN01.next().pos.translate(-p01.x + min.x, -p01.y + min.y);
		}
		
		itN01 = nodes.iterator();
		while (itN01.hasNext()) {
			GraphNode A = itN01.next();
			while (A.pos.x > max.x) A.pos.translate(-A.pos.x / 2, 0);
			while (A.pos.y > max.y) A.pos.translate(0, -A.pos.y / 2);
		}
	}
}
