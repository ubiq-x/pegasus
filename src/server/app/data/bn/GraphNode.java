package app.data.bn;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;


/**
 * @author Tomek D. Loboda
 */
public class GraphNode {
	public Point pos   = new Point(0,0);
	public Point force = new Point(0,0);
	
	public List<GraphEdge> edges = new ArrayList<GraphEdge>();
	
	
	// ---------------------------------------------------------------------------------
	public GraphNode() {}
}
