/*
Required R packages
  Misc
    install.packages("nortest")
    install.packages("lme4")
  DB
    MySQL
      install.packages("RMySQL")
    PostgreSQL
      install.packages("RPostgreSQL")
      source("http://bioconductor.org/biocLite.R")
      biocLite("RdbiPgSQL")


RPostgreSQL
  http://code.google.com/p/rpostgresql/
  http://cran.r-project.org/web/packages/RPostgreSQL/index.html
  
  http://blog.earlh.com/index.php/2010/01/querying-databases-from-r-on-a-mac/


Rserve
  http://rforge.net/Rserve/doc/
  http://www.rforge.net/Rserve/example.html
  
  Rserve is thread safe across connections, but eval methods are not thread safe within one connection. This means 
    that multiple threads should not use the same connection unless they guarantee that no eval calls are run in 
    parallel.


GENERAL
  http://www.rosuda.org/r/nightly/javadoc/org/rosuda/JRI/Rengine.html
  http://www.statmethods.net/index.html
  
  http://en.wikipedia.org/wiki/GNU_Scientific_Library
  http://en.wikipedia.org/wiki/GNU_Octave
  http://www.ci.tuwien.ac.at/gR/


GRAPHICS
  http://osiris.sunderland.ac.uk/~cs0her/Statistics/UsingLatticeGraphicsInR.htm


DATABASES
  http://cran.r-project.org/doc/manuals/R-data.html#Relational-databases
  http://stat.bell-labs.com/RS-DBI/doc/html/DBI.html
  
  http://bioconductor.org/packages/2.6/bioc/html/RdbiPgSQL.html
  
  install.packages("RMySQL")
  library(RMySQL)
  conn <- dbConnect(dbDriver("MySQL"), db="pegasus-data-tomek", user="root", password="sa")
  dbListTables(conn)
  dbListFields(conn, "ds_1")
  D <- dbReadTable(conn, "ds_1")
  dbDisconnect(conn)
*/


package app.data;

import inter.DBI;
import inter.JSI;

import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import main.Do;
import main.Result;

import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;


/**
 * @author Tomek D. Loboda
 */
public class R {
	private static final String RSERVE_HOSTNAME = "localhost";
	private static final int RSERVE_PORT = 6311;
	
	private static final Pattern PATT_LOAD_DS = Pattern.compile("^([\\w]+)[\\s]*<-[\\s]*pegasus.loadDataSet\\(\\)$");  // e.g., x <- pegasus.loadDataSet()
	private static final Pattern PATT_PROHIB  = Pattern.compile(
		".*library\\(*." +
		"|.*require\\(*." +
		"|.*install[.]packages*." +
		"|.*dev[.]*."
	);
	
	private final RConnection rconn;
	private final DBI dbi;
	
	private boolean isConn = false;
	private String dsName = null;
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public R(DBI dbi)  throws RserveException {
		this.dbi = dbi;
		
		rconn = new RConnection(RSERVE_HOSTNAME, RSERVE_PORT);
		//rconn.eval("library(R)");  // TODO: not available for R 3.1.0
		rconn.eval("library(lme4)");
		rconn.eval("library(graphics)");
		//rconn.eval("library(grDevices)");  // TODO: not available for R 3.1.0
		rconn.eval("library(RPostgreSQL)");  // library(RMySQL)
		
		rconn.eval("pegasus.dbDrv <- dbDriver(\"PostgreSQL\")");  // MySQL
		
		System.out.println(rconn);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public void destroy()  throws RserveException {
		rconn.eval("dbUnloadDriver(drv)");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Result exec(String username, Integer dsId, String code, boolean persist, boolean js)  throws REXPMismatchException, SQLException {
		if (code.trim().length() == 0) return new Result(true, "res:\"\"");
		
		StringBuffer res = new StringBuffer();
		Matcher m = null;
		
		// (1) Prohibited commands:
		m = PATT_PROHIB.matcher(code);
		if (m.find()) return new Result(false, "The command contains prohibited string");
		
		// (2) Pegasus commands:
		// (2.1) Load dataset:
		m = PATT_LOAD_DS.matcher(code);
		if (m.find()) {
			dsName = m.group(1);
			
			try {
				rconn.eval(getDBConnCmd("pegasus.dbConn"));
				//rconn.eval(dsName + " <- dbReadTable(pegasus.dbConn, '\"" + Do.DB_DATA_DS + "-" + username + "\".\"ds_" + dsId + "\"')");
					// Quick fix to make it work for GitLab description (2018.09.21)
				rconn.eval(dsName + " <- dbGetQuery(pegasus.dbConn, 'SELECT * FROM \"" + Do.DB_DATA_DS + "-" + username + "\".\"ds_" + dsId + "\"')");
				rconn.eval("dbDisconnect(pegasus.dbConn)");
				res.append("TRUE");
			}
			catch (RserveException ex) {
				res.append(ex.getMessage());
			}
		}
		
		// (3) R commands:
		else {
			REXP rx;
			try {
				rx = rconn.parseAndEval("try(capture.output(print(" + code + ")),silent=TRUE)");
				if (rx.inherits("try-error")) {
					String err = rx.asString();
					if (err.charAt(err.length() - 1) == '\n') err = err.substring(0, err.length() - 1);
					res.append(err);
				}
				else {
					String[] RX = rx.asStrings();
					for (int i = 0; i < RX.length; i++) {
						res.append(RX[i] + (i < RX.length - 1 ? "\n" : ""));
					}
				}
			}
			catch (REngineException ex) {
				res.append(ex.getMessage());
			}
		}
		
		// (4) Persist and return (we need the result of the command here so this cannot be done earlier):
		int rId = DBI.ID_NONE;
		if (persist && dsId != null) {
			rId = DBI.insertGetId(dbi, null, "INSERT INTO \"" + Do.DB_DATA + "-" + username + "\".r_int (ds_id, cmd, res, res_ln_cnt) VALUES (" + dsId + ", " + DBI.esc(code) + ", " + DBI.esc(res.toString()) + "," + res.toString().split("\n").length + ")");
		}
		
		return new Result(true, (js ? "id:" + rId + ",res:" + JSI.str2js(res) : res.toString()));
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public RConnection getConn() { return rconn; }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public void connEnd()  throws RserveException {
		if (isConn) rconn.eval("dbDisconnect(pegasus.dbConn)");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public void connStart()  throws RserveException {
		if (!isConn) rconn.eval("pegasus.dbConn <- dbConnect(pegasus.dbDrv, dbname=\"" + DBI.DB_NAME + "\", user=\"" + DBI.DB_USERNAME + "\", password=\"" + DBI.DB_PASSWD + "\")");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static String getDBConnCmd(String var) {
		return var + " <- dbConnect(pegasus.dbDrv, dbname=\"" + DBI.DB_NAME + "\", user=\"" + DBI.DB_USERNAME + "\", password=\"" + DBI.DB_PASSWD + "\")";
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public void loadVar(String username, String rVarName, int dsId, String dbVarName)  throws RserveException   {
		/*
		System.out.println(rVarName + " (" + dbVarName + ")");
		System.out.println("  SELECT \"" + dbVarName + "\" AS x FROM \"" + Do.DB_DATA_DS + "-" + username + "\".ds_" + dsId + " WHERE \"" + dbVarName + "\" IS NOT NULL");
		
		REXP rx;
		try {
			System.out.println("1.1");
			rx = rconn.parseAndEval("try(capture.output(print(" + getDBConnCmd("pegasus.dbConn") + ")),silent=TRUE)");
			System.out.println("1.2");
			if (rx.inherits("try-error")) {
				String err = rx.asString();
				if (err.charAt(err.length() - 1) == '\n') err = err.substring(0, err.length() - 1);
				System.out.println(err);
			}
			
			System.out.println("2.1");
			rx = rconn.parseAndEval("try(capture.output(print(" + rVarName + " <- dbGetQuery(pegasus.dbConn, 'SELECT \"" + dbVarName + "\" AS x FROM \"" + Do.DB_DATA_DS + "-" + username + "\".ds_" + dsId + " WHERE \"" + dbVarName + "\" IS NOT NULL')" + ")),silent=TRUE)");
			System.out.println("2.2");
			if (rx.inherits("try-error")) {
				String err = rx.asString();
				if (err.charAt(err.length() - 1) == '\n') err = err.substring(0, err.length() - 1);
				System.out.println(err);
			}
			
			System.out.println("3.1");
			rx = rconn.parseAndEval("try(capture.output(print(" + "dbDisconnect(pegasus.dbConn)" + ")),silent=TRUE)");
			System.out.println("3.2");
			if (rx.inherits("try-error")) {
				String err = rx.asString();
				if (err.charAt(err.length() - 1) == '\n') err = err.substring(0, err.length() - 1);
				System.out.println(err);
			}
		}
		catch (REngineException ex) {
			System.out.println(ex.getMessage());
		}
		*/
		
		/*
		System.out.println(getDBConnCmd("pegasus.dbConn"));
		System.out.println(rVarName + " <- dbGetQuery(pegasus.dbConn, 'SELECT \"" + dbVarName + "\" AS x FROM \"" + Do.DB_DATA_DS + "-" + username + "\".ds_" + dsId + " WHERE \"" + dbVarName + "\" IS NOT NULL')");
		*/
		
		// TODO: the line below has been altered to retrieve only the first 1000 rows; otherwise, big data sets result in a BrokenPipe exception
		rconn.eval(rVarName + " <- dbGetQuery(pegasus.dbConn, 'SELECT \"" + dbVarName + "\" AS x FROM \"" + Do.DB_DATA_DS + "-" + username + "\".ds_" + dsId + " WHERE \"" + dbVarName + "\" IS NOT NULL LIMIT 1000')");
		//System.out.println(rVarName + " <- dbGetQuery(pegasus.dbConn, 'SELECT \"" + dbVarName + "\" AS x FROM \"" + Do.DB_DATA_DS + "-" + username + "\".ds_" + dsId + " WHERE \"" + dbVarName + "\" IS NOT NULL LIMIT 1000')");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Translates name of a variable to the R-acceptable format.
	 */
	public static String trName(String s) {
		//return s.replaceAll("-", ".");
		return s;
	}
}
