package app.data;

import inter.DBI;

import java.util.Hashtable;

import main.Do;


/**
 * Value substirution unit
 * 
 * @author Tomek D. Loboda
 */
public class ValSubstUnit {
	private final String name;
	private final int type;
	
	private final Hashtable<String,String> map = new Hashtable<String,String>();
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Automatically determine if the variable should be of the DIS, CON, or STR type.
	 * 
	 * spec: "0->m,1->f|skill:0->low,1->medium,2->high"
	 */
	public ValSubstUnit(String name, String spec, int type) {
		boolean isInt = (type == Var.TYPE_BIN || type == Var.TYPE_CAT || type == Var.TYPE_DIS || type == Var.TYPE_ORD);
		boolean isFloat = (type == Var.TYPE_CON);
		
		String[] A = spec.split(",");
		for (String a: A) {
			String[] B = a.split("->");
			
			if (B[1].equals(Do.MISS_VAL)) {
				System.out.println(B[0] + " -> " + Do.MISS_VAL);
				map.put(B[0], "null");
				continue;
			}
			
			map.put(B[0], B[1]);
			
			if (isInt) {
				try { new Integer(B[1]); }
				catch (NumberFormatException e) { isInt = false; }
			}
			if (isFloat) {
				try { new Double(B[1]); }
				catch (NumberFormatException e) { isFloat = false; }
			}
		}
		
		this.name = name;
		this.type = (isInt ? Var.TYPE_DIS : (isFloat ? Var.TYPE_CON : Var.TYPE_STR));
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Returns a value usable directly in a SQL query.
	 */
	public String getValSQL(String val) {
		if (val == null || val.equals(Do.MISS_VAL)) return "null";
		
		if (map.containsKey(val)) {
			if (type != Var.TYPE_STR) return map.get(val);
			else return DBI.esc(map.get(val));
		}
		else return val;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public String getName() { return name; }
	public int    getType() { return type; }
}
