package app.adm;

import inter.DBI;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import main.Do;
import main.Result;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import app.gaze.ROI;
import app.gaze.Stim;
import app.gaze.Var;
import exception.ValidException;


/**
 * @author Tomek D. Loboda
 */
public class Adm {
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Adm() {}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Do ad-hoc type of work.
	 * 
	 * http://localhost:8080/pegasus/do?username=tomek&app=adm&cmd=tmp
	 */
	public static void tmp(DBI dbi, String username)  throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		/*
		String dbG = "\"" + Do.DB_GAZE + "-" + username + "\"";
		
		DBI dbiX = new DBI();
		Statement stX = dbiX.transBegin();
		stX.execute("SET search_path TO " + dbG + "");
		
		try {
			Var.addSys(dbiX, stX, username, 92, "roi", "n", "pred-m3-pe-log", "Word log-predictability (Microsoft 3-grams; punctuation excluded)", false);
			Var.addSys(dbiX, stX, username, 92, "roi", "n", "pred-m4-pe-log", "Word log-predictability (Microsoft 4-grams; punctuation excluded)", false);
			Var.addSys(dbiX, stX, username, 92, "roi", "n", "pred-m5-pe-log", "Word log-predictability (Microsoft 5-grams; punctuation excluded)", false);
			
			Var.addSys(dbiX, stX, username, 92, "roi", "n", "pred-m3-pi-log", "Word log-predictability (Microsoft 3-grams; punctuation ignored)", false);
			Var.addSys(dbiX, stX, username, 92, "roi", "n", "pred-m4-pi-log", "Word log-predictability (Microsoft 4-grams; punctuation ignored)", false);
			Var.addSys(dbiX, stX, username, 92, "roi", "n", "pred-m5-pi-log", "Word log-predictability (Microsoft 5-grams; punctuation ignored)", false);
			
			DBI.transCommit(stX, true);
		}
		catch (SQLException e) {
			DBI.transRollback(stX, true);
			throw e;
		}
		*/
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Calculates times of all the trials.
	 */
	public static Result calcTrialTimes(DBI dbi, String username)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException, IOException, ValidException {
		String dbG = "\"" + Do.DB_GAZE + "-" + username + "\"";
		int trialCnt = 0;
		
		int prevSubId = -1;
		int t0 = 0;
		int t1 = 0;
		int approxInterTrialT = 120000;
		
		Statement stX = (new DBI()).transBegin();
		stX.execute("SET search_path TO " + dbG + "");
		try {
			ResultSet rs = dbi.execQry("SELECT id, sub_id, dur FROM " + dbG + ".trial ORDER BY id");
			while (rs.next()) {
				int trialId = rs.getInt("id");
				int subId   = rs.getInt("sub_id");
				int dur     = rs.getInt("dur");
				
				if (subId != prevSubId) {
					t0 = 0;
					t1 = dur;
				}
				else {
					t0  = t1  + approxInterTrialT;
					t1 += dur + approxInterTrialT;
				}
				
				stX.executeUpdate("UPDATE trial SET t0 = " + t0 + ", t1 = " + t1 + " WHERE id = " + trialId);
				
				trialCnt++;
				
				prevSubId = subId;
			}
			rs.close();
			
			DBI.transCommit(stX, true);
		}
		catch (SQLException e) {
			DBI.transRollback(stX, true);
			throw e;
		}
		
		return new Result(true, "msg:\"Trials processed: " + trialCnt + "\"");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Copies trial times from the source to the destination experiment.
	 */
	public static Result copyTrialTimes(DBI dbi, String username, int expIdSrc, int expIdDst)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		String dbG = "\"" + Do.DB_GAZE + "-" + username + "\"";
		int trialCnt = 0;
		
		Statement stX = (new DBI()).transBegin();
		stX.execute("SET search_path TO " + dbG + "");
		try {
			stX.executeUpdate("UPDATE " + dbG + ".trial t SET t0 = null, t1 = null FROM " + dbG + ".sub s WHERE t.sub_id = s.id AND s.exp_id = " + expIdDst);
			
			ResultSet rs = dbi.execQry("SELECT s.name AS sname, t.name AS tname, t.t0, t.t1 FROM " + dbG + ".trial t INNER JOIN " + dbG + ".sub s ON t.sub_id = s.id WHERE s.exp_id = " + expIdSrc);
			while (rs.next()) {
				String sname = rs.getString("sname");
				String tname = rs.getString("tname");
				int t0 = rs.getInt("t0");
				int t1 = rs.getInt("t1");
				
				stX.executeUpdate("UPDATE " + dbG + ".trial t SET t0 = " + t0 + ", t1 = " + t1 + " FROM " + dbG + ".sub s WHERE t.sub_id = s.id AND s.exp_id = " +  expIdDst + " AND s.name = '" + sname + "' AND t.name = '" + tname + "'");
				
				trialCnt++;
			}
			rs.close();
			
			DBI.transCommit(stX, true);
		}
		catch (SQLException e) {
			DBI.transRollback(stX, true);
			throw e;
		}
		
		return new Result(true, "msg:\"Trials processed: " + trialCnt + "\"");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Clears the recorded time columns for all trials in the designated experiment.
	 * 
	 * http://localhost:8080/pegasus/do?app=adm&username=tomek&cmd=clear-trial-rec-time&exp-id=92
	 * 
	 */
	public static Result clearTrialRecTime(DBI dbi, String username, int expId)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		String dbG = "\"" + Do.DB_GAZE + "-" + username + "\"";
		
		Statement stX = (new DBI()).transBegin();
		stX.execute("SET search_path TO " + dbG + "");
		try {
			stX.executeUpdate("UPDATE " + dbG + ".trial t SET t0_rec = null, t1_rec = null FROM " + dbG + ".sub s WHERE t.sub_id = s.id AND s.exp_id = " + expId);
			DBI.transCommit(stX, true);
		}
		catch (SQLException e) {
			DBI.transRollback(stX, true);
			throw e;
		}
		
		return new Result(true, "msg:\"Trials processed.\"");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Sets the recorded time columns for all trials in the designated experiment.
	 * 
	 * http://localhost:8080/pegasus/do?app=adm&username=tomek&cmd=set-trial-rec-time&exp-id=92
	 */
	public static Result setTrialRecTime(DBI dbi, String username, int expId)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		String dbG = "\"" + Do.DB_GAZE + "-" + username + "\"";
		int trialCnt = 0;
		
		Statement stX = (new DBI()).transBegin();
		stX.execute("SET search_path TO " + dbG + "");
		try {
			stX.executeUpdate("UPDATE " + dbG + ".trial t SET t0_rec = null, t1_rec = null FROM " + dbG + ".sub s WHERE t.sub_id = s.id AND s.exp_id = " + expId);
			
			Long prevSID = new Long(DBI.ID_NONE);
			long prevT1  = 0;
			
			ResultSet rs = dbi.execQry("SELECT t.id AS tid, s.id AS sid, t.dur FROM " + dbG + ".trial t INNER JOIN " + dbG + ".sub s ON t.sub_id = s.id WHERE s.exp_id = " + expId + " ORDER BY t.id");
			while (rs.next()) {
				Long tid = rs.getLong("tid");
				Long sid = rs.getLong("sid");
				Long dur = rs.getLong("dur");
				
				if (!rs.wasNull()) {  // if trial duration is non-null
					long t0, t1;
					if (sid == prevSID.longValue()) {  // same subject -- continuing
						t0 = prevT1;
						t1 = prevT1 + dur;
					}
					else {  // new subject -- restarting from zero
						t0 = 0;
						t1 = dur;
					}
					
					stX.executeUpdate("UPDATE " + dbG + ".trial t SET t0_rec = " + t0 + ", t1_rec = " + t1 + " FROM " + dbG + ".sub s WHERE t.id = " + tid);
					
					prevT1 = t1;
				}
				
				prevSID = sid;
				
				trialCnt++;
			}
			rs.close();
			
			DBI.transCommit(stX, true);
		}
		catch (SQLException e) {
			DBI.transRollback(stX, true);
			throw e;
		}
		
		return new Result(true, "msg:\"Trials processed: " + trialCnt + "\"");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Generates images of all the ROIs.
	 */
	public static Result genROIImgs(DBI dbi, String username)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException, IOException, ValidException {
		String dbG = "\"" + Do.DB_GAZE + "-" + username + "\"";
		int stimCnt = 0;
		
		Statement stX = (new DBI()).transBegin();
		stX.execute("SET search_path TO " + dbG + "");
		try {
			ResultSet rsStim = dbi.execQry("SELECT id FROM " + dbG + ".stim");
			while (rsStim.next()) {
				int stimId = rsStim.getInt("id");
				List<ROI> rois = new ArrayList<ROI>();
				
				// Retrieve ROIs:
				ResultSet rsROI = dbi.execQry("SELECT id FROM " + dbG + ".roi WHERE stim_id = " + stimId);
				while (rsROI.next()) {
					rois.add(new ROI(dbi, username, stimId, rsROI.getInt("id")));
				}
				rsROI.close();
				
				// Generate the image:
				Stim.genROIImg(dbi, username, stX, stimId, rois);
				stimCnt++;
			}
			rsStim.close();
			
			DBI.transCommit(stX, true);
		}
		catch (SQLException e) {
			DBI.transRollback(stX, true);
			throw e;
		}
		
		return new Result(true, "msg:\"Stimuli processed: " + stimCnt + "\"");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Part-of-speech tags text of all stimuli.  The input is read from the 'txt' column and the output is saved in the 
	 * 'txt_pos' column.  Additionally, , the 'pos' field of every ROI involved is set accordingly.
	 * 
	 * http://localhost:8080/pegasus/do?app=adm&cmd=pos-tag-stim-txt&username=tomek
	 */
	public static Result posTagStimTxt(DBI dbi, String username)  throws IOException, SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException, ValidException {
		String dbG = "\"" + Do.DB_GAZE + "-" + username + "\"";
		
		int stimCnt = 0;
		
		InputStream fisModel = null;
		try {
			fisModel = new FileInputStream(Do.getPathRes() + "pos-tagger-models/en-pos-maxent.bin");  // en-pos-perceptron.bin
			
			POSModel model = new POSModel(fisModel);
			POSTaggerME tagger = new POSTaggerME(model);
			
			Statement stX = (new DBI()).transBegin();
			
			stX.execute("SET search_path TO " + dbG + "");
			
			stX.executeUpdate("UPDATE stim SET txt_pos = null");
			stX.executeUpdate("UPDATE roi SET pos = null");
			
			try {
				ResultSet rsStim = dbi.execQry("SELECT s.id, COUNT(r.id) AS roi_cnt FROM " + dbG + ".stim s INNER JOIN " + dbG + ".roi r ON r.stim_id = s.id GROUP BY s.id ORDER BY s.id");
				while (rsStim.next()) {
					int stimId = rsStim.getInt("id");
					int roiCnt = rsStim.getInt("roi_cnt");
					
					if (roiCnt == 0) continue;
					
					int    ids   [] = new int    [roiCnt];
					String names [] = new String [roiCnt];
					
					// Retrieve ROIs:
					int i = 0;
					ResultSet rsROI = dbi.execQry("SELECT id, name FROM " + dbG + ".roi WHERE stim_id = " + stimId + " ORDER BY id");
					while (rsROI.next()) {
						ids   [i] = rsROI.getInt    ("id");
						names [i] = rsROI.getString ("name").trim();
						i++;
					}
					rsROI.close();
					
					// POS tag:
					String tags[] = tagger.tag(names);
					
					// Update the DB:
					StringBuffer txtPOS = new StringBuffer();
					for (i = 0; i < roiCnt; i++) {
						stX.executeUpdate("UPDATE roi SET pos = '" + tags[i] + "' WHERE id = " + ids[i]);
						txtPOS.append(names[i] + "_" + tags[i] + " ");
					}
					stX.executeUpdate("UPDATE stim SET txt_pos = " + DBI.esc(txtPOS.toString().substring(0, txtPOS.length() - 2)) + " WHERE id = " + stimId);
					
					stimCnt++;
				}
				rsStim.close();
				
				DBI.transCommit(stX, true);
			}
			catch (SQLException e) {
				DBI.transRollback(stX, true);
				throw e;
			}
			
			/*
			String W[] = new String[] { "Testing", "this", "cool", "new", "feature", ".", "Testing", "this", "cool", "new", "(quite", "unknown)", "feature", ".", "The", "list", "is", "this:", "one", "." };		  
			String T[] = tagger.tag(W);
			
			for (int i = 0; i < W.length; i++) {
				res.append(W[i] + "_" + T[i] + " ");
			}
			*/
		}
		catch (IOException e) { throw e; }
		finally {
			if (fisModel != null) {
				try {
					fisModel.close();
				}
				catch (IOException e) { throw e; }
			}
		}
		
		return new Result(true, "msg:\"Stimuli processed: " + stimCnt + "\"");
	}
}
