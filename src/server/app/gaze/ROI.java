package app.gaze;

import inter.DBI;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import main.Do;
import util.RegExpr;
import exception.ValidException;


/**
 * @author Tomek D. Loboda
 */
public class ROI {
	public final String name;
	public final int x1;
	public final int y1;
	public final int x2;
	public final int y2;
	public final int num;
	public final List<String> vars = new ArrayList<String>();
	
	public final String spec;  // the specification from which all the info is extracted from (null if retrieved from the DB)
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Pass all data as arguments.
	 */
	public ROI(DBI dbi, String username, int stimId, String spec, int num)  throws ValidException, SQLException {
		this.spec = spec;
		
		Matcher m = RegExpr.match("^.*\\t\\d+,\\d+,\\d+,\\d+\\t.*$|^.*\\t\\d+,\\d+,\\d+,\\d+$", spec);
		if (!m.matches()) throw new ValidException("Invalid ROI format (line " + num + "), stimulus '" + DBI.getStr(dbi, "SELECT name FROM \"" + Do.DB_GAZE + "-" + username + "\".stim WHERE id = " + stimId, "name") + "':\n\n" + spec + "\n\nRemember that the tab character needs to be used to separate the word from the coordinates and the coordinates from the variables. If there are no variables the second tab character is non-mandatory, but no other character can be used instead of it (e.g., space is not allowed)");
		
		// ROI:
		String S[] = spec.split("\t");
		name = S[0].replaceAll("'", "\\\\'");
		this.num = num;
		
		// Vertices:
		String[] coords = S[1].split(",");
		x1 = new Integer(coords[0]);
		y1 = new Integer(coords[1]);
		x2 = new Integer(coords[2]);
		y2 = new Integer(coords[3]);
		
		// Variables:
		if (S.length == 3) {
			String[] V = S[2].split(",");
			for (String v: V) {
				vars.add(v);
			}
		}
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Retrieve the data from the DB.
	 */
	public ROI(DBI dbi, String username, int stimId, int id)  throws ValidException, SQLException {
		String dbG = "\"" + Do.DB_GAZE + "-" + username + "\"";
		spec = null;
		
		// ROI:
		ResultSet rsROI = dbi.execQry("SELECT name, num FROM " + dbG + ".roi WHERE id = " + id);
		rsROI.next();
		name = rsROI.getString("name");
		num = rsROI.getInt("num");
		rsROI.close();
		
		// Vertices:
		ResultSet rsVertex = dbi.execQry("SELECT x,y FROM " + dbG + ".vertex WHERE roi_id = " + id + " ORDER BY id");
		rsVertex.next();
		x1 = rsVertex.getInt("x");
		y1 = rsVertex.getInt("y");
		rsVertex.next();
		rsVertex.next();
		x2 = rsVertex.getInt("x");
		y2 = rsVertex.getInt("y");
		rsVertex.close();
		
		// Variables:
		// TODO
	}
}
