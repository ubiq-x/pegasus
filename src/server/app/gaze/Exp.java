package app.gaze;

import inter.DBI;
import inter.JSI;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import main.Do;
import main.Result;

import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.Rserve.RserveException;

import util.$;
import util.Convert;
import app.data.DataSet;
import app.gaze.dstruct.Fix;
import app.gaze.dstruct.ROI;
import app.gaze.dstruct.Stim;
import app.gaze.ngram.NgramMan;
import app.gaze.oper.StimSeq;


/**
 * @author Tomek D. Loboda
 */
public class Exp {
	public static final int DIR_NONE = 0;
	public static final int DIR_NW   = 1;
	public static final int DIR_N    = 2;
	public static final int DIR_NE   = 3;
	public static final int DIR_E    = 4;
	public static final int DIR_SE   = 5;
	public static final int DIR_S    = 6;
	public static final int DIR_SW   = 7;
	public static final int DIR_W    = 8;
	public static final int DIR_I    = 9;  // for fixations: off-text but within the text bounding box (i.e., between paragraphs)
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Exp() {}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static Result addNew(DBI dbi, String username, int prjId, String name)  throws UnsupportedEncodingException, InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException, RserveException, REXPMismatchException {
		int expId = DBI.ID_NONE;
		
		DBI dbiX = new DBI();
		Statement stX = dbiX.transBegin();
		stX.execute("SET search_path TO \"" + Do.DB_GAZE + "-" + username + "\"");
		
		try {
			// (1) Add the experiment:
			expId = DBI.insertGetId(stX, null, "INSERT INTO exp (prj_id, name) VALUES (" + prjId + ", " + DBI.esc(name) + ")");
			
			// (2) Add system variables:
			stX.executeUpdate("INSERT INTO var (exp_id, obj_name, type, name, memo, sys, def_val) VALUES (" + expId + ", 'roi', 'b', 'pg-s',   'Page start',     1, '0')");
			stX.executeUpdate("INSERT INTO var (exp_id, obj_name, type, name, memo, sys, def_val) VALUES (" + expId + ", 'roi', 'b', 'pg-e',   'Page end',       1, '0')");
			stX.executeUpdate("INSERT INTO var (exp_id, obj_name, type, name, memo, sys, def_val) VALUES (" + expId + ", 'roi', 'b', 'ln-s',   'Line start',     1, '0')");
			stX.executeUpdate("INSERT INTO var (exp_id, obj_name, type, name, memo, sys, def_val) VALUES (" + expId + ", 'roi', 'b', 'ln-e',   'Line end',       1, '0')");
			stX.executeUpdate("INSERT INTO var (exp_id, obj_name, type, name, memo, sys, def_val) VALUES (" + expId + ", 'roi', 'b', 'sent-s', 'Sentence start', 1, '0')");
			stX.executeUpdate("INSERT INTO var (exp_id, obj_name, type, name, memo, sys, def_val) VALUES (" + expId + ", 'roi', 'b', 'sent-e', 'Sentence end',   1, '0')");
			stX.executeUpdate("INSERT INTO var (exp_id, obj_name, type, name, memo, sys, def_val) VALUES (" + expId + ", 'roi', 'b', 'cl-s',   'Clause start',   1, '0')");
			stX.executeUpdate("INSERT INTO var (exp_id, obj_name, type, name, memo, sys, def_val) VALUES (" + expId + ", 'roi', 'b', 'cl-e',   'Clause end',     1, '0')");
			
			DBI.transCommit(stX, true);
		}
		catch (SQLException e) {
			DBI.transRollback(stX, true);
			
			if (e.getErrorCode() == DBI.SQL_ERR_DUP_ENTRY) return new Result(false, "msg:\"An experiment with that name already exists. Experiment names must be unique within a project.\"");
			else throw e;
		}
		
		return new Result(true, "exp:" + get(dbi, username, expId));
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static Result adjEMs(DBI dbi, String username, int expId, String ssIds)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		util.Timer timer = new util.Timer();
		
		String db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		int lnCnt = 0;
		
		DBI dbiX = new DBI();
		Statement stX = dbiX.transBegin();
		
		try {
			// (1) Load stimuli (along with ROIs):
			Hashtable<Integer,Stim> stim = new Hashtable<Integer,Stim>();
			
			ResultSet rsStim = dbi.execQry("SELECT id, name, txt, res_h, res_v FROM " + db + ".stim WHERE exp_id = " + expId + " AND type = 't'");
			
			while (rsStim.next()) {
				Stim s = new Stim(rsStim.getInt("id"), rsStim.getString("name"), rsStim.getString("txt"), rsStim.getInt("res_h"), rsStim.getInt("res_v"));
				s.load(dbiX, db);
				stim.put(s.id, s);
			}
			
			rsStim.close();
			
			// (2) Process all stimuli sequences:
			ResultSet rsSS = dbi.execQry("SELECT ss.id, ss.stim_id FROM " + db + ".stim_seq ss INNER JOIN " + db + ".stim s ON ss.stim_id = s.id WHERE s.exp_id = " + expId + (ssIds != null && ssIds.length() > 0 ? " AND ss.id IN (" + ssIds + ")" : "") + " ORDER BY ss.id");
			
			while (rsSS.next()) {
				Stim s = stim.get(rsSS.getInt("stim_id"));
				if (s == null) continue;
				
				app.gaze.adj.StimSeq ss = new app.gaze.adj.StimSeq(dbiX, db, rsSS.getInt("id"), s, 80, 6, 30, 5);  // TODO: params
				lnCnt += ss.adj();
			}
			
			rsSS.close();
			
			stX.executeUpdate("UPDATE " + db + ".trial SET linked = FALSE WHERE id IN (SELECT t.id FROM " + db + ".trial t INNER JOIN " + db + ".sub s ON t.sub_id = s.id WHERE s.exp_id = " + expId + ");");
			stX.executeUpdate("UPDATE " + db + ".exp SET linked = FALSE WHERE id = " + expId);
			
			DBI.transCommit(stX, true);
		}
		catch (SQLException e) {
			DBI.transRollback(stX, true);
			throw e;
		}
		
		long t = timer.elapsed();
		return new Result(true, "rep:" + JSI.str2js("Lines identified: " + lnCnt + "\nExecution time: " + Convert.time2str(t) + " (" + t + " ms)"));
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static Result del(DBI dbi, String username, int expId)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		dbi.execUpd("DELETE FROM \"" + Do.DB_GAZE + "-" + username + "\".exp WHERE id = " + expId);
		return new Result(true, "");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static Result dsAttrSet(DBI dbi, String username, int dsId, String attrName, String attrVal)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		if (DBI.getLong(dbi, "SELECT id FROM \"" + Do.DB_GAZE + "-" + username + "\".ds WHERE ds_id = " + dsId, "id") == DBI.ID_NONE) return new Result(false, "msg:\"The specified data set does not exist or you do not have access rights to change that attribute of it.\"");
		if (!app.data.DB.isObjAttrValid(dbi, "ds", attrName)) return new Result(false, "msg:\"Trying to set an invalid attribute.\"");
		
		return DBI.attrSet(dbi, username, Do.APP_ID_STR_DATA, "ds", "data set", dsId, attrName, attrVal);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	@SuppressWarnings("deprecation")
	public static StringBuffer get(DBI dbi, String username, int expId)  throws SQLException, UnsupportedEncodingException, InstantiationException, IllegalAccessException, ClassNotFoundException, RserveException, REXPMismatchException {
		String db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		
		StringBuffer res = new StringBuffer();
		
		// (1) Experiment:
		ResultSet rsExp = dbi.execQry(
			"SELECT id, name, created, memo, LENGTH(imp_rep) AS imp_rep_len, imp_ts, linked, stim_zoom, note, LENGTH(note) AS note_len," +
			"(SELECT COUNT(*) FROM " + db + ".sub WHERE exp_id = " + expId + ") AS sub_cnt, " +
			"(SELECT COUNT(*) FROM " + db + ".trial INNER JOIN " + db + ".sub ON trial.sub_id = sub.id WHERE sub.exp_id = " + expId + ") AS trial_cnt, " +
			"(SELECT COUNT(*) FROM " + db + ".stim WHERE exp_id = " + expId + ") AS stim_cnt, " +
			"(SELECT COUNT(*) FROM " + db + ".roi INNER JOIN " + db + ".stim ON roi.stim_id = stim.id WHERE stim.exp_id = " + expId + ") AS roi_cnt " +
			"FROM " + db + ".exp e WHERE e.id = " + expId
		);
		rsExp.next();
		
		boolean impRep = (rsExp.getInt("imp_rep_len") > 0);
		String expMemo = rsExp.getString("memo");
		res.append(
			"{" +
			"loaded:true," +
			"id:" + expId + "," +
			"name:" + JSI.str2js(rsExp.getString("name")) + "," +
			(expMemo != null && expMemo.length() > 0 ? "memo:" + JSI.str2js(expMemo) + "," : "") +
			"created:" + (rsExp.getTimestamp("created").getYear() == 0 ? "null" : "new Date(" + rsExp.getTimestamp("created").getTime() + ")") + "," +
			"impDate:" + (!impRep ? "null" : "new Date(" + rsExp.getTimestamp("imp_ts").getTime() + ")") + "," +
			"impRep:null," +
			"linked:" + rsExp.getBoolean("linked") + "," +
			"subCnt:" + rsExp.getInt("sub_cnt") + "," +
			"trialCnt:" + rsExp.getInt("trial_cnt") + "," +
			"stimCnt:" + rsExp.getInt("stim_cnt") + "," +
			"roiCnt:" + rsExp.getInt("roi_cnt") + "," +
			"note:" + JSI.str2js(rsExp.getString("note")) + "," +
			"noteLen:" + rsExp.getInt("note_len") + "," +
			"settings:{" +
			"stim: {" +
			"zoom:" + rsExp.getFloat("stim_zoom") +
			"}" +
			"},"
		);
		
		// (2) Subjects:
		res.append("sub:[");
		ResultSet rsSub = dbi.execQry("SELECT id, name, memo, sex, exc, exc_info FROM " + db + ".sub WHERE exp_id = " + expId + " ORDER BY name");
		while (rsSub.next()) {
			int subId = rsSub.getInt("id");
			String subMemo = rsSub.getString("memo");
			res.append(
				"{" +
				"id:" + subId + "," +
				"name:" + JSI.str2js(rsSub.getString("name")) + "," +
				(subMemo != null && subMemo.length() > 0 ? "memo:" + JSI.str2js(subMemo) + "," : "") +
				"exc:" + (rsSub.getBoolean("exc") ? "true" : "false") + "," +
				"excInfo:" + JSI.str2js(rsSub.getString("exc_info")) + ","
			);
			
			// (2a) Trials:
			res.append("trials:[");
			//ResultSet rsTrial = dbi.execQry("SELECT id, name, t0, t1, t0_rec, t1_rec, dur, t_buff, memo, exc, exc_info FROM " + db + ".trial WHERE sub_id = " + subId + " ORDER BY id");
				// Quick fix to make it work for GitLab description (2018.09.19)
			ResultSet rsTrial = dbi.execQry("SELECT id, name, t0, t1, dur, t_buff, memo, exc, exc_info FROM " + db + ".trial WHERE sub_id = " + subId + " ORDER BY id");
			while (rsTrial.next()) {
				int trialId = rsTrial.getInt("id");
				String trialMemo = rsTrial.getString("memo");
				res.append(
					"{" +
					"id:\"" + trialId +"\"," +
					"name:" + JSI.str2js(rsTrial.getString("name")) +"," +
					"t0:" + rsTrial.getString("t0") +"," +
					"t1:" + rsTrial.getString("t1") +"," +
					//"t0rec:" + rsTrial.getString("t0_rec") +"," +
					//"t1rec:" + rsTrial.getString("t1_rec") +"," +
						// Quick fix to make it work for GitLab description (2018.09.19)
					"t0rec:0," +
					"t1rec:0," +
					"dur:" + rsTrial.getString("dur") +"," +
					"tBuff:" + rsTrial.getString("t_buff") +"," +
					(trialMemo != null && trialMemo.length() > 0 ? "memo:" + JSI.str2js(trialMemo) + "," : "") +
					"exc:" + (rsTrial.getBoolean("exc") ? "true" : "false") + "," +
					"excInfo:" + JSI.str2js(rsTrial.getString("exc_info")) + "," +
					"cal:" + getCal(dbi, username, trialId) + "," +
					"stimSeq:" + (new StimSeq(dbi)).get(username, rsTrial.getInt("id")) +
					"}" + (!rsTrial.isLast() ? "," : "")
				);
			}
			rsTrial.close();
			res.append("]");
			
			res.append("}" + (!rsSub.isLast() ? "," : ""));
		}
		rsSub.close();
		res.append("],");
		
		// (3) Stimuli:
		res.append("stim:[");
		ResultSet rsStim = dbi.execQry(
			"SELECT s.id, s.exp_id, s.name, s.memo, s.txt, s.type, LENGTH(s.img) AS img_len, s.res_h, s.res_v, s.font_fam, s.font_size, s.roi_h, s.roi_exp, s.offset_x, s.offset_y, s.align_h, s.align_v, s.margin_t, s.margin_l, s.margin_r, s.letter_spac, s.line_h, s.padding_t, s.padding_b, s.box_x1, s.box_y1, s.box_x2, s.box_y2, exc, exc_info, COALESCE(r.roi_cnt, 0) AS roi_cnt " +
			"FROM " + db + ".stim s " +
			"LEFT JOIN (SELECT stim_id, COUNT(*) AS roi_cnt FROM " + db + ".roi GROUP BY stim_id) r ON r.stim_id = s.id " +
			"WHERE s.exp_id = " + expId + " " +
			"ORDER BY s.id"
		);
		while (rsStim.next()) {
			String stimMemo = rsStim.getString("memo");
			res.append(
				"{" +
				"id:" + rsStim.getString("id") + "," +
				"name:" + JSI.str2js(rsStim.getString("name")) + "," +
				"type:\"" + rsStim.getString("type") + "\"," +
				"txt:" + JSI.str2js(rsStim.getString("txt")) +"," +
				"img:{" +
				"len:" + rsStim.getInt("img_len") + "," +
				"uri:null" +
				"}," +
				"imgROI:{" +
				"uri:null" +
				"}," +
				"resH:" + rsStim.getString("res_h") + "," +
				"resV:" + rsStim.getString("res_v") + "," +
				"fontFamily:\"" + rsStim.getString("font_fam") + "\"," +
				"fontSize:" + rsStim.getString("font_size") + "," +
				"letterSpac:" + rsStim.getString("letter_spac") + "," +
				"lineH:" + rsStim.getString("line_h") + "," +
				"offsetX:" + rsStim.getString("offset_x") + "," +
				"offsetY:" + rsStim.getString("offset_y") + "," +
				"alignH:\"" + rsStim.getString("align_h") + "\"," +
				"alignV:\"" + rsStim.getString("align_v") + "\"," +
				"marginT:" + rsStim.getString("margin_t") + "," +
				"marginL:" + rsStim.getString("margin_l") + "," +
				"marginR:" + rsStim.getString("margin_r") + "," +
				"paddingT:" + rsStim.getString("padding_t") + "," +
				"paddingB:" + rsStim.getString("padding_b") + "," +
				"roiH:" + rsStim.getString("roi_h") + "," +
				"roiExp:" + (rsStim.getBoolean("roi_exp") ? "\"1\"" : "\"0\"") + "," +
				(stimMemo != null && stimMemo.length() > 0 ? "memo:" + JSI.str2js(stimMemo) + "," : "") +
				"exc:" + (rsStim.getBoolean("exc") ? "true" : "false") + "," +
				"excInfo:" + JSI.str2js(rsStim.getString("exc_info")) + "," +
				"rois:null," +
				"roiCnt:" + rsStim.getString("roi_cnt") +
				"}" + (!rsStim.isLast() ? "," : "")
			);
		}
		rsStim.close();
		res.append("],");
		
		// (4) Variables:
		res.append(
			"vars:{" +
			"roi:"   + Var.getAll(dbi, username, expId, "roi",   false) + "," +
			"stim:"  + Var.getAll(dbi, username, expId, "stim",  true ) + "," +
			"sub:"   + Var.getAll(dbi, username, expId, "sub",   true ) + "," +
			"trial:" + Var.getAll(dbi, username, expId, "trial", true ) +
			"},"
		);
		
		// (5) Events:
		res.append("evt:[");
		ResultSet rsEvt = dbi.execQry("SELECT name, memo FROM " + db + ".evt_lst WHERE exp_id = " + expId);
		while (rsEvt.next()) {
			String evtMemo = rsEvt.getString("memo");
			res.append(
				"{" +
				"name:" + JSI.str2js(rsEvt.getString("name")) + "," +
				(evtMemo != null && evtMemo.length() > 0 ? "memo:" + JSI.str2js(evtMemo) : "") +
				"}" + (!rsEvt.isLast() ? "," : "")
			);
		}
		rsEvt.close();
		res.append("],");
		
		// (6) Datasets:
		ResultSet rsDS = dbi.execQry(ResultSet.TYPE_SCROLL_INSENSITIVE, "SELECT ds_id FROM " + db + ".ds WHERE exp_id = " + expId);
		if (DBI.getRecCnt(rsDS) == 0) {
			res.append("ds:[]");
		}
		else {
			String dsIds = "";
			while (rsDS.next()) {
				dsIds += rsDS.getInt("ds_id") + (!rsDS.isLast() ? "," : "");
			}
			res.append("ds:" + DataSet.getLst(dbi, username, null, dsIds, null));
		}
		rsDS.close();
		
		res.append("}");
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Returns calibrations for the specified trial.
	 */
	public static StringBuffer getCal(DBI dbi, String username, int trialId)  throws SQLException {
		String db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		StringBuffer res = new StringBuffer("[");
		
		ResultSet rsCal = dbi.execQry("SELECT id, t, eye, point_cnt, cal_res, val_res, val_err_avg, val_err_max, stim_cnt, dur, scr_w, scr_h FROM " + db + ".cal WHERE trial_id = " + trialId + " ORDER BY id");
		while (rsCal.next()) {
			int calId = rsCal.getInt("id");
			
			res.append(
				"{" +
				"id:" + calId + "," +
				"t:" + rsCal.getString("t") + "," +
				"eye:\"" + rsCal.getString("eye") + "\"," +
				"pointCnt:" + rsCal.getString("point_cnt") + "," +
				"calRes:\"" + rsCal.getString("cal_res") + "\"," +
				"valRes:\"" + rsCal.getString("val_res") + "\"," +
				"valErrAvg:" + rsCal.getString("val_err_avg") + "," +
				"valErrMax:" + rsCal.getString("val_err_max") + "," +
				"stimCnt:" + rsCal.getString("stim_cnt") + "," +
				"dur:" + rsCal.getString("dur") + "," +
				"scrW:" + rsCal.getString("scr_w") + "," +
				"scrH:" + rsCal.getString("scr_h") + "," +
				"points:["
			);
			
			ResultSet rsCalPoints = dbi.execQry("SELECT id, num, x, y, x_err, y_err, ang_err FROM " + db + ".cal_point WHERE cal_id = " + calId + " ORDER BY num");
			while (rsCalPoints.next()) {
				res.append(
					"{" +
					"id:" + rsCalPoints.getString("id") + "," +
					"num:" + rsCalPoints.getString("num") + "," +
					"x:" + rsCalPoints.getString("x") + "," +
					"y:" + rsCalPoints.getString("y") + "," +
					"xErr:" + rsCalPoints.getString("x_err") + "," +
					"yErr:" + rsCalPoints.getString("y_err") + "," +
					"angErr:" + rsCalPoints.getString("ang_err") +
					"}" +
					(!rsCalPoints.isLast() ? "," : "")
				);
			}
			rsCalPoints.close();
			
			res.append("]}" + (!rsCal.isLast() ? "," : ""));
		}
		rsCal.close();
		res.append("]");
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static long getCnt(DBI dbi, String username)  throws SQLException {
		return DBI.getLong(dbi, "SELECT COUNT(id) AS cnt FROM \"" + Do.DB_GAZE + "-" + username + "\".exp", "cnt");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static StringBuffer getExcRep(DBI dbi, String username, int expId)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		String db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		StringBuffer res = new StringBuffer();
		
		// (1) Stimuli:
		res.append("STIMULI\n");
		ResultSet rsStim = dbi.execQry("SELECT s.name, s.exc, COALESCE(s.exc_info, '') AS exc_info FROM " + db + ".stim s WHERE s.exp_id = " + expId + " ORDER BY name");
		while (rsStim.next()) {
			res.append(
				"\tstim:\t" + rsStim.getString("name") +
				(rsStim.getBoolean("exc") ? "\tEXC\t" + rsStim.getString("exc_info") : "") +
				"\n"
			);
		}
		rsStim.close();
		res.append("\n");
		
		// (2) Subjects:
		res.append("SUBJECTS\n");
		ResultSet rsSub = dbi.execQry("SELECT s.id, s.name, s.exc, COALESCE(s.exc_info, '') AS exc_info FROM " + db + ".sub s WHERE s.exp_id = " + expId + " ORDER BY name");
		while (rsSub.next()) {
			int subId = rsSub.getInt("id");
			res.append(
				"\tsub:\t" + rsSub.getString("name") +
				(rsSub.getBoolean("exc") ? "\tEXC\t" + rsSub.getString("exc_info") : "") +
				"\n"
			);
			
			ResultSet rsTrial = dbi.execQry("SELECT t.id, t.name, t.exc, COALESCE(t.exc_info, '') AS exc_info FROM " + db + ".trial t WHERE t.sub_id = " + subId + " ORDER BY name");
			while (rsTrial.next()) {
				int trialId = rsTrial.getInt("id");
				res.append(
					"\t\ttrial:\t" + rsTrial.getString("name") +
					(rsTrial.getBoolean("exc") ? "\tEXC\t" + rsTrial.getString("exc_info") : "") +
					"\n"
				);
				
				ResultSet rsStimSeq = dbi.execQry("SELECT ss.id, s.name, ss.exc, COALESCE(ss.exc_info, '') AS exc_info FROM " + db + ".stim_seq ss INNER JOIN " + db + ".stim s ON ss.stim_id = s.id WHERE trial_id = " + trialId + " ORDER BY t0");
				while (rsStimSeq.next()) {
					int stimSeqId = rsStimSeq.getInt("id");
					res.append(
						"\t\t\ts-seq:\t" + rsStimSeq.getString("name") +
						(rsStimSeq.getBoolean("exc") ? "\tEXC\t" + rsStimSeq.getString("exc_info"): "") +
						"\n"
					);
					
					ResultSet rsEvt = dbi.execQry("SELECT el.name, e.exc, COALESCE(e.exc_info, '') AS exc_info FROM " + db + ".evt e INNER JOIN " + db + ".evt_lst el ON e.evt_lst_id = el.id WHERE stim_seq_id = " + stimSeqId + " ORDER BY e.t_prompt");
					while (rsEvt.next()) {
						res.append(
							"\t\t\t\tevt:\t" + rsEvt.getString("name") +
							(rsEvt.getBoolean("exc") ? "\tEXC\t" + rsEvt.getString("exc_info"): "") +
							"\n"
						);
					}
					rsEvt.close();
				}
				rsStimSeq.close();
			}
			rsTrial.close();
		}
		rsSub.close();
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * If 'expId' is not 'null', that experiment will be returned in its entirety.
	 */
	@SuppressWarnings("deprecation")
	public static Result getLst(DBI dbi, String username, Integer prjId, Integer expId)  throws SQLException, UnsupportedEncodingException, InstantiationException, IllegalAccessException, ClassNotFoundException, RserveException, REXPMismatchException {
		String db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		
		ResultSet rs = dbi.execQry(
			"SELECT id, name, created, memo, LENGTH(imp_rep) AS imp_rep_len, imp_ts, linked, LENGTH(note) AS note_len, COALESCE(sub_cnt, 0) AS sub_cnt, COALESCE(trial_cnt, 0) AS trial_cnt, COALESCE(stim_cnt, 0) AS stim_cnt, COALESCE(roi_cnt, 0) AS roi_cnt " +
			"FROM " + db + ".exp e " +
			"LEFT JOIN (SELECT exp_id, COUNT(*) AS sub_cnt FROM " + db + ".sub GROUP BY exp_id) tmp01 ON tmp01.exp_id = e.id " +
			"LEFT JOIN (SELECT exp_id, COUNT(*) AS trial_cnt FROM " + db + ".trial INNER JOIN " + db + ".sub ON trial.sub_id = sub.id GROUP BY sub.exp_id) tmp02 ON tmp02.exp_id = e.id " +
			"LEFT JOIN (SELECT exp_id, COUNT(*) AS stim_cnt FROM " + db + ".stim GROUP BY exp_id) tmp03 ON tmp03.exp_id = e.id " +
			"LEFT JOIN (SELECT exp_id, COUNT(*) AS roi_cnt FROM " + db + ".roi INNER JOIN " + db + ".stim ON roi.stim_id = stim.id GROUP BY stim.exp_id) tmp04 ON tmp04.exp_id = e.id " +
			(prjId == null ? "" : "WHERE prj_id = " + prjId.intValue())
		);
		
		StringBuffer res = new StringBuffer("exp:[");
		while (rs.next()) {
			boolean impRep = (rs.getInt("imp_rep_len") > 0);
			int id = rs.getInt("id");
			String memo = rs.getString("memo");
			
			if (expId != null && expId.intValue() == id) res.append(get(dbi, username, id));
			else {
				res.append(
					"{" +
					"loaded:false," +
					"id:" + id + "," +
					"name:" + JSI.str2js(rs.getString("name")) + "," +
					(memo != null && memo.length() > 0 ? "memo:" + JSI.str2js(memo) + "," : "") +
					"created:" + (rs.getTimestamp("created").getYear() == 0 ? "null" : "new Date(" + rs.getTimestamp("created").getTime() + ")") + "," +
					"impDate:" + (!impRep ? "null" : "new Date(" + rs.getTimestamp("imp_ts").getTime() + ")") + "," +
					"impRep:null," +
					"linked:" + rs.getBoolean("linked") + "," + 
					"noteLen:" + rs.getInt("note_len") + "," +
					"subCnt:" + rs.getInt("sub_cnt") + "," +
					"trialCnt:" + rs.getInt("trial_cnt") + "," +
					"stimCnt:" + rs.getInt("stim_cnt") + "," +
					"roiCnt:" + rs.getInt("roi_cnt") +
					"}"
				);
			}
			if (!rs.isLast()) res.append(",");
		}
		rs.close();
		res.append("]");
		
		return new Result(true, res);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static Result getStats(DBI dbi, String username, int expId)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		util.Timer timer = new util.Timer();
		
		return new Result(
			true,
			new StringBuffer(
				"stats:" + JSI.str2js(
					getStats_general(dbi, username, expId) + "\n" +
					getStats_evt    (dbi, username, expId) + "\n" +
					getStats_sub    (dbi, username, expId) + "\n" +
					"LEGEND\n" +
					"\tsseq - stimulus sequence item (i.e., the presentation of a certain stimulus in a certain trial of a certain subject)\n" +
					"\ta    - all (e.g., all pages inspected by a subject, irrespective of the fact that the same page could have been viewed more than once)\n" +
					"\tu    - unique (e.g., unique pages inspected by a subject, i.e., multiple views of the same page are counted as one)\n" +
					"\tf    - ROIs fixated\n\n" +
					"EXTRA INFO\n" +
					"\tCalculation time: " + timer.elapsedHuman() + "\n"
				)
			)
		);
	}
	
	
	// ----^----
	private static StringBuffer getStats_evt(DBI dbi, String username, int expId)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		String db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		StringBuffer res = new StringBuffer("EVENTS\n");
		
		// (1) Overall:
		res.append(
			"\tOVERALL\n" +
			"\t\tevt\ttot\texc\tret\t%-exc\n" +
			"\t\t\n"
		);
		
		StringBuilder sbEvtNames01 = new StringBuilder();
		StringBuilder sbEvtNames02 = new StringBuilder();
		ResultSet rsEvt = dbi.execQry(
			"SELECT name, evt_tot, evt_exc " +
			"FROM " + db + ".evt_lst el " +
			"LEFT JOIN (SELECT el.id AS el_id, COUNT(*) AS evt_tot FROM " + db + ".evt_lst el LEFT JOIN " + db + ".evt e ON e.evt_lst_id = el.id WHERE el.exp_id = " + expId + "AND NOT e.exc GROUP BY el.id) tmp01 ON tmp01.el_id = el.id " +
			"LEFT JOIN (SELECT el.id AS el_id, COUNT(*) AS evt_exc FROM " + db + ".evt_lst el LEFT JOIN " + db + ".evt e ON e.evt_lst_id = el.id LEFT JOIN " + db + ".stim s01 ON e.stim_id = s01.id LEFT JOIN " + db + ".trial t ON e.trial_id = t.id LEFT JOIN " + db + ".sub s02 ON t.sub_id = s02.id LEFT JOIN " + db + ".stim_seq ss ON e.stim_seq_id = ss.id WHERE el.exp_id = " + expId + "AND (e.exc OR s01.exc OR t.exc OR s02.exc OR ss.exc) GROUP BY el.id) tmp02 ON tmp02.el_id = el.id " +
			"WHERE el.exp_id = " + expId + " ORDER BY name"
		);
		while (rsEvt.next()) {
			String name = rsEvt.getString("name");
			res.append("\t\t" + name + "\t" + getStats_getExc(rsEvt, "evt") + "\n");
			
			sbEvtNames01.append("\t" + name + "\t\t\t");
			sbEvtNames02.append("\ttot\texc\tret\t%-exc");
		}
		rsEvt.close();
		res.append("\t\n");
		
		// (2) By subject:
		res.append(
			"\tBY SUBJECT\n" +
			"\t\tsub\t\tevt\n" +
			"\t\t\t" + sbEvtNames01.toString() + "\n" +
			"\t\tname\tis-exc" + sbEvtNames02.toString() + "\n" +
			"\t\t\n"
		);
		
		ResultSet rsSub = dbi.execQry("SELECT id, name, exc FROM " + db + ".sub WHERE exp_id = " + expId + " ORDER BY name");
		while (rsSub.next()) {
			int subId = rsSub.getInt("id");
			res.append("\t\t" + rsSub.getString("name") + "\t" + (rsSub.getBoolean("exc") ? "1" : "0"));
			
			ResultSet rsSubEvt = dbi.execQry(
				"SELECT el.name, SUM(COALESCE(evt_tot,0)) AS evt_tot, SUM(COALESCE(evt_exc,0)) AS evt_exc " +
				"FROM " + db + ".evt_lst el " +
				"LEFT JOIN (SELECT evt_lst_id, COUNT(*) AS evt_tot FROM " + db + ".evt e INNER JOIN " + db + ".trial t ON e.trial_id = t.id INNER JOIN " + db + ".sub s01 ON t.sub_id = s01.id WHERE s01.exp_id = " + expId + " AND s01.id = " + subId + " GROUP BY evt_lst_id) tmp01 ON tmp01.evt_lst_id = el.id " +
				"LEFT JOIN (SELECT evt_lst_id, COUNT(*) AS evt_exc FROM " + db + ".evt e INNER JOIN " + db + ".trial t ON e.trial_id = t.id INNER JOIN " + db + ".sub s01 ON t.sub_id = s01.id INNER JOIN " + db + ".stim s02 ON e.stim_id = s02.id INNER JOIN " + db + ".stim_seq ss ON e.stim_seq_id = ss.id WHERE s01.exp_id = " + expId + " AND s01.id = " + subId + "AND (e.exc OR s01.exc OR t.exc OR s02.exc OR ss.exc) GROUP BY evt_lst_id) tmp02 ON tmp02.evt_lst_id = el.id " +
				"GROUP BY el.name ORDER BY el.name"
			);
			while (rsSubEvt.next()) {
				res.append("\t" + getStats_getExc(rsSubEvt, "evt"));
			}
			rsSubEvt.close();
			
			res.append("\n");
		}
		rsSub.close();
		
		return res;
	}
	
	
	// ----^----
	private static StringBuffer getStats_general(DBI dbi, String username, int expId)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		String db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		StringBuffer res = new StringBuffer("GENERAL\n");
		
		// (1) Overall:
		ResultSet rsExp = dbi.execQry(
			"SELECT " +
			
			"(SELECT COUNT(*) FROM "                             + db + ".stim s WHERE s.exp_id = "                     + expId + " AND s.exc) AS stim_exc, " +
			"(SELECT COUNT(*) FROM "                             + db + ".stim s WHERE s.exp_id = "                     + expId + ") AS stim_tot, " +
			"(SELECT COUNT(*) FROM " + db + ".roi r INNER JOIN " + db + ".stim s ON r.stim_id = s.id WHERE s.exp_id = " + expId + ") AS roi_tot, " +
			"(SELECT COUNT(*) FROM " + db + ".roi r INNER JOIN " + db + ".stim s ON r.stim_id = s.id WHERE s.exp_id = " + expId + " AND s.exc) AS roi_exc, " +
			
			"(SELECT COUNT(*) FROM "                                                                                                                                                  + db + ".sub s WHERE s.exp_id = "                    + expId + ") AS sub_tot, " +
			"(SELECT COUNT(*) FROM "                                                                                                                                                  + db + ".sub s WHERE s.exp_id = "                    + expId + " AND s.exc) AS sub_exc, " +
			"(SELECT COUNT(*) FROM "                                   + db + ".trial t INNER JOIN "                                                                                  + db + ".sub s ON t.sub_id = s.id WHERE s.exp_id = " + expId + ") AS trial_tot, " +
			"(SELECT COUNT(*) FROM "                                   + db + ".trial t INNER JOIN "                                                                                  + db + ".sub s ON t.sub_id = s.id WHERE s.exp_id = " + expId + " AND (s.exc OR t.exc)) AS trial_exc, " +
			"(SELECT COUNT(*) FROM " + db + ".stim_seq ss INNER JOIN " + db + ".trial t ON ss.trial_id = t.id INNER JOIN "                                                            + db + ".sub s ON t.sub_id = s.id WHERE exp_id = "   + expId + ") AS stim_seq_tot, " +
			"(SELECT COUNT(*) FROM " + db + ".stim_seq ss INNER JOIN " + db + ".trial t ON ss.trial_id = t.id INNER JOIN "                                                            + db + ".sub s ON t.sub_id = s.id WHERE exp_id = "   + expId + " AND (s.exc OR t.exc OR ss.exc)) AS stim_seq_exc, " +
			"(SELECT COUNT(*) FROM " + db + ".evt e       INNER JOIN " + db + ".stim_seq ss ON e.stim_seq_id = ss.id INNER JOIN " + db + ".trial t ON ss.trial_id = t.id INNER JOIN " + db + ".sub s ON t.sub_id = s.id WHERE exp_id = "   + expId + ") AS evt_tot, " +
			"(SELECT COUNT(*) FROM " + db + ".evt e       INNER JOIN " + db + ".stim_seq ss ON e.stim_seq_id = ss.id INNER JOIN " + db + ".trial t ON ss.trial_id = t.id INNER JOIN " + db + ".sub s ON t.sub_id = s.id WHERE exp_id = "   + expId + " AND (s.exc OR t.exc OR ss.exc OR e.exc)) AS evt_exc, " +
			
			"(SELECT COUNT(*) FROM " + db + ".fix f       INNER JOIN " + db + ".stim_seq ss ON f.stim_seq_id = ss.id INNER JOIN " + db + ".trial t ON ss.trial_id = t.id INNER JOIN " + db + ".sub s ON t.sub_id = s.id WHERE exp_id = "   + expId + ") AS fix_tot, " +
			"(SELECT COUNT(*) FROM " + db + ".fix f       INNER JOIN " + db + ".stim_seq ss ON f.stim_seq_id = ss.id INNER JOIN " + db + ".trial t ON ss.trial_id = t.id INNER JOIN " + db + ".sub s ON t.sub_id = s.id WHERE exp_id = "   + expId + " AND (s.exc OR t.exc OR ss.exc OR f.exc)) AS fix_exc, " +
			"(SELECT COUNT(*) FROM " + db + ".blink b     INNER JOIN " + db + ".stim_seq ss ON b.stim_seq_id = ss.id INNER JOIN " + db + ".trial t ON ss.trial_id = t.id INNER JOIN " + db + ".sub s ON t.sub_id = s.id WHERE exp_id = "   + expId + ") AS blink_tot, " +
			"(SELECT COUNT(*) FROM " + db + ".blink b     INNER JOIN " + db + ".stim_seq ss ON b.stim_seq_id = ss.id INNER JOIN " + db + ".trial t ON ss.trial_id = t.id INNER JOIN " + db + ".sub s ON t.sub_id = s.id WHERE exp_id = "   + expId + " AND (s.exc OR t.exc OR ss.exc OR b.exc)) AS blink_exc"
		);
		rsExp.next();
		res.append(
			"\tOVERALL\n" +
			"\t\titem\ttot\texc\tret\t%-exc\n" +
			"\t\t\n" +
			"\t\tstim\t"  + getStats_getExc(rsExp, "stim")     + "\n" +
			"\t\troi\t"   + getStats_getExc(rsExp, "roi")      + "\n" +
			"\t\tsub\t"   + getStats_getExc(rsExp, "sub")      + "\n" +
			"\t\ttrial\t" + getStats_getExc(rsExp, "trial")    + "\n" +
			"\t\tsseq\t"  + getStats_getExc(rsExp, "stim_seq") + "\n" +
			"\t\tevt\t"   + getStats_getExc(rsExp, "evt")      + "\n" +
			"\t\tfix\t"   + getStats_getExc(rsExp, "fix")      + "\n" +
			"\t\tblink\t" + getStats_getExc(rsExp, "blink")    + "\n" +
			"\t\n"
		);
		rsExp.close();
		
		// (2) By subject:
		res.append(
			"\tBY SUBJECT\n" +
			"\t\tsub\t\ttrial\t\t\t\tsseq-u\t\t\t\troi-u\t\t\t\tevt\t\t\t\tfix\t\t\t\tblink\t\t\t\troi-fu\t\t\t\troi-fa\t\t\t\tt\t\t\t\t\n" +
			"\t\tname\tis-exc\ttot\texc\tret\t%-exc\ttot\texc\tret\t%-exc\ttot\texc\tret\t%-exc\ttot\texc\tret\t%-exc\ttot\texc\tret\t%-exc\ttot\texc\tret\t%-exc\ttot\texc\tret\t%-exc\ttot\texc\tret\t%-exc\ttot\texc\tret\t%-exc\n" +
			"\t\t\n"
		);
		ResultSet rsSub = dbi.execQry("SELECT id, name FROM " + db + ".sub WHERE exp_id = " + expId + " ORDER BY name");
		while (rsSub.next()) {
			int subId = rsSub.getInt("id");
			ResultSet rsSubInfo = dbi.execQry(
				"SELECT exc AS is_sub_exc, " +
				
				"(SELECT COUNT(*) FROM "                                                                                                                                                          + db + ".trial t INNER JOIN "                       + db + ".sub s ON t.sub_id = s.id WHERE s.id = " + subId + ") AS trial_tot, " +
				"(SELECT COUNT(*) FROM "                                                                                                                                                          + db + ".trial t INNER JOIN "                       + db + ".sub s ON t.sub_id = s.id WHERE s.id = " + subId + " AND (s.exc OR t.exc)) AS trial_exc, " +
				"(SELECT COUNT(*) FROM (SELECT DISTINCT ss.id FROM "                                                                   + db + ".stim_seq ss INNER JOIN "                          + db + ".trial t ON ss.trial_id = t.id INNER JOIN " + db + ".sub s ON t.sub_id = s.id WHERE s.id = " + subId + ") tmp) AS stim_seq_uniq_tot, " +
				"(SELECT COUNT(*) FROM (SELECT DISTINCT ss.id FROM "                                                                   + db + ".stim_seq ss INNER JOIN "                          + db + ".trial t ON ss.trial_id = t.id INNER JOIN " + db + ".sub s ON t.sub_id = s.id WHERE s.id = " + subId + " AND (s.exc OR t.exc OR ss.exc)) tmp) AS stim_seq_uniq_exc, " +
				"(SELECT COUNT(*) FROM (SELECT DISTINCT r.id FROM " + db + ".roi r WHERE stim_id IN (SELECT DISTINCT ss.stim_id FROM " + db + ".stim_seq ss INNER JOIN "                          + db + ".trial t ON ss.trial_id = t.id INNER JOIN " + db + ".sub s ON t.sub_id = s.id WHERE s.id = " + subId + ")) tmp_b) AS roi_uniq_tot, " +
				"(SELECT COUNT(*) FROM (SELECT DISTINCT r.id FROM " + db + ".roi r WHERE stim_id IN (SELECT DISTINCT ss.stim_id FROM " + db + ".stim_seq ss INNER JOIN "                          + db + ".trial t ON ss.trial_id = t.id INNER JOIN " + db + ".sub s ON t.sub_id = s.id WHERE s.id = " + subId + " AND (s.exc OR t.exc OR ss.exc))) tmp_b) AS roi_uniq_exc, " +
				"(SELECT COUNT(*) FROM " + db + ".evt e   INNER JOIN "                                                                 + db + ".stim_seq ss ON e.stim_seq_id = ss.id INNER JOIN " + db + ".trial t ON ss.trial_id = t.id INNER JOIN " + db + ".sub s ON t.sub_id = s.id WHERE s.id = " + subId + ") AS evt_tot, " +
				"(SELECT COUNT(*) FROM " + db + ".evt e   INNER JOIN "                                                                 + db + ".stim_seq ss ON e.stim_seq_id = ss.id INNER JOIN " + db + ".trial t ON ss.trial_id = t.id INNER JOIN " + db + ".sub s ON t.sub_id = s.id WHERE s.id = " + subId + " AND (s.exc OR t.exc OR ss.exc OR e.exc)) AS evt_exc, " +
				
				"(SELECT COUNT(*) FROM " + db + ".fix f   INNER JOIN " + db + ".stim_seq ss ON f.stim_seq_id = ss.id INNER JOIN " + db + ".trial t ON ss.trial_id = t.id INNER JOIN " + db + ".sub s ON t.sub_id = s.id WHERE s.id = " + subId + ") AS fix_tot, " +
				"(SELECT COUNT(*) FROM " + db + ".fix f   INNER JOIN " + db + ".stim_seq ss ON f.stim_seq_id = ss.id INNER JOIN " + db + ".trial t ON ss.trial_id = t.id INNER JOIN " + db + ".sub s ON t.sub_id = s.id WHERE s.id = " + subId + " AND (s.exc OR t.exc OR ss.exc OR f.exc)) AS fix_exc, " +
				"(SELECT COUNT(*) FROM " + db + ".blink b INNER JOIN " + db + ".stim_seq ss ON b.stim_seq_id = ss.id INNER JOIN " + db + ".trial t ON ss.trial_id = t.id INNER JOIN " + db + ".sub s ON t.sub_id = s.id WHERE s.id = " + subId + ") AS blink_tot, " +
				"(SELECT COUNT(*) FROM " + db + ".blink b INNER JOIN " + db + ".stim_seq ss ON b.stim_seq_id = ss.id INNER JOIN " + db + ".trial t ON ss.trial_id = t.id INNER JOIN " + db + ".sub s ON t.sub_id = s.id WHERE s.id = " + subId + " AND (s.exc OR t.exc OR ss.exc OR b.exc)) AS blink_exc, " +
				
				"(SELECT COUNT(*) FROM (SELECT DISTINCT r.id FROM " + db + ".roi r INNER JOIN " + db + ".fix f ON f.roi_id_adj = r.id INNER JOIN " + db + ".stim_seq ss ON f.stim_seq_id = ss.id INNER JOIN " + db + ".trial t ON ss.trial_id = t.id INNER JOIN " + db + ".sub s ON t.sub_id = s.id WHERE f.sacc_i_roi_adj IS NOT NULL AND f.sacc_i_roi_adj <> 0 AND s.id = " + subId + ") tmp) AS roi_fix_uniq_tot, " +
				"(SELECT COUNT(*) FROM (SELECT DISTINCT r.id FROM " + db + ".roi r INNER JOIN " + db + ".fix f ON f.roi_id_adj = r.id INNER JOIN " + db + ".stim_seq ss ON f.stim_seq_id = ss.id INNER JOIN " + db + ".trial t ON ss.trial_id = t.id INNER JOIN " + db + ".sub s ON t.sub_id = s.id WHERE f.sacc_i_roi_adj IS NOT NULL AND f.sacc_i_roi_adj <> 0 AND s.id = " + subId + " AND (s.exc OR t.exc OR ss.exc)) tmp) AS roi_fix_uniq_exc, " +
				"(SELECT COUNT(*) FROM (SELECT          r.id FROM " + db + ".roi r INNER JOIN " + db + ".fix f ON f.roi_id_adj = r.id INNER JOIN " + db + ".stim_seq ss ON f.stim_seq_id = ss.id INNER JOIN " + db + ".trial t ON ss.trial_id = t.id INNER JOIN " + db + ".sub s ON t.sub_id = s.id WHERE f.sacc_i_roi_adj IS NOT NULL AND f.sacc_i_roi_adj <> 0 AND s.id = " + subId + ") tmp) AS roi_fix_all_tot, " +
				"(SELECT COUNT(*) FROM (SELECT          r.id FROM " + db + ".roi r INNER JOIN " + db + ".fix f ON f.roi_id_adj = r.id INNER JOIN " + db + ".stim_seq ss ON f.stim_seq_id = ss.id INNER JOIN " + db + ".trial t ON ss.trial_id = t.id INNER JOIN " + db + ".sub s ON t.sub_id = s.id WHERE f.sacc_i_roi_adj IS NOT NULL AND f.sacc_i_roi_adj <> 0 AND s.id = " + subId + " AND (s.exc OR t.exc OR ss.exc)) tmp) AS roi_fix_all_exc, " +
				
				"(SELECT SUM(t1-t0) FROM "                                                                                                         + db + ".stim_seq ss INNER JOIN " +                          db + ".trial t ON ss.trial_id = t.id INNER JOIN " + db + ".sub s ON t.sub_id = s.id WHERE s.id = " + subId + ") AS t_tot, " +
				"(SELECT SUM(t1-t0) FROM "                                                                                                         + db + ".stim_seq ss INNER JOIN " +                          db + ".trial t ON ss.trial_id = t.id INNER JOIN " + db + ".sub s ON t.sub_id = s.id WHERE s.id = " + subId + " AND (s.exc OR t.exc OR ss.exc)) AS t_exc " +
				
				"FROM " + db + ".sub s WHERE s.id = " + subId
			);
			rsSubInfo.next();
			res.append(
				"\t\t" + rsSub.getString("name")                 + "\t" + 
				(rsSubInfo.getBoolean("is_sub_exc") ? "1" : "0") + "\t" +
				getStats_getExc(rsSubInfo, "trial")              + "\t" +
				getStats_getExc(rsSubInfo, "stim_seq_uniq")      + "\t" +
				getStats_getExc(rsSubInfo, "roi_uniq")           + "\t" +
				getStats_getExc(rsSubInfo, "evt")                + "\t" +
				getStats_getExc(rsSubInfo, "fix")                + "\t" +
				getStats_getExc(rsSubInfo, "blink")              + "\t" +
				getStats_getExc(rsSubInfo, "roi_fix_uniq")       + "\t" +
				getStats_getExc(rsSubInfo, "roi_fix_all")        + "\t" +
				getStats_getExc(rsSubInfo, "t")                  + "\n"
			);
			rsSubInfo.close();
		}
		rsSub.close();
		
		return res;
	}
	
	
	// ----^----
	private static StringBuffer getStats_sub(DBI dbi, String username, int expId)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		String db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		StringBuffer res = new StringBuffer("SUBJECTS\n");
		
		res.append(
			"\tsub\twpm\n" +
			"\t\t\n"
		);
		
		ResultSet rs = dbi.execQry(
			"SELECT sub_id, sub_name, (CASE WHEN wpm < 1000 THEN wpm ELSE -1 END) AS wpm FROM " +
			"( " +
			"SELECT sub_id, sub_name, ROUND(AVG(roi_cnt / t)) AS wpm FROM " +
			"( " +
			"SELECT s02.id AS sub_id, s02.name AS sub_name, s01.id AS stim_id, s01.name AS stim_name, r.cnt AS roi_cnt, (SUM(ss.t1-ss.t0) / (1000*60)) AS t " +
			"FROM " + db + ".stim_seq ss " +
			"INNER JOIN " + db + ".stim s01 ON ss.stim_id = s01.id " +
			"INNER JOIN " + db + ".trial t ON ss.trial_id = t.id " +
			"INNER JOIN " + db + ".sub s02 ON s02.id = t.sub_id " +
			"INNER JOIN (SELECT stim_id, COUNT(*) AS cnt FROM " + db + ".roi GROUP BY stim_id) r ON r.stim_id = s01.id " +
			"WHERE s02.exp_id = " + expId + " AND s01.type = 't' AND r.cnt > 0 " +
			"GROUP BY s01.id, s01.name, s02.id, s02.name, r.cnt " +
			") a " +
			"GROUP BY sub_id, sub_name " +
			") b " +
			"ORDER BY sub_name;"
		);
		while (rs.next()) {
			res.append("\t" + rs.getString("sub_name") + "\t" + rs.getInt("wpm") + "\n");
		}
		rs.close();
		
		return res;
	}
	
	
	// ----^----
	private static String getStats_getExc(ResultSet rs, String name)  throws SQLException {
		int tot = rs.getInt(name + "_tot");
		int exc = rs.getInt(name + "_exc");
		double excp = $.trunc((double) exc / (double) tot * 100d, 2);  // % excluded
		return tot + "\t" + exc + "\t" + (tot-exc) + "\t" + excp;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Get a list of ngrams that can be used by the Ngram app to retrieve word predictability on a one-by-one basis.
	 * 
	 * localhost:8080/pegasus/do?app=gaze&cmd=exp-gen-ngram-in-files&username=tomek&exp-id=92  [Zoning out: Ind. diff.]
	 */
	public static Result genNgramInFiles(DBI dbi, String username, int expId)  throws SQLException, IOException {
		String db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		
		NgramMan ngramMan = new NgramMan();
		
		//ResultSet rsStim = dbi.execQry("SELECT id FROM " + db + ".stim WHERE exp_id = " + expId + " AND name = 'c01p01' ORDER BY id LIMIT 1");
		ResultSet rsStim = dbi.execQry("SELECT id FROM " + db + ".stim WHERE exp_id = " + expId + " ORDER BY id");
		while (rsStim.next()) {
			app.gaze.ngram.Stim stim = new app.gaze.ngram.Stim(dbi, username, rsStim.getInt("id"));
			
			stim.getNgramLst(ngramMan, 3);
			stim.getNgramLst(ngramMan, 4);
			stim.getNgramLst(ngramMan, 5);
		}
		rsStim.close();
		
		int ngramCnt = ngramMan.genInFiles();
		
		return new Result(true, "ngramCnt:" + ngramCnt);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Retrieve and set word predictability for all stimuli in the specified experiment.
	 */
	public static Result setWordPred(DBI dbi, String username, int expId)  throws MalformedURLException, IOException, SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		//if (!HTTPI.isWebServiceOnline(Do.URL_NGRAM_GOOGLE, Do.NGRAM_APP_TIMEOUT)) return new Result(false, "msg:\"Error: The Ngram Web service cannot be reached.\"");
		
		String db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		
		StringBuilder res = new StringBuilder();
		
		ResultSet rsStim = dbi.execQry("SELECT id FROM " + db + ".stim WHERE exp_id = " + expId + " AND name = 'c01p01' ORDER BY id LIMIT 1");
		while (rsStim.next()) {
			app.gaze.ngram.Stim stim = new app.gaze.ngram.Stim(dbi, username, rsStim.getInt("id"));
			res.append(stim.retrieveWordPred(username, true, true));
		}
		rsStim.close();
		
		return new Result(true, "rep:" + JSI.str2js(res.toString()));
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Link EMs to ROIs.
	 */
	public static Result lnk(DBI dbi, String username, int expId)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException, UnsupportedEncodingException, RserveException, REXPMismatchException {
		String db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		
		util.Timer timer = new util.Timer();
		
		int trialCnt = 0;
		int fixCnt = 0;
		int blinkCnt = 0;
		int lnkFixCnt = 0;
		int lnkFixCntAdj = 0;
		int lnkBlinkCnt = 0;
		
		DBI dbiX = new DBI();
		Statement stX = dbi.transBegin();
		stX.execute("SET search_path TO " + db);
		
		//PreparedStatement psXFix = dbiX.getConn().prepareStatement("UPDATE " + db + ".fix SET roi_id = ?, roi_id_adj = ?, sacc = ?, sacc_adj = ? WHERE id = ?");
		
		try {
			// (1) Load stimuli and ROIs:
			Hashtable<String,Stim> stim = new Hashtable<String,Stim>();
			
			ResultSet rsStim = dbi.execQry(
				"SELECT DISTINCT s.id, s.name, s.txt, s.res_h, s.res_v " +
				"FROM " + db + ".stim s " +
				"INNER JOIN " + db + ".stim_seq ss ON ss.stim_id = s.id " +
				"INNER JOIN " + db + ".trial t ON ss.trial_id = t.id " +
				//"WHERE s.exp_id = " + expId + " AND t.linked = FALSE AND s.type = 't'"
				"WHERE s.exp_id = " + expId + " AND s.type = 't'"
			);
			while (rsStim.next()) {
				Stim s = new Stim(rsStim.getInt("id"), rsStim.getString("name"), rsStim.getString("txt"), rsStim.getInt("res_h"), rsStim.getInt("res_v"));
				s.load(dbiX, db);
				stim.put("" + s.id, s);  // use String as a key to avoid conversions to Integer from ResultSet
			}
			rsStim.close();
			
			// (2) Process all trials:
			//ResultSet rsTrial = dbi.execQry("SELECT t.id, t.t_buff FROM " + db + ".trial t INNER JOIN " + db + ".sub s ON t.sub_id = s.id WHERE s.exp_id = " + expId + " AND t.linked = FALSE ORDER BY id");
			ResultSet rsTrial = dbi.execQry("SELECT t.id, t.t_buff FROM " + db + ".trial t INNER JOIN " + db + ".sub s ON t.sub_id = s.id WHERE s.exp_id = " + expId + " ORDER BY id");
			
			while (rsTrial.next()) {
				trialCnt++;
				int trialId = rsTrial.getInt("id");
				int tBuff = rsTrial.getInt("t_buff");
				
				// (2.1) Load stimuli sequence:
				List<app.gaze.dstruct.StimSeq> stimSeq = new ArrayList<app.gaze.dstruct.StimSeq>();
				
				ResultSet rsStimSeq = dbi.execQry("SELECT id, stim_id, t0, t1 FROM " + db + ".stim_seq WHERE trial_id = " + trialId + " ORDER BY t0");
				while (rsStimSeq.next()) {
					stimSeq.add(new app.gaze.dstruct.StimSeq(rsStimSeq.getInt("id"), stim.get(rsStimSeq.getString("stim_id")), rsStimSeq.getLong("t0"), rsStimSeq.getLong("t1")));
				}
				rsStimSeq.close();
				
				if (stimSeq.size() == 0) continue;
				
				// (2.2) Link fixations:
				// part of the query below should be a part of import routine
				stX.executeUpdate("UPDATE fix SET roi_id = null, roi_id_adj = null, sacc_i_x = null, sacc_i_y = null, sacc_o_x = null, sacc_o_y = null, sacc_i_x_adj = null, sacc_i_y_adj = null, sacc_o_x_adj = null, sacc_o_y_adj = null, sacc_i_roi = null, sacc_o_roi = null, sacc_i_roi_adj = null, sacc_o_roi_adj = null, sacc_i_let = null, sacc_o_let = null, sacc_i_let_adj = null, sacc_o_let_adj = null WHERE trial_id = " + trialId);
				
				int currStimIdx = 0, currStimFixCntROI = 0, currStimFixCntROIAdj = 0;
				app.gaze.dstruct.StimSeq ss = stimSeq.get(0);
				
				Fix prevF   = null;
				ROI currROI = null, currROIAdj = null, prevROI = null, prevROIAdj = null;
				
				ResultSet rsFix = dbi.execQry("SELECT id, t, x, y, x_adj, y_adj, COALESCE(fix_ln_id, " + DBI.ID_NONE + ") AS fix_ln_id FROM " + db + ".fix WHERE trial_id = " + trialId + " ORDER BY num");
				while (rsFix.next()) {
					fixCnt++;
					
					// (2.2.1) Retrieve the fixation details:
					Float x    = rsFix.getFloat("x");      if (rsFix.wasNull()) x    = null;
					Float y    = rsFix.getFloat("y");      if (rsFix.wasNull()) y    = null;
					Float xAdj = rsFix.getFloat("x_adj");  if (rsFix.wasNull()) xAdj = null;
					Float yAdj = rsFix.getFloat("y_adj");  if (rsFix.wasNull()) yAdj = null;
					
					Fix currF = new Fix(rsFix.getInt("id"), -1l, rsFix.getLong("t"), -1, x, y, xAdj, yAdj);
					
					// (2.2.2) Check timing issues:
					if (currF.tTrial < stimSeq.get(0).t0) continue;  // fixation from before the very first stimulus sequence entry
					
					if (currF.tTrial >= ss.t1) {  // updating the current stimulus and advancing to the next one
						stX.executeUpdate("UPDATE stim_seq SET fix_cnt_roi = " + currStimFixCntROI + ", fix_cnt_roi_adj = " + currStimFixCntROIAdj + " WHERE id = " + ss.id);
						ss = stimSeq.get(++currStimIdx);
						currStimFixCntROI    = 0;
						currStimFixCntROIAdj = 0;
					}
					
					if (currF.tTrial - ss.t0 <= tBuff) continue;  // fixation in the "disregard" buffer (stimulus-end side)
					if (currF.tTrial + tBuff >= ss.t1) continue;  // fixation in the "disregard" buffer (stimulus-start side)
					
					if (ss.stim == null) continue;
					
					// (2.2.3) Link:
					currROI    = ss.stim.getROIByCoords(currF.x,    currF.y);
					if (currF.xAdj != null && currF.yAdj != null) currROIAdj = ss.stim.getROIByCoords(currF.xAdj, currF.yAdj);
					
					if (currROI != null || currROIAdj != null) {
						currF.saccInROI    = (currROI    != null && prevROI    != null ? (currROI.num    - prevROI.num)    : null);
						currF.saccInROIAdj = (currROIAdj != null && prevROIAdj != null ? (currROIAdj.num - prevROIAdj.num) : null);
						
						currF.saccInLet    = ss.stim.getSaccLet(prevROI,    currROI);
						currF.saccInLetAdj = ss.stim.getSaccLet(prevROIAdj, currROIAdj);
						
						// TODO: Use prepared statement (see below for my previous attempts of making it work with PostgreSQL):
						stX.executeUpdate(
							"UPDATE " + db + ".fix SET " +
							"roi_id = "         + (currROI    != null                           ? currROI.id     : "null") + ", " +
							"roi_id_adj = "     + (currROIAdj != null                           ? currROIAdj.id  : "null") + ", " +
							"let = "            + (currROI    != null && currROI.let    != null ? currROI.let    : "null") + ", " +
							"let_adj = "        + (currROIAdj != null && currROIAdj.let != null ? currROIAdj.let : "null") + ", " +
							"sacc_i_roi = "     + currF.saccInROI    + ", " +
							"sacc_i_roi_adj = " + currF.saccInROIAdj + ", " +
							"sacc_i_let = "     + currF.saccInLet    + ", " +
							"sacc_i_let_adj = " + currF.saccInLetAdj + " "  +
							"WHERE id = "       + currF.id
						);
						
						/*
						// Prepared statements -- Attempt 1:
						psXFix.setObject(1, (roi[0] != DBI.ID_NONE ? roi[0] : null));
						psXFix.setObject(2, (roiAdj[0] != DBI.ID_NONE ? roiAdj[0] : null));
						psXFix.setObject(3, (prevROINum != null ? new Integer((roi[1] - prevROINum.intValue())) : null));
						psXFix.setObject(4, (prevROINumAdj != null ? new Integer((roiAdj[1] - prevROINumAdj.intValue())) : null));
						psXFix.setInt(5, rsFix.getInt("id"));
						psXFix.executeUpdate();
						*/
						
						/*
						// Prepared statements -- Attempt 2:
						psXFix.setObject(1, (roi[0] != DBI.ID_NONE ? roi[0] : null), Types.INTEGER);
						psXFix.setObject(2, (roiAdj[0] != DBI.ID_NONE ? roiAdj[0] : null), Types.INTEGER);
						psXFix.setObject(3, (prevROINum != null ? new Integer((roi[1] - prevROINum.intValue())) : null), Types.INTEGER);
						psXFix.setObject(4, (prevROINumAdj != null ? new Integer((roiAdj[1] - prevROINumAdj.intValue())) : null), Types.INTEGER);
						psXFix.setInt(5, rsFix.getInt("id"));
						psXFix.executeUpdate();
						*/
						
						/*
						// Prepared statements -- Attempt 3:
						if (roi[0] != DBI.ID_NONE) psXFix.setInt(1, roi[0]); else psXFix.setNull(1, Types.INTEGER);
						if (roiAdj[0] != DBI.ID_NONE) psXFix.setInt(2, roiAdj[0]); else psXFix.setNull(2, Types.INTEGER);
						if (prevROINum != null) psXFix.setInt(3, new Integer((roi[1] - prevROINum.intValue()))); else psXFix.setNull(3, Types.INTEGER);
						if (prevROINumAdj != null) psXFix.setInt(4, new Integer((roiAdj[1] - prevROINumAdj.intValue()))); else psXFix.setNull(4, Types.INTEGER);
						psXFix.setInt(5, rsFix.getInt("id"));
						psXFix.executeUpdate();
						*/
						
						if (currROI    != null) { currStimFixCntROI++;     lnkFixCnt++;    }
						if (currROIAdj != null) { currStimFixCntROIAdj++;  lnkFixCntAdj++; }
					}
					
					// (2.2.4) ROI-unrelated stuff:
					currF.saccX    = (prevF != null ? currF.x    - prevF.x    : null);
					currF.saccY    = (prevF != null ? currF.y    - prevF.y    : null);
					
					if (currF.xAdj != null) currF.saccXAdj = (prevF != null ? currF.xAdj - prevF.xAdj : null);
					if (currF.yAdj != null) currF.saccYAdj = (prevF != null ? currF.yAdj - prevF.yAdj : null);
					
					// TODO: Use prepared statement:
					stX.executeUpdate(  // this should be a part of the import routine
						"UPDATE " + db + ".fix SET " +
						"sacc_i_x = "     + currF.saccX    + ", " +
						"sacc_i_y = "     + currF.saccY    + ", " +
						"sacc_i_x_adj = " + currF.saccXAdj + ", " +
						"sacc_i_y_adj = " + currF.saccYAdj + " "  +
						"WHERE id = " + currF.id
					);
					
					// (2.2.5) Update previous fixation (outgoing saccades):
					if (prevF != null) {
						// TODO: Use prepared statement:
						stX.executeUpdate(
							"UPDATE " + db + ".fix SET " +
							"sacc_o_x = "       + currF.saccX        + ", " +  // this (and the three following lines) should be a part of the import routine
							"sacc_o_y = "       + currF.saccY        + ", " +
							"sacc_o_x_adj = "   + currF.saccXAdj     + ", " +
							"sacc_o_y_adj = "   + currF.saccYAdj     + ", " +
							"sacc_o_roi = "     + currF.saccInROI    + ", " +
							"sacc_o_roi_adj = " + currF.saccInROIAdj + ", " +
							"sacc_o_let = "     + currF.saccInLet    + ", " +
							"sacc_o_let_adj = " + currF.saccInLetAdj + " "  +
							"WHERE id = " + prevF.id
						);
					}
					prevF = currF;
					
					prevROI    = currROI;
					prevROIAdj = currROIAdj;
				}
				rsFix.close();
				
				// (2.3) Link blinks:
				stX.executeUpdate("UPDATE blink SET roi_01_id = null, roi_02_id = null WHERE trial_id = " + trialId);
				ResultSet rsBlink = dbi.execQry("SELECT id, t, sx, sy, ex, ey FROM " + db + ".blink WHERE trial_id = " + trialId + " ORDER BY id");
				
				currStimIdx = 0;
				int currStimBlinkCnt = 0;
				ss = stimSeq.get(0);
				
				while (rsBlink.next()) {
					blinkCnt++;
					
					long t = rsBlink.getLong("t");
					
					if (t >= ss.t1) {  // updating the current stimulus and advancing to the next one
						stX.executeUpdate("UPDATE stim_seq SET blink_cnt = " + currStimBlinkCnt + " WHERE id = " + ss.id);
						ss = stimSeq.get(++currStimIdx);
						currStimBlinkCnt = 0;
					}
					
					if (t - ss.t0 <= tBuff) continue;  // blink in the "disregard" buffer (stimulus-end side)
					if (t + tBuff >= ss.t1) continue;  // blink in the "disregard" buffer (stimulus-start side)
					
					if (ss.stim == null) continue;
					
					int[] roi01 = ss.stim.inside(rsBlink.getFloat("sx"), rsBlink.getFloat("sy"));  // returns int array [id,num,let]
					int[] roi02 = ss.stim.inside(rsBlink.getFloat("ex"), rsBlink.getFloat("ey"));  // ditto
					
					if (roi01[0] != DBI.ID_NONE || roi02[0] != DBI.ID_NONE) {
						stX.executeUpdate("UPDATE " + db + ".blink SET roi_01_id = " + (roi01[0] != DBI.ID_NONE ? roi01[0] : "null") + ", roi_02_id = " + (roi02[0] != DBI.ID_NONE ? roi02[0] : "null") + " WHERE id = " + rsBlink.getInt("id"));
						
						currStimBlinkCnt++;
						lnkBlinkCnt++;
					}
				}
				rsBlink.close();
				
				// (2.4) Finish up:
				stX.executeUpdate("UPDATE stim_seq SET fix_cnt_roi = " + currStimFixCntROI + ", fix_cnt_roi_adj = " + currStimFixCntROIAdj + " WHERE id = " + ss.id);
				stX.executeUpdate("UPDATE trial SET linked = TRUE WHERE id = " + trialId);
			}
			rsTrial.close();
			
			stX.executeUpdate("UPDATE exp SET linked = TRUE WHERE id = " + expId);
			
			DBI.transCommit(stX, true);
		}
		catch (SQLException e) {
			DBI.transRollback(stX, true);
			throw e;
		}
		
		long t = timer.elapsed();
		return new Result(true, "rep:" + JSI.str2js("Trials processed: " + trialCnt + "\nFixations processed: " + fixCnt + "\nFixation-ROI links added: " + lnkFixCnt + "\nFixation-ROI links added (adjusted): " + lnkFixCntAdj + "\nBlinks processed: " + blinkCnt + "\nBlink-ROI links added: " + lnkBlinkCnt + "\nExecution time: " + Convert.time2str(t) + " (" + t + " ms)") + ",exp:" + get(dbi, username, expId));
	}
}
