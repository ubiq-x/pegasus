package app.gaze;

import inter.DBI;
import inter.JSI;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import app.gaze.dstruct.ROIVar;


import main.Do;
import main.Result;
import util.$;


/**
 * @author Tomek D. Loboda
 */
public class Var {
	public final static String[] RESERVED_NAMES = { "id","name","memo","abs","acos","asin","atan","ceiling","cos","cot","crc32","degrees","exp","floor","ln","log","log2","log10","mod","pi","pow","radians","rand","sign","sin","sqrt","tan","truncate", "pg-s", "pg-e", "ln-s", "ln-e", "sent-s", "sent-e", "cl-s", "cl-e", "pred-g-n3", "pred-g-n4", "pred-g-n5", "pred-m-n3", "pred-m-n4", "pred-m-n5" };
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Var()  throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static String isNameValid(DBI dbi, String username, String name)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		if (name.length() == 0) return "Variable name cannot be empty";
		if (name.indexOf("'") != -1 || name.indexOf("\"") != -1 || name.indexOf(",") != -1 || name.indexOf("=") != -1 || name.indexOf(";") != -1 || name.indexOf(":") != -1 || name.indexOf("|") != -1 || name.indexOf("+") != -1 || name.indexOf("/") != -1 || name.indexOf("\\") != -1) return "Variable names cannot contain punctuation characters.";
		if ($.strInArr(name.toLowerCase(), RESERVED_NAMES)) return "Name '" + name + "' is reserved. Please choose a differnt name.";
		
		return null;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static Result add(DBI dbi, String username, int expId, String objName, String type, String name)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		String db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		
		String isNameValid = isNameValid(dbi, username, name);
		if (isNameValid != null) return new Result(false, "msg:\"" + isNameValid + "\"");
		/*
		if (name.length() == 0) return new Result(false, "msg:\"Variable name cannot be empty.\"");
		if (name.indexOf("'") != -1 || name.indexOf("\"") != -1 || name.indexOf(",") != -1 || name.indexOf("=") != -1 || name.indexOf(";") != -1 || name.indexOf(":") != -1 || name.indexOf("|") != -1 || name.indexOf("+") != -1 || name.indexOf("/") != -1 || name.indexOf("\\") != -1) return new Result(false, "msg:\"Variable names cannot contain punctuation characters.\"");
		if ($.strInArr(name.toLowerCase(), RESERVED_NAMES)) return new Result(false, "msg:\"Name '" + name + "' is reserved. Please choose a differnt name.\"");
		*/
		
		if (type == null || (type != null && type.length() == 0)) type = "n";
		
		DBI dbiX = new DBI();
		Statement stX = dbiX.transBegin();
		
		String res = "";
		try {
			stX.executeUpdate("INSERT INTO " + db + ".var (exp_id, obj_name, type, name) VALUES (" + expId + ", '" + objName + "', '" + type + "', '" + name + "')", Statement.RETURN_GENERATED_KEYS);
			ResultSet rs = stX.getGeneratedKeys();
			rs.next();
			res = "v:{id:" + rs.getInt(1) + ",type:\"" + type +  "\",name:\"" + name + "\",memo:\"\",sys:0,val:{}}";
			rs.close();
			
			DBI.transCommit(stX, true);
		}
		catch (SQLException e) {
			DBI.transRollback(stX, true);
			
			if (e.getErrorCode() == DBI.SQL_ERR_DUP_ENTRY) return new Result(false, "msg:\"A variables with that name already exists. Variable names must be unique.\"");
			else throw e;
		}
		
		return new Result(true, res);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * This method adds a system variable and returns its ID.  If the specified variable already exists, its ID is 
	 * returned and the database is not changed.  This method should not be exposed to the front-end and therefore 
	 * there is no need to validate the variable name.
	 */
	public static int addSys(DBI dbiX, Statement stX, String username, int expId, String objName, String type, String name, String memo, boolean doCommit)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		String db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		
		int id = DBI.ID_NONE;
		
		if (DBI.getLong(dbiX, "SELECT COUNT(*) AS cnt FROM " + db + ".var WHERE exp_id = " + expId + " AND obj_name = '" + objName + "' AND name = '" + name + "' AND sys = 1", "cnt") == 1) {
			id = (int) DBI.getLong(dbiX, "SELECT id FROM " + db + ".var WHERE exp_id = " + expId + " AND obj_name = '" + objName + "' AND name = '" + name + "' AND sys = 1", "id");
		}
		else {
			// if (stX == null) stX = dbiX.transBegin();
			
			try {
				stX.executeUpdate("INSERT INTO " + db + ".var (exp_id, obj_name, type, name, memo, sys) VALUES (" + expId + ", '" + objName + "', '" + type + "', '" + name + "', '" + memo + "', 1)", Statement.RETURN_GENERATED_KEYS);
				ResultSet rs = stX.getGeneratedKeys();
				rs.next();
				id = rs.getInt(1);
				rs.close();
				
				if (doCommit) DBI.transCommit(stX, true);
			}
			catch (SQLException e) {
				if (doCommit) DBI.transRollback(stX, true);
				
				if (e.getErrorCode() == DBI.SQL_ERR_DUP_ENTRY) return DBI.ID_NONE;
				else throw e;
			}
		}
		
		return id;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static Result del(DBI dbi, String username, int id)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		String db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		
		if (DBI.getLong(dbi, "SELECT sys FROM " + db + ".var WHERE id = " + id, "sys") == 1) return new Result(false, "msg:\"System variables cannot be deleted.\"");
		
		DBI dbiX = new DBI();
		Statement stX = dbiX.transBegin();
		
		try {
			stX.executeUpdate("DELETE FROM " + db + ".var WHERE id = " + id);
			DBI.transCommit(stX, true);
		}
		catch (SQLException e) {
			DBI.transRollback(stX, true);
			throw e;
		}
		
		return new Result(true, "");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Generate variable.
	 */
	public static Result gen(DBI dbi, String username, int expId, String var, String name)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		String db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		
		String isNameValid = isNameValid(dbi, username, name);
		if (isNameValid != null) return new Result(false, "msg:\"" + isNameValid + "\"");
		
		String objName = null, type = null, memo = null;
		
		if (var.equals("read-spd-a")) {
			objName = "sub";  type = "n";  memo = "Reading speed A (roi_cnt / t)";
		}
		else return new Result(false, "msg:\"Incorrect variable specified.\"");
		
		DBI dbiX = new DBI();
		Statement stX = dbiX.transBegin();
		
		try {
			stX.executeUpdate("INSERT INTO " + db + ".var (exp_id, obj_name, type, name, memo) VALUES (" + expId + ", '" + objName + "', '" + type + "', '" + name + "', '" + memo + "')", Statement.RETURN_GENERATED_KEYS);
			ResultSet rsIns = stX.getGeneratedKeys();
			rsIns.next();
			int varId = rsIns.getInt(1);
			rsIns.close();
			
			if (var.equals("read-spd-a")) {
				dbiX.execUpd(
					"INSERT INTO " + db + ".xr_var_sub (var_id, sub_id, val) " +
					"SELECT " + varId + ", sub_id, (CASE WHEN wpm < 1000 THEN wpm ELSE NULL END) FROM " +
					"( " +
					"SELECT sub_id, ROUND(AVG(roi_cnt / t)) AS wpm FROM " +
					"( " +
					"SELECT s02.id AS sub_id, s01.id AS stim_id, r.cnt AS roi_cnt, (SUM(ss.t1-ss.t0) / (1000*60)) AS t " +
					"FROM " + db + ".stim_seq ss " +
					"INNER JOIN " + db + ".stim s01 ON ss.stim_id = s01.id " +
					"INNER JOIN " + db + ".trial t ON ss.trial_id = t.id " +
					"INNER JOIN " + db + ".sub s02 ON s02.id = t.sub_id " +
					"INNER JOIN (SELECT stim_id, COUNT(*) AS cnt FROM " + db + ".roi GROUP BY stim_id) r ON r.stim_id = s01.id " +
					"WHERE s02.exp_id = " + expId + " AND s01.type = 't' AND r.cnt > 0 " +
					"GROUP BY s01.id, s02.id, r.cnt " +
					") a " +
					"GROUP BY sub_id " +
					") b"
				);
			}
			
			DBI.transCommit(stX, true);
		}
		catch (SQLException e) {
			DBI.transRollback(stX, true);
			
			if (e.getErrorCode() == DBI.SQL_ERR_DUP_ENTRY) return new Result(false, "msg:\"A variables with that name already exists. Variable names must be unique.\"");
			else throw e;
		}
		
		return new Result(true, "v:" + getOne(dbi, username, expId, objName, name));
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static StringBuffer getAll(DBI dbi, String username, int expId, String objName, boolean doGetValues)  throws SQLException {
		String db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		
		StringBuffer res = new StringBuffer("[");
		
		ResultSet rsVar = dbi.execQry("SELECT id, type, name, memo, sys FROM " + db + ".var WHERE exp_id = " + expId + " AND obj_name = '" + objName + "' ORDER BY name");
		while (rsVar.next()) {
			res.append(
				"{" +
				"id:" + rsVar.getString("id") + "," +
				"type:\"" + rsVar.getString("type") + "\"," +
				"name:" + JSI.str2js(rsVar.getString("name")) + "," +
				"memo:" + JSI.str2js(rsVar.getString("memo")) + "," +
				"sys:" + rsVar.getString("sys") + "," +
				"val:"
			);
			
			if (doGetValues) {
				res.append("{");
				ResultSet rsVarVal = dbi.execQry("SELECT id, " + objName + "_id, val FROM " + db + ".xr_var_" + objName + " WHERE var_id = " + rsVar.getString("id"));
				while (rsVarVal.next()) {
					res.append(
						rsVarVal.getString(objName + "_id") + ":" +
						"{" +
						"id:" + rsVarVal.getString("id") + "," +
						objName + "Id:" + rsVarVal.getString(objName + "_id") + "," +
						"val:" + (rsVar.getString("type").equals("s") ? JSI.str2js(rsVarVal.getString("val")) : rsVarVal.getString("val")) +
						"}" + (!rsVarVal.isLast() ? "," : "")
					);
				}
				rsVarVal.close();
				res.append("}");
			}
			else res.append("null");
			res.append("}" + (!rsVar.isLast() ? "," : ""));
		}
		rsVar.close();
		res.append("]");
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/*
	private static int getId(DBI dbi, String username, int prjId, String dsName, String varName)  throws SQLException {
		return (int) DBI.getLong(dbi, "SELECT id FROM \"" + Do.DB_DATA + "-" + username + "\".var WHERE prj_id = " + prjId + " AND name = '" + dsName + "'", "id");
	}
	*/
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static StringBuffer getOne(DBI dbi, String username, int expId, String objName, String name)  throws SQLException {
		String db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		
		StringBuffer res = new StringBuffer();
		
		ResultSet rsVar = dbi.execQry("SELECT id, type, name, memo, sys FROM " + db + ".var WHERE exp_id = " + expId + " AND obj_name = '" + objName + "' AND name = '" + name + "'");
		rsVar.next();
		res.append(
			"{" +
			"id:" + rsVar.getString("id") + "," +
			"type:\"" + rsVar.getString("type") + "\"," +
			"name:" + JSI.str2js(rsVar.getString("name")) + "," +
			"memo:" + JSI.str2js(rsVar.getString("memo")) + "," +
			"sys:" + rsVar.getString("sys") + "," +
			"val:{");
		
		ResultSet rsVarVal = dbi.execQry("SELECT id, " + objName + "_id, val FROM " + db + ".xr_var_" + objName + " WHERE var_id = " + rsVar.getString("id"));
		while (rsVarVal.next()) {
			res.append(
				rsVarVal.getString(objName + "_id") + ":" +
				"{" +
				"id:" + rsVarVal.getString("id") + "," +
				objName + "Id:" + rsVarVal.getString(objName + "_id") + "," +
				"val:" + (rsVar.getString("type").equals("s") ? JSI.str2js(rsVarVal.getString("val")) : rsVarVal.getString("val")) +
				"}" + (!rsVarVal.isLast() ? "," : "")
			);
		}
		rsVarVal.close();
		res.append("}}" + (!rsVar.isLast() ? "," : ""));
		
		rsVar.close();
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static Result mutateROIVar(DBI dbi, String username, int origVarId, String name, String method)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		String db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		
		if (name.length() == 0) return new Result(false, "msg:\"Variable name cannot be empty.\"");
		if (name.indexOf("'") != -1 || name.indexOf("\"") != -1 || name.indexOf(",") != -1 || name.indexOf("=") != -1 || name.indexOf(";") != -1 || name.indexOf(":") != -1 || name.indexOf("|") != -1 || name.indexOf("+") != -1 || name.indexOf("/") != -1 || name.indexOf("\\") != -1) return new Result(false, "msg:\"Variable names cannot contain punctuation characters.\"");
		if ($.strInArr(name.toLowerCase(), RESERVED_NAMES)) return new Result(false, "msg:\"Name '" + name + "' is reserved. Please choose a differnt name.\"");
		
		util.Timer timer = new util.Timer();
		
		DBI dbiX = new DBI();
		Statement stX = dbiX.transBegin();
		stX.executeQuery("USE " + db);
		
		HashSet<Integer> addedROIIds = new HashSet<Integer>();
		boolean expC = method.equals("exp-c");
		int expId = (int) DBI.getLong(stX, "SELECT exp_id FROM var WHERE id = " + origVarId, "exp_id");
		int newVarId = DBI.ID_NONE;
		
		try {
			newVarId = DBI.insertGetId(stX, null, "INSERT INTO var (obj_name, exp_id, name, sys, type, def_val) VALUES ('roi', " + expId + ", '" + name + "', 0, 'b', '0')");
			
			Hashtable<Integer,ROIVar> RV = new Hashtable<Integer,ROIVar>();
			
			ResultSet rsStim = dbi.execQry("SELECT id FROM " + db + ".stim WHERE exp_id = " + expId);
			
			while (rsStim.next()) {  // process on stimulus-by-stimulus basis (avoid loading too many ROIs in the memory at once)
				int stimId = rsStim.getInt("id");
				
				// (1) Read all ROIs and values of relevant variables:
				Statement stROI = dbi.newSt();
				stROI.executeQuery("USE " + db);
				ResultSet rsROI = stROI.executeQuery(
					"SELECT r.id, ss.val AS ss, se.val AS se, cs.val AS cs, ce.val AS ce FROM roi r " +
					
					"LEFT JOIN ( " +
					"  SELECT x.roi_id, x.val FROM xr_var_roi x " +
					"  INNER JOIN var v ON x.var_id = v.id " +
					"  WHERE v.exp_id = " + expId + " AND v.name = 'sent-s' " +
					") ss ON ss.roi_id = r.id " +
					
					"LEFT JOIN (" +
					"  SELECT x.roi_id, x.val FROM xr_var_roi x " +
					"  INNER JOIN var v ON x.var_id = v.id " +
					"  WHERE v.exp_id = " + expId + " AND v.name = 'sent-e' " +
					") se ON se.roi_id = r.id " +
						
					"LEFT JOIN (" +
					"  SELECT x.roi_id, x.val FROM xr_var_roi x " +
					"  INNER JOIN var v ON x.var_id = v.id " +
					"  WHERE v.exp_id = " + expId + " AND v.name = 'cl-s' " +
					") cs ON cs.roi_id = r.id " +
					
					"LEFT JOIN (" +
					"  SELECT x.roi_id, x.val FROM xr_var_roi x " +
					"  INNER JOIN var v ON x.var_id = v.id " +
					"  WHERE v.exp_id = " + expId + " AND v.name = 'cl-e' " +
					") ce ON ce.roi_id = r.id " +
					
					"WHERE r.stim_id = " + stimId + " " +
					"ORDER BY r.id"
				);
				
				while (rsROI.next()) RV.put(rsROI.getInt("id"), new ROIVar(rsROI.getInt("id"), rsROI.getBoolean("ss"), rsROI.getBoolean("se"), rsROI.getBoolean("cs"), rsROI.getBoolean("ce")));
				
				rsROI.close();
				
				// (2) Retrieve IDs of ROIs with the original variable set:
				int idMin = (int) DBI.getLong(stX, "SELECT MIN(x.roi_id) AS min FROM xr_var_roi x INNER JOIN roi r ON r.id = x.roi_id WHERE r.stim_id = " + stimId, "min");
				int idMax = (int) DBI.getLong(stX, "SELECT MAX(x.roi_id) AS max FROM xr_var_roi x INNER JOIN roi r ON r.id = x.roi_id WHERE r.stim_id = " + stimId, "max");
				
				List<Integer> origROIs = new ArrayList<Integer>();
				rsROI = stROI.executeQuery("SELECT x.roi_id FROM xr_var_roi x INNER JOIN roi r ON r.id = x.roi_id WHERE r.stim_id = " + stimId + " AND x.var_id = " + origVarId + " AND x.val = 1 ORDER BY x.roi_id");
				while (rsROI.next()) origROIs.add(rsROI.getInt("roi_id"));
				rsROI.close();
				stROI.close();
				
				// (3) Expand:
				// TODO: utilize the 'ROIVar.done' attribute for performance
				Iterator<Integer> it = origROIs.iterator();
				while (it.hasNext()) {
					int roiId = it.next();
					ROIVar rvCurr = RV.get(roiId);
					
					// (2.1) Current:
					if (!addedROIIds.contains(roiId)) {
						addedROIIds.add(roiId);
						stX.executeUpdate("INSERT INTO xr_var_roi (var_id, roi_id, val) VALUES (" + newVarId + ", " + roiId + ", 1)");
					}
					
					// (2.2) To the left:
					ROIVar rvLeft = rvCurr;
					int j = roiId;
					
					while (j >= idMin && (rvLeft != null ? ((expC ? !rvLeft.cs : true) && !rvLeft.ss) : true)) {
						rvLeft = RV.get(j--);
						if (rvLeft != null && !addedROIIds.contains(rvLeft.id)) {
							addedROIIds.add(rvLeft.id);
							stX.executeUpdate("INSERT INTO xr_var_roi (var_id, roi_id, val) VALUES (" + newVarId + ", " + rvLeft.id + ", 1)");
						}
					}
					
					// (2.3) To the right:
					ROIVar rvRight = rvCurr;
					j = roiId;
					
					while (j <= idMax && (rvRight != null ? ((expC ? !rvRight.ce : true) && !rvRight.se) : true)) {
						rvRight = RV.get(j++);
						if (rvRight != null && !addedROIIds.contains(rvRight.id)) {
							addedROIIds.add(rvRight.id);
							stX.executeUpdate("INSERT INTO xr_var_roi (var_id, roi_id, val) VALUES (" + newVarId + ", " + rvRight.id + ", 1)");
						}
					}
				}
			}
			
			DBI.transCommit(stX, true);
		}
		catch (SQLException e) {
			DBI.transRollback(stX, true);
			
			if (e.getErrorCode() == DBI.SQL_ERR_DUP_ENTRY) return new Result(false, "msg:\"A variables with that name already exists. Variable names must be unique.\"");
			else throw e;
		}
		
		return new Result(true, "v:{id:" + newVarId + ",type:\"b\",name:\"" + name + "\",memo:\"\",sys:0},t:" + timer.elapsed() + ",tHuman:\"" + timer.elapsedHuman() + "\"");
	}
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * Sets the type of a given variable performing all necessary casting.
	 * 
	 * 'stX' is assumed to have the default schema set correctly already.
	 */
	public static Result setType(DBI dbi, String username, Statement stX, int id, String type)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		if (stX == null) {
			DBI dbiX = new DBI();
			stX = dbiX.transBegin();
			stX.executeQuery("SET search_path TO \"" + Do.DB_GAZE + "-" + username + "\"");
		}
		
		type = type.substring(1,2).toLowerCase();  // remove quotes around it
		int expId = (int) DBI.getLong(stX, "SELECT exp_id FROM var WHERE id = " + id, "exp_id");
		
		String oldType = DBI.getStr(stX, "SELECT type FROM var WHERE id = " + id, "type");
		String objName = DBI.getStr(stX, "SELECT obj_name FROM var WHERE id = " + id, "obj_name");
		
		try {
			// Update the type:
			stX.executeUpdate("UPDATE var SET type = '" + type + "' WHERE id = " + id);
			
			// Convert the values:
			if (oldType.equals("s") && type.equals("n")) {  // string --> number
				ResultSet rsVal = stX.executeQuery("SELECT id, val FROM xr_var_" + objName + " WHERE var_id = " + id);
				while (rsVal.next()) {
					try {
						Double d = new Double(rsVal.getString("val"));
						stX.addBatch("UPDATE xr_var_" + objName + " SET val = " + (d.doubleValue() == (double) d.intValue() ? d.toString().substring(0, d.toString().indexOf(".")) : d.doubleValue()) + " WHERE id = " + rsVal.getInt("id"));
					}
					catch (NumberFormatException e) {
						stX.addBatch("DELETE FROM xr_var_" + objName + " WHERE id = " + rsVal.getInt("id"));
					}
				}
				rsVal.close();
			}
			
			else if ((oldType.equals("n") || oldType.equals("s")) && type.equals("b")) {  // number,string --> boolean
				ResultSet rsVal = stX.executeQuery("SELECT id, val FROM xr_var_" + objName + " WHERE var_id = " + id);
				while (rsVal.next()) {
					try {
						Double d = new Double(rsVal.getString("val"));
						stX.addBatch("UPDATE xr_var_" + objName + " SET val = " + (d.intValue() == 0 ? 0 : 1) + " WHERE id = " + rsVal.getInt("id"));
					}
					catch (NumberFormatException e) {
						stX.addBatch("DELETE FROM xr_var_" + objName + " WHERE id = " + rsVal.getInt("id"));
					}
				}
				rsVal.close();
			}
			
			stX.executeBatch();
			DBI.transCommit(stX, true);
		}
		catch (SQLException e) {
			DBI.transRollback(stX, true);
			throw e;
		}
		
		return new Result(true, "var:" + getAll(dbi, username, expId, objName, true) + "");
	}
}
