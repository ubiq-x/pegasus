package app.gaze;

import inter.DBI;
import inter.HTTPI;
import inter.JSI;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.imageio.ImageIO;

import main.Do;
import main.Result;
import exception.ValidException;


/**
 * @author Tomek D. Loboda
 */
public class Stim {
	private static final Color COLOR_ROI_BG_NORMAL   = new Color(    0,     0,     0,     0);
	private static final Color COLOR_ROI_BG_SENT_S   = new Color(0.78f, 1.00f, 0.78f, 0.75f);
	private static final Color COLOR_ROI_BG_SENT_E   = new Color(1.00f, 0.78f, 0.78f, 0.75f);
	private static final Color COLOR_ROI_BG_CLAUSE_S = new Color(0.98f, 0.87f, 0.70f, 0.75f);
	private static final Color COLOR_ROI_BG_CLAUSE_E = new Color(0.75f, 0.93f, 0.98f, 0.75f);
	
	private static final Color COLOR_ROI_BORDER_NORMAL   = new Color(0.37f, 0.60f, 0.98f);
	private static final Color COLOR_ROI_BORDER_SENT_S   = new Color(0.20f, 0.66f, 0.10f);
	private static final Color COLOR_ROI_BORDER_SENT_E   = new Color(0.93f, 0.02f, 0.02f);
	private static final Color COLOR_ROI_BORDER_CLAUSE_S = new Color(0.78f, 0.63f, 0.43f);
	private static final Color COLOR_ROI_BORDER_CLAUSE_E = new Color(0.38f, 0.55f, 0.60f);
	
	
	// -----------------------------------------------------------------------------------------------------------------
	private static List<ROI> decodeROIs(DBI dbi, String username, int stimId, String[] R)  throws ValidException, SQLException {
		List<ROI> res = new ArrayList<ROI>();
		
		for (int i = 0; i < R.length; i++) {
			res.add(new ROI(dbi, username, stimId, R[i], i));
		}
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static void addROIs(DBI dbi, String username, Statement stX, String[] R, int stimId)  throws SQLException, ValidException, IOException {
		String db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		
		int roiTypeId = (int) DBI.getLong(stX, "SELECT id FROM roi_type WHERE name = 'rect'", "id");
		int expId     = (int) DBI.getLong(stX, "SELECT exp_id FROM stim WHERE id = " + stimId, "exp_id");
		
		Point boxP1 = (R.length == 0 ? new Point(0,0) : new Point(Integer.MAX_VALUE, Integer.MAX_VALUE));
		Point boxP2 = (R.length == 0 ? new Point(0,0) : new Point(Integer.MIN_VALUE, Integer.MIN_VALUE));
		
		List<ROI> lstROIs = decodeROIs(dbi, username, stimId, R);
		
		Iterator<ROI> itROI = lstROIs.iterator();
		while (itROI.hasNext()) {
			ROI r = itROI.next();
			int roiId = DBI.insertGetId(stX, null, "INSERT INTO roi (stim_id, type_id, name, num) VALUES (" + stimId + ", " + roiTypeId + ", " + DBI.esc(r.name) + ", " + r.num + ")");
			
			// Vertices:
			stX.executeUpdate("INSERT INTO vertex (roi_id, x, y) VALUES (" + roiId + "," + r.x1 + "," + r.y1 + ")");
			stX.executeUpdate("INSERT INTO vertex (roi_id, x, y) VALUES (" + roiId + "," + r.x2 + "," + r.y1 + ")");
			stX.executeUpdate("INSERT INTO vertex (roi_id, x, y) VALUES (" + roiId + "," + r.x2 + "," + r.y2 + ")");
			stX.executeUpdate("INSERT INTO vertex (roi_id, x, y) VALUES (" + roiId + "," + r.x1 + "," + r.y2 + ")");
			
			// Variables:
			if (r.vars.size() > 0) {
				Iterator<String> itVar = r.vars.iterator();
				while (itVar.hasNext()) {
					String var = itVar.next();
					String[] varDec = var.split(":");
					
					int varId = (int) DBI.getLong(stX, "SELECT id FROM " + db + ".var WHERE exp_id = " + expId + " AND obj_name = 'roi' AND name = '" + varDec[0] + "'", "id");
					if (varId == DBI.ID_NONE) throw new ValidException("Invalid format in the 'rois' file (line " + r.num + ") for the '" + DBI.getStr(stX, "SELECT name FROM " + db + ".stim WHERE id = " + stimId, "name") + "' stimulus:\n\n" + r.spec + "\n\nThe two most likely causes of this error are: (1) variables are not separated with a comma and (2) the variable listed here has not been added (inspect the 'var' directory of your import ZIP archive to make sure it is there).");
					
					stX.executeUpdate("INSERT INTO xr_var_roi (roi_id, var_id, val) VALUES (" + roiId + ", " + varId + ", '" + (varDec.length == 1 ? "1" : varDec[1]) + "')");
				}
			}
			
			// Text bounding box:
			if (r.x1 < boxP1.x) boxP1.x = r.x1;
			if (r.y1 < boxP1.y) boxP1.y = r.y1;
			if (r.x2 > boxP2.x) boxP2.x = r.x2;
			if (r.y2 > boxP2.y) boxP2.y = r.y2;
		}
		
		// Text bounding box:
		stX.executeUpdate("UPDATE stim SET box_x1 = " + boxP1.x + ", box_y1 = " + boxP1.y + ", box_x2 = " + boxP2.x + ", box_y2 = " + boxP2.y + " WHERE id = " + stimId);
		
		// Update images:
		genROIImg(dbi, username, stX, stimId, lstROIs);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Generates an image showing ROIs of the specified stimulus and saves it in the database.
	 */
	public static void genROIImg(DBI dbi, String username, Statement stX, int stimId, List<ROI> lstROIs)  throws SQLException, IOException {
		String db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		
		// (1) Get info:
		ResultSet rsInfo = stX.executeQuery("SELECT res_h, res_v FROM stim WHERE id = " + stimId);
		rsInfo.next();
		int w = rsInfo.getInt("res_h");
		int h = rsInfo.getInt("res_v");
		rsInfo.close();
		
		// (2) Generate image:
		BufferedImage bImg = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = bImg.createGraphics();
		
		// (2.1) Background:
		g2d.setColor(new Color(0, 0, 0, 0));  // transparent
		g2d.setComposite(AlphaComposite.Src);
		g2d.fill(new Rectangle2D.Float(0, 0, w, h));
		
		// (2.2) ROIs:
		g2d.setColor(COLOR_ROI_BORDER_NORMAL);
		g2d.setStroke(new BasicStroke(1f));
		
		Iterator<ROI> it = lstROIs.iterator();
		while (it.hasNext()) {
			ROI r = it.next();
			g2d.draw(new Rectangle2D.Float(r.x1, r.y1, r.x2 - r.x1, r.y2 - r.y1));
		}
		
		// (3) Persist:
		g2d.dispose();
		
		// (3.1) Prepare stream:
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(bImg, "png", baos);
		baos.flush();
		ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
		baos.close();
		
		// (3.2) Write:
		PreparedStatement ps = stX.getConnection().prepareStatement("UPDATE " + db + ".stim SET img_roi = ? WHERE id = " + stimId);
		ps.setBinaryStream(1, bais, (int) baos.size());
		ps.executeUpdate();
		ps.close();
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static Result getROIs(DBI dbi, String username, int stimId)  throws SQLException, UnsupportedEncodingException {
		String db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		
		util.Timer timer = new util.Timer();
		
		StringBuffer res = new StringBuffer("rois:[");
		
		ResultSet rsROI = dbi.execQry("SELECT id, name FROM " + db + ".roi WHERE stim_id = " + stimId + " ORDER BY id");
		while (rsROI.next()) {
			int roiId = rsROI.getInt("id");
			
			res.append("{name:" + JSI.str2js(rsROI.getString("name")) + ",");
			
			// Vertices:
			ResultSet rsVertex = dbi.execQry("SELECT x, y FROM " + db + ".vertex WHERE roi_id = " + roiId + " ORDER BY id");
			rsVertex.next();
			res.append("x1:" + rsVertex.getInt("x") + ",y1:" + rsVertex.getInt("y") + ",");
			rsVertex.next();
			rsVertex.next();
			res.append("x2:" + rsVertex.getInt("x") + ",y2:" + rsVertex.getInt("y") + ",vars:{");
			rsVertex.close();
			
			// Variables:
			ResultSet rsVar = dbi.execQry("SELECT v.name, v.type, COALESCE(x.val, 'null') AS val FROM " + db + ".xr_var_roi x INNER JOIN " + db + ".var v ON x.var_id = v.id WHERE x.roi_id = " + roiId);
			while (rsVar.next()) {
				String val = (rsVar.getString("type").equals("s") ? JSI.str2js(rsVar.getString("val")) : rsVar.getString("val"));
				res.append("\"" + rsVar.getString("name") + "\":" + val + (!rsVar.isLast() ? "," : ""));
			}
			rsVar.close();
			
			res.append((!rsROI.isLast() ? "}}," : "}}"));
		}
		rsROI.close();
		
		res.append("],t:" + timer.elapsed());
		
		return new Result(true, res);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static Result getWordPred(DBI dbi, String username, int stimId)  {
		String db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		
		StringBuilder res = new StringBuilder("yo mamma");
		return new Result(true, "wordPred:" + JSI.str2js(res.toString()));
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Retrieve and set word predictability for all stimuli in the specified experiment.
	 */
	public static Result setWordPred(DBI dbi, String username, int stimId)  throws MalformedURLException, IOException, SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		if (!HTTPI.isWebServiceOnline(Do.URL_NGRAM_GOOGLE, Do.NGRAM_APP_TIMEOUT)) return new Result(false, "msg:\"Error: The Ngram Web service cannot be reached.\"");
		
		StringBuilder res = new StringBuilder();
		
		app.gaze.ngram.Stim stim = new app.gaze.ngram.Stim(dbi, username, stimId);
		res.append(stim.retrieveWordPred(username, true, true));
		
		return new Result(true, "rep:" + JSI.str2js(res.toString()));
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static Result updateStim(DBI dbi, String username, int id, int fontSize, float letterSpac, int lineH, String alignH, String alignV, int marginT, int marginL, int marginR, int paddingT, int paddingB, int roiH, String roiExp, int offsetX, int offsetY, String rois)  throws SQLException, ValidException, InstantiationException, IllegalAccessException, ClassNotFoundException, IOException {
		String db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		
		int roiCnt = 0;
		
		DBI dbiX = new DBI();
		Statement stX = dbiX.transBegin();
		try {
			stX.execute("SET search_path TO " + db);
			stX.executeUpdate("UPDATE stim SET font_size = " + fontSize + ", letter_spac = " + letterSpac + ", line_h = " + lineH + ", align_h = '" + alignH + "', align_v = '" + alignV + "', margin_t = " + marginT + ", margin_l = " + marginL + ", margin_r = " + marginR + ", padding_t = " + paddingT + ", padding_b = " + paddingB + ", roi_h = " + roiH + ", roi_exp = " + roiExp + ", offset_x = " + offsetX + ", offset_y = " + offsetY + " WHERE id = " + id);
			stX.executeUpdate("UPDATE fix SET roi_id = null WHERE stim_id = " + id);
			stX.executeUpdate("DELETE FROM roi WHERE stim_id = " + id);
			
			if (rois != null) {
				String[] R = rois.split("\\|");
				roiCnt = R.length;
				addROIs(dbi, username, stX, R, id);
			}
			else roiCnt = 0;
			
			long expId = DBI.getLong(stX, "SELECT e.id FROM exp e INNER JOIN stim s ON s.exp_id = e.id WHERE s.id = " + id, "id");
			stX.executeUpdate("UPDATE trial SET linked = FALSE WHERE id IN (SELECT trial_id FROM stim_seq WHERE stim_id = " + id + ")");
			stX.executeUpdate("UPDATE exp SET linked = FALSE WHERE id = " + expId);
			stX.close();
			
			DBI.transCommit(stX, true);
		}
		catch (SQLException e) {
			DBI.transRollback(stX, true);
			throw e;
		}
		
		return new Result(true, "roiCnt:" + roiCnt);
	}
}
