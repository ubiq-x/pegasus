package app.gaze;

import inter.DBI;

import java.sql.SQLException;
import java.sql.Statement;

import main.Do;


/**
 * @author Tomek D. Loboda
 */
public class StimSeq {
	public final String stim;
	public final int t0;
	public final int t1;
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public StimSeq(String stim, int t0, int t1) {
		this.stim = stim;
		this.t0 = t0;
		this.t1 = t1;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static StringBuffer xform(DBI dbi, String username, int stimSeqId, int tx, int ty, float sx, float sy, double a)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		String dbG = "\"" + Do.DB_GAZE + "-" + username + "\"";
		Statement stX = (new DBI()).transBegin();
		stX.execute("SET search_path TO " + dbG);
		try {
			xform(dbi, stX, username, stimSeqId, tx, ty, sx, sy, a);
			DBI.transCommit(stX, true);
		}
		catch (SQLException e) { DBI.transRollback(stX, true); throw e; }
		
		return DBI.getEMData(dbi, username, stimSeqId);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Applies the specified transformation to all fixations belonging to the specified stimulus sequence entry. 
	 * Updates fixations and blinks.
	 */
	public static void xform(DBI dbi, Statement stX, String username, int stimSeqId, int tx, int ty, float sx, float sy, double a)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		// Log the transformation:
		if (tx != 0 || ty != 0 || sx != 1 || sy != 1 || a != 0) {
			stX.executeUpdate("INSERT INTO log_xform (stim_seq_id, tx, ty, sx, sy, a) VALUES (" + stimSeqId + ", " + tx + ", " + ty + ", " + sx + ", " + sy + ", " + a + ")");
		}
		
		// Translation and scale:
		if (tx != 0 || ty != 0 || sx != 1 || sy != 1) {
			stX.executeUpdate("UPDATE fix SET x = (x + " + tx + ") * " + sx + ", y = (y + " + ty + ") * " + sy  + " WHERE stim_seq_id = " + stimSeqId);
			stX.executeUpdate("UPDATE fix SET x_adj = (x_adj + " + tx + ") * " + sx + ", y_adj = (y_adj + " + ty + ") * " + sy  + " WHERE stim_seq_id = " + stimSeqId + " AND (NOT x_adj IS NULL)");
			stX.executeUpdate("UPDATE blink SET sx = (sx + " + tx + ") * " + sx + ", sy = (sy + " + ty + ") * " + sy + ", ex = (ex + " + tx + ") * " + sx + ", ey = (ey + " + ty + ") * " + sy + " WHERE stim_seq_id = " + stimSeqId);
		}
		
		// Rotation:
		a = (double) (Math.PI * a) / 180.0;
		if (a != 0) {
			double sin = Math.sin(a);
			double cos = Math.cos(a);
			
			stX.executeUpdate("UPDATE fix SET x = " + cos + " * x - " + sin + " * y" +  ", y = " + sin + " * x + " + cos + " * y" +  " WHERE stim_seq_id = " + stimSeqId);
			stX.executeUpdate("UPDATE fix SET x_adj = " + cos + " * x_adj - " + sin + " * y_adj" +  ", y_adj = " + sin + " * x_adj + " + cos + " * y_adj" +  " WHERE stim_seq_id = " + stimSeqId + " AND (NOT x_adj IS NULL)");
			stX.executeUpdate("UPDATE blink SET sx = " + cos + " * sx - " + sin + " * sy" +  ", sy = " + sin + " * sx + " + cos + " * sy" +  ", ex = " + cos + " * ex - " + sin + " * ey" +  ", ey = " + sin + " * ex + " + cos + " * ey" +  " WHERE stim_seq_id = " + stimSeqId);
		}
		
		long expId = DBI.getLong(stX, "SELECT e.id FROM exp e INNER JOIN stim s ON s.exp_id = e.id INNER JOIN stim_seq ss ON ss.stim_id = s.id WHERE ss.id = " + stimSeqId, "id");
		stX.executeUpdate("UPDATE trial SET linked = FALSE WHERE id = (SELECT trial_id FROM stim_seq WHERE id = " + stimSeqId + ")");
		stX.executeUpdate("UPDATE exp SET linked = FALSE WHERE id = " + expId);
	}
}
