package app.gaze.imp;


/**
 * @author Tomek D. Loboda
 */
public class XForm {
	private int tx = 0;
	private int ty = 0;
	private float sx = 0.0f;
	private float sy = 0.0f;
	private double a = 0.0d;
	
	private String spec = null;
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * spec: 'tx:10,ty:-47,sx:1.0,sy:0.86,a:0.0'
	 */
	public XForm(String spec) {
		this.spec = spec;
		
		String[] P = spec.split(",");
		for (String p: P) {
			String[] kv = p.split(":");
			if (kv.length < 2) continue;
			
			if (kv[0].equals("tx")) tx = new Integer(kv[1]);
			if (kv[0].equals("ty")) ty = new Integer(kv[1]);
			if (kv[0].equals("sx")) sx = new Float(kv[1]);
			if (kv[0].equals("sy")) sy = new Float(kv[1]);
			if (kv[0].equals("a"))  a  = new Double(kv[1]);
		}
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public String getSpec() { return spec; }
	public int getTX()      { return tx; }
	public int getTY()      { return ty; }
	public float getSX()    { return sx; }
	public float getSY()    { return sy; }
	public double getA()    { return a;  }
}
