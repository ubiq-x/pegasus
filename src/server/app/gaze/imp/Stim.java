package app.gaze.imp;

import java.util.ArrayList;
import java.util.List;


/**
 * @author Tomek D. Loboda
 */
public class Stim {
	private int id;
	private String name;
	private String txt;
	
	private final List<ROI> R = new ArrayList<ROI>(); 
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Stim(int id, String name, String txt) {
		this.id = id;
		this.name = name;
		this.txt = txt;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public int getId()      { return id;   }
	public String getName() { return name; }
	public String getTxt()  { return txt;  }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public void setId(int id)        { this.id = id;     }
	public void setName(String name) { this.name = name; }
	public void setTxt(String txt)   { this.txt = txt;   }
	
	
	// =================================================================================================================
	private class ROI {
		private final int id;
		private final String name;
		
		
		// -------------------------------------------------------------------------------------------------------------
		public ROI(int id, String name) {
			this.id   = id;
			this.name = name;
		}
		
		
		// -------------------------------------------------------------------------------------------------------------
		public int getId() { return id; }
		public String getName() { return name; }
	}
}
