package app.gaze.imp;

import inter.DBI;

import java.awt.Dimension;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


/**
 * @author Tomek D. Loboda
 */
public class Cal {
	public static final float ERR_MAX_ANG = 999.99f;  // DB: numeric(5,2)
	public static final float ERR_MAX_PX = 99999.9f;  // DB: numeric(6,1)
	
	private static final Map<Integer, Map<Integer, Integer>> mapPointNum;
		/*
		 * Maps the EyeLink point order (see below) to the Pegasus one (left-to-right, top-to-bottom). The key of the 
		 * outter map is the number of points used in the calibration routine (e.g., 9). The key of the inner map is the
		 * EyeLink point index. 
		 * 
		 * EyeLink point order:
		 *   9 points:
		 *     5 1 6
		 *     3 0 4
		 *     7 2 8
		 * 
		 * TODO: add orders for other possible point counts
		 */
	
	static {
		Map<Integer, Integer> map9 = new HashMap<Integer, Integer>();
		map9.put(0,4);
		map9.put(1,1);
		map9.put(2,7);
		map9.put(3,3);
		map9.put(4,5);
		map9.put(5,0);
		map9.put(6,2);
		map9.put(7,6);
		map9.put(8,8);
		
		mapPointNum = new HashMap<Integer, Map<Integer, Integer>>();
		mapPointNum.put(9, map9);
	}
	
	public int id = DBI.ID_NONE;
	
	public int trialId;
	
	public final Dimension scrDim;
	
	public final long t;  // [ms]
	
	public char eye = 'x';
	
	public int pointCnt = -1;
	
	public String calRes = "";
	public String valRes = "";
	
	public float valErrAvg = -1.0f;  // [deg]
	public float valErrMax = -1.0f;  // [deg]
	
	public int stimCnt = 0;
	public long dur = -1;  // [ms]
	
	private final List<CalPoint> P = new ArrayList<CalPoint>();
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Cal(int trialId, Dimension scrDim, long t) {
		this.trialId = trialId;
		this.scrDim = scrDim;
		this.t = t;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public void addPoint(int num, int x, int y, float xErr, float yErr, float angErr) {
		P.add(new CalPoint(mapPointNum.get(pointCnt).get(num), x, y, xErr, yErr, angErr));
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Updates the persisted object. This method should be called after a new calibration has been encountered or the 
	 * trial has ended. What is updated below is everything that could have changed during that time.
	 */
	public void del(Statement stX)  throws SQLException {
		stX.executeUpdate("UPDATE stim_seq SET cal_id = null WHERE cal_id = " + id);
		stX.executeUpdate("DELETE FROM cal WHERE id = " + id);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public boolean isTrialBound() { return trialId != DBI.ID_NONE; }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public boolean hasAllPoints() { return P.size() == pointCnt; }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public void persist(Statement stX)  throws SQLException {
		id = DBI.insertGetId(stX, null, "INSERT INTO cal (trial_id, scr_w, scr_h, t, eye, point_cnt, cal_res, val_res, val_err_avg, val_err_max, stim_cnt, dur) VALUES (" + trialId + ", " + scrDim.width + ", " + scrDim.height + ", " + t + ", '" + eye + "', " + pointCnt + ", '" + calRes + "', '" + valRes + "', " + (valErrAvg > ERR_MAX_ANG ? ERR_MAX_ANG : valErrAvg) + ", " + (valErrMax > ERR_MAX_ANG ? ERR_MAX_ANG : valErrMax) + ", " + stimCnt + ", " + dur + ")");
		
		Iterator<CalPoint> it = P.iterator();
		while (it.hasNext()) {
			it.next().persist(stX, id);
		}
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public void resetPoints() { P.clear(); }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Updates the persisted object. This method should be called after a new calibration has been encountered or the 
	 * trial has ended. What is updated below is everything that could have changed during that time.
	 */
	public void upd(Statement stX, long t)  throws SQLException {
		dur = t - this.t;
		stX.executeUpdate("UPDATE cal SET stim_cnt = " + stimCnt + ", dur = " + dur + " WHERE id = " + id);
	}
}
