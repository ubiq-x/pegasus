package app.gaze.imp;

import inter.DBI;
import inter.FSI;

import java.awt.Dimension;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import main.Do;

import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.Rserve.RserveException;

import util.$;
import util.KeyVal;
import util.RegExpr;
import app.gaze.Exp;
import app.gaze.StimSeq;
import exception.PegasusException;
import exception.ValidException;


/**
 * PATTERN_SUB_XXX constants will be applied to the contents of the subject
 * file(s) and filter out the unnecessary lines.
 * 
 * @author Tomek D. Loboda
 */
public class Import {
	private final static int FORMAT_UNKNOWN = 0;
	private final static int FORMAT_EL2 = 1;  // EyeLink 2
	private final static int FORMAT_TOBII = 2;
	private final static int FORMAT_DP6 = 3;  // Generation VI Dual Purkinjie
	
	private final static String GLOBAL_STIM_TXT_FILE_DELIM_01 = "=== ";
	private final static String GLOBAL_STIM_TXT_FILE_DELIM_02 = " ===";
	
	private final static Pattern PATTERN_EYELINK_SUB_FILE = Pattern.compile(
		"^MSG[\\s].*$" +
		"|^START[\\s].*$" +
		"|^END[\\s].*$" +
		"|^EFIX[\\s].*$" +
		"|^ESACC[\\s].*$" +
		"|^EBLINK[\\s].*$"
	);
	
	private final static Pattern PATTERN_EYELINK_CAL_CAL_HEAD = Pattern.compile("^MSG[\\s]+[\\d]+[\\s]+!CAL[\\s]+CALIBRATION[\\s]+[^\\d]+([\\d]+)[\\s]+([LR])[\\s]+[^\\s]+[\\s]+([^\\s]+).*$");
		// e.g.: MSG 4525978 !CAL CALIBRATION HV9 R RIGHT   GOOD
	    //                                      - -         ----
		// $:                                   1 2         3
	
	private final static Pattern PATTERN_EYELINK_CAL_VAL_HEAD = Pattern.compile("^MSG[\\s]+[\\d]+[\\s]+!CAL[\\s]+VALIDATION[\\s]+[^\\d]+([\\d]+)[\\s]+([LR])[\\s]+[^\\s]+[\\s]+([^\\s]+)[\\s]+ERROR[\\s]+([\\d.-]+)[\\s]+avg[.][\\s]+([\\d.-]+)[\\s]+max[\\s]+OFFSET[\\s]+([\\d.-]+)[\\s]+deg[.][\\s]+([\\d.-]+),([\\d.-]+)[\\s]+pix[.].*$");
		// e.g.: MSG 4543167 !CAL VALIDATION HV9 R RIGHT POOR ERROR 3.06 avg. 23.10 max  OFFSET 2.09 deg. 85.1,-6.3 pix.
		//                                     - -       ----       ----      -----             ----      ---- ----
		// $:                                  1 2       3          4         5                 6         7    8
	
	private final static Pattern PATTERN_EYELINK_CAL_VAL_POINT = Pattern.compile("^MSG[\\s]+[\\d]+[\\s]+VALIDATE[\\s]+([LR])[\\s]+[^\\s]+[\\s]+([\\d]+)[^\\d]+([\\d]+)[,]([\\d]+)[\\s]+OFFSET[\\s]+([\\d.-]+)[\\s]+deg[.][\\s]+([\\d.-]+)[,]([\\d.-]+)[\\s]+pix[.].*$");
		// e.g.: MSG 4543167 VALIDATE R 4POINT 5 RIGHT  at 800,600  OFFSET 0.96 deg.  -25.7,29.1 pix.
		//                            -        -           --- ---         ----       ----- ----
		// $:                         1        2           3   4           5          6     7
	
	private final String username;
	private final String db;
	private final int expId;
	private final String filename;
	private final DBI dbi;
	
	private final StringBuffer warnings = new StringBuffer();
	private int warningCnt = 0;
	
	private int globalStimCnt = 0;
	private int globalSubCnt = 0;
	private int globalTrialCnt = 0;
	private int globalROICnt = 0;
	
	private final Hashtable<String, Stim> htStim = new Hashtable<String, Stim>();  // stim-name --> app.gaze.imp.Stim
	private final EvtLst evtLst = new EvtLst();
	private final Hashtable<String, PreparedStatement> ps = new Hashtable<String, PreparedStatement>();
	
	private int fixCnt = 0;
	private int blinkCnt = 0;
	private int evtCnt = 0;
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Import(String username, int expId, String filename)  throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		this.username = username;
		this.db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		this.expId = expId;
		this.filename = filename;
		
		dbi = new DBI();
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Complements a variable by making sure each object has an entry for it with the 
	 * default value.
	 */
	/*
	private void complementVar(Statement st)  throws SQLException {
		Statement stVar = dbi.getConn().createStatement();
		ResultSet rsVar = stVar.executeQuery("SELECT obj_name, exp_id, name, def_val FROM " + db + ".var WHERE id = " + id);
		if (!rsVar.next()) {
			rsVar.close();
			stVar.close();
			return;
		}
		String obj = rsVar.getString("obj_name");
		int expId = rsVar.getInt("exp_id");
		String name = rsVar.getString("name");
		String defVal = rsVar.getString("def_val");
		rsVar.close();
		stVar.close();
		
		st.executeUpdate("UPDATE " + db + ".xr_var_" + obj + " x SET ");
	}
	*/
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Returns the format of the data.
	 */
	private int identifyFormat(File dir) {
		boolean unknown = false;
		boolean el2     = true;
		boolean tobii   = true;
		boolean dp6     = true;
		
		// DP6:
		{
			File[] trialFiles = dir.listFiles(new FSI.FileFilterExact("trial"))[0].listFiles();
			for (File f: trialFiles) {
				if (f.getName().startsWith(".")) continue;
				
				dp6 = dp6 && f.isFile() && FSI.getExt(f).equalsIgnoreCase("tra"); 
				if (!dp6) break;
			}
			if (dp6) return FORMAT_DP6;
		}
		
		// EL2 or Tobii:
		{
			File[] trialDirs = dir.listFiles(new FSI.FileFilterExact("trial"))[0].listFiles();
			for (File t: trialDirs) {
				File[] F = t.listFiles();
				if (F == null) continue;  // skip regular files (not directories)
				
				for (File f: F) {
					if (f.getName().startsWith(".")) continue;
					
					el2 = el2 && ((!f.isDirectory() && f.getName().substring(f.getName().indexOf('.')).equalsIgnoreCase(".asc")));
					tobii = tobii && f.isDirectory();
					unknown = !el2 && !tobii;
					if (unknown) break;
				}
				if (unknown) break;
			}
		}
		
		return (unknown ? FORMAT_UNKNOWN : ( el2 ? FORMAT_EL2 : FORMAT_TOBII));
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Processes the temporary directory assuming the data conforms to the specified format.
	 */
	private StringBuffer procDir(Statement stX, int format, File dir)  throws SQLException, IOException, ValidException, InstantiationException, IllegalAccessException, ClassNotFoundException, PegasusException {
		StringBuffer res = new StringBuffer();
		util.Timer timer = new util.Timer();
		
		// (1) Prepare:
		res.append("PREPARE\n");
		
		// (1.1) Events file:
		res.append(procEvtFile(stX, dir));
		
		// (1.2) Remove existing subjects:
		res.append("REMOVE EXISTING DATA\n");
		stX.executeUpdate("DELETE FROM sub WHERE exp_id = " + expId);
		res.append("\tRemoved existing subjects (with all associated trials, events, and gaze data)\n");
		
		// (1.3) Remove existing stimuli:
		stX.executeUpdate("DELETE FROM stim WHERE exp_id = " + expId);
		res.append("\tRemoved existing stimuli (with all associated ROIs)\n");
		
		// (1.4) Preserve existing data sets (Pegasus Data):
		res.append("\tPreserved existing data sets (Pegasus Data; manual removal only)\n\n");
		
		// (2) Import:
		// (2.1) Add variables:
		res.append("IMPORT\n");
		res.append(procVar(stX, dir));
		
		// (2.2) Add stimuli:
		res.append(procStim(stX, dir));
		
		if (format == FORMAT_DP6) res.append(procDir_ig6(stX, dir));
		else {
			// (2.3) Add subjects:
			res.append(procSub(stX, dir));
			
			// (2.4) Process trials:
			res.append("\tTRIALS\n\t\tInfo: (fixations, blinks, events)\n");
			File[] dirs = dir.listFiles(new FSI.FileFilterExact("sub"))[0].listFiles();
			for (File s: dirs) {
				String subName = s.getName();
				if (subName.startsWith(".")) continue;
				
				int subId = (int) DBI.getLong(stX, "SELECT id FROM sub WHERE exp_id = " + expId + " AND name = '" + subName + "'", "id");
				res.append("\t\tSUBJECT: " + subName + "\n");
				
				// Add trials for the subject:
				File[] trialDirs = dir.listFiles(new FSI.FileFilterExact("trial"))[0].listFiles();
				for (File t: trialDirs) {
					if (t.isFile()) continue;
					
					globalTrialCnt++;
					int trialId = DBI.insertGetId(stX,
						"SELECT id FROM trial WHERE sub_id = " + subId + " AND name = '" + t.getName() + "'",
						"INSERT INTO trial (sub_id, name, memo, dur) VALUES (" + subId + ", '" + t.getName() + "', '', null)"
					);
					res.append("\t\t\tTRIAL: " + t.getName() + " (file)\n");
					switch (format) {
						case FORMAT_EL2:
							res.append(procTrialEL2(stX, t.getAbsolutePath(), subId, subName, trialId));
							break;
						case FORMAT_TOBII:
							res.append(procTrialTobii(stX, t.getAbsolutePath(), subName, trialId));
							break;
					}
				}
				res.append("\n");
			}
			//complementVar(stX);
		}
		
		// (2.6) Account for base trials:
		res.append(procTrials_calcBase(stX, dir));
		
		// (2.5) Exclude:
		res.append(procExcFile(stX, dir) + "\n");
		
		// (3) Finish:
		// (3.1) Warnings:
		if (warnings.length() > 0) res.append("WARNINGS\n" + warnings + "\n");
		
		// (3.2) Summary:
		ResultSet rs = stX.executeQuery("SELECT name FROM exp WHERE id = " + expId);
		rs.next();
		res.append(
			"SUMMARY\n" +
			"\tExperiment: '" + rs.getString("name") + "'\n" +
			"\t\n" +
			"\tStimuli: " + globalStimCnt + "\n" +
			"\tROIs: " + globalROICnt + "\n" +
			"\t\n" +
			"\tSubjects: " + globalSubCnt + "\n" +
			"\tTrials: " + globalTrialCnt + "\n" +
			"\tEvents: " + evtCnt + "\n" +
			"\t\n" +
			"\tFixations: " + fixCnt + "\n" +
			"\tBlinks: " + blinkCnt + "\n" +
			"\t\n" +
			"\tImport time (excluding unzipping): " + timer.elapsedHuman() + "\n"
		);
		rs.close();
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	// http://localhost:8080/pegasus/servlet/main.Do?username=tomek&app=gaze&cmd=exp-import&exp-id=93&filename=%2Fq%2Ftmp%2Fpp.zip
	private StringBuffer procDir_ig6(Statement stX, File dir)  throws SQLException, IOException, ValidException, InstantiationException, IllegalAccessException, ClassNotFoundException, PegasusException {
		// (1) Init:
		StringBuffer res = new StringBuffer();
		
		List <TrialDP6> T = new ArrayList<TrialDP6>();
		TrialDP6 trialCurr = null;
		
		int trialOrdNo = -1;
		boolean isAfter = false;
		
		// (2) Process trials:
		File[] F = dir.listFiles(new FSI.FileFilterExact("trial"))[0].listFiles();
		for (File f: F) {
			// (2.1) Digest the trials file:
			globalSubCnt++;
			String subName = FSI.getFilename(f);
			int subId = DBI.insertGetId(stX, null, "INSERT INTO sub (exp_id, name) VALUES (" + expId + ", '" + subName + "')");
			
			List<String> L = FSI.file2lst(f.getAbsolutePath());  // lines
			for (String l: L) {
				l = l.trim();
				if (l.length() == 0) continue;
				
				if (l.toLowerCase().startsWith("before corrections:")) {
					if (trialCurr != null) T.add(trialCurr);
					trialCurr = null;
					isAfter = false;
					trialOrdNo = -1;
					continue;
				}
				
				if (l.toLowerCase().startsWith("after corrections:")) {
					isAfter = true;
					continue;
				}
				
				if (!isAfter) continue;
				
				Matcher m = Pattern.compile("trial (\\d+) item (\\d+) condition (\\d+)", Pattern.CASE_INSENSITIVE).matcher(l);
				if (m.find()) {
					trialOrdNo = new Integer(m.group(1));
					int item   = new Integer(m.group(2));
					int cond   = new Integer(m.group(3));
					
					trialCurr = new TrialDP6(htStim, subId, trialOrdNo, item, cond);
					continue;
				}
				
				if (l.replaceAll(" ", "").equalsIgnoreCase("fixletdurfixletdurfixletdur")) continue;
				if (trialOrdNo == -1) continue;
				if (l.toLowerCase().startsWith("number of ")) continue;
				
				trialCurr.addLine(l);
			}
			if (trialCurr != null) T.add(trialCurr);
			
			// (2.2) Persist:
			for (TrialDP6 t: T) {
				t.decode();
				fixCnt += t.getFixCnt();
				t.persist(stX);
			}
			
			// (2.3) Finish:
			globalTrialCnt += T.size();
			trialCurr = null;
			T.clear();
			isAfter = false;
		}
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	private StringBuffer procExcFile(Statement stX, File dir)  throws IOException, SQLException {
		StringBuffer res = new StringBuffer("EXCLUDE\n");
		List<String> errLines = new ArrayList<String>();
		int stimCnt = 0, subCnt = 0, trialCnt = 0, stimSeqCnt = 0, evtCnt = 0;
		
		File[] F = dir.listFiles(new FSI.FileFilterExact("exc"));
		if (F.length > 0 && F[0].isFile()) {
			String currSubName = null, currTrialName = null;
			int currSubId = DBI.ID_NONE, currTrialId = DBI.ID_NONE, currStimSeqId = DBI.ID_NONE;
			int currStimSeqIdx = -1, currEvtIdx = -1;
			
			List<String> L = FSI.file2lst(F[0].getAbsolutePath());
			for (int i = 0; i < L.size(); i++) {
				String l = L.get(i).trim();
				if (l.length() == 0) continue;
				
				String[] A = l.split("\t");
				if (A.length < 2) continue;
				
				boolean isExc = (A.length > 2 && A[2].equals("EXC"));
				String excInfo = (A.length == 4 ? DBI.esc(A[3]) : "NULL");
				
				// Stimulus:
				if (A[0].equals("stim:")) {
					if (!isExc) continue;
					
					int stimId = (int) DBI.getLong(stX, "SELECT id FROM stim WHERE exp_id = " + expId + " AND name = " + DBI.esc(A[1]), "id");
					if (stimId == DBI.ID_NONE) { errLines.add((i+1) + " (stim): stimulus does not exist"); continue; }
					
					stX.executeUpdate("UPDATE stim SET exc = TRUE, exc_info = " + excInfo + " WHERE id = " + stimId);
					stimCnt++;
				}
				
				// Subject:
				else if (A[0].equals("sub:")) {
					currSubName = A[1];
					currSubId = DBI.ID_NONE;
					
					if (!isExc) continue;
					
					currSubId = (int) DBI.getLong(stX, "SELECT id FROM sub WHERE exp_id = " + expId + " AND name = " + DBI.esc(currSubName), "id");
					if (currSubId == DBI.ID_NONE) { errLines.add((i+1) + " (sub): subject does not exist"); continue; }
					
					stX.executeUpdate("UPDATE sub SET exc = TRUE, exc_info = " + excInfo + " WHERE id = " + currSubId);
					subCnt++;
				}
				
				// Trial:
				else if (A[0].equals("trial:")) {
					currTrialName = A[1];
					currTrialId = DBI.ID_NONE;
					currStimSeqIdx = -1;
					
					if (!isExc) continue;
					
					if (currSubId == DBI.ID_NONE) currSubId = (int) DBI.getLong(stX, "SELECT id FROM sub WHERE exp_id = " + expId + " AND name = " + DBI.esc(currSubName), "id");
					if (currSubId == DBI.ID_NONE) { errLines.add((i+1) + " (trial): associated subject does not exist"); continue; }
					currTrialId = (int) DBI.getLong(stX, "SELECT id FROM trial WHERE sub_id = " + currSubId + " AND name = " + DBI.esc(currTrialName), "id");
					if (currTrialId == DBI.ID_NONE) { errLines.add("" + (i+1) + " (trial): trial does not exist"); continue; }
					
					stX.executeUpdate("UPDATE trial SET exc = TRUE, exc_info = " + excInfo + " WHERE id = " + currTrialId);
					trialCnt++;
				}
				
				// Stim-sequence:
				else if (A[0].equals("s-seq:")) {
					currStimSeqId = DBI.ID_NONE;
					currStimSeqIdx++;
					currEvtIdx = -1;
					
					if (!isExc) continue;
					
					if (currSubId == DBI.ID_NONE) currSubId = (int) DBI.getLong(stX, "SELECT id FROMsub WHERE exp_id = " + expId + " AND name = " + DBI.esc(currSubName), "id");
					if (currSubId == DBI.ID_NONE) { errLines.add((i+1) + " (s-seq): associated subject does not exist"); continue; }
					if (currTrialId == DBI.ID_NONE) currTrialId = (int) DBI.getLong(stX, "SELECT id FROM trial WHERE sub_id = " + currSubId + " AND name = " + DBI.esc(currTrialName), "id");
					if (currTrialId == DBI.ID_NONE) { errLines.add((i+1) + " (s-seq): associated trial does not exist"); continue; }
					currStimSeqId = (int) DBI.getLong(stX, "SELECT id FROM stim_seq WHERE trial_id = " + currTrialId + " ORDER BY t0 LIMIT 1 OFFSET " + currStimSeqIdx, "id");
					if (currStimSeqId == DBI.ID_NONE) { errLines.add((i+1) + " (s-seq): stimulus sequence entry does not exist"); continue; }
					
					stX.executeUpdate("UPDATE stim_seq SET exc = TRUE, exc_info = " + excInfo + " WHERE id = " + currStimSeqId);
					stimSeqCnt++;
				}
				
				// Event:
				else if (A[0].equals("evt:")) {
					currEvtIdx++;
					
					if (A.length == 2) continue;  // not excluded
					
					if (currSubId == DBI.ID_NONE) currSubId = (int) DBI.getLong(stX, "SELECT id FROM sub WHERE exp_id = " + expId + " AND name = " + DBI.esc(currSubName), "id");
					if (currSubId == DBI.ID_NONE) { errLines.add((i+1) + " (evt): associated subject does not exist"); continue; }
					if (currTrialId == DBI.ID_NONE) currTrialId = (int) DBI.getLong(stX, "SELECT id FROM trial WHERE sub_id = " + currSubId + " AND name = " + DBI.esc(currTrialName), "id");
					if (currTrialId == DBI.ID_NONE) { errLines.add((i+1) + " (evt): associated trial does not exist"); continue; }
					if (currStimSeqId == DBI.ID_NONE) currStimSeqId = (int) DBI.getLong(stX, "SELECT id FROM stim_seq WHERE trial_id = " + currTrialId + " ORDER BY t0 LIMIT 1 OFFSET " + currStimSeqIdx, "id");
					if (currStimSeqId == DBI.ID_NONE) { errLines.add((i + 1) + " (evt): associated stimulus sequence entry does not exist"); continue; }
					int evtId = (int) DBI.getLong(stX, "SELECT id FROM evt WHERE stim_seq_id = " + currStimSeqId + " ORDER BY t_prompt LIMIT 1 OFFSET " + currEvtIdx, "id");
					if (evtId == DBI.ID_NONE) { errLines.add((i+1) + " (evt): event does not exist"); continue; }
					
					stX.executeUpdate("UPDATE evt SET exc = TRUE, exc_info = " + DBI.esc(A[3]) + " WHERE id = " + evtId);
					evtCnt++;
				}
			}
		}
		else res.append("\tNo exclusions -- no 'exc' file found\n");
		
		res.append(
			(errLines.size() > 0 ? "\tFailed on lines\n\t\t" + $.join(errLines, "", "", "\n\t\t", false, false) + "\n\t\n" : "") +
			"\tExcluded\n" +
			"\t\tStimuli: " + stimCnt + "\n" +
			"\t\tSubjects: " + subCnt + "\n" +
			"\t\tTrials: " + trialCnt + "\n" +
			"\t\tStimulus seq.: " + stimSeqCnt + "\n" +
			"\t\tEvents: " + evtCnt + "\n"
		);
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	private StringBuffer procEvtFile(Statement stX, File dir)  throws IOException, SQLException {
		StringBuffer res = new StringBuffer("\tANTICIPATED EVENTS\n");
		File[] F = dir.listFiles(new FSI.FileFilterExact("evt"));
		if (F.length > 0 && F[0].isFile()) {
			List<String> L = FSI.file2lst(F[0].getAbsolutePath());
			for (String l: L) {
				String oldName = null;  // name of the event to be antiticaped in the data stream
				String newName = null;  // name to be stored in the DB
				if (l.indexOf("|") == -1) {
					oldName = l;
					newName = l;
				}
				else {
					oldName = l.substring(0, l.indexOf("|"));
					newName = l.substring(l.indexOf("|") + 1);
				}
				evtLst.add(stX, expId, oldName, newName, null);
				res.append("\t\t" + (oldName.equalsIgnoreCase(newName) ? "'" + oldName + "'" : "'" + oldName + "' (rename to '" + newName + "')") + "\n");
			}
		}
		else res.append("\t\tNone -- no 'evt' file found\n");
		res.append("\n");
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Processes the 'stim' directory by adding all stimuli inside it to the DB.
	 */
	private StringBuffer procStim(Statement stX, File dir)  throws SQLException, IOException, ValidException {
		StringBuffer res = new StringBuffer("\tSTIMULI\n\t\tStimulus\t\tImage size [kB]\tText length [char]\tMemo length [char]\tType\n");
		File[] stimuliDirs = dir.listFiles(new FSI.FileFilterExact("stim"))[0].listFiles();
		
		// Global 'text':
		String globalStimTxtPath = dir.getAbsolutePath() + File.separatorChar + "stim" + File.separatorChar + "text.txt";
		if (new File(globalStimTxtPath).exists()) {
			String[] S = FSI.getStr(globalStimTxtPath).toString().split(GLOBAL_STIM_TXT_FILE_DELIM_01);
			for (String s: S) {
				if (s.length() == 0) continue;
				int idxEndLine = s.indexOf("\n");
				String name = s.substring(0, idxEndLine - GLOBAL_STIM_TXT_FILE_DELIM_02.length());
				String txt = s.substring(idxEndLine + 1);
				procStim_updStim(DBI.ID_NONE, name, txt);
			}
		}
		
		// Global 'inf':
		KeyVal infG = new KeyVal(null, "?");
		File[] infGFiles = dir.listFiles(new FSI.FileFilterExact("stim"))[0].listFiles(new FSI.FileFilterExact("inf"));
		if (infGFiles.length > 0 && infGFiles[0].isFile()) {
			infG.digest(FSI.file2hash(infGFiles[0].getAbsolutePath(), ":"));
		}
		
		for (File s: stimuliDirs) {
			if (!s.isDirectory()) continue;
			
			globalStimCnt++;
			
			// (1) Local 'inf':
			KeyVal infL = new KeyVal(infG.get(), "?");
			File[] infLFiles = s.listFiles(new FSI.FileFilterExact("inf"));
			if (infLFiles.length > 0 && infLFiles[0].isFile()) {
				infL.digest(FSI.file2hash(infLFiles[0].getAbsolutePath(), ":"));
			}
			
			// (2) Init:
			String name = s.getName();
			String type = "u";
			procStim_updStim(DBI.ID_NONE, name, null);
			
			//res.append("\t\tStimulus: '" + name + "'");
			res.append("\t\t" + name + "\t");
			String infK = infL.getKeys("", ",");
			infK = infK.replaceAll("-", "_");
			PreparedStatement ps = stX.getConnection().prepareStatement(
				"INSERT INTO stim (exp_id, name, img, txt, memo, type" + (infK.length() > 0 ? ", " + infK : "") + ") " +
				"VALUES (" + expId + ", '" + name + "' , ?, ?, ?, ?" + (infK.length() > 0 ? ", " + infL.getValues("", ",") : "") + ")",
				Statement.RETURN_GENERATED_KEYS
			);
			
			// (3) Image:
			File[] imgFiles = s.listFiles(new FSI.FileFilterNameExt(name, new String[]{"gif","jpg","jpeg","png"}));
			FileInputStream fisImg = null;
			if (imgFiles.length > 0) {
				File img = imgFiles[0];
				fisImg = new FileInputStream(img);
				ps.setBinaryStream(1, fisImg, (int) img.length());
				//res.append("\t\t\tImage size: " + (img.length() / 1024) + "kB\n");
				res.append((img.length() / 1024) + "\t");
			}
			else {
				ps.setBinaryStream(1, null, 0);
				//res.append("\t\t\tImage size: 0kB\n");
				res.append("0\t");
			}
			
			// (4) Text:
			String txt = procStim_getTxt(name, s, dir);
			if (txt != null) {
				type = "t";
				ps.setString(2, txt);
				//res.append("\t\t\tText length: " + txt.length() + "\n");
				res.append(txt.length() + "\t");
			}
			else {
				type = "v";
				ps.setString(2, null);
				//res.append("\t\t\tText length: 0\n");
				res.append("0\t");
			}
			
			// (5) Memo:
			File[] memoFiles = s.listFiles(new FSI.FileFilterExact("memo"));
			FileInputStream fisMemo = null;
			if (memoFiles.length > 0) {
				File memo = memoFiles[0];
				fisMemo = new FileInputStream(memo);
				ps.setAsciiStream(3, fisMemo, (int)memo.length());
				//res.append("\t\t\tMemo length: " + memo.length() + "\n");
				res.append(memo.length() + "\t");
			}
			else {
				ps.setString(3, null);
				//res.append("\t\t\tMemo length: 0\n");
				res.append("0\t");
			}
			
			// (6) Type:
			//res.append("\t\t\tType: '" + type + "'\n");
			res.append(type + "\n");
			ps.setString(4, type);
			
			// (7) Finalize:
			ps.executeUpdate();
			
			ResultSet rsStimId = ps.getGeneratedKeys();
			rsStimId.next();
			int stimId = rsStimId.getInt(1);
			procStim_updStim(stimId, name, txt);
			
			if (fisImg != null) fisImg.close();
			ps.close();
			
			// (8) Process the ROIs file:
			int roiCnt = procStim_procROIs(stX, s, name);
			if (roiCnt > 0) {
				res.append("\t\t\tROIs: " + roiCnt + "\n");
				globalROICnt += roiCnt;
			}
		}
		res.append("\n");
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Updates the stimulus defined by the name. If it doesn't exists in the `htStim` hashtable yet, it is added.
	 */
	private void procStim_updStim(int id, String name, String txt) {
		name = name.trim();
		if (!htStim.containsKey(name)) {
			htStim.put(name, new Stim(id, name, txt));
		}
		else {
			Stim stim = htStim.get(name);
			stim.setId(id);
			stim.setName(name);
			if (txt != null) stim.setTxt(txt);
		}
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Retrieves the text of the specified stimulus. If the text file exists in the stimulus' folder its content is 
	 * returned. Otherwise, the global text file is checked. Returns null if neither can be found.
	 */
	private String procStim_getTxt(String name, File dirStim, File dirStims)  throws IOException {
		// Local file:
		File[] F = dirStim.listFiles(new FSI.FileFilterExact(name + ".txt"));
		if (F.length > 0) {
			Stim stim = htStim.get(name);
			stim.setTxt(FSI.getStr(F[0].getAbsolutePath()).toString());
			return stim.getTxt();
		}
		
		// Global file:
		if (htStim.containsKey(name)) return htStim.get(name).getTxt(); 
		
		return null;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Processes the 'rois' file of a stimulus.
	 */
	private int procStim_procROIs(Statement stX, File dir, String stimName)  throws SQLException, IOException, ValidException {
		File f = new File(dir.getAbsolutePath() + File.separatorChar + "rois");
		if (!f.exists()) return 0;
		
		int stimId = htStim.get(stimName).getId();
		List<String> R = FSI.file2lst(f.getAbsolutePath());
		app.gaze.Stim.addROIs(dbi, username, stX, R.toArray(new String[0]), stimId);
		
		return R.size();
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Processes the 'sub' directory by adding all subjects inside it to the DB.
	 */
	private StringBuffer procSub(Statement stX, File dir)  throws SQLException, IOException, ValidException {
		StringBuffer res = new StringBuffer("\tSUBJECTS\n");
		File[] dirs = dir.listFiles(new FSI.FileFilterExact("sub"))[0].listFiles();
		
		for (File s: dirs) {
			if (!s.isDirectory()) continue;
			
			globalSubCnt++;
			String name = s.getName();
			stX.executeUpdate("INSERT INTO sub (exp_id, name) VALUES (" + expId + ", '" + name + "')");
			//res.append("\t\tSubject: '" + name + "'\n");
			res.append("\t\t" + name + "\n");
		}
		
		res.append("\n");
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Processes the file of base trials for all trials. The base trial's (BT's) t0 will be subtracted from all t0 and 
	 * t1 of trials for which the BT trial has been defined as a base of. The specification should be contained in a 
	 * file 'base-trials' in the root directory of the import archive. Below is an example format of that file.
	 * 
	 * sub-a
	 *     trial-y    trial-x
	 *     trial-z    trial-x
	 *     trial-x    trial-x
	 * 
	 * The above specification declares 'trial-x' as a base for trials 'x', 'y', and 'z' for subject 'sub-a'. That is, 
	 * the base trial comes second. The tab character should be used to indent the trials and to separate trial names. 
	 * Note, that 'trial-x' is specified as it's own base at the very end. That is necessary. Otherwise, t0 of that 
	 * trial will be zero (0) and trials 'y' and 'z' will not be modified. It is up to the file creator to ensure the 
	 * proper order.
	 * 
	 * This function continues despite errors but returns the list of all offending lines. That makes it producing a 
	 * master-file and reusing it in "subset" imports a viable option.
	 */
	private StringBuffer procTrials_calcBase(Statement stX, File dir)  throws IOException, SQLException {
		StringBuffer resErr = new StringBuffer();  // error lines
		
		// (1) File exists:
		File[] F = dir.listFiles(new FSI.FileFilterExact("base-trials"));
		if (F.length > 0 && F[0].isFile()) {
			List<List<Long>> spec = new ArrayList<List<Long>>();  // each spec item: <ID of the to-be-altered trial, t0 of the base trial> 
			
			Integer subId = DBI.ID_NONE;
			
			// (1.1) Read the specification from the file:
			List<String> L = FSI.file2lst(F[0].getAbsolutePath());
			for (int i = 0; i < L.size(); i++) {
				String l = L.get(i);
				
				// (1.1.1) New subject:
				if (!l.startsWith("\t")) {
					subId = (int) DBI.getLong(stX, "SELECT id FROM sub WHERE exp_id = " + expId + " AND name = '" + l.trim() + "'", "id");
					if (subId == DBI.ID_NONE) resErr.append("\t\t\t\t" + "Line " + (i+1) + ": Subject not found\n");
				}
				
				// (1.1.2) New trial specification:
				else {
					if (subId == DBI.ID_NONE)  continue;
					
					String[] T = l.trim().split("\t");
					if (T.length != 2) {
						resErr.append("\t\t\t\t" + "Line " + (i+1) + ": More than two trials specified\n");
						continue;
					}
					
					Long tId01 = DBI.getLong(stX, "SELECT id FROM trial WHERE sub_id = " + subId + " AND name = '" + T[0] + "'", "id");
					Long tId02 = DBI.getLong(stX, "SELECT id FROM trial WHERE sub_id = " + subId + " AND name = '" + T[1] + "'", "id");
					
					if (tId01 == DBI.ID_NONE && tId02 == DBI.ID_NONE) {
						resErr.append("\t\t\t\t" + "Line " + (i+1) + ": Neither of the trials found\n");
						continue;
					}
					else if (tId01 == DBI.ID_NONE) {
						resErr.append("\t\t\t\t" + "Line " + (i+1) + ": The first trial not found\n");
						continue;
					}
					else if (tId02 == DBI.ID_NONE) {
						resErr.append("\t\t\t\t" + "Line " + (i+1) + ": The second trial not found\n");
						continue;
					}
					
					Long t0 = DBI.getLong(stX, "SELECT t0 FROM trial WHERE sub_id = " + subId + " AND name = '" + T[1] + "'", "t0");
					
					spec.add(Arrays.asList(tId01, t0));
				}
			}
			
			// (1.2) Imprint the specification onto the data structures:
			Iterator<List<Long>> it = spec.iterator();
			while (it.hasNext()) {
				List<Long> lst = it.next();
				stX.executeUpdate("UPDATE trial SET t0 = t0 - " + lst.get(1) + ", t1 = t1 - " + lst.get(1) + " WHERE id = " + lst.get(0));
			}
			
			return new StringBuffer("\t\t").append("OTHER: TRIALS BASE\n").append("\t\t\t").append("Result: File found and processed\n\t\t\tErrors\n").append(resErr);
		}
		
		// (2) File not found:
		return new StringBuffer("\t\t" + "OTHER: TRIALS BASE\n" + "\t\t\t" + "Result: File not found\n");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Processes a trial assuming the data conforms to the Eye Link 2 format. This method is called for each subject, 
	 * once per trial.
	 */
	private StringBuffer procTrialEL2(Statement stX, String path, int subId, String subName, int trialId)  throws IOException, SQLException, PegasusException {
		StringBuffer res = new StringBuffer();
		
		List<String> ss = new ArrayList<String>();  // stimulus sequence
		List<String> em = null;  // list of eye movement file lines for line-by-line processing (EDF ASCII)
		try {
			em = FSI.file2lstRegExpr(path + File.separatorChar + subName + ".asc", PATTERN_EYELINK_SUB_FILE);
		}
		catch (java.io.FileNotFoundException ex) {
			return new StringBuffer("\t\t\t\tFile not found; skipping\n");
		}
		
		int trialT0    = -1;
		int trialT1    = -1;
		int currStimT0 = 0;
		
		int stimFixCnt   = 0;
		int stimBlinkCnt = 0;
		int stimEvtCnt   = 0;
		
		int trialFixCnt   = 0;
		int trialBlinkCnt = 0;
		int trialEvtCnt   = 0;
		
		int currSSIdx  = 0;  // index of the current stimulus in the 'ss' list
		int currSSId   = DBI.ID_NONE;
		int currStimId = DBI.ID_NONE;
		
		int stimChangeOffset = 0;  // 0: no change, gt 0: forward, lt 0: backwards
		
		boolean hasStimChanged  = false;  // flag: has the stimulus changed and the name of the new one should be determined before any processing can commence?
		boolean hasTrialStarted = true;   // flag: has a trial started? (a trial can be started and stopped through messages and after a trial is stopped and before another is started again no eye movements will be retained)
		boolean isProbeShown    = false;  // flag: is probe currently being displayed? no eye-movements are read during prove presentation
		
		int blinkT0  = 0;
		int blinkDur = 0;
		
		int evtPromptT = -1;
		
		Cal currCal = null;
		
		Dimension scrDim = new Dimension(0, 0);
		
		PreparedStatement psFix = ps.get("fix");
		
		Iterator<String> it = em.iterator();
		while (it.hasNext()) {
			String l = it.next();
			
			// (1) Messages:
			if (l.startsWith("MSG")) {  // MSG <time> <content>
				Matcher mMsg = RegExpr.match("MSG[\\s]+([\\d]+)[\\s]*(.*)", l);
				int t = (new Integer(mMsg.group(1))).intValue();  // time [ms]
				int dt = t - trialT0;   // time from the beginning of the trial
				String c = mMsg.group(2);  // message content
				
				// (1.a) Calibration:
				if (c.startsWith("!CAL")) {  // because this `if` iterates through the file lines, no `else` should be put after it
					if (currCal != null) currCal.upd(stX, dt);
					
					currCal = new Cal(trialId, scrDim, (trialT0 == -1 ? -1 : dt));
					
					while (!currCal.hasAllPoints() && it.hasNext() && (c.startsWith("!CAL") || c.startsWith("VALIDATE"))) {
						Matcher mCal;
						
						// (1.a.i) Calibration header:
						// e.g.: MSG 4525978 !CAL CALIBRATION HV9 R RIGHT   GOOD
					    //                                      - -         ----
						// $:                                   1 2         3
						mCal = PATTERN_EYELINK_CAL_CAL_HEAD.matcher(l);
						if (mCal.find()) {
							currCal.pointCnt = (new Integer(mCal.group(1))).intValue();
							currCal.eye = mCal.group(2).toUpperCase().charAt(0);
							currCal.calRes = mCal.group(3).toLowerCase();
						}
						
						// (1.a.ii) Validation header:
						// e.g.: MSG 4543167 !CAL VALIDATION HV9 R RIGHT POOR ERROR 3.06 avg. 23.10 max  OFFSET 2.09 deg. 85.1,-6.3 pix.
						//                                     - -       ----       ----      -----             ----      ---- ----
						// $:                                  1 2       3          4         5                 6         7    8
						mCal = PATTERN_EYELINK_CAL_VAL_HEAD.matcher(l);
						if (mCal.find()) {
							currCal.resetPoints();
							currCal.pointCnt = (new Integer(mCal.group(1))).intValue();
							currCal.valRes = mCal.group(3).toLowerCase();
							currCal.valErrAvg = (new Float(mCal.group(4))).floatValue();
							currCal.valErrMax = (new Float(mCal.group(5))).floatValue();
						}
						
						// (1.a.iii) Validation point:
						// e.g.: MSG 4543167 VALIDATE R 4POINT 5 RIGHT  at 800,600  OFFSET 0.96 deg.  -25.7,29.1 pix.
						//                            -        -           --- ---         ----       ----- ----
						// $:                         1        2           3   4           5          6     7
						mCal = PATTERN_EYELINK_CAL_VAL_POINT.matcher(l);
						if (mCal.find()) {
							currCal.addPoint(
								(new Integer(mCal.group(2))).intValue(),
								(new Integer(mCal.group(3))).intValue(),
								(new Integer(mCal.group(4))).intValue(),
								(new Float  (mCal.group(6))).floatValue(),
								(new Float  (mCal.group(7))).floatValue(),
								(new Float  (mCal.group(5))).floatValue()
							);
						}
						
						if (it.hasNext()) l = it.next();
						
						mMsg = RegExpr.match("MSG[\\s]+([\\d]+)[\\s]*(.*)", l);  // copied from above (probably not the nicest way of doing this, but what the heck)
						t = (new Integer(mMsg.group(1))).intValue();
						dt = t - trialT0;
						c = mMsg.group(2);
					}
					
					if (currCal.hasAllPoints()) {  // makes sure that all points have been read
						res.append("\t\t\t\tCalibration (points: " + currCal.pointCnt + ", cal-res: '" + currCal.calRes + "', val-res: '" + currCal.valRes + "', err-avg: " + currCal.valErrAvg + " deg, err-max: " + currCal.valErrMax + " deg)\n");
						if (currCal.isTrialBound()) {
							currCal.persist(stX);
							currCal = null;
						}
					}
					else {
						res.append("\t\t\t\tCalibration (incomplete points list; disregarding)\n");
						currCal = null;
					}
				}
				
				// (1.b) Screen size:
				if (c.startsWith("DISPLAY_COORDS")) {
					Matcher mScrSize = RegExpr.match("DISPLAY_COORDS[\\s]+0[\\s]+0[\\s]+([\\d]+)[\\s]+([\\d]+)", c);
					scrDim.width  = (new Integer(mScrSize.group(1))).intValue() + 1;
					scrDim.height = (new Integer(mScrSize.group(2))).intValue() + 1;
				}
				
				// (1.c) Trial -- Start:
				if (c.startsWith("Trial start")) {
					trialT0 = t;
					
					trialFixCnt   = 0;
					trialBlinkCnt = 0;
					trialEvtCnt   = 0;
					
					hasTrialStarted = true;
					
					Matcher m = RegExpr.match("Trial start: (.+)", c);
					String trialName = m.group(1).trim().toLowerCase();
					
					globalTrialCnt++;
					trialId = DBI.insertGetId(stX,
						"SELECT id FROM trial WHERE sub_id = " + subId + " AND name = '" + trialName + "'",
						"INSERT INTO trial (sub_id, name, memo, dur) VALUES (" + subId + ", '" + trialName + "', '', null)"
					);
					
					if (currCal != null) {
						currCal.trialId = trialId;
						currCal.persist(stX);
					}
					
					res.append("\t\t\tTRIAL: " + trialName + " (message)\n\t\t\t\tStimulus\tFixations\tBlinks\tEvents\n");
					//System.out.println("TS " + t + " " + trialId + " " + trialName);
				}
				
				// (1.d) Trial -- End:
				if (c.startsWith("Trial end")) {
					trialT1 = t;
					
					if (currCal != null) currCal.upd(stX, trialT1);
					stX.executeUpdate("UPDATE trial SET t0 = " + trialT0 + ", t1 = " + trialT1 + ", dur = " + (trialT1 - trialT0) + " WHERE id = " + trialId);
					
					hasTrialStarted = false;
					
					/*
					res.append(
						"\t\t\t\tSummary\n" +
						"\t\t\t\t\tFixations: " + trialFixCnt   + "\n" +
						"\t\t\t\t\tBlinks: "    + trialBlinkCnt + "\n" +
						"\t\t\t\t\tEvents: "    + trialEvtCnt   + "\n"
					);
					*/
					res.append("\t\t\t\tTotal\t" + trialFixCnt + "\t" + trialBlinkCnt + "\t" + trialEvtCnt + "\n");
					
					//System.out.println("TE " + t + " " + trialId);
					trialId = DBI.ID_NONE;
				}
				
				// (1.e) Stimulus navigation -- Next:
				if (c.equalsIgnoreCase("Stim nav: Next") || c.equalsIgnoreCase("ADVANCE")) {  // TODO: remove "ADVANCE" (it was specific to Reichle et. al 2010 experiment)
					//stimChangeOffset = (stimChangeOffset <= 0 ? 1 : stimChangeOffset + 1);
					hasStimChanged = true;
				}
				
				// (1.f) Stimulus navigation -- Previous:
				else if (c.equalsIgnoreCase("Stim nav: Prev") || c.equalsIgnoreCase("GO_BACK")) {  // TODO: remove "GO_BACK" (it was specific to Reichle et al. 2010 experiment)
					//stimChangeOffset = (stimChangeOffset >= 0 ? -1 : stimChangeOffset - 1);
					hasStimChanged = true;
				}
				
				// (1.g) Stimulus display:
				// MSG 102006694 !V IMGLOAD TOP_LEFT ../../library/images/C1P01.png 0 0
				// MSG	6716822 Stim display: Question (C01P03Q01)
				else if ((c.startsWith("Stim display") || c.startsWith("Stim load") || c.indexOf("IMGLOAD") != -1) && hasStimChanged) {  // TODO: remove "IMGLOAD" (it was specific to Reichle et al. 2010 experiment)
					if (currCal != null) currCal.stimCnt++;
					
					// If moving forward to the next stimulus, decode it's name and store in the stim-seq list:
					if (false && stimChangeOffset >= 0) {
						if (c.indexOf("Stim display") != -1) {
							Matcher mStimLoad = RegExpr.match("Stim display: (Text|Question|Question EOC) [(](.+)[)].*", c);
							String name = mStimLoad.group(2).trim().toLowerCase();
							if (!ss.contains(name)) ss.add(name);
						}
						else if (c.indexOf("Stim load") != -1) {  // TODO: remove after fixing the experiment
							Matcher mStimLoad = RegExpr.match("Stim load: (Text|Question||Question EOC) [(](.+)[)].*", c);
							String name = mStimLoad.group(2).trim().toLowerCase();
							if (!ss.contains(name)) ss.add(name);
						}
						else if (c.indexOf("IMGLOAD") != -1) {
							Matcher mStimLoad = RegExpr.match(".*IMGLOAD\\s*\\w*\\s*.*/(.+).png\\s*.*", c);
							String name = mStimLoad.group(1).trim().toLowerCase();
							if (!ss.contains(name)) ss.add(name);
						}
					}
					
					String stimName = null;
					if (c.indexOf("Stim display") != -1) {
						Matcher mStimLoad = RegExpr.match("Stim display: (Text|Question|Question EOC) [(](.+)[)].*", c);
						stimName = mStimLoad.group(2).trim().toLowerCase();
						ss.add(stimName);
					}
					else if (c.indexOf("Stim load") != -1) {  // TODO: remove after fixing the experiment
						Matcher mStimLoad = RegExpr.match("Stim load: (Text|Question|Question EOC) [(](.+)[)].*", c);
						stimName = mStimLoad.group(2).trim().toLowerCase();
						ss.add(stimName);
					}
					else if (c.indexOf("IMGLOAD") != -1) {
						Matcher mStimLoad = RegExpr.match(".*IMGLOAD\\s*\\w*\\s*.*/(.+).png\\s*.*", c);
						stimName = mStimLoad.group(1).trim().toLowerCase();
						ss.add(stimName);
					}

					// Update the current stimulus' index in the stim seq list:
					/*
					currSSIdx += stimChangeOffset;
					if (currSSIdx >= ss.size()) currSSIdx -= ss.size();  // connect end of stimuli to the beginning (i.e., the very first is after the very last)
					stimChangeOffset = 0;
					String stimName = ss.get(currSSIdx);
					*/
					
					// Retrieve the current stimulus from the stim hash:
					Stim currStim = htStim.get(stimName);
					if (currStim == null) {  // stimulus encountered that does not exists in the 'stim' dir of the import archive -- select one the name of which matches, case-insensitive (if exists or course)
						Iterator<String> it2 = htStim.keySet().iterator();
						while (it2.hasNext()) {
							String s = it2.next();
							if (stimName.equalsIgnoreCase(s)) {
								currStim = htStim.get(s);
								warnings.append("\tSubject '" + subName + "': stimulus '" + stimName + "' found in the EM recordings, but not in the import file; using '" + s + "'\n");
								warningCnt++;
							}
						}
					}
					if (currStim == null) {  // stimulus encountered in the EM file was not found in the import archive -- terminate the import routine
						throw new PegasusException("The import routine has encountered a stimulus ('" + stimName + "') that appears in the eye movement recordings, but has not been provided in the 'stim' directory of the import archive file. Please create an appropriate stimulus and try again. All names are case-sensitive. Import will not continue. No changes to the active experiment have been made.");
					}
					
					currStimId = currStim.getId();
					currSSId = DBI.insertGetId(stX, null,
						"INSERT INTO stim_seq (trial_id, stim_id, cal_id, t0) " +
						"VALUES (" + trialId + ", " + currStimId + ", " + (currCal == null ? "null" : currCal.id) + ", " + (currStimT0 - trialT0) + ")"
					);
					res.append("\t\t\t\t" + stimName + "\t");
					
					hasStimChanged = false;
					
					//System.out.println("   " + t + " " + currStim.getName());
				}
				
				// (1.f) Probe displayed:
				else if (c.indexOf("Evt: Probe display") != -1 || c.indexOf("DISPLAY_QUESTION") != -1) {  // remove "DISPLAY_QUESTION" (it was specific to Reichle et al. 2010 experiment)
					isProbeShown = true;
					evtPromptT   = dt;
				}
				
				// (1.g) Probe-response event:
				else if (evtLst.exists(c)) {
					evtCnt      ++;
					trialEvtCnt ++;
					stimEvtCnt  ++;
					
					if (currStimId != DBI.ID_NONE) evtLst.persist(stX, c, trialId, currStimId, currSSId, (evtPromptT != -1 ? evtPromptT : dt), dt);
					
					evtPromptT = -1;
					isProbeShown = false;
				}
				
				continue;
			}
			
			// (2) Stimulus start (starting here and until the actual stimulus has been displayed no eye movements will be imported):
			if (l.startsWith("START")) {  // START <time> <eyes> <types>
				Matcher m = RegExpr.match("START[\\s]+([\\d]+)[\\s]*", l);
				
				if (trialT0 == -1) trialT0 = Integer.parseInt(m.group(1));
				currStimT0 = Integer.parseInt(m.group(1));
				hasStimChanged = true;
				
				continue;
			}
			
			// (3) Stimulus end:
			if (l.startsWith("END")) {  // END <time> <types> RES <xres> <yres>
				Matcher m = RegExpr.match("END[\\s]+([\\d]+)[\\s]*", l);
				
				trialT1 = Integer.parseInt(m.group(1));
				
				stX.executeUpdate("UPDATE stim_seq SET t1 = " + (trialT1 - trialT0) + ", fix_cnt = " + stimFixCnt + ", blink_cnt = " + stimBlinkCnt + " WHERE id = " + currSSId);
				
				/*
				res.append(
					"\t\t\t\t\tFixations: " + stimFixCnt   + "\n" +
					"\t\t\t\t\tBlinks: "    + stimBlinkCnt + "\n" +
					"\t\t\t\t\tEvents: "    + stimEvtCnt   + "\n"
				);
				*/
				res.append(stimFixCnt + "\t" + stimBlinkCnt + "\t" + stimEvtCnt + "\n");
					
				stimFixCnt   = 0;
				stimBlinkCnt = 0;
				stimEvtCnt   = 0;
				currStimId   = DBI.ID_NONE;
				
				hasStimChanged = true;
				
				continue;
			}
			
			// (4) Fixation:
			if (hasTrialStarted && currStimId != DBI.ID_NONE && !isProbeShown && l.startsWith("EFIX") && !hasStimChanged) {  // EFIX <eye> <stime> <etime> <dur> <axp> <ayp> <aps>
				Matcher m = RegExpr.match("EFIX[\\s]+[LR][\\s]+([\\d]+)[\\s]+([\\d]+)[\\s]+([\\d]+)[\\s]+([-\\d.e\\+]+)[\\s]+([-\\d.e\\+]+)[\\s]+([\\d]+)", l);
				
				fixCnt++;
				trialFixCnt++;
				stimFixCnt++;
				
				try {
					int t0 = Integer.parseInt(m.group(1));        // timestamp of first sample [ms]
					//int t1 = Integer.parseInt(m.group(2));        // timestamp of last sample [ms]
					int dur = Integer.parseInt(m.group(3));       // duration [ms]
					float avgX = Float.parseFloat(m.group(4));    // average X position [pixel]
					float avgY = Float.parseFloat(m.group(5));    // average Y position [pixel]
					int avgPupil = Integer.parseInt(m.group(6));  // average pupil size (area or diameter) [?]
					
					int dt = t0 - trialT0;  // time from the beginning of the trial
					if (dt > 0) {
						// stX.executeUpdate("INSERT INTO fix (trial_id, stim_id, stim_seq_id, num, t, dur, x, y, pupil) VALUES (" + trialId + ", " + currStimId + ", " + currSSId + ", " + fixCnt + ", " + dt + ", " + dur + ", " + avgX + ", " + avgY + ", " + avgPupil + ")");
						psFix.setInt(1, trialId);
						psFix.setInt(2, currStimId);
						psFix.setInt(3, currSSId);
						psFix.setInt(4, fixCnt);
						psFix.setInt(5, dt);
						psFix.setInt(6, dur);
						psFix.setFloat(7, avgX);
						psFix.setFloat(8, avgY);
						psFix.setInt(9, avgPupil);
						psFix.executeUpdate();
					}
				}
				catch (NumberFormatException e) {}
				
				continue;
			}
			
			// (5) Saccade (blink end):
			if (hasTrialStarted && currStimId != DBI.ID_NONE && !isProbeShown && l.startsWith("ESACC") && blinkDur > 0 && !hasStimChanged) {  // ESACC <eye> <stime> <etime> <dur> <sxp> <syp> <exp> <eyp> <ampl> <pv>
				Matcher m = RegExpr.match("ESACC[\\s]+[LR][\\s]+([\\d]+)[\\s]+([\\d]+)[\\s]+([\\d]+)[\\s]+([-\\d.e\\+]+)[\\s]+([-\\d.e\\+]+)[\\s]+([-\\d.e\\+]+)[\\s]+([-\\d.e\\+]+)[\\s]+([\\d.e\\+]+)[\\s]+([\\d]+)", l);
				
				blinkCnt++;
				trialBlinkCnt++;
				stimBlinkCnt++;
				
				try {
					//int t0 = Integer.parseInt(m.group(1));           // timestamp of first sample [ms]
					//int t1 = Integer.parseInt(m.group(2));           // timestamp of last sample [ms]
					int   saccDur = Integer. parseInt   (m.group(3));  // duration [ms]
					float sx      = Float.   parseFloat (m.group(4));  // start X position
					float sy      = Float.   parseFloat (m.group(5));  // start Y position
					float ex      = Float.   parseFloat (m.group(6));  // end X position
					float ey      = Float.   parseFloat (m.group(7));  // end Y position
					float saccAmp = Float.   parseFloat (m.group(8));  // saccadic amplitude [deg]
					int   saccPV  = Integer. parseInt   (m.group(9));  // peak velocity [degrees/sec]
					
					int dt = blinkT0 - trialT0;  // time from the beginning of the trial
					if (dt > 0) {
						stX.executeUpdate(
							"INSERT INTO blink (trial_id, stim_id, stim_seq_id, sx, sy, ex, ey, dur, sacc_dur, sacc_amp, sacc_pv, t) " +
							"VALUES (" + trialId + ", " + currStimId + ", " + currSSId + ", " + sx + ", " + sy + ", " + ex + ", " + ey + ", " + blinkDur + ", " + saccDur + ", " + saccAmp + "," + saccPV + ", " + dt + ")"
						);
					}
				}
				catch (NumberFormatException e) {}
				
				blinkDur = 0;
				
				continue;
			}
			
			// (6) Blink:
			if (hasTrialStarted && currStimId != DBI.ID_NONE && !isProbeShown && l.startsWith("EBLINK") && !hasStimChanged) {  // EBLINK <eye> <stime> <etime> <dur>
				Matcher m = RegExpr.match("EBLINK[\\s]+[LR][\\s]+([\\d]+)[\\s]+([\\d]+)[\\s]+([\\d]+)", l);
				
				try {
					int t0  = Integer.parseInt(m.group(1));   // timestamp of first sample [ms]
					//int t1 = Integer.parseInt(m.group(2));   // timestamp of last sample [ms]
					int dur = Integer.parseInt(m.group(3));   // duration [ms]
					
					if (blinkDur == 0) blinkT0 = t0;
					blinkDur += dur;
				}
				catch (NumberFormatException e) {}
				
				continue;
			}
		}
		
		if (currCal != null) currCal.upd(stX, trialT1);
		stX.executeUpdate("UPDATE trial SET t0 = " + trialT0 + ", t1 = " + trialT1 + ", dur = " + (trialT1 - trialT0) + " WHERE id = " + trialId);
		
		res.append("\t\t\t\tTOTAL\t" + trialFixCnt + "\t" + trialBlinkCnt + "\t" + trialEvtCnt + "\n");
		//System.out.println("TE ....... " + trialId + "\n");
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Processes a trial assuming the data conforms to the Tobii format. This method is called for each subject, once 
	 * per trial.
	 */
	private StringBuffer procTrialTobii(Statement stX, String pathTrial, String subName, int trialId)  throws IOException, SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		StringBuffer res = new StringBuffer();
		String pathSub = pathTrial + File.separatorChar + subName + File.separatorChar;
		
		// (1) Decode the transformations file:
		Hashtable<Integer,XForm[]> htXForm = new Hashtable<Integer,XForm[]>();
		try {
			if (new File(pathSub + "xform").exists()) {
				List<String> L = FSI.file2lst(pathSub + "xform");
				Iterator<String> it = L.iterator();
				while (it.hasNext()) {
					// line format: '5 tx:0,ty:-47,sx:1.0,sy:1.0,a:0.0|tx:-10,ty:0,sx:1.0,sy:1.0,a:0.0'
					String l = it.next();
					
					int idx = (new Integer(l.substring(0, l.indexOf(" ")))).intValue() - 1;
					String[] XFS = l.substring(l.indexOf(" ") + 1).split("\\|");  // XForm String
					
					XForm[] XF = new XForm[XFS.length];
					for (int i = 0; i < XFS.length; i++) {
						XF[i] = new XForm(XFS[i]);
					}
					
					htXForm.put(idx, XF);
				}
			}
		}
		catch (NumberFormatException c) {}
		
		// (2) Decode the stimuli sequence file:
		List<StimSeq> ss = new ArrayList<StimSeq>();
		List<String> L = FSI.file2lst(pathSub + "stim-seq.txt");
		for (String l: L) {
			Matcher m = RegExpr.match("^\\s*(.+)\\s+(\\d+),(\\d+)\\s*$", l);
			ss.add(new StimSeq(m.group(1), new Integer(m.group(2)), new Integer(m.group(3))));
		}
		
		// (3) Import the EM data:
		boolean ssFinalized = false;
		
		int t0 = -1;
		int t1 = -1;
		
		int ssFixCnt = 0;
		
		int currSSIdx  = -1;  // an idx of the current stimulus in the 'ss' list
		int currSSId   = DBI.ID_NONE;
		int currStimId = DBI.ID_NONE;
		
		PreparedStatement psFix = ps.get("fix");
		
		List<String[]> em = FSI.file2lstSplit(pathSub + "FXD.txt", "\t", 20);
		Iterator<String[]> it = em.iterator();
		while (it.hasNext()) {
			String[] f = it.next();
			int t = new Integer(f[1]);
			int dur = new Integer(f[2]);
			int x = new Integer(f[3]);
			int y = new Integer(f[4]);
			
			// Check stimuli sequence:
			while (currSSIdx < ss.size()-1 && (currSSIdx == -1 || t > ss.get(currSSIdx).t1)) {
				if (currSSIdx != -1) {
					stX.executeUpdate("UPDATE stim_seq SET fix_cnt = " + ssFixCnt + " WHERE id = " + currSSId);
					res.append("\t\t\t\t\tNumber of fixations extracted: " + ssFixCnt + "\n");
					
					XForm[] XF = htXForm.get(currSSIdx);
					if (XF != null && XF.length > 0) {
						for (int i = 0; i < XF.length; i++) {
							XForm xf = XF[i];
							StimSeq.xform(dbi, stX, username, currSSId, xf.getTX(), xf.getTY(), xf.getSX(), xf.getSY(), xf.getA());
							res.append("\t\t\t\t\tTransformation applied: tx=" + xf.getTX() + ", ty=" + xf.getTY() + ", sx=" + xf.getSX() + ", sy=" + xf.getSY() + ", a=" + xf.getA() + "\n");
						}
					}
					ssFinalized = true;
				}
				
				ssFinalized = false;
				currSSIdx++;
				ssFixCnt = 0;
				t0 = ss.get(currSSIdx).t0;
				t1 = ss.get(currSSIdx).t1;
				
				String stimName = ss.get(currSSIdx).stim;
				currStimId = (int) DBI.getLong(stX, "SELECT id FROM stim WHERE exp_id = " + expId + " AND name = '" + stimName + "'", "id");
				currSSId = DBI.insertGetId(stX, null, "INSERT INTO stim_seq (trial_id, stim_id, t0, t1, blink_cnt) VALUES (" + trialId + ", " + currStimId + ", " + t0 + ", " + t1 + ", 0)");
				res.append("\t\t\t\tAssociating EM data with stimulus: '" + stimName + "'\n");
			}
			if (t > t1) continue;  // discard fixations past the last stim-seq entry
			
			// Persist:
			psFix.setInt(1, trialId);
			psFix.setInt(2, currStimId);
			psFix.setInt(3, currSSId);
			psFix.setInt(4, fixCnt);  // TODO: should that be a fixation number within the trial or stim-seq? now it's the former
			psFix.setInt(5, t);
			psFix.setInt(6, dur);
			psFix.setFloat(7, x);
			psFix.setFloat(8, y);
			psFix.setInt(9, 0);  // TODO: extract pupil info
			psFix.executeUpdate();
			
			fixCnt++;
			ssFixCnt++;
		}
		
		if (!ssFinalized) {
			stX.executeUpdate("UPDATE stim_seq SET fix_cnt = " + ssFixCnt + " WHERE id = " + currSSId);
			res.append("\t\t\t\t\tNumber of fixations extracted: " + ssFixCnt + "\n");
			
			XForm[] XF = htXForm.get(currSSIdx);
			if (XF != null && XF.length > 0) {
				for (int i = 0; i < XF.length; i++) {
					XForm xf = XF[i];
					StimSeq.xform(dbi, stX, username, currSSId, xf.getTX(), xf.getTY(), xf.getSX(), xf.getSY(), xf.getA());
					res.append("\t\t\t\t\tTransformation applied: tx=" + xf.getTX() + ", ty=" + xf.getTY() + ", sx=" + xf.getSX() + ", sy=" + xf.getSY() + ", a=" + xf.getA() + "\n");
				}
			}
			ssFinalized = true;
		}
		
		stX.executeUpdate("UPDATE trial SET dur = " + (ss.get(ss.size()-1).t1 - ss.get(0).t0) + " WHERE id = " + trialId);
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Processes the 'var' directory by adding all variables inside it to the DB. 
	 */
	private StringBuffer procVar(Statement stX, File dir)  throws IOException, SQLException {
		StringBuffer res = new StringBuffer("\tVARIABLES\n");
		
		String[] O = { "roi","stim","sub","trial" };  // objects
		String[] OH = { "ROIs","Stimuli","Subjects","Trials" };  // objects human-readible
		
		for (int i = 0; i < O.length; i++) {
			String o = O[i];
			res.append("\t\t" + OH[i] + "\n");
			File dirVars = new File(dir.getAbsolutePath() + File.separatorChar + "var" + File.separatorChar + o);
			if (!dirVars.exists()) continue;
			
			File[] V = dirVars.listFiles();
			for (File v: V) {
				if (v.getName().startsWith(".")) continue;  // skip hidden files (especially the fucking .DS_Store file created by fucking Mac OS)
				
				res.append("\t\t\t" + v.getName() + "\n");
				String type = FSI.getStr(v.getAbsolutePath() + File.separatorChar + "type").toString();
				String memo = FSI.getStr(v.getAbsolutePath() + File.separatorChar + "memo").toString();
				String defVal = (FSI.getStr(v.getAbsolutePath() + File.separatorChar + "def-value").toString());
				if (type != null) {
					DBI.insertGetId(stX, "SELECT id FROM var WHERE exp_id = " + expId + " AND name = '" + v.getName() + "'", "INSERT INTO var (obj_name, memo, exp_id, name, sys, type, def_val) VALUES ('" + o + "', " + (memo == null ? "''" : DBI.esc(memo)) + ", " + expId + ", '" + v.getName() + "', 0, '" + type + "'," + (defVal == null ? "null" : "'" + defVal + "'") + ")");
				}
				else res.append(" not added (the required 'type' file not found)\n");
			}
		}
		
		res.append("\n");
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Starts the entire import routine.
	 */
	public StringBuffer run()  throws IOException, SQLException, ValidException, InstantiationException, IllegalAccessException, ClassNotFoundException, RserveException, REXPMismatchException, PegasusException {
		if (!(new File(filename)).exists()) return new StringBuffer("{outcome:false,msg:\"File could not be found. If you are certain the file exists make sure access rights do not prevent the access.\"}");
		
		util.Timer timer = new util.Timer();
		StringBuffer res = new StringBuffer();
		StringBuffer rep = new StringBuffer("START\n\n");  // report
		
		Statement stX = new DBI().transBegin();
		stX.execute("SET search_path TO " + db);
		
		try {
			// (1) Prepare:
			ps.put("fix", stX.getConnection().prepareStatement("INSERT INTO fix (trial_id, stim_id, stim_seq_id, num, t, dur, x, y, pupil) VALUES (?,?,?,?,?,?,?,?,?)"));
			
			// (2) Process the directory:
			util.Timer timerUnzip = new util.Timer();
			File dir = FSI.unzip(filename);
			String unzipTime = timerUnzip.elapsedHuman();
			int format = identifyFormat(dir);
			rep.append(procDir(stX, format, dir));
			
			FSI.delDir(dir, true);
			rep.append(
				"\tUnzipping time: " + unzipTime + "\n" +
				"\t\n" +
				"\tWarnings: " + warningCnt + "\n\n"
			);
			
			// (3) Store the import report:
			if (format != FORMAT_UNKNOWN) {
				rep.append("END");
				stX.executeUpdate("UPDATE exp SET linked = FALSE, imp_ts = CURRENT_TIMESTAMP, imp_rep = " + DBI.esc(rep.toString()) + " WHERE id = " + expId);
			}
			else res.append("{outcome:false,msg:\"File format was not recognized. Make sure the ZIP file you requested to be imported conforms to the format requirements.\"}");
			
			DBI.transCommit(stX, true);
		}
		catch (SQLException e)   { DBI.transRollback(stX, true); throw e; }
		catch (IOException e)    { DBI.transRollback(stX, true); throw e; }
		catch (ValidException e) { DBI.transRollback(stX, true); throw e; }
		catch (InstantiationException e) { DBI.transRollback(stX, true); throw e; }
		catch (IllegalAccessException e) { DBI.transRollback(stX, true); throw e; }
		catch (ClassNotFoundException e) { DBI.transRollback(stX, true); throw e; }
		
		res.append("{outcome:true,exp:" + Exp.get(dbi, username, expId) + ",t:" + timer.elapsed() + "}");
		
		return res;
	}
}
