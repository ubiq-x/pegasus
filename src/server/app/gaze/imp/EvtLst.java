package app.gaze.imp;

import inter.DBI;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;


/**
 * @author Tomek D. Loboda
 */
public class EvtLst {
	private final Map<String, Long> oldName2id = new HashMap<String, Long>();
	private final Map<String, String> oldName2newName = new HashMap<String, String>();  // new names should be escaped on addition to be ready to use right away
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public EvtLst() {}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Add to the list.
	 */
	public void add(Statement stX, int expId, String oldName, String newName, String memo)  throws SQLException {
		long id = DBI.insertGetId(stX, "SELECT id FROM evt_lst WHERE exp_id = " + expId + " AND name = " + DBI.esc(newName), "INSERT INTO evt_lst (exp_id, name, memo) VALUES (" + expId + ", " + DBI.esc(newName) + ", " + DBI.esc(memo) + ")");
		if (!oldName2id.containsKey(oldName)) {
			oldName2id.put(oldName, id);
			oldName2newName.put(oldName, DBI.esc(newName));
		}
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public boolean exists(String oldName) { return oldName2id.containsKey(oldName); }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Persist in the DB an event for a given stimulus sequence entry.
	 */
	public void persist(Statement stX, String oldName, int trialId, int stimId, int stimSeqId, long tPrompt, long tResp)  throws SQLException {
		stX.executeUpdate("INSERT INTO evt (evt_lst_id, trial_id, stim_id, stim_seq_id, t_prompt, t_resp) VALUES (" + oldName2id.get(oldName) + ", " + trialId + ", " + stimId + ", " + stimSeqId + ", " + tPrompt + ", " + tResp + ")");
	}
}
