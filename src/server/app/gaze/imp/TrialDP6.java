package app.gaze.imp;

import inter.DBI;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;


/**
 * Stimulus for the Image Generation VI eye tracker.
 */
public class TrialDP6 {
	private int subId     = DBI.ID_NONE;
	private int trialId   = DBI.ID_NONE;
	private int stimId    = DBI.ID_NONE;
	private int stimSeqId = DBI.ID_NONE;
	
	private final Hashtable<String, Stim> htStim;
	
	private final int trial;
	private final int item;
	private final int cond;
	private final String stimName;
	
	private String txt = null;  // entire text of the stimulus
	
	private final List<String> L = new ArrayList<String>();  // lines (undecoded)
	private final List<ROI>    R = new ArrayList<ROI>();
	
	private int fixCnt = 0;
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public TrialDP6(Hashtable<String, Stim> htStim, int subId, int trial, int item, int cond) {
		this.subId = subId;
		
		this.htStim = htStim;
		
		this.trial = trial;
		this.item  = item;
		this.cond  = cond;
		
		stimName = item + "." + cond;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public void addLine(String l) { L.add(l); }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Decodes the info stored in lines. Should be called only after all the lines has been added. 
	 */
	public void decode() {
		Hashtable<Integer, Fix> F = new Hashtable<Integer, Fix>();  // for accessing all fixations in a sequential manner to determine the saccade amplitude
		txt = "";
		
		for (int i = 0; i < L.size(); i++) {
			String[] X = L.get(i).split("\\s+");
			
			// ROI:
			X[0] = " " + X[0];
			txt += X[0];
			ROI r = new ROI(i+1, X[0], (i == 0 ? X[0].length() - 1 : X[0].length()));
			R.add(r);
			
			// Extract fixations:
			for (int j = 0; j < (X.length - 1) / 3; j++) {
				int num = new Integer(X[j*3 + 1]);
				int let = new Integer(X[j*3 + 2]);
				int dur = new Integer(X[j*3 + 3]);
				
				Fix f = new Fix(num, let, dur, r);
				F.put(num, f);
				r.getFix().add(f);
				fixCnt++;
			}
		}
		
		// Calculate saccades:
		Vector<Integer> v = new Vector<Integer>(F.keySet());
		Collections.sort(v);
		Iterator<Integer> it = v.iterator();
		
		Fix prevFix = null;
		ROI prevROI = null;
		if (it.hasNext()) {
			prevFix = F.get(it.next());
			prevROI = prevFix.getROI();
		}
		
		while (it.hasNext()) {
			Fix currFix = F.get(it.next());
			ROI currROI = currFix.getROI();
			//System.out.println(currROI.getName() + "  [len: " + currROI.getLen() + ", fix-num: " + currFix.getNum() + "]");
			
			// ROI-based saccade:
			int saccROI = currROI.getNum() - prevROI.getNum();
			currFix.setSaccROI(saccROI);
			
			// Letter-based saccade:
			int saccLet = 0;
			switch (Math.round(Math.signum(saccROI))) {
				case 0:  // intra-word
					//System.out.println("   0  curr-let: " + currFix.getLet() + ", prev-let:" + prevFix.getLet());
					saccLet = (currFix.getLet() - prevFix.getLet());
					if (saccLet < 0) saccLet--;
					//System.out.println("  saccLet: " + saccLet);
					break;
					
				case 1:  // inter-word succesive
					//System.out.println("   1  curr-let: " + currFix.getLet() + ", prev-let:" + prevFix.getLet());
					for (int i = prevROI.getNum(); i < currROI.getNum() - 1; i++) {
						saccLet += R.get(i).getLen();
						//System.out.println("  " + R.get(i).getName() + "; len: " + R.get(i).getLen());
					}
					saccLet += (prevROI.getLen() - prevFix.getLet()) + (currFix.getLet() == 0 ? 1 : currFix.getLet());
					//System.out.println("  saccLet: " + saccLet);
					break;
					
				case -1:  // inter-word regressive
					//System.out.println("  -1  curr-let: " + currFix.getLet() + ", prev-let:" + prevFix.getLet());
					for (int i = currROI.getNum(); i < prevROI.getNum() - 1; i++) {
						saccLet += R.get(i).getLen();
						//System.out.println("  " + R.get(i).getName() + "; len: " + R.get(i).getLen());
					}
					saccLet += (prevFix.getLet() == prevROI.getLen() ? 1 : prevFix.getLet()) + (currROI.getLen() - currFix.getLet());
					        // ^-- not sure about this part; added it by analogy to the succesive saccade
					saccLet = -saccLet;
					//System.out.println("  saccLet: " + saccLet);
					break;
			}
			currFix.setSaccLet(saccLet);
			
			//System.out.println("roi: " + currROI.getName() + " (" + currROI.getNum() + "), num: " + currFix.getNum() + ", dur: " + currFix.getDur() + ", sacc-roi: " + currFix.getSaccROI() + ", sacc-let: " + currFix.getSaccLet());
			prevFix = currFix;
			prevROI = currROI;
		}
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public int getTrial()  { return trial;  }
	public int getItem()   { return item;   }
	public int getCond()   { return cond;   }
	public int getFixCnt() { return fixCnt; }
	
	public String getTxt() { return txt; }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public void persist(Statement stX)  throws SQLException {
		// (1) Trial and stimulus sequences:
		trialId = DBI.insertGetId(stX, null, "INSERT INTO trial (sub_id, name, memo, dur) VALUES (" + subId + ", '" + trial + "', '', null)");
		stimId = htStim.get(stimName).getId();
		stimSeqId = DBI.insertGetId(stX, null, "INSERT INTO stim_seq (trial_id, stim_id, fix_cnt, fix_cnt_roi) VALUES (" + trialId + ", " + stimId + "," + fixCnt + "," + fixCnt + ")");
		
		// (2) Check if good amount of ROIs has been read from the trial file:
		if (R.size() < DBI.getLong(stX, "SELECT COUNT(*) AS cnt FROM roi WHERE stim_id = " + stimId, "cnt")) return;
		
		// (3) Fixations (go ROI-by-ROI):
		int i = 0;
		Statement stROI = stX.getConnection().createStatement();
		ResultSet rsROI = stROI.executeQuery("SELECT id, name FROM roi WHERE stim_id = " + stimId + " ORDER BY id");
		while (rsROI.next()) {
			int roiId = rsROI.getInt("id");
			for (Fix f: R.get(i++).getFix()) {
				stX.executeUpdate("INSERT INTO fix (trial_id, stim_id, stim_seq_id, roi_id, num, dur, sacc_roi, sacc_let, let) VALUES (" + trialId + ", " + stimId + ", " + stimSeqId + ", " + roiId + ", " + f.getNum() + ", " + f.getDur() + ", " + f.getSaccROI() + ", " + f.getSaccLet() + ", " + f.getLet() + ")");
			}
		}
		rsROI.close();
	}
	
	
	// =================================================================================================================
	private class ROI {
		private final int num;
		private final String name;
		private final int len;
		private final List<Fix> F = new ArrayList<Fix>();
		
		
		// -------------------------------------------------------------------------------------------------------------
		public ROI(int num, String name, int len) {
			this.num   = num;
			this.name  = name;
			this.len   = len;
		}
		
		
		// -------------------------------------------------------------------------------------------------------------
		public int       getNum()  { return num;  }
		public String    getName() { return name; }
		public int       getLen()  { return len;  }
		public List<Fix> getFix()  { return F;    }
	}
	
	
	// =================================================================================================================
	private class Fix {
		private final int num;
		private final int let;
		private final int dur;
		private Integer saccROI = null;
		private Integer saccLet = null;
		
		private final ROI roi;
		
		
		// -------------------------------------------------------------------------------------------------------------
		public Fix(int num, int let, int dur, ROI roi) {
			this.num = num;
			this.let = let;
			this.dur = dur;
			this.roi = roi;
		}
		
		
		// -------------------------------------------------------------------------------------------------------------
		public int getNum() { return num; }
		public int getLet() { return let; }
		public int getDur() { return dur; }
		
		public Integer getSaccROI() { return saccROI; }
		public Integer getSaccLet() { return saccLet; }
		
		public ROI getROI() { return roi; }
		
		
		// -------------------------------------------------------------------------------------------------------------
		public void setSaccROI(Integer s) { saccROI = s; }
		public void setSaccLet(Integer s) { saccLet = s; }
	}
}
