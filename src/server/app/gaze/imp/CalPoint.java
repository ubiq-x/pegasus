package app.gaze.imp;

import java.sql.SQLException;
import java.sql.Statement;


/**
 * @author Tomek D. Loboda
 */
public class CalPoint {
	public final int num;
	
	public final int x;  // [px]
	public final int y;  // [px]
	
	public final float xErr;  // [px]
	public final float yErr;  // [px]
	
	public final float angErr;  // [deg]
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public CalPoint(int num, int x, int y, float xErr, float yErr, float angErr) {
		this.num = num;
		this.x = x;
		this.y = y;
		this.xErr = xErr;
		this.yErr = yErr;
		this.angErr = angErr;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public void persist(Statement stX, int calId)  throws SQLException {
		stX.executeUpdate("INSERT INTO cal_point (cal_id, num, x, y, x_err, y_err, ang_err) VALUES (" + calId + ", " + num + ", " + x + ", " + y + ", " + (xErr > Cal.ERR_MAX_PX ? Cal.ERR_MAX_PX : xErr) + ", " + (yErr > Cal.ERR_MAX_PX ? Cal.ERR_MAX_PX : yErr) + ", " + (angErr > Cal.ERR_MAX_ANG ? Cal.ERR_MAX_ANG : angErr) + ")");
	}
}
