// calcAggr({cmd:"an-calc-aggr","prj-id":"16","ds-name":"test",unit:"roi-prev-next-adj-ord",adj:"1","exp-id":"92","ids-sub":"","ids-trial":"","ids-stim":"","vars-sub":"","vars-trial":"","trial-cal":"0","trial-val":"0","vars-stim":"","vars-roi":"","roi-max-dist":"0","roi-exp":"-",evts:"'PN':p,1000,0,0,0,eeee",t0:"",t1:"","fix-dur-0":"","fix-dur-1":"",vars:""}, true)


package app.gaze.aggr;

import inter.DBI;

import java.awt.Point;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import main.Do;
import util.$;
import util.RegExpr;
import util.Stats;
import app.data.Var;
import app.gaze.Exp;
import app.gaze.dstruct.Blink;
import app.gaze.dstruct.Fix;
import app.gaze.dstruct.Word;


/**
 * Unit of analysis (stim-seq, event, ROI, or fixation).
 * 
 * @author Tomek D. Loboda
 */
public class Unit {
	private final DBI dbi;
	
	private String qryF;
	private String qryB;
	
	private final String anUnit;
	private final List<Var> sysVarsROI;
	private final int maxROISeenDist;
	
	//private final Hashtable<String,ROISet> rois = new Hashtable<String,ROISet>();
	private final Hashtable<Integer,ROI> roisById  = new Hashtable<Integer,ROI>();
	private final Hashtable<Integer,ROI> roisByNum = new Hashtable<Integer,ROI>();
	private final List<String> roiIdLst = new ArrayList<String>(); 
	private final List<Fix> ntFixs = new ArrayList<Fix>();  // not-on-text fixations
	private final List<Blink> ntBlinks = new ArrayList<Blink>();  // not-on-text blinks
	
	private int t0_exp = -1;  // t0 for this unit (i.e., the time of the first fixation) w.r.t. the start of the experiment
	private int tn_exp = -1;  // tn for this unit (i.e., the time of the last fixation) w.r.t. the start of the experiment
	private int dur_act;      // duration of the exposition (actual, i.e., tn_exp - t0_exp)
	private int dur_nom;      // duration of the exposition (nominal)
	private int nf;           // number of fixations
	private int nb;           // number of blinks
	
	private int minFixNum = Integer.MAX_VALUE;
	
	private final Point txtP1;
	private final Point txtP2;
	private final Point scrP2;
	
	private int notf=0, notfNW=0, notfN=0, notfNE=0, notfE=0, notfSE=0, notfS=0, notfSW=0, notfW=0, notfI=0;  // number of off-text fixations
	private int nosf=0, nosfNW=0, nosfN=0, nosfNE=0, nosfE=0, nosfSE=0, nosfS=0, nosfSW=0, nosfW=0;           // number of off-screen fixations
	
	private int notb=0, notbNW=0, notbN=0, notbNE=0, notbE=0, notbSE=0, notbS=0, notbSW=0, notbW=0, notbI=0;  // number of off-text blinks
	private int nosb=0, nosbNW=0, nosbN=0, nosbNE=0, nosbE=0, nosbSE=0, nosbS=0, nosbSW=0, nosbW=0;           // number of off-screen blinks
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Unit(DBI dbi, String username, String anUnit, int ssId, String qryF, String qryB, String roiIds, int dur, Point txtP1, Point txtP2, Point scrP2, List<Var> sysVarsROI, int maxROISeenDist)  throws SQLException {
		this.dbi            = dbi;
		this.qryF           = qryF;
		this.qryB           = qryB;
		this.anUnit         = anUnit;
		this.dur_nom        = dur;
		this.txtP1          = txtP1;
		this.txtP2          = txtP2;
		this.scrP2          = scrP2;
		this.sysVarsROI     = sysVarsROI;
		this.maxROISeenDist = maxROISeenDist;
		
		// Load ROIs and find the bounding box:
		String db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		
		ResultSet rs = dbi.execQry(
			"SELECT DISTINCT r.id, r.name, r.num FROM " + db + ".roi r " +
			"INNER JOIN " + db + ".stim s ON r.stim_id = s.id " +
			"INNER JOIN " + db + ".stim_seq ss ON ss.stim_id = s.id " +
			"WHERE ss.id = " + ssId + " " + (roiIds.length() > 0 ? " AND r.id IN (" + roiIds + ") " : "") +
			"ORDER BY r.num"
		);
		
		Statement stTmp = dbi.newSt();
		stTmp.execute("SET search_path TO " + db);
		while (rs.next()) {
			String roiName = rs.getString("name");
			int id = rs.getInt("id");
			int num = rs.getInt("num");
			
			//if (!rois.containsKey(roiName)) rois.put(roiName, new ROISet(roiName));
			//ROI r = rois.get(roiName).addROI(stTmp, id, num);
			ROI r = new ROI(dbi, username, stTmp, id, roiName, num, sysVarsROI);
			
			this.roisById.put(id, r);
			this.roisByNum.put(num, r);
			this.roiIdLst.add(rs.getString("id"));
		}
		stTmp.close();
		
		rs.close();
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * vars: variables to be included in the output
	 */
	public List<String> calc(Set<String> incVars, List<String> lstEmptyVal)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		procFix(qryF);
		procBlinks(qryB);
		
		     if (this.anUnit.equals("fix"))                   return calcByFix(incVars, lstEmptyVal);
		else if (this.anUnit.equals("roi"))                   return calcByROI(incVars, lstEmptyVal);
		else if (this.anUnit.equals("roi-prev-next-adj-ord")) return calcByROIPrevNext(true,  incVars, lstEmptyVal);
		else if (this.anUnit.equals("roi-prev-next-adj-fix")) return calcByROIPrevNext(false, incVars, lstEmptyVal);
		else if (this.anUnit.equals("s-seq"))                 return calcByStimSeq(incVars);
		
		return null;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/*
	private String getSysVars(List<String> roiIdLst)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		StringBuffer res = new StringBuffer(lstSysVars.size() == 0 ? "" : ",");
		for (int i = 0; i < lstSysVars.size(); i++) {
			res.append(lstSysVars.get(i).calcValues(roiIdLst) + (i < lstSysVars.size()-1 ? "," : ""));
		}
		return res.toString();
	}
	*/
	
	
	// -----------------------------------------------------------------------------------------------------------------
	private List<String> getSysVars(Set<String> incVars, ROI r, String prefix)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		List<String> res = new ArrayList<String>();
		
		Iterator<Var> it = this.sysVarsROI.iterator();
		while (it.hasNext()) {
			Var v = it.next();
			if (incVars != null && incVars.size() > 0 && incVars.contains("roi_" + v.getName().replace("-", "_"))) continue;
			
			if (r == null) {
				res.add("null");
				continue;
			}
			
			String val = r.getSysVar(v.getName());
			res.add(val == null ? "null" : val);
		}
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public int getTSLen() { return this.nf; }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	private void procBlinks(String qryB)  throws SQLException {
		this.nb   = 0;
		this.notb = 0;
		
		if (qryB.length() == 0) return;
		
		ResultSet rs = dbi.execQry(qryB.toString());
		while (rs.next()) {
			this.nb++;
			
			Float   sx      = rs.getFloat("sx");        if (rs.wasNull())  sx       = null;
			Float   sy      = rs.getFloat("sy");        if (rs.wasNull())  sy       = null;
			Integer dur     = rs.getInt("dur");         if (rs.wasNull())  dur      = null;
			Integer saccDur = rs.getInt("sacc_dur");    if (rs.wasNull())  saccDur  = null;
			Float   saccAmp = rs.getFloat("sacc_amp");  if (rs.wasNull())  saccAmp  = null;
			
			if (sx == null || sy == null) continue;
			
			Blink b = new Blink(-1, rs.getLong("t_exp"), rs.getLong("t_trial"), sx, sy, null, null, dur, saccDur, saccAmp, Exp.DIR_NONE, Exp.DIR_NONE);  // -1 is used for the ID here because ID is not needed (at least I think, because I'm commenting this line a long time after adding it)
			ROI r = this.roisById.get(rs.getInt("rid"));
			
			// (1) Off-screen and off-text:
			if (rs.getInt("rid") == DBI.ID_NONE) {
				// (1.1) Off-screen:
				     if (b.sx <  0       && b.sy <  0)                          { nosb++;  nosbNW++;  b.os = Exp.DIR_NW; }
				else if (b.sx >= 0       && b.sx <= scrP2.x && b.sy <  0)       { nosb++;  nosbN++;   b.os = Exp.DIR_N;  }
				else if (b.sx >  scrP2.x && b.sy <  0)                          { nosb++;  nosbNE++;  b.os = Exp.DIR_NE; }
				else if (b.sx >  scrP2.x && b.sy >= 0       && b.sy <= scrP2.y) { nosb++;  nosbE++;   b.os = Exp.DIR_E;  }
				else if (b.sx >  scrP2.x && b.sy >  scrP2.y)                    { nosb++;  nosbSE++;  b.os = Exp.DIR_SE; }
				else if (b.sx >= 0       && b.sx <= scrP2.x && b.sy >  scrP2.y) { nosb++;  nosbS++;   b.os = Exp.DIR_S;  }
				else if (b.sx <  0       && b.sy >  scrP2.y)                    { nosb++;  nosbSW++;  b.os = Exp.DIR_SW; }
				else if (b.sx <  0       && b.sy >= 0       && b.sy <= scrP2.y) { nosb++;  nosbW++;   b.os = Exp.DIR_W;  }
				
				// (1.2) Off-text:
				else if (b.sx <  txtP1.x && b.sy <  txtP1.y)                    { notb++;  notbNW++;  b.ot = Exp.DIR_NW; }
				else if (b.sx >= txtP1.x && b.sx <= txtP2.x && b.sy <  txtP1.y) { notb++;  notbN++;   b.ot = Exp.DIR_N;  }
				else if (b.sx >  txtP2.x && b.sy <  txtP1.y)                    { notb++;  notbNE++;  b.ot = Exp.DIR_NE; }
				else if (b.sx >  txtP2.x && b.sy >= txtP1.y && b.sy <= txtP2.y) { notb++;  notbE++;   b.ot = Exp.DIR_E;  }
				else if (b.sx >  txtP2.x && b.sy >  txtP2.y)                    { notb++;  notbSE++;  b.ot = Exp.DIR_SE; }
				else if (b.sx >= txtP1.x && b.sx <= txtP2.x && b.sy >  txtP2.y) { notb++;  notbS++;   b.ot = Exp.DIR_E;  }
				else if (b.sx <  txtP1.x && b.sy >  txtP2.y)                    { notb++;  notbSW++;  b.ot = Exp.DIR_SW; }
				else if (b.sx <  txtP1.x && b.sy >= txtP1.y && b.sy <= txtP2.y) { notb++;  notbW++;   b.ot = Exp.DIR_W;  }
				else { notb++;  notbI++;  b.ot = Exp.DIR_I; }
				
				this.ntBlinks.add(b);
				continue;
			}
			
			// (2) Digest blink:
			if (r != null) {
				r.digestBlink(b);
			}
		}
		
		rs.close();
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * In this method, only the unadjusted properties of fixations are referred to, e.g., "x" instead of "x_adj." That 
	 * is because the query for selecting fixations has already determined if the unadjusted or adjusted EMs should be  
	 * used.
	 */
	// TODO: check if 'prevTrial' and 'currTrial' are still necessary
	private void procFix(String qryF)  throws SQLException {
		this.nf   = 0;
		this.notf = 0;
		
		if (qryF.length() == 0) return;
		
		ROI prevROI = null;
		Fix prevF = null;
		List<ROI> lstRP = new ArrayList<ROI>();  // for RP calc
		int prevTrialId = -1;
		
		ResultSet rs = dbi.execQry(qryF.toString());
		boolean ok = rs.next();
		
		while (ok) {
			this.nf++;
			
			Float   x        = rs.getFloat("x");         if (rs.wasNull()) x        = null;
			Float   y        = rs.getFloat("y");         if (rs.wasNull()) y        = null;
			Integer num      = rs.getInt("fnum");        if (rs.wasNull()) num      = null;
			Integer tExp     = rs.getInt("t_exp");       if (rs.wasNull()) tExp     = null;
			Integer saccIROI = rs.getInt("sacc_i_roi");  if (rs.wasNull()) saccIROI = null;
			Integer saccILet = rs.getInt("sacc_i_let");  if (rs.wasNull()) saccILet = null;
			Integer saccOROI = rs.getInt("sacc_o_roi");  if (rs.wasNull()) saccOROI = null;
			Integer saccOLet = rs.getInt("sacc_o_let");  if (rs.wasNull()) saccOLet = null;
			Float   pupil    = rs.getFloat("pupil");     if (rs.wasNull()) pupil    = null;
			Integer let      = rs.getInt("let");         if (rs.wasNull()) let      = null;
			
			if (t0_exp == -1 && tExp != null) t0_exp = tExp;
			if (tExp != null) tn_exp = tExp;
			
			if (x == null || y == null) {
				ok = rs.next();
				continue;
			}
			
			Fix currF = new Fix(-1, rs.getLong("t_exp"), rs.getLong("t_trial"), rs.getInt("dur"), x, y, null, null, num, saccIROI, saccILet, saccOROI, saccOLet, pupil, Exp.DIR_NONE, Exp.DIR_NONE, false, false, let);  // -1 is used for the ID here because ID is not needed (at least I think, because I'm commenting this line a long time after adding it)
			
			if (currF.num < minFixNum) this.minFixNum = currF.num;
			int roiId = rs.getInt("rid");
			int currTrialId = rs.getInt("tid");
			
			// (1) Off-screen and off-text:
			if (rs.getInt("rid") == DBI.ID_NONE) {
				// (1.1) Off-screen:
				     if (currF.x <  0       && currF.y <  0)                             { nosf++;  nosfNW++;  currF.os = Exp.DIR_NW; }
				else if (currF.x >= 0       && currF.x <= scrP2.x && currF.y < 0)        { nosf++;  nosfN++;   currF.os = Exp.DIR_N;  }
				else if (currF.x >  scrP2.x && currF.y <  0)                             { nosf++;  nosfNE++;  currF.os = Exp.DIR_NE; }
				else if (currF.x >  scrP2.x && currF.y >= 0       && currF.y <= scrP2.y) { nosf++;  nosfE++;   currF.os = Exp.DIR_E;  }
				else if (currF.x >  scrP2.x && currF.y >  scrP2.y)                       { nosf++;  nosfSE++;  currF.os = Exp.DIR_SE; }
				else if (currF.x >= 0       && currF.x <= scrP2.x && currF.y > scrP2.y)  { nosf++;  nosfS++;   currF.os = Exp.DIR_S;  }
				else if (currF.x <  0       && currF.y >  scrP2.y)                       { nosf++;  nosfSW++;  currF.os = Exp.DIR_SW; }
				else if (currF.x <  0       && currF.y >= 0       && currF.y <= scrP2.y) { nosf++;  nosfW++;   currF.os = Exp.DIR_W;  }
				
				// (1.2) Off-text:
				else if (currF.x <  txtP1.x && currF.y <  txtP1.y)                       { notf++;  notfNW++;  currF.ot = Exp.DIR_NW; }
				else if (currF.x >= txtP1.x && currF.x <= txtP2.x && currF.y <  txtP1.y) { notf++;  notfN++;   currF.ot = Exp.DIR_N;  }
				else if (currF.x >  txtP2.x && currF.y <  txtP1.y)                       { notf++;  notfNE++;  currF.ot = Exp.DIR_NE; }
				else if (currF.x >  txtP2.x && currF.y >= txtP1.y && currF.y <= txtP2.y) { notf++;  notfE++;   currF.ot = Exp.DIR_E;  }
				else if (currF.x >  txtP2.x && currF.y >  txtP2.y)                       { notf++;  notfSE++;  currF.ot = Exp.DIR_SE; }
				else if (currF.x >= txtP1.x && currF.x <= txtP2.x && currF.y >  txtP2.y) { notf++;  notfS++;   currF.ot = Exp.DIR_E;  }
				else if (currF.x <  txtP1.x && currF.y >  txtP2.y)                       { notf++;  notfSW++;  currF.ot = Exp.DIR_SW; }
				else if (currF.x <  txtP1.x && currF.y >= txtP1.y && currF.y <= txtP2.y) { notf++;  notfW++;   currF.ot = Exp.DIR_W;  }
				else { notf++; notfI++; currF.ot = Exp.DIR_I; }
				
				ok = rs.next();
				this.ntFixs.add(currF);
				continue;
			}
			if (!ok) continue;
			
			//ROI currROI = rois.get(rs.getString("rname")).get(roiId);
			ROI currROI = this.roisById.get(roiId);
			
			// (2) Set the next first-pass ROI:
			if (prevROI != null && prevROI.getNF() == prevROI.getNFPF()) prevROI.setFPNextROI(currROI);
			
			// (3) Mark the current and the next ROIs as seen (until the provided threshold or the end of the line have been reached):
			currROI.setSeen(true);
			for (int i = currROI.getNum() + 1; i < currROI.getNum() + this.maxROISeenDist + 1; i++) {
				ROI r = this.roisByNum.get(i);
				if (r != null && r.getY() == currROI.getY()) r.setSeen(true);
			}
			
			// (4) Terminate first-pass reading (if ROI different than the one the previous fixation was made on is being fixated at the moment):
			if ((prevROI != null && prevROI != currROI) || (prevROI == currROI && prevF != null && currF.num - prevF.num != 1)) {
				prevROI.setFP(false);
				if (currF != null && currF.saccInROI != null && currF.saccInROI > 0) {  // successive incoming saccade -- terminate first-pass reading on all skipped ROIs that have been seen
					for (int i = prevROI.getNum() + 1; i < Math.min(currROI.getNum(), prevROI.getNum() + this.maxROISeenDist + 1); i++) {
						ROI r = this.roisByNum.get(i);
						if (r != null && r.isSeen() && r.getY() == prevROI.getY()) r.setFP(false);
					}
				}
			}
			
			// (5) RP:
			// TODO: redo this part (it might yield incorrect results)
			if (currROI.isRP() && !lstRP.contains(currROI)) lstRP.add(currROI);
			Iterator<ROI> it = lstRP.iterator();
			while (it.hasNext()) {
				ROI r = (ROI) it.next();
				if (currTrialId != prevTrialId || currROI.getNum() > r.getNum()) {
					r.setRP(false);
					it.remove();
				}
				else r.digestFixRP(currF.dur);
			}
			
			// (6) First fixation on the word and first-pass reading:
			if (currROI.getNF() == 0) currF.f = true;
			if (currROI.isFP()) currF.fp = true;
			
			// (7) Digest fix:
			//System.out.println("'" + currROI.getName() + "': fix-dur=" + f.dur);
			//System.out.println("  B: nf=" + currROI.getNF() + ", nfpf=" + currROI.getNFPF() + ", nr=" + currROI.getNR() + ", nfpr=" + currROI.getNFPR() + ", afd=" + $.trunc(currROI.getAFD(),2) + ", ffd=" + $.trunc(currROI.getFFD(),2) + ", sfd=" + $.trunc(currROI.getSFD(),2) + ", gd=" + $.trunc(currROI.getGD(),2) + ", tt=" + $.trunc(currROI.getTT(),2));
			currROI.digestFix(currF, prevROI);
			//System.out.println("  A: nf=" + currROI.getNF() + ", nfpf=" + currROI.getNFPF() + ", nr=" + currROI.getNR() + ", nfpr=" + currROI.getNFPR() + ", afd=" + $.trunc(currROI.getAFD(),2) + ", ffd=" + $.trunc(currROI.getFFD(),2) + ", sfd=" + $.trunc(currROI.getSFD(),2) + ", gd=" + $.trunc(currROI.getGD(),2) + ", tt=" + $.trunc(currROI.getTT(),2));
			
			// (8) Digest sacc:
			// NOTE: We gotta look on the next ROI since saccade direction is stored in the target ROI only (that 
			//       should probably be changed at some point, though the price will be redundancy).
			ok = rs.next();
			if (ok && rs.getInt("sacc_i_roi") < 0) {
				currROI.digestRegr();
			}
			/*
			if (prevROI != null && currTrialId == prevTrialId) {
				if (sacc < 0) prevROI.digestRegr();
			}
			*/
			
			// (9) Finish:
			prevROI = currROI;
			prevF = currF;
			prevTrialId = currTrialId;
		}
		
		rs.close();
		
		dur_act = tn_exp - t0_exp;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * This method should be called after fixations have been processed. It produces a row of values per fixation.
	 */
	private List<String> calcByFix(Set<String> incVars, List<String> lstEmptyVal)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		if (this.nf == 0) return null;
		
		Hashtable<Integer,String> htRes = new Hashtable<Integer,String>();  // key: ROI id (stores results before sorting them by fix ord. num.)
		Map<String,Word> mapWord = Do.dbCache.getWords();
		
		// (1) On-text fixations:
		Iterator<ROI> it = this.roisByNum.values().iterator();
		while (it.hasNext()) {
			ROI r = it.next();
			if (!r.isSeen()) continue;
			
			String roiName = r.getName();
			
			List<String> lstId = new ArrayList<String>();
			lstId.clear();
			lstId.add("" + r.getId());
			
			// Word variables:
			Word w = null;
			String word = RegExpr.match("[^\\w]*([\\w]*)[^\\s]*", r.getName()).group(1);
			if (mapWord.get(word) != null) w = mapWord.get(word);
			
			// Output:
			for (Fix f: r.getFix()) {
				List<String> lstVal = new ArrayList<String>();
				
				lstVal.add(Aggr.varGetVal(incVars, "em_t0_exp",  this.t0_exp));
				lstVal.add(Aggr.varGetVal(incVars, "em_tn_exp",  this.tn_exp));
				lstVal.add(Aggr.varGetVal(incVars, "em_dur_act", this.dur_act));
				lstVal.add(Aggr.varGetVal(incVars, "em_dur_nom", this.dur_nom));
				
				lstVal.add("" + (f.num - this.minFixNum));  // i
				lstVal.add(Aggr.varGetVal(incVars, "em_roi",        DBI.esc(roiName)));
				lstVal.add(Aggr.varGetVal(incVars, "em_t_exp",      (f.tExp == null ? "null" : "" + f.tExp)));
				lstVal.add(Aggr.varGetVal(incVars, "em_t_trial",    (f.tTrial == null ? "null" : "" + f.tTrial)));
				lstVal.add(Aggr.varGetVal(incVars, "em_dur",        f.dur));
				lstVal.add(Aggr.varGetVal(incVars, "em_wlen",       roiName.length()));
				lstVal.add(Aggr.varGetVal(incVars, "em_f",          f.f));
				lstVal.add(Aggr.varGetVal(incVars, "em_fp",         f.fp));
				lstVal.add(Aggr.varGetVal(incVars, "em_let",        (f.let == -1 ? "null" : "" + f.let)));
				lstVal.add(Aggr.varGetVal(incVars, "em_let_norm",   (f.let == -1 ? "null" : "" + Math.min((float)f.let / (float)roiName.length(), 1.0f))));
				lstVal.add(Aggr.varGetVal(incVars, "em_sacc_i_roi", (f.saccInROI == null ? "null" : "" + f.saccInROI)));
				lstVal.add(Aggr.varGetVal(incVars, "em_sacc_o_roi", (f.saccOutROI == null ? "null" : "" + f.saccOutROI)));
				lstVal.add(Aggr.varGetVal(incVars, "em_sacc_i_let", (f.saccInLet == null ? "null" : "" + f.saccInLet)));
				lstVal.add(Aggr.varGetVal(incVars, "em_sacc_o_let", (f.saccOutLet == null ? "null" : "" + f.saccOutLet)));
				lstVal.add(Aggr.varGetVal(incVars, "em_pupil",      (f.pupil == null ? "null" : "" + f.pupil)));
				lstVal.add(Aggr.varGetVal(incVars, "em_ot_dir",     "0"));
				lstVal.add(Aggr.varGetVal(incVars, "em_ot_bin",     "0"));
				lstVal.add(Aggr.varGetVal(incVars, "em_os_dir",     "0"));
				lstVal.add(Aggr.varGetVal(incVars, "em_os_bin",     "0"));
				
				//lstVal.addAll((w != null ? w.getValues(incVars, true) : Word.getValuesEmpty(incVars, "null", true)));
				lstVal.addAll((w != null ? w.getValues(incVars, true) : lstEmptyVal));
				lstVal.addAll(getSysVars(incVars, r, ""));
				
				htRes.put(f.num, $.join(lstVal, "", "", ",", false, true).toString());
			}
		}
		
		// (2) Off-text fixations:
		for (Fix f: this.ntFixs) {
			// Output:
			List<String> lstVal = new ArrayList<String>();
			
			lstVal.add(Aggr.varGetVal(incVars, "em_t0_exp",  this.t0_exp));
			lstVal.add(Aggr.varGetVal(incVars, "em_tn_exp",  this.tn_exp));
			lstVal.add(Aggr.varGetVal(incVars, "em_dur_act", this.dur_act));
			lstVal.add(Aggr.varGetVal(incVars, "em_dur_nom", this.dur_nom));
			
			lstVal.add("" + (f.num - this.minFixNum));  // i
			lstVal.add(Aggr.varGetVal(incVars, "em_roi",        "null"));
			lstVal.add(Aggr.varGetVal(incVars, "em_t_exp",      (f.tExp == null ? "null" : "" + f.tExp)));
			lstVal.add(Aggr.varGetVal(incVars, "em_t_trial",    (f.tTrial == null ? "null" : "" + f.tTrial)));
			lstVal.add(Aggr.varGetVal(incVars, "em_dur",        f.dur));
			lstVal.add(Aggr.varGetVal(incVars, "em_wlen",       "0"));
			lstVal.add(Aggr.varGetVal(incVars, "em_f",          "null"));
			lstVal.add(Aggr.varGetVal(incVars, "em_fp",         "null"));
			lstVal.add(Aggr.varGetVal(incVars, "em_let",        "null"));
			lstVal.add(Aggr.varGetVal(incVars, "em_let_norm",   "null"));
			lstVal.add(Aggr.varGetVal(incVars, "em_sacc_i_roi", "null"));
			lstVal.add(Aggr.varGetVal(incVars, "em_sacc_o_roi", "null"));
			lstVal.add(Aggr.varGetVal(incVars, "em_sacc_i_let", "null"));
			lstVal.add(Aggr.varGetVal(incVars, "em_sacc_o_let", "null"));
			lstVal.add(Aggr.varGetVal(incVars, "em_pupil",      (f.pupil == null ? "null" : "" + f.pupil)));
			lstVal.add(Aggr.varGetVal(incVars, "em_ot_dir",     f.ot));
			lstVal.add(Aggr.varGetVal(incVars, "em_ot_bin",     (f.ot == 0 ? "0" : "1")));
			lstVal.add(Aggr.varGetVal(incVars, "em_os_dir",     f.os));
			lstVal.add(Aggr.varGetVal(incVars, "em_os_bin",     (f.os == 0 ? "0" : "1")));
			
			//lstVal.addAll(Word.getValuesEmpty(incVars, "null", true));
			lstVal.addAll(lstEmptyVal);
			lstVal.addAll(getSysVars(incVars, null, ""));
			
			htRes.put(f.num, $.join(lstVal, "", "", ",", false, true).toString());
		}
		
		// (3) Finish:
		List<String> res = new ArrayList<String>();
		Vector<Integer> v = new Vector<Integer>(htRes.keySet());
		Collections.sort(v);
		Iterator<Integer> it03 = v.iterator();
		while (it03.hasNext()) res.add(htRes.get(it03.next()));
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * This method should be called after fixations have been processed. It produces a row of values per ROI.
	 */
	private List<String> calcByROI(Set<String> incVars, List<String> lstEmptyVal)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		if (this.nf == 0) return null;
		
		Hashtable<Integer,String> htRes = new Hashtable<Integer,String>();  // key: ROI id (stores results before sorted them by ROI id)
		Map<String,Word> mapWord = Do.dbCache.getWords();
		
		Iterator<ROI> it = this.roisByNum.values().iterator();
		while (it.hasNext()) {
			ROI r = it.next();
			if (!r.isSeen()) continue;  // ROIs not seen will not be outputted
			
			List<String> lstVal = new ArrayList<String>();
			
			lstVal.add(Aggr.varGetVal(incVars, "em_t0_exp",  this.t0_exp));
			lstVal.add(Aggr.varGetVal(incVars, "em_tn_exp",  this.tn_exp));
			lstVal.add(Aggr.varGetVal(incVars, "em_dur_act", this.dur_act));
			lstVal.add(Aggr.varGetVal(incVars, "em_dur_nom", this.dur_nom));
			
			calcByROI_procROI(r, lstVal, incVars);
			
			Word me = null;
			String word = RegExpr.match("[^\\w]*([\\w]*)[^\\s]*", r.getName()).group(1);
			if (mapWord.get(word) != null) me = mapWord.get(word);
			
			//lstVal.addAll(me != null ? me.getValues(incVars, true) : Word.getValuesEmpty(incVars, "null", true));
			lstVal.addAll(me != null ? me.getValues(incVars, true) : lstEmptyVal);
			lstVal.addAll(getSysVars(incVars, r, ""));
			
			htRes.put(r.getId(), $.join(lstVal, "", "", ",", false, true).toString());
		}
		
		// Finish:
		List<String> res = new ArrayList<String>();
		Vector<Integer> v = new Vector<Integer>(htRes.keySet());
		Collections.sort(v);
		Iterator<Integer> it03 = v.iterator();
		while (it03.hasNext()) res.add(htRes.get(it03.next()));
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * This method should be called after fixations have been processed. It produces a row of values per ROI. For each 
	 * ROI the characteristics of the two adjacent ROIs are outputted as well (if they exist, which is not the case 
	 * for the first and last word in a line).
	 * 
	 * - isAdjOrd: Indicates whether the previous and next ROIs need to be order-adjacent to the current one, 
	 *     i.e., do the three ROIs need to be next to each other given the order in which they were defined 
	 *     (e.g., the previous, current, and next word in text). If they do not have to be, then the previous 
	 *     and the next ROIs fixated in first-pass are returned (i.e., the three fixation-adjacent ROIs).
	 */
	private List<String> calcByROIPrevNext(boolean isAdjOrd, Set<String> incVars, List<String> lstEmptyVal)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		if (this.nf == 0) return null;
		
		Hashtable<Integer,String> htRes = new Hashtable<Integer,String>();  // key: ROI id (stores results before sorted them by ROI id)
		Map<String,Word> mapWord = Do.dbCache.getWords();
		
		Iterator<ROI> it = this.roisByNum.values().iterator();
		while (it.hasNext()) {
			ROI r = it.next();
			if (!r.isFix()) continue;  // ROIs not fixated will not be outputted
			
			List<String> lstVal = new ArrayList<String>();
			
			lstVal.add(Aggr.varGetVal(incVars, "em_t0_exp",  this.t0_exp));
			lstVal.add(Aggr.varGetVal(incVars, "em_tn_exp",  this.tn_exp));
			lstVal.add(Aggr.varGetVal(incVars, "em_dur_act", this.dur_act));
			lstVal.add(Aggr.varGetVal(incVars, "em_dur_nom", this.dur_nom));
			
			// Current ROI:
			calcByROI_procROI(r, lstVal, incVars);
			
			Word me = null;
			String word = RegExpr.match("[^\\w]*([\\w]*)[^\\s]*", r.getName()).group(1);
			if (mapWord.get(word) != null) me = mapWord.get(word);
			
			//lstVal.addAll(me != null ? me.getValues(incVars, true) : Word.getValuesEmpty(incVars, "null", true));
			lstVal.addAll(me != null ? me.getValues(incVars, true) : lstEmptyVal);
			lstVal.addAll(getSysVars(incVars, r, "c_"));
			
			// Previous ROI:
			ROI rPrev = (isAdjOrd ? roisByNum.get(r.getNum() - 1) : r.getPrevROI());
			if (rPrev != null && this.roisById.get(rPrev.getId()) != null) rPrev = this.roisById.get(rPrev.getId());
			
			lstVal.add(Aggr.varGetVal(incVars, "em_seen_p", (rPrev == null ? "null" : (rPrev.isSeen() ? "1" : "0"))));
			calcByROI_procROI(rPrev, lstVal, incVars);
			
			Word mePrev = null;
			if (rPrev != null) {
				String wordPrev = RegExpr.match("[^\\w]*([\\w]*)[^\\s]*", rPrev.getName()).group(1);
				if (mapWord.get(wordPrev) != null) mePrev = mapWord.get(wordPrev);
			}
			
			//lstVal.addAll(mePrev != null ? mePrev.getValues(incVars, true) : Word.getValuesEmpty(incVars, "null", true));
			lstVal.addAll(mePrev != null ? mePrev.getValues(incVars, true) : lstEmptyVal);
			lstVal.addAll(getSysVars(incVars, rPrev, "p_"));
			
			// Next ROI:
			ROI rNext = (isAdjOrd ? roisByNum.get(r.getNum() + 1) : r.getNextROI());
			if (rNext != null && this.roisById.get(rNext.getId()) != null) rNext = this.roisById.get(rNext.getId());
			
			lstVal.add(Aggr.varGetVal(incVars, "em_seen_n", (rNext == null ? "null" : "" + (rNext.isSeen() ? "1" : "0"))));
			calcByROI_procROI(rNext, lstVal, incVars);
			Word meNext = null;
			if (rNext != null) {
				String wordNext = RegExpr.match("[^\\w]*([\\w]*)[^\\s]*", rNext.getName()).group(1);
				if (mapWord.get(wordNext) != null) meNext = mapWord.get(wordNext);
			}
			
			//lstVal.addAll(meNext != null ? meNext.getValues(incVars, true) : Word.getValuesEmpty(incVars, "null", true));
			lstVal.addAll(meNext != null ? meNext.getValues(incVars, true) : lstEmptyVal);
			lstVal.addAll(getSysVars(incVars, rNext, "n_"));
			
			// Add to the final hashtable:
			htRes.put(r.getId(), $.join(lstVal, "", "", ",", false, true).toString());
		}
		
		// Finish:
		List<String> res = new ArrayList<String>();
		Vector<Integer> v = new Vector<Integer>(htRes.keySet());
		Collections.sort(v);
		Iterator<Integer> it03 = v.iterator();
		while (it03.hasNext()) res.add(htRes.get(it03.next()));
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	private void calcByROI_procROI(ROI r, List<String> lstVal, Set<String> incVars) {
		lstVal.add(Aggr.varGetVal(incVars, "em_num" ,           (r == null                      ? "null" : "" + r.getNum()                       )));
		lstVal.add(Aggr.varGetVal(incVars, "em_roi" ,           (r == null                      ? "null" : DBI.esc(r.getName())                  )));
		lstVal.add(Aggr.varGetVal(incVars, "em_wlen",           (r == null                      ? "null" : "" + r.getLen()                       )));
		
		lstVal.add(Aggr.varGetVal(incVars, "em_nf",             (r == null                      ? "null" : "" + r.getNF()                        )));
		lstVal.add(Aggr.varGetVal(incVars, "em_nfpf",           (r == null                      ? "null" : "" + r.getNFPF()                      )));
		lstVal.add(Aggr.varGetVal(incVars, "em_nspf",           (r == null                      ? "null" : "" + r.getNSPF()                      )));
		lstVal.add(Aggr.varGetVal(incVars, "em_nr",             (r == null                      ? "null" : "" + r.getNR()                        )));
		lstVal.add(Aggr.varGetVal(incVars, "em_nfpr",           (r == null                      ? "null" : "" + r.getNFPR()                      )));
		lstVal.add(Aggr.varGetVal(incVars, "em_nspr",           (r == null                      ? "null" : "" + r.getNSPR()                      )));
		lstVal.add(Aggr.varGetVal(incVars, "em_nb",             (r == null                      ? "null" : "" + r.getNB()                        )));
		
		lstVal.add(Aggr.varGetVal(incVars, "em_fpfd_min",       (r == null || r.getNFPF() == 0  ? "null" : "" + r.getFPFDMin()                   )));
		lstVal.add(Aggr.varGetVal(incVars, "em_fpfd_max",       (r == null || r.getNFPF() == 0  ? "null" : "" + r.getFPFDMax()                   )));
		lstVal.add(Aggr.varGetVal(incVars, "em_spfd_min",       (r == null || r.getNSPF() == 0  ? "null" : "" + r.getSPFDMin()                   )));
		lstVal.add(Aggr.varGetVal(incVars, "em_spfd_max",       (r == null || r.getNSPF() == 0  ? "null" : "" + r.getSPFDMax()                   )));
		
		lstVal.add(Aggr.varGetVal(incVars, "em_afd",            (r == null || r.getNF() == 0    ? "null" : "" + $.trunc(r.getAFD(), 2)           )));
		lstVal.add(Aggr.varGetVal(incVars, "em_ffd",            (r == null || r.getNF() == 0    ? "null" : "" + $.trunc(r.getFFD(), 2)           )));
		lstVal.add(Aggr.varGetVal(incVars, "em_sfd",            (r == null || r.getNFPF() != 1  ? "null" : "" + $.trunc(r.getSFD(), 2)           )));
		lstVal.add(Aggr.varGetVal(incVars, "em_gd",             (r == null || r.getNFPF() == 0  ? "null" : "" + $.trunc(r.getGD(), 2)            )));
		lstVal.add(Aggr.varGetVal(incVars, "em_tt",             (r == null || r.getNF() == 0    ? "null" : "" + $.trunc(r.getTT(), 2)            )));
		lstVal.add(Aggr.varGetVal(incVars, "em_rp",             (r == null                      ? "null" : "null"                                )));
		
		lstVal.add(Aggr.varGetVal(incVars, "em_abd",            (r == null || r.getNB() == 0    ? "null" : "" + $.trunc(r.getABD(), 2)           )));
		lstVal.add(Aggr.varGetVal(incVars, "em_absd",           (r == null || r.getNB() == 0    ? "null" : "" + $.trunc(r.getABSD(), 2)          )));
		lstVal.add(Aggr.varGetVal(incVars, "em_absa",           (r == null || r.getNB() == 0    ? "null" : "" + $.trunc(r.getABSA(), 2)          )));
		
		lstVal.add(Aggr.varGetVal(incVars, "em_let_i_fp",       (r == null || r.getNFIn()  == 0 ? "null" : "" + r.getLetInFP()                   )));
		lstVal.add(Aggr.varGetVal(incVars, "em_let_o_fp",       (r == null || r.getNFOut() == 0 ? "null" : "" + r.getLetOutFP()                  )));
		lstVal.add(Aggr.varGetVal(incVars, "em_let_i_avg",      (r == null || r.getNFIn()  == 0 ? "null" : "" + $.trunc(r.getLetInAvg(), 2)      )));
		lstVal.add(Aggr.varGetVal(incVars, "em_let_o_avg",      (r == null || r.getNFOut() == 0 ? "null" : "" + $.trunc(r.getLetOutAvg(), 2)     )));
		
		lstVal.add(Aggr.varGetVal(incVars, "em_sacc_i_roi_fp",  (r == null || r.getNFIn()  == 0 ? "null" : "" + r.getSaccInROIFP()               )));
		lstVal.add(Aggr.varGetVal(incVars, "em_sacc_o_roi_fp",  (r == null || r.getNFOut() == 0 ? "null" : "" + r.getSaccOutROIFP()              )));
		lstVal.add(Aggr.varGetVal(incVars, "em_sacc_i_let_fp",  (r == null || r.getNFIn()  == 0 ? "null" : "" + r.getSaccInLetFP()               )));
		lstVal.add(Aggr.varGetVal(incVars, "em_sacc_o_let_fp",  (r == null || r.getNFOut() == 0 ? "null" : "" + r.getSaccOutLetFP()              )));
		lstVal.add(Aggr.varGetVal(incVars, "em_sacc_i_roi_avg", (r == null || r.getNFIn()  == 0 ? "null" : "" + $.trunc(r.getSaccInROIAvg(), 2)  )));
		lstVal.add(Aggr.varGetVal(incVars, "em_sacc_o_roi_avg", (r == null || r.getNFOut() == 0 ? "null" : "" + $.trunc(r.getSaccOutROIAvg(), 2) )));
		lstVal.add(Aggr.varGetVal(incVars, "em_sacc_i_let_avg", (r == null || r.getNFIn()  == 0 ? "null" : "" + $.trunc(r.getSaccInLetAvg(), 2)  )));
		lstVal.add(Aggr.varGetVal(incVars, "em_sacc_o_let_avg", (r == null || r.getNFOut() == 0 ? "null" : "" + $.trunc(r.getSaccOutLetAvg(), 2) )));
		
		lstVal.add(Aggr.varGetVal(incVars, "em_pupil",          (r == null || r.getNFPup() == 0 ? "null" : "" + $.trunc(r.getPupil(), 2)         )));
		
		//lstVal.add(Aggr.varGetVal(incVars, "em_nfpb",           (r == null                      ? "null" : "" + r.getNB()                        )));
		//lstVal.add(Aggr.varGetVal(incVars, "em_nspb",           (r == null                      ? "null" : "" + r.getNB()                        )));
		// TODO: (r.getNF() > 0 && r.getNR() > 0 ? "" + $.trunc(r.getRP(), 2) : "null"))
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * This method should be called after fixations have been processed. It produces a single row of averages.
	 */
	private List<String> calcByStimSeq(Set<String> incVars)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		if (this.nf == 0) return null;
		
		int nroi   = this.roisByNum.size();
		int nrois  = 0;
		int nr     = 0;
		int nfpf   = 0;
		int nfpr   = 0;
		int nwf0   = 0;
		int nwf1   = 0;
		int nwf2   = 0;
		int nfpwf0 = 0;
		int nfpwf1 = 0;
		int nfpwf2 = 0;
		
		Stats sNF   = new Stats();
		Stats sNFPF = new Stats();
		Stats sNR   = new Stats();
		Stats sNFPR = new Stats();
		Stats sAFD  = new Stats();
		Stats sFFD  = new Stats();
		Stats sSFD  = new Stats();
		Stats sGD   = new Stats();
		Stats sTT   = new Stats();
		Stats sRP   = new Stats();
		Stats sABD  = new Stats();
		Stats sABSD = new Stats();
		Stats sABSA = new Stats();
		Stats sWL   = new Stats();
		Stats sWLS  = new Stats();
		
		WordStats sWord = new WordStats();
		Map<String,Word> w = Do.dbCache.getWords();
		List<String> lstId = new ArrayList<String>();
		
		Iterator<ROI> it = this.roisByNum.values().iterator();
		while (it.hasNext()) {
			ROI r = it.next();
			sWL.addVal(r.getLen());
			if (r.isSeen()) {
				nrois++;
				lstId.add("" + r.getId());
				sWLS.addVal(r.getLen());
			}
			
			nfpf += r.getNFPF();
			nr   += r.getNR();
			nfpr += r.getNFPR();
			
			// Aggregates:
			if (r.getNF() > 0) {
				sNF.addVal(r.getNF());
				sNFPF.addVal(r.getNFPF());
				sAFD.addVal(r.getAFD());
				sTT.addVal(r.getTT());
				sNR.addVal(r.getNR());
				if (r.getNR() > 0) sRP.addVal(r.getRP());
				if (r.getNFPF() > 0) {
					sFFD.addVal(r.getFFD());
					sGD.addVal(r.getGD());
					sNFPR.addVal(r.getNFPR());
				}
				if (r.getNFPF() == 1) sSFD.addVal(r.getSFD());
			}
			
			if (r.getNB() > 0) {
				sABD.addVal(r.getABD());
				sABSD.addVal(r.getABSD());
				sABSA.addVal(r.getABSA());
			}
			
			// Word fixation counts:
			switch (r.getNF()) {
				case 0  : nwf0++; break;
				case 1  : nwf1++; break;
				default : nwf2++; break;
			}
			
			switch (r.getNFPF()) {
				case 0  : nfpwf0++; break;
				case 1  : nfpwf1++; break;
				default : nfpwf2++; break;
			}
			
			// Word variables:
			if (r.isSeen()) {
				String word = RegExpr.match("[^\\w]*([\\w]*)[^\\s]*", r.getName()).group(1);
				if (w.get(word) != null) sWord.add(w.get(word));
			}
		}
		
		float p1 = ((float) Math.round(((float) nwf1 / (float) nroi) * 100) / 100);
		float p2 = ((float) Math.round(((float) nwf2 / (float) nroi) * 100) / 100);
		float p0 = ((float) Math.round((1 - p1 - p2) * 100) / 100);
		
		float p1s = ((float) Math.round(((float) nwf1 / (float) nrois) * 100) / 100);
		float p2s = ((float) Math.round(((float) nwf2 / (float) nrois) * 100) / 100);
		float p0s = ((float) Math.round((1 - p1s - p2s) * 100) / 100);
		
		float p1fp = ((float) Math.round(((float) nfpwf1 / (float) nrois) * 100) / 100);
		float p2fp = ((float) Math.round(((float) nfpwf2 / (float) nrois) * 100) / 100);
		float p0fp = ((float) Math.round((1 - p1fp - p2fp) * 100) / 100);
		
		double wpm  = util.$.trunc((float) nroi / ((float) dur_act/60000), 2);
		double wpms = util.$.trunc((float) nrois / ((float) dur_act/60000), 2);
		
		int nwf = nwf1 + nwf2;
		int nwf0s = nrois - (nwf1 + nwf2);
		
		double pwf  = util.$.trunc((float) nwf / (float) nroi, 2); 
		double pwfs = util.$.trunc((float) nwf / (float) nrois, 2); 
		
		// Finish up:
		sWL.calc();
		sWLS.calc();
		sAFD.calc();
		sFFD.calc();
		sSFD.calc();
		sGD.calc();
		sTT.calc();
		sRP.calc();
		sABD.calc();
		sABSD.calc();
		sABSA.calc();
		
		List<String> lstVal = new ArrayList<String>();
		
		lstVal.add(Aggr.varGetVal(incVars, "em_t0_exp",  this.t0_exp));
		lstVal.add(Aggr.varGetVal(incVars, "em_tn_exp",  this.tn_exp));
		lstVal.add(Aggr.varGetVal(incVars, "em_dur_act", this.dur_act));
		lstVal.add(Aggr.varGetVal(incVars, "em_dur_nom", this.dur_nom));
		
		lstVal.add(Aggr.varGetVal(incVars, "em_nroi",  nroi)); 
		lstVal.add(Aggr.varGetVal(incVars, "em_nrois", nrois));
		lstVal.add(Aggr.varGetVal(incVars, "em_wpm",   wpm));
		lstVal.add(Aggr.varGetVal(incVars, "em_wpms",  wpms));
		
		lstVal.add(Aggr.varGetVal(incVars, "em_nf",   this.nf));
		lstVal.add(Aggr.varGetVal(incVars, "em_nfpf", nfpf));
		lstVal.add(Aggr.varGetVal(incVars, "em_nr",   nr));
		lstVal.add(Aggr.varGetVal(incVars, "em_nfpr", nfpr));
		lstVal.add(Aggr.varGetVal(incVars, "em_nb",   "null"));  // TODO: fix number of blinks
		
		lstVal.add(Aggr.varGetVal(incVars, "em_wlen",  $.ifNull("" + sWL.getMean(2),   "null")));
		lstVal.add(Aggr.varGetVal(incVars, "em_wlens", $.ifNull("" + sWLS.getMean(2),  "null")));
		lstVal.add(Aggr.varGetVal(incVars, "em_afd",   $.ifNull("" + sAFD.getMean(2),  "null")));
		lstVal.add(Aggr.varGetVal(incVars, "em_ffd",   $.ifNull("" + sFFD.getMean(2),  "null")));
		lstVal.add(Aggr.varGetVal(incVars, "em_sfd",   $.ifNull("" + sSFD.getMean(2),  "null")));
		lstVal.add(Aggr.varGetVal(incVars, "em_gd",    $.ifNull("" + sGD.getMean(2),   "null")));
		lstVal.add(Aggr.varGetVal(incVars, "em_tt",    $.ifNull("" + sTT.getMean(2),   "null")));
		lstVal.add(Aggr.varGetVal(incVars, "em_rp",    $.ifNull("" + sRP.getMean(2),   "null")));
		lstVal.add(Aggr.varGetVal(incVars, "em_abd",   $.ifNull("" + sABD.getMean(2),  "null")));
		lstVal.add(Aggr.varGetVal(incVars, "em_absd",  $.ifNull("" + sABSD.getMean(2), "null")));
		lstVal.add(Aggr.varGetVal(incVars, "em_absa",  $.ifNull("" + sABSA.getMean(2), "null")));
		
		lstVal.add(Aggr.varGetVal(incVars, "em_nwf0",  nwf0));
		lstVal.add(Aggr.varGetVal(incVars, "em_nwf0s", nwf0s));
		lstVal.add(Aggr.varGetVal(incVars, "em_nwf1",  nwf1));
		lstVal.add(Aggr.varGetVal(incVars, "em_nwf2",  nwf2));
		lstVal.add(Aggr.varGetVal(incVars, "em_nwf",   nwf));
		lstVal.add(Aggr.varGetVal(incVars, "em_pwf",   pwf));
		lstVal.add(Aggr.varGetVal(incVars, "em_pwfs",  pwfs));
		
		lstVal.add(Aggr.varGetVal(incVars, "em_notf",    notf));
		lstVal.add(Aggr.varGetVal(incVars, "em_notf_i",  notfI));
		lstVal.add(Aggr.varGetVal(incVars, "em_notf_nw", notfNW));
		lstVal.add(Aggr.varGetVal(incVars, "em_notf_n",  notfN));
		lstVal.add(Aggr.varGetVal(incVars, "em_notf_ne", notfNE));
		lstVal.add(Aggr.varGetVal(incVars, "em_notf_e",  notfE));
		lstVal.add(Aggr.varGetVal(incVars, "em_notf_se", notfSE));
		lstVal.add(Aggr.varGetVal(incVars, "em_notf_s",  notfS));
		lstVal.add(Aggr.varGetVal(incVars, "em_notf_sw", notfSW));
		lstVal.add(Aggr.varGetVal(incVars, "em_notf_w",  notfW));
		
		lstVal.add(Aggr.varGetVal(incVars, "em_nosf",    nosf));
		lstVal.add(Aggr.varGetVal(incVars, "em_nosf_nw", nosfNW));
		lstVal.add(Aggr.varGetVal(incVars, "em_nosf_n",  nosfN));
		lstVal.add(Aggr.varGetVal(incVars, "em_nosf_ne", nosfNE));
		lstVal.add(Aggr.varGetVal(incVars, "em_nosf_e",  nosfE));
		lstVal.add(Aggr.varGetVal(incVars, "em_nosf_se", nosfSE));
		lstVal.add(Aggr.varGetVal(incVars, "em_nosf_s",  nosfS));
		lstVal.add(Aggr.varGetVal(incVars, "em_nosf_sw", nosfSW));
		lstVal.add(Aggr.varGetVal(incVars, "em_nosf_w",  nosfW));
		
		lstVal.add(Aggr.varGetVal(incVars, "em_notb",    notb));
		lstVal.add(Aggr.varGetVal(incVars, "em_notb_i",  notbI));
		lstVal.add(Aggr.varGetVal(incVars, "em_notb_nw", notbNW));
		lstVal.add(Aggr.varGetVal(incVars, "em_notb_n",  notbN));
		lstVal.add(Aggr.varGetVal(incVars, "em_notb_ne", notbNE));
		lstVal.add(Aggr.varGetVal(incVars, "em_notb_e",  notbE));
		lstVal.add(Aggr.varGetVal(incVars, "em_notb_se", notbSE));
		lstVal.add(Aggr.varGetVal(incVars, "em_notb_s",  notbS));
		lstVal.add(Aggr.varGetVal(incVars, "em_notb_sw", notbSW));
		lstVal.add(Aggr.varGetVal(incVars, "em_notb_w",  notbW));
		
		lstVal.add(Aggr.varGetVal(incVars, "em_nosb",    nosb));
		lstVal.add(Aggr.varGetVal(incVars, "em_nosb_nw", nosbNW));
		lstVal.add(Aggr.varGetVal(incVars, "em_nosb_n",  nosbN));
		lstVal.add(Aggr.varGetVal(incVars, "em_nosb_ne", nosbNE));
		lstVal.add(Aggr.varGetVal(incVars, "em_nosb_e",  nosbE));
		lstVal.add(Aggr.varGetVal(incVars, "em_nosb_se", nosbSE));
		lstVal.add(Aggr.varGetVal(incVars, "em_nosb_s",  nosbS));
		lstVal.add(Aggr.varGetVal(incVars, "em_nosb_sw", nosbSW));
		lstVal.add(Aggr.varGetVal(incVars, "em_nosb_w",  nosbW));
		
		/*
		lstVal.addAll(sLen.get(vars,  "em_wlen"));
		lstVal.addAll(sNF.get(vars,   "em_nf"));
		lstVal.addAll(sNFPF.get(vars, "em_nfpf"));
		lstVal.addAll(sNR.get(vars,   "em_nr"));
		lstVal.addAll(sNFPR.get(vars, "em_nfpr"));
		lstVal.addAll(sAFD.get(vars,  "em_afd"));
		lstVal.addAll(sFFD.get(vars,  "em_ffd"));
		lstVal.addAll(sSFD.get(vars,  "em_sfd"));
		lstVal.addAll(sGD.get(vars,   "em_gd"));
		lstVal.addAll(sTT.get(vars,   "em_tt"));
		lstVal.addAll(sRP.get(vars,   "em_rp"));
		*/
		
		lstVal.add(Aggr.varGetVal(incVars, "em_p0",   $.trunc(p0, 6)));
		lstVal.add(Aggr.varGetVal(incVars, "em_p1",   $.trunc(p1, 6)));
		lstVal.add(Aggr.varGetVal(incVars, "em_p2",   $.trunc(p2, 6)));
		lstVal.add(Aggr.varGetVal(incVars, "em_p0s",  $.trunc(p0s, 6)));
		lstVal.add(Aggr.varGetVal(incVars, "em_p1s",  $.trunc(p1s, 6)));
		lstVal.add(Aggr.varGetVal(incVars, "em_p2s",  $.trunc(p2s, 6)));
		lstVal.add(Aggr.varGetVal(incVars, "em_p0fp", $.trunc(p0fp, 6)));
		lstVal.add(Aggr.varGetVal(incVars, "em_p1fp", $.trunc(p1fp, 6)));
		lstVal.add(Aggr.varGetVal(incVars, "em_p2fp", $.trunc(p2fp, 6)));
		
		lstVal.addAll(sWord.getValues(incVars, "null"));
		
		// TODO: getSysVars(lstId)
		
		return Arrays.asList($.join(lstVal, "", "", ",", false, true).toString());
	}
}
