package app.gaze.aggr;

import inter.DBI;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import main.Do;
import main.Result;

import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.Rserve.RserveException;

import app.data.DataSet;
import app.data.Var;
import app.gaze.dstruct.ROIVar;
import app.gaze.dstruct.Word;


/**
 * Calculates aggregate EM measures.
 * 
 * @author Tomek D. Loboda
 */
public class Aggr {
	private static final String COL_NAME_TS = "ts_i";
	
	private final DBI dbi;
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Aggr(DBI dbi) {
		this.dbi = dbi;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Calculates everything. The results are stored in a new data set.
	 * 
	 * TODO: Consider adding these extra variables:
	 * - # of regressions-out
	 * - p(sacc is successive)
	 * - p(sacc is regressive)
	 * - launch site
	 * - landing site
	 */
	public Result calc(String username, int prjId, String anUnit, boolean adj, int expId, String idsSub, String idsTrial, String idsStim, String idsROI, String varsSub, String varsTrial, String varsStim, String varsROI, int trialCal, int trialVal, int maxROIDist, String roiExp, String evts, Long tExp0, Long tExp1, Long tTrial0, Long tTrial1, Long tRec0, Long tRec1, Integer fixDur0, Integer fixDur1, String dsName, String vars, String cons, String req)  throws RserveException, SQLException, REXPMismatchException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		util.Timer timer = new util.Timer();
		
		String dbG = "\"" + Do.DB_GAZE + "-" + username + "\"";
		String dbD = "\"" + Do.DB_DATA + "-" + username + "\"";
		
		int dsId = DBI.ID_NONE;
		DBI dbiX = new DBI();
		Statement stX = dbiX.transBegin();
		
		if (DBI.getLong(dbi, "SELECT id FROM " + dbD + ".ds WHERE prj_id = " + prjId + " AND name = " + DBI.esc(dsName), "id") != DBI.ID_NONE) return new Result(false, "msg:\"A data set with the name specified already exists in this project. Please provide a different name.\\n\\nPlease remember that data sets added through the Pegasus Data application will not be visible in the Pegasus Gaze application, but are still counted as data sets belonging to this project.\"");
		
		try {
			// (1) Process variables and constants:
			Map<String, List<Var>> sysVars = new HashMap<String, List<Var>>();
			sysVars.put("sub"  , new ArrayList<Var>());
			sysVars.put("trial", new ArrayList<Var>());
			sysVars.put("stim" , new ArrayList<Var>());
			sysVars.put("roi"  , new ArrayList<Var>());
			
			Set<String> incVars = null;  // variables to be included in the data set
			if (vars != null && vars.length() > 0) {
				incVars = new HashSet<String>();
				for (String v: vars.split(",")) {
					if (v.length() > 0) incVars.add(v);
				}
			}
			
			// (2) Generate the list of variables and add a new data set:
			List<Var> lstVar = null;
			
			// (2.1) Fixation:
			if (anUnit.equals("fix")) {
				lstVar = getVarsByFix(dbi, username, expId, incVars, sysVars);
				dsId = addDataSet(stX, username, prjId, dsName, expId, COL_NAME_TS, lstVar);
			}
			
			// (2.2) ROI (curr):
			else if (anUnit.equals("roi")) {
				lstVar = getVarsByROI(dbi, username, expId, incVars, sysVars);
				dsId = addDataSet(stX, username, prjId, dsName, expId, null, lstVar);
			}
			
			// (2.2) ROI (curr, prev, next: order-adjacent):
			else if (anUnit.equals("roi-prev-next-adj-ord")) {
				lstVar = getVarsByROIPrevNext(dbi, username, expId, incVars, sysVars);
				dsId = addDataSet(stX, username, prjId, dsName, expId, null, lstVar);
			}
			
			// (2.3) ROI (curr, prev, next: fixation-adjacent):
			else if (anUnit.equals("roi-prev-next-adj-fix")) {
				lstVar = getVarsByROIPrevNext(dbi, username, expId, incVars, sysVars);
				dsId = addDataSet(stX, username, prjId, dsName, expId, null, lstVar);
			}
			
			// (2.3) Stimulus sequence (event):
			else if (anUnit.equals("s-seq")) {
				lstVar = getVarsByStimSeq(dbi, username, expId, incVars, sysVars);
				dsId = addDataSet(stX, username, prjId, dsName, expId, null, lstVar);
				
				/*
				// System variables:
				Statement stSV = dbi.newSt();
				ResultSet rsSV = stSV.executeQuery("SELECT id, name, \"type\" FROM " + db + ".var v WHERE exp_id = " + expId + " AND obj_name = 'roi'");
				while (rsSV.next()) {
					SysVar sv = new SysVar(dbi, username, rsSV.getInt("id"), rsSV.getString("name"), rsSV.getString("type").charAt(0));
					lstSysVars.add(sv);
					sbV.append(sv.getColNames() + (!rsSV.isLast() ? "\t" : ""));
				}
				rsSV.close();
				stSV.close();
				sbV.append("\n");
				*/
			}
			
			// (3) Generate and execute the query, calculate the aggregates:
			// (3.1) Trial calibration and validation:
			String qryTrialCalVal = "";
			
			switch (trialCal) {
				case 1: qryTrialCalVal = "AND c.cal_res = 'good' "; break;
				case 2: qryTrialCalVal = "AND c.cal_res = 'fair' "; break;
				case 3: qryTrialCalVal = "AND c.cal_res = 'poor' "; break;
				case 4: qryTrialCalVal = "AND c.cal_res IN ('fair','good') "; break;
				case 5: qryTrialCalVal = "AND c.cal_res IN ('fair','poor') "; break;
			}
			
			switch (trialVal) {
				case 1: qryTrialCalVal += (trialCal > 0 ? "AND " : "") + "c.val_res = 'good' "; break;
				case 2: qryTrialCalVal += (trialCal > 0 ? "AND " : "") + "c.val_res = 'fair' "; break;
				case 3: qryTrialCalVal += (trialCal > 0 ? "AND " : "") + "c.val_res = 'poor' "; break;
				case 4: qryTrialCalVal += (trialCal > 0 ? "AND " : "") + "c.val_res IN ('fair','good') "; break;
				case 5: qryTrialCalVal += (trialCal > 0 ? "AND " : "") + "c.val_res IN ('fair','poor') "; break;
			}
			
			// (3.2) The rest:
			String roiIds = getVarConstrIds(username, "roi",  expId, varsROI);
			roiIds = expandROIs(username, roiIds, expId, roiExp);
			
			ResultSet rsSS = dbi.execQry(
				"SELECT ss.id, (ss.t1 - ss.t0) AS dur, s01.id AS stim_id, s01.name AS stim_name, t.name AS trial_name, s02.name AS sub_name " +
				"FROM " + dbG + ".stim_seq ss " +
				"INNER JOIN " + dbG + ".stim s01 ON ss.stim_id = s01.id " +
				"INNER JOIN " + dbG + ".trial t ON ss.trial_id = t.id " +
				"INNER JOIN " + dbG + ".sub s02 ON s02.id = t.sub_id " +
				(qryTrialCalVal.length() > 0 ? "INNER JOIN (SELECT trial_id, cal_res, val_res FROM " + dbG + ".cal WHERE id IN (SELECT MAX(id) FROM " + dbG + ".cal GROUP BY trial_id)) c ON c.trial_id = t.id " : "") +
				"WHERE s01.exp_id = " + expId + " AND NOT (s01.exc OR s02.exc OR t.exc OR ss.exc) " +
				qryTrialCalVal +
				//(tRead0    != null ? " AND ss.t1 >= " + tRead0 : "") +
				//(tRead1    != null ? " AND ss.t0 <  " + tRead1 : "") +
				(idsStim   != null && idsStim.length()   > 0 ? " AND ss.stim_id IN ("  + idsStim  + ") " : "") +
				(idsTrial  != null && idsTrial.length()  > 0 ? " AND ss.trial_id IN (" + idsTrial + ") " : "") +
				(idsSub    != null && idsSub.length()    > 0 ? " AND s02.id IN ("      + idsSub   + ") " : "") +
				(varsStim  != null && varsStim.length()  > 0 ? " AND s01.id IN ("      + getVarConstrIds(username, "stim",  expId, varsStim)  + ") " : "") +
				(varsTrial != null && varsTrial.length() > 0 ? " AND ss.trial_id IN (" + getVarConstrIds(username, "trial", expId, varsTrial) + ") " : "") +
				(varsSub   != null && varsSub.length()   > 0 ? " AND s02.id IN ("      + getVarConstrIds(username, "sub",   expId, varsSub)   + ") " : "") +
				"ORDER BY ss.id"
			);
			while (rsSS.next()) {
				StimSeq ss = new StimSeq(dbi, username, anUnit, adj, rsSS.getInt("id"), rsSS.getInt("dur"), rsSS.getInt("stim_id"), rsSS.getString("stim_name"), rsSS.getString("trial_name"), rsSS.getString("sub_name"), roiIds, fixDur0, fixDur1, evts, sysVars, maxROIDist, tExp0, tExp1, tTrial0, tTrial1, tRec0, tRec1);
				ss.calc(stX, username, dsId, incVars, getSQLVars(lstVar));
				stX.executeUpdate(DataSet.getSQLUpdateMetaInfo(dbiX, username, dsId, timer.elapsed()));
			}
			rsSS.close();
			
			/*
			// (4) Retrieve the list of stimulus sequences:
			System.out.println("Heap free (before stim-seq): " + (Runtime.getRuntime().freeMemory() % 1024) + " MB");
			List<StimSeq> lstSS = new ArrayList<StimSeq>();
			
			String roiIds = getVarConstrIds(username, "roi",  expId, varsROI);
			roiIds = expandROIs(username, roiIds, expId, roiExp);
			
			ResultSet rsSS = dbi.execQry(
				"SELECT ss.id, (ss.t1-ss.t0) AS dur, s01.id AS stim_id, s01.name AS stim_name, t.name AS trial_name, s02.name AS sub_name " +
				"FROM " + dbG + ".stim_seq ss " +
				"INNER JOIN " + dbG + ".stim s01 ON ss.stim_id = s01.id " +
				"INNER JOIN " + dbG + ".trial t ON ss.trial_id = t.id " +
				"INNER JOIN " + dbG + ".sub s02 ON s02.id = t.sub_id " +
				"WHERE s01.exp_id = " + expId + " AND NOT (s01.exc OR s02.exc OR t.exc OR ss.exc) " +
				(idsStim   != null && idsStim.length()   > 0 ? " AND ss.stim_id IN ("  + idsStim  + ") " : "") +
				(idsTrial  != null && idsTrial.length()  > 0 ? " AND ss.trial_id IN (" + idsTrial + ") " : "") +
				(idsSub    != null && idsSub.length()    > 0 ? " AND s02.id IN ("      + idsSub   + ") " : "") +
				(varsStim  != null && varsStim.length()  > 0 ? " AND s01.id IN ("      + getVarConstrIds(username, "stim",  expId, varsStim)  + ") " : "") +
				(varsTrial != null && varsTrial.length() > 0 ? " AND ss.trial_id IN (" + getVarConstrIds(username, "trial", expId, varsTrial) + ") " : "") +
				(varsSub   != null && varsSub.length()   > 0 ? " AND s02.id IN ("      + getVarConstrIds(username, "sub",   expId, varsSub)   + ") " : "") +
				"ORDER BY ss.id"
			);
			while (rsSS.next()) {
				lstSS.add(new StimSeq(dbi, username, anUnit, adj, rsSS.getInt("id"), rsSS.getInt("dur"), rsSS.getInt("stim_id"), rsSS.getString("stim_name"), rsSS.getString("trial_name"), rsSS.getString("sub_name"), roiIds, fixDur0, fixDur1, evts, sysVarsROI, maxROIDist));
			}
			rsSS.close();
			System.out.println("Heap free (after stim-seq): " + (Runtime.getRuntime().freeMemory() % 1024) + " MB");
			
			// (5) Process stimuli sequences:
			int obsCnt = -1;
			Iterator<StimSeq> it = lstSS.iterator();
			while (it.hasNext()) {
				StimSeq ss = it.next();
				System.out.println("Heap free (before stim-seq processing): " + (Runtime.getRuntime().freeMemory() % 1024) + " MB");
				obsCnt = ss.calc(stX, username, dsId, incVars, getSQLVars(lstVar));
				System.out.println("Heap free (after stim-seq processing): " + (Runtime.getRuntime().freeMemory() % 1024) + " MB");
			}
			
			// (6) Finish up:
			stX.executeUpdate("UPDATE " + dbD + ".ds SET obs_cnt = " + obsCnt + ", calc_time = " + timer.elapsed() + ", size = (" + DBI.REC_SIZE_DATA_SET + " + " + DBI.getLong(dbi, "SELECT LENGTH(name) AS len FROM " + dbD + ".ds WHERE id = " + dsId, "len") + " + " + DBI.getLong(dbi, "SELECT LENGTH(memo) AS len FROM " + dbD + ".ds WHERE id = " + dsId, "len") + " + " + (DBI.REC_SIZE_VAR * lstVar.size()) + " + " + DBI.getTblSize(dbi, Do.DB_DATA_DS + "-" + username, "ds_" + dsId) + ") WHERE id = " + dsId);
			*/
			
			DBI.transCommit(stX, true);
		}
		catch (SQLException e)           { DBI.transRollback(stX, true); throw e; }
		/*
		catch (InstantiationException e) { DBI.transRollback(stX, true); throw e; }
		catch (IllegalAccessException e) { DBI.transRollback(stX, true); throw e; }
		catch (ClassNotFoundException e) { DBI.transRollback(stX, true); throw e; }
		*/
		
		Runtime.getRuntime().gc();
		return new Result(true, new StringBuffer("ds:" + DataSet.get(dbi, username, dsId)));
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	private int addDataSet(Statement stX, String username, int prjId, String name, int expId, String tsVar, List<Var> vars)  throws SQLException {
		String dbData = "\"" + Do.DB_DATA + "-" + username + "\"";
		
		int dsId = DBI.insertGetId(stX, null, "INSERT INTO " + dbData + ".ds (prj_id,app_id,name,ts_var) VALUES (" + prjId + "," + Do.getAppIdGaze() + "," + DBI.esc(name) + "," + DBI.esc(tsVar) + ")");
		stX.executeUpdate("INSERT INTO \"" + Do.DB_GAZE + "-" + username + "\".ds (exp_id, ds_id) VALUES (" + expId + "," + dsId + ")");
		
		StringBuffer qryTbl = new StringBuffer(
			"CREATE TABLE \"" + Do.DB_DATA_DS + "-" + username + "\".\"ds_" + dsId + "\" (" +
			"\"" + DataSet.ID_COL_NAME + "\" serial,"
		);
		
		for (int i = 0; i < vars.size(); i++) {
			Var v = vars.get(i);
			stX.executeUpdate("INSERT INTO " + dbData + ".var (ds_id, type_id, name, ord) VALUES (" + dsId + "," + v.getType() + "," + DBI.esc(v.getName()) + "," + i + ")");
			qryTbl.append("\"" + v.getName() + "\" " + Var.getSQLColType(v.getType()) + " NULL,");
		}
		
		qryTbl.append("PRIMARY KEY (\"" + DataSet.ID_COL_NAME + "\"))");
		
		stX.executeUpdate(qryTbl.toString());
		
		return dsId;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Takes the `ids` string containing comma-separated ROI IDs and expands that selection to either a clause 
	 * (exp = 'c') or a sentence (exp = 's'). Nothing is done if `ids` has a different value.
	 */
	private String expandROIs(String username, String ids, int expId, String exp)  throws SQLException {
		String db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		
		if (ids.length() == 0 || (!exp.equals("s") && !exp.equals("c"))) return ids;
		
		Hashtable<Integer,ROIVar> RV = new Hashtable<Integer,ROIVar>();  // we want the key to be String to quickly use ids from the 'ids' string
		String[] ID = ids.split(",");
		Set<String> res = new HashSet<String>();
		
		boolean expC = exp.equals("c");
		
		// (1) Read all ROIs with relevant variables:
		int idMin = DBI.ID_NONE;
		int idMax = DBI.ID_NONE;
		
		Statement st = dbi.newSt();
		st.executeQuery("USE " + db);
		ResultSet rs = st.executeQuery(
			"SELECT r.id, ss.val AS ss, se.val AS se, cs.val AS cs, ce.val AS ce FROM roi r " +
			"INNER JOIN stim s ON r.stim_id = s.id " +
			
			"LEFT JOIN ( " +
			"  SELECT x.roi_id, x.val FROM xr_var_roi x " +
			"  INNER JOIN var v ON x.var_id = v.id " +
			"  WHERE v.exp_id = " + expId + " AND v.name = 'sent-s' " +
			") ss ON ss.roi_id = r.id " +
			
			"LEFT JOIN (" +
			"  SELECT x.roi_id, x.val FROM xr_var_roi x " +
			"  INNER JOIN var v ON x.var_id = v.id " +
			"  WHERE v.exp_id = " + expId + " AND v.name = 'sent-e' " +
			") se ON se.roi_id = r.id " +
			
			"LEFT JOIN (" +
			"  SELECT x.roi_id, x.val FROM xr_var_roi x " +
			"  INNER JOIN var v ON x.var_id = v.id " +
			"  WHERE v.exp_id = " + expId + " AND v.name = 'cl-s' " +
			") cs ON cs.roi_id = r.id " +
			
			"LEFT JOIN (" +
			"  SELECT x.roi_id, x.val FROM xr_var_roi x " +
			"  INNER JOIN var v ON x.var_id = v.id " +
			"  WHERE v.exp_id = " + expId + " AND v.name = 'cl-e' " +
			") ce ON ce.roi_id = r.id " +
			
			"WHERE s.exp_id = " + expId + " " +
			"ORDER BY r.id"
		);
		
		while (rs.next()) {
			if (rs.isFirst()) idMin = rs.getInt("id");
			if (rs.isLast()) idMax = rs.getInt("id");
			RV.put(rs.getInt("id"), new ROIVar(rs.getInt("id"), rs.getBoolean("ss"), rs.getBoolean("se"), rs.getBoolean("cs"), rs.getBoolean("ce")));
		}
		
		rs.close();
		st.close();
		
		// (2) Expand:
		// TODO: make use of the 'ROIVar.done' attribute
		for (int i = 0; i < ID.length; i++) {
			int id = new Integer(ID[i]);
			res.add(ID[i]);
			
			ROIVar rvStart = RV.get(id);
			
			// (2.1) Go left:
			ROIVar rvCurr = rvStart;
			int j = id;
			
			while (j >= idMin && (expC ? !rvCurr.cs : true) && !rvCurr.ss) {
				do { rvCurr = RV.get(--j); } while (j >= idMin && rvCurr == null);
				if (rvCurr != null) res.add("" + rvCurr.id);
			}
			
			// (2.2) Go right:
			rvCurr = rvStart;
			j = id;
			
			while (j <= idMax && (expC ? !rvCurr.ce : true) && !rvCurr.se) {
				do { rvCurr = RV.get(++j); } while (j <= idMax && rvCurr == null);
				if (rvCurr != null) res.add("" + rvCurr.id);
			}
		}
		
		// (3) Contruct the return value:
		StringBuilder sb = new StringBuilder();
		Iterator<String> it = res.iterator();
		while (it.hasNext()) sb.append(it.next() + (it.hasNext() ? "," : ""));
		
		return sb.toString();
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/*
	private int getMaxTSLen(List<StimSeq> lstSS) {
		int max = 0;
		Iterator<StimSeq> it = lstSS.iterator();
		while (it.hasNext()) {
			int curr = it.next().getMaxTSLen();
			if (curr > max) max = curr;
		}
		return max;
	}
	*/
	
	
	// -----------------------------------------------------------------------------------------------------------------
	private String getVarConstrIds(String username, String obj, int expId, String constr)  throws SQLException {
		String dbG = "\"" + Do.DB_GAZE + "-" + username + "\"";
		
		if (constr == null || (constr != null && constr.length() == 0)) return "";
		
		// (1) Create the query:
		StringBuffer qry = new StringBuffer();
		String qryPre = "SELECT id FROM (SELECT v0.id, ";
		String qryPost = "WHERE ";
		
		String[] P = constr.split(";");  // name-value pairs
		for (int i = 0; i < P.length; i++) {
			String p = P[i];
			int idx = p.indexOf(":");
			String n = p.substring(0, idx);
			String v = p.substring(idx+1);
			
			qryPre  += "v" + i + ".\"" + n + "\"" + (i < P.length-1 ? ", " : " ");
			qryPost += v + (i < P.length-1 ? " AND " : "");
			
			qry.append(
				"(" +
				"SELECT r.id, 1 AS \"" + n + "\" FROM roi r " +
				"INNER JOIN stim s ON s.id = r.stim_id " +
				"LEFT JOIN xr_var_roi x ON x.roi_id = r.id " +
				"LEFT JOIN var v ON v.id = x.var_id " +
				"WHERE s.exp_id = " + expId + " AND v.name = '" + n + "' " +
				
				"UNION " +
				
				"SELECT r.id, 0 AS \"" + n + "\" FROM roi r " +
				"LEFT JOIN " +
				"( " +
				"SELECT r.id FROM roi r " +
				"INNER JOIN stim s ON s.id = r.stim_id " +
				"LEFT JOIN xr_var_roi x ON x.roi_id = r.id " +
				"LEFT JOIN var v ON v.id = x.var_id " +
				"WHERE s.exp_id = " + expId + " AND v.name = '" + n + "' " +
				") tmp ON tmp.id = r.id " +
				"INNER JOIN stim s ON s.id = r.stim_id " +
				"WHERE s.exp_id = " + expId + " AND tmp.id IS NULL" +
				") v" + i + " "
			);
			if (i > 0) {
				qry.append(" ON v" + i + ".id = v" + (i-1) + ".id ");
			}
			if (i < P.length-1) {
				qry.append("INNER JOIN ");
			}
		}
		qryPre  += "FROM ";
		qryPost += ") a";
		qry.insert(0, qryPre);
		qry.append(qryPost);
		
		// (2) Get the ROI ids:
		StringBuffer ids = new StringBuffer();
		
		Statement st = dbi.newSt();
		st.execute("SET search_path TO " + dbG);
		ResultSet rs = st.executeQuery(qry.toString());
		while (rs.next()) ids.append(rs.getString("id") + (!rs.isLast() ? "," : ""));
		rs.close();
		st.close();
		
		return ids.toString();
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Returns the list of variables for the INSERT query.
	 */
	private String getSQLVars(List<Var> vars) {
		if (vars == null) return "";
		
		StringBuilder res = new StringBuilder();
		
		Iterator<Var> it = vars.iterator();
		while (it.hasNext()) {
			res.append("\"" + it.next().getName() + "\"" + (it.hasNext() ? "," : ""));
		}
		
		return res.toString();
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Returns a list of variables used for the by-fixation calculation.
	 */
	private List<Var> getVarsByFix(DBI dbi, String username, int expId, Set<String> incVars, Map<String,List<Var>> sysVars)  throws SQLException {
		List<Var> res = new ArrayList<Var>();
		
		// (1) General:
		res.add(new Var(DBI.ID_NONE, Var.TYPE_STR, "sub"  , null));  res.addAll(getSysVars01(dbi, username, expId, incVars, sysVars.get("sub")  , "sub"  , "", false));
		res.add(new Var(DBI.ID_NONE, Var.TYPE_STR, "trial", null));  res.addAll(getSysVars01(dbi, username, expId, incVars, sysVars.get("trial"), "trial", "", false));
		res.add(new Var(DBI.ID_NONE, Var.TYPE_STR, "stim" , null));  res.addAll(getSysVars01(dbi, username, expId, incVars, sysVars.get("stim") , "stim" , "", false));
		
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_t0_exp",  "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_tn_exp",  "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_dur_act", "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_dur_nom", "");
		
		res.add(new Var(DBI.ID_NONE, Var.TYPE_DIS, COL_NAME_TS, null));
		
		// (2) Fixation:
		varLstAdd(res, incVars, Var.TYPE_STR, "em_roi",        "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_t_exp",      "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_t_trial",    "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_dur",        "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_wlen",       "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_f",          "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_fp",         "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_let",        "");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_let_norm",   "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_sacc_i_roi", "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_sacc_o_roi", "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_sacc_i_let", "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_sacc_o_let", "");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_pupil",      "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_ot_dir",     "");
		varLstAdd(res, incVars, Var.TYPE_BIN, "em_ot_bin",     "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_os_dir",     "");
		varLstAdd(res, incVars, Var.TYPE_BIN, "em_os_bin",     "");
		
		// (3) Word and system variables:
		res.addAll(Word.getVars(incVars, true, ""));
		res.addAll(getSysVars01(dbi, username, expId, incVars, sysVars.get("roi"), "roi", "", false));
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Returns a list of variables used for the by-ROI calculation.
	 */
	private List<Var> getVarsByROI(DBI dbi, String username, int expId, Set<String> incVars, Map<String,List<Var>> sysVars)  throws SQLException {
		List<Var> res = new ArrayList<Var>();
		
		// (1) General:
		res.add(new Var(DBI.ID_NONE, Var.TYPE_STR, "sub"  , null));  res.addAll(getSysVars01(dbi, username, expId, incVars, sysVars.get("sub")  , "sub"  , "", false));
		res.add(new Var(DBI.ID_NONE, Var.TYPE_STR, "trial", null));  res.addAll(getSysVars01(dbi, username, expId, incVars, sysVars.get("trial"), "trial", "", false));
		res.add(new Var(DBI.ID_NONE, Var.TYPE_STR, "stim" , null));  res.addAll(getSysVars01(dbi, username, expId, incVars, sysVars.get("stim") , "stim" , "", false));
		
		res.add(new Var(DBI.ID_NONE, Var.TYPE_DIS, "em_nevt", null));
		
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_t0_exp",  "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_tn_exp",  "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_dur_act", "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_dur_nom", "");
		
		// (2) ROI:
		// (2.1) EM variables:
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_num",            "");
		varLstAdd(res, incVars, Var.TYPE_STR, "em_roi",            "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_wlen",           "");
		
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nf",             "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nfpf",           "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nspf",           "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nr",             "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nfpr",           "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nspr",           "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nb",             "");
		
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_fpfd_min",       "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_fpfd_max",       "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_spfd_min",       "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_spfd_max",       "");
		
		varLstAdd(res, incVars, Var.TYPE_CON, "em_afd",            "");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_ffd",            "");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_sfd",            "");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_gd",             "");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_tt",             "");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_rp",             "");
		
		varLstAdd(res, incVars, Var.TYPE_CON, "em_abd",            "");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_absd",           "");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_absa",           "");
		
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_let_i_fp",       "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_let_o_fp",       "");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_let_i_avg",      "");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_let_o_avg",      "");
		
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_sacc_i_roi_fp",  "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_sacc_o_roi_fp",  "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_sacc_i_let_fp",  "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_sacc_o_let_fp",  "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_sacc_i_roi_avg", "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_sacc_o_roi_avg", "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_sacc_i_let_avg", "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_sacc_o_let_avg", "");
		
		varLstAdd(res, incVars, Var.TYPE_CON, "em_pupil",          "");
		
		// (2.2) Word and system varables:
		res.addAll(Word.getVars(incVars, true, ""));
		res.addAll(getSysVars01(dbi, username, expId, incVars, sysVars.get("roi"), "roi", "", false));
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Returns a list of variables used for the by-ROI (prev, next; adjacent and fixed) calculation.
	 */
	private List<Var> getVarsByROIPrevNext(DBI dbi, String username, int expId, Set<String> incVars, Map<String,List<Var>> sysVars)  throws SQLException {
		List<Var> res = new ArrayList<Var>();
		
		// (1) General:
		res.add(new Var(DBI.ID_NONE, Var.TYPE_STR, "sub"  , null));  res.addAll(getSysVars01(dbi, username, expId, incVars, sysVars.get("sub")  , "sub"  , "", false));
		res.add(new Var(DBI.ID_NONE, Var.TYPE_STR, "trial", null));  res.addAll(getSysVars01(dbi, username, expId, incVars, sysVars.get("trial"), "trial", "", false));
		res.add(new Var(DBI.ID_NONE, Var.TYPE_STR, "stim" , null));  res.addAll(getSysVars01(dbi, username, expId, incVars, sysVars.get("stim") , "stim" , "", false));
		
		res.add(new Var(DBI.ID_NONE, Var.TYPE_DIS, "em_nevt", null));
		
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_t0_exp",  "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_tn_exp",  "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_dur_act", "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_dur_nom", "");
		
		// (2) Current ROI:
		// (2.1) EM variables:
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_num",            "c_");
		varLstAdd(res, incVars, Var.TYPE_STR, "em_roi",            "c_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_wlen",           "c_");
		
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nf",             "c_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nfpf",           "c_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nspf",           "c_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nr",             "c_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nfpr",           "c_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nspr",           "c_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nb",             "c_");
		
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_fpfd_min",       "c_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_fpfd_max",       "c_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_spfd_min",       "c_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_spfd_max",       "c_");
		
		varLstAdd(res, incVars, Var.TYPE_CON, "em_afd",            "c_");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_ffd",            "c_");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_sfd",            "c_");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_gd",             "c_");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_tt",             "c_");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_rp",             "c_");
		
		varLstAdd(res, incVars, Var.TYPE_CON, "em_abd",            "c_");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_absd",           "c_");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_absa",           "c_");
		
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_let_i_fp",       "c_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_let_o_fp",       "c_");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_let_i_avg",      "c_");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_let_o_avg",      "c_");
		
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_sacc_i_roi_fp",  "c_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_sacc_o_roi_fp",  "c_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_sacc_i_let_fp",  "c_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_sacc_o_let_fp",  "c_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_sacc_i_roi_avg", "c_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_sacc_o_roi_avg", "c_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_sacc_i_let_avg", "c_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_sacc_o_let_avg", "c_");
		
		varLstAdd(res, incVars, Var.TYPE_CON, "em_pupil",          "c_");
		
		// (2.2) Word and system varables:
		res.addAll(Word.getVars(incVars, true, "c_"));
		res.addAll(getSysVars01(dbi, username, expId, incVars, sysVars.get("roi"), "roi", "c_", false));
		
		// (3) Previous ROI:
		// (3.1) EM variables:
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_seen",           "p_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_num",            "p_");
		varLstAdd(res, incVars, Var.TYPE_STR, "em_roi",            "p_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_wlen",           "p_");
		
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nf",             "p_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nfpf",           "p_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nspf",           "p_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nr",             "p_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nfpr",           "p_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nspr",           "p_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nb",             "p_");
		
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_fpfd_min",       "p_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_fpfd_max",       "p_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_spfd_min",       "p_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_spfd_max",       "p_");
		
		varLstAdd(res, incVars, Var.TYPE_CON, "em_afd",            "p_");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_ffd",            "p_");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_sfd",            "p_");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_gd",             "p_");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_tt",             "p_");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_rp",             "p_");
		
		varLstAdd(res, incVars, Var.TYPE_CON, "em_abd",            "p_");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_absd",           "p_");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_absa",           "p_");
		
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_let_i_fp",       "p_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_let_o_fp",       "p_");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_let_i_avg",      "p_");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_let_o_avg",      "p_");
		
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_sacc_i_roi_fp",  "p_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_sacc_o_roi_fp",  "p_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_sacc_i_let_fp",  "p_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_sacc_o_let_fp",  "p_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_sacc_i_roi_avg", "p_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_sacc_o_roi_avg", "p_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_sacc_i_let_avg", "p_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_sacc_o_let_avg", "p_");
		
		varLstAdd(res, incVars, Var.TYPE_CON, "em_pupil",          "p_");
		
		// (3.2) Word and system varables:
		res.addAll(Word.getVars(incVars, true, "p_"));
		res.addAll(getSysVars01(dbi, username, expId, incVars, null, "roi", "p_", false));
		
		// (4) Next ROI:
		// (4.1) EM variables:
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_seen",           "n_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_num",            "n_");
		varLstAdd(res, incVars, Var.TYPE_STR, "em_roi",            "n_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_wlen",           "n_");
		
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nf",             "n_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nfpf",           "n_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nspf",           "n_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nr",             "n_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nfpr",           "n_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nspr",           "n_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nb",             "n_");
		
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_fpfd_min",       "n_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_fpfd_max",       "n_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_spfd_min",       "n_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_spfd_max",       "n_");
		
		varLstAdd(res, incVars, Var.TYPE_CON, "em_afd",            "n_");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_ffd",            "n_");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_sfd",            "n_");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_gd",             "n_");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_tt",             "n_");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_rp",             "n_");
		
		varLstAdd(res, incVars, Var.TYPE_CON, "em_abd",            "n_");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_absd",           "n_");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_absa",           "n_");
		
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_let_i_fp",       "n_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_let_o_fp",       "n_");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_let_i_avg",      "n_");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_let_o_avg",      "n_");
		
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_sacc_i_roi_fp",  "n_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_sacc_o_roi_fp",  "n_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_sacc_i_let_fp",  "n_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_sacc_o_let_fp",  "n_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_sacc_i_roi_avg", "n_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_sacc_o_roi_avg", "n_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_sacc_i_let_avg", "n_");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_sacc_o_let_avg", "n_");
		
		varLstAdd(res, incVars, Var.TYPE_CON, "em_pupil",          "n_");
		
		// (4.2) Word and system varables:
		res.addAll(Word.getVars(incVars, true, "n_"));
		res.addAll(getSysVars01(dbi, username, expId, incVars, null, "roi", "n_", false));
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Returns a list of variables used for the by-stim-seq or by-event calculation.
	 */
	private List<Var> getVarsByStimSeq(DBI dbi, String username, int expId, Set<String> incVars, Map<String,List<Var>> sysVars) throws SQLException {
		List<Var> res = new ArrayList<Var>();
		
		// (1) General:
		res.add(new Var(DBI.ID_NONE, Var.TYPE_STR, "sub"  , null));  res.addAll(getSysVars01(dbi, username, expId, incVars, sysVars.get("sub")  , "sub"  , "", false));
		res.add(new Var(DBI.ID_NONE, Var.TYPE_STR, "trial", null));  res.addAll(getSysVars01(dbi, username, expId, incVars, sysVars.get("trial"), "trial", "", false));
		res.add(new Var(DBI.ID_NONE, Var.TYPE_STR, "stim" , null));  res.addAll(getSysVars01(dbi, username, expId, incVars, sysVars.get("stim") , "stim" , "", false));
		
		res.add(new Var(DBI.ID_NONE, Var.TYPE_DIS, "em_nevt", null));
		
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_t0_exp",  "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_tn_exp",  "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_dur_act", "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_dur_nom", "");
		
		// (2) Stim-seq:
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nroi",  "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nrois", "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_wpm",   "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_wpms",  "");
		
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nf",   "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nfpf", "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nr",   "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nfpr", "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nb",   "");
		
		varLstAdd(res, incVars, Var.TYPE_CON, "em_wlen",  "");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_wlens", "");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_afd",   "");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_ffd",   "");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_sfd",   "");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_gd",    "");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_tt",    "");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_rp",    "");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_abd",   "");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_absd",  "");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_absa",  "");
		
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nwf0",  "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nwf0s", "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nwf1",  "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nwf2",  "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nwf",   "");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_pwf",   "");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_pwfs",  "");
		
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_notf",    "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_notf_i",  "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_notf_nw", "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_notf_n",  "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_notf_ne", "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_notf_e",  "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_notf_se", "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_notf_s",  "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_notf_sw", "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_notf_w",  "");
		
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nosf",    "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nosf_nw", "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nosf_n",  "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nosf_ne", "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nosf_e",  "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nosf_se", "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nosf_s",  "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nosf_sw", "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nosf_w",  "");
		
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_notb",    "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_notb_i",  "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_notb_nw", "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_notb_n",  "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_notb_ne", "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_notb_e",  "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_notb_se", "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_notb_s",  "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_notb_sw", "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_notb_w",  "");
		
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nosb",    "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nosb_nw", "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nosb_n",  "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nosb_ne", "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nosb_e",  "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nosb_se", "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nosb_s",  "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nosb_sw", "");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nosb_w",  "");
		
		/*
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_wlen_min");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_wlen_max");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_wlen_mean");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_wlen_med");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_wlen_sd");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_wlen_se");
		
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nf_min");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nf_max");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_nf_mean");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_nf_med");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_nf_sd");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_nf_se");
		
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nfpf_min");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nfpf_max");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_nfpf_mean");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_nfpf_med");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_nfpf_sd");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_nfpf_se");
		
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nr_min");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nr_max");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_nr_mean");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_nr_med");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_nr_sd");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_nr_se");
		
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nfpr_min");
		varLstAdd(res, incVars, Var.TYPE_DIS, "em_nfpr_max");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_nfpr_mean");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_nfpr_med");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_nfpr_sd");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_nfpr_se");
		
		varLstAdd(res, incVars, Var.TYPE_CON, "em_afd_min");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_afd_max");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_afd_mean");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_afd_med");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_afd_sd");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_afd_se");
		
		varLstAdd(res, incVars, Var.TYPE_CON, "em_ffd_min");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_ffd_max");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_ffd_mean");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_ffd_med");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_ffd_sd");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_ffd_se");
		
		varLstAdd(res, incVars, Var.TYPE_CON, "em_sfd_min");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_sfd_max");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_sfd_mean");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_sfd_med");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_sfd_sd");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_sfd_se");
		
		varLstAdd(res, incVars, Var.TYPE_CON, "em_gd_min");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_gd_max");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_gd_mean");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_gd_med");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_gd_sd");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_gd_se");
		
		varLstAdd(res, incVars, Var.TYPE_CON, "em_tt_min");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_tt_max");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_tt_mean");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_tt_med");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_tt_sd");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_tt_se");
		
		varLstAdd(res, incVars, Var.TYPE_CON, "em_rp_min");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_rp_max");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_rp_mean");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_rp_med");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_rp_sd");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_rp_se");
		*/
		
		varLstAdd(res, incVars, Var.TYPE_CON, "em_p0",   "");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_p1",   "");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_p2",   "");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_p0s",  "");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_p1s",  "");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_p2s",  "");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_p0fp", "");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_p1fp", "");
		varLstAdd(res, incVars, Var.TYPE_CON, "em_p2fp", "");
		
		res.addAll(Word.getVars(incVars, false, ""));
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Because this method can be called more than once per a single calculation of a data set, it is essential that 
	 * the 'sysVars' variable is non-null only the first time it is passed here.
	 */
	private static List<Var> getSysVars01(DBI dbi, String username, int expId, Set<String> incVars, List<Var> sysVars, String objName, String prefix, boolean aggr)  throws SQLException {
		String dbG = "\"" + Do.DB_GAZE + "-" + username + "\"";
		List<Var> res = new ArrayList<Var>();
		
		ResultSet rs = dbi.execQry("SELECT id, name, type, def_val FROM " + dbG + ".var WHERE exp_id = " + expId + " AND obj_name = '" + objName + "' ORDER BY name");
		while (rs.next()) {
			String name = objName + "_" + rs.getString("name").replace("-", "_");
			if (incVars != null && incVars.size() > 0 && !incVars.contains(name)) continue;
			
			if (sysVars != null) sysVars.add(new Var(rs.getInt("id"), getSysVars02_getType(rs.getString("type").charAt(0), aggr), rs.getString("name"), rs.getString("def_val")));
			res.add(new Var(rs.getInt("id"), getSysVars02_getType(rs.getString("type").charAt(0), aggr), prefix + name, rs.getString("def_val")));
		}
		rs.close();
		
		return res;
	}
	
	
	// - - - -
	private static int getSysVars02_getType(char type, boolean aggr) {
		if (aggr) {
			if (type == 's') return Var.TYPE_STR;
			else return Var.TYPE_CON;
		}
		else {
			switch (type) {
				case 'b' : return Var.TYPE_BIN;
				case 'n' : return Var.TYPE_CON;  // Var.TYPE_DIS
				default  : return Var.TYPE_STR;
			}
		}
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Returns a list of variables associated with the given object name (sub, trial, or stimulus).
	 */
	/*
	private List<Var> getSysVars02(DBI dbi, String username, int expId, Set<String> incVars, List<Var> sysVars, String objName, String prefix, boolean aggr) throws SQLException {
		List<Var> res = new ArrayList<Var>();
		
		res.add(new Var(DBI.ID_NONE, Var.TYPE_STR, objName, null));
		res.addAll(getSysVars01(dbi, username, expId, incVars, sysVars, objName, prefix, aggr));
		
		Iterator<Var> it = sysVars.iterator();
		while (it.hasNext()) {
			Var v = it.next();
			if (incVars == null || incVars.size() == 0 || incVars.contains(v.getName())) res.add(v);
		}
		
		return res;
	}
	*/
	
	/*
	private List<Var> getSysVars02(String objName, Map<String,List<Var>> sysVars, Set<String> incVars) {
		List<Var> res = new ArrayList<Var>();
		
		res.add(new Var(DBI.ID_NONE, Var.TYPE_STR, objName, null));
		
		Iterator<Var> it = sysVars.get(objName).iterator();
		while (it.hasNext()) {
			Var v = it.next();
			if (incVars == null || incVars.size() == 0 || incVars.contains(v.getName())) res.add(v);
		}
		
		return res;
	}
	*/
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static void varLstAdd(List<Var> vars, Set<String> incVars, int type, String name, String prefix) {
		if (incVars == null || incVars.size() == 0 || incVars.contains(name)) vars.add(new Var(DBI.ID_NONE, type, prefix + name, null));
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static String varGetVal(Set<String> incVars, String name, String val)  { return (val != null && (incVars == null || incVars.size() == 0 || incVars.contains(name)) ? val               : null ); }
	public static String varGetVal(Set<String> incVars, String name, Float val)   { return (val != null && (incVars == null || incVars.size() == 0 || incVars.contains(name)) ? "" + val          : null ); }
	public static String varGetVal(Set<String> incVars, String name, Double val)  { return (val != null && (incVars == null || incVars.size() == 0 || incVars.contains(name)) ? "" + val          : null ); }
	public static String varGetVal(Set<String> incVars, String name, Integer val) { return (val != null && (incVars == null || incVars.size() == 0 || incVars.contains(name)) ? "" + val          : null ); }
	public static String varGetVal(Set<String> incVars, String name, Boolean val) { return (val != null && (incVars == null || incVars.size() == 0 || incVars.contains(name)) ? (val ? "1" : "0") : null ); }
}
