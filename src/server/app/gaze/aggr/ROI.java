package app.gaze.aggr;

import inter.DBI;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import main.Do;
import app.data.Var;
import app.gaze.dstruct.Blink;
import app.gaze.dstruct.Fix;


/**
 * Calculates EM variables for a single ROI (defined as a ID-name pair, i.e., same words but with different IDs are separate ROIs).
 * 
 * @author Tomek D. Loboda
 */
public class ROI {
	public final static int UNDEF = -1;
	
	private DBI dbi;
	private String username;
	private List<Var> sysVars;
	
	private final int id;
	private final String name;
	private final int num;  // ordinal number
	private final int y;    // y1 coordinate of the ROI 
	private final int len;
	
	private boolean isFP   = true;   // flag: is first-pass reading stil going on?
	private boolean isRP   = true;   // flag: has the eye not progressed passed this ROI? (for regression path)
	private boolean isSeen = false;  // flag: has this ROI been seen (think directly fixated or through the parafoveal preview)
	
	private int nf   = 0;  // number of fixations
	private int nfpf = 0;  // number of first-pass fixations
	private int nspf = 0;  // number of second-pass fixations
	private int nr   = 0;  // number of regressions
	private int nfpr = 0;  // number of first-pass regressions
	private int nspr = 0;  // number of second-pass regressions
	private int nb   = 0;  // number of blinks
	private int nfpb = 0;  // number of first-pass blinks
	private int nspb = 0;  // number of second-pass blinks
	
	private int fpfdMin = UNDEF;
	private int fpfdMax = UNDEF;
	private int spfdMin = UNDEF;
	private int spfdMax = UNDEF;
	
	private float ffd     = UNDEF;
	private float sfd     = UNDEF;
	private float gd      = UNDEF;
	private float tt      = UNDEF;
	private float rp      = UNDEF;
	
	private float bdSum  = UNDEF;  // sum of blink dur 
	private float bsdSum = UNDEF;  // sum of blink saccade dur 
	private float bsaSum = UNDEF;  // sum of blink saccade amplitude
	
	private int nfIn      = 0;  // number of incoming fixations
	private int nfOut     = 0;  // number of outgoing fixations
	private int letInFP   = UNDEF; 
	private int letOutFP  = UNDEF; 
	private int letInSum  = UNDEF; 
	private int letOutSum = UNDEF; 
	
	private int saccInROIFP  = UNDEF;
	private int saccOutROIFP = UNDEF;
	private int saccInLetFP  = UNDEF;
	private int saccOutLetFP = UNDEF;
	
	private int saccInROISum  = 0;
	private int saccOutROISum = 0;
	private int saccInLetSum  = 0;
	private int saccOutLetSum = 0;
	
	private float pupil = UNDEF;
	private int   nfPup = 0;  // number of fixations with pupil value defined
	
	private ROI fpPrevROI = null;  // first-pass previous ROI
	private ROI fpNextROI = null;  // first-pass next ROI
	
	private final List<Fix>   F = new ArrayList<Fix>();    // fixations
	private final List<Blink> B = new ArrayList<Blink>();  // blinks
	
	private final Map<String,String> SV = new HashMap<String,String>();  // system variables
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * If 'st' is passed (i.e., is not null) it is assumed that the correct DB has already been selected.
	 */
	public ROI(DBI dbi, String username, Statement st, int id, String name, int num, List<Var> sysVars)  throws SQLException {
		this.dbi = dbi;
		this.username = username;
		this.sysVars = sysVars;
		
		this.id = id;
		this.name = name;
		this.num = num;
		
		len = name.trim().length();
		
		boolean stNew = (st == null);
		if (stNew) {
			st = dbi.newSt();
			st.execute("SET search_path TO " + "\"" + Do.DB_GAZE + "-" + username + "\"");
		}
		
		ResultSet rsY = st.executeQuery("SELECT y FROM vertex WHERE roi_id = " + id + " ORDER BY id LIMIT 1");
		rsY.next();
		y = rsY.getInt("y");
		rsY.close();
		
		// System variables:
		if (sysVars == null) {
			// TODO: Read the list of variables from the DB
			sysVars = new ArrayList<Var>();
		}
		
		if (sysVars.size() > 0) {
			StringBuilder qrySysVars = new StringBuilder("SELECT ");
			Iterator<Var> it = sysVars.iterator();
			while (it.hasNext()) {
				Var v = it.next();
				qrySysVars.append("(SELECT COALESCE((SELECT val FROM xr_var_roi WHERE roi_id = " + id + " AND var_id = " + v.getId() + "), " + v.getDefVal() + ")) AS \"" + v.getName() + "\"" + (it.hasNext() ? "," : ""));
			}
			
			ResultSet rsSysVars = st.executeQuery(qrySysVars.toString());
			rsSysVars.next();
			it = sysVars.iterator();
			while (it.hasNext()) {
				Var v = it.next();
				String val = rsSysVars.getString(v.getName());
				SV.put(v.getName(), (val == null ? "null" : val));
			}
			rsSysVars.close();
		}
		
		if (stNew) {
			st.close();
		}
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public void digestBlink(Blink b) {
		B.add(b);
		
		nb++;
		if (isFP) nfpb++;
		else nspb++;
		
		bdSum  = (bdSum  == UNDEF ? b.dur     : bdSum  + b.dur);
		bsdSum = (bsdSum == UNDEF ? b.saccDur : bsdSum + b.saccDur);
		bsaSum = (bsaSum == UNDEF ? b.saccAmp : bsaSum + b.saccAmp);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public void digestFix(Fix f, ROI prevROI) {
		F.add(f);
		
		if (nf == 0 && prevROI != this && prevROI != null && prevROI.getNF() == prevROI.getNFPF()) fpPrevROI = prevROI;
		
		// misc:
		nf++;
		tt  = (tt  == UNDEF ? f.dur : tt + f.dur);
		ffd = (ffd == UNDEF ? f.dur : ffd);
		
		if (isFP) {  // first-pass reading
			nfpf++;
			
			sfd = (nfpf == 1 ? f.dur : UNDEF);
			gd = (gd == UNDEF ? f.dur : gd + f.dur);
			
			if (letInFP == UNDEF) letInFP = f.let;
			letOutFP = f.let;  // by updating it on all FD fixations I ensure, that the last FP fixation will be reflected in this variable
			
			if (fpfdMin == UNDEF || f.dur < fpfdMin) fpfdMin = f.dur; 
			if (fpfdMax == UNDEF || f.dur > fpfdMax) fpfdMax = f.dur; 
		}
		else {
			nspf++;
			if (spfdMin == UNDEF || f.dur < spfdMin) spfdMin = f.dur; 
			if (spfdMax == UNDEF || f.dur > spfdMax) spfdMax = f.dur; 
		}
		
		// launch and landing sites (for calculating averages):
		if (f.saccInROI != null && f.saccInROI != 0) {  // an incoming fixation
			nfIn++;
			letInSum = (letInSum == UNDEF ? f.let : letInSum + f.let);
		}
		
		if (f.saccOutROI != null && f.saccOutROI != 0) {  // an outgoing fixation
			nfOut++;
			letOutSum = (letOutSum == UNDEF ? f.let : letOutSum + f.let);
		}
		
		// saccades:
		if (isFP) {
			//if (nfIn == 1) {
			if (nf == 1) {
				if (f.saccInROI != null && f.saccInROI != 0) saccInROIFP = f.saccInROI;
				if (f.saccInLet != null && f.saccInROI != 0) saccInLetFP = f.saccInLet;
			}
			if (f.saccOutROI != null) saccOutROIFP = f.saccOutROI;
			if (f.saccOutLet != null) saccOutLetFP = f.saccOutLet;
		}
		
		if (f.saccInROI != null)  saccInROISum  += f.saccInROI;
		if (f.saccInLet != null)  saccInLetSum  += f.saccInLet;
		if (f.saccOutROI != null) saccOutROISum += f.saccOutROI;
		if (f.saccOutLet != null) saccOutLetSum += f.saccOutLet;
		
		// pupil:
		if (f.pupil != null) {
			nfPup++;
			pupil = (pupil == UNDEF ? f.pupil : pupil + f.pupil);
		}
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public void digestFixRP(float dur) {
		if (isRP) rp = (rp == UNDEF ? dur : rp + dur);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public void digestRegr() {
		nr++;
		if (isFP) nfpr++;
		else nspr++;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public int    getId()   { return id;   }
	public String getName() { return name; }
	public int    getNum()  { return num;  }
	public int    getY()    { return y;    }
	public int    getLen()  { return len;  }
	
	public int getNF()    { return nf;    }
	public int getNFIn()  { return nfIn;  }
	public int getNFOut() { return nfOut; }
	public int getNFPF()  { return nfpf;  }
	public int getNSPF()  { return nspf;  }
	public int getNR()    { return nr;    }
	public int getNFPR()  { return nfpr;  }
	public int getNSPR()  { return nspr;  }
	public int getNB()    { return nb;    }
	public int getNFNB()  { return nfpb;  }
	public int getNSNB()  { return nspb;  }
	
	public int getFPFDMin() { return fpfdMin; }
	public int getFPFDMax() { return fpfdMax; }
	public int getSPFDMin() { return spfdMin; }
	public int getSPFDMax() { return spfdMax; }
	
	public float getAFD()     { return tt / (float) nf; }
	public float getFFD()     { return ffd;             }
	public float getSFD()     { return sfd;             }
	public float getGD()      { return gd;              }
	public float getTT()      { return tt;              }
	public float getRP()      { return rp;              }
	
	public float getABD()  { return (float) bdSum / (float) nb;  }
	public float getABSD() { return (float) bsdSum / (float) nb; }
	public float getABSA() { return (float) bsaSum / (float) nb; }
	
	public int getLetInFP()  { return letInFP;  }
	public int getLetOutFP() { return letOutFP; }
	
	public float getLetInAvg()  { return (float) letInSum  / (float) nfIn;  }
	public float getLetOutAvg() { return (float) letOutSum / (float) nfOut; }
	
	public int getSaccInROIFP()  { return saccInROIFP;  }
	public int getSaccOutROIFP() { return saccOutROIFP; }
	public int getSaccInLetFP()  { return saccInLetFP;  }
	public int getSaccOutLetFP() { return saccOutLetFP; }
	
	public float getSaccInROIAvg()  { return (float) saccInROISum  / (float) nfIn;  }
	public float getSaccOutROIAvg() { return (float) saccOutROISum / (float) nfOut; }
	public float getSaccInLetAvg()  { return (float) saccInLetSum  / (float) nfIn;  }
	public float getSaccOutLetAvg() { return (float) saccOutLetSum / (float) nfOut; }
	
	public int   getNFPup()  { return nfPup; }
	public float getPupil()  { return (nfPup == 0 ? UNDEF : pupil / (float) nfPup); }
	
	public ROI getPrevROI() { return fpPrevROI; }
	public ROI getNextROI() { return fpNextROI; }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public List<Fix>   getFix()    { return F; }
	public List<Blink> getBlinks() { return B; }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public String getSysVar(String name) { return SV.get(name); }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public ROI getLeftROI()   throws SQLException { return getROI(num-1); }
	public ROI getRightROI()  throws SQLException { return getROI(num+1); }
	
	
	// - - - -
	private ROI getROI(int num)  throws SQLException {
		if (num == 0) return null;
		
		ResultSet rs = dbi.execQry("SELECT id, name FROM roi WHERE stim_id = (SELECT stim_id FROM roi WHERE id = " + id + ") AND num = " + num);
		
		if (!rs.next()) {
			rs.close();
			return null;
		}
		
		int id = rs.getInt("id");
		String name = rs.getString("name");
		
		rs.close();
		
		return new ROI(dbi, username, null, id, name, num, sysVars);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public boolean isFix()  { return nf > 0; }
	public boolean isFP()   { return isFP;   }
	public boolean isRP()   { return isRP;   }
	public boolean isSeen() { return isSeen; }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public void setFP(boolean fp)     { isFP = fp;     }
	public void setRP(boolean rp)     { isRP = rp;     }
	public void setSeen(boolean seen) { isSeen = seen; }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public void setFPNextROI(ROI r) { fpNextROI = r; }
}
