package app.gaze.aggr;

import java.util.regex.Matcher;

import util.RegExpr;


public class Evt {
	public final static int TIME_RESP = 0;
	public final static int TIME_PROMPT = 1;
	
	public final String name;
	public final int time;
	
	public final int t01;
	public final int t02;
	public final int t03;
	public final int t04;
	
	public final char f01;
	public final char f02;
	public final char f03;
	public final char f04;
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * C'tor.
	 */
	public Evt(String e) {
		Matcher m = RegExpr.match("'(.+)':(.),(\\d+),(\\d+),(\\d+),(\\d+),(.)(.)(.)(.)", e);
		
		name = m.group(1);
		
		time = (m.group(2).equals("r") ? TIME_RESP : TIME_PROMPT);
		
		t01 = new Integer(m.group(3));
		t02 = new Integer(m.group(4));
		t03 = new Integer(m.group(5));
		t04 = new Integer(m.group(6));
		
		f01 = m.group(7).charAt(0);
		f02 = m.group(8).charAt(0);
		f03 = m.group(9).charAt(0);
		f04 = m.group(10).charAt(0);
	}
	
	
	// ---------------------------------------------------------------------------------
}
