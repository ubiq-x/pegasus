package app.gaze.aggr;

import inter.DBI;

import java.awt.Point;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import main.Do;
import app.data.Var;
import app.gaze.dstruct.Word;


/**
 * @author Tomek D. Loboda
 */
public class StimSeq {
	private final DBI dbi;
	private final String username;
	
	private final boolean adj;
	private final String anUnit;
	private final int id;
	private final int dur;
	private final String stimName;
	private final String trialName;
	private final String subName;
	private final String roiIds;
	private final Long tExp0;
	private final Long tExp1;
	private final Long tTrial0;
	private final Long tTrial1;
	private final Long tRec0;
	private final Long tRec1;
	private final Integer fixDur0;
	private final Integer fixDur1;
	private final Map<String, List<Var>> sysVars;
	private final int maxROIDist;
	private final Point txtP1;
	private final Point txtP2;
	private final Point scrP2;
	
	private final List<Unit> units = new ArrayList<Unit>();  // a unit is a subject or an event
	private final List<Evt> evts = new ArrayList<Evt>();
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public StimSeq(DBI dbi, String username, String anUnit, boolean adj, int id, int dur, int stimId, String stimName, String trialName, String subName, String roiIds, Integer fixDur0, Integer fixDur1, String evts, Map<String,List<Var>> sysVars, int maxROIDist, Long tExp0, Long tExp1, Long tTrial0, Long tTrial1, Long tRec0, Long tRec1)  throws SQLException {
		this.dbi        = dbi;
		this.username   = username;
		this.anUnit     = anUnit;
		this.adj        = adj;
		this.id         = id;
		this.dur        = dur;
		
		this.stimName   = stimName;
		this.trialName  = trialName;
		this.subName    = subName;
		
		this.roiIds     = roiIds;
		this.fixDur0    = fixDur0;
		this.fixDur1    = fixDur1;
		this.sysVars    = sysVars;
		this.maxROIDist = maxROIDist;
		
		this.tExp0      = tExp0;
		this.tExp1      = tExp1;
		this.tTrial0    = tTrial0;
		this.tTrial1    = tTrial1;
		this.tRec0      = tRec0;
		this.tRec1      = tRec1;
		
		// Text bounding box and screen dimensions:
		ResultSet rs = dbi.execQry("SELECT box_x1 AS x1, box_y1 AS y1, box_x2 AS x2, box_y2 AS y2, res_h, res_v FROM \"" + Do.DB_GAZE + "-" + username + "\".stim WHERE id = " + stimId);
		rs.next();
		txtP1 = new Point(rs.getInt("x1"), rs.getInt("y1"));
		txtP2 = new Point(rs.getInt("x2"), rs.getInt("y2"));
		scrP2 = new Point(rs.getInt("res_h"), rs.getInt("res_v"));
		rs.close();
		
		// Events:
		decodeEvts(evts);
		
		// Units:
		initUnits();
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Returns: row count.
	 */
	public int calc(Statement stX, String username, int dsId, Set<String> incVars, String sqlVars)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		// (1) Prepare:
		List<String> lstEmptyVal = Word.getValuesEmpty(incVars, "null", true);
		
		// (2) Run:
		//int evtCnt = 0;
		int rowCnt = 0;
		
		String qry01 = "INSERT INTO \"" + Do.DB_DATA_DS + "-" + username + "\".ds_" + dsId + " (" + sqlVars + ") VALUES (" + DBI.esc(subName) + getObjVars(stX, "sub") + "," + DBI.esc(trialName) + getObjVars(stX, "trial") + "," + DBI.esc(stimName) + getObjVars(stX, "stim") + ",";
		
		for (int i = 0; i < units.size(); i++) {
			//evtCnt++;
			
			List<String> lstQry03 = units.get(i).calc(incVars, lstEmptyVal);
			if (lstQry03 != null && lstQry03.size() > 0) {
				Iterator<String> it = lstQry03.iterator();
				while (it.hasNext()) {
					String qry03 = it.next();
					
					String qry02 = "";
					if (anUnit.equals("roi") || anUnit.equals("roi-prev-next-adj-ord") || anUnit.equals("roi-prev-next-adj-fix") || anUnit.equals("s-seq")) {
						qry02 += (evts.size() > 0 ? (i+1) + "," : "null,");
					}
					else if (anUnit.equals("fix")) {}
					
					//System.out.println(qry01 + qry02 + qry03 + ")");
					stX.executeUpdate(qry01 + qry02 + qry03 + ")");
					rowCnt++;
				}
			}
		}
		
		return rowCnt;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	private String getObjVars(Statement st, String objName)  throws SQLException {
		String db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		StringBuffer res = new StringBuffer();
		
		String tblAbbr = null;
		     if (objName.equals("sub"))   tblAbbr = "su";
		else if (objName.equals("stim"))  tblAbbr = "st";
		else if (objName.equals("trial")) tblAbbr = "t";
		
		for (Var v: sysVars.get(objName)) {
			String val = DBI.getStr(
				dbi,
				"SELECT xv.val AS val " +
				"FROM " + db + ".stim_seq ss " + 
				"INNER JOIN " + db + ".stim st ON ss.stim_id = st.id " +
				"INNER JOIN " + db + ".trial t ON ss.trial_id = t.id " +
				"INNER JOIN " + db + ".sub su ON t.sub_id = su.id " +
				"INNER JOIN " + db + ".xr_var_" + objName + " xv ON xv." + objName + "_id = " + tblAbbr + ".id " +
				"INNER JOIN " + db + ".var v ON xv.var_id = v.id " +
				"WHERE ss.id = " + id + " AND v.name = '" + v.getName() + "'",
				"val"
			);
			res.append("," + (v.getType() == Var.TYPE_STR && val != null ? "'" + val + "'" : val));
		}
		
		return res.toString();
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	private void decodeEvts(String evtsStr) {
		if (evtsStr != null && evtsStr.length() > 0) {
			String [] E = evtsStr.split(";");
			for (String e: E) {
				evts.add(new Evt(e));
			}
		}
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public int getMaxTSLen() {
		int max = 0;
		Iterator<Unit> it = units.iterator();
		while (it.hasNext()) {
			int curr = it.next().getTSLen();
			if (curr > max) max = curr;
		}
		return max;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Returns a query that retrieves fixations matching the specified set of constraints (stimuli and events).
	 */
	private void initUnits()  throws SQLException {
		String db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		String adjStr =  (adj ? "_adj" : "");
		
		units.clear();
		
		String qryTimeF = 
			(tExp0   == null ? "" : "AND (t.t0 + f.t) >= "     + tExp0   .intValue() + " ") +
			(tExp1   == null ? "" : "AND (t.t0 + f.t) <= "     + tExp1   .intValue() + " ") +
			(tTrial0 == null ? "" : "AND f.t >= "              + tTrial0 .intValue() + " ") +
			(tTrial1 == null ? "" : "AND f.t <= "              + tTrial1 .intValue() + " ") +
			(tRec0   == null ? "" : "AND (t.t0_rec + f.t) >= " + tRec0   .intValue() + " ") +
			(tRec1   == null ? "" : "AND (t.t0_rec + f.t) <= " + tRec1   .intValue() + " ") +
			(fixDur0 == null ? "" : "AND f.dur >= "            + fixDur0 .intValue() + " ") +
			(fixDur1 == null ? "" : "AND f.dur <= "            + fixDur1 .intValue() + " ");
		
		String qryTimeB = 
			(tExp0   == null ? "" : "AND (t.t0 + b.t) >= "     + tExp0   .intValue() + " ") +
			(tExp1   == null ? "" : "AND (t.t0 + b.t) <= "     + tExp1   .intValue() + " ") +
			(tTrial0 == null ? "" : "AND b.t >= "              + tTrial0 .intValue() + " ") +
			(tTrial1 == null ? "" : "AND b.t <= "              + tTrial1 .intValue() + " ") +
			(tRec0   == null ? "" : "AND (t.t0_rec + b.t) >= " + tRec0   .intValue() + " ") +
			(tRec1   == null ? "" : "AND (t.t0_rec + b.t) <= " + tRec1   .intValue() + " ");
		
		// Without event constraints:
		if (evts.size() == 0) {
			String qryF = 
				"SELECT f.x" + adjStr + " AS x, f.y" + adjStr + " AS y, f.dur, (t.t0 + f.t) AS t_exp, (t.t0_rec + f.t) AS t_rec, f.t AS t_trial, f.num AS fnum, f.let" + adjStr + " AS let, t.id AS tid, COALESCE(r.id, " + DBI.ID_NONE + ") AS rid, r.name AS rname, r.num AS rnum, f.sacc_i_roi" + adjStr + " AS sacc_i_roi, f.sacc_o_roi" + adjStr + " AS sacc_o_roi, f.sacc_i_let" + adjStr + " AS sacc_i_let, f.sacc_o_let" + adjStr + " AS sacc_o_let, f.pupil " +
				"FROM " + db + ".fix f " +
				"INNER JOIN " + db + ".trial t ON f.trial_id = t.id " +
				"LEFT JOIN " + db + ".roi r ON f.roi_id" + adjStr + " = r.id " +
				"WHERE stim_seq_id = " + id + " AND NOT f.exc " + qryTimeF + 
				(roiIds.length() > 0 ? " AND f.roi_id" + adjStr + " IN (" + roiIds + ") " : "") +
				"ORDER BY f.trial_id, f.num";
			//System.out.println(qryF);
			
			String qryB = 
				"SELECT b.sx AS sx, b.sy AS sy, b.ex AS ex, b.ey AS ey, b.dur, (t.t0 + b.t) AS t_exp, (t.t0_rec + b.t) AS t_rec, b.t AS t_trial, b.sacc_dur, b.sacc_amp, b.roi_01_id AS rid " +
				"FROM " + db + ".blink b " +
				"INNER JOIN " + db + ".trial t ON b.trial_id = t.id " +
				"LEFT JOIN " + db + ".roi r ON b.roi_01_id = r.id " +
				"WHERE b.stim_seq_id = " + id + " AND NOT b.exc " + qryTimeB +
				(roiIds.length() > 0 ? " AND b.roi_01_id IN (" + roiIds + ") " : "") +
				"ORDER BY b.trial_id, b.id";
			//System.out.println(qryB);
			
			units.add(new Unit(dbi, username, anUnit, id, qryF, qryB, roiIds, dur, txtP1, txtP2, scrP2, sysVars.get("roi"), maxROIDist));
		}
		
		// With event constraints:
		else {
			String qryF01 =
				"SELECT f.x" + adjStr + " AS x, f.y" + adjStr + " AS y, f.dur, (t.t0 + f.t) AS t_exp, (t.t0_rec + f.t) AS t_rec, f.t AS t_trial, f.num AS fnum, f.let" + adjStr + " AS let, t.id AS tid, COALESCE(r.id, " + DBI.ID_NONE + ") AS rid, r.name AS rname, r.num AS rnum, f.sacc_i_roi" + adjStr + " AS sacc_i_roi, f.sacc_o_roi" + adjStr + " AS sacc_o_roi, f.sacc_i_let" + adjStr + " AS sacc_i_let, f.sacc_o_let" + adjStr + " AS sacc_o_let, f.pupil " +
				"FROM " + db + ".fix f " +
				"INNER JOIN " + db + ".trial t ON f.trial_id = t.id " +
				"LEFT JOIN " + db + ".roi r ON f.roi_id" + adjStr + " = r.id " +
				"WHERE stim_seq_id = " + id + " AND NOT f.exc " + qryTimeF +
				/*
				(fixDur0 == null ? "" : "AND f.dur >= " + fixDur0.intValue() + " ") +
				(fixDur1 == null ? "" : "AND f.dur <= " + fixDur1.intValue() + " ") +
				*/
				(roiIds.length() > 0 ? " AND f.roi_id" + adjStr + " IN (" + roiIds + ") " : "") +
				"AND (";
			
			String qryB01 =
				"SELECT b.sx AS sx, b.sy AS sy, b.ex AS ex, b.ey AS ey, b.dur, (t.t0 + b.t) AS t_exp, (t.t0_rec + b.t) AS t_rec, b.t AS t_trial, b.sacc_dur, b.sacc_amp, b.roi_01_id AS rid " +
				"FROM " + db + ".blink b " +
				"INNER JOIN " + db + ".trial t ON b.trial_id = t.id " +
				"LEFT JOIN " + db + ".roi r ON b.roi_01_id = r.id " +
				"WHERE b.stim_seq_id = " + id + " AND NOT b.exc " + qryTimeB +
				(roiIds.length() > 0 ? " AND b.roi_01_id IN (" + roiIds + ") " : "") +
				"AND (";
			
			Iterator<Evt> it = evts.iterator();
			while (it.hasNext()) {
				Evt e = (Evt) it.next();
				String name = e.name;
				
				boolean f01 = e.f01 == 'i';
				boolean f02 = e.f02 == 'i';
				boolean f03 = e.f03 == 'i';
				boolean f04 = e.f04 == 'i';
				
				ResultSet rsEvt = dbi.execQry("(SELECT " + (e.time == Evt.TIME_RESP ? "e.t_resp" : "e.t_prompt") + " AS t FROM " + db + ".evt e INNER JOIN " + db + ".evt_lst el ON e.evt_lst_id = el.id WHERE e.stim_seq_id = " + id + " AND NOT e.exc AND el.name = " + DBI.esc(name) + ")");
				
				while (rsEvt.next()) {
					String qryF02 = "", qryB02 = "";
					
					int t = rsEvt.getInt("t");
					int t01 = t - e.t01;
					int t02 = t - e.t02;
					int t03 = t + e.t03;
					int t04 = t + e.t04;
					
					if (e.t01 > 0 && e.t04 > 0 && e.t02 == 0 && e.t03 == 0) {  // to the left and right (no space in-between)
						qryF02 += initUnits_getEMObjInRangeSql("f", t01, t04, f01, f04);
						qryB02 += initUnits_getEMObjInRangeSql("b", t01, t04, f01, f04);
					}
					else if (e.t01 > 0 && e.t04 == 0) {  // to the left only
						qryF02 += initUnits_getEMObjInRangeSql("f", t01, t02, f01, f02);
						qryB02 += initUnits_getEMObjInRangeSql("b", t01, t02, f01, f02);
					}
					else if (e.t01 == 0 && e.t04 > 0) {  // to the right only
						qryF02 += initUnits_getEMObjInRangeSql("f", t03, t04, f03, f04);
						qryB02 += initUnits_getEMObjInRangeSql("b", t03, t04, f03, f04);
					}
					else {  // to the left and right (a space in-between)
						qryF02 += "(" + initUnits_getEMObjInRangeSql("f", t01, t02, f01, f02) + " OR " + initUnits_getEMObjInRangeSql("f", t03, t04, f03, f04) + ")";
						qryB02 += "(" + initUnits_getEMObjInRangeSql("b", t01, t02, f01, f02) + " OR " + initUnits_getEMObjInRangeSql("b", t03, t04, f03, f04) + ")";
					}
					qryF02 += ") ORDER BY f.trial_id, f.num";
					qryB02 += ") ORDER BY b.trial_id, b.id";
					
					units.add(new Unit(dbi, username, anUnit, id, qryF01 + qryF02, qryB01 + qryB02, roiIds, (e.t01-e.t02) + (e.t04-e.t03), txtP1, txtP2, scrP2, sysVars.get("roi"), maxROIDist));
				}
				
				rsEvt.close();
			}
		}
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * obj = {"f","b"}
	 */
	private String initUnits_getEMObjInRangeSql(String obj, int t1, int t2, boolean in, boolean out) {
		String res = "(" + obj + ".t BETWEEN " + t1 + " AND " + t2 + (in ? " OR " + obj + ".t+" + obj + ".dur BETWEEN " + t1 + " AND " + t2 + ")" : ")");
		if (!out) res = "(" + res + " AND (" + obj + ".t+" + obj + ".dur BETWEEN " + t1 + " AND " + t2 + "))";
		return res;
	}
}
