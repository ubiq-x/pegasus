package app.gaze.aggr;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import util.Stats;
import app.gaze.dstruct.Word;


/**
 * @author Tomek D. Loboda
 */
public class WordStats {
	private final Stats nlet            = new Stats();
	
	private final Stats mrc_n           = new Stats();
	private final Stats mrc_nphon       = new Stats();
	private final Stats mrc_nsyl        = new Stats();
	private final Stats mrc_kffrq       = new Stats();
	private final Stats mrc_kffrq_ln    = new Stats();
	private final Stats mrc_kffrq_log10 = new Stats();
	private final Stats mrc_kfcat       = new Stats();
	private final Stats mrc_kfsmp       = new Stats();
	private final Stats mrc_tlfrq       = new Stats();
	private final Stats mrc_tlfrq_ln    = new Stats();
	private final Stats mrc_tlfrq_log10 = new Stats();
	private final Stats mrc_bfrq        = new Stats();
	private final Stats mrc_bfrq_ln     = new Stats();
	private final Stats mrc_bfrq_log10  = new Stats();
	private final Stats mrc_fam         = new Stats();
	private final Stats mrc_cnc         = new Stats();
	private final Stats mrc_img         = new Stats();
	private final Stats mrc_meanc       = new Stats();
	private final Stats mrc_meanp       = new Stats();
	private final Stats mrc_aoa         = new Stats();
	
	private final Stats subtl_frq       = new Stats();
	private final Stats subtl_frq_ln    = new Stats();
	private final Stats subtl_frq_log10 = new Stats();
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public WordStats() {}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public void add(Word w) {
		if (w.getWord() == null || w.getWord().length() == 0) return;
		
		if (w.getNlet()          != null) nlet.           addVal(w.getNlet()         );
		
		if (w.getMRCN()          != null) mrc_n.          addVal(w.getMRCN()         );
		if (w.getMRCNphon()      != null) mrc_nphon.      addVal(w.getMRCNphon()     );
		if (w.getMRCNsyl()       != null) mrc_nsyl.       addVal(w.getMRCNsyl()      );
		if (w.getMRCKffrq()      != null) mrc_kffrq.      addVal(w.getMRCKffrq()     );
		if (w.getMRCKffrqLn()    != null) mrc_kffrq_ln.   addVal(w.getMRCKffrqLn()   );
		if (w.getMRCKffrqLog10() != null) mrc_kffrq_log10.addVal(w.getMRCKffrqLog10());
		if (w.getMRCKfcat()      != null) mrc_kfcat.      addVal(w.getMRCKfcat()     );
		if (w.getMRCKfsmp()      != null) mrc_kfsmp.      addVal(w.getMRCKfsmp()     );
		if (w.getMRCTlfrq()      != null) mrc_tlfrq.      addVal(w.getMRCTlfrq()     );
		if (w.getMRCTlfrqLn()    != null) mrc_tlfrq_ln.   addVal(w.getMRCTlfrqLn()   );
		if (w.getMRCTlfrqLog10() != null) mrc_tlfrq_log10.addVal(w.getMRCTlfrqLog10());
		if (w.getMRCBfrq()       != null) mrc_bfrq.       addVal(w.getMRCBfrq()      );
		if (w.getMRCBfrqLn()     != null) mrc_bfrq_ln.    addVal(w.getMRCBfrqLn()    );
		if (w.getMRCBfrqLog10()  != null) mrc_bfrq_log10. addVal(w.getMRCBfrqLog10() );
		if (w.getMRCFam()        != null) mrc_fam.        addVal(w.getMRCFam()       );
		if (w.getMRCCnc()        != null) mrc_cnc.        addVal(w.getMRCCnc()       );
		if (w.getMRCImg()        != null) mrc_img.        addVal(w.getMRCImg()       );
		if (w.getMRCMeanc()      != null) mrc_meanc.      addVal(w.getMRCMeanc()     );
		if (w.getMRCMeanp()      != null) mrc_meanp.      addVal(w.getMRCMeanp()     );
		if (w.getMRCAoa()        != null) mrc_aoa.        addVal(w.getMRCAoa()       );
		
		if (w.getSubtlFrq()      != null) subtl_frq.      addVal(w.getSubtlFrq()     );
		if (w.getSubtlFrqLn()    != null) subtl_frq_ln.   addVal(w.getSubtlFrqLn()   );
		if (w.getSubtlFrqLog10() != null) subtl_frq_log10.addVal(w.getSubtlFrqLog10());
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public List<String> getValues(Set<String> vars, String defVal) {
		//if (mrc_n.size() == 0) return Word.getValuesEmpty(vars, defVal, false);
		
		nlet.calc();
		
		mrc_n.calc();
		mrc_nphon.calc();
		mrc_nsyl.calc();
		mrc_kffrq.calc();
		mrc_kffrq_ln.calc();
		mrc_kffrq_log10.calc();
		mrc_kfcat.calc();
		mrc_kfsmp.calc();
		mrc_tlfrq.calc();
		mrc_tlfrq_ln.calc();
		mrc_tlfrq_log10.calc();
		mrc_bfrq.calc();
		mrc_bfrq_ln.calc();
		mrc_bfrq_log10.calc();
		mrc_fam.calc();
		mrc_cnc.calc();
		mrc_img.calc();
		mrc_meanc.calc();
		mrc_meanp.calc();
		mrc_aoa.calc();
		
		subtl_frq.calc();
		subtl_frq_ln.calc();
		subtl_frq_log10.calc();
		
		List<String> res = new ArrayList<String>();
		
		/*
		res.add(Aggr.varGetVal(vars, "nlet",            $.ifNull("" + nlet.getMean(2),            "null")));
		
		res.add(Aggr.varGetVal(vars, "mrc_n",           $.ifNull("" + mrc_n.getMean(2),           "null")));
		res.add(Aggr.varGetVal(vars, "mrc_nphon",       $.ifNull("" + mrc_nphon.getMean(2),       "null")));
		res.add(Aggr.varGetVal(vars, "mrc_nsyl",        $.ifNull("" + mrc_nsyl.getMean(2),        "null")));
		res.add(Aggr.varGetVal(vars, "mrc_kffrq",       $.ifNull("" + mrc_kffrq.getMean(2),       "null")));
		res.add(Aggr.varGetVal(vars, "mrc_kffrq_ln",    $.ifNull("" + mrc_kffrq_ln.getMean(2),    "null")));
		res.add(Aggr.varGetVal(vars, "mrc_kffrq_log10", $.ifNull("" + mrc_kffrq_log10.getMean(2), "null")));
		res.add(Aggr.varGetVal(vars, "mrc_kfcat",       $.ifNull("" + mrc_kfcat.getMean(2),       "null")));
		res.add(Aggr.varGetVal(vars, "mrc_kfsmp",       $.ifNull("" + mrc_kfsmp.getMean(2),       "null")));
		res.add(Aggr.varGetVal(vars, "mrc_tlfrq",       $.ifNull("" + mrc_tlfrq.getMean(2),       "null")));
		res.add(Aggr.varGetVal(vars, "mrc_tlfrq_ln",    $.ifNull("" + mrc_tlfrq_ln.getMean(2),    "null")));
		res.add(Aggr.varGetVal(vars, "mrc_tlfrq_log10", $.ifNull("" + mrc_tlfrq_log10.getMean(2), "null")));
		res.add(Aggr.varGetVal(vars, "mrc_bfrq",        $.ifNull("" + mrc_bfrq.getMean(2),        "null")));
		res.add(Aggr.varGetVal(vars, "mrc_bfrq_ln",     $.ifNull("" + mrc_bfrq_ln.getMean(2),     "null")));
		res.add(Aggr.varGetVal(vars, "mrc_bfrq_log10",  $.ifNull("" + mrc_bfrq_log10.getMean(2),  "null")));
		res.add(Aggr.varGetVal(vars, "mrc_fam",         $.ifNull("" + mrc_fam.getMean(2),         "null")));
		res.add(Aggr.varGetVal(vars, "mrc_cnc",         $.ifNull("" + mrc_cnc.getMean(2),         "null")));
		res.add(Aggr.varGetVal(vars, "mrc_img",         $.ifNull("" + mrc_img.getMean(2),         "null")));
		res.add(Aggr.varGetVal(vars, "mrc_meanc",       $.ifNull("" + mrc_meanc.getMean(2),       "null")));
		res.add(Aggr.varGetVal(vars, "mrc_meanp",       $.ifNull("" + mrc_meanp.getMean(2),       "null")));
		res.add(Aggr.varGetVal(vars, "mrc_aoa",         $.ifNull("" + mrc_aoa.getMean(2),         "null")));
		
		res.add(Aggr.varGetVal(vars, "subtl_frq",       $.ifNull("" + subtl_frq.getMean(2),       "null")));
		res.add(Aggr.varGetVal(vars, "subtl_frq_ln",    $.ifNull("" + subtl_frq_ln.getMean(2),    "null")));
		res.add(Aggr.varGetVal(vars, "subtl_frq_log10", $.ifNull("" + subtl_frq_log10.getMean(2), "null")));
		*/
		
		res.add(Aggr.varGetVal(vars, "nlet",            (nlet.size()            == 0 ? "null" : "" + nlet.getMean(2))           ));
		
		res.add(Aggr.varGetVal(vars, "mrc_n",           (mrc_n.size()           == 0 ? "null" : "" + mrc_n.getMean(2))          ));
		res.add(Aggr.varGetVal(vars, "mrc_nphon",       (mrc_nphon.size()       == 0 ? "null" : "" + mrc_nphon.getMean(2))      ));
		res.add(Aggr.varGetVal(vars, "mrc_nsyl",        (mrc_nsyl.size()        == 0 ? "null" : "" + mrc_nsyl.getMean(2))       ));
		res.add(Aggr.varGetVal(vars, "mrc_kffrq",       (mrc_kffrq.size()       == 0 ? "null" : "" + mrc_kffrq.getMean(2))      ));
		res.add(Aggr.varGetVal(vars, "mrc_kffrq_ln",    (mrc_kffrq_ln.size()    == 0 ? "null" : "" + mrc_kffrq_ln.getMean(2))   ));
		res.add(Aggr.varGetVal(vars, "mrc_kffrq_log10", (mrc_kffrq_log10.size() == 0 ? "null" : "" + mrc_kffrq_log10.getMean(2))));
		res.add(Aggr.varGetVal(vars, "mrc_kfcat",       (mrc_kfcat.size()       == 0 ? "null" : "" + mrc_kfcat.getMean(2))      ));
		res.add(Aggr.varGetVal(vars, "mrc_kfsmp",       (mrc_kfsmp.size()       == 0 ? "null" : "" + mrc_kfsmp.getMean(2))      ));
		res.add(Aggr.varGetVal(vars, "mrc_tlfrq",       (mrc_tlfrq.size()       == 0 ? "null" : "" + mrc_tlfrq.getMean(2))      ));
		res.add(Aggr.varGetVal(vars, "mrc_tlfrq_ln",    (mrc_tlfrq_ln.size()    == 0 ? "null" : "" + mrc_tlfrq_ln.getMean(2))   ));
		res.add(Aggr.varGetVal(vars, "mrc_tlfrq_log10", (mrc_tlfrq_log10.size() == 0 ? "null" : "" + mrc_tlfrq_log10.getMean(2))));
		res.add(Aggr.varGetVal(vars, "mrc_bfrq",        (mrc_bfrq.size()        == 0 ? "null" : "" + mrc_bfrq.getMean(2))       ));
		res.add(Aggr.varGetVal(vars, "mrc_bfrq_ln",     (mrc_bfrq_ln.size()     == 0 ? "null" : "" + mrc_bfrq_ln.getMean(2))    ));
		res.add(Aggr.varGetVal(vars, "mrc_bfrq_log10",  (mrc_bfrq_log10.size()  == 0 ? "null" : "" + mrc_bfrq_log10.getMean(2)) ));
		res.add(Aggr.varGetVal(vars, "mrc_fam",         (mrc_fam.size()         == 0 ? "null" : "" + mrc_fam.getMean(2))        ));
		res.add(Aggr.varGetVal(vars, "mrc_cnc",         (mrc_cnc.size()         == 0 ? "null" : "" + mrc_cnc.getMean(2))        ));
		res.add(Aggr.varGetVal(vars, "mrc_img",         (mrc_img.size()         == 0 ? "null" : "" + mrc_img.getMean(2))        ));
		res.add(Aggr.varGetVal(vars, "mrc_meanc",       (mrc_meanc.size()       == 0 ? "null" : "" + mrc_meanc.getMean(2))      ));
		res.add(Aggr.varGetVal(vars, "mrc_meanp",       (mrc_meanp.size()       == 0 ? "null" : "" + mrc_meanp.getMean(2))      ));
		res.add(Aggr.varGetVal(vars, "mrc_aoa",         (mrc_aoa.size()         == 0 ? "null" : "" + mrc_aoa.getMean(2))        ));
		
		res.add(Aggr.varGetVal(vars, "subtl_frq",       (subtl_frq.size()       == 0 ? "null" : "" + subtl_frq.getMean(2))      ));
		res.add(Aggr.varGetVal(vars, "subtl_frq_ln",    (subtl_frq_ln.size()    == 0 ? "null" : "" + subtl_frq_ln.getMean(2))   ));
		res.add(Aggr.varGetVal(vars, "subtl_frq_log10", (subtl_frq_log10.size() == 0 ? "null" : "" + subtl_frq_log10.getMean(2))));
		
		return res;
	}
}
