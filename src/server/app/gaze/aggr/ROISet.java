package app.gaze.aggr;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.Hashtable;

import app.gaze.dstruct.Fix;


/**
 * Stores ROIs with the same names but different IDs.
 * 
 * @author Tomek D. Loboda
 */
public class ROISet {
	private final String name;
	private final Hashtable<Integer,ROI> rois = new Hashtable<Integer,ROI>();
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * C'tor.
	 */
	public ROISet(String name) { this.name = name; }
	
	
	// ---------------------------------------------------------------------------------
	public ROI addROI(Statement st, int id, int num)  throws SQLException {
		ROI r = new ROI(st, id, name, num, null);
		rois.put(new Integer(id), r);
		return r;
	}
	
	
	// ---------------------------------------------------------------------------------
	// public void digestDur(int id, float dur) { rois.get(new Integer(id)).digestDur(dur); }
	public void digestFix(int id, Fix f) { rois.get(new Integer(id)).digestFix(f); }
	public void digestFixRP(int id, float dur) { rois.get(new Integer(id)).digestFixRP(dur); }
	
	
	// ---------------------------------------------------------------------------------
	//public void fixProgressedPast(int id) { rois.get(new Integer(id)).isRP = false; }
	//public void fixMovedOut(int id) { rois.get(new Integer(id)).isFP = false; }
	
	
	// ---------------------------------------------------------------------------------
	public ROI get(int id) { return rois.get(new Integer(id)); }
	
	
	// ---------------------------------------------------------------------------------
	public int getCnt() { return rois.size(); }
	
	
	// ---------------------------------------------------------------------------------
	public Hashtable<Integer,ROI> getROIs() { return rois; }
	
	
	// ---------------------------------------------------------------------------------
	// tt is unadjusted (not divided by the number of ROIs in the name-group)
	// TODO: This implementation looks suspecious...
	/*
	public ROIVars getVars() {
		ROIVars v = new ROIVars();
		
		Iterator<Integer> it = rois.keySet().iterator();
		while (it.hasNext()) {
			ROICalc r = rois.get(it.next());
			
			v.nf   += r.getNF();
			v.nfpf += r.getNFPF();
			v.nr   += r.getNR();
			v.nfpr += r.getNFPR();
			
			v.tt   += r.getTT();
			
			if (r.getNF() > 0) {
				v.afd = (v.afd == 0 ? r.getAFD() : (v.afd + r.getAFD()) / 2);
				v.ffd = (v.ffd == 0 ? r.getFFD() : (v.ffd + r.getFFD()) / 2);
				v.sfd = (v.sfd == 0 ? r.getSFD() : (v.sfd + r.getSFD()) / 2);
				v.gd  = (v.gd  == 0 ? r.getGD()  : (v.gd  + r.getGD())  / 2);
				v.rp  = (v.rp  == 0 ? r.getRP()  : (v.rp  + r.getRP())  / 2);
			}
		}
		
		return v;
	}
	*/
}
