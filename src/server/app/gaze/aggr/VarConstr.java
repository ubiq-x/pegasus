package app.gaze.aggr;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import util.RegExpr;


/**
 * Handles decoding and usage of the variable constraint string passed by the client.
 * 
 * @author Tomek D. Loboda
 */
public class VarConstr {
	
	// ---------------------------------------------------------------------------------
	/**
	 * C'tor.
	 */
	public VarConstr(Connection conn, String db, String obj, String s)  throws SQLException {
		List<String> constr = new ArrayList<String>();
		
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery("SELECT x." + obj + "_id, x.val, v.name FROM " + db + ".xr_var_" + obj + " x INNER JOIN " + db + ".var v ON x.var_id = v.id WHERE v.obj_name = '" + obj + "'");
		
		String[] V = s.split(",");
		for (String v: V) {
			if (v.length() == 0) continue;
			
			rs.beforeFirst();
			while (rs.next()) {
				String name = rs.getString("name");
				Matcher m = RegExpr.match(".*" + name + ".*", v);
				v = m.replaceAll("var_" + obj + "_" + name);
			}
			
			constr.add(v);
			System.out.println(v);
		}
		
		rs.close();
		st.close();
	}
	
	
	// ---------------------------------------------------------------------------------
	public String getIds() {
		return "";
	}
}
