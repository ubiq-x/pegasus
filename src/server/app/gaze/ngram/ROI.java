package app.gaze.ngram;

import java.sql.ResultSet;
import java.sql.SQLException;

import main.Do;
import inter.DBI;


/**
 * @author Tomek D. Loboda
 */
public class ROI {
	private final int id;
	private final int num;
	private final String name;
	private final String pos;
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public ROI(DBI dbi, String username, int id, int num) throws SQLException {
		String db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		
		ResultSet rs = dbi.execQry("SELECT name, COALESCE(pos, '') AS pos FROM " + db + ".roi WHERE id = " + id + "");
		rs.next();
		
		this.id   = id;
		this.num  = num;
		this.name = rs.getString("name");
		this.pos  = rs.getString("pos");
		
		rs.close();
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public int    getId()   { return id;   }
	public int    getNum()  { return num;  }
	public String getName() { return name; }
	public String getPOS()  { return pos;  }
}
