package app.gaze.ngram;

import inter.DBI;
import inter.HTTPI;

import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import main.Do;
import util.$;
import app.gaze.Var;


/**
 * @author Tomek D. Loboda
 */
public class Stim {
	private final static double LOG_PRED_STEP = Math.log(0.1);  // used in discretizing continuous word log-predictability
	
	private final static String MICROSOFT_USER_TOKEN = "2646a191-74d1-4c95-8080-d63736cf2552";
	private final static double MICROSOFT_MIN_CP     = 0.00001;  // minimum conditional probability of a word (indicating word predictability) to consider a valid value (Microsoft's Web service doesn't provide zero probabilities for non-existent ngrams; fucking retards...)
	private final static double MICROSOFT_MIN_LOG_CP = Math.log(MICROSOFT_MIN_CP);
	
	private final static Map<String, String> posPT2G;
		// Penn Treebank to Google ngram part-of-speech tag name translation
		// http://www.comp.leeds.ac.uk/ccalas/tagsets/upenn.html
	
	static {
		// [?] = not sure about this translation (but I did check Google Ngram Viewer to get a good idea; https://books.google.com/ngrams)
		
		posPT2G = new HashMap<String, String>();
		
		// Noun:
		posPT2G.put("NN",   "NOUN");  // noun, common, singular or mass: common-carrier cabbage knuckle-duster Casino afghan shed thermostat investment slide humour falloff slick wind hyena override subhumanity machinist ...
		posPT2G.put("NNP",  "NOUN");  // noun, proper, singular: Motown Venneboerger Czestochwa Ranzer Conchita Trumplane Christos Oceanside Escobar Kreisler Sawyer Cougar Yvette Ervin ODI Darryl CTCA Shannon A.K.C. Meltex Liverpool ...
		posPT2G.put("NNPS", "NOUN");  // noun, proper, plural: Americans Americas Amharas Amityvilles Amusements Anarcho-Syndicalists Andalusians Andes Andruses Angels Animals Anthony Antilles Antiques Apache Apaches Apocrypha ...
		posPT2G.put("NNS",  "NOUN");  // noun, common, plural: undergraduates scotches bric-a-brac products bodyguards facets coasts divestitures storehouses designs clubs fragrances averages subjectivists apprehensions muses factory-jobs ...
		
		// Verb:
		posPT2G.put("MD",   "VERB");  // modal auxiliary: can_VERB cannot_ADV_ADJ_VERB could_VERB couldn't_ADJ_VERB dare_VERB may_VERB might_VERB must_VERB need_VERB_NOUN ought_VERB shall_VERB should_VERB shouldn't_ADJ_VERB will_VERB would_VERB
		posPT2G.put("VB",   "VERB");  // verb, base form: ask assemble assess assign assume atone attention avoid bake balkanize bank begin behold believe bend benefit bevel beware bless boil bomb boost brace break bring broil brush build ...
		posPT2G.put("VBD",  "VERB");  // verb, past tense: dipped pleaded swiped regummed soaked tidied convened halted registered cushioned exacted snubbed strode aimed adopted belied figgered speculated wore appreciated contemplated ...
		posPT2G.put("VBG",  "VERB");  // verb, present participle or gerund: telegraphing stirring focusing angering judging stalling lactating hankerin' alleging veering capping approaching traveling besieging encrypting interrupting erasing wincing ...
		posPT2G.put("VBN",  "VERB");  // verb, past participle: multihulled dilapidated aerosolized chaired languished panelized used experimented flourished imitated reunifed factored condensed sheared unsettled primed dubbed desired ...
		posPT2G.put("VBP",  "VERB");  // verb, present tense, not 3rd person singular: predominate wrap resort sue twist spill cure lengthen brush terminate appear tend stray glisten obtain comprise detest tease attract emphasize mold postpone sever return wag ...
		posPT2G.put("VBZ",  "VERB");  // verb, present tense, 3rd person singular: bases reconstructs marks mixes displeases seals carps weaves snatches slumps stretches authorizes smolders pictures emerges stockpiles seduces fizzes uses bolsters slaps speaks pleads ...
		
		// Adjective:
		posPT2G.put("JJ",   "ADJ" );  // adjective or numeral, ordinal: third ill-mannered pre-war regrettable oiled calamitous first separable ectoplasmic battery-powered participatory fourth still-to-be-named multilingual multi-disciplinary ...
		posPT2G.put("JJR",  "ADJ" );  // adjective, comparative: bleaker braver breezier briefer brighter brisker broader bumper busier calmer cheaper choosier cleaner clearer closer colder commoner costlier cozier creamier crunchier cuter ...
		posPT2G.put("JJS",  "ADJ" );  // adjective, superlative: calmest cheapest choicest classiest cleanest clearest closest commonest corniest costliest crassest creepiest crudest cutest darkest deadliest dearest deepest densest dinkiest ...
		
		// Adverb:
		posPT2G.put("RB",   "ADV" );  // adverb: occasionally unabatingly maddeningly adventurously professedly stirringly prominently technologically magisterially predominately swiftly fiscally pitilessly ...
		posPT2G.put("RBR",  "ADV" );  // adverb, comparative: further gloomier grander graver greater grimmer harder harsher healthier heavier higher however larger later leaner lengthier less-perfectly lesser lonelier longer louder lower more ...
		posPT2G.put("RBS",  "ADV" );  // adverb, superlative: best biggest bluntest earliest farthest first furthest hardest heartiest highest largest least less most nearest second tightest worst
		posPT2G.put("WRB",  "ADV" );  // Wh-adverb: how however whence whenever where whereby whereever wherein whereof why
		
		// Pronoun:
		posPT2G.put("PRP",  "PRON");  // pronoun, personal: hers herself him himself hisself it itself me myself one oneself ours ourselves ownself self she thee theirs them themselves they thou thy us
		posPT2G.put("PRP$", "PRON");  // pronoun, possessive: her his mine my our ours their thy your
		posPT2G.put("WP",   "PRON");  // WH-pronoun: that what whatever whatsoever which who whom whosoever
		posPT2G.put("WP$",  "PRON");  // WH-pronoun, possessive: whose
		
		// Determiner or article:
		posPT2G.put("EX",   "DET" );  // [?] existential there: there_DET_ADV
		posPT2G.put("DT",   "DET" );  // determiner: all an another any both del each either every half la many much nary neither no some such that the them these this those
		posPT2G.put("PDT",  "DET" );  // [?] pre-determiner: all_DET both_DET half_DET_NOUN_ADJ many_ADJ quite_ADV such_ADJ sure_ADJ this_DET
		posPT2G.put("WDT",  "DET" );  // WH-determiner: that what whatever which whichever
		
		// Adposition: either a preposition or a postposition:
		posPT2G.put("IN",   "ADP" );  // [?] preposition or conjunction, subordinating: astride_ADP among_ADP upon_ADP whether_ADP out_PRT_ADP inside_ADP_ADV_NOUN pro_ADJ_NOUN despite_ADP on_ADP by_ADP throughout_ADP below_ADP_ADV within_ADP for_ADP towards_ADP near_ADP_ADJ behind_ADP atop_ADP around_ADP if_ADP like_ADP until_ADP below_ADP_ADV next_ADJ into_ADP if_ADP beside_ADP ...
		
		// Numeral:
		posPT2G.put("CD",   "NUM" );  // numeral, cardinal: mid-1890 nine-thirty forty-two one-tenth ten million 0.5 one forty-seven 1987 twenty '79 zero two 78-degrees eighty-four IX '60s .025 fifteen 271,124 dozen quintillion DM2,000 ...
		
		// Conjunction:
		posPT2G.put("CC",  "CONJ");  // conjunction, coordinating: & 'n and both but either et for less minus neither nor or plus so therefore times v. versus vs. whether yet
		
		// Particle:
		posPT2G.put("TO",   "PRT" );  // [?] "to" as preposition or infinitive marker: to_PRT
		posPT2G.put("RP",   "PRT" );  // particle: aboard about across along apart around aside at away back before behind by crop down ever fast for forth from go high i.e. in into just later low more off on open out over per pie raising start teeth that through under unto up up-pp upon whole with you
		
		// Unused:
		/*
		posPT2G.put("POS", "");  // genitive marker: ' 's_PRT
		posPT2G.put("UH",  "");  // interjection: Goodbye Goody Gosh Wow Jeepers Jee-sus Hubba Hey Kee-reist Oops amen huh howdy uh dammit whammo shucks heck anyways whodunnit honey golly man baby diddle hush sonuvabitch ...
		*/
	}
	
	private final int prjId;
	private final int expId;
	private final int stimId;
	
	private final String prjName;
	private final String expName;
	private final String stimName;
	private final String lang;
	
	private final List<ROI> rois = new ArrayList<ROI>();
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Stim(DBI dbi, String username, int stimId)  throws SQLException {
		String db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		
		this.stimId = stimId;
		
		ResultSet rsStim = dbi.execQry("SELECT exp_id, lang, name FROM " + db + ".stim WHERE id = " + stimId + "");
		rsStim.next();
		
		expId    = rsStim.getInt("exp_id");
		stimName = rsStim.getString("name");
		lang     = rsStim.getString("lang");
		
		rsStim.close();
		
		prjId = (int) DBI.getLong(dbi, "SELECT prj_id FROM " + db + ".exp WHERE id = " + expId, "prj_id");
		
		expName = DBI.getStr(dbi, "SELECT name FROM " + db + ".exp WHERE id = " + expId, "name");
		prjName = DBI.getStr(dbi, "SELECT name FROM " + Do.DB_MAIN + ".prj WHERE id = " + prjId, "name");
		
		//ResultSet rsROI = dbi.execQry("SELECT id, name FROM " + db + ".roi WHERE stim_id = " + stimId + " ORDER BY id LIMIT 20");
		ResultSet rsROI = dbi.execQry("SELECT id, num, name FROM " + db + ".roi WHERE stim_id = " + stimId + " ORDER BY id");
		while (rsROI.next()) {
			rois.add(new ROI(dbi, username, rsROI.getInt("id"), rsROI.getInt("num")));
		}
		rsROI.close();
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Generates a list of ngrams that can be digested by the Ngram app.
	 * 
	 * The following rules apply to contractions:
	 * 
	 *     X's     --> X 's      [remove]
	 *     X'      --> X '       [remove]
	 *     they're --> they 're  [remove]
	 *     we'll   --> we 'll    [remove]
	 *     don't   --> do not    [remove]
	 *     won't   --> will not  [remove]
	 *     can't   --> cannot    [substitute]
	 * 
	 * I choose to remove all ngrams with at least one apostrophe other than those ending with "'s " and "' " because 
	 * it should be fine to substitute them with the main form (don't know how it's called grammatically).  Also, I 
	 * choose to substitute "can't" with "cannot" because that matches the way in which Google books were parsed.
	 * 
	 * With respect to abbreviations, I select a handful of the most common ones as defined here:
	 * 
	 *     http://public.oed.com/how-to-use-the-oed/abbreviations
	 * 
	 * and run them against the Google's Ngram Viewer:
	 * 
	 *     https://books.google.com/ngrams
	 * 
	 * to determine which ones were retained by their parsers.  I consequently make sure these abbreviations are also 
	 * retained in the output of this method.
	 */
	public void getNgramLst(NgramMan ngramMan, int n) {
		if (rois.size() < n) return;
		
		List<ROI> roisNgram = new ArrayList<ROI>(rois.subList(0, n));  // the current ngram (i.e., the moving window that encompasses the current n words)
		
		for (int i = n; i < rois.size(); i++) {
			String ngram [] = new String[n];
			String pos   [] = new String[n];
			
			for (int j = 0; j < n; j++) {
				ngram [j] = roisNgram.get(j).getName().trim();
				pos   [j] = roisNgram.get(j).getPOS() .trim();
			}
			
			String s = ($.join(ngram, "", "", " ", false, false).toString()).trim().
				replaceAll( "  "         , " "          ).
				replaceAll( "\t"         , " "          ).
				replaceAll( "\""         , ""           ).
				replaceAll( "' "         , " "          ).
				replaceAll( "'s "        , " "          ).
				replaceAll( "_"          , "-"          ).
				replaceAll( " can't "    , " cannot "   ).
				replaceAll( " can't$"    , " cannot"    ).
				replaceAll( "^can't "    , "cannot "    ).
				replaceAll( " Mr. "      , " Mr_ "      ).
				replaceAll( " Mr.$"      , " Mr_"       ).
				replaceAll( "^Mr. "      , "Mr_ "       ).
				replaceAll( " Mrs. "     , " Mrs_ "     ).
				replaceAll( " Mrs.$"     , " Mrs_"      ).
				replaceAll( "^Mrs. "     , "Mrs_ "      ).
				replaceAll( " Dr. "      , " Dr_ "      ).
				replaceAll( " Dr.$"      , " Dr_"       ).
				replaceAll( "^Dr. "      , "Dr_ "       ).
				replaceAll( " Prof. "    , " Prof_ "    ).
				replaceAll( " Prof.$"    , " Prof_"     ).
				replaceAll( "^Prof. "    , "Prof_ "     ).
				replaceAll( " Rev. "     , " Rev_ "     ).
				replaceAll( " Rev.$"     , " Rev_"      ).
				replaceAll( "^Rev. "     , "Rev_ "      );
				/*
				replaceAll( " A.D. "     , " A_D_ "     ).
				replaceAll( " A.D.$"     , " A_D_"      ).
				replaceAll( "^A.D. "     , "A_D_ "      ).
				replaceAll( " B.C. "     , " B_C_ "     ).
				replaceAll( " B.C.$"     , " B_C_"      ).
				replaceAll( "^B.C. "     , "B_C_ "      ).
				replaceAll( " e.g. "     , " e_g_ "     ).
				replaceAll( " e.g.$"     , " e_g_"      ).
				replaceAll( "^e.g. "     , "e_g_ "      ).
				replaceAll( " etc. "     , " etc_ "     ).
				replaceAll( " etc.$"     , " etc_"      ).
				replaceAll( "^etc. "     , "etc_ "      ).
				replaceAll( " i.e. "     , " i_e_ "     ).
				replaceAll( " i.e.$"     , " i_e_"      ).
				replaceAll( "^i.e. "     , "i_e_ "      ).
				replaceAll( " N.Z. "     , " N_Z_ "     ).
				replaceAll( " N.Z.$"     , " N_Z_"      ).
				replaceAll( "^N.Z. "     , "N_Z_ "      ).
				replaceAll( " U.K. "     , " U_K_ "     ).
				replaceAll( " U.K.$"     , " U_K_"      ).
				replaceAll( "^U.K. "     , "U_K_ "      ).
				replaceAll( " U.S.A. "   , " U_S_A_ "   ).
				replaceAll( " U.S.A.$"   , " U_S_A_"    ).
				replaceAll( "^U.S.A. "   , "U_S_A_ "    ).
				replaceAll( " U.S.S.R. " , " U_S_S_R_ " ).
				replaceAll( " U.S.S.R.$" , " U_S_S_R_"  ).
				replaceAll( "^U.S.S.R. " , "U_S_S_R_ "  );
				*/
			
			if (s.startsWith("(")) s = s.substring(1);
			if (s.endsWith(".") || s.endsWith("!") || s.endsWith("?") || s.endsWith(",") || s.endsWith(";") || s.endsWith(":") || s.endsWith(")")) s = s.substring(0, s.length()-1);
			
			if 	(
					!s.contains( "  " ) &&
					!s.contains( "."  ) &&
					!s.contains( "?"  ) &&
					!s.contains( "!"  ) &&
					!s.contains( "'"  ) &&
					!s.contains( "-"  ) &&
					!s.contains( "="  ) &&
					!s.contains( "+"  ) &&
					!s.contains( "*"  ) &&
					!s.contains( "@"  ) &&
					!s.contains( "#"  ) &&
					!s.contains( "$"  ) &&
					!s.contains( "%"  ) &&
					!s.contains( "^"  ) &&
					!s.contains( "&"  ) &&
					!s.contains( "\\" ) &&
					!s.contains( "("  ) &&
					!s.contains( ")"  ) &&
					!s.contains( "["  ) &&
					!s.contains( "]"  ) &&
					!s.contains( "{"  ) &&
					!s.contains( "}"  ) &&
					!s.contains( "<"  ) &&
					!s.contains( ">"  ) &&
					!s.matches(".*\\d+.*")
				) {
				s = s.replace("_", ".");  // restore dots which we know are correct (i.e., part of abbreviations)
				
				String inf = prjId + "\t" + prjName.replace("\t", " ") + "\t" + expId + "\t" + expName.replace("\t", " ") + "\t" + stimId + "\t" + stimName.replace("\t", " ") + "\t" + roisNgram.get(roisNgram.size() - 1).getId() + "\t" + roisNgram.get(roisNgram.size() - 1).getNum() + "\t";
				
				// Punctuation excluded (pe; i.e., ngrams containing it are removed):
				if (!s.contains(",") && !s.contains(";") && !s.contains(":")) {
					// Plain:
					ngramMan.add(lang, n, s, inf + "pe\t-");
					ngramMan.add(lang, n, s, inf + "pi\t-");
					ngramMan.add(lang, n, s, inf + "pp\t-");
					
					// POS tagged:
					String aPOS  [] = s.split(" ");
					String aPOSX [] = s.split(" ");
					for (int j = 0; j < n - 1; j++) {
						aPOS  [j] = getPOS(aPOS  [j], pos[j], false);
						aPOSX [j] = getPOS(aPOSX [j], pos[j], true );
					}
					String sPOS  = $.join(aPOS,  "", "", " ", false, false).toString();
					String sPOSX = $.join(aPOSX, "", "", " ", false, false).toString();
					
					ngramMan.add(lang, n, sPOS,  inf + "pe\tpos");
					ngramMan.add(lang, n, sPOS,  inf + "pi\tpos");
					ngramMan.add(lang, n, sPOS,  inf + "pp\tpos");
					
					ngramMan.add(lang, n, sPOSX, inf + "pe\tposx");
					ngramMan.add(lang, n, sPOSX, inf + "pi\tposx");
					ngramMan.add(lang, n, sPOSX, inf + "pp\tposx");
				}
				else if (n >= 4) {
					// Punctuation ignored (pi; i.e., punctuation marks are removed):
					String sPI = s.replace(",", "").replace(";", "").replace(":", "");
					
					// Plain:
					ngramMan.add(lang, n, sPI, inf + "pi\t-");
					
					// POS tagged:
					String aPOSPI  [] = sPI.split(" ");
					String aPOSXPI [] = sPI.split(" ");
					for (int j = 0; j < n - 1; j++) {
						aPOSPI  [j] = getPOS(aPOSPI  [j], pos[j], false);
						aPOSXPI [j] = getPOS(aPOSXPI [j], pos[j], true );
					}
					ngramMan.add(lang, n, $.join(aPOSPI,  "", "", " ", false, false).toString(), inf + "pi\tpos");
					ngramMan.add(lang, n, $.join(aPOSXPI, "", "", " ", false, false).toString(), inf + "pi\tposx");
					
					// Punctuation preserved (pp; i.e., punctuation marks are treated as ngram terms):
					String sPP = s.replace(",", " ,").replace(";", " ;").replace(":", " :");
					int nPP = sPP.split(" ").length;
					if (nPP >= 4 && nPP <= 5) {
						// Plain:
						ngramMan.add(lang, nPP, sPP, inf + "pp\t-");
						
						// POS tagged:
						int k = 0;
						String aPOSPP  [] = sPP.split(" ");
						String aPOSXPP [] = sPP.split(" ");
						for (int j = 0; j < nPP - 1; j++) {
							if (!aPOSPP[j].equals(",") && !aPOSPP[j].equals(";") && !aPOSPP[j].equals(":")) {
								aPOSPP  [j] = getPOS(aPOSPP  [j], pos[k], false);
								aPOSXPP [j] = getPOS(aPOSXPP [j], pos[k], true );
								k++;
							}
						}
						ngramMan.add(lang, nPP, $.join(aPOSPP,  "", "", " ", false, false).toString(), inf + "pp\tpos");
						ngramMan.add(lang, nPP, $.join(aPOSXPP, "", "", " ", false, false).toString(), inf + "pp\tposx");
					}
				}
			}
			
			roisNgram.remove(0);
			roisNgram.add(rois.get(i));
		}
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	private String getPOS(String term, String pos, boolean isExtreme) {
		if (!posPT2G.containsKey(pos)) return term;
		
		String posG = posPT2G.get(pos);  // POS for Google ngram data set
		
		if (!isExtreme) return term + "_" + posG;
		else {
			return (posG.equals("ADP") || posG.equals("CONJ") || posG.equals("NUM") || posG.equals("DET") || posG.equals("PRON") || posG.equals("PRT") ? "_" + posG + "_" : term + "_" + posG);
		}
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	private double getWordPredGoogle(String lang, int n, int decNumCnt, int year01, int year02, String ngram)  throws MalformedURLException, IOException {
		String s = HTTPI.makeReq(Do.URL_NGRAM_GOOGLE + "/get-pred?lang=" + lang + "&n=" + n + "&dec-num-cnt=" + (decNumCnt <= 0 ? 20 : decNumCnt) + "&year-01=" + year01 + "&year-02=" + year02 + "&ngram=" + ngram).toString();
		if (s.indexOf(' ') > 0) s = s.substring(0, s.indexOf(' '));
		return new Double(s);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	private double getWordPredMicrosoft(String lang, int n, String ngram)  throws MalformedURLException, IOException {
		return 0.01;
		//return new Double(HTTPI.makeReq(Do.URL_NGRAM_MICROSOFT + "/" + n + "/cp?u=" + MICROSOFT_USER_TOKEN + "&p=" + ngram).toString());
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	* Note that this method adds all the variables.  This is done on purpose to ensure that Google word predictability 
	* is added first and Microsoft is added later rather than having the two intermixed when variables are sorted by 
	* their ID.  This does not serve any other than aesthetic purpose, but elegance of software is very important to 
	* me.
	*/
	public StringBuilder retrieveWordPred(String username, boolean doGoogle, boolean doMicrosoft)  throws MalformedURLException, IOException, SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		StringBuilder res = new StringBuilder();
		
		String db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		
		DBI dbiX = new DBI();
		Statement stX = dbiX.transBegin();
		stX.execute("SET search_path TO " + db);
		
		// (1) Add variables (if they don't exist already):
		// (1.1) Google, case preserved (cp), punctuation excluded (pe):
		int varIdG3_cp_pe      = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g3-cp-pe",      "Word predictability (Google 3-grams; case preserved, punctuation excluded)",                      false);
		int varIdG4_cp_pe      = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g4-cp-pe",      "Word predictability (Google 4-grams; case preserved, punctuation excluded)",                      false);
		int varIdG5_cp_pe      = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g5-cp-pe",      "Word predictability (Google 5-grams; case preserved, punctuation excluded)",                      false);
		
		// (1.2) Google, case preserved (cp), punctuation ignored (pi):
		int varIdG3_cp_pi      = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g3-cp-pi",      "Word predictability (Google 3-grams; case preserved, punctuation ignored)",                       false);
		int varIdG4_cp_pi      = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g4-cp-pi",      "Word predictability (Google 4-grams; case preserved, punctuation ignored)",                       false);
		int varIdG5_cp_pi      = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g5-cp-pi",      "Word predictability (Google 5-grams; case preserved, punctuation ignored)",                       false);
		
		// (1.3) Google, case preserved (cp), punctuation preserved (pp):
		int varIdG3_cp_pp      = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g3-cp-pp",      "Word predictability (Google 3-grams; case preserved, punctuation preserved)",                     false);
		int varIdG4_cp_pp      = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g4-cp-pp",      "Word predictability (Google 4-grams; case preserved, punctuation preserved)",                     false);
		int varIdG5_cp_pp      = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g5-cp-pp",      "Word predictability (Google 5-grams; case preserved, punctuation preserved)",                     false);
		
		// (1.4) Google, case ignored (ci), punctuation excluded (pe):
		int varIdG3_ci_pe      = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g3-ci-pe",      "Word predictability (Google 3-grams; case ignored, punctuation excluded)",                        false);
		int varIdG4_ci_pe      = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g4-ci-pe",      "Word predictability (Google 4-grams; case ignored, punctuation excluded)",                        false);
		int varIdG5_ci_pe      = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g5-ci-pe",      "Word predictability (Google 5-grams; case ignored, punctuation excluded)",                        false);
		
		// (1.5) Google, case ignored (ci), punctuation ignored (pi):
		int varIdG3_ci_pi      = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g3-ci-pi",      "Word predictability (Google 3-grams; case ignored, punctuation ignored)",                         false);
		int varIdG4_ci_pi      = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g4-ci-pi",      "Word predictability (Google 4-grams; case ignored, punctuation ignored)",                         false);
		int varIdG5_ci_pi      = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g5-ci-pi",      "Word predictability (Google 5-grams; case ignored, punctuation ignored)",                         false);
		
		// (1.6) Google, case ignored (ci), punctuation preserved (pp):
		int varIdG3_ci_pp      = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g3-ci-pp",      "Word predictability (Google 3-grams; case ignored, punctuation preserved)",                       false);
		int varIdG4_ci_pp      = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g4-ci-pp",      "Word predictability (Google 4-grams; case ignored, punctuation preserved)",                       false);
		int varIdG5_ci_pp      = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g5-ci-pp",      "Word predictability (Google 5-grams; case ignored, punctuation preserved)",                       false);
		
		// (1.7) Google, POS tagged, case preserved (cp), punctuation excluded (pe):
		int varIdG3_pos_cp_pe  = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g3-pos-cp-pe",  "Word predictability (Google 3-grams; POS tagged, case preserved, punctuation excluded)",          false);
		int varIdG4_pos_cp_pe  = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g4-pos-cp-pe",  "Word predictability (Google 4-grams; POS tagged, case preserved, punctuation excluded)",          false);
		int varIdG5_pos_cp_pe  = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g5-pos-cp-pe",  "Word predictability (Google 5-grams; POS tagged, case preserved, punctuation excluded)",          false);
		
		// (1.8) Google, POS tagged, case preserved (cp), punctuation ignored (pi):
		int varIdG3_pos_cp_pi  = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g3-pos-cp-pi",  "Word predictability (Google 3-grams; POS tagged, case preserved, punctuation ignored)",           false);
		int varIdG4_pos_cp_pi  = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g4-pos-cp-pi",  "Word predictability (Google 4-grams; POS tagged, case preserved, punctuation ignored)",           false);
		int varIdG5_pos_cp_pi  = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g5-pos-cp-pi",  "Word predictability (Google 5-grams; POS tagged, case preserved, punctuation ignored)",           false);
		
		// (1.9) Google, POS tagged, case preserved (cp), punctuation preserved (pp):
		int varIdG3_pos_cp_pp  = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g3-pos-cp-pp",  "Word predictability (Google 3-grams; POS tagged, case preserved, punctuation preserved)",         false);
		int varIdG4_pos_cp_pp  = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g4-pos-cp-pp",  "Word predictability (Google 4-grams; POS tagged, case preserved, punctuation preserved)",         false);
		int varIdG5_pos_cp_pp  = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g5-pos-cp-pp",  "Word predictability (Google 5-grams; POS tagged, case preserved, punctuation preserved)",         false);
		
		// (1.10) Google, POS tagged, case ignored (ci), punctuation excluded (pe):
		int varIdG3_pos_ci_pe  = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g3-pos-ci-pe",  "Word predictability (Google 3-grams; POS tagged, case ignored, punctuation excluded)",            false);
		int varIdG4_pos_ci_pe  = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g4-pos-ci-pe",  "Word predictability (Google 4-grams; POS tagged, case ignored, punctuation excluded)",            false);
		int varIdG5_pos_ci_pe  = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g5-pos-ci-pe",  "Word predictability (Google 5-grams; POS tagged, case ignored, punctuation excluded)",            false);
		
		// (1.11) Google, POS tagged, case ignored (ci), punctuation ignored (pi):
		int varIdG3_pos_ci_pi  = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g3-pos-ci-pi",  "Word predictability (Google 3-grams; POS tagged, case ignored, punctuation ignored)",             false);
		int varIdG4_pos_ci_pi  = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g4-pos-ci-pi",  "Word predictability (Google 4-grams; POS tagged, case ignored, punctuation ignored)",             false);
		int varIdG5_pos_ci_pi  = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g5-pos-ci-pi",  "Word predictability (Google 5-grams; POS tagged, case ignored, punctuation ignored)",             false);
		
		// (1.12) Google, POS tagged, case ignored (ci), punctuation preserved (pp):
		int varIdG3_pos_ci_pp  = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g3-pos-ci-pp",  "Word predictability (Google 3-grams; POS tagged, case ignored, punctuation preserved)",           false);
		int varIdG4_pos_ci_pp  = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g4-pos-ci-pp",  "Word predictability (Google 4-grams; POS tagged, case ignored, punctuation preserved)",           false);
		int varIdG5_pos_ci_pp  = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g5-pos-ci-pp",  "Word predictability (Google 5-grams; POS tagged, case ignored, punctuation preserved)",           false);
		
		// (1.13) Google, POS tagged extreme, case preserved (cp), punctuation excluded (pe):
		int varIdG3_posx_cp_pe = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g3-posx-cp-pe", "Word predictability (Google 3-grams; POS tagged extreme, case preserved, punctuation excluded)",  false);
		int varIdG4_posx_cp_pe = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g4-posx-cp-pe", "Word predictability (Google 4-grams; POS tagged extreme, case preserved, punctuation excluded)",  false);
		int varIdG5_posx_cp_pe = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g5-posx-cp-pe", "Word predictability (Google 5-grams; POS tagged extreme, case preserved, punctuation excluded)",  false);
		
		// (1.14) Google, POS tagged extreme, case preserved (cp), punctuation ignored (pi):
		int varIdG3_posx_cp_pi = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g3-posx-cp-pi", "Word predictability (Google 3-grams; POS tagged extreme, case preserved, punctuation ignored)",   false);
		int varIdG4_posx_cp_pi = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g4-posx-cp-pi", "Word predictability (Google 4-grams; POS tagged extreme, case preserved, punctuation ignored)",   false);
		int varIdG5_posx_cp_pi = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g5-posx-cp-pi", "Word predictability (Google 5-grams; POS tagged extreme, case preserved, punctuation ignored)",   false);
		
		// (1.15) Google, POS tagged extreme, case preserved (cp), punctuation preserved (pp):
		int varIdG3_posx_cp_pp = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g3-posx-cp-pp", "Word predictability (Google 3-grams; POS tagged extreme, case preserved, punctuation preserved)", false);
		int varIdG4_posx_cp_pp = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g4-posx-cp-pp", "Word predictability (Google 4-grams; POS tagged extreme, case preserved, punctuation preserved)", false);
		int varIdG5_posx_cp_pp = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g5-posx-cp-pp", "Word predictability (Google 5-grams; POS tagged extreme, case preserved, punctuation preserved)", false);
		
		// (1.16) Google, POS tagged extreme, case ignored (ci), punctuation excluded (pe):
		int varIdG3_posx_ci_pe = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g3-posx-ci-pe", "Word predictability (Google 3-grams; POS tagged extreme, case ignored, punctuation excluded)",    false);
		int varIdG4_posx_ci_pe = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g4-posx-ci-pe", "Word predictability (Google 4-grams; POS tagged extreme, case ignored, punctuation excluded)",    false);
		int varIdG5_posx_ci_pe = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g5-posx-ci-pe", "Word predictability (Google 5-grams; POS tagged extreme, case ignored, punctuation excluded)",    false);
		
		// (1.17) Google, POS tagged extreme, case ignored (ci), punctuation ignored (pi):
		int varIdG3_posx_ci_pi = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g3-posx-ci-pi", "Word predictability (Google 3-grams; POS tagged extreme, case ignored, punctuation ignored)",     false);
		int varIdG4_posx_ci_pi = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g4-posx-ci-pi", "Word predictability (Google 4-grams; POS tagged extreme, case ignored, punctuation ignored)",     false);
		int varIdG5_posx_ci_pi = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g5-posx-ci-pi", "Word predictability (Google 5-grams; POS tagged extreme, case ignored, punctuation ignored)",     false);
		
		// (1.18) Google, POS tagged extreme, case ignored (ci), punctuation preserved (pp):
		int varIdG3_posx_ci_pp = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g3-posx-ci-pp", "Word predictability (Google 3-grams; POS tagged extreme, case ignored, punctuation preserved)",   false);
		int varIdG4_posx_ci_pp = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g4-posx-ci-pp", "Word predictability (Google 4-grams; POS tagged extreme, case ignored, punctuation preserved)",   false);
		int varIdG5_posx_ci_pp = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-g5-posx-ci-pp", "Word predictability (Google 5-grams; POS tagged extreme, case ignored, punctuation preserved)",   false);
		
		/*
		 * https://books.google.com/ngrams/info:
		 *     book_INF --> book books booked booking
		 *     *_DET --> a an the this that each every any no some another (determiner or article)
		 *     cook_VERB_INF --> cook_VERB cooks_VERB cooked_VERB cooking_VERB
		 */

		// (1.19) Microsoft, punctuation excluded (pe):
		int varIdM3_pe         = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-m3-pe-log",     "Word log-predictability (Microsoft 3-grams; punctuation excluded)",                               false);
		int varIdM4_pe         = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-m4-pe-log",     "Word log-predictability (Microsoft 4-grams; punctuation excluded)",                               false);
		int varIdM5_pe         = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-m5-pe-log",     "Word log-predictability (Microsoft 5-grams; punctuation excluded)",                               false);
		
		// (1.20) Microsoftpunctuation ignored (pi):
		int varIdM3_pi         = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-m3-pi-log",     "Word log-predictability (Microsoft 3-grams; punctuation ignored)",                                false);
		int varIdM4_pi         = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-m4-pi-log",     "Word log-predictability (Microsoft 4-grams; punctuation ignored)",                                false);
		int varIdM5_pi         = Var.addSys(dbiX, stX, username, expId, "roi", "n", "pred-m5-pi-log",     "Word log-predictability (Microsoft 5-grams; punctuation ignored)",                                false);
		
		// (2) Reset the variables:
		// (2.1) Google, case preserved (cp), punctuation excluded (pe):
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG3_cp_pe);
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG4_cp_pe);
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG5_cp_pe);
		
		// (2.2) Google, case preserved (cp), punctuation ignored (pi):
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG3_cp_pi);
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG4_cp_pi);
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG5_cp_pi);
		
		// (2.3) Google, case preserved (cp), punctuation preserved (pp):
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG3_cp_pp);
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG4_cp_pp);
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG5_cp_pp);
		
		// (2.4) Google, case ignored (ci), punctuation excluded (pe):
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG3_ci_pe);
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG4_ci_pe);
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG5_ci_pe);
		
		// (2.5) Google, case ignored (ci), punctuation ignored (pi):
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG3_ci_pi);
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG4_ci_pi);
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG5_ci_pi);
		
		// (2.6) Google, case ignored (ci), punctuation preserved (pp):
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG3_ci_pp);
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG4_ci_pp);
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG5_ci_pp);
		
		// (2.7) Google, POS tagged, case preserved (cp), punctuation excluded (pe):
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG3_pos_cp_pe);
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG4_pos_cp_pe);
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG5_pos_cp_pe);
		
		// (2.8) Google, POS tagged, case preserved (cp), punctuation ignored (pi):
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG3_pos_cp_pi);
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG4_pos_cp_pi);
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG5_pos_cp_pi);
		
		// (2.9) Google, POS tagged, case preserved (cp), punctuation preserved (pp):
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG3_pos_cp_pp);
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG4_pos_cp_pp);
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG5_pos_cp_pp);
		
		// (2.10) Google, POS tagged, case ignored (ci), punctuation excluded (pe):
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG3_pos_ci_pe);
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG4_pos_ci_pe);
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG5_pos_ci_pe);
		
		// (2.11) Google, POS tagged, case ignored (ci), punctuation ignored (pi):
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG3_pos_ci_pi);
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG4_pos_ci_pi);
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG5_pos_ci_pi);
		
		// (2.12) Google, POS tagged, case ignored (ci), punctuation preserved (pp):
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG3_pos_ci_pp);
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG4_pos_ci_pp);
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG5_pos_ci_pp);
		
		// (2.13) Google, POS tagged, case preserved (cp), punctuation excluded (pe):
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG3_posx_cp_pe);
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG4_posx_cp_pe);
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG5_posx_cp_pe);
		
		// (2.14) Google, POS tagged, case preserved (cp), punctuation ignored (pi):
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG3_posx_cp_pi);
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG4_posx_cp_pi);
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG5_posx_cp_pi);
		
		// (2.15) Google, POS tagged, case preserved (cp), punctuation preserved (pp):
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG3_posx_cp_pp);
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG4_posx_cp_pp);
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG5_posx_cp_pp);
		
		// (2.16) Google, POS tagged, case ignored (ci), punctuation excluded (pe):
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG3_posx_ci_pe);
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG4_posx_ci_pe);
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG5_posx_ci_pe);
		
		// (2.17) Google, POS tagged, case ignored (ci), punctuation ignored (pi):
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG3_posx_ci_pi);
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG4_posx_ci_pi);
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG5_posx_ci_pi);
		
		// (2.18) Google, POS tagged, case ignored (ci), punctuation preserved (pp):
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG3_posx_ci_pp);
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG4_posx_ci_pp);
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdG5_posx_ci_pp);
		
		// (1.19) Microsoft, punctuation excluded (pe):
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdM3_pe);
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdM4_pe);
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdM5_pe);
		
		// (1.20) Microsoft, punctuation ignored (pi):
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdM3_pi);
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdM4_pi);
		stX.executeUpdate("DELETE FROM xr_var_roi WHERE var_id = " + varIdM5_pi);
		
		// (3) Retrieve word predictability:
		try {
			/*
			res.append(retrieveWordPred_n(dbiX, stX, username, 3, (doGoogle ? varIdG3c : DBI.ID_NONE), varIdG3d, (doMicrosoft ? varIdM3c : DBI.ID_NONE), varIdM3d));
			res.append(retrieveWordPred_n(dbiX, stX, username, 4, (doGoogle ? varIdG4c : DBI.ID_NONE), varIdG4d, (doMicrosoft ? varIdM4c : DBI.ID_NONE), varIdM4d));
			res.append(retrieveWordPred_n(dbiX, stX, username, 5, (doGoogle ? varIdG5c : DBI.ID_NONE), varIdG5d, (doMicrosoft ? varIdM5c : DBI.ID_NONE), varIdM5d));
			*/
			
			DBI.transCommit(stX, true);
		}
		catch (SQLException e) {
			DBI.transRollback(stX, true);
			
			if (e.getErrorCode() == DBI.SQL_ERR_DUP_ENTRY) return res;
			else throw e;
		}
		
		return res;
	}
	
	
	// ----^----
	private StringBuilder retrieveWordPred_n(DBI dbiX, Statement stX, String username, int n, int varIdGc, int varIdGd, int varIdMc, int varIdMd)  throws MalformedURLException, IOException, SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		StringBuilder res = new StringBuilder("expId:" + this.expId + ", stimId:" + this.stimId + ", n:" + n + "\n");
		
		if (rois.size() < n) return res;
		
		List<ROI> ngram = new ArrayList<ROI>(rois.subList(0, n));
		for (int i = n; i < rois.size(); i++) {
			ROI lastROI = ngram.get(ngram.size() - 1);
			
			String ngramStr = "";
			for (int j = 0; j < n; j++) ngramStr += ngram.get(j).getName();
			
			// Google:
			if (varIdGc != DBI.ID_NONE) {
				double predC = getWordPredGoogle(lang, n, 100, 0, 0, ngramStr.replaceAll(" ", "+"));  // getPred("spa", 3, 2, 0, 0, "Ngo+Dinh+Diem")
				if (predC > 0) {
					predC = Math.log(predC);
					int predD = (int) Math.floor(predC / LOG_PRED_STEP);
					if (predD > 5) predD = 5;
					
					res.append(ngramStr + " " + predC + " " + predD + (i == rois.size() - 1 ? "\n\n" : "|"));
					stX.executeUpdate("INSERT INTO xr_var_roi (roi_id, var_id, val) VALUES (" + lastROI.getId() + ", " + varIdGc + ", " + predC + ")");
					stX.executeUpdate("INSERT INTO xr_var_roi (roi_id, var_id, val) VALUES (" + lastROI.getId() + ", " + varIdGd + ", " + predD + ")");
				}
			}
			
			// Microsoft:
			if (varIdMc != DBI.ID_NONE) {
				double predC = getWordPredMicrosoft(lang, n, ngramStr.replaceAll(" ", "+"));
				if (predC > MICROSOFT_MIN_LOG_CP) {
					int predD = (int) Math.floor(predC / LOG_PRED_STEP);
					if (predD > 5) predD = 5;
					
					res.append(ngramStr + " " + predC + " " + predD + (i == rois.size() - 1 ? "\n\n" : "|"));
					stX.executeUpdate("INSERT INTO xr_var_roi (roi_id, var_id, val) VALUES (" + lastROI.getId() + ", " + varIdMc + ", " + predC + ")");
					stX.executeUpdate("INSERT INTO xr_var_roi (roi_id, var_id, val) VALUES (" + lastROI.getId() + ", " + varIdMd + ", " + predD + ")");
				}
			}
			
			ngram.remove(0);
			ngram.add(rois.get(i));
		}
		
		return res;
	}
}
