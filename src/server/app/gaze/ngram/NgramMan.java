package app.gaze.ngram;

import inter.FSI;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import main.Do;
import app.gaze.ngram.NgramManItem.NgramManItemComparator;


/**
 * This class assists in creation of a list of ngrams that can later be used by the Ngram app.  This class ensures that 
 * an ngram which appears multiple times only appears once on the resulting ngram list but with all the DB information 
 * necessary to update all relevant ROIs.  This way, the Ngram app usage will be optimized.
 * 
 * @author Tomek D. Loboda
 */
public class NgramMan {
	final static String DIR_IN_FILES        = Do.getPathWork() + "ngrams" + File.separatorChar;
	final static String IN_FILE_SUFFIX = "-in";
	
	final Map<String, List<NgramManItem>> items = new LinkedHashMap<String, List<NgramManItem>>();
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public NgramMan() {}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public void add(String lang, int n, String ngram, String inf) {
		String letters = "";
		
		     if (ngram.startsWith( "_ADJ_ "  )) letters = "_ADJ_"  ;
		else if (ngram.startsWith( "_ADP_ "  )) letters = "_ADP_"  ;
		else if (ngram.startsWith( "_ADV_ "  )) letters = "_ADV_"  ;
		else if (ngram.startsWith( "_CONJ_ " )) letters = "_CONJ_" ;
		else if (ngram.startsWith( "_DET_ "  )) letters = "_DET_"  ;
		else if (ngram.startsWith( "_NOUN_ " )) letters = "_NOUN_" ;
		else if (ngram.startsWith( "_NUM_ "  )) letters = "_NUM_"  ;
		else if (ngram.startsWith( "_PRON_ " )) letters = "_PRON_" ;
		else if (ngram.startsWith( "_PRT_ "  )) letters = "_PRT_"  ;
		else if (ngram.startsWith( "_VERB_ " )) letters = "_VERB_" ;
		else {
			letters = ngram.substring(0,1);
			if (ngram.charAt(1) != ' ') letters += ngram.charAt(1);
			else letters += '_';
			letters = letters.toLowerCase();
		}
		
		String k = genKey(lang, n, letters);
		if (!items.containsKey(k)) items.put(k, new ArrayList<NgramManItem>());
		items.get(k).add(new NgramManItem(lang, n, letters, ngram, inf));
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Generates the input files used by the Ngram app (the file system based version) to retrieve word predictability.
	 */
	public int genInFiles()  throws IOException {
		FSI.addDir   (DIR_IN_FILES);
		FSI.cleanDir (DIR_IN_FILES);
		
		int n = 0;
		for (String k : items.keySet()) {
			List<NgramManItem> lst = items.get(k);
			Collections.sort(lst, new NgramManItemComparator());
			
			StringBuffer sb = new StringBuffer();
			Iterator<NgramManItem> i = lst.iterator();
			while (i.hasNext()) {
				sb.append(i.next().get());
			}
			FSI.addFile(DIR_IN_FILES + k + IN_FILE_SUFFIX, sb.toString());
			
			n += lst.size();
		}
		
		return n;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public String genKey(String lang, int n, String letters) { return lang + "-" + n + "-" + letters; }
}
