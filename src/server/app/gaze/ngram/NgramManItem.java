package app.gaze.ngram;

import java.util.Comparator;


/**
 * An element stored and managed by the NgramMan object.
 * 
 * @author Tomek D. Loboda
 */
public class NgramManItem {
	private final String lang;
	private final int     n;
	private final String  letters;
	private final String  ngram;
	private final String  inf;
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public NgramManItem(String lang, int n, String letters, String ngram, String inf) {
		this.lang        = lang;
		this.n           = n;
		this.letters     = letters;
		this.ngram       = ngram;
		this.inf         = inf;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public String get() { return ngram + "\t" + inf +  "\n"; }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public String getLang    () { return lang;    }
	public int    getN       () { return n;       }
	public String getLetters () { return letters; }
	public String getNgram   () { return ngram;   }
	
	
	// =================================================================================
	public static class NgramManItemComparator implements Comparator<NgramManItem> {
	    @Override
	    public int compare(NgramManItem a, NgramManItem b) {
	        return a.getNgram().compareTo(b.getNgram());
	    }
	}
}
