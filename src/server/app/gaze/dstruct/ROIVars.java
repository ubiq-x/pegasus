package app.gaze.dstruct;


public class ROIVars {
	public int nf = 0;
	public int nfpf = 0;
	public int nr = 0;
	public int nfpr = 0;
	
	public float afd = 0;
	public float ffd = 0;
	public float sfd = 0;
	public float gd = 0;
	public float tt = 0;
	public float rp = 0;
}
