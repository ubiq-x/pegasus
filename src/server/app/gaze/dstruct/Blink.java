package app.gaze.dstruct;


public class Blink {
	public int id;
	public Long tExp;
	public Long tTrial;
	public Float sx;
	public Float sy;
	public Float ex;
	public Float ey;
	public int dur;
	public int saccDur;
	public Float saccAmp;
	public int ot;      // off-text code
	public int os;      // off-screen code
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Blink(int id, Long tExp, Long tTrial, Float sx, Float sy, Float ex, Float ey, int dur, int saccDur, Float saccAmp, int ot, int os) {
		this.id      = id;
		this.tExp    = tExp;
		this.tTrial  = tTrial;
		this.sx      = sx;
		this.sy      = sy;
		this.ex      = ex;
		this.ey      = ey;
		this.dur     = dur;
		this.saccDur = saccDur;
		this.saccAmp = saccAmp;
		this.ot      = ot;
		this.os      = os;
	}
}
