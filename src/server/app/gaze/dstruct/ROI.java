package app.gaze.dstruct;


import inter.DBI;

import java.awt.Point;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 * Vertices are listed in the clockwise order.
 * 
 * @author Tomek D. Loboda
 */
public class ROI {
	public final int id;
	public final int num;
	public final String name;
	public final String type;
	public final int len;
	
	public float ppc = -1;  // pixels per character; -1 indicates the values has to be calculated
	
	public final List<Point> vertices = new ArrayList<Point>();
	
	//public Integer lnNum  = null;
	public Integer let    = null;
	public Integer letAdj = null;
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * C'tor.
	 */
	public ROI(int id, int num, String name, String type) {
		this.id    = id;
		this.num   = num;
		this.name  = name;
		this.type  = type;
		this.len   = name.length();
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Point getA() { return vertices.get(0); }
	public Point getB() { return vertices.get(1); }
	public Point getC() { return vertices.get(2); }
	public Point getD() { return vertices.get(3); }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Returns the character order number that occupies the given coordinates. The value of -1 is returned if the 
	 * coordinates are not inside this ROI. 
	 * 
	 * It is assumed the ROI is a rectangle (polygons not implemented yet).
	 */
	public int getLet(float x, float y) {
		Point A = vertices.get(0);
		Point C = vertices.get(2);
		
		boolean inside = (A.x <= x && A.y <= y && C.x >= x && C.y >= y);
		
		if (!inside) return -1;
		
		int d = Math.round(x) - A.x;                   // the x distance from the beginning of the ROI to the fixation
		int let = ((int) Math.ceil((float) d / ppc));  // use ceil to get a 1-based value
		if (let == 0) let = 1;                         // a fix for fixations which happen right at the left edge of the ROI
		if (name.charAt(0) == ' ') let--;              // transform into a 0-based value (the leading space gets the 0)
		if (let > name.length()) let--;                // don't know why, but some (only few) ROIs need this fix (probably it's due to rounding errors); it's a sae fix anyway so no biggie
		
		return let;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * It is assumed the ROI is a rectangle (polygons not implemented yet).
	 */
	public boolean isInside(float x, float y) {
		x = Math.round(x);
		y = Math.round(y);
		
		Point A = vertices.get(0);
		Point C = vertices.get(2);
		
		return (A.x <= x && A.y <= y && C.x >= x && C.y >= y);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public void load(DBI dbi, String db) 
		throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		
		ResultSet rs = dbi.execQry("SELECT x, y FROM " + db + ".vertex WHERE roi_id = " + id + " ORDER BY id");
		while (rs.next()) vertices.add(new Point(rs.getInt("x"), rs.getInt("y")));
		rs.close();
		
		ppc = (vertices.get(2).x - vertices.get(0).x) / name.length();
	}
}
