package app.gaze.dstruct;

import java.util.ArrayList;
import java.util.List;

import app.gaze.adj.Box;


public class StimPar {
	public final int idx;
	public final List<ROI> rois = new ArrayList<ROI>();
	//public final List<List<ROI>> tLines = new ArrayList<List<ROI>>();
	public final List<TLine> tLines = new ArrayList<TLine>();
	public final List<FLine> fLines = new ArrayList<FLine>();
	
	public final Box box = new Box();
	
	public int longLineCnt = 0;
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * C'tor.
	 */
	public StimPar(int idx) {
		this.idx = idx;
		tLines.add(new TLine());
	}
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * Called to commit a paragraph, i.e. when no more ROIs are to be added. Bounding 
	 * box dimensions are calculated here.
	 */
	public void done() {
		if (tLines.size() == 0) return;
		
		// Find the bounding box:
		box.t = tLines.get(0).getROI(0).getA().y;
		box.b = tLines.get(tLines.size()-1).getROI(0).getC().y;
		
		for (TLine l: tLines) {
			int x = l.getROI(0).getA().x;
			if (x < box.l) box.l = x;
			if (x > box.r) box.r = x;
		}
	}
	
	
	// ---------------------------------------------------------------------------------
	public FLine getBottomMostFLn() {
		if (fLines.size() == 0) return null;
		
		FLine l = fLines.get(0);
		for (int i = 1; i < fLines.size()-1; i++) {
			if (fLines.get(i).b > l.b) l = fLines.get(i); 
		}
		return l;
	}
	
	
	// ---------------------------------------------------------------------------------
	public FLine getTopMostFLn() {
		if (fLines.size() == 0) return null;
		
		FLine l = fLines.get(0);
		for (int i = 1; i < fLines.size()-1; i++) {
			if (fLines.get(i).b < l.b) l = fLines.get(i); 
		}
		return l;
	}
}
