package app.gaze.dstruct;

import java.util.Hashtable;


/**
 * ...
 * 
 * @author Tomek D. Loboda
 */
public class SubStats {
	private final Hashtable<String,SubVars> trials = new Hashtable<String, SubVars>();
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * C'tor.
	 */
	public SubStats() {}
	
	
	// ---------------------------------------------------------------------------------
	public void addROIVars(String trial, ROIVars rv) {
		if (!trials.containsKey(trial)) trials.put(trial, new SubVars());
		SubVars sv = trials.get(trial);
		
		sv.fc   += rv.nf;
		sv.fpfc += rv.nfpf;
		
		sv.afd = (sv.afd == 0 ? rv.afd : (sv.afd + rv.afd) / 2);
		sv.ffd = (sv.ffd == 0 ? rv.ffd : sv.ffd);
		sv.sfd = (sv.fc == 1 ? rv.sfd : -1);
		
		sv.gd += rv.gd;
		sv.tt += rv.tt;
		sv.rp += rv.rp;
		
		sv.wfc++;
	}
	
	
	// ---------------------------------------------------------------------------------
	public void setVars(String trial, int otfc, int regrCnt, int evtCnt) {
		if (!trials.containsKey(trial)) trials.put(trial, new SubVars());
		SubVars sv = trials.get(trial);
		
		sv.otfc    = otfc;
		sv.regrCnt = regrCnt;
		sv.evtCnt  = evtCnt;
	}
}
