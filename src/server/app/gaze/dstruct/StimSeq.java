package app.gaze.dstruct;


/**
 * @author Tomek D. Loboda
 */
public class StimSeq {
	public final int id;
	public final Stim stim;
	public final long t0, t1;
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * C'tor.
	 */
	public StimSeq(int id, Stim stim, long t0, long t1) {
		this.id = id;
		this.stim = stim;
		this.t0 = t0;
		this.t1 = t1;
	}
}
