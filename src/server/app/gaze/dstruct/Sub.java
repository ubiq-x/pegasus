package app.gaze.dstruct;

import inter.DBI;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;

import app.gaze.aggr.ROISet;

import util.RegExpr;


/**
 * Stores all ROIs for a given subject (i.e. for all stimuli used in a set of given trials).
 * 
 * @author Tomek D. Loboda
 */
public class Sub {
	private final String db;
	private final int id;
	@SuppressWarnings("unused")
	private final String name;
	
	private final Hashtable<String,ROISet> rois = new Hashtable<String,ROISet>();
	@SuppressWarnings("unused")
	private final List<String> trialNames = new ArrayList<String>();
	
	public int regrCnt = 0;
	public int evtCnt = 0;
	
	public String info = "";
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * C'tor.
	 */
	public Sub(String db, int id, String name) {
		this.db = db;
		this.id = id;
		this.name = name;
	}
	
	
	// ---------------------------------------------------------------------------------
	public void addROI(int id, String name, int num)  throws SQLException {
		if (!rois.containsKey(name)) rois.put(name, new ROISet(name));
		rois.get(name).addROI(null, id, num);
	}
	
	
	// ---------------------------------------------------------------------------------
	/*
	public void calcStats2(SQLConnection conn, String trialIds, String stimIds, String evts)  throws SQLException {
		evtCnt = 0;
		info = name + "\n";
		
		StringBuffer qry = getFixQry(conn, trialIds, stimIds, evts);
		if (qry.length() == 0) return;
		
		Statement st = conn.get().createStatement();
		ResultSet rs = st.executeQuery(qry.toString());
		
		ROIComp prevROI = null;  // for gaze time calculation
		List<ROIComp> lstRP = new ArrayList<ROIComp>();  // for regr. path calculation
		int prevTrialId = -1;  // for regr. path calculation
		
		while (rs.next()) {
			trialNames.add(rs.getString("tname"));
			
			float dur = rs.getFloat("dur");
			
			ROIComp currROI = rois.get(rs.getString("rname")).get(rs.getInt("rid"));
			int currTrialId = rs.getInt("tid");
			
			if (prevROI != null && prevROI != currROI) prevROI.setGDDone();
			
			if (!currROI.isRPDone() && !lstRP.contains(currROI)) lstRP.add(currROI);
			Iterator it = lstRP.iterator();
			while (it.hasNext()) {
				ROIComp r = (ROIComp) it.next();
				if (currTrialId != prevTrialId || currROI.num > r.num) {
					r.setRPDone();
					it.remove();
				}
				else {
					r.digestFixRP(dur);
				}
			}
			currROI.digestFix(dur);
			if (rs.getInt("sacc") < 0) regrCnt++;
			
			prevROI = currROI;
			prevTrialId = currTrialId;
		}
		
		rs.close();
		st.close();
		
		updateStats();
	}
	*/
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * Decodes the events string received from the client.
	 */
	private Object[] decodeEvts(String evts) {
		Hashtable<String, int[]> t = new Hashtable<String, int[]>();
		Hashtable<String, char[]> fix = new Hashtable<String, char[]>();
		
		if (evts != null && evts.length() > 0) {
			String [] E = evts.split(";");
			for (String e: E) {
				Matcher m = RegExpr.match("\"(.+)\":(\\d+),(\\d+),(\\d+),(\\d+),(.)(.)(.)(.)", e);
				t.put(m.group(1), new int[] { new Integer(m.group(2)), new Integer(m.group(3)), new Integer(m.group(4)), new Integer(m.group(5)) });
				fix.put(m.group(1), new char[] { m.group(6).charAt(0), m.group(7).charAt(0), m.group(8).charAt(0), m.group(9).charAt(0) });
			}
		}
		
		return new Object[] { t, fix };
	}
	
	
	// ---------------------------------------------------------------------------------
	private String getFixInRangeSql(int t1, int t2, boolean in, boolean out) {
		String res = "(f.t BETWEEN " + t1 + " AND " + t2 + (in ? " OR f.t+f.dur BETWEEN " + t1 + " AND " + t2 + ")" : ")");
		if (!out) res = "(" + res + " AND (f.t+f.dur BETWEEN " + t1 + " AND " + t2 + "))";
		return res;
	}
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * Returns a query that retrieves fixations matching the speficied set of 
	 * constraints.
	 */
	@SuppressWarnings({ "unchecked", "unused" })
	private StringBuffer getFixQry(Connection conn, String trialIds, String stimIds, String evts)  throws SQLException {
		List<String> lstQry = new ArrayList<String>();
		
		Object[] E = decodeEvts(evts);
		Hashtable<String, int[]> ET = (Hashtable<String, int[]>) E[0];  // times
		Hashtable<String, char[]> EF = (Hashtable<String, char[]>) E[1];  // fixations
		
		Statement stTrial = conn.createStatement();
		ResultSet rsTrial = stTrial.executeQuery("SELECT id, name FROM " + db + ".trial WHERE sub_id = " + id + (trialIds != null && trialIds.length() > 0 ? " AND id IN (" + trialIds + ")" : "") + " ORDER BY id");
		
		if (evts.length() > 0) info += "    Trials\n";
		while (rsTrial.next()) {
			if (evts.length() > 0) info += "        " + rsTrial.getString("name") + ": ";
			
			int trialEvtCnt = 0;
			
			Statement stSS = conn.createStatement();
			ResultSet rsSS = stSS.executeQuery("SELECT id FROM " + db + ".stim_seq WHERE trial_id = " + rsTrial.getInt("id") + (stimIds != null && stimIds.length() > 0 ? " AND stim_id IN (" + stimIds + ")" : "") + " ORDER BY t0");
			
			while (rsSS.next()) {
				int ssId = rsSS.getInt("id");
				
				// Event constraints absent:
				if (ET.size() == 0) {
					lstQry.add(
						"(" +
						"SELECT f.dur, t.id AS tid, t.name AS tname, r.id AS rid, r.name AS rname, r.num AS rnum, f.sacc " +
						"FROM " + db + ".fix f " +
						"INNER JOIN " + db + ".trial t ON f.trial_id = t.id " +
						"INNER JOIN " + db + ".roi r ON f.roi_id = r.id " +
						"WHERE stim_seq_id = " + ssId + " AND NOT ISNULL(f.roi_id) AND f.exc = 0 " +
						"ORDER BY f.trial_id, f.num" +
						")\n"
					);
				}
				
				// Event constraints present:
				else {
					int constrCnt = 0;  // number of constraints actually entering the query
					
					String qry =
						"(" +
						"SELECT f.dur, t.id AS tid, r.id AS rid, r.name AS rname, r.num AS rnum, f.sacc " +
						"FROM " + db + ".fix f " +
						"INNER JOIN " + db + ".trial t ON f.trial_id = t.id " +
						"INNER JOIN " + db + ".roi r ON f.roi_id = r.id " +
						"WHERE stim_seq_id = " + ssId + " AND NOT ISNULL(f.roi_id) AND f.exc = 0 ";
					
					Iterator it = ET.keySet().iterator();
					while (it.hasNext()) {
						String name = (String) it.next();
						int [] T = ET.get(name);
						char[] F = EF.get(name);
						
						boolean f01 = F[0] == 'i';
						boolean f02 = F[1] == 'i';
						boolean f03 = F[2] == 'i';
						boolean f04 = F[3] == 'i';
						
						Statement stEvt = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
						ResultSet rsEvt = stEvt.executeQuery("(SELECT t FROM " + db + ".evt WHERE stim_seq_id = " + ssId + " AND name = '" + name + "')\n");
						
						if (DBI.getRecCnt(rsEvt) == 0) continue;  // no event occurrances
						
						qry += "AND (";
						while (rsEvt.next()) {
							int t = rsEvt.getInt("t");
							int t01 = t - T[0];
							int t02 = t - T[1];
							int t03 = t + T[2];
							int t04 = t + T[3];
							
							if (T[0] > 0 && T[3] > 0 && T[1] == 0 && T[2] == 0) {  // around
								qry += getFixInRangeSql(t01, t04, f01, f04);
							}
							else if (T[0] > 0 && T[3] == 0) {  // to the left only
								qry += getFixInRangeSql(t01, t02, f01, f02);
							}
							else if (T[0] == 0 && T[3] > 0) {  // to the right only
								qry += getFixInRangeSql(t03, t04, f03, f04);
							}
							else {  // to the left and right (with a space in-between)
								qry += "(" + getFixInRangeSql(t01, t02, f01, f02) + " OR " + getFixInRangeSql(t03, t04, f03, f04) + ")";
							}
							qry += (!rsEvt.isLast() ? " OR " : " ");
							
							constrCnt++;
							trialEvtCnt++;
							evtCnt++;
						}
						rsEvt.close();
						stEvt.close();
						qry += (evtCnt > 0 ? ") " : "");
					}
					
					qry +=
						"ORDER BY f.trial_id, f.num" +
						")\n";
					if (constrCnt > 0) lstQry.add(qry);
				}
			}
			rsSS.close();
			stSS.close();
			
			if (evts.length() > 0) info += "evt-cnt=" + trialEvtCnt + "\n";
		}
		rsTrial.close();
		stTrial.close();
		
		if (evts.length() > 0) info += "    Total event count: " + evtCnt + "\n";
		
		StringBuffer res = new StringBuffer();
		for (int i = 0; i < lstQry.size()-1; i++) res.append(lstQry.get(i) + " UNION ALL ");
		if (lstQry.size() > 0) res.append(lstQry.get(lstQry.size()-1));
		
		return res;
	}
	
	
	// ---------------------------------------------------------------------------------
	public ROISet getROI(String name) { return rois.get(name); }
}
