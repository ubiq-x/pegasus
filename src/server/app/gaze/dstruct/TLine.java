package app.gaze.dstruct;

import java.util.ArrayList;
import java.util.List;


public class TLine {
	public final List<ROI> rois = new ArrayList<ROI>();
	
	public int len = 0;
	public int cy = -1;  // center y
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * C'tor.
	 */
	public TLine() {}
	
	
	// ---------------------------------------------------------------------------------
	public void addROI(ROI r) {
		len += r.getC().x - r.getA().x;
		if (rois.size() == 0) cy = (r.getC().y - r.getA().y) / 2;
		rois.add(r);
	}
	
	
	// ---------------------------------------------------------------------------------
	public ROI getROI(int idx) { return (idx >= rois.size() ? null : rois.get(idx)); }
}
