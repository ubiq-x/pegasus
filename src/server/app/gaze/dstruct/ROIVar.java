package app.gaze.dstruct;


public class ROIVar {
	public final int id;
	public final boolean ss;
	public final boolean se;
	public final boolean ce;
	public final boolean cs;
	
	public boolean done = false;
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * C'tor.
	 */
	public ROIVar(int id, boolean ss, boolean se, boolean ce, boolean cs) {
		this.id = id;
		this.ss = ss;
		this.se = se;
		this.cs = cs;
		this.ce = ce;
	}
}
