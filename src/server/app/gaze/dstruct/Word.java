package app.gaze.dstruct;

import inter.DBI;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import util.$;
import app.data.Var;
import app.gaze.aggr.Aggr;


/**
 * @author Tomek D. Loboda
 */
public class Word {
	public static final String[] VAR_NAMES = { "word", "word_nlet", "mrc_n", "mrc_nphon", "mrc_nsyl", "mrc_kffrq", "mrc_kffrq_ln", "mrc_kffrq_log10", "mrc_kfcat", "mrc_kfsmp", "mrc_tlfrq", "mrc_tlfrq_ln", "mrc_tlfrq_log10", "mrc_bfrq", "mrc_bfrq_ln", "mrc_bfrq_log10", "mrc_fam", "mrc_cnc", "mrc_img", "mrc_meanc", "mrc_meanp", "mrc_aoa", "subtl_frq", "subtl_frq_ln", "subtl_frq_log10" };
	
	private final String  word;
	private Integer nlet;
	
	private Integer mrc_n;
	private Integer mrc_nphon;
	private Integer mrc_nsyl;
	private Integer mrc_kffrq;
	private Float   mrc_kffrq_ln;
	private Float   mrc_kffrq_log10;
	private Integer mrc_kfcat;
	private Integer mrc_kfsmp;
	private Integer mrc_tlfrq;
	private Float   mrc_tlfrq_ln;
	private Float   mrc_tlfrq_log10;
	private Integer mrc_bfrq;
	private Float   mrc_bfrq_ln;
	private Float   mrc_bfrq_log10;
	private Integer mrc_fam;
	private Integer mrc_cnc;
	private Integer mrc_img;
	private Integer mrc_meanc;
	private Integer mrc_meanp;
	private Integer mrc_aoa;
	
	private Integer subtl_frq;
	private Float   subtl_frq_ln;
	private Float   subtl_frq_log10;
	
	/*
	mrc_tq2
	mrc_type
	mrc_pdtype
	mrc_alphsyl
	mrc_status
	mrc_var
	mrc_cap
	mrc_plur
	mrc_phon
	mrc_dphon
	mrc_stress
	*/
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Word(ResultSet rs)  throws SQLException {
		word            = rs.getString("word");
		
		nlet            = rs.getInt  ("nlet");             if (rs.wasNull()) nlet            = null;
		
		mrc_n           = rs.getInt  ("mrc_n");            if (rs.wasNull()) mrc_n           = null;
		mrc_nphon       = rs.getInt  ("mrc_nphon");        if (rs.wasNull()) mrc_nphon       = null;
		mrc_nsyl        = rs.getInt  ("mrc_nsyl");         if (rs.wasNull()) mrc_nsyl        = null;
		mrc_kffrq       = rs.getInt  ("mrc_kffrq");        if (rs.wasNull()) mrc_kffrq       = null;
		mrc_kffrq_ln    = rs.getFloat("mrc_kffrq_ln");     if (rs.wasNull()) mrc_kffrq_ln    = null;
		mrc_kffrq_log10 = rs.getFloat("mrc_kffrq_log10");  if (rs.wasNull()) mrc_kffrq_log10 = null;
		mrc_kfcat       = rs.getInt  ("mrc_kfcat");        if (rs.wasNull()) mrc_kfcat       = null;
		mrc_kfsmp       = rs.getInt  ("mrc_kfsmp");        if (rs.wasNull()) mrc_kfsmp       = null;
		mrc_tlfrq       = rs.getInt  ("mrc_tlfrq");        if (rs.wasNull()) mrc_tlfrq       = null;
		mrc_tlfrq_ln    = rs.getFloat("mrc_tlfrq_ln");     if (rs.wasNull()) mrc_tlfrq_ln    = null;
		mrc_tlfrq_log10 = rs.getFloat("mrc_tlfrq_log10");  if (rs.wasNull()) mrc_tlfrq_log10 = null;
		mrc_bfrq        = rs.getInt  ("mrc_bfrq");         if (rs.wasNull()) mrc_bfrq        = null;
		mrc_bfrq_ln     = rs.getFloat("mrc_bfrq_ln");      if (rs.wasNull()) mrc_bfrq_ln     = null;
		mrc_bfrq_log10  = rs.getFloat("mrc_bfrq_log10");   if (rs.wasNull()) mrc_bfrq_log10  = null;
		mrc_fam         = rs.getInt  ("mrc_fam");          if (rs.wasNull()) mrc_fam         = null;
		mrc_cnc         = rs.getInt  ("mrc_cnc");          if (rs.wasNull()) mrc_cnc         = null;
		mrc_img         = rs.getInt  ("mrc_img");          if (rs.wasNull()) mrc_img         = null;
		mrc_meanc       = rs.getInt  ("mrc_meanc");        if (rs.wasNull()) mrc_meanc       = null;
		mrc_meanp       = rs.getInt  ("mrc_meanp");        if (rs.wasNull()) mrc_meanp       = null;
		mrc_aoa         = rs.getInt  ("mrc_aoa");          if (rs.wasNull()) mrc_aoa         = null;
		
		subtl_frq       = rs.getInt  ("subtl_frq");        if (rs.wasNull()) subtl_frq       = null;
		subtl_frq_ln    = rs.getFloat("subtl_frq_ln");     if (rs.wasNull()) subtl_frq_ln    = null;
		subtl_frq_log10 = rs.getFloat("subtl_frq_log10");  if (rs.wasNull()) subtl_frq_log10 = null;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public List<String> getValues(Set<String> incVars, boolean incWord) {
		List<String> res = new ArrayList<String>();
		
		if (incWord) res.add(Aggr.varGetVal(incVars, "word", DBI.esc(word)));
		
		res.add(Aggr.varGetVal(incVars, "word_nlet",       nlet)                                                                 );
		
		res.add(Aggr.varGetVal(incVars, "mrc_n",           (mrc_n           == null ? "null" : "" + mrc_n))                      );
		res.add(Aggr.varGetVal(incVars, "mrc_nphon",       (mrc_nphon       == null ? "null" : "" + mrc_nphon))                  );
		res.add(Aggr.varGetVal(incVars, "mrc_nsyl",        (mrc_nsyl        == null ? "null" : "" + mrc_nsyl))                   );
		res.add(Aggr.varGetVal(incVars, "mrc_kffrq",       (mrc_kffrq       == null ? "null" : "" + mrc_kffrq))                  );
		res.add(Aggr.varGetVal(incVars, "mrc_kffrq_ln",    (mrc_kffrq_ln    == null ? "null" : "" + $.trunc(mrc_kffrq_ln,    2))));
		res.add(Aggr.varGetVal(incVars, "mrc_kffrq_log10", (mrc_kffrq_log10 == null ? "null" : "" + $.trunc(mrc_kffrq_log10, 2))));
		res.add(Aggr.varGetVal(incVars, "mrc_kfcat",       (mrc_kfcat       == null ? "null" : "" + mrc_kfcat))                  );
		res.add(Aggr.varGetVal(incVars, "mrc_kfsmp",       (mrc_kfsmp       == null ? "null" : "" + mrc_kfsmp))                  );
		res.add(Aggr.varGetVal(incVars, "mrc_tlfrq",       (mrc_tlfrq       == null ? "null" : "" + mrc_tlfrq))                  );
		res.add(Aggr.varGetVal(incVars, "mrc_tlfrq_ln",    (mrc_tlfrq_ln    == null ? "null" : "" + $.trunc(mrc_tlfrq_ln,    2))));
		res.add(Aggr.varGetVal(incVars, "mrc_tlfrq_log10", (mrc_tlfrq_log10 == null ? "null" : "" + $.trunc(mrc_tlfrq_log10, 2))));
		res.add(Aggr.varGetVal(incVars, "mrc_bfrq",        (mrc_bfrq        == null ? "null" : "" + mrc_bfrq))                   );
		res.add(Aggr.varGetVal(incVars, "mrc_bfrq_ln",     (mrc_bfrq_ln     == null ? "null" : "" + $.trunc(mrc_bfrq_ln,    2)) ));
		res.add(Aggr.varGetVal(incVars, "mrc_bfrq_log10",  (mrc_bfrq_log10  == null ? "null" : "" + $.trunc(mrc_bfrq_log10, 2)) ));
		res.add(Aggr.varGetVal(incVars, "mrc_fam",         (mrc_fam         == null ? "null" : "" + mrc_fam))                    );
		res.add(Aggr.varGetVal(incVars, "mrc_cnc",         (mrc_cnc         == null ? "null" : "" + mrc_cnc))                    );
		res.add(Aggr.varGetVal(incVars, "mrc_img",         (mrc_img         == null ? "null" : "" + mrc_img))                    );
		res.add(Aggr.varGetVal(incVars, "mrc_meanc",       (mrc_meanc       == null ? "null" : "" + mrc_meanc))                  );
		res.add(Aggr.varGetVal(incVars, "mrc_meanp",       (mrc_meanp       == null ? "null" : "" + mrc_meanp))                  );
		res.add(Aggr.varGetVal(incVars, "mrc_aoa",         (mrc_aoa         == null ? "null" : "" + mrc_aoa))                    );
		
		res.add(Aggr.varGetVal(incVars, "subtl_frq",       (subtl_frq       == null ? "null" : "" + subtl_frq))                  );
		res.add(Aggr.varGetVal(incVars, "subtl_frq_ln",    (subtl_frq_ln    == null ? "null" : "" + $.trunc(subtl_frq_ln,    2))));
		res.add(Aggr.varGetVal(incVars, "subtl_frq_log10", (subtl_frq_log10 == null ? "null" : "" + $.trunc(subtl_frq_log10, 2))));
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static List<String> getValuesEmpty(Set<String> incVars, String val, boolean incWord) {
		List<String> res = new ArrayList<String>();
		
		int varCnt = 0;
		if (incVars == null || incVars.size() == 0) varCnt = VAR_NAMES.length - (incWord ? 0 : 1);
		else {
			for (String v: VAR_NAMES) {
				if (incVars.contains(v) && (!incWord || (incWord && v.equals("word")))) varCnt++;
			}
		}
		
		for (int i = 0; i < varCnt; i++) {
			res.add(val);
		}
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Returns names and types of variables. 
	 */
	public static List<Var> getVars(Set<String> incVars, boolean singleROI, String prefix) {
		List<Var> res = new ArrayList<Var>();
		
		if (singleROI) Aggr.varLstAdd(res, incVars, Var.TYPE_STR, "word", prefix);
		
		int type = (singleROI ? Var.TYPE_DIS : Var.TYPE_CON);
		
		Aggr.varLstAdd(res, incVars, type,         "word_nlet",       prefix);
		
		Aggr.varLstAdd(res, incVars, type,         "mrc_n",           prefix);
		Aggr.varLstAdd(res, incVars, type,         "mrc_nphon",       prefix);
		Aggr.varLstAdd(res, incVars, type,         "mrc_nsyl",        prefix);
		Aggr.varLstAdd(res, incVars, type,         "mrc_kffrq",       prefix);
		Aggr.varLstAdd(res, incVars, Var.TYPE_CON, "mrc_kffrq_ln",    prefix);
		Aggr.varLstAdd(res, incVars, Var.TYPE_CON, "mrc_kffrq_log10", prefix);
		Aggr.varLstAdd(res, incVars, type,         "mrc_kfcat",       prefix);
		Aggr.varLstAdd(res, incVars, type,         "mrc_kfsmp",       prefix);
		Aggr.varLstAdd(res, incVars, type,         "mrc_tlfrq",       prefix);
		Aggr.varLstAdd(res, incVars, Var.TYPE_CON, "mrc_tlfrq_ln",    prefix);
		Aggr.varLstAdd(res, incVars, Var.TYPE_CON, "mrc_tlfrq_log10", prefix);
		Aggr.varLstAdd(res, incVars, type,         "mrc_bfrq",        prefix);
		Aggr.varLstAdd(res, incVars, Var.TYPE_CON, "mrc_bfrq_ln",     prefix);
		Aggr.varLstAdd(res, incVars, Var.TYPE_CON, "mrc_bfrq_log10",  prefix);
		Aggr.varLstAdd(res, incVars, type,         "mrc_fam",         prefix);
		Aggr.varLstAdd(res, incVars, type,         "mrc_cnc",         prefix);
		Aggr.varLstAdd(res, incVars, type,         "mrc_img",         prefix);
		Aggr.varLstAdd(res, incVars, type,         "mrc_meanc",       prefix);
		Aggr.varLstAdd(res, incVars, type,         "mrc_meanp",       prefix);
		Aggr.varLstAdd(res, incVars, type,         "mrc_aoa",         prefix);
		
		Aggr.varLstAdd(res, incVars, Var.TYPE_CON, "subtl_frq",       prefix);
		Aggr.varLstAdd(res, incVars, Var.TYPE_CON, "subtl_frq_ln",    prefix);
		Aggr.varLstAdd(res, incVars, Var.TYPE_CON, "subtl_frq_log10", prefix);
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public String  getWord()          { return word;            }
	public Integer getNlet()          { return nlet;            }
	
	public Integer getMRCN()          { return mrc_n;           }
	public Integer getMRCNphon()      { return mrc_nphon;       }
	public Integer getMRCNsyl()       { return mrc_nsyl;        }
	public Integer getMRCKffrq()      { return mrc_kffrq;       }
	public Float   getMRCKffrqLn()    { return mrc_kffrq_ln;    }
	public Float   getMRCKffrqLog10() { return mrc_kffrq_log10; }
	public Integer getMRCKfcat()      { return mrc_kfcat;       }
	public Integer getMRCKfsmp()      { return mrc_kfsmp;       }
	public Integer getMRCTlfrq()      { return mrc_tlfrq;       }
	public Float   getMRCTlfrqLn()    { return mrc_tlfrq_ln;    }
	public Float   getMRCTlfrqLog10() { return mrc_tlfrq_log10; }
	public Integer getMRCBfrq()       { return mrc_bfrq;        }
	public Float   getMRCBfrqLn()     { return mrc_bfrq_ln;     }
	public Float   getMRCBfrqLog10()  { return mrc_bfrq_log10;  }
	public Integer getMRCFam()        { return mrc_fam;         }
	public Integer getMRCCnc()        { return mrc_cnc;         }
	public Integer getMRCImg()        { return mrc_img;         }
	public Integer getMRCMeanc()      { return mrc_meanc;       }
	public Integer getMRCMeanp()      { return mrc_meanp;       }
	public Integer getMRCAoa()        { return mrc_aoa;         }
	
	public Integer getSubtlFrq()      { return subtl_frq;       }
	public Float   getSubtlFrqLn()    { return subtl_frq_ln;    }
	public Float   getSubtlFrqLog10() { return subtl_frq_log10; }
}
