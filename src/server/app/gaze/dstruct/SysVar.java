package app.gaze.dstruct;


import inter.DBI;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import main.Do;
import util.$;
import util.Stats;


public class SysVar {
	private final DBI dbi;
	private final String db;
	
	public final int id;
	public final String name;
	public final char type;
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * C'tor.
	 */
	public SysVar(DBI dbi, String username, int id, String name, char type) {
		this.dbi = dbi;
		this.db   = "\"" + Do.DB_GAZE + username + "\"";
		this.id   = id;
		this.name = name;
		this.type = type;
	}
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * Returns the column values to be put in the body of calculation results. The 
	 * number depends on the variable type.
	 */
	public String calcValues(List<String> roiIds) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		switch (type) {
			case 'b': return calcValuesB(roiIds);
			case 'n': return calcValuesN(roiIds);
			case 's': return "";
		}
		return "";
	}
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * Returns the column names to be put in the header of calculation results. The 
	 * number and names depend on the variable type.
	 */
	public String getColNames() {
		String n = "var-" + name;
		switch (type) {
			case 'b': return n + "-n0," + n + "-n1," + n + "-p0," + n + "-,";
			case 'n': return n + "-min," + n + "-max," + n + "-mean," + n + "-med," + n + "-sd," + n + "-se";
			case 's': return n;
		}
		return "";
	}
	
	
	// ---------------------------------------------------------------------------------
	private String calcValuesB(List<String> roiIds)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		if (roiIds == null || roiIds.size() == 0) return "null,null,null,null";
		
		ResultSet rs = dbi.execQry("SELECT COUNT(*) AS cnt FROM " + db + ".xr_var_roi x INNER JOIN " + db + ".var v ON x.var_id = v.id WHERE v.name = '" + name + "' AND roi_id IN (" + $.join(roiIds, "", "", ",", false, false) + ") AND val = 1");
		rs.next();
		int n1 = rs.getInt("cnt");
		rs.close();
		
		int n = roiIds.size();
		int n0 = n - n1;
		
		return n0 + "," + n1 + "," + $.trunc((float)n0 / (float)n, 2) + "," + $.trunc((float)n1 / (float)n, 2);
	}
	
	
	// ---------------------------------------------------------------------------------
	private String calcValuesN(List<String> roiIds)  throws SQLException {
		if (roiIds == null || roiIds.size() == 0) return "null,null,null,null,null,null";
		
		Stats s = new Stats();
		ResultSet rs = dbi.execQry("SELECT val FROM " + db + ".xr_var_roi x INNER JOIN " + db + ".var v ON x.var_id = v.id WHERE v.name = '" + name + "' AND roi_id IN (" + $.join(roiIds, "", "", ",", false, false) + ")");
		while (rs.next()) {
			s.addVal(rs.getFloat("val"));
		}
		rs.close();
		
		return s.get();
	}
}
