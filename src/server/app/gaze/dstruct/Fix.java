package app.gaze.dstruct;

import app.gaze.Exp;


public class Fix {
	public int id  = -1;
	public Long tExp = -1l;
	public Long tTrial = -1l;
	public int dur = -1;
	public int num = -1;
	
	public Float x    = null;
	public Float y    = null;
	public Float xAdj = null;
	public Float yAdj = null;
	
	public Float saccX    = null;
	public Float saccY    = null;
	public Float saccXAdj = null;
	public Float saccYAdj = null;
	
	public Integer saccInROI     = null;
	public Integer saccInROIAdj  = null;
	public Integer saccInLet     = null;
	public Integer saccInLetAdj  = null;
	public Integer saccOutROI    = null;
	public Integer saccOutROIAdj = null;
	public Integer saccOutLet    = null;
	public Integer saccOutLetAdj = null;
	
	public Float pupil = null;
	
	public int ot = Exp.DIR_NONE;  // off-text code
	public int os = Exp.DIR_NONE;  // off-screen code
	
	public boolean f  = false;  // flag: the first fixation on a word?
	public boolean fp = false;  // flag: first-pass?
	
	public Integer let = null;
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Fix(int id, Long tExp, Long tTrial, int dur, Float x, Float y, Float xAdj, Float yAdj, int num, Integer saccInROI, Integer saccInLet, Integer saccOutROI, Integer saccOutLet, Float pupil, int ot, int os, boolean fp, boolean f, Integer let) {
		this.id     = id;
		this.tExp   = tExp;
		this.tTrial = tTrial;
		this.dur    = dur;
		this.num    = num;
		
		this.x    = x;
		this.y    = y;
		this.xAdj = xAdj;
		this.yAdj = yAdj;
		
		this.saccInROI  = saccInROI;
		this.saccInLet  = saccInLet;
		this.saccOutROI = saccOutROI;
		this.saccOutLet = saccOutLet;
		
		this.pupil = pupil;
		
		this.ot = ot;
		this.os = os;
		
		this.fp = fp;
		this.f  = f;
		
		this.let = let;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Fix(int id, Long tExp, Long tTrial, int dur, Float x, Float y, Float xAdj, Float yAdj) {
		this.id     = id;
		this.tExp   = tExp;
		this.tTrial = tTrial;
		this.dur    = dur;
		
		this.x    = x;
		this.y    = y;
		this.xAdj = xAdj;
		this.yAdj = yAdj;
	}
	
	
}
