package app.gaze.dstruct;

import inter.DBI;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.gaze.adj.Box;


/**
 * @author Tomek D. Loboda
 */
public class Stim {
	public final int id;
	public final String name;
	public final String txt;
	public final int w;
	public final int h;
	
	public final List<ROI> rois = new ArrayList<ROI>();
	public final List<List<ROI>> lines = new ArrayList<List<ROI>>();
	public final List<StimPar> paragraphs = new ArrayList<StimPar>();
	
	private final Map<Integer, ROI> mapROIByNum = new HashMap<Integer, ROI>();
	
	public int lnH = -1;
	
	public final Box box = new Box();
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * C'tor.
	 */
	public Stim(int id, String name, String txt, int w, int h) {
		this.id   = id;
		this.name = name;
		this.txt  = txt;
		this.w = w;
		this.h = h;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * The first ROI the given coordinates are inside of will be returned. Returns the following int array:
	 * 
	 *   [id,num,let]
	 */
	public int[] inside(float x, float y) {
		for (ROI r: rois) {
			int let = r.getLet(x,y);
			if (let >= 0) {
				return new int[] { r.id, r.num, let };
			}
		}
		
		return new int[] { DBI.ID_NONE, 0, -1 };
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public ROI getROIByCoords(float x, float y) {
		for (ROI r: rois) {
			int let = r.getLet(x,y);
			if (let >= 0) {
				r.let = let;
				return r;
				
				// Above, the letter is set irrespective of whether the (x,y) coordinates are of the original or 
				// adjusted fixation. That is wrong and something like below should be done instead, where a new 
				// ROI is returned. As far as I understand after revising the EM-ROI linking code in the 
				// Exp.lnk() method this letter setting is inconsequential at least for the linking process, 
				// because the letter is computer later irrespective of the assignment below. Nevertheless this 
				// is an error and should be removed. This method isn't used for any other purpose than for 
				// the linking process.
				/*
				ROI r2 = new ROI(r.id, r.num, r.name, r.type);
				r2.let = let;
				return r2;
				*/
			}
		}
		
		return null;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public int getLineCnt() {
		return lines.size();
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public int getBoxH(int minROICnt) {
		int i = lines.size()-1;
		while (i > 0 && lines.get(i).size() < minROICnt) i--;
		return (lines.get(i).get(0).getC().y - box.t);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/*
	public int getFixSyncBoxH(List<Line> fLines, int maxStepCnt) {
		// Find the length of the bottom-most f-line:
		double bottomMostLnY = Double.MIN_VALUE;
		float bottomMostLnLen = -1.0f;
		for (int i = 0; i < fLines.size(); i++) {
			if (fLines.get(i).b > bottomMostLnY) {
				bottomMostLnY = fLines.get(i).b;
				bottomMostLnLen = fLines.get(i).len;
			}
		}
		
		// Check how many t-lines should be excluded:
		int i = lines.size()-1;
		while (i > 0 && (getLineLen(lines.get(i)) > 1.30 * bottomMostLnLen || getLineLen(lines.get(i)) < 0.70 * bottomMostLnLen)) i--;
		
		System.out.println(name + ": " + "i=" + i + ", bottomMostLnLen=" + bottomMostLnLen + ", lnLen=" + getLineLen(lines.get(i)));
		// Return:
		if (lines.size()-1 - i > maxStepCnt) return -1;  // step limit exceeded
		if (i < lines.size()-1) return lines.get(i).get(0).getC().y - box.t;  // some lines excluded
		return box.getH();  // all good as is
	}
	*/
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public int getLineLen(List<ROI> l) { return l.get(l.size()-1).getC().x - l.get(0).getA().x; }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * This method returns the saccade amplitude (or length) in character spaces between two ROIs. Obviously, these 
	 * ROIs should be fixated right after each other. They are assumed to be the source and destination of the  
	 * saccade, respectively.
	 */
	public Integer getSaccLet(ROI from, ROI to) {
		if (from == null || to == null) return null;
		
		// (1) Intra-ROI saccade:
		if (from.num == to.num) {
			return from.let - to.let;
		}
		
		// (2) Inter-ROI saccade:
		int res = 0;
		
		// (2.1) Determine the direction and account for letters on the source and destination ROIs (checking for leading spaces):
		int sign = (from.num < to.num ? 1 : -1);
		if (sign > 0) {  // forward saccade
			res += (from.len - from.let) + to.let + (to.name.charAt(0) == ' ' && from.let == from.len ? 1 : 0);
		}
		else {  // regression
			res += from.let + (from.name.charAt(0) == ' ' ? 1 : 0) + Math.max(to.len - to.let - (to.name.charAt(0) == ' ' ? 1 : 0), 0);
			
			ROI tmp = from;
			from = to;
			to = tmp;
		}
		
		// (2.2) Account for words in between (checking for leading spaces):
		for (int i = from.num + 1; i < to.num; i++) {
			ROI r = mapROIByNum.get(i);
			if (r != null) {
				res += mapROIByNum.get(i).len;
			}
		}
		
		return res * sign;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public void load(DBI dbi, String db)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		// Retrieve ROIs and distribute them among the lines and paragraphs: 
		ResultSet rs = dbi.execQry(
			"SELECT r.id, r.num, r.name, rt.name AS type FROM " + db + ".roi r " +
			"INNER JOIN " + db + ".roi_type rt ON r.type_id = rt.id " +
			"WHERE r.stim_id = " + id
		);
		
		List<ROI> L = new ArrayList<ROI>();  // line
		lines.add(L);
		
		StimPar p = new StimPar(0);
		paragraphs.add(p);
		TLine pL = p.tLines.get(0);  // paragraph line
		
		ROI pr = null;  // prev ROI
		
		while (rs.next()) {
			ROI r = new ROI(rs.getInt("id"), rs.getInt("num"), rs.getString("name"), rs.getString("type"));
			r.load(dbi, db);
			
			if (lnH == -1) lnH = r.getC().y - r.getA().y;
			
			rois.add(r);
			mapROIByNum.put(r.num, r);
			
			if (pr != null && pr.getA().y < r.getA().y) {
				L = new ArrayList<ROI>();
				lines.add(L);
				
				if (r.getA().y > pr.getC().y + 1) {
					p = new StimPar(paragraphs.size());
					paragraphs.add(p);
					pL = p.tLines.get(0);
				}
				else {
					pL = new TLine();
					p.tLines.add(pL);
				}
			}
			L.add(r);
			p.rois.add(r);
			pL.addROI(r);
			
			pr = r;
		}
		
		rs.close();
		
		// Set paragraphs done:
		/*
		Iterator<StimPar> it = paragraphs.iterator();
		while (it.hasNext()) {
			p = it.next();
			if (p.rois.size() > 0) p.done();
			else it.remove();
		}
		*/
		
		if (lines.size() == 1 && lines.get(0).size() == 0) return;
		
		for (int i = 0; i < paragraphs.size(); i++) {
			paragraphs.get(i).done();
		}
		
		// Find the bounding box:
		box.t = lines.get(0).get(0).getA().y;
		box.b = lines.get(lines.size()-1).get(0).getC().y;
		
		for (List<ROI> L2: lines) {
			int x1 = L2.get(0).getA().x;
			int x2 = L2.get(L2.size()-1).getC().x;
			if (x1 < box.l) box.l = x1;
			if (x2 > box.r) box.r = x2;
		}
	}
}
