package app.gaze.dstruct;


/**
 * @author Tomek D. Loboda
 */
public class Sess {
	public final int id;
	public final String name;
	public final String type;
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * C'tor.
	 */
	public Sess(int id, String name, String type) {
		this.id =  id;
		this.name =  name;
		this.type = type;
	}
}
