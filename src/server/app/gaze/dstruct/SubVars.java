package app.gaze.dstruct;


public class SubVars {
	public int fc = 0;
	public int fpfc = 0;
	public int otfc = 0;  // off-text fixations
	public float afd = 0;
	public float ffd = 0;
	public float sfd = 0;
	public float gd = 0;
	public float tt = 0;
	public float rp = 0;
	public int wfc = 0;  // words fixated
	public int regrCnt = 0;
	public int evtCnt = 0;
}
