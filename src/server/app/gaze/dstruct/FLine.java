package app.gaze.dstruct;

import inter.DBI;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import util.$;


public class FLine {
	//public static int lnId = 0;
	//private static PreparedStatement psFix = null;
	
	private final DBI dbiX;
	private final String db;
	
	//public final int id = lnId++;
	public final List<Fix> fix = new ArrayList<Fix>();
	
	public StimPar paragraph = null;
	public double paragraphDist = Double.MAX_VALUE;
	
	private final int stimSeqId;
	
	private final int minFixCnt;
	private final int maxYDist;
	private final int maxNoFixCnt;
	
	private Fix fixPrev = null;       // last fixation the line has been requested to include (but not necessarily included)
	private Fix fixLastAdded = null;  // last fixation actually included
	private Fix fixLeftMost = null;
	
	private boolean maturedInThisStep = false;
	private boolean mature = false;
	private boolean done = false;
	
	public boolean longLine = false;
	
	private int noFixCnt = 0;
	
	public double a;    // slope
	public double b;    // intercept
	public double r2;   // R squared (measure of goodness of fit)
	public double sd;   // std dev
	public double SEa;  // std err for slope
	public double SEb;  // std err for intercept
	
	public float x1 = 0;
	public float x2 = 0;
	public double y = 0;
	public float len = 0;
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * C'tor.
	 */
	public FLine(DBI dbiX, String db, int stimSeqId, int minFixCnt, int maxYDist, int maxNoFixCnt)  throws SQLException {
		//System.out.println("Line " + id + ": spawned");
		
		this.dbiX        = dbiX;
		this.db          = db;
		this.stimSeqId   = stimSeqId;
		this.minFixCnt   = minFixCnt;
		this.maxYDist    = maxYDist;
		this.maxNoFixCnt = maxNoFixCnt;
		
		//if (psFix == null || psFix.isClosed()) psFix = dbiX.getConn().get().prepareStatement("UPDATE " + db + ".fix SET x_adj = ?, y_adj = ?, fix_ln_id = ? WHERE id = ?");
	}
	
	
	// ---------------------------------------------------------------------------------
	public boolean addFix(Fix f) {
		if (done) return false;
		
		boolean added = false;
		if (fixPrev != null) {
			double dx = f.x - (fixLastAdded == null ? fixPrev.x : fixLastAdded.x);
			double dy = f.y - (fixLastAdded == null ? fixPrev.y : fixLastAdded.y);
			
			if (fix.size() > 0 && noFixCnt == 0 && (-dx >= 0.5*fixLastAdded.x)) {  // return saccade
				//System.out.println("  Line " + id + ": ret sacc");
				setDone();
			}
			
			if (!done && Math.abs(dy) <= maxYDist) {  // within the max y distance
				//System.out.println("  Line " + id + ": added " + (fixLastAdded == null ? "2 fix" : "1 fix"));
				if (fixLastAdded == null) {
					fix.add(fixPrev);
					fixLeftMost = fixPrev;
				}
				fix.add(f);
				fixLastAdded = f;
				
				if (f.x < fixLeftMost.x) fixLeftMost = f; 
				
				noFixCnt = 0;
				added = true;
			}
			else if (++noFixCnt > maxNoFixCnt) {
				//System.out.println("  Line " + id + ": marked done (noFixCnt)");
				setDone();
			}
		}
		fixPrev = f;
		
		maturedInThisStep = (fix.size() == minFixCnt && noFixCnt == 0);
		mature = (fix.size() >= minFixCnt);
		
		return added;
	}
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * Performs the adjustment:
	 * - aligns fixations along the line
	 * - rotates the line around it's lest-most fixation driveing the slope to zero
	 */
	private void adj() {
		double angle = -Math.atan(a);  // angle of rotation in radians
		double cos = Math.cos(angle);
		double sin = Math.sin(angle);
		y = a * fixLeftMost.x + b;
		
		x1 = fixLeftMost.x;
		for (Fix f: fix) {
			double yHat = a * f.x + b;
			f.xAdj = (float) (cos * (f.x - fixLeftMost.x) + sin * (yHat - y) + fixLeftMost.x);
			f.yAdj = (float) y;
			
			if (f.xAdj > x2) x2 = f.xAdj;
		}
		len = x2-x1;
	}
	
	
	// ---------------------------------------------------------------------------------
	// http://mathworld.wolfram.com/LeastSquaresFitting.html (NOTE: a and b are switched in this class as compared to the article)
	private void fit() {
		int N = fix.size();
		
		double mx = 0;  // m - mean
		double my = 0;
		double SSxx = 0;  // S - sum
		double SSyy = 0;
		double SSxy = 0;
		
		for (Fix f: fix) {
			mx += f.x;
			my += f.y;
		}
		mx /= N;
		my /= N;
		
		for (Fix f: fix) {
			double dx = f.x - mx;
			double dy = f.y - my;
			
			SSxx += dx * dx;
			SSyy += dy * dy;
			SSxy += dx * dy;
		}
		
		a  = SSxy / SSxx;
		b  = my - a*mx;
		r2 = (SSxy*SSxy) / (SSxx*SSyy);
		
		sd = Math.sqrt((SSyy - a*SSxy) / (N-2));
		
		SEa = sd * Math.sqrt(1/N + (mx*mx) / SSxx);
		SEb = sd / Math.sqrt(SSxx);
	}
	
	
	// ---------------------------------------------------------------------------------
	public String getFixIds() {
		String s = "";
		for (Fix f: fix) s += f.id + ",";
		return s;
	}
	
	
	// ---------------------------------------------------------------------------------
	public String getEqn() {
		return "y = " + $.trunc(a,2) + " x + " + $.trunc(b,2) + " (R2=" + $.trunc(r2,2) + "; point-cnt=" + fix.size() + ")";
	}
	
	
	// ---------------------------------------------------------------------------------
	public double getSlope()   { return a; }
	public double getInt()     { return b; }
	public double getR2()      { return r2; }
	public double getSD()      { return sd; }
	public double getSESlope() { return SEa; }
	public double getSEInt()   { return SEb; }
	
	
	// ---------------------------------------------------------------------------------
	public boolean hasMaturedInThisStep() { return maturedInThisStep; }
	
	
	// ---------------------------------------------------------------------------------
	public boolean isDone()   { return done; }
	public boolean isMature() { return mature; }
	
	
	// ---------------------------------------------------------------------------------
	public void moveBy(float x, float y) {
		for (Fix f: fix) {
			f.xAdj += x;
			f.yAdj += y;
		}
	}
	
	
	// ---------------------------------------------------------------------------------
	public void noFix() {
		maturedInThisStep = false;
		if (noFixCnt >= maxNoFixCnt) done = true;
	}
	
	
	// ---------------------------------------------------------------------------------
	public void persist()  throws SQLException {
		// Line:
		Statement stX = dbiX.newSt();
		stX.executeUpdate("INSERT INTO " + db + ".fix_ln (stim_seq_id, a, b, r2, sd, se_a, se_b, fix_cnt) VALUES (" + stimSeqId + ", " + a + ", " + b + ", " + r2 + ", " + sd + ", " + SEa + ", " + SEb + ", " + fix.size() + ")", Statement.RETURN_GENERATED_KEYS);
		ResultSet rs = stX.getGeneratedKeys();
		rs.next();
		int lnId = rs.getInt(1);
		rs.close();
		
		// Fixations:
		for (Fix f: fix) {
			stX.executeUpdate("UPDATE " + db + ".fix SET x_adj = " + f.xAdj + ", y_adj = " + f.yAdj + ", fix_ln_id = " + lnId + " WHERE id = " + f.id);
			/*
			psFix.setFloat(1, f.xAdj);
			psFix.setFloat(2, f.yAdj);
			psFix.setInt(3, lnId);
			psFix.setInt(4, f.id);
			psFix.executeUpdate();
			*/
		}
		
		stX.close();
	}
	
	
	// ---------------------------------------------------------------------------------
	public void removeFix(List<Fix> F) {
		fix.removeAll(F);
	}
	
	
	// ---------------------------------------------------------------------------------
	public void setYAdj(double y) {
		for (Fix f: fix) f.yAdj = (float) y;
		this.y = y;
	}
	
	
	// ---------------------------------------------------------------------------------
	public void setDone() {
		if (done) return;
		done = true;
		
		if (mature) { 
			fit();
			adj();
		}
	}
}
