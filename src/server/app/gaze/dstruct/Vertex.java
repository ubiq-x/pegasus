package app.gaze.dstruct;


/**
 * @author Tomek D. Loboda
 */
public class Vertex {
	public final int x;
	public final int y;
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * C'tor.
	 */
	public Vertex(int x, int y) {
		this.x =  x;
		this.y =  y;
	}
}
