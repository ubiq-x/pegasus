package app.gaze;

import inter.DBI;
import inter.JSI;

import java.sql.ResultSet;
import java.sql.SQLException;

import main.Do;


/**
 * @author Tomek D. Loboda
 */
public class Evt {

	
	// -----------------------------------------------------------------------------------------------------------------
	public Evt()  throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static StringBuffer get(DBI dbi, String username, int stimSeqId)  throws SQLException {
		String db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		
		// TODO: don't select `name` below but select the ID of the evt_lst record and reconstruct everything on the 
		//       client to reduce the size of the localy stored data 
		ResultSet rs = dbi.execQry("SELECT e.id, el.name, e.t_prompt, e.t_resp, e.memo, e.exc, e.exc_info FROM " + db + ".evt e INNER JOIN " + db + ".evt_lst el ON e.evt_lst_id = el.id WHERE stim_seq_id = " + stimSeqId + " ORDER BY t_prompt");
		
		StringBuffer res = new StringBuffer("[");
		while (rs.next()) {
			res.append(
				"{" +
				"id:" + rs.getInt("id") + "," +
				"name:" + JSI.str2js(rs.getString("name")) + "," +
				"tPrompt:" + rs.getInt("t_prompt") + "," +
				"tResp:" + rs.getInt("t_resp") + "," +
				"memo:" + JSI.str2js(rs.getString("memo")) + "," +
				"exc:" + (rs.getBoolean("exc") ? "true" : "false") + "," +
				"excInfo:" + JSI.str2js(rs.getString("exc_info")) +
				"}" +
				(!rs.isLast() ? "," : "")
			);
		}
		res.append("]");
		rs.close();
		
		return res;
	}
}
