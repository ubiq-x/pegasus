package app.gaze.adj;


/**
 * A rectangle but in the screen coordinates (i.e., left-top conrner is the origin and axis go 
 * to the right and down).
 */
public class Box {
	public int t = Integer.MAX_VALUE;
	public int b = Integer.MIN_VALUE;
	public int l = Integer.MAX_VALUE;
	public int r = Integer.MIN_VALUE;
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * C'tor.
	 */
	public Box() {}
	
	
	// ---------------------------------------------------------------------------------
	public int getH() { return b-t; }
	public int getW() { return r-l; }
}
