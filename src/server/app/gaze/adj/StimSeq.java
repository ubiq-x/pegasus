package app.gaze.adj;

import inter.DBI;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import util.Stats;
import app.gaze.Exp;
import app.gaze.dstruct.FLine;
import app.gaze.dstruct.Fix;
import app.gaze.dstruct.ROI;
import app.gaze.dstruct.Stim;
import app.gaze.dstruct.StimPar;


public class StimSeq {
	private final DBI dbiX;
	private final String db;
	private final int id;
	private final Stim stim;
	
	private final int minDur;
	private final int minLnFixCnt;
	private final int maxYDist;
	private final int maxLnNoFix;
	
	private int globalTx = 0;
	private int globalTy = 0;
	private float globalSx = 1.0f;
	private float globalSy = 1.0f;
	
	private final List<Fix> fix = new ArrayList<Fix>();
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public StimSeq(DBI dbiX, String db, int id, Stim stim, int minDur, int minFixPerLn, int maxYDist, int maxLnNoFixCnt)  throws SQLException {
		this.dbiX = dbiX;
		this.db   = db;
		this.id   = id;
		this.stim = stim;
		
		this.minDur      = minDur;
		this.minLnFixCnt = minFixPerLn;
		this.maxYDist    = maxYDist;
		this.maxLnNoFix  = maxLnNoFixCnt;
		
		// Load fixations:
		ResultSet rs = dbiX.execQry("SELECT id, t, dur, x, y, x_adj, y_adj, num, sacc_i_roi, sacc_i_let, sacc_o_roi, sacc_o_let, pupil, COALESCE(let, -1) AS let FROM " + db + ".fix WHERE stim_seq_id = " + id + " ORDER BY num");
		while (rs.next()) {
			Float x          = rs.getFloat("x");         if (rs.wasNull())  x        = null;
			Float y          = rs.getFloat("y");         if (rs.wasNull())  y        = null;
			Float xAdj       = rs.getFloat("x_adj");     if (rs.wasNull())  xAdj     = null;
			Float yAdj       = rs.getFloat("y_adj");     if (rs.wasNull())  yAdj     = null;
			Integer num      = rs.getInt("num");         if (rs.wasNull())  num      = null;
			Integer saccIROI = rs.getInt("sacc_i_roi");  if (rs.wasNull())  saccIROI = null;
			Integer saccILet = rs.getInt("sacc_i_let");  if (rs.wasNull())  saccILet = null;
			Integer saccOROI = rs.getInt("sacc_o_roi");  if (rs.wasNull())  saccOROI = null;
			Integer saccOLet = rs.getInt("sacc_o_let");  if (rs.wasNull())  saccOLet = null;
			Float pupil      = rs.getFloat("pupil");     if (rs.wasNull())  pupil    = null;
			Integer let      = rs.getInt("let");         if (rs.wasNull())  let      = null;
			
			fix.add(new Fix(rs.getInt("id"), -1l, rs.getLong("t"), rs.getInt("dur"), x, y, xAdj, yAdj, num, saccIROI, saccILet, saccOROI, saccOLet, pupil, Exp.DIR_NONE, Exp.DIR_NONE, false, false, let));
		}
		rs.close();
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/*
	 * Adjusts the fixations in an attempt to create a better match between the and the text of the stimulus.
	 */
	public int adj()  throws SQLException {
		// (1) Prepare:
		for (StimPar p: stim.paragraphs) p.fLines.clear();
		
		// (2) Calculate:
		List<FLine> L = inferFixLines();
		syncFixROI(L, stim.lines);
		
		// (3) Persist:
		Statement stX = dbiX.newSt();
		
		// (3.a) Remove the current stuff:
		stX.executeUpdate("UPDATE " + db + ".fix SET x_adj = null, y_adj = null, sacc_i_roi_adj = null, sacc_o_roi_adj = null, roi_id_adj = null, fix_ln_id = null WHERE stim_seq_id = " + id);
		stX.executeUpdate("DELETE FROM " + db + ".fix_ln WHERE stim_seq_id = " + id);
		
		// (3.b) Write the new stuff:
		Iterator<FLine> it = L.iterator();
		while (it.hasNext()) {
			FLine l = it.next();
			l.persist();
		}
		
		stX.executeUpdate("UPDATE " + db + ".fix SET x_adj = (x + " + globalTx + "), y_adj = ((y + " + globalTy + ") * " + globalSy + "), roi_id_adj = null WHERE stim_seq_id = " + id + " AND fix_ln_id IS NULL");
		stX.close();
		
		return L.size();
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/*
	 * Divides the fixation sequence into lines.  The algorithm steps are as follows:
	 * 
	 * 1. Keep spawning lines and try assigning the new (i.e., current) fixation to all of existing lines.
	 * 2. When a line matures remove fixations from that line from all immature lines.
	 * 3. Stop trying to add the new (i.e., current) fixation once it has been added to a mature line. 
	 * 4. Terminate degenerated (FC = 0) lines.
	 * 5. Keep the no-fix-added counter; commit matured and terminate immature lines if the counter reaches a threshold.
	 * 6. At the end commit all mature lines.
	 * 7. Commit matured and terminate immature lines at a return saccade.
	 */
	private List<FLine> inferFixLines()  throws SQLException {
		ArrayList<FLine> L = new ArrayList<FLine>();  // lines
		
		for (int i = 0; i < fix.size(); i++) {
			Fix f = fix.get(i);
			//System.out.println("Fix " + i + ": x=" + f.x + ", y=" + f.y);
			
			if (f.os != Exp.DIR_NONE || f.dur < minDur) continue;
			
			// Spawn new line and try to add the new fixation to all existing lines; remove all 
			// fixations from lines that matured in this iteration from all immature lines:
			L.add(new FLine(dbiX, db, id, minLnFixCnt, maxYDist, maxLnNoFix));
			for (FLine l01: L) {
				boolean added = l01.addFix(f);
				if (l01.hasMaturedInThisStep()) {
					for (FLine l02: L) {
						if (!l02.isMature()) {
							//System.out.print("  Line " + l02.id + ": removing fix from Line " + l01.id + " (size: " + l02.fix.size() + " --> ");
							l02.removeFix(l01.fix);
							//System.out.println(l02.fix.size() + ")");
							if (l02.fix.size() == 0) l02.setDone();
						}
					}
				}
				if (l01.isMature() && added) break;
			}
			
			// Terminate degenerate and immature-done lines:
			Iterator<FLine> it = L.iterator();
			while (it.hasNext()) {
				FLine l = it.next();
				if (l.isDone() && !l.isMature()) {
					//System.out.println("  Line " + l.id + ": removing as degerate or immature-done");
					it.remove();
				}
			}
		}
		
		// All fixations have been processed -- consolidate the lines list:
		Iterator<FLine> it = L.iterator();
		while (it.hasNext()) {
			FLine l = it.next();
			if (l.isMature()) l.setDone(); 
			else it.remove();
		}
		
		// Debug:
		/*
		System.out.println("\nln	FC	a	b	r2	sd	SEa	SEb	eqn	id	x	y");
		for (int i = 0; i < L.size(); i++) {
			Line l = L.get(i);
			System.out.print(i + "\t" + l.fix.size() + "\t" + Util.trunc(l.getSlope(), 2) + "\t" + Util.trunc(l.getInt(), 2) + "\t" + Util.trunc(l.getR2(), 2) + "\t" + Util.trunc(l.getSD(), 2) + "\t" + Util.trunc(l.getSESlope(), 2) + "\t" + Util.trunc(l.getSEInt(), 2) + "\t" + l.getEqn());
			for (int j = 0; j < l.fix.size(); j++) {
				Fix f = l.fix.get(j);
				if (j > 0) System.out.print("\t\t\t\t\t\t\t\t");
				System.out.println("\t" + f.id + "\t" + f.x + "\t" + f.y);
			}
		}
		
		System.out.println("\nln	FC	a	b	r2	sd	SEa	SEb	eqn");
		for (int i = 0; i < L.size(); i++) {
			Line l = L.get(i);
			System.out.println(i + "\t" + l.fix.size() + "\t" + Util.trunc(l.getSlope(), 2) + "\t" + Util.trunc(l.getInt(), 2) + "\t" + Util.trunc(l.getR2(), 2) + "\t" + Util.trunc(l.getSD(), 2) + "\t" + Util.trunc(l.getSESlope(), 2) + "\t" + Util.trunc(l.getSEInt(), 2) + "\t" + l.getEqn());
		}
		System.out.println("\n");
		*/
		
		return L;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/*
	 * Synchronizes fixations and ROIs.
	 */
	private void syncFixROI(List<FLine> fLines, List<List<ROI>> tLines) {
		// Return if not enough lines (both f- and t-):
		int stimLnCnt = stim.lines.size();
		if (stimLnCnt <= 7 || (stimLnCnt > 7 && fLines.size() <= stimLnCnt - 6)) return;  // TODO: param x 2
		
		// (1) Line ounding boxes:
		// (1.1) Find:
		Box box =  new Box();
		Stats left = new Stats();
		for (FLine l: fLines) {
			if (l.b < box.t) box.t = (int) l.b;
			if (l.b > box.b) box.b = (int) l.b;
			if (l.len >= 0.80 * stim.box.getW()) left.addVal(l.x1);  // TODO: param
			if (l.x2 > box.r) box.r = (int) l.x2;
		}
		if (left.size() == 0) return;  // impossible to determine the left boundary
		left.calc();
		box.l = left.getMin(-1).intValue();
		
		// (1.2) Return if the bounding box is too small as compared to the bounding box of the stimulus:
		if (box.getH() < 0.70 * stim.box.getH()) return;  // TODO: param
		
		// (2) Correct the global offset and scale:
		// (2.1) Determine the magnitude:
		globalTx = stim.box.l - box.l + stim.lnH;
		globalTy = stim.box.t - Math.max(box.t, 0) + stim.lnH / 2;
		globalSy = (float) stim.getBoxH(4) / (float) (box.getH() + stim.lnH);  // TODO: param
		
		// (2.2) Perform:
		for (FLine l: fLines) {
			for (Fix f: l.fix) {
				f.xAdj = f.xAdj + globalTx;
				f.yAdj = (f.yAdj + globalTy) * globalSy;
			}
			
			l.x1 = l.x1 + globalTx;
			l.x2 = l.x2 + globalTx;
			l.y = (l.y + globalTy) * globalSy;
		}
		
		// (3) Associate lines with paragraphs:
		// (3.1) Associated based on y-coordinate and paragraphs' bounding boxes:
		for (FLine l: fLines) {
			boolean associated = false;
			int i = 0;
			while (!associated && i < stim.paragraphs.size()) {
				StimPar p = stim.paragraphs.get(i);
				if (l.y >= p.box.t && l.y <= p.box.b) {
					l.paragraph = p;
					l.paragraphDist = 0;
					associated = true;
				}
				else if (Math.min(Math.abs(l.y - p.box.t), Math.abs(l.y - p.box.b)) < l.paragraphDist) {
					l.paragraph = p;
					l.paragraphDist = Math.min(Math.abs(l.y - p.box.t), Math.abs(l.y - p.box.b));
				}
				i++;
			}
			l.paragraph.fLines.add(l);
		}
		
		/*
		if (stim.name.equals("C3P02")) {
			System.out.println(stim.name);
		}
		*/
		
		// (3.2) Check if the last line of a paragraph hasn't been mistakingly associated with the next paragraph: 
		for (int i = 0; i < stim.paragraphs.size() - 1; i++) {
			StimPar p1 = stim.paragraphs.get(i);    // curr
			StimPar p2 = stim.paragraphs.get(i+1);  // next
			
			int p1tNLen = p1.tLines.get(p1.tLines.size()-1).len;
			int p2t0Len = p2.tLines.get(0).len;
			
			FLine p1fN = p1.getBottomMostFLn();
			FLine p2f0 = p2.getTopMostFLn();
			
			/*
			if (stim.name.equals("C3P02")) {
				System.out.println("  p1tNLen=" + p1tNLen + ", p2t0Len=" + p2t0Len + ", p1fN.len=" + p1fN.len + ", p2f0.len=" + p2f0.len);
			}
			*/
			
			if (p1fN != null && p2f0 != null) {
				int lineDiff = Math.abs(p1tNLen - p2t0Len);
				double p1Diff = Math.abs(p1fN.len - p1tNLen);  // the difference between the length of the last f- and t- lines for the current paragraph
				double p2Diff = Math.abs(p2f0.len - p2t0Len);  // the difference between the length of the first f- and t- lines for the next paragraph
				//double linesDist = Math.abs(p1fN.y - p2f0.y);
				
				if (lineDiff >= 200 && p1Diff >= 200 && p2Diff >= 200 && Math.abs(p2f0.len - p1tNLen) <= 200) {  // TODO: param (x 1-4)
					p2.fLines.remove(p2f0);
					p1.fLines.add(p2f0);
					p2f0.paragraph = p1;
				}
			}
		}
		
		/*
		if (stim.name.equals("C3P02")) {
			for (StimPar p: stim.paragraphs) {
				System.out.print("  P" + p.idx + ": [" + p.fLines.size() + "] ");
				for (Line l: p.fLines) {
					System.out.print(Util.trunc(l.y, 2) + ", ");
				}
				System.out.print("\n");
			}
			System.out.println();
		}
		*/
		
		// (3.3) Mark f-lines as long:
		for (StimPar p: stim.paragraphs) {
			for (FLine l: p.fLines) {
				if (l.len >= 0.75 * p.box.getW()) {  // TODO: param
					l.longLine = true;
					p.longLineCnt++;
				}
			}
		}
		
		// (3.4) Associate f-lines with t-lines:
		/*
		for (StimPar p: stim.paragraphs) {
			if (p.longLineCnt == p.tLines.size()) {
				Stats lenDiff = new Stats();
				Stats centerOffset = new Stats();
				for (int i = 0; i < p.tLines.size() - 1; i++) {
					lenDiff.addVal(Math.abs(p.tLines.get(i).len - p.fLines.get(i).len));
					centerOffset.addVal(Math.abs(p.tLines.get(i).cy - p.fLines.get(i).y));
				}
				lenDiff.calc();
				centerOffset.calc();
				if (lenDiff.stdev <= 100 && centerOffset.stdev < (double) stim.lnH / 2) {  // TODO: param x 2
					for (int i = 0; i < p.fLines.size() - 1; i++) {
						FLine fl = p.fLines.get(i);
						TLine tl = p.tLines.get(i);
						fl.setYAdj(tl.cy);
					}
				}
			}
		}
		*/
		
		// (3.x) Correct paragraphs' offset and scale:
		/*
		for (StimPar p: stim.paragraphs) {
			if (p.tLines.size() < 2 || p.fLines.size() < 2) continue;
			
			Line ln0 = p.getTopMostFLn();
			Line lnN = p.getBottomMostFLn();
			
			//if (ln0 == null || lnN == null) continue;
			//if (lnN.y - ln0.y < stim.lnH) continue;
			
			//int tx = 0;  // p.box.l - box.l + stim.lnH;
			int ty = p.box.t - (int) ln0.y + stim.lnH / 2;
			float sy = (float) p.box.getH() / (float) (lnN.y - ln0.y + stim.lnH);
			
			// (2.2) Perform:
			for (Line l: p.fLines) {
				for (Fix f: l.fix) {
					//f.xAdj = f.xAdj + tx;
					f.yAdj = ((f.yAdj + ty - p.box.t) * sy) + p.box.t;
				}
				
				//l.x1 = l.x1 + tx;
				//l.x2 = l.x2 + tx;
				l.y = ((l.y + ty - p.box.t) * sy) + p.box.t;
			}
		}
		*/
	}
}
