package app.gaze;

import inter.DBI;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import main.Do;
import main.Result;


/**
 * @author Tomek D. Loboda
 */
public class DB {
	private static final String TYPE_OBJ_ATTR_SEP = ":";
	
	private static final Map<String, List<String>> mapObjAttr;
	private static final Map<String, String> mapImgType;
	
	static {
		mapObjAttr = new HashMap<String, List<String>>();
		mapObjAttr.put("ds", Arrays.asList("name", "memo"));
		mapObjAttr.put("exp", Arrays.asList("stim_zoom"));
		mapObjAttr.put("stim", Arrays.asList("img", "img_roi"));
		
		mapImgType = new HashMap<String, String>();
		mapImgType.put("stim" + TYPE_OBJ_ATTR_SEP + "img", "jpeg");
		mapImgType.put("stim" + TYPE_OBJ_ATTR_SEP + "img_roi", "png");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/*
	 * [B].
	 */
	public static long getSize(DBI dbi, String username)  throws SQLException {
		return DBI.getLong(
			dbi,
			"SELECT " +
			"pg_total_relation_size('" + Do.DB_GAZE + "-tomek.blink') + " +
			"pg_total_relation_size('" + Do.DB_GAZE + "-tomek.ds') + " +
			"pg_total_relation_size('" + Do.DB_GAZE + "-tomek.evt') + " +
			"pg_total_relation_size('" + Do.DB_GAZE + "-tomek.exp') + " +
			"pg_total_relation_size('" + Do.DB_GAZE + "-tomek.fix') + " +
			"pg_total_relation_size('" + Do.DB_GAZE + "-tomek.fix_ln') + " +
			"pg_total_relation_size('" + Do.DB_GAZE + "-tomek.form') + " +
			"pg_total_relation_size('" + Do.DB_GAZE + "-tomek.log_cmd') + " +
			"pg_total_relation_size('" + Do.DB_GAZE + "-tomek.log_xform') + " +
			"pg_total_relation_size('" + Do.DB_GAZE + "-tomek.roi') + " +
			"pg_total_relation_size('" + Do.DB_GAZE + "-tomek.roi_type') + " +
			"pg_total_relation_size('" + Do.DB_GAZE + "-tomek.stim') + " +
			"pg_total_relation_size('" + Do.DB_GAZE + "-tomek.stim_seq') + " +
			"pg_total_relation_size('" + Do.DB_GAZE + "-tomek.sub') + " +
			"pg_total_relation_size('" + Do.DB_GAZE + "-tomek.trial') + " +
			"pg_total_relation_size('" + Do.DB_GAZE + "-tomek.var') + " +
			"pg_total_relation_size('" + Do.DB_GAZE + "-tomek.vertex') + " +
			"pg_total_relation_size('" + Do.DB_GAZE + "-tomek.xr_var_roi') + " +
			"pg_total_relation_size('" + Do.DB_GAZE + "-tomek.xr_var_stim') + " +
			"pg_total_relation_size('" + Do.DB_GAZE + "-tomek.xr_var_sub') + " +
			"pg_total_relation_size('" + Do.DB_GAZE + "-tomek.xr_var_trial') " +
			"AS size",
			"size"
		);
		
		// MySQL: return dbi.getLong(null, "SELECT SUM(data_length + index_length) AS size FROM information_schema.TABLES WHERE table_schema IN ('" + Do.DB_PEGASUS_GAZE + username + "')", "size");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static Result attrSet(DBI dbi, String username, String obj, String attr, String val, int id)  throws SQLException {
		if (!isObjAttrValid(dbi, obj, attr)) return new Result(false, "msg:\"Incorrect object-attribute pair specified.\"");
		dbi.execUpd("UPDATE \"" + Do.DB_GAZE + "-" + username + "\".\"" + obj + "\" SET \"" + attr + "\" = " + val + " WHERE id = " + id);
		return new Result(true, "");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static void getImg(HttpServletResponse res, DBI dbi, String username, String obj, int objId, String attr)  throws SQLException, IOException {
		String db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		if (!isObjAttrValid(dbi, obj, attr) || DBI.getLong(dbi, "SELECT COALESCE(LENGTH(\"" + attr + "\"), 0) AS len FROM " + db + ".\"" + obj + "\" WHERE id = " + objId, "len") == 0) return;
		dbi.getImg(res, db, obj, objId, attr, mapImgType.get(obj + TYPE_OBJ_ATTR_SEP + attr));
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static boolean isObjAttrValid(DBI dbi, String obj, String attr) { return (mapObjAttr.containsKey(obj) && mapObjAttr.get(obj).contains(attr)); }
}
