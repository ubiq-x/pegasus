package app.gaze.oper;

import inter.DBI;
import inter.FSI;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import main.Do;


/**
 * ...
 * 
 * @author Tomek D. Loboda
 */
public class Export {
	private final String dbG;
	private final int expId;
	private final String expName;
	private final DBI dbi;
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * C'tor.
	 */
	public Export(String username, int expId)  throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		this.dbG = "\"" + Do.DB_GAZE + "-" + username + "\"";
		dbi = new DBI();
		
		this.expId = expId;
		expName = DBI.getStr(dbi, "SELECT name FROM " + dbG + ".exp WHERE id = " + expId, "name");
	}
	
	
	// ---------------------------------------------------------------------------------
	public File getDir()  throws IOException, SQLException {
		File dirRoot = FSI.getTmpWorkDir("export-", "");
		
		// (1) Variables (non-system only, obviously):
		File dVar = FSI.mkdir(dirRoot, "var");
		FSI.mkdir(dVar, "roi");
		FSI.mkdir(dVar, "stim");
		FSI.mkdir(dVar, "sub");
		FSI.mkdir(dVar, "trial");
		
		Statement stVar = dbi.getConn().get().createStatement();
		ResultSet rsVar = stVar.executeQuery("SELECT obj_name, name, type, memo FROM " + dbG + ".var WHERE exp_id = " + expId + " AND sys = 0");
		
		while (rsVar.next()) {
			File dTmp = FSI.mkdir(dVar.getAbsolutePath() + File.separatorChar + rsVar.getString("obj_name"), rsVar.getString("name"));
			FSI.addFile(dTmp.getAbsolutePath() + File.separatorChar + "memo", rsVar.getString("memo"));
			FSI.addFile(dTmp.getAbsolutePath() + File.separatorChar + "type", rsVar.getString("type"));
		}
		rsVar.close();
		stVar.close();
		
		// (2) Stimuli:
		File dirStim = FSI.mkdir(dirRoot, "stim");
		ResultSet rsStim = dbi.execQry("SELECT * FROM " + dbG + ".stim WHERE exp_id = " + expId);
		
		while (rsStim.next()) {
			String name = rsStim.getString("name");
			File d = new File(dirStim, name);
			d.mkdir();
			String pathStim = d.getAbsolutePath() + File.separatorChar;
			
			// (2.a) The INF file:
			FSI.addFile(pathStim + "memo", rsStim.getString("memo"));
			FSI.addFile(pathStim + name + ".txt", rsStim.getString("txt"));
			FSI.addFile(
				pathStim + "inf",
				"res-h:" + rsStim.getString("res_h") + "\n" +
				"res-v:" + rsStim.getString("res_v") + "\n" +
				"font-fam:'" + rsStim.getString("font_fam") + "'\n" +
				"font-size:" + rsStim.getString("font_size") + "\n" +
				"letter-spac:" + rsStim.getString("letter_spac") + "\n" +
				"line-h:" + rsStim.getString("line_h") + "\n" +
				"align-h:'" + rsStim.getString("align_h") + "'\n" +
				"align-v:'" + rsStim.getString("align_v") + "'\n" +
				"margin-t:" + rsStim.getString("margin_t") + "\n" +
				"margin-l:" + rsStim.getString("margin_l") + "\n" +
				"margin-r:" + rsStim.getString("margin_r") + "\n" +
				"padding-t:" + rsStim.getString("padding_t") + "\n" +
				"padding-b:" + rsStim.getString("padding_b") + "\n" +
				"roi-h:" + rsStim.getString("roi_h") + "\n" +
				"offset-x:" + rsStim.getString("offset_x") + "\n" +
				"offset-y:" + rsStim.getString("offset_y") + "\n"
			);
			
			// (2.b) The ROIs file:
			StringBuffer rois = new StringBuffer();
			Statement stROI = dbi.getConn().get().createStatement();
			ResultSet rsROI = stROI.executeQuery("SELECT * FROM " + dbG + ".roi WHERE stim_id = " + rsStim.getInt("id") + " ORDER BY id");
			while (rsROI.next()) {  // below I assume all ROIs are rectangular (just like anywhere else so far)
				rois.append(rsROI.getString("name") + "\t");				
				
				// - Vertices:
				Statement stVertex = dbi.getConn().get().createStatement();
				ResultSet rsVertex = stVertex.executeQuery("SELECT * FROM " + dbG + ".vertex WHERE roi_id = " + rsROI.getInt("id") + " ORDER BY id");
				
				rsVertex.next();
				rois.append(rsVertex.getString("x") + "," + rsVertex.getString("y") + ",");
				rsVertex.next();
				rsVertex.next();
				rois.append(rsVertex.getString("x") + "," + rsVertex.getString("y"));
				
				rsVertex.close();
				stVertex.close();
				
				// - Variables:
				Statement stROIVar = dbi.getConn().get().createStatement();
				ResultSet rsROIVar = stROIVar.executeQuery("SELECT v.name, v.type, x.val FROM " + dbG + ".xr_var_roi x INNER JOIN " + dbG + ".var v ON x.var_id = v.id WHERE x.roi_id = " + rsROI.getInt("id") + " ORDER BY v.id");
				rois.append("\t");
				while (rsROIVar.next()) {
					rois.append(rsROIVar.getString("name") + (rsROIVar.getString("type").equals("b") ? "" : "=" + rsROIVar.getString("val")) + (rsROIVar.isLast() ? "" : ","));
				}
				rsROIVar.close();
				stROIVar.close();
				
				rois.append("\n");
			}
			rsROI.close();
			stROI.close();
			FSI.addFile(pathStim + "rois", rois.toString());
		}
		rsStim.close();
		
		// (3) Subjects:
		File dirSub = FSI.mkdir(dirRoot, "sub");
		
		ResultSet rsSub = dbi.execQry("SELECT id, name, memo FROM " + dbG + ".sub s WHERE exp_id = " + expId + " ORDER BY id");
		while (rsSub.next()) {
			// (3.1) Info:
			File dirCurrSub = FSI.mkdir(dirSub, rsSub.getString("name"));
			FSI.addFile(dirCurrSub.getAbsolutePath() + File.separatorChar + "memo", rsSub.getString("memo"));
			
			// (3.2) Variables:
			Statement stSubVar = dbi.getConn().get().createStatement();
			ResultSet rsSubVar = stSubVar.executeQuery("SELECT v.name, x.val FROM " + dbG + ".xr_var_sub x INNER JOIN " + dbG + ".var v ON x.var_id = v.id WHERE x.sub_id = " + rsSub.getInt("id") + " ORDER BY v.id");
			StringBuffer vars = new StringBuffer();
			while (rsSubVar.next()) {
				vars.append(rsSubVar.getString("name") + ":" + rsSubVar.getString("val") + "\n");
			}
			rsSubVar.close();
			stSubVar.close();
			
			FSI.addFile(dirCurrSub.getAbsolutePath() + File.separatorChar + "vars", vars.toString());
		}
		rsSub.close();
		
		// (4) Trials:
		File dirTrial = FSI.mkdir(dirRoot, "trial");
		ResultSet rsTrial = dbi.execQry("SELECT DISTINCT t.id, t.name FROM " + dbG + ".trial t INNER JOIN " + dbG + ".sub s ON t.sub_id = s.id WHERE s.exp_id = " + expId + " ORDER BY t.id");
		while (rsTrial.next()) {
			String trialName = rsTrial.getString("name");
			File dirCurrTrial = FSI.mkdir(dirTrial, trialName);
			
			// Subjects:
			rsSub = dbi.execQry("SELECT id, name FROM " + dbG + ".sub s WHERE exp_id = " + expId + " ORDER BY id");
			while (rsSub.next()) {
				File dirCurrTrialSub = FSI.mkdir(dirCurrTrial, rsSub.getString("name"));
				
				// Stimuli sequence:
				int ssIdx = 1;
				StringBuilder sbXForm = new StringBuilder();
				ResultSet rsStimSeq = dbi.execQry("SELECT ss.id FROM " + dbG + ".stim_seq ss INNER JOIN " + dbG + ".trial t ON ss.trial_id = t.id WHERE t.id = " + rsTrial.getInt("id") + " AND t.sub_id = " + rsSub.getInt("id") + " ORDER BY t0");
				while (rsStimSeq.next()) {
					StringBuilder sbCurrXForm = new StringBuilder();
					ResultSet rsXForm = dbi.execQry("SELECT ts,tx,ty,sx,sy,a FROM " + dbG + ".log_xform WHERE stim_seq_id = " + rsStimSeq.getInt("id"));
					while (rsXForm.next()) {
						sbCurrXForm.append("tx:" + rsXForm.getInt("tx") + ",ty:" + rsXForm.getInt("ty") + ",sx:" + rsXForm.getFloat("sx") + ",sy:" + rsXForm.getFloat("sy") + ",a:" + rsXForm.getFloat("a") + (!rsXForm.isLast() ? "|" : ""));
					}
					rsXForm.close();
					sbXForm.append((ssIdx++) + " " + sbCurrXForm + "\n");
				}
				rsStimSeq.close();
				FSI.addFile(dirCurrTrialSub.getAbsolutePath() + File.separatorChar + "xform", sbXForm.toString());
			}
		}
		rsTrial.close();
		
		// (4) Finish up:
		return dirRoot;
	}
	
	
	// ---------------------------------------------------------------------------------
	public String getFilename() { return expName; }
}
