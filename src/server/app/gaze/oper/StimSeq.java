package app.gaze.oper;

import inter.DBI;
import inter.JSI;

import java.sql.ResultSet;
import java.sql.SQLException;

import app.gaze.Evt;

import main.Do;


/**
 * @author Tomek D. Loboda
 */
public class StimSeq {
	private final DBI dbi;
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public StimSeq(DBI dbi)  throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		this.dbi = dbi;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public StringBuffer get(String username, int trialId)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		String db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		
		ResultSet rs = dbi.execQry(
			"SELECT ss.id, ss.stim_id, ss.t0, ss.t1, ss.exc, ss.exc_info, COALESCE(ss.fix_cnt, -1) AS fix_cnt, COALESCE(ss.fix_cnt_roi, -1) AS fix_cnt_roi, COALESCE(ss.fix_cnt_roi_adj, -1) AS fix_cnt_roi_adj, fix_cnt_exc, COALESCE(ss.blink_cnt, -1) AS blink_cnt, s.name, COUNT(x.id) AS xform_cnt " +
			"FROM " + db + ".stim_seq ss " +
			"INNER JOIN " + db + ".stim s ON ss.stim_id = s.id " +
			"LEFT JOIN " + db + ".log_xform x ON x.stim_seq_id = ss.id " +
			"WHERE ss.trial_id = " + trialId + " " +
			"GROUP BY ss.id, ss.stim_id, ss.t0, ss.t1, ss.exc, ss.exc_info, fix_cnt, fix_cnt_roi, fix_cnt_roi_adj, fix_cnt_exc, blink_cnt, s.name " +
			"ORDER BY ss.t0"
		);
		
		StringBuffer res = new StringBuffer("[");
		while (rs.next()) {
			int id = rs.getInt("id");
			res.append(
				"{" +
				"id:" + id + "," +
				"stimId:" + rs.getInt("stim_id") + "," +
				"name:" + JSI.str2js(rs.getString("name")) + "," +
				"t0:" + rs.getInt("t0") + "," +
				"t1:" + rs.getInt("t1") + "," +
				"exc:" + (rs.getBoolean("exc") ? "true" : "false") + "," +
				"excInfo:" + JSI.str2js(rs.getString("exc_info")) + "," +
				"fixCnt:" + rs.getInt("fix_cnt") + "," +
				"fixCntROI:" + rs.getInt("fix_cnt_roi") +"," +
				"fixCntROIAdj:" + rs.getInt("fix_cnt_roi_adj") +"," +
				"fixCntExc:" + rs.getInt("fix_cnt_exc") + "," +
				"blinkCnt:" + rs.getInt("blink_cnt") + "," +
				"xformCnt:" + rs.getInt("xform_cnt") + "," +
				"evt:" + Evt.get(dbi, username, id) + "," + 
				"fix:null," +
				"blinks:null," +
				"xform:null" +
				"}" +
				(!rs.isLast() ? "," : "")
			);
		}
		res.append("]");
		rs.close();
		
		return res;
	}
}
