package time;


/**
 * @author Tomek D. Loboda
 */
public class Point {
	//public final List<Float> data = new ArrayList<Float>();
	
	public double min;
	public double max;
	public double sum;
	public double mean;
	public double median;
	public double stdev;
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * C'tor.
	 */
	public Point(double min, double max, double sum, double mean, double median, double stdev) {
		this.min = min;
		this.max = max;
		this.sum = sum;
		this.mean = mean;
		this.median = median;
		this.stdev = stdev;
	}
	
	
	// ---------------------------------------------------------------------------------
	/*
	public void calcStats() {
		int n = data.size();
		float sum = 0;    // sum of x
		float sumSq = 0;  // sum of squared x
		
		for (float d: data) {
			sum   += d;
			sumSq += d*d;
		}
		
		m = sum / n;
		sd = Math.sqrt((n*sumSq-(sum*sum)) / (n*(n-1)));
	}
	*/
}
