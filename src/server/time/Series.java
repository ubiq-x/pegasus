package time;

import java.util.Iterator;
import java.util.LinkedHashMap;


/**
 * @author Tomek D. Loboda
 */
public class Series {
	public final LinkedHashMap<Long,Point> times = new LinkedHashMap<Long,Point>();
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * C'tor.
	 */
	public Series() {}
	
	
	// ---------------------------------------------------------------------------------
	public void add(long t, double min, double max, double sum, double mean, double median, double stdev) {
		Long key = new Long(t);
		Point p = null;
		if (times.get(key) == null) {
			p = new Point(min, max, sum, mean, median, stdev);
			times.put(key, p);
		}
		else p = times.get(key);
	}
	
	
	// ---------------------------------------------------------------------------------
	public StringBuffer getJSGraph(String grp, String ser, String color, String point, String line, int width) {
		StringBuffer res = new StringBuffer();
		
		res.append(
			"{" +
			"  grp  : \"" + grp + "\"," +
			"  name  : \"" + ser + "\"," +
			"  plot  : {" +
			"    type    : graph.PLOT_TYPE_POINT," +
			"    line    : { type: " + line + ", width: " + width + ", color: \"" + color + "\" }," +
			"    point   : { type: " + point + ", radius: 5, side: 9, width: " + width + ", fgColor: \"" + color + "\", bgColor: \"#ffffff\" }," +
			"    tooltip : { coords: true, coordsBgColor: \"" + color + "\", coordsFgColor: \"#ffffff\" }" +
			"  }," +
			"  points : ["
		);
		
		Iterator<Long> it = times.keySet().iterator();
		while (it.hasNext()) {
			Long t = it.next();
			Point p = times.get(t);
			res.append("{x:" + t.longValue() + ",y:" + p.mean + ",sd:" + p.stdev + "},");
		}
		
		res.append(
			"  ]," +
			"}"
		);
		
		return res;
	}
}
