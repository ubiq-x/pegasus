package standalone;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class PopulateWordTbl {
	private static final int ID_NONE = -1;
	
	private static final String DB_NAME        = "pegasus";
	private static final String DB_USERNAME    = "postgres";
	private static final String DB_PASSWD      = "sa";
	private static final String DB_SCHEMA_NAME = "g";
	
	private static Connection conn = null;
	
	private static int wordDelCnt = 0, wordInsCnt = 0, wordUpdCnt = 0;
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static String dbEsc(String s) {
		if (s == null) return null;
		return "'" + s.replaceAll("'", "\\\\'") + "'";
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static long getLong(String qry, String col)  throws SQLException {
		long res = ID_NONE;
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery(qry);
		
		if (rs.next()) res = rs.getLong(col);
		
		rs.close();
		st.close();
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	private static long insGetId(String qrySel, String qryIns)  throws SQLException {
		long res = (qrySel == null ? ID_NONE : getLong(qrySel, "id"));
		
		if (res == ID_NONE) {
			Statement st = conn.createStatement();
			st.executeUpdate(qryIns, Statement.RETURN_GENERATED_KEYS);
			ResultSet rs = st.getGeneratedKeys();
			rs.next();
			res = rs.getLong(1);
			rs.close();
			wordInsCnt++;
		}
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * This method always adds a word; it never checks if that word already exists. That is because the same word can 
	 * be used as a different part of a sentence.
	 */
	private static void mrcProcLine(String l, PreparedStatement psUpd)  throws SQLException {
		l = l.trim();
		
		int nlet  = (new Integer(l.substring( 0, 2))).intValue();
		
		psUpd.setInt(1,  (new Integer(l.substring( 2, 4))).intValue());
		psUpd.setInt(2,  (new Integer(l.substring( 4, 5))).intValue());
		psUpd.setInt(3,  (new Integer(l.substring( 5,10))).intValue());
		psUpd.setInt(4,  (new Integer(l.substring(10,12))).intValue());
		psUpd.setInt(5,  (new Integer(l.substring(12,15))).intValue());
		psUpd.setInt(6,  (new Integer(l.substring(15,21))).intValue());
		psUpd.setInt(7,  (new Integer(l.substring(21,25))).intValue());
		psUpd.setInt(8,  (new Integer(l.substring(25,28))).intValue());
		psUpd.setInt(9,  (new Integer(l.substring(28,31))).intValue());
		psUpd.setInt(10, (new Integer(l.substring(31,34))).intValue());
		psUpd.setInt(11, (new Integer(l.substring(34,37))).intValue());
		psUpd.setInt(12, (new Integer(l.substring(37,40))).intValue());
		psUpd.setInt(13, (new Integer(l.substring(40,43))).intValue());
		
		psUpd.setString(14, l.substring(43,44));
		psUpd.setString(15, l.substring(44,45));
		psUpd.setString(16, l.substring(45,46));
		psUpd.setString(17, l.substring(46,47));
		psUpd.setString(18, l.substring(47,48));
		psUpd.setString(19, l.substring(48,49));
		psUpd.setString(20, l.substring(49,50));
		psUpd.setString(21, l.substring(50,51));
		
		int pipeIdx01 = l.indexOf('|', 51         );
		int pipeIdx02 = l.indexOf('|', pipeIdx01+1);
		int pipeIdx03 = l.indexOf('|', pipeIdx02+1);
		
		String word = dbEsc(l.substring(51,pipeIdx01).toLowerCase());
		
		psUpd.setString(22, l.substring(pipeIdx01+1,pipeIdx02));
		psUpd.setString(23, l.substring(pipeIdx02+1,pipeIdx03));
		psUpd.setString(24, l.substring(pipeIdx03+1)          );
		
		long id = insGetId(null, "INSERT INTO " + DB_SCHEMA_NAME + ".word (word, nlet) VALUES (" + word + "," + nlet + ")");
		psUpd.setLong(25, id);
		
		psUpd.executeUpdate();
		wordUpdCnt++;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * This method adds a word only if it doesn't exist. Otherwise, it updates the already existing one.
	 */
	private static void subtlProcLine(String l, PreparedStatement psUpd, PreparedStatement psIns)  throws SQLException {
		l = l.trim();
		
		String[] X = l.split("\t");
		String word = X[0].toLowerCase();
		
		if (getLong("SELECT COUNT(id) AS cnt FROM " + DB_SCHEMA_NAME + ".word WHERE word = " + dbEsc(word), "cnt") > 0) {
			psUpd.setInt   (1, (new Integer(X[1]).intValue())  );
			psUpd.setInt   (2, (new Integer(X[2]).intValue())  );
			psUpd.setInt   (3, (new Integer(X[3]).intValue())  );
			psUpd.setInt   (4, (new Integer(X[4]).intValue())  );
			psUpd.setFloat (5, (new Float  (X[5]).floatValue()));
			psUpd.setFloat (6, (new Float  (X[7]).floatValue()));
			psUpd.setString(7, dbEsc(word)                     );
			
			psUpd.executeUpdate();
			wordUpdCnt++;
		}
		else {
			psIns.setString(1, dbEsc(word)                     );
			psIns.setInt   (2, word.length()                   );
			psUpd.setInt   (3, (new Integer(X[1]).intValue())  );
			psUpd.setInt   (4, (new Integer(X[2]).intValue())  );
			psUpd.setInt   (5, (new Integer(X[3]).intValue())  );
			psUpd.setInt   (6, (new Integer(X[4]).intValue())  );
			psUpd.setFloat (7, (new Float  (X[5]).floatValue()));
			psUpd.setFloat (8, (new Float  (X[7]).floatValue()));
			
			psIns.executeUpdate();
			wordInsCnt++;
		}
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static void main(String[] args)  throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException, IOException {
		boolean ok = true;
		
		// Error -- no dictionary file name specified:
		if (args.length == 0) {
			System.out.println("Usage: PupulateWordTbl <action> <optional-input-file>");
			return;
		}
		
		// (1) Init:
		String action = args[0];
		
		Class.forName("org.postgresql.Driver").newInstance();
		conn = DriverManager.getConnection("jdbc:postgresql:" + DB_NAME, DB_USERNAME, DB_PASSWD);
		conn.setAutoCommit(false);
		
		conn.createStatement().execute("SET search_path TO " + DB_SCHEMA_NAME);
		
		try{
			// (2) Execute action:
			// (2.1) Delete all words:
			if (action.equalsIgnoreCase("delete-all")) {
				System.out.print("Processing... ");
				wordDelCnt = (int) getLong("SELECT COUNT(id) AS cnt FROM word", "cnt");
			}
			
			// (2.2) Import the MRC database:
			else if (action.equalsIgnoreCase("import-mrc")) {
				// Error -- input file not provided:
				if (args.length < 2) {
					System.out.println("Error: Input file not provided");
					return;
				}
				
				// Error -- input file doesn't exist:
				String filepath = args[1];
				if (!(new File(filepath)).exists()) {
					System.out.println("Error: designated input (\"" + filepath + "\") file does not exist");
					return;
				}
				
				// Execute:
				System.out.print("Processing... ");
				
				// remove all existing words:
				wordDelCnt = (int) getLong("SELECT COUNT(id) AS cnt FROM word", "cnt");
				conn.createStatement().executeUpdate("DELETE FROM " + DB_SCHEMA_NAME + ".word");
				
				// import:
				PreparedStatement psUpd = conn.prepareStatement("UPDATE " + DB_SCHEMA_NAME + ".word SET mrc_nphon=?, mrc_nsyl=?, mrc_kffrq=?, mrc_kfcat=?, mrc_kfsmp=?, mrc_tlfrq=?, mrc_bfrq=?, mrc_fam=?, mrc_cnc=?, mrc_img=?, mrc_meanc=?, mrc_meanp=?, mrc_aoa=?, mrc_tq2=?, mrc_type=?, mrc_pdtype=?, mrc_alphsyl=?, mrc_status=?, mrc_var=?, mrc_cap=?, mrc_plur=?, mrc_phon=?, mrc_dphon=?, mrc_stress=? WHERE id = ?");
				
				BufferedReader in = new BufferedReader(new FileReader(filepath));
				String l;
				while ((l = in.readLine()) != null) mrcProcLine(l, psUpd);
				in.close();
				
				// set some of the fields null (they are marked with zeros in the input file):
				Statement stTmp = conn.createStatement();
				stTmp.addBatch("UPDATE " + DB_SCHEMA_NAME + ".word SET mrc_nphon = null WHERE mrc_nphon = 0");
				stTmp.addBatch("UPDATE " + DB_SCHEMA_NAME + ".word SET mrc_nsyl  = null WHERE mrc_nsyl  = 0");
				stTmp.addBatch("UPDATE " + DB_SCHEMA_NAME + ".word SET mrc_kffrq = null WHERE mrc_kffrq = 0");
				stTmp.addBatch("UPDATE " + DB_SCHEMA_NAME + ".word SET mrc_kfcat = null WHERE mrc_kfcat = 0");
				stTmp.addBatch("UPDATE " + DB_SCHEMA_NAME + ".word SET mrc_kfsmp = null WHERE mrc_kfsmp = 0");
				stTmp.addBatch("UPDATE " + DB_SCHEMA_NAME + ".word SET mrc_tlfrq = null WHERE mrc_tlfrq = 0");
				stTmp.addBatch("UPDATE " + DB_SCHEMA_NAME + ".word SET mrc_bfrq  = null WHERE mrc_bfrq  = 0");
				stTmp.addBatch("UPDATE " + DB_SCHEMA_NAME + ".word SET mrc_fam   = null WHERE mrc_fam   = 0");
				stTmp.addBatch("UPDATE " + DB_SCHEMA_NAME + ".word SET mrc_img   = null WHERE mrc_img   = 0");
				stTmp.addBatch("UPDATE " + DB_SCHEMA_NAME + ".word SET mrc_cnc   = null WHERE mrc_cnc   = 0");
				stTmp.addBatch("UPDATE " + DB_SCHEMA_NAME + ".word SET mrc_meanc = null WHERE mrc_meanc = 0");
				stTmp.addBatch("UPDATE " + DB_SCHEMA_NAME + ".word SET mrc_meanp = null WHERE mrc_meanp = 0");
				stTmp.addBatch("UPDATE " + DB_SCHEMA_NAME + ".word SET mrc_aoa   = null WHERE mrc_aoa   = 0");
				stTmp.executeBatch();
				
				// commit:
				conn.commit();
				conn.setAutoCommit(true);
			}
			
			// (2.3) Import the SUBTL database:
			else if (action.equalsIgnoreCase("import-subtl")) {
				// Error -- input file not provided:
				if (args.length < 2) {
					System.out.println("Error: Input file not provided");
					return;
				}
				
				// Error -- input file doesn't exist:
				String filepath = args[1];
				if (!(new File(filepath)).exists()) {
					System.out.println("Error: designated input (\"" + filepath + "\") file does not exist");
					return;
				}
				
				// Execute:
				System.out.print("Processing... ");
				
				// import:
				PreparedStatement psUpd = conn.prepareStatement("UPDATE " + DB_SCHEMA_NAME + ".word SET subtl_nocc=?, subtl_nmov=?, subtl_nocc_lc=?, subtl_nmov_lc=?, subtl_frq=?, subtl_pcd=? WHERE word = ?");
				PreparedStatement psIns = conn.prepareStatement("INSERT INTO " + DB_SCHEMA_NAME + ".word (word, nlet, subtl_nocc, subtl_nmov, subtl_nocc_lc, subtl_nmov_lc, subtl_frq, subtl_pcd) VALUES (?,?,?,?,?,?,?,?)");
				
				BufferedReader in = new BufferedReader(new FileReader(filepath));
				String l;
				in.readLine();  // skip the header line
				while ((l = in.readLine()) != null) subtlProcLine(l, psUpd, psIns);
				in.close();
				
				// commit:
				conn.commit();
				conn.setAutoCommit(true);
			}
			
			// (2.4)
			else {
				ok = false;
				System.out.println("Error: Unknown action \"" + action + "\"");
			}
		}
		catch (SQLException e) {
			conn.rollback();
			conn.setAutoCommit(true);
		}
		
		// (3) Finish:
		if (ok) System.out.println("done (deleted: " + wordDelCnt + ", inserted: " + wordInsCnt + ", updated: " + wordUpdCnt + ")");
	}
}
