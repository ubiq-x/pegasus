package inter;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.Result;


/**
 * @author Tomek D. Loboda
 */
public class HTTPI {
	public static final int BUF_SIZE = 8192;
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static String getParamStr(HttpServletRequest req, String name)  throws UnsupportedEncodingException {
		String p = req.getParameter(name);
		try {
			return (p == null ? null : URLDecoder.decode(p, "utf-8").trim());
		}
		catch (IllegalArgumentException e) {
			return (p == null ? null : p.trim());
		}
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static String getParamStr(HttpServletRequest req, String name, String defVal) {
		String param = req.getParameter(name);
		return (param != null ? param : defVal);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static int getParamInt(HttpServletRequest req, String name, int defVal) {
		String param = req.getParameter(name);
		return (param != null ? (new Integer(param)).intValue() : defVal);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static boolean getParamBool(HttpServletRequest req, String name, boolean defVal) {
		String param = req.getParameter(name);
		if (param == null) return defVal;
		
		int i = new Integer(param).intValue();
		return (i == 1 ? true : false);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static boolean isWebServiceOnline(String url, int timeoutSec) {
		try {
			URL u = new URL(url);
			HttpURLConnection huc =  (HttpURLConnection) u.openConnection();
			if (timeoutSec > 0) huc.setConnectTimeout(timeoutSec * 1000);
			huc.setRequestMethod("HEAD");
			return (huc.getResponseCode() == HttpURLConnection.HTTP_OK);
			
			//huc.setRequestMethod("GET");
			//huc.connect();
			//return huc.getResponseCode();
		}
		catch (Exception e) {
			return false;
		}
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static StringBuilder makeReq(String url)  throws MalformedURLException, IOException {
		StringBuilder res = new StringBuilder();
	    URLConnection uconn = (new URL(url)).openConnection();
	    BufferedReader in = new BufferedReader(new InputStreamReader(uconn.getInputStream()));
	    
	    String ln;
	    while ((ln = in.readLine()) != null) res.append(ln);
	    in.close();
		
	    return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static void retRes(HttpServletResponse res, Result r)  throws IOException {
		HTTPI.retTxt(res, r.getMsg(true));
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static void retFile(HttpServletResponse res, String contType, String filename, boolean delete)  throws IOException {
		File f = new File(filename);
		res.setContentType(contType); 
		res.setContentLength((int) f.length());
		res.setHeader("Content-Disposition","attachment; filename=" + filename);
		
		ServletOutputStream os = res.getOutputStream();
		byte[] buff = new byte[8192];
		DataInputStream is = new DataInputStream(new FileInputStream(f));
		int len = 0;
		while (is != null && (len = is.read(buff)) != -1) os.write(buff, 0, len);
		is.close();
		os.flush();
		os.close();
		
		if (delete) f.delete();
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static void retFileDB(HttpServletResponse res, DBI dbi, String qry, String contType, String filename)  throws IOException, SQLException {
		ResultSet rs = dbi.execQry(qry);
		rs.next();
		byte[] b = rs.getBytes(1);
		rs.close();
		
		res.setContentType(contType);
		res.setContentLength(b.length);
		res.setHeader("Content-Disposition","attachment; filename=" + filename);
		
		ServletOutputStream os = res.getOutputStream();
		os.write(b);
		os.flush();
		os.close();
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static void retHTML(HttpServletResponse res, StringBuffer txt)   throws IOException { retHTML(res, txt.toString()); }
	public static void retHTML(HttpServletResponse res, StringBuilder txt)  throws IOException { retHTML(res, txt.toString()); }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static void retTxt(HttpServletResponse res, StringBuffer txt)   throws IOException { retTxt(res, txt.toString());         }
	public static void retTxt(HttpServletResponse res, StringBuilder txt)  throws IOException { retTxt(res, txt.toString());         }
	public static void retTxt(HttpServletResponse res, boolean b)          throws IOException { retTxt(res, (b ? "true" : "false")); }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static void retStr(HttpServletResponse res, Exception e)  throws IOException {
		res.setContentType("text/plain");
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		pw.flush();
		
		PrintWriter out = res.getWriter();
		out.write("{outcome:false,msg:" + JSI.str2js("SERVER SIDE EXCEPTION:\n\n" + sw.getBuffer().toString()) + "}");
		out.close();
		
		pw.close();
		sw.close();
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static void retTxt  (HttpServletResponse res, String cont)  throws IOException { ret(res, "text/plain", BUF_SIZE, cont); }
	public static void retHTML (HttpServletResponse res, String cont)  throws IOException { ret(res, "text/html",  BUF_SIZE, cont); }
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static void ret(HttpServletResponse res, String contType, int buffsize, String cont)  throws IOException {
		res.setContentType(contType);
		res.setBufferSize(buffsize);
		PrintWriter out = res.getWriter();
		out.print(cont);
		out.close();
		res.flushBuffer();
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	// src: http://blog.csdn.net/Pockey/archive/2009/03/16/3994462.aspx
	public static void retZIP(HttpServletResponse res, File dir, String filename, boolean delete)  throws IOException {
		dir.setReadable(true);
		byte[] zip = FSI.zipDir(dir, 0); 
		ServletOutputStream os = res.getOutputStream();
		res.setContentType("application/zip"); 
		res.setContentLength((int) zip.length);
		res.setHeader("Content-Disposition","attachment; filename=" + filename + ".zip");
		os.write(zip);
		os.flush();
		
		if (delete) FSI.delDir(dir, true);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Prepares the response object and returns the writer. The method that get this writer should take care of 
	 * closing it, and flushing the response object buffer.
	 */
	public static Writer getWriter(HttpServletResponse res, String contType)  throws IOException {
		res.setContentType(contType);
		res.setBufferSize(BUF_SIZE);
		PrintWriter out = res.getWriter();
		return out;
	}
}
