/**
http://www.rosuda.org/r/nightly/javadoc/org/rosuda/JRI/Rengine.html
http://www.statmethods.net/index.html

http://en.wikipedia.org/wiki/GNU_Scientific_Library
http://en.wikipedia.org/wiki/GNU_Octave
http://www.ci.tuwien.ac.at/gR/


* Graphics:

http://osiris.sunderland.ac.uk/~cs0her/Statistics/UsingLatticeGraphicsInR.htm
*/

package inter;

import java.io.IOException;

import main.Do;

import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;


/**
 * @author Tomek D. Loboda
 */
public class DSI {
	//private final static String VAR = "@@VAR@@";
	//private final static String DS = "@@DS@@";
	
	public final RConnection conn;
	
	
	// ---------------------------------------------------------------------------------
	public DSI()  throws RserveException {
		conn = new RConnection();
	}
	
	
	// ---------------------------------------------------------------------------------
	public void plotDist(String ds, String plot, String x1, String x2, String x3, String type, String layout)  throws RserveException {
		String ds2 = ds.replaceAll("-", "_");
		
		conn.eval(ds2 + " <- read.table(\"" + Do.getPathDS().replace("\\", "/") + ds + "\\/ds\", header = T, sep = \"\\t\", na.strings = \".\")");
		conn.eval("png(\"" + Do.getPathDS().replace("\\", "/") + ds + "\\/work\\/dist.png\")");
		
		if (plot.equals("histogram")) {
			conn.eval("print(histogram(" + x1 + " ~ " + x2 + (x3.length() > 0 ? " | " + x3 : "") + ", data = " + ds2 + ", layout = c(" + layout + "), type=\"" + type + "\"))");
		}
		else if (plot.equals("density")) {
			conn.eval("print(densityplot(" + x1 + " ~ " + x2 + (x3.length() > 0 ? " | " + x3 : "") + ", data = " + ds2 + ", layout = c(" + layout + "), type=\"" + type + "\"))");
		}
		else if (plot.equals("scatter")) {
			conn.eval("print(xyplot(" + x1 + " ~ " + x2 + (x3.length() > 0 ? " | " + x3 : "") + ", data = " + ds2 + ", layout = c(" + layout + ")))");
		}
		else if (plot.equals("time-series")) {
			conn.eval("print(xyplot(" + x1 + " ~ min(" + x1 + ", na.rm = T):max(" + x1 + ", na.rm = T), data = " + ds2 + ", layout = c(" + layout + "), type = \"l\"))");
		}
		else if (plot.equals("qq")) {
			conn.eval("print(qq(" + x1 + " ~ " + x2 + (x3.length() > 0 ? " | " + x3 : "") + ", data = " + ds2 + ", layout = c(" + layout + ")))");
		}
		
		conn.eval("dev.off()");
	}
	
	
	// ---------------------------------------------------------------------------------
	/*
	public static String getLst() {
		File dsDir = new File(Do.getPathDS());
		String[] F = dsDir.list();
		List<String> F2 = new ArrayList<String>();
		for (int i = 0; i < F.length; i++) {
			F2.add(JSI.str2js(F[i]));
		}
		Collections.sort(F2);
		
		return "{outcome:true,lst:[" + $.join(F2, "", ",", false, false) + "]}";
	}
	*/
	
	
	// ---------------------------------------------------------------------------------
	public String gfxGenBoxplot(String ds, String var)  throws RserveException {
		String v = ds.replaceAll("-", "_") + "$" + var;
		conn.eval("png(\"" + Do.getPathDS().replace("\\", "/") + ds + "/img-sys/boxplot-" + var + ".png\")");
		conn.eval("plot.new()");
		conn.eval("boxplot(" + v + ", horizontal = T, main = \"" + var + "\")");
		conn.eval("dev.off()");
		
		return "boxplot-" + var + ".png";
	}
	
	
	// ---------------------------------------------------------------------------------
	public String gfxGenHist(String ds, String var)  throws RserveException {
		String v = ds.replaceAll("-", "_") + "$" + var;
		conn.eval("png(\"" + Do.getPathDS().replace("\\", "/") + ds + "/img-sys/hist-" + var + ".png\")");
		conn.eval("plot.new()");
		conn.eval("hist(" + v + ", freq = F, main = \"" + var + "\")");
		conn.eval("lines(density(" + v + "), col = \"red\")");
		conn.eval("dev.off()");
		
		return "hist-" + var + ".png";
	}
	
	
	// ---------------------------------------------------------------------------------
	public String gfxGenPiechart(String ds, String var)  throws RserveException {
		String v = ds.replaceAll("-", "_") + "$" + var;
		conn.eval("png(\"" + Do.getPathDS().replace("\\", "/") + ds + "/img-sys/pie-" + var + ".png\")");
		conn.eval("plot.new()");
		conn.eval("pie(" + v + ", main = \"" + var + "\")");
		conn.eval("dev.off()");
		
		return "pie-" + var + ".png";
	}
	
	
	// ---------------------------------------------------------------------------------
	public StringBuilder dsGetVars(String ds)  throws RserveException, REXPMismatchException {
		String[] vars = conn.eval("names(" + ds.replaceAll("-", "_") + ")").asStrings();
		
		StringBuilder res = new StringBuilder("[");
		for (int i = 0; i < vars.length; i++) {
			String v = ds.replaceAll("-", "_") + "$'" + vars[i] + "'";
			
			// TODO: check if numeric type:
			//REXP rxMean   = conn.eval("mean("   + v + ", na.rm = T)");
			REXP rxMean   = conn.eval("colMeans(" + v + ", na.rm = T)[1]");
			REXP rxMin    = conn.eval("min("      + v + ", na.rm = T)");
			REXP rxMax    = conn.eval("max("      + v + ", na.rm = T)");
			REXP rxSD     = conn.eval("sd("       + v + ", na.rm = T)");
			REXP rxVar    = conn.eval("var("      + v + ", na.rm = T)");
			REXP rxMedian = conn.eval("median("   + v + ", na.rm = T)");
			
			REXP rxSW  = conn.eval("shapiro.test(" + v + ")");
			REXP rxKS  = conn.eval("lillie.test("  + v + ")");
			REXP rxAD  = conn.eval("ad.test("      + v + ")");
			REXP rxCVM = conn.eval("cvm.test("     + v + ")");
			
			res.append(
				"{" +
					"name:\"" + vars[i] + "\"," +
					"type:0," +
					"cols:[" +
						"{type:\"sep\"}," +
						
						"{name:\"n\",type:\"num\",val:" + conn.eval("length(" + v + ")").asInteger() + "}," +
						
						"{type:\"sep\"}," +
						
						"{name:\"mean\",type:\"num\",val:"   + (rxMean    == null ? "\"\"" : rxMean.asDouble()) + "}," +
						"{name:\"median\",type:\"num\",val:" + (rxMedian  == null ? "\"\"" : rxMedian.asDouble()) + "}," +
						"{name:\"min\",type:\"num\",val:"    + (rxMin     == null ? "\"\"" : rxMin.asDouble())    + "}," +
						"{name:\"max\",type:\"num\",val:"    + (rxMax     == null ? "\"\"" : rxMax.asDouble())    + "}," +
						
						"{type:\"sep\"}," +
						
						"{name:\"sd\",type:\"num\",val:"  + (rxSD  == null ? "\"\"" : rxSD.asDouble())  + "}," +
						"{name:\"var\",type:\"num\",val:" + (rxVar == null ? "\"\"" : rxVar.asDouble()) + "}," +
						
						"{type:\"sep\"}," +
						
						"{name:\"ks-s\",type:\"num\",val:" + (rxKS == null ? "\"\"" : rxKS.asDoubles()[0]) + "}," +
						"{name:\"ks-p\",type:\"num\",val:" + (rxKS == null ? "\"\"" : rxKS.asDoubles()[1]) + "}," +
						"{name:\"ks-d\",type:\"str\",val:" + (rxKS == null ? "\"\"" : (rxKS.asDoubles()[1] > 0.05 ? "\"n\"" : "\"\""))  + "}," +
						
						"{type:\"sep\"}," +
						
						"{name:\"ad-s\",type:\"num\",val:" + (rxAD == null ? "\"\"" : rxAD.asDoubles()[0]) + "}," +
						"{name:\"ad-p\",type:\"num\",val:" + (rxAD == null ? "\"\"" : rxAD.asDoubles()[1]) + "}," +
						"{name:\"ad-d\",type:\"str\",val:" + (rxAD == null ? "\"\"" : (rxAD.asDoubles()[1] > 0.05 ? "\"n\"" : "\"\""))  + "}," +
						
						"{type:\"sep\"}," +
						
						"{name:\"sw-s\",type:\"num\",val:" + (rxSW == null ? "\"\"" : rxSW.asDoubles()[0]) + "}," +
						"{name:\"sw-p\",type:\"num\",val:" + (rxSW == null ? "\"\"" : rxSW.asDoubles()[1]) + "}," +
						"{name:\"sw-d\",type:\"str\",val:" + (rxSW == null ? "\"\"" : (rxSW.asDoubles()[1] > 0.05 ? "\"n\"" : "\"\""))  + "}," +
						
						"{type:\"sep\"}," +
						
						"{name:\"cvm-s\",type:\"num\",val:" + (rxCVM == null ? "\"\"" : rxCVM.asDoubles()[0]) + "}," +
						"{name:\"cvm-p\",type:\"num\",val:" + (rxCVM == null ? "\"\"" : rxCVM.asDoubles()[1]) + "}," +
						"{name:\"cvm-d\",type:\"str\",val:" + (rxCVM == null ? "\"\"" : (rxCVM.asDoubles()[1] > 0.05 ? "\"n\"" : "\"\""))  + "}," +
						
						"{type:\"sep\"}," +
						
						"{name:\"box\",type:\"img\",val:"  + JSI.str2js(gfxGenBoxplot (ds, vars[i])) + "}" + "," +
						"{name:\"hist\",type:\"img\",val:" + JSI.str2js(gfxGenHist    (ds, vars[i])) + "}" +
					"]" +
				"}" + (i < vars.length - 1 ? "," : "")
			);
		}
		res.append("]");
		return res;
	}
	
	
	// ---------------------------------------------------------------------------------
	public String dsOpen(String ds)  throws Exception {
		StringBuilder res = new StringBuilder();
		
		conn.eval(ds.replaceAll("-", "_") + " <- read.table(\"" + Do.getPathDS().replace("\\", "/") + ds + "\\/ds\", header = T, sep = \"\\t\", na.strings = \".\")");
		//onAllVars(re, "D", "@@VAR@@$@@VAR@@ <- as.numeric(@@DS@@$@@VAR@@");
		res.append("{outcome:true,vars:" + dsGetVars(ds) + "}");
        
		return res.toString();
	}
	
	
	// ---------------------------------------------------------------------------------
	// TODO: implement of remove
	public String xform2TS()  throws IOException {
		/*
		StringBuffer sb = FSInterface.getStr("D:\\ds");
		int idxEndLn = sb.indexOf("\n");
		DataSet ds = new DataSet(sb.substring(idxEndLn+1), sb.substring(0,idxEndLn), "\n", "\t");
		*/
		
		return "{outcome:true}";
	}
	
	
	// ---------------------------------------------------------------------------------
	/*
	public void onAllVars(Rengine re, String ds, String cmd) {
		String[] V = re.eval("names(" + ds.replaceAll("-", "_") + ")").asStringArray();
		for (String v: V) {
			re.eval(cmd.replaceAll(DS, ds).replaceAll(VAR, v));
		}
	}
	*/
}
