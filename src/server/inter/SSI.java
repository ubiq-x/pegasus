package inter;

import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;


/**
 * Spreadsheet Interface
 * 
 * @author Tomek D. Loboda
 */
public class SSI {
	public final HSSFWorkbook wb = new HSSFWorkbook();
	
	private int sheetCnt = 0;
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * C'tor.
	 */
	public SSI() {}
	
	
	// ---------------------------------------------------------------------------------
	public HSSFSheet addSheet(String name) {
		HSSFSheet s = wb.createSheet();
		wb.setSheetName(sheetCnt++, name);
		return s;
	}
	
	
	// ---------------------------------------------------------------------------------
	public HSSFCell addCell(HSSFRow r, int idx, int type, HSSFCellStyle cs) {
		HSSFCell c = r.createCell(idx, type);
		if (cs != null) c.setCellStyle(cs);
		return c;
	}
	
	
	// ---------------------------------------------------------------------------------
	public void addRow(HSSFSheet s, int idx, int cellIdxOffset, String txt, String delim, HSSFCellStyle csInt, HSSFCellStyle csDbl, HSSFCellStyle csStr) {
		HSSFRow r = s.createRow(idx);
		String[] C = txt.split("\t");  // cells
		
		for (int j = 0; j < C.length; j++) {
			HSSFCell c = null;
			try {
				int val = new Integer(C[j]);
				c = r.createCell(cellIdxOffset + j, HSSFCell.CELL_TYPE_NUMERIC);
				c.setCellValue(val);
				if (csInt != null) c.setCellStyle(csInt);
			}
			catch (NumberFormatException e01) {
				try {
					double val = new Double(C[j]);
					c = r.createCell(cellIdxOffset + j, HSSFCell.CELL_TYPE_NUMERIC);
					c.setCellValue(val);
					if (csDbl != null) c.setCellStyle(csDbl);
				}
				catch (NumberFormatException e02) {
					c = r.createCell(cellIdxOffset + j, HSSFCell.CELL_TYPE_STRING);
					c.setCellValue(new HSSFRichTextString(C[j]));
					if (csStr != null) c.setCellStyle(csStr);
				}
			}
		}
	}
	
	
	// ---------------------------------------------------------------------------------
	public void addStats(HSSFSheet s, int rowStart, int colStart, int rowCnt, int colCnt, HSSFCellStyle csH, HSSFCellStyle cs) {
		if (rowCnt == 0 || colCnt == 0) return;
		
		int rowEnd = ++rowStart + rowCnt - 2;
		int colEnd = colStart + colCnt;
		
		for (int i = 1; i <= 6; i++) {
			HSSFCell c = s.createRow(i-1).createCell(0, HSSFCell.CELL_TYPE_STRING);
			switch (i) {
				case 1: c.setCellValue(new HSSFRichTextString("mean")); break;
				case 2: c.setCellValue(new HSSFRichTextString("median")); break;
				case 3: c.setCellValue(new HSSFRichTextString("std dev")); break;
				case 4: c.setCellValue(new HSSFRichTextString("std err")); break;
				case 5: c.setCellValue(new HSSFRichTextString("min")); break;
				case 6: c.setCellValue(new HSSFRichTextString("max")); break;
			}
			c.setCellStyle(csH);
		}
		
		for (int j = colStart; j < colEnd; j++) {
			String colName = getColName(j);
			for (int i = 1; i <= 6; i++) {
				String range = colName + rowStart + ":" + colName + rowEnd;
				HSSFCell c = s.getRow(i-1).createCell(j, HSSFCell.CELL_TYPE_NUMERIC);
				switch (i) {
					case 1: c.setCellFormula("AVERAGE(" + range + ")"); break;
					case 2: c.setCellFormula("MEDIAN(" + range + ")"); break;
					case 3: c.setCellFormula("STDEV(" + range + ")"); break;
					case 4: c.setCellFormula("STDEV(" + range + ")/" + (rowEnd-rowStart)); break;
					case 5: c.setCellFormula("MIN(" + range + ")"); break;
					case 6: c.setCellFormula("MAX(" + range + ")"); break;
				}
				c.setCellStyle(cs);
			}
		}
	}
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * Converts column index into the name (0=A, 1=B, ...)
	 */
	public static String getColName(int colIdx) { return (colIdx <= 25 ? "" + (char) (colIdx + 65) : "" + (char) (colIdx / 26 + 64) + (char) (colIdx % 26 + 65)); }
	
	
	// ---------------------------------------------------------------------------------
	public HSSFCellStyle getStyle(int ftSize, String ftName, boolean ftBold, boolean ftItalic, int align, String formatStr) {
		HSSFFont f = wb.createFont();
		f.setFontHeightInPoints((short) ftSize);
		if (ftName != null) f.setFontName(ftName);
		f.setBoldweight(ftBold ? HSSFFont.BOLDWEIGHT_BOLD : HSSFFont.BOLDWEIGHT_NORMAL);
		f.setItalic(ftItalic);
		HSSFCellStyle cs = wb.createCellStyle();
		cs.setFont(f);
		cs.setAlignment((short) align);
		if (formatStr != null) {
			HSSFDataFormat format = wb.createDataFormat();
			cs.setDataFormat(format.getFormat(formatStr));
		}
		return cs;
	}
	
	
	// ---------------------------------------------------------------------------------
	public void write(String filename)  throws IOException {
		FileOutputStream fos = new FileOutputStream(filename);
		try {
			wb.write(fos);
		}
		catch (java.lang.OutOfMemoryError e) {
			throw new java.lang.OutOfMemoryError();
		}
		finally {
			fos.close();
		}
	}
}



