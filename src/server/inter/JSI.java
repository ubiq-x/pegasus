package inter;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;


/**
 * @author Tomek D. Loboda
 */
public class JSI {
	
	// ---------------------------------------------------------------------------------
	public static String str2js(String s) {
		if (s == null) return "\"\"";
		try {
			s = "$dec(\"" + URLEncoder.encode(s,"UTF-8") + "\")";
		}
		catch (UnsupportedEncodingException e) {}
		return s;
	}
	
	
	// ---------------------------------------------------------------------------------
	public static String str2js(StringBuffer s)  { return str2js(s.toString()); }
	public static String str2js(StringBuilder s) { return str2js(s.toString()); }
	
	
	// ---------------------------------------------------------------------------------
	public static StringBuffer getGraphSeries(String name, String color, String point, List<String> points) {
		StringBuffer res = new StringBuffer();
		
		res.append(
			"{" +
			"  name  : \"" + name + "\"," +
			"  plot  : {" +
			"    type    : graph.PLOT_TYPE_POINT," +
			"    line    : { width: 1, color: \"" + color + "\" }," +
			"    point   : { type: " + point + ", radius: 5, side: 9, width: 1, fgColor: \"" + color + "\", bgColor: \"#ffffff\" }," +
			"    tooltip : { coords: true, coordsBgColor: \"" + color + "\", coordsFgColor: \"#ffffff\" }" +
			"  }," +
			"  points : ["
		);
		
		for (String p: points) {
			res.append(p + ",");
		}
		
		res.append(
			"  ]," +
			"}"
		);
		
		return res;
	}
}
