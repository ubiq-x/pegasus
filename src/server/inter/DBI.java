package inter;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import main.Do;
import main.Result;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;

import util.$;
import util.Convert;
import util.SQLConn;
import app.data.DataSet;
import app.gaze.Exp;
import app.gaze.Var;
import app.gaze.dstruct.Word;
import app.main.Prj;


/**
 * @author Tomek D. Loboda
 */
public class DBI {
	public final static int SQL_ERR_DUP_ENTRY = 1062;
	
	//public final static String CONN_STR = "jdbc:mysql://localhost?user=root&password=sa";
	public final static int ID_NONE = -1;
	
	public final static String DB_NAME = "pegasus";
	public final static String DB_USERNAME = "postgres";
	public final static String DB_PASSWD = "sa";
	
	// http://www.postgresql.org/docs/8.1/static/datatype.html
	
	// http://dev.mysql.com/doc/refman/5.0/en/storage-requirements.html
	public final static int FIELD_SIZE_TINY_INT = 1;
	public final static int FIELD_SIZE_INT = 4;
	public final static int FIELD_SIZE_BIGINT = 8;
	public final static int FIELD_SIZE_FLOAT = 4;
	public final static int FIELD_SIZE_DOUBLE = 8;
	public final static int FIELD_SIZE_DATE = 3;
	public final static int FIELD_SIZE_TIME = 3;
	public final static int FIELD_SIZE_DATETIME = 8;
	public final static int FIELD_SIZE_TIMESTAMP = 4;
	public final static int FIELD_SIZE_CHAR = 1;  // incorrect?
	public final static int FIELD_SIZE_VARCHAR = 1;
	public final static int FIELD_SIZE_BLOB = 2;
	public final static int FIELD_SIZE_TEXT = 2;
	
	public final static int REC_SIZE_DATA_SET = 32;
	public final static int REC_SIZE_REP = 25;
	public final static int REC_SIZE_REP_ITEM = 20;
	public final static int REC_SIZE_VAR = 76;
	
	private final SQLConn conn;
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public DBI()  throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		//conn = new SQLConn();
		conn = Do.sqlConn;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Result delAllDS(String username, int expId)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		StringBuffer dsIds = new StringBuffer();
		ResultSet rs = execQry("SELECT ds_id FROM \"" + Do.DB_GAZE + "-" + username + "\".ds WHERE exp_id = " + expId);
		while (rs.next()) {
			dsIds.append(rs.getString("ds_id") + (!rs.isLast() ? "," : ""));
		}
		
		return DataSet.del(this, username, dsIds.toString());
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/*
	public String delVar(String username, int id)  throws SQLException {
		String db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		
		execUpd("DELETE FROM " + db + ".var WHERE id = " + id + " AND sys = 0");
		return "{outcome:true}";
	}
	*/
	
	// -----------------------------------------------------------------------------------------------------------------
	public void destroy()  throws SQLException {}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public String dsGetVars(String username, int id)  throws SQLException {
		String db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		
		String[] V = getStr(this, "SELECT vars FROM " + db + ".ds WHERE id = " + id, "vars").split("\t");
		StringBuilder sb = new StringBuilder("{outcome:true,vars:[");
		for (int i = 0; i < V.length; i++) sb.append(JSI.str2js(V[i]) + (i < V.length - 1 ? "," : ""));
		sb.append("]}");
		return sb.toString();
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public String excFirstNFix(String username, int trialId, int stimId, int stimSeqId, int t0, int t1, int n)  throws SQLException {
		String db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		
		Statement st = conn.get().createStatement();
		String where = "WHERE trial_id = " + trialId + " AND stim_id = " + stimId + " AND t >= " + t0 + " AND t <= " + t1;
		long num01 = getLong(this, "SELECT num FROM " + db + ".fix " + where + " ORDER BY num LIMIT 1", "num");
		st.executeUpdate("UPDATE " + db + ".fix SET exc = 0 " + where);
		st.executeUpdate("UPDATE " + db + ".fix SET exc = 1 " + where + " AND num < " + (num01+n));
		st.executeUpdate("UPDATE " + db + ".stim_seq SET fix_cnt_exc = " + n + " WHERE id = " + stimSeqId);
		return "{outcome:true}";
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Result excObjToggle(String username, String name, int id, String info)  throws SQLException {
		execUpd("UPDATE \"" + Do.DB_GAZE + "-" + username + "\".\"" + name + "\" SET exc = NOT exc, exc_info = " + DBI.esc(info) + " WHERE id = " + id);
		return new Result(true, "");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public ResultSet execQry(String qry)  throws SQLException {
		return execQry(ResultSet.TYPE_FORWARD_ONLY, qry);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public ResultSet execQry(int type, String qry)  throws SQLException {
		return conn.get().createStatement(type, ResultSet.CONCUR_READ_ONLY).executeQuery(qry);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public void execUpd(String qry)  throws SQLException {
		Statement st = conn.get().createStatement();
		st.executeUpdate(qry);
		st.close();
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static String esc(String s) {
		if (s == null) return null;
		//return "'" + s.replaceAll("'", "\\\\'") + "'";
		return "'" + s.replaceAll("'", "''") + "'";
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public String genSpreadsheet(String username, String dsIds)  throws IOException, SQLException {
		String db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		
		// (1) Prepare the spreadsheet:
		SSI si = new SSI();
		
		HSSFCellStyle csIntH = si.getStyle(8, "Tahoma", true,  false, HSSFCellStyle.ALIGN_CENTER, null);
		HSSFCellStyle csDblH = si.getStyle(8, "Tahoma", true,  false, HSSFCellStyle.ALIGN_CENTER, "0.00");
		HSSFCellStyle csStrH = si.getStyle(8, "Tahoma", true,  false, HSSFCellStyle.ALIGN_CENTER, null);
		HSSFCellStyle csInt  = si.getStyle(8, "Tahoma", false, false, HSSFCellStyle.ALIGN_CENTER, null);
		HSSFCellStyle csDbl  = si.getStyle(8, "Tahoma", false, false, HSSFCellStyle.ALIGN_CENTER, "0.00");
		HSSFCellStyle csStr  = si.getStyle(8, "Tahoma", false, false, HSSFCellStyle.ALIGN_CENTER, null);
		
		HSSFSheet compSheet = si.addSheet("Comp");  // the comparison sheet should be the first
		HSSFSheet statSheet = si.addSheet("Stat");  // the stat sheet should be the second
		
		// (2) Create a sheet for each dataset:
		Statement st = conn.get().createStatement();
		ResultSet rs = st.executeQuery("SELECT name, vars, data FROM " + db + ".ds WHERE id IN (" + dsIds + ") ORDER BY id");
		
		while (rs.next()) {
			HSSFSheet s = si.addSheet(rs.getString("name"));
			String[] L = rs.getString("data").replaceAll("\r", "").split("\n");  // lines
			
			si.addRow(s, 0+7, 0, rs.getString("vars"), "\t", csIntH, csDblH, csStrH);
			for (int i = 0; i < L.length; i++) si.addRow(s, i+8, 0, L[i], "\t", csInt, csDbl, csStr);
			si.addStats(s, 8, 3, L.length + 1, L[0].split("\t").length - 3, csStrH, csDbl);
		}
		
		// (3) The comparison sheet:
		rs.beforeFirst();
		int idxR = 1;
		while (rs.next()) {
			HSSFRow r = compSheet.createRow(idxR);
			String name = rs.getString("name");
			
			// (3.1) Name:
			HSSFCell c = null;
			try {
				int nameInt = new Integer(name);
				c = r.createCell(0, HSSFCell.CELL_TYPE_NUMERIC);
				c.setCellValue(nameInt);
				c.setCellStyle(csIntH);
			}
			catch (NumberFormatException e) {
				c = r.createCell(0, HSSFCell.CELL_TYPE_STRING);
				c.setCellValue(new HSSFRichTextString(name));
				c.setCellStyle(csStrH);
			}
			
			// (3.2) Values:
			si.addRow(compSheet, 0, 1, "nroi\tnrois	t	wpm\twpms	nf\tnfpf\tnblink\tnr\tnfpr	nwf\tpwf\tpwfs	notf\tnosf	wlen-m	nf-m\tnftf-m	nr-m\tnfpr-m	afd-m\tffd-m\tsfd-m	gd-m	tt-m	rp-m	p0\tp1\tp2	p0s\tp1s\tp2s	p0fp\tp1fp\tp2fp	nmrc	nlet	nphon	nsyl	kffrq	ln-kffrq	log10-kffrq	kfcat	kfsmp	tlfrq	ln-tlfrq	log10-tlfrq	bfrq	ln-bfrq	log10-bfrq	fam	cnc	img	meanc	meanp	aoa	var-cl-e-n1	var-sent-e-n1", "\t", csIntH, csDblH, csStrH);
			int j = 1;
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!F1");  // nroi
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!G1");  // nrois
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!H1");  // t
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!I1");  // wpm
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!J1");  // wpms
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!K1");  // nf
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!L1");  // nftf
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!M1");  // nblink
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!N1");  // nr
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!O1");  // nfpr
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!T1");  // nwf
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!U1");  // pwf
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!V1");  // pwfs
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!W1");  // notf
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!AG1");  // nosf
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!AR1");  // wlen-m
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!AY1");  // nf-m
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!BD1");  // nftf-m
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!BK1");  // nr-m
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!BP1");  // nfpr-m
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!BV1");  // afd-m
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!CB1");  // ffd-m
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!CH1");  // sfd-m
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!CN1");  // gd-m
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!CT1");  // tt-m
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!CZ1");  // rp-m
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!DD1");  // p0
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!DE1");  // p1
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!DF1");  // p2
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!DG1");  // p0s
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!DH1");  // p1s
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!DI1");  // p2s
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!DJ1");  // p0fp
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!DK1");  // p1fp
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!DL1");  // p2fp
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!DN1");  // nmrc
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!DO1");  // nlet
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!DP1");  // nphon
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!DQ1");  // nsyl
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!DR1");  // kffrq
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!DS1");  // ln_kffrq
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!DT1");  // log10_kffrq
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!DU1");  // kfcat
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!DV1");  // kfsmp
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!DW1");  // tlfrq
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!DX1");  // ln_tlfrq
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!DY1");  // log10_tlfrq
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!DZ1");  // bfrq
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!EA1");  // ln_bfrq
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!EB1");  // log10_bfrq
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!EC1");  // fam
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!ED1");  // crc
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!EE1");  // img
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!EF1");  // meanc
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!EG1");  // meanp
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!EH1");  // aoa
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!EJ1");  // var-cl-e-n1
			si.addCell(r, j++, HSSFCell.CELL_TYPE_NUMERIC, csDbl).setCellFormula("'" + name + "'!FH1");  // var-sent-e-n1
			
			idxR++;
		}
		
		// (4) The stat sheet (for in-place analysis or easy coping over to a stats package):
		boolean statSheetHeadAdded = false;
		idxR = 1;
		rs.beforeFirst();
		while (rs.next()) {
			String name = rs.getString("name");
			String[] L = rs.getString("data").replaceAll("\r", "").split("\n");  // lines
			
			if (!statSheetHeadAdded) {
				si.addRow(statSheet, 0, 0, "cond\t" + rs.getString("vars"), "\t", csIntH, csDblH, csStrH);
				statSheetHeadAdded = true;
			}
			for (int i = 0; i < L.length - 1; i++) si.addRow(statSheet, idxR + i, 0, name + "\t" + L[i], "\t", csInt, csDbl, csStr);
			
			idxR += L.length - 1;
		}
		
		// (5) Finalize (includes persisting):
		st.close();
		rs.close();
		
		File tmpFile = File.createTempFile("ds-", ".xls", new File(Do.getPathWork()));
		
		try {
			si.write(tmpFile.getAbsolutePath());
		}
		catch (java.lang.OutOfMemoryError e) {
			throw new java.lang.OutOfMemoryError("Please try adding fewer results to your spreadsheet. The resources available on the server do not allow constructing such a big speadsheet.");
		}
		
		return tmpFile.getAbsolutePath();
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	@SuppressWarnings("deprecation")
	public StringBuffer getDS(String username, int id)  throws SQLException {
		String db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		
		StringBuffer res = new StringBuffer();
		
		Statement st = conn.get().createStatement();
		ResultSet rs = st.executeQuery("SELECT id, name, ts, qry, qry_h, var_cnt, obs_cnt, ts_len, (LENGTH(vars) + LENGTH(data)) AS size, calc_time, memo FROM " + db + ".ds WHERE id = " + id);
		while (rs.next()) {
			res.append(
				"{" +
				"id:" + rs.getString("id") + "," +
				"name:" + JSI.str2js(rs.getString("name")) + "," +
				"date:" + (rs.getTimestamp("ts").getYear() == 0 ? "null" : "new Date(" + rs.getTimestamp("ts").getTime() + ")") + "," +
				"qry:" + JSI.str2js(rs.getString("qry")) + "," +
				"qryH:" + JSI.str2js(rs.getString("qry_h")) + "," +
				"varCnt:" + rs.getInt("var_cnt") + "," +
				"obsCnt:" + rs.getInt("obs_cnt") + "," +
				"tsLen:" + rs.getInt("ts_len") + "," +
				"size:\"" + Convert.size2str(rs.getInt("size")) + "\"," +
				"calcTime:\"" + Convert.time2str(rs.getLong("calc_time")) + "\"," +
				"memo:" + JSI.str2js(rs.getString("memo")) +
				"}" + (!rs.isLast() ? "," : "")
			);
		}
		rs.close();
		st.close();
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public SQLConn getConn()  throws SQLException {
		return conn;
	}
	
	
	// ---------------------------------------------------------------------------------
	public static StringBuffer getEMData(DBI dbi, String username, int stimSeqId)  throws SQLException {
		String dbG = "\"" + Do.DB_GAZE + "-" + username + "\"";
		
		// (1) Fixations:
		ResultSet rsF = dbi.execQry(
			"SELECT " +
				"f.id, f.t, f.dur, f.x, f.y, f.x_adj, f.y_adj, roi_id_adj, " +
				"COALESCE(f.sacc_i_roi::text, 'null') AS sacc_i_roi, " +
				"COALESCE(f.sacc_i_roi_adj::text, 'null') AS sacc_i_roi_adj, " +
				"COALESCE(f.sacc_o_roi::text, 'null') AS sacc_o_roi, " +
				"COALESCE(f.sacc_o_roi_adj::text, 'null') AS sacc_o_roi_adj, " +
				"COALESCE(f.sacc_i_let::text, 'null') AS sacc_i_let, " +
				"COALESCE(f.sacc_i_let_adj::text, 'null') AS sacc_i_let_adj, " +
				"COALESCE(f.sacc_o_let::text, 'null') AS sacc_o_let, " +
				"COALESCE(f.sacc_o_let_adj::text, 'null') AS sacc_o_let_adj, " +
				"COALESCE(r.num::text, 'null') AS roi_num " +
			"FROM " + dbG + ".fix f " +
			"LEFT JOIN " + dbG + ".roi r ON f.roi_id = r.id " +
			//"LEFT JOIN " + db + ".roi r_adj ON f.roi_id_adj = r.id " +
			"WHERE f.stim_seq_id = " + stimSeqId + " " +
			"ORDER BY f.t"
		);
		
		StringBuffer res = new StringBuffer("{outcome:true,fix:[");
		while (rsF.next()) {
			res.append(
				"{" +
				"id:"            + rsF.getInt("id") + "," +
				"t:"             + rsF.getInt("t") + "," +
				"dur:"           + rsF.getInt("dur") + "," +
				"p:{x:"          + rsF.getFloat("x") + ",y:" + rsF.getFloat("y") + "}," +
				"pAdj:{x:"       + rsF.getFloat("x_adj") + ",y:" + rsF.getFloat("y_adj") + "}," +
				"saccInROI:"     + rsF.getString("sacc_i_roi") + "," +
				"saccInROIAdj:"  + rsF.getString("sacc_i_roi_adj") + "," +
				"saccOutROI:"    + rsF.getString("sacc_o_roi") + "," +
				"saccOutROIAdj:" + rsF.getString("sacc_o_roi_adj") + "," +
				"saccInLet:"     + rsF.getString("sacc_i_let") + "," +
				"saccInLetAdj:"  + rsF.getString("sacc_i_let_adj") + "," +
				"saccOutLet:"    + rsF.getString("sacc_o_let") + "," +
				"saccOutLetAdj:" + rsF.getString("sacc_o_let_adj") + "," +
				"roiNum:"        + rsF.getString("roi_num") +
				"}" + (!rsF.isLast() ? "," : "")
			);
		}
		rsF.close();
		res.append("],blinks:[");
		
		// (2) Blinks:
		ResultSet rsB = dbi.execQry(
			"SELECT b.id, b.sx, b.sy, b.ex, b.ey, b.t, b.dur, b.sacc_dur, b.sacc_amp, b.sacc_pv " +
			"FROM " + dbG + ".blink b " +
			"WHERE b.stim_seq_id = " + stimSeqId + " " +
			"ORDER BY b.t"
		);
		
		while (rsB.next()) {
			res.append(
				"{" +
				"id:" + rsB.getInt("id") + "," +
				"p1:{x:" + rsB.getFloat("sx") + ",y:" + rsB.getFloat("sy") + "}," +
				"p2:{x:" + rsB.getFloat("ex") + ",y:" + rsB.getFloat("ey") + "}," +
				"dur:" + rsB.getInt("dur") + "," +
				"sacc:{dur:" + rsB.getInt("sacc_dur") + ",amp:" + rsB.getFloat("sacc_amp") + ",pv:" + rsB.getInt("sacc_pv") + "}" +
				"}" +
				(!rsB.isLast() ? "," : "")
			);
		}
		rsB.close();
		res.append("],xform:[");
		
		// (3) Transformations:
		ResultSet rsX = dbi.execQry(
			"SELECT ts, tx, ty, sx, sy, a " +
			"FROM " + dbG + ".log_xform l " +
			"WHERE l.stim_seq_id = " + stimSeqId + " " +
			"ORDER BY id"
		);
		
		while (rsX.next()) {
			res.append("\"" + rsX.getString("ts") + " : tx=" + rsX.getFloat("tx") + ", ty=" + rsX.getFloat("ty") + ", sx=" + rsX.getFloat("sx") + ", sy=" + rsX.getFloat("sy") + ", a=" + rsX.getFloat("a") + "\"" + (!rsX.isLast() ? "," : ""));
		}
		rsX.close();
		res.append("]}");
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public void getImg(HttpServletResponse res, String db, String obj, int objId, String attr, String type)  throws SQLException, IOException {
		Statement st = conn.get().createStatement();
		ResultSet rs = st.executeQuery("SELECT \"" + attr + "\" AS img FROM " + db + ".\"" + obj + "\" WHERE id = " + objId);
		rs.next();
		InputStream is = rs.getBinaryStream("img");
		
		res.setContentType("image/" + type);
		BufferedOutputStream out = new BufferedOutputStream(res.getOutputStream());
		byte b[] = new byte[is.available()];
		int idx = is.read(b, 0, is.available());
		out.write(b, 0, idx);
		out.flush();
		
		rs.close();
		st.close();
	}
	
	
	// ---------------------------------------------------------------------------------
	public void getStimImg(HttpServletResponse res, String username, int stimId)  throws SQLException, IOException {
		String db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		
		Statement st = conn.get().createStatement();
		ResultSet rs = st.executeQuery("SELECT img FROM " + db + ".stim WHERE id = " + stimId);
		rs.next();
		InputStream is = rs.getBinaryStream("img");
		
		res.setContentType("image/jpeg");
		BufferedOutputStream out = new BufferedOutputStream(res.getOutputStream());
		byte b[] = new byte[is.available()];
		int idx = is.read(b, 0, is.available());
		out.write(b, 0, idx);
		out.flush();
		
		rs.close();
		st.close();
	}
	
	
	// ---------------------------------------------------------------------------------
	public static long getLong(DBI dbi, String qry, String col)  throws SQLException {
		long res = ID_NONE;
		Statement st = dbi.getConn().get().createStatement();
		ResultSet rs = st.executeQuery(qry);
		
		if (rs.next()) res = rs.getLong(col);
		
		rs.close();
		st.close();
		
		return res;
	}
	
	
	// ---------------------------------------------------------------------------------
	public static long getLong(Statement st, String qry, String col)  throws SQLException {
		long res = ID_NONE;
		ResultSet rs = st.executeQuery(qry);
		if (rs.next()) res = rs.getLong(col);
		rs.close();
		return res;
	}
	
	
	// ---------------------------------------------------------------------------------
	public static boolean isNull(DBI dbi, String qry)  throws SQLException {
		boolean res = false;
		Statement st = dbi.getConn().get().createStatement();
		ResultSet rs = st.executeQuery("SELECT (" + qry + ") IS NULL");
		
		if (rs.next()) res = rs. getBoolean(0);
		
		rs.close();
		st.close();
		
		return res;
	}
	
	
	// ---------------------------------------------------------------------------------
	@SuppressWarnings("deprecation")
	public String getProfile(String username)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		Statement st = conn.get().createStatement();
		ResultSet rs = st.executeQuery("SELECT pseudo_passwd, email, login_cnt, last_login FROM \"" + Do.DB_MAIN + "\".\"user\" WHERE username = '" + username + "'");
		rs.next();
		
		String res =
			"{" +
			  "username:\"" + username + "\"," +
			  "passwd:\"" + rs.getString("pseudo_passwd") + "\"," +
			  "email:\"" + rs.getString("email") + "\"," +
			  "loginCnt:" + rs.getString("login_cnt") + "," +
			  "lastLogin:" + (rs.getTimestamp("last_login").getYear() == 0 ? "null" : "new Date(" + rs.getTimestamp("last_login").getTime() + ")") + "," +
			  "apps:{" +
			    "data:{" +
			      "dbSize:" + app.data.DB.getSize(this, username) + "," +
			      "dsCnt:" + DataSet.getCnt(this, username) +
			    "}," +
			    "gaze:{" +
			      "dbSize:" + app.gaze.DB.getSize(this, username) + "," +
			      "expCnt:" + Exp.getCnt(this, username) +
			    "}" +
			  "}" +
			"}";
		
		rs.close();
		st.close();
		
		return res;
	}
	
	
	// ---------------------------------------------------------------------------------
	static public int getRecCnt(ResultSet rs)  throws SQLException {
		rs.last();
		int cnt = rs.getRow();
		rs.beforeFirst();
		return cnt;
	}
	
	
	// ---------------------------------------------------------------------------------
	public static String getStr(DBI dbi, String qry, String col)  throws SQLException {
		String res = null;
		Statement st = dbi.getConn().get().createStatement();
		ResultSet rs = st.executeQuery(qry);
		
		if (rs.next()) res = rs.getString(col);
		
		rs.close();
		st.close();
		
		return res;
	}
	
	
	// ---------------------------------------------------------------------------------
	public static String getStr(Statement st, String qry, String col)  throws SQLException {
		String res = null;
		ResultSet rs = st.executeQuery(qry);
		if (rs.next()) res = rs.getString(col);
		rs.close();
		
		return res;
	}
	
	
	// ---------------------------------------------------------------------------------
	public static List<String> getStrLst(DBI dbi, String qry, String col)  throws SQLException {
		List<String> res = new ArrayList<String>();
		Statement st = dbi.getConn().get().createStatement();
		ResultSet rs = st.executeQuery(qry);
		
		while (rs.next()) res.add(rs.getString(col));
		
		rs.close();
		st.close();
		
		return res;
	}
	
	
	// ---------------------------------------------------------------------------------
	public static List<String> getStrLst(Statement st, String qry, String col)  throws SQLException {
		List<String> res = new ArrayList<String>();
		ResultSet rs = st.executeQuery(qry);
		while (rs.next()) res.add(rs.getString(col));
		rs.close();
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/*
	 * Returns size of the given table in [B].
	 * 
	 * The reason this method does not use the information from the 'information_schema' schema is that the table 
	 * may (and most of the time is) being changed inside a transaction. What that means, is that the new data 
	 * length will be available until after the transaction has committed. And most of the time, I want to update the 
	 * size inside of the table inside the very transaction that is changing it. For completeness, below is how the 
	 * other way should be done:
	 * 
	 *   dbi.getLong(null, "SELECT SUM(data_length + index_length) AS size FROM information_schema.TABLES WHERE table_schema = '" + db + "' AND table_name = '" + tbl + "'", "size");
	 * 
	 * Neither 'db' nor 'tbl' should have quotes or apostrophies around them.
	 * 
	 * TODO:
	 *   - count indeces (don't think it's possible for tables with sequential numerical primary keys)
	 */
	public static long getTblSize(DBI dbi, String db, String tbl)  throws SQLException {
		return DBI.getLong(dbi, "SELECT pg_total_relation_size('" + db + "." + tbl + "') AS size", "size");
		
		/*
		int size = 0;
		int rowCnt = (int) dbi.getLong(null, "SELECT COUNT(*) AS cnt FROM \"" + db + "\".\"" + tbl + "\"", "cnt");
		
		ResultSet rsCol = dbi.execQry("SELECT column_name, data_type FROM information_schema.columns WHERE table_schema = '" + db + "' AND table_name = '" + tbl + "';");
		while (rsCol.next()) {
			String type = rsCol.getString("data_type");
			
			if (type.equals("int")) size += FIELD_SIZE_INT * rowCnt;
			else if (type.equals("bigint")) size += FIELD_SIZE_BIGINT * rowCnt;
			else if (type.equals("float")) size += FIELD_SIZE_FLOAT * rowCnt;
			else if (type.equals("double")) size += FIELD_SIZE_DOUBLE * rowCnt;
			else if (type.equals("tinyint")) size += FIELD_SIZE_TINY_INT * rowCnt;
			else if (type.equals("tinyint")) size += FIELD_SIZE_TINY_INT * rowCnt;
			else if (type.equals("date")) size += FIELD_SIZE_DATE * rowCnt;
			else if (type.equals("time")) size += FIELD_SIZE_TIME * rowCnt;
			else if (type.equals("datetime")) size += FIELD_SIZE_DATETIME * rowCnt;
			else if (type.equals("timestamp")) size += FIELD_SIZE_TIMESTAMP * rowCnt;
			
			else if (type.equals("varchar")) size += FIELD_SIZE_VARCHAR + dbi.getLong(null, "SELECT SUM(COALESCE(LENGTH(\"" + rsCol.getString("column_name") + "\"),0)) AS sum FROM \"" + db + "\".\"" + tbl + "\"", "sum");
			else if (type.equals("text")) size += FIELD_SIZE_TEXT + dbi.getLong(null, "SELECT SUM(COALESCE(LENGTH(\"" + rsCol.getString("column_name") + "\"),0)) AS sum FROM \"" + db + "\".\"" + tbl + "\"", "sum");
			else if (type.equals("blob")) size += FIELD_SIZE_BLOB + dbi.getLong(null, "SELECT SUM(COALESCE(LENGTH(\"" + rsCol.getString("column_name") + "\"),0)) AS sum FROM \"" + db + "\".\"" + tbl + "\"", "sum");
		}
		rsCol.close();
		
		return size;
		*/
	}
	
	
	// ---------------------------------------------------------------------------------
	public static String getWhere(int expId, String stimIds, String roiIds, String subIds, String trialIds, String other) {
		List<String> l = new ArrayList<String>();  // list of conditions
		
		l.add("e.id = " + expId);
		if (stimIds != null && stimIds.length() > 0) l.add("s1.id IN (" + stimIds + ")");
		if (roiIds != null && roiIds.length() > 0) l.add("r.id IN (" + roiIds + ") ");
		if (subIds != null && subIds.length() > 0) l.add("s2.id IN (" + subIds + ") ");
		if (trialIds != null && trialIds.length() > 0) l.add("t.id IN (" + trialIds + ") ");
		if (other != null && other.length() > 0) l.add(other);
		
		return (l.size() == 0 ? "" : "WHERE " + $.join(l.toArray(new String[0]), "", "", " AND ", false, false).toString() + " \n");
	}
	
	
	// ---------------------------------------------------------------------------------
	public Map<String,Word> getWords()  throws SQLException {
		Map<String,Word> mapWord = new HashMap<String,Word>();
		
		// Below, I use the MAX function for these columns which are the same for several word instances. I do that for 
		// performance reasons -- to avoid unnecessary averaging and rounding.
		String qry = 
			"SELECT " +
			
			"word, " +
			"COUNT(*) AS mrc_n, " +
			
			"MAX(nlet) AS nlet, " +  // ROUND(AVG(mrc_fam),2)
			"MAX(mrc_nphon) AS mrc_nphon, " +
			"MAX(mrc_nsyl) AS mrc_nsyl, " +
			
			"MAX(mrc_kffrq) AS mrc_kffrq, " +
			"ROUND(ln(MAX(mrc_kffrq))::numeric,2) AS mrc_kffrq_ln, " +  // MySQL: "ln(SUM(mrc_kffrq)) AS mrc_kffrq_ln, "
			"ROUND(log(MAX(mrc_kffrq))::numeric,2) AS mrc_kffrq_log10, " +  // CASE MAX(mrc_kffrq) WHEN null THEN null ELSE ROUND(log(MAX(mrc_kffrq))::numeric,2) END AS mrc_kffrq_log10
			
			"MAX(mrc_kfcat) AS mrc_kfcat, " +
			"MAX(mrc_kfsmp) AS mrc_kfsmp, " +
			
			"MAX(mrc_tlfrq) AS mrc_tlfrq, " +
			"ROUND(ln(MAX(mrc_tlfrq))::numeric,2) AS mrc_tlfrq_ln, " +
			"ROUND(log(MAX(mrc_tlfrq))::numeric,2) AS mrc_tlfrq_log10, " +
			
			"MAX(mrc_bfrq) AS mrc_bfrq, " +
			"ROUND(ln(MAX(mrc_bfrq))::numeric,2) AS mrc_bfrq_ln, " +
			"ROUND(log(MAX(mrc_bfrq))::numeric,2) AS mrc_bfrq_log10, " +
			
			"MAX(mrc_fam) AS mrc_fam, " +
			"MAX(mrc_cnc) AS mrc_cnc, " +
			"MAX(mrc_img) AS mrc_img, " +
			"MAX(mrc_meanc) AS mrc_meanc, " +
			"MAX(mrc_meanp) AS mrc_meanp, " +
			"MAX(mrc_aoa) AS mrc_aoa, " +
			
			"MAX(subtl_frq) AS subtl_frq, " + 
			"ROUND(ln(MAX(subtl_frq))::numeric,2) AS subtl_frq_ln, " +
			"ROUND(log(MAX(subtl_frq))::numeric,2) AS subtl_frq_log10 " +
			
			/*
			"mrc_tq2, " +
			"mrc_type, " +
			"mrc_pdtype, " +
			"mrc_alphsyl, " +
			"mrc_status, " +
			"mrc_var, " +
			"mrc_cap, " +
			"mrc_plur, " +
			"mrc_phon, " +
			"mrc_dphon, " +
			"mrc_stress " +
			*/
			
			//"FROM \"" + Do.DB_GAZE + "\".word WHERE word NOT LIKE '&%' AND word NOT LIKE '''%' AND word NOT LIKE '-%' AND word NOT LIKE '\\_%'GROUP BY word"
			"FROM \"" + Do.DB_GAZE + "\".word WHERE word NOT LIKE '&%' AND word NOT LIKE '''%' AND word NOT LIKE '-%' GROUP BY word"
			//"FROM \"" + Do.DB_GAZE + "\".word GROUP BY word"
			;
		//System.out.println(qry);
		
		ResultSet rs = execQry(qry);
		while (rs.next()) { mapWord.put(rs.getString("word"), new Word(rs)); }
		rs.close();
		
		return mapWord;
	}
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * Checkes if the 'qrySel' returns an id. If yes, it returns it. Otherwise, it runs 
	 * the 'qryIns' and returns the generated id. 'qrySel' should be a SELECT query 
	 * returning a single integer ('id') and 'qryIns' should be an INSERT query.
	 */
	public static int insertGetId(Statement stX, String qrySel, String qryIns)  throws SQLException {
		int res = (qrySel == null ? ID_NONE : (int) DBI.getLong(stX, qrySel, "id"));
		
		if (res == ID_NONE) {
			stX.executeUpdate(qryIns, Statement.RETURN_GENERATED_KEYS);
			ResultSet rs = stX.getGeneratedKeys();
			rs.next();
			res = rs.getInt(1);
			rs.close();
		}
		
		return res;
	}
	
	
	// - - - -
	public static int insertGetId(DBI dbi, String qrySel, String qryIns)  throws SQLException {
		Statement st = dbi.newSt();
		int res = insertGetId(st, qrySel, qryIns);
		st.close();
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Checks if the specified pair of credentials exists and if yes it returns all 
	 * experiments, subjects, trials, and the user's profile in JSON.
	 */
	public StringBuffer login(String username, String passwd, boolean auto, Integer expId)  throws SQLException, UnsupportedEncodingException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		StringBuffer res = new StringBuffer();
		
		boolean userExists = getLong(this, "SELECT id FROM \"" + Do.DB_MAIN + "\".\"user\" WHERE username = '" + username + "' AND " + (auto ? "pseudo_passwd" : "passwd") + " = '" + passwd + "'", "id") != ID_NONE;
		if (userExists) {
			res.append("{outcome:true,data:{profile:" + getProfile(username) + ",prj:" + Prj.getLst(this, username) + "}}");
			
			Statement st = conn.get().createStatement();
			st.executeUpdate("UPDATE \"" + Do.DB_MAIN + "\".\"user\" SET login_cnt = login_cnt + 1");
			st.executeUpdate("UPDATE \"" + Do.DB_MAIN + "\".\"user\" SET last_login = CURRENT_TIMESTAMP");
			st.close();
		}
		else res.append("{outcome:false,msg:\"" + (auto ? "Auto login unsuccessful. Please log in." : "The username-password combination provided is incorrect.") + "\"}");
		
		return res;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Statement newSt()  throws SQLException {
		return conn.get().createStatement();
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public String qry2json(Statement st, String qry)  throws SQLException {
		List<String> res = new ArrayList<String>();
		boolean newSt = st == null;
		
		if (newSt) st = conn.get().createStatement();
		ResultSet rs = st.executeQuery(qry);
		
		while (rs.next()) {
			StringBuffer o = new StringBuffer();
			o.append("{");
			for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
				boolean isStr =
					rs.getMetaData().getColumnType(i) == java.sql.Types.CHAR ||
					rs.getMetaData().getColumnType(i) == java.sql.Types.CLOB   ||
					rs.getMetaData().getColumnType(i) == java.sql.Types.VARCHAR;// ||
					//rs.getMetaData().getColumnType(i) == java.sql.Types.NCHAR ||
					//rs.getMetaData().getColumnType(i) == java.sql.Types.NVARCHAR;
				String k = rs.getMetaData().getColumnName(i);
				String v = (
					isStr
						? "\"" + JSI.str2js(rs.getString(i)) + "\""
						: rs.getString(i)
				);
				o.append(k + ":" + v + (i < rs.getMetaData().getColumnCount() ? "," : ""));
			}
			o.append("}");
			res.add(o.toString());
		}
		
		rs.close();
		if (newSt) st.close();
		
		return (res.size() == 1 ? res.get(0) : "");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * A general routine for setting a value of a given column ('attrName') in a given row ('objId') of a given table 
	 * ('objName'). This method handles simple value setting itself and dispatches requests for complex value setting, 
	 * e.g. a request to change a variable's type which may require additional operations.
	 * 
	 * All data integrity validation should come from the underlying data structure (i.e., the database) constraints 
	 * (e.g., unique values, range checks, etc.). Some are checked for below, for perormance reasons.
	 */
	public static Result attrSet(DBI dbi, String username, String app, String objName, String objHumanName, int objId, String attrName, String attrVal)  throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		DBI dbiX = new DBI();
		Statement stX = dbiX.transBegin();
		
		if (app.equals(Do.APP_ID_STR_DATA) ) {
			stX.execute("SET search_path TO \"" + Do.DB_DATA + "-" + username + "\"");
		}
		else if (app.equals(Do.APP_ID_STR_GAZE) ) {
			stX.execute("SET search_path TO \"" + Do.DB_GAZE + "-" + username + "\"");
		}
		else if (app.equals(Do.APP_ID_STR_MAIN) ) {
			stX.execute("SET search_path TO \"" + Do.DB_MAIN + "\"");
		}
		
		try {
			// Trial name:
			if (objName.equalsIgnoreCase("trial") && attrName.equalsIgnoreCase("name")) {
				int expId = (int) getLong(stX, "SELECT e.id FROM exp e INNER JOIN sub s ON s.exp_id = e.id INNER JOIN trial t ON t.sub_id = s.id WHERE t.id = " + objId, "id");
				String currName = getStr(stX, "SELECT name FROM trial WHERE id = " + objId, "name");
				return setTrialName(stX, username, expId, currName, attrVal);
			}
			
			// Variable type:
			if (objName.equalsIgnoreCase("var") && attrName.equalsIgnoreCase("type")) {
				return Var.setType(dbiX, username, stX, objId, attrVal);
			}
			
			// Other:
			stX.executeUpdate("UPDATE \"" + objName + "\" SET \"" + attrName + "\" = " + attrVal + " WHERE id = " + objId);
			DBI.transCommit(stX, true);
			
			return new Result(true, "");
		}
		catch (SQLException e) {
			DBI.transRollback(stX, true);
			
			String msg = "";
			if (e.getErrorCode() == SQL_ERR_DUP_ENTRY) {
				msg = "msg:\"" + objHumanName.substring(0,1).toUpperCase() + objHumanName.substring(1) + " with that value of the '" + attrName + "' attribute already exists. Values of that attribute must be unique.\"";
			}
			else throw e;
			
			return new Result(false, msg);
		}
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * 'st' is assumed to have the '"SET search_path TO " + db' query executed on it.
	 */
	private static Result setTrialName(Statement stX, String username, int expId, String currName, String newName)  throws SQLException {
		if (getLong(stX, "SELECT COUNT(*) AS cnt FROM trial WHERE name = '" + newName + "' AND sub_id IN (SELECT id FROM sub WHERE exp_id = " + expId + ")", "cnt") == 0) {
			stX.executeUpdate(
				"UPDATE trial " +
				"SET name = " + newName + " " +
				"WHERE " +
				"  name = '" + currName + "' AND " +
				"  sub_id IN (SELECT id FROM sub WHERE exp_id = " + expId + ")"
			);
			return new Result(true, "");
		}
		else {
			return new Result(false, "msg:\"Trial with that value of the 'name' attribute already exists. Values of that attribute must be unique.");
		}
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/**
	 * Passing empty or null 'val' will result in the variable value deleted from the 
	 * 'xr_var_X' table.
	 */
	public String setVar(String username, String objName, int objId, int varId, String val)  throws SQLException {
		String db = "\"" + Do.DB_GAZE + "-" + username + "\"";
		
		Statement st = conn.get().createStatement();
		st.execute("SET search_path TO " + db);
		
		boolean valEmpty = (val == null || (val != null && (val.length() == 0 || val.equals("''"))));
		int id = (int) getLong(st, "SELECT id FROM xr_var_" + objName + " WHERE var_id = " + varId + " AND " + objName + "_id = " + objId, "id");
		
		if (id == ID_NONE) {
			if (valEmpty) return "{outcome:false,msg:\"Variable does not exist.\"}";
			
			st.executeUpdate("INSERT INTO xr_var_" + objName + "(var_id, " + objName + "_id, val) VALUES (" + varId + ", " + objId + ", " + val + ")");
			id = (int) getLong(st, "SELECT id FROM xr_var_" + objName + " WHERE var_id = " + varId + " AND " + objName + "_id = " + objId, "id");
		}
		else {
			if (valEmpty) {
				st.executeUpdate("DELETE FROM xr_var_" + objName + " WHERE var_id = " + varId + " AND " + objName + "_id = " + objId);
			}
			else {
				st.executeUpdate("UPDATE xr_var_" + objName + " SET val = " + val + " WHERE var_id = " + varId + " AND " + objName + "_id = " + objId);
			}
		}
		
		return "{outcome:true,id:" + id + "}";
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public Statement transBegin()  throws SQLException {
		conn.get().setAutoCommit(false);
		Statement st = conn.get().createStatement();
		return st;
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static void transCommit(Statement st, boolean closeConn)  throws SQLException {
		Connection c = st.getConnection();
		st.close();
		c.commit();
		c.setAutoCommit(true);
		if (closeConn) c.close();
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public static void transRollback(Statement st, boolean closeConn)  throws SQLException {
		Connection c = st.getConnection();
		st.close();
		c.rollback();
		c.setAutoCommit(true);
		if (closeConn) c.close();
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	public void vacuum()  throws SQLException {
		conn.get().createStatement().execute("VACUUM ANALYZE");
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------
	/*
	public String appComplementVars(String username, int expId)  throws SQLException {
		return "{outcome:false,msg:\"This command has been removed.\"}";
		PreparedStatement psAddX = null;
		
		conn.get().setAutoCommit(false);
		try {
			Statement stVar = conn.get().createStatement();
			ResultSet rsVar = stVar.executeQuery("SELECT id, name, obj_name, def_val FROM " + db + ".var WHERE exp_id = " + expId);
			while (rsVar.next()) {
				//System.out.println("*** " + rsVar.getString("name") + ":");
				int varId = rsVar.getInt("id");
				String o = rsVar.getString("obj_name");
				
				psAddX = conn.get().prepareStatement("INSERT INTO " + db + ".xr_var_" + o + " (var_id, " + o + "_id, val) VALUES (" + varId + ", ?, '" + rsVar.getString("def_val") + "')");
				
				String qryX = "";
				if (o.equals("roi")) {
					qryX =
						"SELECT r.id FROM " + db + ".roi r " +
						"LEFT JOIN " +
						"( " +
						"SELECT r.id, x.val FROM " + db + ".roi r " +
						"INNER JOIN " + db + ".stim s ON s.id = r.stim_id " +
						"LEFT JOIN " + db + ".xr_var_roi x ON x.roi_id = r.id " +
						"LEFT JOIN " + db + ".var v ON v.id = x.var_id " +
						"WHERE s.exp_id = " + expId + " AND v.id = " + varId + " " +
						") tmp ON tmp.id = r.id " +
						"INNER JOIN " + db + ".stim s ON s.id = r.stim_id " +
						"WHERE s.exp_id = " + expId + " AND tmp.id IS NULL";
				}
				else if (o.equals("stim") || o.equals("sub")) {
					qryX =
						"SELECT o.id FROM " + db + "." + o + " o " +
						"LEFT JOIN " +
						"( " +
						"SELECT o.id, x.val FROM " + db + "." + o + " o " +
						"LEFT JOIN " + db + ".xr_var_" + o + " x ON x." + o + "_id = o.id " +
						"LEFT JOIN " + db + ".var v ON v.id = x.var_id " +
						"WHERE o.exp_id = " + expId + " AND v.id = " + varId + " " +
						") tmp ON tmp.id = o.id " +
						"WHERE o.exp_id = " + expId + " AND tmp.id IS NULL";
				}
				
				//System.out.println(qryX + "\n");
				
				Statement stX = conn.get().createStatement();
				ResultSet rsX = stX.executeQuery(qryX);
				while (rsX.next()) {
					psAddX.setInt(1, rsX.getInt("id"));
					psAddX.execute();
				}
				rsX.close();
				stX.close();
				
				psAddX.close();
			}
			rsVar.close();
			stVar.close();
			
			conn.get().commit();
			conn.get().setAutoCommit(true);
		}
		catch (SQLException e) {
			psAddX.close();
			conn.get().rollback();
			conn.get().setAutoCommit(true);
			throw e;
		}
		
		return "{outcome:true," + getDBSize(db) + "}";
	}
	*/
}