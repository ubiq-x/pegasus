package inter;


import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import main.Do;


/**
 * @author Tomek D. Loboda
 */
public class FSI {
	public static final int BUF_SIZE = 8192;
	
	
	// ---------------------------------------------------------------------------------
	public static boolean addDir(String path) {
		File dir = new File(path);
		if (dir.exists()) return dir.isDirectory();
		return dir.mkdir();
	}
	
	
	// ---------------------------------------------------------------------------------
	public static void addFile(String path, String s)  throws IOException {
		if (s == null || (s != null && s.length() == 0)) return;
		BufferedWriter out = new BufferedWriter(new FileWriter(path));
		out.write(s);
		out.close();
	}
	
	
	// ---------------------------------------------------------------------------------
	public static boolean cleanDir(String path) {
		File dir = new File(path);
		if (!dir.isDirectory()) return false;
		
		for (File f: dir.listFiles()) f.delete();
		
		return true;
	}
	
	
	// ---------------------------------------------------------------------------------
	public static boolean delDir(File d, boolean recursive) {
		File[] F = d.listFiles();
		
		if (!recursive && F.length > 0) return false;
		
		for (File f: F) {
			if (f.isDirectory()) {
				delDir(f, true);
				f.delete();
			}
			else f.delete();
		}
		d.delete();
		
		return true;
	}
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * Assumes the file contains key-values pairs delimited with 'delim' string. It 
	 * returns a hashtable corresponding to those pairs.
	 */
	public static Hashtable<String, String> file2hash(String path, String delim)  throws IOException {
		Hashtable<String, String> res = new Hashtable<String, String>();
		
		//BufferedReader in = new BufferedReader(new FileReader(path));
		BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(path), "UTF8"));
		String l = null;
		while ((l = in.readLine()) != null && l.length() > 0) {
			String [] kv = l.split(delim);
			if (kv.length == 2) res.put(kv[0], kv[1]);
		}
		in.close();
		
		return res;
	}
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * Returns the content of the file as a List.
	 */
	public static List<String> file2lst(String path)  throws IOException {
		List<String> lst = new ArrayList<String>();
		
		//BufferedReader in = new BufferedReader(new FileReader(path));
		BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(path), "UTF8"));
		String l = null;
		while ((l = in.readLine()) != null) {
			lst.add(l);
		}
		in.close();
		
		return lst;
	}
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * Returns the content of the file as a List, skipping the specified amount of 
	 * lines from its beginning. Each line is split on the specified delimiter and thus 
	 * each element of the returned List is an String array of tokens.
	 */
	public static List<String[]> file2lstSplit(String filename, String delim, int skipLineCnt)  throws IOException {
		List<String[]> lst = new ArrayList<String[]>();
		
		//BufferedReader in = new BufferedReader(new FileReader(filename));
		BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(filename), "UTF8"));
		
		for (int i = 0; i < skipLineCnt; i++) {
			in.readLine();
		}
		
		String l = null;
		while ((l = in.readLine()) != null && l.length() > 0) {
			String[] A = l.split(delim);
			lst.add(A);
		}
		
		in.close();
		
		return lst;
	}
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * Returns the content of the file as a List, excluding lines not matching the 
	 * specified RegExpr.
	 */
	public static List<String> file2lstRegExpr(String path, Pattern pattern)  throws IOException {
		List<String> lst = new ArrayList<String>();
		
		/*
		// An alternative solution (not tested):
		FileInputStream fis = new FileInputStream(filename);
		FileChannel fc = fis.getChannel();
		ByteBuffer bbuff = fc.map(FileChannel.MapMode.READ_ONLY, 0, (int)fc.size());
		CharBuffer cbuff = Charset.forName("8859_1").newDecoder().decode(bbuff);
        
		Matcher matcher = pattern.matcher(cbuff);
		while (matcher.find()) {
			lst.add(matcher.group());
		}
		*/
		
		//BufferedReader in = new BufferedReader(new FileReader(path));
		BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(path), "UTF8"));
		
		Matcher m = pattern.matcher("");
		
		String l = null;
		while ((l = in.readLine()) != null) {
			m.reset(l);
			if (m.find()) {
				lst.add(l);
			}
		}
		
        in.close();
		
		return lst;
	}
	
	
	// ---------------------------------------------------------------------------------
	public static List<String> file2lstRegExpr(String path, String pattern)  throws IOException {
		Pattern p = Pattern.compile(pattern);
		return file2lstRegExpr(path, p);
	}
	
	
	// ---------------------------------------------------------------------------------
	public static String getExt(File f) {
		String n = f.getName();
		int lastDotIdx = n.lastIndexOf('.');
		return (lastDotIdx == -1 ? "" : n.substring(lastDotIdx+1));
	}
	
	
	// ---------------------------------------------------------------------------------
	public static String getFilename(File f) {
		String n = f.getName();
		int lastDotIdx = n.lastIndexOf('.');
		return (lastDotIdx == -1 ? n : n.substring(0, lastDotIdx));
	}
	
	
	// ---------------------------------------------------------------------------------
	public static StringBuffer getFiles(String[] F)  throws IOException {
		StringBuffer res = new StringBuffer();
		for (String f: F) {
			String path = Do.getPathApp() + File.separatorChar + f;
			//BufferedReader in = new BufferedReader(new FileReader(path));
			BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(path), "UTF8"));
			
			String l = null;
			while ((l = in.readLine()) != null) {
				res.append(l + "\n");
			}
			in.close();
		}
		return res;
	}
	
	
	// ---------------------------------------------------------------------------------
	public static StringBuilder getStr(String path)  throws IOException {
		if (!(new File(path)).exists()) return null;
		
		StringBuilder res = new StringBuilder();
		//BufferedReader in = new BufferedReader(new FileReader(path));
		BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(path), "UTF8"));
		int lnCnt = 0;
		String l = null;
		while ((l = in.readLine()) != null) {
			if (lnCnt++ > 0) res.append("\n");
			res.append(l);
		}
		in.close();
		
		return res;
	}
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * Returns the given portion (line, column, character count) of the given file.
	 */
	public static String getStr(String path, int line, int col, int len) throws IOException {
		//BufferedReader in = new BufferedReader(new FileReader(path));
		BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(path), "UTF8"));
		for (int i = 0; i < line-1; i++) {
			in.readLine();
		}
		String l = in.readLine();
		in.close();
		
		return l.substring(col-1, Math.min(col-1+len, l.length()));
	}
	
	
	// ---------------------------------------------------------------------------------
	public static File getTmpWorkDir(String prefix, String postfix)  throws IOException {
		File dir = File.createTempFile(prefix, postfix, new File(Do.getPathWork()));
		dir.delete();
		dir.mkdir();
		return dir;
	}
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * Returns the listing of the given directory in JSON format.
	 */
	public static String ls(String path) {
		String s = "[";
		File dir = new File(path);
		File[] F = dir.listFiles();
		for (File f: F) {
			s += "{name:\"" + f.getName() + "\",size:" + f.length() + "},";
		}
		s += "]";
		
		return s;
	}
	
	
	// ---------------------------------------------------------------------------------
	public static File mkdir(File parent, String name) {
		File d = new File(parent, name);
		d.mkdir();
		return d;
	}
	
	
	// ---------------------------------------------------------------------------------
	public static File mkdir(String path, String name) {
		return mkdir(new File(path), name);
	}
	
	
	// ---------------------------------------------------------------------------------
	/*
	public static void saveImg(String path, String base64) {
		try {
			base64 = base64.substring(base64.indexOf(',')+1);  // skipping: data:image/png;base64,
			base64 = base64.replace(" ", "+");  // reversing transmission translation
			
			BASE64Decoder d = new BASE64Decoder();
			byte[] data = d.decodeBuffer(base64.substring(base64.indexOf(',')+1));
			
			FileOutputStream os = new FileOutputStream(path);
			os.write(data);
			os.close();
		}
		catch (IOException e) {}
	}
	*/
	
	
	// ---------------------------------------------------------------------------------
	/**
	 * Unzips the file into a temporary directory and returns the reference to it.
	 */
	public static File unzip(String path)  throws IOException {
		File dir = getTmpWorkDir("import-", "");
		
		InputStream in = new BufferedInputStream(new FileInputStream(path));
		ZipInputStream zin = new ZipInputStream(in);
		ZipEntry e;
		
		while ((e = zin.getNextEntry()) != null) {
			String pathname = dir.getAbsolutePath() + File.separatorChar + e.getName();
			if (e.isDirectory()) (new File(pathname)).mkdir();
			else {
				FileOutputStream out = new FileOutputStream(pathname);
				byte [] b = new byte[512];
				int len = 0;
				while ((len = zin.read(b)) != -1) {
					out.write(b, 0, len);
				}
				out.close();
			}
		}
		zin.close();
		return dir;
	}
	
	
	// ---------------------------------------------------------------------------------
	public static byte[] zipDir(File dir, int compLvl)  throws IOException {
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		ZipOutputStream zos = new ZipOutputStream(bout);
		zos.setLevel(compLvl);
		
		zipDir_rec(dir, zos);
		
		zos.close();
		bout.close();
		
		return bout.toByteArray();
	}
	
	
	// ---------------------------------------------------------------------------------
	// src: http://www.devx.com/tips/Tip/14049
	private static void zipDir_rec(File dir, ZipOutputStream zos)  throws IOException {
		String[] dirList = dir.list();
		byte[] buff = new byte[4096];
		int byteCnt = 0;
		for (int i = 0; i < dirList.length; i++) {
			File f = new File(dir, dirList[i]);
			if (f.isDirectory()) {
				zipDir_rec(new File(f.getPath()), zos);
				continue;
			}
			FileInputStream fis = new FileInputStream(f);
			ZipEntry anEntry = new ZipEntry(f.getPath());
			zos.putNextEntry(anEntry);
			while ((byteCnt = fis.read(buff)) != -1) {
				zos.write(buff, 0, byteCnt);
			}
			fis.close(); 
		}
	}
	
	
	// ---------------------------------------------------------------------------------
	// src: http://blog.csdn.net/Pockey/archive/2009/03/16/3994462.aspx
	/*
	public static byte[] zipDir(File dir, int compLvl)  throws IOException {
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		ZipOutputStream zos = new ZipOutputStream(bout);
		zos.setLevel(compLvl);
		byte[] bytes = new byte[300000];
		
		String[] filenames = dir.list();
		for (int i = 0; i < filenames.length; ++i) {
			FileInputStream fis = new FileInputStream(dir.getAbsolutePath() + File.separatorChar + filenames[i]);
			BufferedInputStream bis = new BufferedInputStream(fis);
			
			zos.putNextEntry(new ZipEntry(filenames[i]));
			int bytesCount = -1;
			while ((bytesCount = bis.read(bytes)) != -1) {
				zos.write(bytes, 0, bytesCount);
			}
			zos.closeEntry();
			bis.close();
			fis.close();
		}
		zos.flush();
		bout.flush();
		
		zos.close();
		bout.close();
		
		return bout.toByteArray();
	}
	*/
	
	
	// =================================================================================
	public static class FileFilterExact implements java.io.FileFilter {
		private final String name;
		
		public FileFilterExact(String name) { this.name = name; }
		public boolean accept(File f) { return f.getName().equals(name); }
	}
	
	
	// =================================================================================
	public static class FileFilterNameExt implements java.io.FileFilter {
		private final String name;
		private final List<String> ext;
		
		public FileFilterNameExt(String name, String[] ext) {
			this.name = name;
			this.ext = Arrays.asList(ext);
		}
		
		public boolean accept(File f) {
			int idxDot = f.getName().indexOf(".");
			if (f.isDirectory() || idxDot == -1) return false;
			String n = f.getName().substring(0, idxDot);
			String e = f.getName().substring(idxDot+1);
			return n.equals(name) && ext.contains(e);
		}
	}
}
