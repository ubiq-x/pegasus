package research;

import inter.FSI;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import main.Do;
import smile.Network;
import util.$;
import util.Stats;


/**
 * @author Tomek D. Loboda
 */
public class Research01 {
	private final static String PATH = "research" + File.separatorChar + "01" + File.separatorChar + "diss" + File.separatorChar;
	
	private final static Hashtable<String,Integer> mapRStr2Int;
	
	static {
		mapRStr2Int = new Hashtable<String,Integer>();
		mapRStr2Int.put("n", 0);
		mapRStr2Int.put("p", 1);
		mapRStr2Int.put("s", 2);
	}
	
	
	// ---------------------------------------------------------------------------------
	public Research01() {}
	
	
	// ---------------------------------------------------------------------------------
	public String run(String netName, String dsName, String evVars, int tMax)  throws IOException {
		util.Timer timer = new util.Timer();
		
		String pathNet = Do.getPathApp() + PATH + "net" + File.separatorChar + netName + ".xdsl";
		String pathDS  = Do.getPathApp() + PATH + "ds" + File.separatorChar + dsName + ".txt";
		String pathRes = Do.getPathApp() + PATH + "res"+ File.separatorChar + "[net=" + netName + "]-[ds=" + dsName + "]-[tmax=" + tMax + "].txt";
		System.out.println("Paths:\n  net: " + pathNet + "\n  ds:  " + pathDS + "\n  res: " + pathRes);
		
		StringBuilder res = new StringBuilder();
		
		// (1) Init network:
		Network net = new Network();
		net.readFile(pathNet);
		net.setSampleCount(1000);
		net.setTarget("R", true);
		
		res.append("Network\n    Samples: " +  net.getSampleCount() + "\n    Inference algorithm: " +  net.getBayesianAlgorithm() + "\n\n");
		
		// (2) Init data set:
		StringBuilder sbDS = FSI.getStr(pathDS);
		int idxEndLn = sbDS.indexOf("\n");
		util.DataSet ds = new util.DataSet(sbDS.substring(idxEndLn+1), sbDS.substring(0,idxEndLn), "\n", "\t");
		
		res.append("Data set\n    Rows: " +  ds.rowCnt + "\n    Cols: " +  ds.colCnt + "\n\n\n");
		
		String[] EV = evVars.split(",");  // evidence variables
		
		// (3) Run:
		List<String> invObs = new ArrayList<String>();
		
		res.append("R\tR_MAP\tp(R=0)\tp(R=1)\tp(R=2)\tp_MAP sep\n");
		int[][] R_mapCnt = {{0,0,0}, {0,0,0}, {0,0,0}};
		Stats[] R_mapStats = { new Stats(), new Stats(), new Stats() };
		
		for (int i = 0; i < ds.rowCnt; i++) {
			net.clearAllEvidence();
			
			int tsLen = new Integer(ds.val(i, "ts_len"));
			int tsLenMax = (tMax == 0 ? tsLen : Math.min(tsLen, tMax));
			net.setSliceCount(tsLenMax);
			
			for (int j = 0; j < tsLenMax; j++) {
				for (String ev: EV) {
					String val = ds.val(i, ev + (j > 0 ? "_" + j : ""));
					if (val.equals(".")) continue;
					net.setTemporalEvidence(ev, j, val);
				}
			}
			
			net.updateBeliefs();
			
			int R = new Integer(mapRStr2Int.get(ds.val(i, "R")));
			
			int R_hat = 0;
			if (net.isValueValid("R")) {
				double[] val = net.getNodeValue("R");
				for (int j = 1; j < val.length; j++) {
					if (val[j] > val[R_hat]) R_hat = j;
				}
				
				R_mapCnt[R][R_hat]++;
				R_mapStats[0].addVal(val[0]);
				R_mapStats[1].addVal(val[1]);
				R_mapStats[2].addVal(val[2]);
				
				double pR0 = util.$.trunc(val[0], 2);
				double pR1 = util.$.trunc(val[1], 2);
				double pR2 = util.$.trunc(val[2], 2);
				Arrays.sort(val);
				double pMAPSep = val[2] - val[1];
				
				res.append(R + "\t" + R_hat + "\t" + pR0 + "\t" + pR1 + "\t"  + pR2 + "\t" + util.$.trunc(pMAPSep, 2) + "\n");
			}
			else invObs.add("" + i);
		}
		
		R_mapStats[0].calc();
		R_mapStats[1].calc();
		R_mapStats[2].calc();
		
		int[] R_mapCntSum = {
			R_mapCnt[0][0] + R_mapCnt[0][1] + R_mapCnt[0][2],
			R_mapCnt[1][0] + R_mapCnt[1][1] + R_mapCnt[1][2],
			R_mapCnt[2][0] + R_mapCnt[2][1] + R_mapCnt[2][2]
		};
		
		res.append(
			"\n\n" +
			"\tR_MAP: count\t\t\t\tR_MAP: prop.\n" +
			"R\t0\t1\t2\tSUM\t0\t1\t2\n" +
			"0\t" + R_mapCnt[0][0] + "\t" + R_mapCnt[0][1] + "\t" + R_mapCnt[0][2] + "\t" + R_mapCntSum[0] + "\t" + util.$.trunc((double)R_mapCnt[0][0] / (double)R_mapCntSum[0], 2) + "\t" + util.$.trunc((double)R_mapCnt[0][1] / (double)R_mapCntSum[0], 2) + "\t" + util.$.trunc((double)R_mapCnt[0][2] / (double)R_mapCntSum[0], 2) + "\n" +
			"1\t" + R_mapCnt[1][0] + "\t" + R_mapCnt[1][1] + "\t" + R_mapCnt[1][2] + "\t" + R_mapCntSum[1] + "\t" + util.$.trunc((double)R_mapCnt[1][0] / (double)R_mapCntSum[1], 2) + "\t" + util.$.trunc((double)R_mapCnt[1][1] / (double)R_mapCntSum[1], 2) + "\t" + util.$.trunc((double)R_mapCnt[1][2] / (double)R_mapCntSum[1], 2) + "\n" +
			"2\t" + R_mapCnt[2][0] + "\t" + R_mapCnt[2][1] + "\t" + R_mapCnt[2][2] + "\t" + R_mapCntSum[2] + "\t" + util.$.trunc((double)R_mapCnt[2][0] / (double)R_mapCntSum[2], 2) + "\t" + util.$.trunc((double)R_mapCnt[2][1] / (double)R_mapCntSum[2], 2) + "\t" + util.$.trunc((double)R_mapCnt[2][2] / (double)R_mapCntSum[2], 2) + "\n" +
			"\n\n" +
			"p(R=r):\n" +
			"r\tm\tsd\tM\tmin\tmax\n" +
			"0\t" + R_mapStats[0].getMean(2) + "\t" + R_mapStats[0].getStdev(2) + "\t" + R_mapStats[0].getMedian(2) + "\t" + R_mapStats[0].getMin(2) + "\t" + R_mapStats[0].getMax(2) + "\n" +
			"1\t" + R_mapStats[1].getMean(2) + "\t" + R_mapStats[1].getStdev(2) + "\t" + R_mapStats[1].getMedian(2) + "\t" + R_mapStats[1].getMin(2) + "\t" + R_mapStats[1].getMax(2) + "\n" +
			"2\t" + R_mapStats[2].getMean(2) + "\t" + R_mapStats[2].getStdev(2) + "\t" + R_mapStats[2].getMedian(2) + "\t" + R_mapStats[2].getMin(2) + "\t" + R_mapStats[2].getMax(2) + "\n" +
			(invObs.size() > 0 ? "\n\nInvalid row indeces: " + $.join(invObs, "", "", ", ", false, false) : "")
		);
		
		FSI.addFile(pathRes, res.toString());
		
		return "Job's done (" + timer.elapsedHuman() + ")! More work?";
	}
}
