--
-- PostgreSQL database dump
--

-- Dumped from database version 14.2
-- Dumped by pg_dump version 14.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: d; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA d;


ALTER SCHEMA d OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: var_type; Type: TABLE; Schema: d; Owner: postgres
--

CREATE TABLE d.var_type (
    id bigint NOT NULL,
    name character(3) NOT NULL
);


ALTER TABLE d.var_type OWNER TO postgres;

--
-- Data for Name: var_type; Type: TABLE DATA; Schema: d; Owner: postgres
--

COPY d.var_type (id, name) FROM stdin;
1	bin
2	cat
3	ord
4	dis
5	con
6	str
\.


--
-- Name: var_type var_type_pkey; Type: CONSTRAINT; Schema: d; Owner: postgres
--

ALTER TABLE ONLY d.var_type
    ADD CONSTRAINT var_type_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

