--
-- PostgreSQL database dump
--

-- Dumped from database version 14.2
-- Dumped by pg_dump version 14.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: d-tomek; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "d-tomek";


ALTER SCHEMA "d-tomek" OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: ds; Type: TABLE; Schema: d-tomek; Owner: postgres
--

CREATE TABLE "d-tomek".ds (
    id bigint NOT NULL,
    prj_id bigint NOT NULL,
    app_id bigint NOT NULL,
    name character varying(128) NOT NULL,
    memo text,
    created timestamp without time zone DEFAULT now() NOT NULL,
    calc_time bigint DEFAULT (0)::bigint NOT NULL,
    obs_cnt bigint DEFAULT (0)::bigint NOT NULL,
    size bigint DEFAULT (32)::bigint NOT NULL,
    note text,
    ts_var character varying(64) DEFAULT NULL::character varying
);


ALTER TABLE "d-tomek".ds OWNER TO postgres;

--
-- Name: COLUMN ds.ts_var; Type: COMMENT; Schema: d-tomek; Owner: postgres
--

COMMENT ON COLUMN "d-tomek".ds.ts_var IS 'name of the time series variable';


--
-- Name: ds_id_seq; Type: SEQUENCE; Schema: d-tomek; Owner: postgres
--

CREATE SEQUENCE "d-tomek".ds_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "d-tomek".ds_id_seq OWNER TO postgres;

--
-- Name: ds_id_seq; Type: SEQUENCE OWNED BY; Schema: d-tomek; Owner: postgres
--

ALTER SEQUENCE "d-tomek".ds_id_seq OWNED BY "d-tomek".ds.id;


--
-- Name: r_int; Type: TABLE; Schema: d-tomek; Owner: postgres
--

CREATE TABLE "d-tomek".r_int (
    id bigint NOT NULL,
    ds_id bigint NOT NULL,
    cmd character varying(255) NOT NULL,
    res text NOT NULL,
    res_ln_cnt integer DEFAULT 0 NOT NULL
);


ALTER TABLE "d-tomek".r_int OWNER TO postgres;

--
-- Name: r_int_id_seq; Type: SEQUENCE; Schema: d-tomek; Owner: postgres
--

CREATE SEQUENCE "d-tomek".r_int_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "d-tomek".r_int_id_seq OWNER TO postgres;

--
-- Name: r_int_id_seq; Type: SEQUENCE OWNED BY; Schema: d-tomek; Owner: postgres
--

ALTER SEQUENCE "d-tomek".r_int_id_seq OWNED BY "d-tomek".r_int.id;


--
-- Name: rep; Type: TABLE; Schema: d-tomek; Owner: postgres
--

CREATE TABLE "d-tomek".rep (
    id bigint NOT NULL,
    ds_id bigint NOT NULL,
    name character varying(128) NOT NULL,
    memo text,
    created timestamp without time zone DEFAULT now() NOT NULL,
    size bigint DEFAULT (25)::bigint NOT NULL,
    calc_time bigint DEFAULT (0)::bigint NOT NULL,
    code text,
    pdf bytea
);


ALTER TABLE "d-tomek".rep OWNER TO postgres;

--
-- Name: rep_id_seq; Type: SEQUENCE; Schema: d-tomek; Owner: postgres
--

CREATE SEQUENCE "d-tomek".rep_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "d-tomek".rep_id_seq OWNER TO postgres;

--
-- Name: rep_id_seq; Type: SEQUENCE OWNED BY; Schema: d-tomek; Owner: postgres
--

ALTER SEQUENCE "d-tomek".rep_id_seq OWNED BY "d-tomek".rep.id;


--
-- Name: rep_item; Type: TABLE; Schema: d-tomek; Owner: postgres
--

CREATE TABLE "d-tomek".rep_item (
    id bigint NOT NULL,
    rep_id bigint NOT NULL,
    type_id bigint NOT NULL,
    content bytea NOT NULL,
    meta text,
    size bigint DEFAULT (20)::bigint NOT NULL
);


ALTER TABLE "d-tomek".rep_item OWNER TO postgres;

--
-- Name: rep_item_id_seq; Type: SEQUENCE; Schema: d-tomek; Owner: postgres
--

CREATE SEQUENCE "d-tomek".rep_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "d-tomek".rep_item_id_seq OWNER TO postgres;

--
-- Name: rep_item_id_seq; Type: SEQUENCE OWNED BY; Schema: d-tomek; Owner: postgres
--

ALTER SEQUENCE "d-tomek".rep_item_id_seq OWNED BY "d-tomek".rep_item.id;


--
-- Name: rep_item_type; Type: TABLE; Schema: d-tomek; Owner: postgres
--

CREATE TABLE "d-tomek".rep_item_type (
    id bigint NOT NULL,
    name character varying(32) NOT NULL
);


ALTER TABLE "d-tomek".rep_item_type OWNER TO postgres;

--
-- Name: settings; Type: TABLE; Schema: d-tomek; Owner: postgres
--

CREATE TABLE "d-tomek".settings (
    missing_value character varying(12) DEFAULT '.'::character varying NOT NULL,
    hist_r_cnt bigint DEFAULT (64)::bigint NOT NULL
);


ALTER TABLE "d-tomek".settings OWNER TO postgres;

--
-- Name: var; Type: TABLE; Schema: d-tomek; Owner: postgres
--

CREATE TABLE "d-tomek".var (
    id bigint NOT NULL,
    ds_id bigint NOT NULL,
    type_id bigint NOT NULL,
    name character varying(64) NOT NULL,
    memo text,
    ord bigint NOT NULL,
    stat_ok smallint DEFAULT (0)::smallint NOT NULL,
    stat_desc_min numeric(24,12) DEFAULT NULL::numeric,
    stat_desc_max numeric(24,12) DEFAULT NULL::numeric,
    stat_desc_mean numeric(24,12) DEFAULT NULL::numeric,
    stat_desc_med numeric(24,12) DEFAULT NULL::numeric,
    stat_desc_sd numeric(24,12) DEFAULT NULL::numeric,
    stat_norm_ad_s numeric(8,4) DEFAULT NULL::numeric,
    stat_norm_ad_p numeric(5,4) DEFAULT NULL::numeric,
    stat_norm_cvm_s numeric(8,4) DEFAULT NULL::numeric,
    stat_norm_cvm_p numeric(5,4) DEFAULT NULL::numeric,
    stat_norm_ks_s numeric(8,4) DEFAULT NULL::numeric,
    stat_norm_ks_p numeric(5,4) DEFAULT NULL::numeric,
    stat_norm_sw_s numeric(8,4) DEFAULT NULL::numeric,
    stat_norm_sw_p numeric(5,4) DEFAULT NULL::numeric,
    stat_misc_miss_val_cnt bigint
);


ALTER TABLE "d-tomek".var OWNER TO postgres;

--
-- Name: COLUMN var.ord; Type: COMMENT; Schema: d-tomek; Owner: postgres
--

COMMENT ON COLUMN "d-tomek".var.ord IS '0-based';


--
-- Name: var_id_seq; Type: SEQUENCE; Schema: d-tomek; Owner: postgres
--

CREATE SEQUENCE "d-tomek".var_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "d-tomek".var_id_seq OWNER TO postgres;

--
-- Name: var_id_seq; Type: SEQUENCE OWNED BY; Schema: d-tomek; Owner: postgres
--

ALTER SEQUENCE "d-tomek".var_id_seq OWNED BY "d-tomek".var.id;


--
-- Name: var_val; Type: TABLE; Schema: d-tomek; Owner: postgres
--

CREATE TABLE "d-tomek".var_val (
    id bigint NOT NULL,
    var_id bigint NOT NULL,
    val bigint NOT NULL,
    str character varying(32) NOT NULL
);


ALTER TABLE "d-tomek".var_val OWNER TO postgres;

--
-- Name: var_val_id_seq; Type: SEQUENCE; Schema: d-tomek; Owner: postgres
--

CREATE SEQUENCE "d-tomek".var_val_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "d-tomek".var_val_id_seq OWNER TO postgres;

--
-- Name: var_val_id_seq; Type: SEQUENCE OWNED BY; Schema: d-tomek; Owner: postgres
--

ALTER SEQUENCE "d-tomek".var_val_id_seq OWNED BY "d-tomek".var_val.id;


--
-- Name: ds id; Type: DEFAULT; Schema: d-tomek; Owner: postgres
--

ALTER TABLE ONLY "d-tomek".ds ALTER COLUMN id SET DEFAULT nextval('"d-tomek".ds_id_seq'::regclass);


--
-- Name: r_int id; Type: DEFAULT; Schema: d-tomek; Owner: postgres
--

ALTER TABLE ONLY "d-tomek".r_int ALTER COLUMN id SET DEFAULT nextval('"d-tomek".r_int_id_seq'::regclass);


--
-- Name: rep id; Type: DEFAULT; Schema: d-tomek; Owner: postgres
--

ALTER TABLE ONLY "d-tomek".rep ALTER COLUMN id SET DEFAULT nextval('"d-tomek".rep_id_seq'::regclass);


--
-- Name: rep_item id; Type: DEFAULT; Schema: d-tomek; Owner: postgres
--

ALTER TABLE ONLY "d-tomek".rep_item ALTER COLUMN id SET DEFAULT nextval('"d-tomek".rep_item_id_seq'::regclass);


--
-- Name: var id; Type: DEFAULT; Schema: d-tomek; Owner: postgres
--

ALTER TABLE ONLY "d-tomek".var ALTER COLUMN id SET DEFAULT nextval('"d-tomek".var_id_seq'::regclass);


--
-- Name: var_val id; Type: DEFAULT; Schema: d-tomek; Owner: postgres
--

ALTER TABLE ONLY "d-tomek".var_val ALTER COLUMN id SET DEFAULT nextval('"d-tomek".var_val_id_seq'::regclass);


--
-- Data for Name: ds; Type: TABLE DATA; Schema: d-tomek; Owner: postgres
--

COPY "d-tomek".ds (id, prj_id, app_id, name, memo, created, calc_time, obs_cnt, size, note, ts_var) FROM stdin;
\.


--
-- Data for Name: r_int; Type: TABLE DATA; Schema: d-tomek; Owner: postgres
--

COPY "d-tomek".r_int (id, ds_id, cmd, res, res_ln_cnt) FROM stdin;
\.


--
-- Data for Name: rep; Type: TABLE DATA; Schema: d-tomek; Owner: postgres
--

COPY "d-tomek".rep (id, ds_id, name, memo, created, size, calc_time, code, pdf) FROM stdin;
\.


--
-- Data for Name: rep_item; Type: TABLE DATA; Schema: d-tomek; Owner: postgres
--

COPY "d-tomek".rep_item (id, rep_id, type_id, content, meta, size) FROM stdin;
\.


--
-- Data for Name: rep_item_type; Type: TABLE DATA; Schema: d-tomek; Owner: postgres
--

COPY "d-tomek".rep_item_type (id, name) FROM stdin;
1	text
2	image
3	section
4	subsection
5	subsubsection
\.


--
-- Data for Name: settings; Type: TABLE DATA; Schema: d-tomek; Owner: postgres
--

COPY "d-tomek".settings (missing_value, hist_r_cnt) FROM stdin;
.	64
\.


--
-- Data for Name: var; Type: TABLE DATA; Schema: d-tomek; Owner: postgres
--

COPY "d-tomek".var (id, ds_id, type_id, name, memo, ord, stat_ok, stat_desc_min, stat_desc_max, stat_desc_mean, stat_desc_med, stat_desc_sd, stat_norm_ad_s, stat_norm_ad_p, stat_norm_cvm_s, stat_norm_cvm_p, stat_norm_ks_s, stat_norm_ks_p, stat_norm_sw_s, stat_norm_sw_p, stat_misc_miss_val_cnt) FROM stdin;
\.


--
-- Data for Name: var_val; Type: TABLE DATA; Schema: d-tomek; Owner: postgres
--

COPY "d-tomek".var_val (id, var_id, val, str) FROM stdin;
\.


--
-- Name: ds_id_seq; Type: SEQUENCE SET; Schema: d-tomek; Owner: postgres
--

SELECT pg_catalog.setval('"d-tomek".ds_id_seq', 6812, true);


--
-- Name: r_int_id_seq; Type: SEQUENCE SET; Schema: d-tomek; Owner: postgres
--

SELECT pg_catalog.setval('"d-tomek".r_int_id_seq', 404, true);


--
-- Name: rep_id_seq; Type: SEQUENCE SET; Schema: d-tomek; Owner: postgres
--

SELECT pg_catalog.setval('"d-tomek".rep_id_seq', 46, true);


--
-- Name: rep_item_id_seq; Type: SEQUENCE SET; Schema: d-tomek; Owner: postgres
--

SELECT pg_catalog.setval('"d-tomek".rep_item_id_seq', 39484, true);


--
-- Name: var_id_seq; Type: SEQUENCE SET; Schema: d-tomek; Owner: postgres
--

SELECT pg_catalog.setval('"d-tomek".var_id_seq', 714323, true);


--
-- Name: var_val_id_seq; Type: SEQUENCE SET; Schema: d-tomek; Owner: postgres
--

SELECT pg_catalog.setval('"d-tomek".var_val_id_seq', 1, false);


--
-- Name: ds ds_pkey; Type: CONSTRAINT; Schema: d-tomek; Owner: postgres
--

ALTER TABLE ONLY "d-tomek".ds
    ADD CONSTRAINT ds_pkey PRIMARY KEY (id);


--
-- Name: ds ds_prj_id_key; Type: CONSTRAINT; Schema: d-tomek; Owner: postgres
--

ALTER TABLE ONLY "d-tomek".ds
    ADD CONSTRAINT ds_prj_id_key UNIQUE (prj_id, name);


--
-- Name: r_int r_int_pkey; Type: CONSTRAINT; Schema: d-tomek; Owner: postgres
--

ALTER TABLE ONLY "d-tomek".r_int
    ADD CONSTRAINT r_int_pkey PRIMARY KEY (id);


--
-- Name: rep_item rep_item_pkey; Type: CONSTRAINT; Schema: d-tomek; Owner: postgres
--

ALTER TABLE ONLY "d-tomek".rep_item
    ADD CONSTRAINT rep_item_pkey PRIMARY KEY (id);


--
-- Name: rep_item_type rep_item_type_pkey; Type: CONSTRAINT; Schema: d-tomek; Owner: postgres
--

ALTER TABLE ONLY "d-tomek".rep_item_type
    ADD CONSTRAINT rep_item_type_pkey PRIMARY KEY (id);


--
-- Name: rep rep_pkey; Type: CONSTRAINT; Schema: d-tomek; Owner: postgres
--

ALTER TABLE ONLY "d-tomek".rep
    ADD CONSTRAINT rep_pkey PRIMARY KEY (id);


--
-- Name: settings settings_pkey; Type: CONSTRAINT; Schema: d-tomek; Owner: postgres
--

ALTER TABLE ONLY "d-tomek".settings
    ADD CONSTRAINT settings_pkey PRIMARY KEY (missing_value);


--
-- Name: var var_ds_id_ord_unique; Type: CONSTRAINT; Schema: d-tomek; Owner: postgres
--

ALTER TABLE ONLY "d-tomek".var
    ADD CONSTRAINT var_ds_id_ord_unique UNIQUE (ds_id, ord);


--
-- Name: var var_pkey; Type: CONSTRAINT; Schema: d-tomek; Owner: postgres
--

ALTER TABLE ONLY "d-tomek".var
    ADD CONSTRAINT var_pkey PRIMARY KEY (id);


--
-- Name: var_val var_val_pkey; Type: CONSTRAINT; Schema: d-tomek; Owner: postgres
--

ALTER TABLE ONLY "d-tomek".var_val
    ADD CONSTRAINT var_val_pkey PRIMARY KEY (id);


--
-- Name: var_val var_val_var_id_key; Type: CONSTRAINT; Schema: d-tomek; Owner: postgres
--

ALTER TABLE ONLY "d-tomek".var_val
    ADD CONSTRAINT var_val_var_id_key UNIQUE (var_id, val);


--
-- Name: ds_app_id; Type: INDEX; Schema: d-tomek; Owner: postgres
--

CREATE INDEX ds_app_id ON "d-tomek".ds USING btree (app_id);


--
-- Name: ds_prj_id; Type: INDEX; Schema: d-tomek; Owner: postgres
--

CREATE INDEX ds_prj_id ON "d-tomek".ds USING btree (prj_id);


--
-- Name: r_int_ds_id; Type: INDEX; Schema: d-tomek; Owner: postgres
--

CREATE INDEX r_int_ds_id ON "d-tomek".r_int USING btree (ds_id);


--
-- Name: rep_ds_id; Type: INDEX; Schema: d-tomek; Owner: postgres
--

CREATE INDEX rep_ds_id ON "d-tomek".rep USING btree (ds_id);


--
-- Name: rep_item_rep_id; Type: INDEX; Schema: d-tomek; Owner: postgres
--

CREATE INDEX rep_item_rep_id ON "d-tomek".rep_item USING btree (rep_id);


--
-- Name: rep_item_type_id; Type: INDEX; Schema: d-tomek; Owner: postgres
--

CREATE INDEX rep_item_type_id ON "d-tomek".rep_item USING btree (type_id);


--
-- Name: var_ds_id; Type: INDEX; Schema: d-tomek; Owner: postgres
--

CREATE INDEX var_ds_id ON "d-tomek".var USING btree (ds_id);


--
-- Name: var_type_id; Type: INDEX; Schema: d-tomek; Owner: postgres
--

CREATE INDEX var_type_id ON "d-tomek".var USING btree (type_id);


--
-- Name: var_val_var_id; Type: INDEX; Schema: d-tomek; Owner: postgres
--

CREATE INDEX var_val_var_id ON "d-tomek".var_val USING btree (var_id);


--
-- Name: ds ds_app_id_fkey; Type: FK CONSTRAINT; Schema: d-tomek; Owner: postgres
--

ALTER TABLE ONLY "d-tomek".ds
    ADD CONSTRAINT ds_app_id_fkey FOREIGN KEY (app_id) REFERENCES m.app(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ds ds_prj_id_fkey; Type: FK CONSTRAINT; Schema: d-tomek; Owner: postgres
--

ALTER TABLE ONLY "d-tomek".ds
    ADD CONSTRAINT ds_prj_id_fkey FOREIGN KEY (prj_id) REFERENCES m.prj(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: r_int r_int_ds_id_fkey; Type: FK CONSTRAINT; Schema: d-tomek; Owner: postgres
--

ALTER TABLE ONLY "d-tomek".r_int
    ADD CONSTRAINT r_int_ds_id_fkey FOREIGN KEY (ds_id) REFERENCES "d-tomek".ds(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rep rep_ds_id_fkey; Type: FK CONSTRAINT; Schema: d-tomek; Owner: postgres
--

ALTER TABLE ONLY "d-tomek".rep
    ADD CONSTRAINT rep_ds_id_fkey FOREIGN KEY (ds_id) REFERENCES "d-tomek".ds(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rep_item rep_item_rep_id_fkey; Type: FK CONSTRAINT; Schema: d-tomek; Owner: postgres
--

ALTER TABLE ONLY "d-tomek".rep_item
    ADD CONSTRAINT rep_item_rep_id_fkey FOREIGN KEY (rep_id) REFERENCES "d-tomek".rep(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: rep_item rep_item_type_id_fkey; Type: FK CONSTRAINT; Schema: d-tomek; Owner: postgres
--

ALTER TABLE ONLY "d-tomek".rep_item
    ADD CONSTRAINT rep_item_type_id_fkey FOREIGN KEY (type_id) REFERENCES "d-tomek".rep_item_type(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: var var_ds_id_fkey; Type: FK CONSTRAINT; Schema: d-tomek; Owner: postgres
--

ALTER TABLE ONLY "d-tomek".var
    ADD CONSTRAINT var_ds_id_fkey FOREIGN KEY (ds_id) REFERENCES "d-tomek".ds(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: var var_type_id_fkey; Type: FK CONSTRAINT; Schema: d-tomek; Owner: postgres
--

ALTER TABLE ONLY "d-tomek".var
    ADD CONSTRAINT var_type_id_fkey FOREIGN KEY (type_id) REFERENCES d.var_type(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

