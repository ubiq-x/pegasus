--
-- PostgreSQL database dump
--

-- Dumped from database version 14.2
-- Dumped by pg_dump version 14.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: g-tomek; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "g-tomek";


ALTER SCHEMA "g-tomek" OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: blink; Type: TABLE; Schema: g-tomek; Owner: postgres
--

CREATE TABLE "g-tomek".blink (
    id bigint NOT NULL,
    trial_id bigint,
    stim_id bigint,
    roi_01_id bigint,
    roi_02_id bigint,
    sx real NOT NULL,
    sy real NOT NULL,
    ex real NOT NULL,
    ey real NOT NULL,
    dur bigint NOT NULL,
    sacc_dur bigint NOT NULL,
    sacc_amp real NOT NULL,
    sacc_pv bigint NOT NULL,
    t bigint NOT NULL,
    stim_seq_id bigint,
    exc boolean DEFAULT false NOT NULL
);


ALTER TABLE "g-tomek".blink OWNER TO postgres;

--
-- Name: blink_id_seq; Type: SEQUENCE; Schema: g-tomek; Owner: postgres
--

CREATE SEQUENCE "g-tomek".blink_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "g-tomek".blink_id_seq OWNER TO postgres;

--
-- Name: blink_id_seq; Type: SEQUENCE OWNED BY; Schema: g-tomek; Owner: postgres
--

ALTER SEQUENCE "g-tomek".blink_id_seq OWNED BY "g-tomek".blink.id;


--
-- Name: cal; Type: TABLE; Schema: g-tomek; Owner: postgres
--

CREATE TABLE "g-tomek".cal (
    id bigint NOT NULL,
    trial_id bigint NOT NULL,
    t bigint NOT NULL,
    eye character(1) NOT NULL,
    point_cnt smallint NOT NULL,
    cal_res character varying(9) NOT NULL,
    val_res character varying(9) NOT NULL,
    val_err_avg numeric(5,2) NOT NULL,
    val_err_max numeric(5,2) NOT NULL,
    stim_cnt smallint DEFAULT 0 NOT NULL,
    dur bigint NOT NULL,
    scr_w smallint NOT NULL,
    scr_h smallint NOT NULL
);


ALTER TABLE "g-tomek".cal OWNER TO postgres;

--
-- Name: COLUMN cal.t; Type: COMMENT; Schema: g-tomek; Owner: postgres
--

COMMENT ON COLUMN "g-tomek".cal.t IS '[ms]';


--
-- Name: COLUMN cal.eye; Type: COMMENT; Schema: g-tomek; Owner: postgres
--

COMMENT ON COLUMN "g-tomek".cal.eye IS '{''R''|''L''}';


--
-- Name: COLUMN cal.val_err_avg; Type: COMMENT; Schema: g-tomek; Owner: postgres
--

COMMENT ON COLUMN "g-tomek".cal.val_err_avg IS '[deg]';


--
-- Name: COLUMN cal.val_err_max; Type: COMMENT; Schema: g-tomek; Owner: postgres
--

COMMENT ON COLUMN "g-tomek".cal.val_err_max IS '[deg]';


--
-- Name: COLUMN cal.stim_cnt; Type: COMMENT; Schema: g-tomek; Owner: postgres
--

COMMENT ON COLUMN "g-tomek".cal.stim_cnt IS 'how many stimulus changes occured until the next calibration of the end of trial';


--
-- Name: COLUMN cal.dur; Type: COMMENT; Schema: g-tomek; Owner: postgres
--

COMMENT ON COLUMN "g-tomek".cal.dur IS 'how much time has passed since the next calibration or the end of trial [ms]';


--
-- Name: cal_id_seq; Type: SEQUENCE; Schema: g-tomek; Owner: postgres
--

CREATE SEQUENCE "g-tomek".cal_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "g-tomek".cal_id_seq OWNER TO postgres;

--
-- Name: cal_id_seq; Type: SEQUENCE OWNED BY; Schema: g-tomek; Owner: postgres
--

ALTER SEQUENCE "g-tomek".cal_id_seq OWNED BY "g-tomek".cal.id;


--
-- Name: cal_point; Type: TABLE; Schema: g-tomek; Owner: postgres
--

CREATE TABLE "g-tomek".cal_point (
    id integer NOT NULL,
    cal_id integer NOT NULL,
    num smallint NOT NULL,
    x smallint NOT NULL,
    y smallint NOT NULL,
    x_err numeric(6,1) NOT NULL,
    y_err numeric(6,1) NOT NULL,
    ang_err numeric(5,2) NOT NULL
);


ALTER TABLE "g-tomek".cal_point OWNER TO postgres;

--
-- Name: COLUMN cal_point.num; Type: COMMENT; Schema: g-tomek; Owner: postgres
--

COMMENT ON COLUMN "g-tomek".cal_point.num IS 'order number (left-to-right, top-t-bottom): {0,1,...}';


--
-- Name: COLUMN cal_point.ang_err; Type: COMMENT; Schema: g-tomek; Owner: postgres
--

COMMENT ON COLUMN "g-tomek".cal_point.ang_err IS '[deg]';


--
-- Name: cal_point_id_seq; Type: SEQUENCE; Schema: g-tomek; Owner: postgres
--

CREATE SEQUENCE "g-tomek".cal_point_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "g-tomek".cal_point_id_seq OWNER TO postgres;

--
-- Name: cal_point_id_seq; Type: SEQUENCE OWNED BY; Schema: g-tomek; Owner: postgres
--

ALTER SEQUENCE "g-tomek".cal_point_id_seq OWNED BY "g-tomek".cal_point.id;


--
-- Name: ds; Type: TABLE; Schema: g-tomek; Owner: postgres
--

CREATE TABLE "g-tomek".ds (
    id bigint NOT NULL,
    exp_id bigint NOT NULL,
    ds_id bigint NOT NULL
);


ALTER TABLE "g-tomek".ds OWNER TO postgres;

--
-- Name: ds_id_seq; Type: SEQUENCE; Schema: g-tomek; Owner: postgres
--

CREATE SEQUENCE "g-tomek".ds_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "g-tomek".ds_id_seq OWNER TO postgres;

--
-- Name: ds_id_seq; Type: SEQUENCE OWNED BY; Schema: g-tomek; Owner: postgres
--

ALTER SEQUENCE "g-tomek".ds_id_seq OWNED BY "g-tomek".ds.id;


--
-- Name: evt; Type: TABLE; Schema: g-tomek; Owner: postgres
--

CREATE TABLE "g-tomek".evt (
    id bigint NOT NULL,
    evt_lst_id bigint NOT NULL,
    memo text,
    trial_id bigint NOT NULL,
    stim_id bigint NOT NULL,
    stim_seq_id bigint NOT NULL,
    t_prompt bigint NOT NULL,
    t_resp bigint NOT NULL,
    exc boolean DEFAULT false NOT NULL,
    exc_info character varying(255) DEFAULT NULL::character varying
);


ALTER TABLE "g-tomek".evt OWNER TO postgres;

--
-- Name: evt_id_seq; Type: SEQUENCE; Schema: g-tomek; Owner: postgres
--

CREATE SEQUENCE "g-tomek".evt_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "g-tomek".evt_id_seq OWNER TO postgres;

--
-- Name: evt_id_seq; Type: SEQUENCE OWNED BY; Schema: g-tomek; Owner: postgres
--

ALTER SEQUENCE "g-tomek".evt_id_seq OWNED BY "g-tomek".evt.id;


--
-- Name: evt_lst; Type: TABLE; Schema: g-tomek; Owner: postgres
--

CREATE TABLE "g-tomek".evt_lst (
    id bigint NOT NULL,
    exp_id bigint NOT NULL,
    memo text,
    name character varying(64) NOT NULL
);


ALTER TABLE "g-tomek".evt_lst OWNER TO postgres;

--
-- Name: evt_lst_id_seq; Type: SEQUENCE; Schema: g-tomek; Owner: postgres
--

CREATE SEQUENCE "g-tomek".evt_lst_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "g-tomek".evt_lst_id_seq OWNER TO postgres;

--
-- Name: evt_lst_id_seq; Type: SEQUENCE OWNED BY; Schema: g-tomek; Owner: postgres
--

ALTER SEQUENCE "g-tomek".evt_lst_id_seq OWNED BY "g-tomek".evt_lst.id;


--
-- Name: exp; Type: TABLE; Schema: g-tomek; Owner: postgres
--

CREATE TABLE "g-tomek".exp (
    id bigint NOT NULL,
    prj_id bigint DEFAULT (2)::bigint NOT NULL,
    name character varying(128) NOT NULL,
    precomp smallint DEFAULT (0)::smallint NOT NULL,
    memo text,
    created timestamp without time zone DEFAULT now() NOT NULL,
    imp_rep text,
    imp_ts timestamp without time zone,
    stim_zoom numeric(3,2) DEFAULT 1.00 NOT NULL,
    linked boolean DEFAULT false NOT NULL,
    note text
);


ALTER TABLE "g-tomek".exp OWNER TO postgres;

--
-- Name: exp_id_seq; Type: SEQUENCE; Schema: g-tomek; Owner: postgres
--

CREATE SEQUENCE "g-tomek".exp_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "g-tomek".exp_id_seq OWNER TO postgres;

--
-- Name: exp_id_seq; Type: SEQUENCE OWNED BY; Schema: g-tomek; Owner: postgres
--

ALTER SEQUENCE "g-tomek".exp_id_seq OWNED BY "g-tomek".exp.id;


--
-- Name: fix; Type: TABLE; Schema: g-tomek; Owner: postgres
--

CREATE TABLE "g-tomek".fix (
    id bigint NOT NULL,
    trial_id bigint NOT NULL,
    roi_id bigint,
    num bigint NOT NULL,
    t bigint NOT NULL,
    dur bigint NOT NULL,
    x real NOT NULL,
    y real NOT NULL,
    stim_id bigint,
    sacc bigint,
    pupil real,
    stim_seq_id bigint,
    x_adj real,
    y_adj real,
    roi_id_adj bigint,
    fix_ln_id bigint,
    sacc_adj bigint,
    exc boolean DEFAULT false NOT NULL
);


ALTER TABLE "g-tomek".fix OWNER TO postgres;

--
-- Name: fix_id_seq; Type: SEQUENCE; Schema: g-tomek; Owner: postgres
--

CREATE SEQUENCE "g-tomek".fix_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "g-tomek".fix_id_seq OWNER TO postgres;

--
-- Name: fix_id_seq; Type: SEQUENCE OWNED BY; Schema: g-tomek; Owner: postgres
--

ALTER SEQUENCE "g-tomek".fix_id_seq OWNED BY "g-tomek".fix.id;


--
-- Name: fix_ln; Type: TABLE; Schema: g-tomek; Owner: postgres
--

CREATE TABLE "g-tomek".fix_ln (
    id bigint NOT NULL,
    stim_seq_id bigint NOT NULL,
    a real NOT NULL,
    b real NOT NULL,
    r2 real NOT NULL,
    sd real NOT NULL,
    se_a real NOT NULL,
    se_b real NOT NULL,
    fix_cnt bigint NOT NULL
);


ALTER TABLE "g-tomek".fix_ln OWNER TO postgres;

--
-- Name: TABLE fix_ln; Type: COMMENT; Schema: g-tomek; Owner: postgres
--

COMMENT ON TABLE "g-tomek".fix_ln IS 'Fixations line';


--
-- Name: fix_ln_id_seq; Type: SEQUENCE; Schema: g-tomek; Owner: postgres
--

CREATE SEQUENCE "g-tomek".fix_ln_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "g-tomek".fix_ln_id_seq OWNER TO postgres;

--
-- Name: fix_ln_id_seq; Type: SEQUENCE OWNED BY; Schema: g-tomek; Owner: postgres
--

ALTER SEQUENCE "g-tomek".fix_ln_id_seq OWNED BY "g-tomek".fix_ln.id;


--
-- Name: form; Type: TABLE; Schema: g-tomek; Owner: postgres
--

CREATE TABLE "g-tomek".form (
    id bigint NOT NULL,
    exp_id bigint NOT NULL,
    name character varying(255) NOT NULL,
    memo text,
    created timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE "g-tomek".form OWNER TO postgres;

--
-- Name: form_id_seq; Type: SEQUENCE; Schema: g-tomek; Owner: postgres
--

CREATE SEQUENCE "g-tomek".form_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "g-tomek".form_id_seq OWNER TO postgres;

--
-- Name: form_id_seq; Type: SEQUENCE OWNED BY; Schema: g-tomek; Owner: postgres
--

ALTER SEQUENCE "g-tomek".form_id_seq OWNED BY "g-tomek".form.id;


--
-- Name: log_cmd; Type: TABLE; Schema: g-tomek; Owner: postgres
--

CREATE TABLE "g-tomek".log_cmd (
    id bigint NOT NULL,
    exp_id bigint NOT NULL,
    ts timestamp without time zone DEFAULT now() NOT NULL,
    cmd character varying(32) NOT NULL,
    qs text NOT NULL,
    exec_t bigint NOT NULL
);


ALTER TABLE "g-tomek".log_cmd OWNER TO postgres;

--
-- Name: log_cmd_id_seq; Type: SEQUENCE; Schema: g-tomek; Owner: postgres
--

CREATE SEQUENCE "g-tomek".log_cmd_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "g-tomek".log_cmd_id_seq OWNER TO postgres;

--
-- Name: log_cmd_id_seq; Type: SEQUENCE OWNED BY; Schema: g-tomek; Owner: postgres
--

ALTER SEQUENCE "g-tomek".log_cmd_id_seq OWNED BY "g-tomek".log_cmd.id;


--
-- Name: log_xform; Type: TABLE; Schema: g-tomek; Owner: postgres
--

CREATE TABLE "g-tomek".log_xform (
    id bigint NOT NULL,
    stim_seq_id bigint NOT NULL,
    ts timestamp without time zone DEFAULT now() NOT NULL,
    tx bigint NOT NULL,
    ty bigint NOT NULL,
    sx real NOT NULL,
    sy real NOT NULL,
    a real NOT NULL
);


ALTER TABLE "g-tomek".log_xform OWNER TO postgres;

--
-- Name: log_xform_id_seq; Type: SEQUENCE; Schema: g-tomek; Owner: postgres
--

CREATE SEQUENCE "g-tomek".log_xform_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "g-tomek".log_xform_id_seq OWNER TO postgres;

--
-- Name: log_xform_id_seq; Type: SEQUENCE OWNED BY; Schema: g-tomek; Owner: postgres
--

ALTER SEQUENCE "g-tomek".log_xform_id_seq OWNED BY "g-tomek".log_xform.id;


--
-- Name: roi; Type: TABLE; Schema: g-tomek; Owner: postgres
--

CREATE TABLE "g-tomek".roi (
    id bigint NOT NULL,
    stim_id bigint NOT NULL,
    type_id bigint NOT NULL,
    name character varying(64) NOT NULL,
    num bigint DEFAULT (0)::bigint NOT NULL
);


ALTER TABLE "g-tomek".roi OWNER TO postgres;

--
-- Name: roi_id_seq; Type: SEQUENCE; Schema: g-tomek; Owner: postgres
--

CREATE SEQUENCE "g-tomek".roi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "g-tomek".roi_id_seq OWNER TO postgres;

--
-- Name: roi_id_seq; Type: SEQUENCE OWNED BY; Schema: g-tomek; Owner: postgres
--

ALTER SEQUENCE "g-tomek".roi_id_seq OWNED BY "g-tomek".roi.id;


--
-- Name: roi_type; Type: TABLE; Schema: g-tomek; Owner: postgres
--

CREATE TABLE "g-tomek".roi_type (
    id bigint NOT NULL,
    name character varying(64) NOT NULL
);


ALTER TABLE "g-tomek".roi_type OWNER TO postgres;

--
-- Name: roi_type_id_seq; Type: SEQUENCE; Schema: g-tomek; Owner: postgres
--

CREATE SEQUENCE "g-tomek".roi_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "g-tomek".roi_type_id_seq OWNER TO postgres;

--
-- Name: roi_type_id_seq; Type: SEQUENCE OWNED BY; Schema: g-tomek; Owner: postgres
--

ALTER SEQUENCE "g-tomek".roi_type_id_seq OWNED BY "g-tomek".roi_type.id;


--
-- Name: stim; Type: TABLE; Schema: g-tomek; Owner: postgres
--

CREATE TABLE "g-tomek".stim (
    id bigint NOT NULL,
    exp_id bigint NOT NULL,
    name character varying(128) NOT NULL,
    memo text,
    txt text,
    type character(1) DEFAULT 'u'::bpchar NOT NULL,
    img bytea,
    res_h bigint DEFAULT (1024)::bigint NOT NULL,
    res_v bigint DEFAULT (768)::bigint NOT NULL,
    font_fam character varying(32) DEFAULT 'Courier New'::character varying NOT NULL,
    font_size real DEFAULT (12)::real NOT NULL,
    roi_h bigint DEFAULT (200)::bigint NOT NULL,
    offset_x bigint DEFAULT (0)::bigint NOT NULL,
    offset_y bigint DEFAULT (0)::bigint NOT NULL,
    align_h character(1) DEFAULT 'c'::bpchar,
    align_v character(1) DEFAULT 'c'::bpchar,
    margin_t bigint DEFAULT (0)::bigint,
    margin_l bigint DEFAULT (0)::bigint,
    margin_r bigint DEFAULT (0)::bigint,
    letter_spac real DEFAULT (0)::real,
    line_h bigint DEFAULT (100)::bigint,
    padding_t bigint DEFAULT (0)::bigint,
    padding_b bigint DEFAULT (0)::bigint,
    box_x1 bigint DEFAULT (0)::bigint NOT NULL,
    box_y1 bigint DEFAULT (0)::bigint NOT NULL,
    box_x2 bigint DEFAULT (0)::bigint NOT NULL,
    box_y2 bigint DEFAULT (0)::bigint NOT NULL,
    exc boolean DEFAULT false NOT NULL,
    exc_info character varying(255) DEFAULT NULL::character varying,
    roi_exp boolean DEFAULT true NOT NULL,
    img_roi bytea
);


ALTER TABLE "g-tomek".stim OWNER TO postgres;

--
-- Name: stim_id_seq; Type: SEQUENCE; Schema: g-tomek; Owner: postgres
--

CREATE SEQUENCE "g-tomek".stim_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "g-tomek".stim_id_seq OWNER TO postgres;

--
-- Name: stim_id_seq; Type: SEQUENCE OWNED BY; Schema: g-tomek; Owner: postgres
--

ALTER SEQUENCE "g-tomek".stim_id_seq OWNED BY "g-tomek".stim.id;


--
-- Name: stim_seq; Type: TABLE; Schema: g-tomek; Owner: postgres
--

CREATE TABLE "g-tomek".stim_seq (
    id bigint NOT NULL,
    trial_id bigint NOT NULL,
    stim_id bigint NOT NULL,
    t0 bigint NOT NULL,
    t1 bigint DEFAULT (0)::bigint NOT NULL,
    fix_cnt bigint,
    fix_cnt_roi bigint,
    blink_cnt bigint,
    fix_cnt_exc bigint DEFAULT (0)::bigint NOT NULL,
    fix_cnt_roi_adj bigint,
    cal_id bigint,
    exc boolean DEFAULT false NOT NULL,
    exc_info character varying(255) DEFAULT NULL::character varying
);


ALTER TABLE "g-tomek".stim_seq OWNER TO postgres;

--
-- Name: stim_seq_id_seq; Type: SEQUENCE; Schema: g-tomek; Owner: postgres
--

CREATE SEQUENCE "g-tomek".stim_seq_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "g-tomek".stim_seq_id_seq OWNER TO postgres;

--
-- Name: stim_seq_id_seq; Type: SEQUENCE OWNED BY; Schema: g-tomek; Owner: postgres
--

ALTER SEQUENCE "g-tomek".stim_seq_id_seq OWNED BY "g-tomek".stim_seq.id;


--
-- Name: sub; Type: TABLE; Schema: g-tomek; Owner: postgres
--

CREATE TABLE "g-tomek".sub (
    id bigint NOT NULL,
    exp_id bigint NOT NULL,
    name character varying(128) NOT NULL,
    memo text,
    sex character(1) DEFAULT '-'::bpchar NOT NULL,
    exc boolean DEFAULT false NOT NULL,
    exc_info character varying(255) DEFAULT NULL::character varying
);


ALTER TABLE "g-tomek".sub OWNER TO postgres;

--
-- Name: sub_id_seq; Type: SEQUENCE; Schema: g-tomek; Owner: postgres
--

CREATE SEQUENCE "g-tomek".sub_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "g-tomek".sub_id_seq OWNER TO postgres;

--
-- Name: sub_id_seq; Type: SEQUENCE OWNED BY; Schema: g-tomek; Owner: postgres
--

ALTER SEQUENCE "g-tomek".sub_id_seq OWNED BY "g-tomek".sub.id;


--
-- Name: trial; Type: TABLE; Schema: g-tomek; Owner: postgres
--

CREATE TABLE "g-tomek".trial (
    id bigint NOT NULL,
    sub_id bigint NOT NULL,
    name character varying(64) NOT NULL,
    memo text,
    dur bigint,
    t_buff bigint DEFAULT (0)::bigint NOT NULL,
    exc boolean DEFAULT false NOT NULL,
    exc_info character varying(255) DEFAULT NULL::character varying,
    linked boolean DEFAULT false NOT NULL
);


ALTER TABLE "g-tomek".trial OWNER TO postgres;

--
-- Name: trial_id_seq; Type: SEQUENCE; Schema: g-tomek; Owner: postgres
--

CREATE SEQUENCE "g-tomek".trial_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "g-tomek".trial_id_seq OWNER TO postgres;

--
-- Name: trial_id_seq; Type: SEQUENCE OWNED BY; Schema: g-tomek; Owner: postgres
--

ALTER SEQUENCE "g-tomek".trial_id_seq OWNED BY "g-tomek".trial.id;


--
-- Name: var; Type: TABLE; Schema: g-tomek; Owner: postgres
--

CREATE TABLE "g-tomek".var (
    id bigint NOT NULL,
    obj_name character varying(32) NOT NULL,
    memo text,
    exp_id bigint NOT NULL,
    name character varying(64) NOT NULL,
    sys smallint DEFAULT (0)::smallint NOT NULL,
    type character(1) DEFAULT 'n'::bpchar NOT NULL,
    def_val character varying(128) DEFAULT NULL::character varying
);


ALTER TABLE "g-tomek".var OWNER TO postgres;

--
-- Name: var_id_seq; Type: SEQUENCE; Schema: g-tomek; Owner: postgres
--

CREATE SEQUENCE "g-tomek".var_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "g-tomek".var_id_seq OWNER TO postgres;

--
-- Name: var_id_seq; Type: SEQUENCE OWNED BY; Schema: g-tomek; Owner: postgres
--

ALTER SEQUENCE "g-tomek".var_id_seq OWNED BY "g-tomek".var.id;


--
-- Name: vertex; Type: TABLE; Schema: g-tomek; Owner: postgres
--

CREATE TABLE "g-tomek".vertex (
    id bigint NOT NULL,
    roi_id bigint NOT NULL,
    x bigint NOT NULL,
    y bigint NOT NULL
);


ALTER TABLE "g-tomek".vertex OWNER TO postgres;

--
-- Name: vertex_id_seq; Type: SEQUENCE; Schema: g-tomek; Owner: postgres
--

CREATE SEQUENCE "g-tomek".vertex_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "g-tomek".vertex_id_seq OWNER TO postgres;

--
-- Name: vertex_id_seq; Type: SEQUENCE OWNED BY; Schema: g-tomek; Owner: postgres
--

ALTER SEQUENCE "g-tomek".vertex_id_seq OWNED BY "g-tomek".vertex.id;


--
-- Name: xr_var_roi; Type: TABLE; Schema: g-tomek; Owner: postgres
--

CREATE TABLE "g-tomek".xr_var_roi (
    id bigint NOT NULL,
    var_id bigint NOT NULL,
    roi_id bigint NOT NULL,
    val character varying(128) DEFAULT NULL::character varying
);


ALTER TABLE "g-tomek".xr_var_roi OWNER TO postgres;

--
-- Name: xr_var_roi_id_seq; Type: SEQUENCE; Schema: g-tomek; Owner: postgres
--

CREATE SEQUENCE "g-tomek".xr_var_roi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "g-tomek".xr_var_roi_id_seq OWNER TO postgres;

--
-- Name: xr_var_roi_id_seq; Type: SEQUENCE OWNED BY; Schema: g-tomek; Owner: postgres
--

ALTER SEQUENCE "g-tomek".xr_var_roi_id_seq OWNED BY "g-tomek".xr_var_roi.id;


--
-- Name: xr_var_stim; Type: TABLE; Schema: g-tomek; Owner: postgres
--

CREATE TABLE "g-tomek".xr_var_stim (
    id bigint NOT NULL,
    var_id bigint NOT NULL,
    stim_id bigint NOT NULL,
    val character varying(128) DEFAULT NULL::character varying
);


ALTER TABLE "g-tomek".xr_var_stim OWNER TO postgres;

--
-- Name: xr_var_stim_id_seq; Type: SEQUENCE; Schema: g-tomek; Owner: postgres
--

CREATE SEQUENCE "g-tomek".xr_var_stim_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "g-tomek".xr_var_stim_id_seq OWNER TO postgres;

--
-- Name: xr_var_stim_id_seq; Type: SEQUENCE OWNED BY; Schema: g-tomek; Owner: postgres
--

ALTER SEQUENCE "g-tomek".xr_var_stim_id_seq OWNED BY "g-tomek".xr_var_stim.id;


--
-- Name: xr_var_sub; Type: TABLE; Schema: g-tomek; Owner: postgres
--

CREATE TABLE "g-tomek".xr_var_sub (
    id bigint NOT NULL,
    var_id bigint NOT NULL,
    sub_id bigint NOT NULL,
    val character varying(128) DEFAULT NULL::character varying
);


ALTER TABLE "g-tomek".xr_var_sub OWNER TO postgres;

--
-- Name: xr_var_sub_id_seq; Type: SEQUENCE; Schema: g-tomek; Owner: postgres
--

CREATE SEQUENCE "g-tomek".xr_var_sub_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "g-tomek".xr_var_sub_id_seq OWNER TO postgres;

--
-- Name: xr_var_sub_id_seq; Type: SEQUENCE OWNED BY; Schema: g-tomek; Owner: postgres
--

ALTER SEQUENCE "g-tomek".xr_var_sub_id_seq OWNED BY "g-tomek".xr_var_sub.id;


--
-- Name: xr_var_trial; Type: TABLE; Schema: g-tomek; Owner: postgres
--

CREATE TABLE "g-tomek".xr_var_trial (
    id bigint NOT NULL,
    var_id bigint NOT NULL,
    trial_id bigint NOT NULL,
    val character varying(128) DEFAULT NULL::character varying
);


ALTER TABLE "g-tomek".xr_var_trial OWNER TO postgres;

--
-- Name: xr_var_trial_id_seq; Type: SEQUENCE; Schema: g-tomek; Owner: postgres
--

CREATE SEQUENCE "g-tomek".xr_var_trial_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "g-tomek".xr_var_trial_id_seq OWNER TO postgres;

--
-- Name: xr_var_trial_id_seq; Type: SEQUENCE OWNED BY; Schema: g-tomek; Owner: postgres
--

ALTER SEQUENCE "g-tomek".xr_var_trial_id_seq OWNED BY "g-tomek".xr_var_trial.id;


--
-- Name: blink id; Type: DEFAULT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".blink ALTER COLUMN id SET DEFAULT nextval('"g-tomek".blink_id_seq'::regclass);


--
-- Name: cal id; Type: DEFAULT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".cal ALTER COLUMN id SET DEFAULT nextval('"g-tomek".cal_id_seq'::regclass);


--
-- Name: cal_point id; Type: DEFAULT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".cal_point ALTER COLUMN id SET DEFAULT nextval('"g-tomek".cal_point_id_seq'::regclass);


--
-- Name: ds id; Type: DEFAULT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".ds ALTER COLUMN id SET DEFAULT nextval('"g-tomek".ds_id_seq'::regclass);


--
-- Name: evt id; Type: DEFAULT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".evt ALTER COLUMN id SET DEFAULT nextval('"g-tomek".evt_id_seq'::regclass);


--
-- Name: evt_lst id; Type: DEFAULT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".evt_lst ALTER COLUMN id SET DEFAULT nextval('"g-tomek".evt_lst_id_seq'::regclass);


--
-- Name: exp id; Type: DEFAULT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".exp ALTER COLUMN id SET DEFAULT nextval('"g-tomek".exp_id_seq'::regclass);


--
-- Name: fix id; Type: DEFAULT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".fix ALTER COLUMN id SET DEFAULT nextval('"g-tomek".fix_id_seq'::regclass);


--
-- Name: fix_ln id; Type: DEFAULT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".fix_ln ALTER COLUMN id SET DEFAULT nextval('"g-tomek".fix_ln_id_seq'::regclass);


--
-- Name: form id; Type: DEFAULT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".form ALTER COLUMN id SET DEFAULT nextval('"g-tomek".form_id_seq'::regclass);


--
-- Name: log_cmd id; Type: DEFAULT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".log_cmd ALTER COLUMN id SET DEFAULT nextval('"g-tomek".log_cmd_id_seq'::regclass);


--
-- Name: log_xform id; Type: DEFAULT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".log_xform ALTER COLUMN id SET DEFAULT nextval('"g-tomek".log_xform_id_seq'::regclass);


--
-- Name: roi id; Type: DEFAULT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".roi ALTER COLUMN id SET DEFAULT nextval('"g-tomek".roi_id_seq'::regclass);


--
-- Name: roi_type id; Type: DEFAULT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".roi_type ALTER COLUMN id SET DEFAULT nextval('"g-tomek".roi_type_id_seq'::regclass);


--
-- Name: stim id; Type: DEFAULT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".stim ALTER COLUMN id SET DEFAULT nextval('"g-tomek".stim_id_seq'::regclass);


--
-- Name: stim_seq id; Type: DEFAULT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".stim_seq ALTER COLUMN id SET DEFAULT nextval('"g-tomek".stim_seq_id_seq'::regclass);


--
-- Name: sub id; Type: DEFAULT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".sub ALTER COLUMN id SET DEFAULT nextval('"g-tomek".sub_id_seq'::regclass);


--
-- Name: trial id; Type: DEFAULT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".trial ALTER COLUMN id SET DEFAULT nextval('"g-tomek".trial_id_seq'::regclass);


--
-- Name: var id; Type: DEFAULT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".var ALTER COLUMN id SET DEFAULT nextval('"g-tomek".var_id_seq'::regclass);


--
-- Name: vertex id; Type: DEFAULT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".vertex ALTER COLUMN id SET DEFAULT nextval('"g-tomek".vertex_id_seq'::regclass);


--
-- Name: xr_var_roi id; Type: DEFAULT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".xr_var_roi ALTER COLUMN id SET DEFAULT nextval('"g-tomek".xr_var_roi_id_seq'::regclass);


--
-- Name: xr_var_stim id; Type: DEFAULT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".xr_var_stim ALTER COLUMN id SET DEFAULT nextval('"g-tomek".xr_var_stim_id_seq'::regclass);


--
-- Name: xr_var_sub id; Type: DEFAULT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".xr_var_sub ALTER COLUMN id SET DEFAULT nextval('"g-tomek".xr_var_sub_id_seq'::regclass);


--
-- Name: xr_var_trial id; Type: DEFAULT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".xr_var_trial ALTER COLUMN id SET DEFAULT nextval('"g-tomek".xr_var_trial_id_seq'::regclass);


--
-- Data for Name: blink; Type: TABLE DATA; Schema: g-tomek; Owner: postgres
--

COPY "g-tomek".blink (id, trial_id, stim_id, roi_01_id, roi_02_id, sx, sy, ex, ey, dur, sacc_dur, sacc_amp, sacc_pv, t, stim_seq_id, exc) FROM stdin;
\.


--
-- Data for Name: cal; Type: TABLE DATA; Schema: g-tomek; Owner: postgres
--

COPY "g-tomek".cal (id, trial_id, t, eye, point_cnt, cal_res, val_res, val_err_avg, val_err_max, stim_cnt, dur, scr_w, scr_h) FROM stdin;
\.


--
-- Data for Name: cal_point; Type: TABLE DATA; Schema: g-tomek; Owner: postgres
--

COPY "g-tomek".cal_point (id, cal_id, num, x, y, x_err, y_err, ang_err) FROM stdin;
\.


--
-- Data for Name: ds; Type: TABLE DATA; Schema: g-tomek; Owner: postgres
--

COPY "g-tomek".ds (id, exp_id, ds_id) FROM stdin;
\.


--
-- Data for Name: evt; Type: TABLE DATA; Schema: g-tomek; Owner: postgres
--

COPY "g-tomek".evt (id, evt_lst_id, memo, trial_id, stim_id, stim_seq_id, t_prompt, t_resp, exc, exc_info) FROM stdin;
\.


--
-- Data for Name: evt_lst; Type: TABLE DATA; Schema: g-tomek; Owner: postgres
--

COPY "g-tomek".evt_lst (id, exp_id, memo, name) FROM stdin;
\.


--
-- Data for Name: exp; Type: TABLE DATA; Schema: g-tomek; Owner: postgres
--

COPY "g-tomek".exp (id, prj_id, name, precomp, memo, created, imp_rep, imp_ts, stim_zoom, linked, note) FROM stdin;
\.


--
-- Data for Name: fix; Type: TABLE DATA; Schema: g-tomek; Owner: postgres
--

COPY "g-tomek".fix (id, trial_id, roi_id, num, t, dur, x, y, stim_id, sacc, pupil, stim_seq_id, x_adj, y_adj, roi_id_adj, fix_ln_id, sacc_adj, exc) FROM stdin;
\.


--
-- Data for Name: fix_ln; Type: TABLE DATA; Schema: g-tomek; Owner: postgres
--

COPY "g-tomek".fix_ln (id, stim_seq_id, a, b, r2, sd, se_a, se_b, fix_cnt) FROM stdin;
\.


--
-- Data for Name: form; Type: TABLE DATA; Schema: g-tomek; Owner: postgres
--

COPY "g-tomek".form (id, exp_id, name, memo, created) FROM stdin;
\.


--
-- Data for Name: log_cmd; Type: TABLE DATA; Schema: g-tomek; Owner: postgres
--

COPY "g-tomek".log_cmd (id, exp_id, ts, cmd, qs, exec_t) FROM stdin;
\.


--
-- Data for Name: log_xform; Type: TABLE DATA; Schema: g-tomek; Owner: postgres
--

COPY "g-tomek".log_xform (id, stim_seq_id, ts, tx, ty, sx, sy, a) FROM stdin;
\.


--
-- Data for Name: roi; Type: TABLE DATA; Schema: g-tomek; Owner: postgres
--

COPY "g-tomek".roi (id, stim_id, type_id, name, num) FROM stdin;
\.


--
-- Data for Name: roi_type; Type: TABLE DATA; Schema: g-tomek; Owner: postgres
--

COPY "g-tomek".roi_type (id, name) FROM stdin;
1	rect
2	poly
\.


--
-- Data for Name: stim; Type: TABLE DATA; Schema: g-tomek; Owner: postgres
--

COPY "g-tomek".stim (id, exp_id, name, memo, txt, type, img, res_h, res_v, font_fam, font_size, roi_h, offset_x, offset_y, align_h, align_v, margin_t, margin_l, margin_r, letter_spac, line_h, padding_t, padding_b, box_x1, box_y1, box_x2, box_y2, exc, exc_info, roi_exp, img_roi) FROM stdin;
\.


--
-- Data for Name: stim_seq; Type: TABLE DATA; Schema: g-tomek; Owner: postgres
--

COPY "g-tomek".stim_seq (id, trial_id, stim_id, t0, t1, fix_cnt, fix_cnt_roi, blink_cnt, fix_cnt_exc, fix_cnt_roi_adj, cal_id, exc, exc_info) FROM stdin;
\.


--
-- Data for Name: sub; Type: TABLE DATA; Schema: g-tomek; Owner: postgres
--

COPY "g-tomek".sub (id, exp_id, name, memo, sex, exc, exc_info) FROM stdin;
\.


--
-- Data for Name: trial; Type: TABLE DATA; Schema: g-tomek; Owner: postgres
--

COPY "g-tomek".trial (id, sub_id, name, memo, dur, t_buff, exc, exc_info, linked) FROM stdin;
\.


--
-- Data for Name: var; Type: TABLE DATA; Schema: g-tomek; Owner: postgres
--

COPY "g-tomek".var (id, obj_name, memo, exp_id, name, sys, type, def_val) FROM stdin;
\.


--
-- Data for Name: vertex; Type: TABLE DATA; Schema: g-tomek; Owner: postgres
--

COPY "g-tomek".vertex (id, roi_id, x, y) FROM stdin;
\.


--
-- Data for Name: xr_var_roi; Type: TABLE DATA; Schema: g-tomek; Owner: postgres
--

COPY "g-tomek".xr_var_roi (id, var_id, roi_id, val) FROM stdin;
\.


--
-- Data for Name: xr_var_stim; Type: TABLE DATA; Schema: g-tomek; Owner: postgres
--

COPY "g-tomek".xr_var_stim (id, var_id, stim_id, val) FROM stdin;
\.


--
-- Data for Name: xr_var_sub; Type: TABLE DATA; Schema: g-tomek; Owner: postgres
--

COPY "g-tomek".xr_var_sub (id, var_id, sub_id, val) FROM stdin;
\.


--
-- Data for Name: xr_var_trial; Type: TABLE DATA; Schema: g-tomek; Owner: postgres
--

COPY "g-tomek".xr_var_trial (id, var_id, trial_id, val) FROM stdin;
\.


--
-- Name: blink_id_seq; Type: SEQUENCE SET; Schema: g-tomek; Owner: postgres
--

SELECT pg_catalog.setval('"g-tomek".blink_id_seq', 4143524, true);


--
-- Name: cal_id_seq; Type: SEQUENCE SET; Schema: g-tomek; Owner: postgres
--

SELECT pg_catalog.setval('"g-tomek".cal_id_seq', 9138, true);


--
-- Name: cal_point_id_seq; Type: SEQUENCE SET; Schema: g-tomek; Owner: postgres
--

SELECT pg_catalog.setval('"g-tomek".cal_point_id_seq', 82751, true);


--
-- Name: ds_id_seq; Type: SEQUENCE SET; Schema: g-tomek; Owner: postgres
--

SELECT pg_catalog.setval('"g-tomek".ds_id_seq', 3609, true);


--
-- Name: evt_id_seq; Type: SEQUENCE SET; Schema: g-tomek; Owner: postgres
--

SELECT pg_catalog.setval('"g-tomek".evt_id_seq', 60467, true);


--
-- Name: evt_lst_id_seq; Type: SEQUENCE SET; Schema: g-tomek; Owner: postgres
--

SELECT pg_catalog.setval('"g-tomek".evt_lst_id_seq', 40, true);


--
-- Name: exp_id_seq; Type: SEQUENCE SET; Schema: g-tomek; Owner: postgres
--

SELECT pg_catalog.setval('"g-tomek".exp_id_seq', 96, true);


--
-- Name: fix_id_seq; Type: SEQUENCE SET; Schema: g-tomek; Owner: postgres
--

SELECT pg_catalog.setval('"g-tomek".fix_id_seq', 55668688, true);


--
-- Name: fix_ln_id_seq; Type: SEQUENCE SET; Schema: g-tomek; Owner: postgres
--

SELECT pg_catalog.setval('"g-tomek".fix_ln_id_seq', 1175136, true);


--
-- Name: form_id_seq; Type: SEQUENCE SET; Schema: g-tomek; Owner: postgres
--

SELECT pg_catalog.setval('"g-tomek".form_id_seq', 1, false);


--
-- Name: log_cmd_id_seq; Type: SEQUENCE SET; Schema: g-tomek; Owner: postgres
--

SELECT pg_catalog.setval('"g-tomek".log_cmd_id_seq', 1, false);


--
-- Name: log_xform_id_seq; Type: SEQUENCE SET; Schema: g-tomek; Owner: postgres
--

SELECT pg_catalog.setval('"g-tomek".log_xform_id_seq', 3257, true);


--
-- Name: roi_id_seq; Type: SEQUENCE SET; Schema: g-tomek; Owner: postgres
--

SELECT pg_catalog.setval('"g-tomek".roi_id_seq', 4129138, true);


--
-- Name: roi_type_id_seq; Type: SEQUENCE SET; Schema: g-tomek; Owner: postgres
--

SELECT pg_catalog.setval('"g-tomek".roi_type_id_seq', 2, true);


--
-- Name: stim_id_seq; Type: SEQUENCE SET; Schema: g-tomek; Owner: postgres
--

SELECT pg_catalog.setval('"g-tomek".stim_id_seq', 29552, true);


--
-- Name: stim_seq_id_seq; Type: SEQUENCE SET; Schema: g-tomek; Owner: postgres
--

SELECT pg_catalog.setval('"g-tomek".stim_seq_id_seq', 271872, true);


--
-- Name: sub_id_seq; Type: SEQUENCE SET; Schema: g-tomek; Owner: postgres
--

SELECT pg_catalog.setval('"g-tomek".sub_id_seq', 6093, true);


--
-- Name: trial_id_seq; Type: SEQUENCE SET; Schema: g-tomek; Owner: postgres
--

SELECT pg_catalog.setval('"g-tomek".trial_id_seq', 31678, true);


--
-- Name: var_id_seq; Type: SEQUENCE SET; Schema: g-tomek; Owner: postgres
--

SELECT pg_catalog.setval('"g-tomek".var_id_seq', 731, true);


--
-- Name: vertex_id_seq; Type: SEQUENCE SET; Schema: g-tomek; Owner: postgres
--

SELECT pg_catalog.setval('"g-tomek".vertex_id_seq', 16516541, true);


--
-- Name: xr_var_roi_id_seq; Type: SEQUENCE SET; Schema: g-tomek; Owner: postgres
--

SELECT pg_catalog.setval('"g-tomek".xr_var_roi_id_seq', 1242707, true);


--
-- Name: xr_var_stim_id_seq; Type: SEQUENCE SET; Schema: g-tomek; Owner: postgres
--

SELECT pg_catalog.setval('"g-tomek".xr_var_stim_id_seq', 7, true);


--
-- Name: xr_var_sub_id_seq; Type: SEQUENCE SET; Schema: g-tomek; Owner: postgres
--

SELECT pg_catalog.setval('"g-tomek".xr_var_sub_id_seq', 1790, true);


--
-- Name: xr_var_trial_id_seq; Type: SEQUENCE SET; Schema: g-tomek; Owner: postgres
--

SELECT pg_catalog.setval('"g-tomek".xr_var_trial_id_seq', 1, false);


--
-- Name: blink blink_pkey; Type: CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".blink
    ADD CONSTRAINT blink_pkey PRIMARY KEY (id);


--
-- Name: cal cal_pkey; Type: CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".cal
    ADD CONSTRAINT cal_pkey PRIMARY KEY (id);


--
-- Name: cal_point cal_point_pkey; Type: CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".cal_point
    ADD CONSTRAINT cal_point_pkey PRIMARY KEY (id);


--
-- Name: ds ds_exp_id_key; Type: CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".ds
    ADD CONSTRAINT ds_exp_id_key UNIQUE (exp_id, ds_id);


--
-- Name: ds ds_pkey; Type: CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".ds
    ADD CONSTRAINT ds_pkey PRIMARY KEY (id);


--
-- Name: evt_lst evt_lst_exp_id_name_uniq; Type: CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".evt_lst
    ADD CONSTRAINT evt_lst_exp_id_name_uniq UNIQUE (exp_id, name);


--
-- Name: evt_lst evt_lst_pkey; Type: CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".evt_lst
    ADD CONSTRAINT evt_lst_pkey PRIMARY KEY (id);


--
-- Name: evt evt_pkey; Type: CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".evt
    ADD CONSTRAINT evt_pkey PRIMARY KEY (id);


--
-- Name: exp exp_pkey; Type: CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".exp
    ADD CONSTRAINT exp_pkey PRIMARY KEY (id);


--
-- Name: exp exp_prj_id_name_uniq; Type: CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".exp
    ADD CONSTRAINT exp_prj_id_name_uniq UNIQUE (prj_id, name);


--
-- Name: fix_ln fix_ln_pkey; Type: CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".fix_ln
    ADD CONSTRAINT fix_ln_pkey PRIMARY KEY (id);


--
-- Name: fix fix_pkey; Type: CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".fix
    ADD CONSTRAINT fix_pkey PRIMARY KEY (id);


--
-- Name: fix fix_trial_id_key; Type: CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".fix
    ADD CONSTRAINT fix_trial_id_key UNIQUE (trial_id, stim_id, num);


--
-- Name: form form_pkey; Type: CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".form
    ADD CONSTRAINT form_pkey PRIMARY KEY (id);


--
-- Name: log_cmd log_cmd_pkey; Type: CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".log_cmd
    ADD CONSTRAINT log_cmd_pkey PRIMARY KEY (id);


--
-- Name: log_xform log_xform_pkey; Type: CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".log_xform
    ADD CONSTRAINT log_xform_pkey PRIMARY KEY (id);


--
-- Name: roi roi_pkey; Type: CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".roi
    ADD CONSTRAINT roi_pkey PRIMARY KEY (id);


--
-- Name: roi_type roi_type_pkey; Type: CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".roi_type
    ADD CONSTRAINT roi_type_pkey PRIMARY KEY (id);


--
-- Name: stim stim_exp_id_key; Type: CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".stim
    ADD CONSTRAINT stim_exp_id_key UNIQUE (exp_id, name);


--
-- Name: stim stim_pkey; Type: CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".stim
    ADD CONSTRAINT stim_pkey PRIMARY KEY (id);


--
-- Name: stim_seq stim_seq_pkey; Type: CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".stim_seq
    ADD CONSTRAINT stim_seq_pkey PRIMARY KEY (id);


--
-- Name: stim_seq stim_seq_trial_id_key; Type: CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".stim_seq
    ADD CONSTRAINT stim_seq_trial_id_key UNIQUE (trial_id, stim_id, t0, t1);


--
-- Name: sub sub_exp_id_key; Type: CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".sub
    ADD CONSTRAINT sub_exp_id_key UNIQUE (exp_id, name);


--
-- Name: sub sub_pkey; Type: CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".sub
    ADD CONSTRAINT sub_pkey PRIMARY KEY (id);


--
-- Name: trial trial_pkey; Type: CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".trial
    ADD CONSTRAINT trial_pkey PRIMARY KEY (id);


--
-- Name: trial trial_sub_id_key; Type: CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".trial
    ADD CONSTRAINT trial_sub_id_key UNIQUE (sub_id, name);


--
-- Name: var var_exp_id_key; Type: CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".var
    ADD CONSTRAINT var_exp_id_key UNIQUE (exp_id, obj_name, name);


--
-- Name: var var_pkey; Type: CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".var
    ADD CONSTRAINT var_pkey PRIMARY KEY (id);


--
-- Name: vertex vertex_pkey; Type: CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".vertex
    ADD CONSTRAINT vertex_pkey PRIMARY KEY (id);


--
-- Name: vertex vertex_roi_id_key; Type: CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".vertex
    ADD CONSTRAINT vertex_roi_id_key UNIQUE (roi_id, x, y);


--
-- Name: xr_var_roi xr_var_roi_pkey; Type: CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".xr_var_roi
    ADD CONSTRAINT xr_var_roi_pkey PRIMARY KEY (id);


--
-- Name: xr_var_roi xr_var_roi_var_id_key; Type: CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".xr_var_roi
    ADD CONSTRAINT xr_var_roi_var_id_key UNIQUE (var_id, roi_id);


--
-- Name: xr_var_stim xr_var_stim_pkey; Type: CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".xr_var_stim
    ADD CONSTRAINT xr_var_stim_pkey PRIMARY KEY (id);


--
-- Name: xr_var_stim xr_var_stim_var_id_key; Type: CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".xr_var_stim
    ADD CONSTRAINT xr_var_stim_var_id_key UNIQUE (var_id, stim_id);


--
-- Name: xr_var_sub xr_var_sub_pkey; Type: CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".xr_var_sub
    ADD CONSTRAINT xr_var_sub_pkey PRIMARY KEY (id);


--
-- Name: xr_var_sub xr_var_sub_var_id_key; Type: CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".xr_var_sub
    ADD CONSTRAINT xr_var_sub_var_id_key UNIQUE (var_id, sub_id);


--
-- Name: xr_var_trial xr_var_trial_pkey; Type: CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".xr_var_trial
    ADD CONSTRAINT xr_var_trial_pkey PRIMARY KEY (id);


--
-- Name: xr_var_trial xr_var_trial_var_id_key; Type: CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".xr_var_trial
    ADD CONSTRAINT xr_var_trial_var_id_key UNIQUE (var_id, trial_id);


--
-- Name: blink_roi_01_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX blink_roi_01_id ON "g-tomek".blink USING btree (roi_01_id);


--
-- Name: blink_roi_02_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX blink_roi_02_id ON "g-tomek".blink USING btree (roi_02_id);


--
-- Name: blink_stim_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX blink_stim_id ON "g-tomek".blink USING btree (stim_id);


--
-- Name: blink_stim_seq_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX blink_stim_seq_id ON "g-tomek".blink USING btree (stim_seq_id);


--
-- Name: blink_trial_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX blink_trial_id ON "g-tomek".blink USING btree (trial_id);


--
-- Name: cal_point_cal_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX cal_point_cal_id ON "g-tomek".cal_point USING btree (cal_id);


--
-- Name: cal_trial_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX cal_trial_id ON "g-tomek".cal USING btree (trial_id);


--
-- Name: ds_ds_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX ds_ds_id ON "g-tomek".ds USING btree (ds_id);


--
-- Name: ds_exp_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX ds_exp_id ON "g-tomek".ds USING btree (exp_id);


--
-- Name: evt_evt_lst_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX evt_evt_lst_id ON "g-tomek".evt USING btree (evt_lst_id);


--
-- Name: evt_lst_exp_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX evt_lst_exp_id ON "g-tomek".evt_lst USING btree (exp_id);


--
-- Name: evt_stim_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX evt_stim_id ON "g-tomek".evt USING btree (stim_id);


--
-- Name: evt_stim_seq_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX evt_stim_seq_id ON "g-tomek".evt USING btree (stim_seq_id);


--
-- Name: evt_trial_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX evt_trial_id ON "g-tomek".evt USING btree (trial_id);


--
-- Name: exp_prj_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX exp_prj_id ON "g-tomek".exp USING btree (prj_id);


--
-- Name: fix_fix_ln_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX fix_fix_ln_id ON "g-tomek".fix USING btree (fix_ln_id);


--
-- Name: fix_ln_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX fix_ln_id ON "g-tomek".fix_ln USING btree (stim_seq_id);


--
-- Name: fix_roi_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX fix_roi_id ON "g-tomek".fix USING btree (roi_id);


--
-- Name: fix_roi_id_adj; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX fix_roi_id_adj ON "g-tomek".fix USING btree (roi_id_adj);


--
-- Name: fix_stim_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX fix_stim_id ON "g-tomek".fix USING btree (stim_id);


--
-- Name: fix_stim_seq_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX fix_stim_seq_id ON "g-tomek".fix USING btree (stim_seq_id);


--
-- Name: fix_trial_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX fix_trial_id ON "g-tomek".fix USING btree (trial_id);


--
-- Name: form_exp_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX form_exp_id ON "g-tomek".form USING btree (exp_id);


--
-- Name: log_cmd_exp_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX log_cmd_exp_id ON "g-tomek".log_cmd USING btree (exp_id);


--
-- Name: log_xform_stim_seq_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX log_xform_stim_seq_id ON "g-tomek".log_xform USING btree (stim_seq_id);


--
-- Name: roi_stim_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX roi_stim_id ON "g-tomek".roi USING btree (stim_id);


--
-- Name: roi_type_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX roi_type_id ON "g-tomek".roi USING btree (type_id);


--
-- Name: stim_exp_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX stim_exp_id ON "g-tomek".stim USING btree (exp_id);


--
-- Name: stim_seq_cal_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX stim_seq_cal_id ON "g-tomek".stim_seq USING btree (cal_id);


--
-- Name: stim_seq_stim_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX stim_seq_stim_id ON "g-tomek".stim_seq USING btree (stim_id);


--
-- Name: stim_seq_trial_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX stim_seq_trial_id ON "g-tomek".stim_seq USING btree (trial_id);


--
-- Name: sub_exp_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX sub_exp_id ON "g-tomek".sub USING btree (exp_id);


--
-- Name: trial_sub_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX trial_sub_id ON "g-tomek".trial USING btree (sub_id);


--
-- Name: var_exp_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX var_exp_id ON "g-tomek".var USING btree (exp_id);


--
-- Name: vertex_roi_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX vertex_roi_id ON "g-tomek".vertex USING btree (roi_id);


--
-- Name: xr_var_roi_roi_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX xr_var_roi_roi_id ON "g-tomek".xr_var_roi USING btree (roi_id);


--
-- Name: xr_var_roi_var_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX xr_var_roi_var_id ON "g-tomek".xr_var_roi USING btree (var_id);


--
-- Name: xr_var_stim_stim_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX xr_var_stim_stim_id ON "g-tomek".xr_var_stim USING btree (stim_id);


--
-- Name: xr_var_stim_var_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX xr_var_stim_var_id ON "g-tomek".xr_var_stim USING btree (var_id);


--
-- Name: xr_var_sub_sub_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX xr_var_sub_sub_id ON "g-tomek".xr_var_sub USING btree (sub_id);


--
-- Name: xr_var_sub_var_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX xr_var_sub_var_id ON "g-tomek".xr_var_sub USING btree (var_id);


--
-- Name: xr_var_trial_trial_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX xr_var_trial_trial_id ON "g-tomek".xr_var_trial USING btree (trial_id);


--
-- Name: xr_var_trial_var_id; Type: INDEX; Schema: g-tomek; Owner: postgres
--

CREATE INDEX xr_var_trial_var_id ON "g-tomek".xr_var_trial USING btree (var_id);


--
-- Name: blink blink_roi_01_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".blink
    ADD CONSTRAINT blink_roi_01_id_fkey FOREIGN KEY (roi_01_id) REFERENCES "g-tomek".roi(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blink blink_roi_02_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".blink
    ADD CONSTRAINT blink_roi_02_id_fkey FOREIGN KEY (roi_02_id) REFERENCES "g-tomek".roi(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blink blink_stim_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".blink
    ADD CONSTRAINT blink_stim_id_fkey FOREIGN KEY (stim_id) REFERENCES "g-tomek".stim(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blink blink_stim_seq_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".blink
    ADD CONSTRAINT blink_stim_seq_id_fkey FOREIGN KEY (stim_seq_id) REFERENCES "g-tomek".stim_seq(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blink blink_trial_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".blink
    ADD CONSTRAINT blink_trial_id_fkey FOREIGN KEY (trial_id) REFERENCES "g-tomek".trial(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cal_point cal_point_cal_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".cal_point
    ADD CONSTRAINT cal_point_cal_id_fkey FOREIGN KEY (cal_id) REFERENCES "g-tomek".cal(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: cal cal_trial_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".cal
    ADD CONSTRAINT cal_trial_id_fkey FOREIGN KEY (trial_id) REFERENCES "g-tomek".trial(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: ds ds_ds_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".ds
    ADD CONSTRAINT ds_ds_id_fkey FOREIGN KEY (ds_id) REFERENCES "d-tomek".ds(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ds ds_exp_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".ds
    ADD CONSTRAINT ds_exp_id_fkey FOREIGN KEY (exp_id) REFERENCES "g-tomek".exp(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: evt evt_evt_lst_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".evt
    ADD CONSTRAINT evt_evt_lst_id_fkey FOREIGN KEY (evt_lst_id) REFERENCES "g-tomek".evt_lst(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: evt_lst evt_lst_exp_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".evt_lst
    ADD CONSTRAINT evt_lst_exp_id_fkey FOREIGN KEY (exp_id) REFERENCES "g-tomek".exp(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: evt evt_stim_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".evt
    ADD CONSTRAINT evt_stim_id_fkey FOREIGN KEY (stim_id) REFERENCES "g-tomek".stim(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: evt evt_stim_seq_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".evt
    ADD CONSTRAINT evt_stim_seq_id_fkey FOREIGN KEY (stim_seq_id) REFERENCES "g-tomek".stim_seq(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: evt evt_trial_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".evt
    ADD CONSTRAINT evt_trial_id_fkey FOREIGN KEY (trial_id) REFERENCES "g-tomek".trial(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: exp exp_prj_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".exp
    ADD CONSTRAINT exp_prj_id_fkey FOREIGN KEY (prj_id) REFERENCES m.prj(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fix fix_fix_ln_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".fix
    ADD CONSTRAINT fix_fix_ln_id_fkey FOREIGN KEY (fix_ln_id) REFERENCES "g-tomek".fix_ln(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fix_ln fix_ln_stim_seq_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".fix_ln
    ADD CONSTRAINT fix_ln_stim_seq_id_fkey FOREIGN KEY (stim_seq_id) REFERENCES "g-tomek".stim_seq(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fix fix_roi_id_adj_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".fix
    ADD CONSTRAINT fix_roi_id_adj_fkey FOREIGN KEY (roi_id_adj) REFERENCES "g-tomek".roi(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fix fix_roi_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".fix
    ADD CONSTRAINT fix_roi_id_fkey FOREIGN KEY (roi_id) REFERENCES "g-tomek".roi(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fix fix_stim_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".fix
    ADD CONSTRAINT fix_stim_id_fkey FOREIGN KEY (stim_id) REFERENCES "g-tomek".stim(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fix fix_stim_seq_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".fix
    ADD CONSTRAINT fix_stim_seq_id_fkey FOREIGN KEY (stim_seq_id) REFERENCES "g-tomek".stim_seq(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fix fix_trial_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".fix
    ADD CONSTRAINT fix_trial_id_fkey FOREIGN KEY (trial_id) REFERENCES "g-tomek".trial(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: form form_exp_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".form
    ADD CONSTRAINT form_exp_id_fkey FOREIGN KEY (exp_id) REFERENCES "g-tomek".exp(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: log_cmd log_cmd_exp_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".log_cmd
    ADD CONSTRAINT log_cmd_exp_id_fkey FOREIGN KEY (exp_id) REFERENCES "g-tomek".exp(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: log_xform log_xform_stim_seq_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".log_xform
    ADD CONSTRAINT log_xform_stim_seq_id_fkey FOREIGN KEY (stim_seq_id) REFERENCES "g-tomek".stim_seq(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: roi roi_stim_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".roi
    ADD CONSTRAINT roi_stim_id_fkey FOREIGN KEY (stim_id) REFERENCES "g-tomek".stim(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: roi roi_type_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".roi
    ADD CONSTRAINT roi_type_id_fkey FOREIGN KEY (type_id) REFERENCES "g-tomek".roi_type(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: stim stim_exp_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".stim
    ADD CONSTRAINT stim_exp_id_fkey FOREIGN KEY (exp_id) REFERENCES "g-tomek".exp(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: stim_seq stim_seq_cal_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".stim_seq
    ADD CONSTRAINT stim_seq_cal_id_fkey FOREIGN KEY (cal_id) REFERENCES "g-tomek".cal(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: stim_seq stim_seq_stim_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".stim_seq
    ADD CONSTRAINT stim_seq_stim_id_fkey FOREIGN KEY (stim_id) REFERENCES "g-tomek".stim(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: stim_seq stim_seq_trial_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".stim_seq
    ADD CONSTRAINT stim_seq_trial_id_fkey FOREIGN KEY (trial_id) REFERENCES "g-tomek".trial(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sub sub_exp_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".sub
    ADD CONSTRAINT sub_exp_id_fkey FOREIGN KEY (exp_id) REFERENCES "g-tomek".exp(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: trial trial_sub_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".trial
    ADD CONSTRAINT trial_sub_id_fkey FOREIGN KEY (sub_id) REFERENCES "g-tomek".sub(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: var var_exp_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".var
    ADD CONSTRAINT var_exp_id_fkey FOREIGN KEY (exp_id) REFERENCES "g-tomek".exp(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: vertex vertex_roi_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".vertex
    ADD CONSTRAINT vertex_roi_id_fkey FOREIGN KEY (roi_id) REFERENCES "g-tomek".roi(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: xr_var_roi xr_var_roi_roi_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".xr_var_roi
    ADD CONSTRAINT xr_var_roi_roi_id_fkey FOREIGN KEY (roi_id) REFERENCES "g-tomek".roi(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: xr_var_roi xr_var_roi_var_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".xr_var_roi
    ADD CONSTRAINT xr_var_roi_var_id_fkey FOREIGN KEY (var_id) REFERENCES "g-tomek".var(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: xr_var_stim xr_var_stim_stim_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".xr_var_stim
    ADD CONSTRAINT xr_var_stim_stim_id_fkey FOREIGN KEY (stim_id) REFERENCES "g-tomek".stim(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: xr_var_stim xr_var_stim_var_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".xr_var_stim
    ADD CONSTRAINT xr_var_stim_var_id_fkey FOREIGN KEY (var_id) REFERENCES "g-tomek".var(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: xr_var_sub xr_var_sub_sub_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".xr_var_sub
    ADD CONSTRAINT xr_var_sub_sub_id_fkey FOREIGN KEY (sub_id) REFERENCES "g-tomek".sub(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: xr_var_sub xr_var_sub_var_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".xr_var_sub
    ADD CONSTRAINT xr_var_sub_var_id_fkey FOREIGN KEY (var_id) REFERENCES "g-tomek".var(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: xr_var_trial xr_var_trial_trial_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".xr_var_trial
    ADD CONSTRAINT xr_var_trial_trial_id_fkey FOREIGN KEY (trial_id) REFERENCES "g-tomek".trial(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: xr_var_trial xr_var_trial_var_id_fkey; Type: FK CONSTRAINT; Schema: g-tomek; Owner: postgres
--

ALTER TABLE ONLY "g-tomek".xr_var_trial
    ADD CONSTRAINT xr_var_trial_var_id_fkey FOREIGN KEY (var_id) REFERENCES "g-tomek".var(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

