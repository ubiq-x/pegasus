--
-- PostgreSQL database dump
--

-- Dumped from database version 14.2
-- Dumped by pg_dump version 14.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: m; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA m;


ALTER SCHEMA m OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: app; Type: TABLE; Schema: m; Owner: postgres
--

CREATE TABLE m.app (
    id integer NOT NULL,
    str_id character varying(4) NOT NULL,
    name character varying(32) NOT NULL
);


ALTER TABLE m.app OWNER TO postgres;

--
-- Name: app_id_seq; Type: SEQUENCE; Schema: m; Owner: postgres
--

CREATE SEQUENCE m.app_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE m.app_id_seq OWNER TO postgres;

--
-- Name: app_id_seq; Type: SEQUENCE OWNED BY; Schema: m; Owner: postgres
--

ALTER SEQUENCE m.app_id_seq OWNED BY m.app.id;


--
-- Name: prj; Type: TABLE; Schema: m; Owner: postgres
--

CREATE TABLE m.prj (
    id integer NOT NULL,
    user_id bigint NOT NULL,
    name character varying(128) NOT NULL,
    memo text,
    created timestamp without time zone DEFAULT now() NOT NULL,
    note text
);


ALTER TABLE m.prj OWNER TO postgres;

--
-- Name: prj_id_seq; Type: SEQUENCE; Schema: m; Owner: postgres
--

CREATE SEQUENCE m.prj_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE m.prj_id_seq OWNER TO postgres;

--
-- Name: prj_id_seq; Type: SEQUENCE OWNED BY; Schema: m; Owner: postgres
--

ALTER SEQUENCE m.prj_id_seq OWNED BY m.prj.id;


--
-- Name: user; Type: TABLE; Schema: m; Owner: postgres
--

CREATE TABLE m."user" (
    id integer NOT NULL,
    username character varying(32) NOT NULL,
    passwd character varying(32) NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    login_cnt bigint DEFAULT (0)::bigint NOT NULL,
    email character varying(64) NOT NULL,
    last_login timestamp without time zone,
    pseudo_passwd character varying(32) DEFAULT NULL::character varying
);


ALTER TABLE m."user" OWNER TO postgres;

--
-- Name: user_id_seq; Type: SEQUENCE; Schema: m; Owner: postgres
--

CREATE SEQUENCE m.user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE m.user_id_seq OWNER TO postgres;

--
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: m; Owner: postgres
--

ALTER SEQUENCE m.user_id_seq OWNED BY m."user".id;


--
-- Name: xr_user_app; Type: TABLE; Schema: m; Owner: postgres
--

CREATE TABLE m.xr_user_app (
    id integer NOT NULL,
    user_id bigint NOT NULL,
    app_id bigint NOT NULL
);


ALTER TABLE m.xr_user_app OWNER TO postgres;

--
-- Name: xr_user_app_id_seq; Type: SEQUENCE; Schema: m; Owner: postgres
--

CREATE SEQUENCE m.xr_user_app_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE m.xr_user_app_id_seq OWNER TO postgres;

--
-- Name: xr_user_app_id_seq; Type: SEQUENCE OWNED BY; Schema: m; Owner: postgres
--

ALTER SEQUENCE m.xr_user_app_id_seq OWNED BY m.xr_user_app.id;


--
-- Name: app id; Type: DEFAULT; Schema: m; Owner: postgres
--

ALTER TABLE ONLY m.app ALTER COLUMN id SET DEFAULT nextval('m.app_id_seq'::regclass);


--
-- Name: prj id; Type: DEFAULT; Schema: m; Owner: postgres
--

ALTER TABLE ONLY m.prj ALTER COLUMN id SET DEFAULT nextval('m.prj_id_seq'::regclass);


--
-- Name: user id; Type: DEFAULT; Schema: m; Owner: postgres
--

ALTER TABLE ONLY m."user" ALTER COLUMN id SET DEFAULT nextval('m.user_id_seq'::regclass);


--
-- Name: xr_user_app id; Type: DEFAULT; Schema: m; Owner: postgres
--

ALTER TABLE ONLY m.xr_user_app ALTER COLUMN id SET DEFAULT nextval('m.xr_user_app_id_seq'::regclass);


--
-- Data for Name: app; Type: TABLE DATA; Schema: m; Owner: postgres
--

COPY m.app (id, str_id, name) FROM stdin;
1	data	Data
2	gaze	Gaze
\.


--
-- Data for Name: prj; Type: TABLE DATA; Schema: m; Owner: postgres
--

COPY m.prj (id, user_id, name, memo, created, note) FROM stdin;
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: m; Owner: postgres
--

COPY m."user" (id, username, passwd, created, login_cnt, email, last_login, pseudo_passwd) FROM stdin;
1	tomek	t	2010-04-19 20:07:48	3684	tol7@pitt.edu	2010-08-30 17:36:59.861319	a7yhs2
\.


--
-- Data for Name: xr_user_app; Type: TABLE DATA; Schema: m; Owner: postgres
--

COPY m.xr_user_app (id, user_id, app_id) FROM stdin;
1	1	1
2	1	2
\.


--
-- Name: app_id_seq; Type: SEQUENCE SET; Schema: m; Owner: postgres
--

SELECT pg_catalog.setval('m.app_id_seq', 2, true);


--
-- Name: prj_id_seq; Type: SEQUENCE SET; Schema: m; Owner: postgres
--

SELECT pg_catalog.setval('m.prj_id_seq', 22, true);


--
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: m; Owner: postgres
--

SELECT pg_catalog.setval('m.user_id_seq', 1, false);


--
-- Name: xr_user_app_id_seq; Type: SEQUENCE SET; Schema: m; Owner: postgres
--

SELECT pg_catalog.setval('m.xr_user_app_id_seq', 2, true);


--
-- Name: app app_pkey; Type: CONSTRAINT; Schema: m; Owner: postgres
--

ALTER TABLE ONLY m.app
    ADD CONSTRAINT app_pkey PRIMARY KEY (id);


--
-- Name: prj prj_pkey; Type: CONSTRAINT; Schema: m; Owner: postgres
--

ALTER TABLE ONLY m.prj
    ADD CONSTRAINT prj_pkey PRIMARY KEY (id);


--
-- Name: user user_pkey; Type: CONSTRAINT; Schema: m; Owner: postgres
--

ALTER TABLE ONLY m."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: xr_user_app xr_user_app_pkey; Type: CONSTRAINT; Schema: m; Owner: postgres
--

ALTER TABLE ONLY m.xr_user_app
    ADD CONSTRAINT xr_user_app_pkey PRIMARY KEY (id);


--
-- Name: prj_user_id; Type: INDEX; Schema: m; Owner: postgres
--

CREATE INDEX prj_user_id ON m.prj USING btree (user_id);


--
-- Name: xr_user_app_app_id; Type: INDEX; Schema: m; Owner: postgres
--

CREATE INDEX xr_user_app_app_id ON m.xr_user_app USING btree (app_id);


--
-- Name: xr_user_app_user_id; Type: INDEX; Schema: m; Owner: postgres
--

CREATE INDEX xr_user_app_user_id ON m.xr_user_app USING btree (user_id);


--
-- Name: prj prj_user_id_fkey; Type: FK CONSTRAINT; Schema: m; Owner: postgres
--

ALTER TABLE ONLY m.prj
    ADD CONSTRAINT prj_user_id_fkey FOREIGN KEY (user_id) REFERENCES m."user"(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: xr_user_app xr_user_app_app_id_fkey; Type: FK CONSTRAINT; Schema: m; Owner: postgres
--

ALTER TABLE ONLY m.xr_user_app
    ADD CONSTRAINT xr_user_app_app_id_fkey FOREIGN KEY (app_id) REFERENCES m.app(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- Name: xr_user_app xr_user_app_user_id_fkey; Type: FK CONSTRAINT; Schema: m; Owner: postgres
--

ALTER TABLE ONLY m.xr_user_app
    ADD CONSTRAINT xr_user_app_user_id_fkey FOREIGN KEY (user_id) REFERENCES m."user"(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

